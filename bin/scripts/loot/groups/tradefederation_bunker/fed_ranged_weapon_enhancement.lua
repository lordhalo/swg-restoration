--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

fed_ranged_weapon_enhancement = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "trade_federation_range_enhancement", weight = 9990000},
		{itemTemplate = "trade_federation_range_enhancement_exceptional", weight = 10000}
	}
}

addLootGroupTemplate("fed_ranged_weapon_enhancement", fed_ranged_weapon_enhancement)
