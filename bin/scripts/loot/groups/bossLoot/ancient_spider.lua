ancient_spider = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "ancient_spider_hide", weight = 4000000},
		{itemTemplate = "ancient_spider_venom", weight = 3000000},
		{itemTemplate = "ancient_spider_fang", weight = 3000000}
	}
}

addLootGroupTemplate("ancient_spider", ancient_spider)
