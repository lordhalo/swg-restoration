power_cube_enhancement = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "power_cube", weight = 10000000}
	}
}

addLootGroupTemplate("power_cube_enhancement", power_cube_enhancement)
