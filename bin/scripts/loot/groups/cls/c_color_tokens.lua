c_color_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
	
		{itemTemplate = "color_token_1of5", weight = 5000000},
		{itemTemplate = "color_token_2of5", weight = 3000000},
		{itemTemplate = "color_token_3of5", weight = 1300000},
		{itemTemplate = "color_token_4of5", weight = 695000},
		{itemTemplate = "color_token_5of5", weight = 5000}
	}
}

addLootGroupTemplate("c_color_tokens", c_color_tokens)
