dark_robe_tier5_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		--dark_robe_tier5_tokens
			{itemTemplate = "robe_dar_5_token_1of5", weight = 2000000},
			{itemTemplate = "robe_dar_5_token_2of5", weight = 2000000},
			{itemTemplate = "robe_dar_5_token_3of5", weight = 2000000},
			{itemTemplate = "robe_dar_5_token_4of5", weight = 2000000},
			{itemTemplate = "robe_dar_5_token_5of5", weight = 2000000}
	}
}

addLootGroupTemplate("dark_robe_tier5_tokens", dark_robe_tier5_tokens)