robe_light = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "r_l1", weight = 5000000},
		{itemTemplate = "r_l2", weight = 2500000},
		{itemTemplate = "r_l3", weight = 1250000},
		{itemTemplate = "r_l4", weight = 750000},
		{itemTemplate = "r_l5", weight = 500000}
	}
}

addLootGroupTemplate("robe_light", robe_light)
