g_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
	--1of5 5m
		{itemTemplate = "goggles_token_s1_1of5", weight = 300000},
		{itemTemplate = "goggles_token_s2_1of5", weight = 500000},
		{itemTemplate = "goggles_token_s3_1of5", weight = 200000},
		{itemTemplate = "goggles_token_s4_1of5", weight = 800000},
		{itemTemplate = "goggles_token_s5_1of5", weight = 1200000},
		{itemTemplate = "goggles_token_s6_1of5", weight = 2000000},
	--2of5 2m
		{itemTemplate = "goggles_token_s1_2of5", weight = 100000},
		{itemTemplate = "goggles_token_s2_2of5", weight = 200000},
		{itemTemplate = "goggles_token_s3_2of5", weight = 50000},
		{itemTemplate = "goggles_token_s4_2of5", weight = 300000},
		{itemTemplate = "goggles_token_s5_2of5", weight = 600000},
		{itemTemplate = "goggles_token_s6_2of5", weight = 750000},
	--3of5 1.5m
		{itemTemplate = "goggles_token_s1_3of5", weight = 100000},
		{itemTemplate = "goggles_token_s2_3of5", weight = 150000},
		{itemTemplate = "goggles_token_s3_3of5", weight = 50000},
		{itemTemplate = "goggles_token_s4_3of5", weight = 300000},
		{itemTemplate = "goggles_token_s5_3of5", weight = 400000},
		{itemTemplate = "goggles_token_s6_3of5", weight = 500000},
	--4of5 1m
		{itemTemplate = "goggles_token_s1_4of5", weight = 50000},
		{itemTemplate = "goggles_token_s2_4of5", weight = 75000},
		{itemTemplate = "goggles_token_s3_4of5", weight = 25000},
		{itemTemplate = "goggles_token_s4_4of5", weight = 200000},
		{itemTemplate = "goggles_token_s5_4of5", weight = 300000},
		{itemTemplate = "goggles_token_s6_4of5", weight = 350000},
	--5of5 .5m
		{itemTemplate = "goggles_token_s1_5of5", weight = 10000},
		{itemTemplate = "goggles_token_s2_5of5", weight = 10000},
		{itemTemplate = "goggles_token_s3_5of5", weight = 5000},
		{itemTemplate = "goggles_token_s4_5of5", weight = 120000},
		{itemTemplate = "goggles_token_s5_5of5", weight = 100000},
		{itemTemplate = "goggles_token_s6_5of5", weight = 255000}
	}
}

addLootGroupTemplate("g_tokens", g_tokens)
