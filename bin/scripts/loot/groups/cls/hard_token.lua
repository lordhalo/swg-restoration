hard_token = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {

		{itemTemplate = "hard_token_1of5", weight = 2000000},
		{itemTemplate = "hard_token_2of5", weight = 1000000},
		{itemTemplate = "hard_token_3of5", weight = 2000000},
		{itemTemplate = "hard_token_4of5", weight = 1000000},
		{itemTemplate = "hard_token_5of5", weight = 3000000}

	}
}

addLootGroupTemplate("hard_token", hard_token)
