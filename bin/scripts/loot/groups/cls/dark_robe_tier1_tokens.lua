dark_robe_tier1_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		--Dark Robe Tier 1 Tokens
			{itemTemplate = "robe_dar_1_token_1of5", weight = 2000000},
			{itemTemplate = "robe_dar_1_token_2of5", weight = 2000000},
			{itemTemplate = "robe_dar_1_token_3of5", weight = 2000000},
			{itemTemplate = "robe_dar_1_token_4of5", weight = 2000000},
			{itemTemplate = "robe_dar_1_token_5of5", weight = 2000000}
	}
}

addLootGroupTemplate("dark_robe_tier1_tokens", dark_robe_tier1_tokens)
