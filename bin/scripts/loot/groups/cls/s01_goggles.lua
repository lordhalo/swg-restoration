s01_goggles = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
	--1of5 5m
		{itemTemplate = "goggles_token_s1_1of5", weight = 3000000},
	--2of5 2m
		{itemTemplate = "goggles_token_s1_2of5", weight = 3000000},
	--3of5 1.5m
		{itemTemplate = "goggles_token_s1_3of5", weight = 1500000},
	--4of5 1m
		{itemTemplate = "goggles_token_s1_4of5", weight = 1500000},
	--5of5 .5m
		{itemTemplate = "goggles_token_s1_5of5", weight = 1000000}
	}
}

addLootGroupTemplate("s01_goggles", s01_goggles)
