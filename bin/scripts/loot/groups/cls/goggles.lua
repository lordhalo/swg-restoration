goggles = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "g_s3", weight = 100000},
		{itemTemplate = "g_s1", weight = 200000},
		{itemTemplate = "g_s2", weight = 700000},
		{itemTemplate = "g_s4", weight = 1500000},
		{itemTemplate = "g_s5", weight = 2500000},
		{itemTemplate = "g_s6", weight = 5000000}
	}
}

addLootGroupTemplate("goggles", goggles)
