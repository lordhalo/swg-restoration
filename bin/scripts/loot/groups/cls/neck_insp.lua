neck_insp = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {

		{itemTemplate = "cook_neck", weight = 2000000},
		{itemTemplate = "cyber_neck", weight = 2000000},

		{itemTemplate = "armor_neck", weight = 2000000},
		{itemTemplate = "weapon_neck", weight = 2000000},

		{itemTemplate = "ls_p_neck", weight = 2000000}
	}
}

addLootGroupTemplate("neck_insp", neck_insp)
