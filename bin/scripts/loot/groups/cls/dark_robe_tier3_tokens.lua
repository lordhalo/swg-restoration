dark_robe_tier3_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {

		--dark_robe_tier3_tokens
			{itemTemplate = "robe_dar_3_token_1of5", weight = 2000000},
			{itemTemplate = "robe_dar_3_token_2of5", weight = 2000000},
			{itemTemplate = "robe_dar_3_token_3of5", weight = 2000000},
			{itemTemplate = "robe_dar_3_token_4of5", weight = 2000000},
			{itemTemplate = "robe_dar_3_token_5of5", weight = 2000000}
	}
}

addLootGroupTemplate("dark_robe_tier3_tokens", dark_robe_tier3_tokens)