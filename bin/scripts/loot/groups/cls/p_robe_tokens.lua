p_robe_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
	--1of5 5m
		{itemTemplate = "robe_pad_token_1of5", weight = 1000000},
		{itemTemplate = "robe_pad_token_2of5", weight = 1000000},
		{itemTemplate = "robe_pad_token_3of5", weight = 1000000},
		{itemTemplate = "robe_pad_token_4of5", weight = 1000000},
		{itemTemplate = "robe_pad_token_5of5", weight = 1000000}
	}
}

addLootGroupTemplate("p_robe_tokens", p_robe_tokens)
