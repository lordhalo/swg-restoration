l_robe_tokens = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		--robe 1 5mil
			{itemTemplate = "robe_lig_1_token_1of5", weight = 1000000},
			{itemTemplate = "robe_lig_1_token_2of5", weight = 1000000},
			{itemTemplate = "robe_lig_1_token_3of5", weight = 1000000},
			{itemTemplate = "robe_lig_1_token_4of5", weight = 1000000},
			{itemTemplate = "robe_lig_1_token_5of5", weight = 1000000},

		--robe 2 2.5 mil
			{itemTemplate = "robe_lig_2_token_1of5", weight = 500000},
			{itemTemplate = "robe_lig_2_token_2of5", weight = 500000},
			{itemTemplate = "robe_lig_2_token_3of5", weight = 500000},
			{itemTemplate = "robe_lig_2_token_4of5", weight = 500000},
			{itemTemplate = "robe_lig_2_token_5of5", weight = 500000},

		--robe 3 1.25 mil
			{itemTemplate = "robe_lig_3_token_1of5", weight = 250000},
			{itemTemplate = "robe_lig_3_token_2of5", weight = 250000},
			{itemTemplate = "robe_lig_3_token_3of5", weight = 250000},
			{itemTemplate = "robe_lig_3_token_4of5", weight = 250000},
			{itemTemplate = "robe_lig_3_token_5of5", weight = 250000},

		--robe 4 750k
			{itemTemplate = "robe_lig_4_token_1of5", weight = 150000},
			{itemTemplate = "robe_lig_4_token_2of5", weight = 150000},
			{itemTemplate = "robe_lig_4_token_3of5", weight = 150000},
			{itemTemplate = "robe_lig_4_token_4of5", weight = 150000},
			{itemTemplate = "robe_lig_4_token_5of5", weight = 150000},

		--robe 5 500k
			{itemTemplate = "robe_lig_5_token_1of5", weight = 100000},
			{itemTemplate = "robe_lig_5_token_2of5", weight = 100000},
			{itemTemplate = "robe_lig_5_token_3of5", weight = 100000},
			{itemTemplate = "robe_lig_5_token_4of5", weight = 100000},
			{itemTemplate = "robe_lig_5_token_5of5", weight = 100000}
	}
}

addLootGroupTemplate("l_robe_tokens", l_robe_tokens)
