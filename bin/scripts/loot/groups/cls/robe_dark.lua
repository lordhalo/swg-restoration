robe_dark = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "r_d1", weight = 5000000},
		{itemTemplate = "r_d2", weight = 2500000},
		{itemTemplate = "r_d3", weight = 1250000},
		{itemTemplate = "r_d4", weight = 750000},
		{itemTemplate = "r_d5", weight = 500000}
	}
}

addLootGroupTemplate("robe_dark", robe_dark)
