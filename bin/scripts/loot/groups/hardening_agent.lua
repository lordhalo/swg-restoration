hardening_agent = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "mandalorian_hardening_agent", weight = 10000000},
	}
}

addLootGroupTemplate("hardening_agent", hardening_agent)
