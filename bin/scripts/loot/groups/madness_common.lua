madness_common = {
  description = "",
  minimumLevel = 0,
  maximumLevel = 0,
  lootItems = {
    {itemTemplate = "ring_of_madness", weight = 5000000},
    {itemTemplate = "necklace_of_madness", weight = 5000000},
  }
}

addLootGroupTemplate("madness_common", madness_common)