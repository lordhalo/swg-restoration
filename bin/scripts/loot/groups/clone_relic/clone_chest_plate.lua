clone_chest_plate = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "clone_armor_chest_plate", weight = 10000000}
	}
}

addLootGroupTemplate("clone_chest_plate", clone_chest_plate)
