clone_leggings = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "clone_armor_leggings", weight = 10000000}
	}
}

addLootGroupTemplate("clone_leggings", clone_leggings)
