clone_gloves = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "clone_armor_gloves", weight = 10000000}
	}
}

addLootGroupTemplate("clone_gloves", clone_gloves)
