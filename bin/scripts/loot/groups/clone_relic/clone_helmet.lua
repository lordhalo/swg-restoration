clone_helmet = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "clone_armor_helmet", weight = 10000000}
	}
}

addLootGroupTemplate("clone_helmet", clone_helmet)
