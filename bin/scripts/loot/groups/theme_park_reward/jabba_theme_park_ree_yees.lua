jabba_theme_park_ree_yees = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "palowick_painting_quest_01", weight = 1200000},
		{itemTemplate = "twilek_painting_lg_quest_01", weight = 1100000},
		{itemTemplate = "twilek_painting_lg_quest_02", weight = 1100000},
		{itemTemplate = "twilek_painting_lg_quest_03", weight = 1100000},
		{itemTemplate = "twilek_painting_lg_quest_04", weight = 1100000},
		{itemTemplate = "twilek_painting_quest_01", weight = 1100000},
		{itemTemplate = "twilek_painting_quest_02", weight = 1100000},
		{itemTemplate = "twilek_painting_quest_03", weight = 1100000},
		{itemTemplate = "twilek_painting_quest_04", weight = 1100000}
	}
}

addLootGroupTemplate("jabba_theme_park_ree_yees", jabba_theme_park_ree_yees)
