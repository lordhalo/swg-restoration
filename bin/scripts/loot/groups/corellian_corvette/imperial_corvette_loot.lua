--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

imperial_corvette_loot = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "berserker_schematic", weight = 1250000},
		{itemTemplate = "veh_power_plant_av21", weight = 1250000},
		{itemTemplate = "spice_crash_n_burn", weight = 2600000},
		{itemTemplate = "spice_giggledust", weight = 2600000},
		{itemTemplate = "bantha_doll", weight = 2300000},

	}
}
addLootGroupTemplate("imperial_corvette_loot", imperial_corvette_loot)
