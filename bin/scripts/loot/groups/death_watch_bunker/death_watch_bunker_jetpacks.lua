--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

death_watch_bunker_jetpacks = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "jetpack_schematic", weight = 5000000},
		{itemTemplate = "jetpack_s02_schematic", weight = 5000000}
	}
}

addLootGroupTemplate("death_watch_bunker_jetpacks", death_watch_bunker_jetpacks)
