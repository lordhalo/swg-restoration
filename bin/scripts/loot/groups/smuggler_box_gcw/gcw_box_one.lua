gcw_box_one = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
	--1of5 5m
		{itemTemplate = "robe_dar_1_token_1of5", weight = 1000000},
		{itemTemplate = "robe_dar_2_token_1of5", weight = 1000000},
		{itemTemplate = "robe_dar_3_token_1of5", weight = 1000000},
		{itemTemplate = "robe_dar_4_token_1of5", weight = 1000000},
		{itemTemplate = "robe_dar_5_token_1of5", weight = 1000000},

	--2of5 2.5m
		{itemTemplate = "robe_dar_1_token_2of5", weight = 1000000},
		{itemTemplate = "robe_dar_2_token_2of5", weight = 500000},
		{itemTemplate = "robe_dar_3_token_2of5", weight = 500000},
		{itemTemplate = "robe_dar_4_token_2of5", weight = 250000},
		{itemTemplate = "robe_dar_5_token_2of5", weight = 250000},

	--3of5 1.25
		{itemTemplate = "robe_dar_1_token_3of5", weight = 500000},
		{itemTemplate = "robe_dar_2_token_3of5", weight = 300000},
		{itemTemplate = "robe_dar_3_token_3of5", weight = 250000},
		{itemTemplate = "robe_dar_4_token_3of5", weight = 150000},
		{itemTemplate = "robe_dar_5_token_3of5", weight = 50000},

	--4of5 700k
		{itemTemplate = "robe_dar_1_token_4of5", weight = 300000},
		{itemTemplate = "robe_dar_2_token_4of5", weight = 150000},
		{itemTemplate = "robe_dar_3_token_4of5", weight = 125000},
		{itemTemplate = "robe_dar_4_token_4of5", weight = 100000},
		{itemTemplate = "robe_dar_5_token_4of5", weight = 25000},

	--5of5 550k
		{itemTemplate = "robe_dar_1_token_5of5", weight = 285000},
		{itemTemplate = "robe_dar_2_token_5of5", weight = 150000},
		{itemTemplate = "robe_dar_3_token_5of5", weight = 100000},
		{itemTemplate = "robe_dar_4_token_5of5", weight = 10000},
		{itemTemplate = "robe_dar_5_token_5of5", weight = 5000}
	}
}

addLootGroupTemplate("gcw_box_one", gcw_box_one)
