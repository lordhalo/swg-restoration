--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

event_carbine = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/weapon/ranged/carbine/carbine_e11.iff",
	craftingValues = {
		{"mindamage",520,520,0},
		{"maxdamage",980,980,0},
		{"attackspeed",3.35,3.35,2},
		{"woundchance",5,5,0},
		{"elementalvalue",5,5,0},
		{"hitpoints",700,700,0},
		{"accuracyBonus",5,5,0},
		{"attackactioncost",125,125,0},
	},
	customizationStringNames = {},
	customizationValues = {},

	randomDotChance = 1000,
	junkDealerTypeNeeded = JUNKARMS,
	junkMinValue = 30,
	junkMaxValue = 55

}

addLootItemTemplate("event_carbine", event_carbine)
