event_launcher = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/weapon/ranged/heavy/heavy_rocket_launcher.iff",
	craftingValues = {
		{"mindamage",400,400,0},
		{"maxdamage",1200,1200,0},
		{"attackspeed",3.65,3.65,2},
		{"woundchance",5,5,0},
		{"elementalvalue",5,5,0},
		{"hitpoints",700,700,0},
		{"accuracyBonus",5,5,0},
		{"attackactioncost",125,125,0},
	},

	-- randomDotChance: The chance of this weapon object dropping with a random dot on it. Higher number means less chance. Set to 0 to always have a random dot.
	randomDotChance = 1000,
	junkDealerTypeNeeded = JUNKARMS,
	junkMinValue = 30,
	junkMaxValue = 55
}

addLootItemTemplate("event_launcher", event_launcher)
