

graul_enhancement = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Graul Bone Melee Weapon Enhancement",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",7,10,0},
		{"maxdamage",7,10,0},
		{"attackspeed",0.0,0.0,2},
		{"woundchance",0,0,0},
		{"hitpoints",0,0,0},
		{"wpn_accuracy",0,0,0},
		{"attackactioncost",0,0,0},
		{"useCount",2,6,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("graul_enhancement", graul_enhancement)
