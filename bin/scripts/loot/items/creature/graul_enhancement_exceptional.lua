

graul_enhancement_exceptional = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Graul Bone Melee Weapon Enhancement",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",21,30,0},
		{"maxdamage",21,30,0},
		{"attackspeed",0.0,0.0,2},
		{"woundchance",0,0,0},
		{"hitpoints",0,0,0},
		{"wpn_accuracy",0,0,0},
		{"attackactioncost",0,0,0},
		{"useCount",2,6,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("graul_enhancement_exceptional", graul_enhancement_exceptional)
