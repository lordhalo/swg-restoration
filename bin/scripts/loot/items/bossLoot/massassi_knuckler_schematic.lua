massassi_knuckler_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Massassi Knuckler Schematic",
	directObjectTemplate = "object/tangible/loot/weapons/massassi_knuckler_schematic.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("massassi_knuckler_schematic", massassi_knuckler_schematic)
