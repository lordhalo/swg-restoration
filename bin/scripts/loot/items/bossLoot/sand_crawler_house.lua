sand_crawler_house = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Sand Crawler House Schematic",
	directObjectTemplate = "object/tangible/loot/loot_schematic/sandcrawler_house_loot_schem.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("sand_crawler_house", sand_crawler_house)

