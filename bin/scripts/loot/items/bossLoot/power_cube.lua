power_cube = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/weapon/geonosian_power_cube.iff",
	craftingValues = {
		{"mindamage",50,100,0},
		{"maxdamage",50,100,0},
		{"useCount",1,3,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("power_cube", power_cube)
