cuRewardSilver = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/event_perk/frn_loyalty_award_plaque_silver.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("cuRewardSilver", cuRewardSilver)
