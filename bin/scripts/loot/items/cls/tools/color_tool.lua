--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

color_tool = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exotic Color Crystal Collection Kit",
	directObjectTemplate = "object/tangible/loot/cls/tool/color_crystal.iff",
	craftingValues = { },
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("color_tool", color_tool)
