--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

r_l5 = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Light Jedi Robe Rank 5 Collection Kit",
	directObjectTemplate = "object/tangible/loot/cls/tool/robe_l_5.iff",
	craftingValues = { },
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("r_l5", r_l5)
