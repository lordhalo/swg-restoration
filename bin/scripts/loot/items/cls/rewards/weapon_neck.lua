weapon_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Weapon Smith)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"weapon_experimentation_special", 1}}
}

addLootItemTemplate("weapon_neck", weapon_neck)
