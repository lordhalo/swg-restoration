ls_1_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (One Handed Lightsaber)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"jedi_saber_experimentation_special", 1}}
}

addLootItemTemplate("ls_1_neck", ls_1_neck)
