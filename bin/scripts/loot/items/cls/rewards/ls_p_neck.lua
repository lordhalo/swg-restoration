ls_p_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Lightsaber)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"jedi_saber_experimentation_special", 1}}
}

addLootItemTemplate("ls_p_neck", ls_p_neck)
