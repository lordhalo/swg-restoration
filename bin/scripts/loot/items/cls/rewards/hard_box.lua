hard_box = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Mystery Box (HARD)",
	directObjectTemplate = "object/tangible/loot/cls/reward/hardBox.iff",
	craftingValues = {},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {}
}

addLootItemTemplate("hard_box", hard_box)
