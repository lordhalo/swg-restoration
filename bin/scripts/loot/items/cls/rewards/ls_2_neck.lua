ls_2_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Two Handed Lightsaber)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	skillMods = {{"ls_2_exp_special", 1}}
}

addLootItemTemplate("ls_2_neck", ls_2_neck)
