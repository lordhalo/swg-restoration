cook_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Chef)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"food_experimentation_special", 1}}
}

addLootItemTemplate("cook_neck", cook_neck)
