armor_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Armor Smith)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"armor_experimentation_special", 1}}
}

addLootItemTemplate("armor_neck", armor_neck)
