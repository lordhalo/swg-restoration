cyber_neck = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Necklace of Inspiration (Cybernetics)",
	directObjectTemplate = "object/tangible/wearables/necklace/neck_s01.iff",
	craftingValues = {
	},
	customizationStneckNames = {},
	customizationValues = {},
	skillMods = {{"cyber_experimentation_special", 1}}
}

addLootItemTemplate("cyber_neck", cyber_neck)
