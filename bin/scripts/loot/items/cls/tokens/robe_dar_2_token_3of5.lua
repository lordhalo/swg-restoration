--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

robe_dar_2_token_3of5 = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Dark Jedi Rank 2 Robe Collection part 3 of 5",
	directObjectTemplate = "object/tangible/loot/cls/token/rd2_token_3of5.iff",
	craftingValues = { },
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("robe_dar_2_token_3of5", robe_dar_2_token_3of5)
