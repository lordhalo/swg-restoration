--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

robe_lig_1_token_2of5 = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Light Jedi Rank 1 Robe Collection part 2 of 5",
	directObjectTemplate = "object/tangible/loot/cls/token/rl1_token_2of5.iff",
	craftingValues = { },
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("robe_lig_1_token_2of5", robe_lig_1_token_2of5)
