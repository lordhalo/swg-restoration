--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

goggles_token_s4_1of5 = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "SE Goggles S4 Collection part 1 of 5",
	directObjectTemplate = "object/tangible/loot/cls/token/g4_token_1of5.iff",
	craftingValues = { },
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("goggles_token_s4_1of5", goggles_token_s4_1of5)
