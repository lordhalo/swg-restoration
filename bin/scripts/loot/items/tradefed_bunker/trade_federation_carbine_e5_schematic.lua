trade_federation_carbine_e5_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "E5 Carbine Schematic",
	directObjectTemplate = "object/tangible/loot/weapons/carbine_e5_schematic.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_carbine_e5_schematic", trade_federation_carbine_e5_schematic)
