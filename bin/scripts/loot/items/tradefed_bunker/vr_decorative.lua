vr_decorative = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Decorative Indoor Fountain",
	directObjectTemplate = "object/tangible/tcg/series1/decorative_indoor_fountain_01.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},

}

addLootItemTemplate("vr_decorative", vr_decorative)
