mechno_speeder = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Mechno Chair Deed",
	directObjectTemplate = "object/tangible/tcg/series1/vehicle_deed_mechno_chair.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},

}

addLootItemTemplate("mechno_speeder", mechno_speeder)
