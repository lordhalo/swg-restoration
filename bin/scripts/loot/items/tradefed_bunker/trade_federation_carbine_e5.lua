trade_federation_carbine_e5 = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/weapon/ranged/carbine/carbine_e5.iff",
	craftingValues = {
		{"mindamage",400,400,0},
		{"maxdamage",860,860,0},
		{"attackspeed",3.35,3.35,2},
		{"woundchance",6,8,0},
		{"elementalvalue",8,8,0},
		{"roundsused",30,65,0},
		{"hitpoints",750,1500,0},
		{"accuracyBonus",15,15,0},
		{"attackactioncost",125,125,0},
	},
	customizationStringNames = {},
	customizationValues = {},

	-- randomDotChance: The chance of this weapon object dropping with a random dot on it. Higher number means less chance. Set to 0 to always have a random dot.
	randomDotChance = 1000,
	junkDealerTypeNeeded = JUNKWEAPONS,
	junkMinValue = 20,
	junkMaxValue = 40
}

addLootItemTemplate("trade_federation_carbine_e5", trade_federation_carbine_e5)
