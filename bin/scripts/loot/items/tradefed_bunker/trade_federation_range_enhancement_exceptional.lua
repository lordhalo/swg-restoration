trade_federation_range_enhancement_exceptional = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Trade Federation Beam Cylinder",
	directObjectTemplate = "object/tangible/component/weapon/energization_crystals.iff",
	craftingValues = {
		{"mindamage",22,22,0},
		{"maxdamage",2,22,0},
		{"attackspeed",-0.05,-0.05,2},
		{"woundchance",35,35,0},
		{"hitpoints",100,100,0},
		{"wpn_accuracy",15,30,0},
		{"attackactioncost",0,0,0},
		{"useCount",4,4,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_range_enhancement_exceptional", trade_federation_range_enhancement_exceptional)
