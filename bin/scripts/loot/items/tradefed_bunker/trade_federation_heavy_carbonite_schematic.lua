trade_federation_heavy_carbonite_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "CC-V 'Legion' Cryo Projector Schematic",
	directObjectTemplate = "object/tangible/loot/weapons/heavy_carbonite_schematic.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_heavy_carbonite_schematic", trade_federation_heavy_carbonite_schematic)
