

trade_federation_melee_enhancement_exceptional = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Trade Federation Balancing Weight",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",12,12,0},
		{"maxdamage",12,12,0},
		{"attackspeed",-0.02,-0.02,2},
		{"woundchance",20,20,0},
		{"hitpoints",50,100,0},
		{"wpn_accuracy",0,0,0},
		{"attackactioncost",0,0,0},
		{"useCount",4,4,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_melee_enhancement_exceptional", trade_federation_melee_enhancement_exceptional)
