

trade_federation_melee_enhancement = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Trade Federation Balancing Weight",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",5,10,0},
		{"maxdamage",5,10,0},
		{"attackspeed",0.00,-0.02,2},
		{"woundchance",15,20,0},
		{"hitpoints",50,100,0},
		{"wpn_accuracy",0,0,0},
		{"attackactioncost",0,0,0},
		{"useCount",3,6,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_melee_enhancement", trade_federation_melee_enhancement)
