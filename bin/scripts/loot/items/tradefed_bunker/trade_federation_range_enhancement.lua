trade_federation_range_enhancement = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Trade Federation Beam Cylinder",
	directObjectTemplate = "object/tangible/component/weapon/energization_crystals.iff",
	craftingValues = {
		{"mindamage",5,20,0},
		{"maxdamage",10,20,0},
		{"attackspeed",0.00,-0.03,2},
		{"woundchance",20,35,0},
		{"hitpoints",50,100,0},
		{"wpn_accuracy",5,15,0},
		{"attackactioncost",0,0,0},
		{"useCount",1,3,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("trade_federation_range_enhancement", trade_federation_range_enhancement)
