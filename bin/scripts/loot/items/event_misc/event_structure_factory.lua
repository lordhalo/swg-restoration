event_structure_factory = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/deed/factory_deed/factory_structure_deed.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_structure_factory", event_structure_factory)
