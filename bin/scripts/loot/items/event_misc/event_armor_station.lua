event_armor_station = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/crafting/station/clothing_station.iff",
	craftingValues = {
		{"usemodifier",20,20,10},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_armor_station", event_armor_station)
