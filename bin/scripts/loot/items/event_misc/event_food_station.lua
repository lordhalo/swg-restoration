event_food_station = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/crafting/station/food_station.iff",
	craftingValues = {
		{"usemodifier",20,20,10},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_food_station", event_food_station)
