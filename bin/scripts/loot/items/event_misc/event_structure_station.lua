event_structure_station = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/crafting/station/structure_station.iff",
	craftingValues = {
		{"usemodifier",20,20,10},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_structure_station", event_structure_station)
