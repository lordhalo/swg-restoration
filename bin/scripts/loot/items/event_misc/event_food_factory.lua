event_food_factory = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/deed/factory_deed/factory_food_deed.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_food_factory", event_food_factory)
