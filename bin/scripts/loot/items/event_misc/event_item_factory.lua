event_item_factory = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/deed/factory_deed/factory_item_deed.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_item_factory", event_item_factory)
