event_armor_factory = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/deed/factory_deed/factory_clothing_deed.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_armor_factory", event_armor_factory)
