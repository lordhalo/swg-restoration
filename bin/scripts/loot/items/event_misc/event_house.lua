event_house = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/deed/player_house_deed/generic_house_small_deed.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_house", event_house)
