event_generic_station = {
	minimumLevel = 0,
	maximumLevel = -1,
	directObjectTemplate = "object/tangible/crafting/station/weapon_station.iff",
	craftingValues = {
		{"usemodifier",20,20,10},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("event_generic_station", event_generic_station)
