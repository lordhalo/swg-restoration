npe_crafting_tool = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Noob Crafting Tool -Race-",
	directObjectTemplate = "object/tangible/crafting/station/generic_tool.iff",
	craftingValues = {
		{"usemodifier",12,12,10},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("npe_crafting_tool", npe_crafting_tool)
