decomposed_skull = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/decomposed_skull.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 312,
	junkMaxValue = 312
}

addLootItemTemplate("decomposed_skull", decomposed_skull)
