pod_slime = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/mind_pod_slime.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 350,
	junkMaxValue = 350
}

addLootItemTemplate("pod_slime", pod_slime)
