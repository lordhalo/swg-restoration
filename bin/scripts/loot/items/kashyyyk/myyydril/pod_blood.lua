pod_blood = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/mind_pod_blood.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 250,
	junkMaxValue = 250
}

addLootItemTemplate("pod_blood", pod_blood)
