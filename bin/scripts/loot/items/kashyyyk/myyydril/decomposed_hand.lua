decomposed_hand = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/decomposed_hand.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 285,
	junkMaxValue = 285
}

addLootItemTemplate("decomposed_hand", decomposed_hand)
