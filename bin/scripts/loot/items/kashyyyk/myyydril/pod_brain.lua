pod_brain = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/mind_pod_brain.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 350,
	junkMaxValue = 350
}

addLootItemTemplate("pod_brain", pod_brain)
