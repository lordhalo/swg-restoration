pod_stomach = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/mind_pod_stomach.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 400,
	junkMaxValue = 400
}

addLootItemTemplate("pod_stomach", pod_stomach)
