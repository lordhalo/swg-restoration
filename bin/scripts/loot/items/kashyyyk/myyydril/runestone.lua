runestone = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/runestone.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("runestone", runestone)
