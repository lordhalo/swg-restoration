decomposed_foot = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/decomposed_foot.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 270,
	junkMaxValue = 270
}

addLootItemTemplate("decomposed_foot", decomposed_foot)
