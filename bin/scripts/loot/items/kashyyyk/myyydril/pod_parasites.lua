pod_parasites = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/quest/mind_pod_parasites.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {},
	junkDealerTypeNeeded = JUNKGENERIC,
	junkMinValue = 365,
	junkMaxValue = 365
}

addLootItemTemplate("pod_parasites", pod_parasites)
