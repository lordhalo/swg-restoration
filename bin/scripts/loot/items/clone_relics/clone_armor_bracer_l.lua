
clone_armor_bracer_l = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/wearables/armor/clone_trooper/armor_clone_trooper_neutral_s01_bracer_l.iff",
	craftingValues = {
		{"armor_rating",1,1,0},
		{"kineticeffectiveness",5750,5750,10},
		{"energyeffectiveness",5750,5750,10},
		{"armor_special_effectiveness",5750,5750,10},
		{"armor_integrity",65000,65000,0},
		{"armor_health_encumbrance",0,0,0},
		{"armor_action_encumbrance",0,0,0},
		{"armor_mind_encumbrance",0,0,0},
	},
	customizationStringNames = {},
	customizationValues = {},

	junkDealerTypeNeeded = JUNKARMOUR,
	junkMinValue = 55,
	junkMaxValue = 110
}

addLootItemTemplate("clone_armor_bracer_l", clone_armor_bracer_l)
