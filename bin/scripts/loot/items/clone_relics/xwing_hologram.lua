xwing_hologram = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/furniture/decorative/hologram_xwing.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("xwing_hologram", xwing_hologram)
