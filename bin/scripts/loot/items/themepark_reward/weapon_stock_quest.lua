--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

weapon_stock_quest = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/weapon/stock_advanced_quest.iff",
	craftingValues = {
		{"attackspeed",-0.06,-0.06,0},
		{"wpn_accuracy",5,5,0},
		{"attackactioncost",10,10,0},
		{"useCount",4,4,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("weapon_stock_quest", weapon_stock_quest)
