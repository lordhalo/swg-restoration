
gamorrean_battleaxe_quest = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/weapon/melee/2h_sword/2h_sword_battleaxe.iff",
	craftingValues = {
		{"mindamage",290,290,0},
		{"maxdamage",900,900,0},
		{"attackspeed",3.5,3.5,2},
		{"woundchance",5,5,0},
		{"elementalvalue",8,8,0},
		{"hitpoints",750,750,0},
		{"accuracyBonus",30,30,0},
		{"attackactioncost",115,115,0},
	},
	customizationStringNames = {},
	customizationValues = {},
	skillMods = {{"melee_defense", 15}},

	-- randomDotChance: The chance of this weapon object dropping with a random dot on it. Higher number means less chance. Set to 0 to always have a random dot.
	randomDotChance = -1,

	-- staticDotChance: The chance of this weapon object dropping with a static dot on it. Higher number means less chance. Set to 0 to always have a static dot.
	staticDotChance = 0,

	-- staticDotType: 1 = Poison, 2 = Disease, 3 = Fire, 4 = Bleed
	staticDotType = 4,

	-- staticDotValues: Object map that can randomly or statically generate a dot (used for weapon objects.)
	staticDotValues = {
		{"attribute", 0, 0}, -- See CreatureAttributes.h in src for numbers.
		{"strength", 75, 75},
		{"duration", 30, 30},
		{"potency", 60, 60},
		{"uses", 1000, 1000}
	}

}

addLootItemTemplate("gamorrean_battleaxe_quest", gamorrean_battleaxe_quest)
