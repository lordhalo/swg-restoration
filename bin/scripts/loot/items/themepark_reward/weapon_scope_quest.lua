

weapon_scope_quest = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/weapon/scope_weapon_advanced_quest.iff",
	craftingValues = {
		{"wpn_accuracy",60,60,0},
		{"attackactioncost",20,20,0},
		{"useCount",4,4,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("weapon_scope_quest", weapon_scope_quest)
