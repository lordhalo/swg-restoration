--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

high_velocity_blaster_pistol_barrel = {
	minimumLevel = 0,
	maximumLevel = 0,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/weapon/blaster_pistol_barrel_quest.iff",
	craftingValues = {
		{"mindamage",35,35,0},
		{"maxdamage",50,50,0},
		{"wpn_accuracy",25,25,0},
		{"useCount",5,5,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("high_velocity_blaster_pistol_barrel", high_velocity_blaster_pistol_barrel)
