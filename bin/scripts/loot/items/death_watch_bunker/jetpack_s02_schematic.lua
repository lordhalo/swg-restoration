jetpack_s02_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/death_watch_mandalorian_jetpack_s02_schematic.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("jetpack_s02_schematic", jetpack_s02_schematic)

