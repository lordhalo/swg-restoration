--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

mandalorian_hardening_agent = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/dungeon/death_watch_bunker/mandalorian_hardening_agent.iff",
	craftingValues = {
		{"mindamage",25,100,0},
		{"maxdamage",25,100,0},
		--{"attackspeed",-0.35,-0.5,2},
		{"useCount",1,1,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("mandalorian_hardening_agent", mandalorian_hardening_agent)
