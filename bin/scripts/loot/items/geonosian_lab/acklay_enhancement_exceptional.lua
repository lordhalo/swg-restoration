

acklay_enhancement_exceptional = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Acklay Bone Melee Weapon Enhancement",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",28,28,0},
		{"maxdamage",28,28,0},
		{"attackspeed",-0.05,-0.05,2},
		{"woundchance",10,10,0},
		{"hitpoints",300,300,0},
		{"wpn_accuracy",15,15,0},
		{"attackactioncost",0,0,0},
		{"useCount",4,12,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("acklay_enhancement_exceptional", acklay_enhancement_exceptional)
