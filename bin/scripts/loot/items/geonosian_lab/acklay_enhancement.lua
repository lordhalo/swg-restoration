

acklay_enhancement = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Acklay Bone Melee Weapon Enhancement",
	directObjectTemplate = "object/tangible/component/weapon/melee_core_enhancement.iff",
	craftingValues = {
		{"mindamage",20,25,0},
		{"maxdamage",20,25,0},
		{"attackspeed",0.02,-0.05,2},
		{"woundchance",5,10,0},
		{"hitpoints",100,200,0},
		{"wpn_accuracy",0,15,0},
		{"attackactioncost",0,0,0},
		{"useCount",4,12,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("acklay_enhancement", acklay_enhancement)
