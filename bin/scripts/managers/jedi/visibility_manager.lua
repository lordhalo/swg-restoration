maxVisibility = 8000      -- Maximum visibility
termThreshold = 50      -- Visibility Jedi will show on terminals
falloffThreshold = 15    -- Visibility jedi will disappear off terminals
totalDecayTimeInDays = 3 -- Total time before visibility decays from cap
tickRateInSeconds = 60*60 -- How often visibility will decay in seconds
pvpRatingDivisor = 18      -- Divisor for calculating visibility
