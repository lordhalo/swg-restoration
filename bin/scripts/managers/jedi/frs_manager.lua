frsEnabled = 1

-- Object ID of Enclave buildings
lightEnclaveID = 8525417
darkEnclaveID = 3435626

petitionInterval = 600 -- 1 day
votingInterval = 600 -- 1 day
acceptanceInterval = 600 -- 1 day
maintenanceInterval = 86400000  -- 1 day
promoteInterval = 43200000 -- 12 hours

requestDemotionDuration = 600 -- 7 days
voteChallengeDuration = 600 -- 7 days

-- Costs are in FRS experience
baseMaintCost = 100
requestDemotionCost = 2500
voteChallengeCost = 2000

maxPetitioners = 11
missedVotePenalty = 100 -- FRS XP

darkFrsSkills = {}
lightFrsSkills = {}
sithFrsSkills = {}

-- { rank, skillName, requiredExperience, playerCap }
lightRankingData = {
	{ 0, "rank_light_novice", 0, -1 },
	{ 1, "rank_light_1", 2000, 10 },
	{ 2, "rank_light_2", 4000, 10 },
	{ 3, "rank_light_3", 8000, 10 },
	{ 4, "rank_light_4", 16000, 10 },
	{ 5, "rank_light_5", 1, 9 },
	{ 6, "rank_light_6", 40000, 9 },
	{ 7, "rank_light_7", 50000, 9 },
	{ 8, "rank_light_8", 75000, 8 },
	{ 9, "rank_light_9", 100000, 8 },
	{ 10, "rank_light_10", 200000, 11 },
	{ 11, "rank_light_master", 1, 1 },
}

sithRankingData = {
	{ 0, "rank_sith_novice", 0, -1 },
	{ 1, "rank_sith_1", 2000, 10 },
	{ 2, "rank_sith_2", 4000, 10 },
	{ 3, "rank_sith_3", 8000, 10 },
	{ 4, "rank_sith_4", 16000, 10 },
	{ 5, "rank_sith_5", 1, 9 },
	{ 6, "rank_sith_6", 40000, 9 },
	{ 7, "rank_sith_7", 50000, 9 },
	{ 8, "rank_sith_8", 75000, 8 },
	{ 9, "rank_sith_9", 100000, 8 },
	{ 10, "rank_sith_10", 200000, 11 },
	{ 11, "rank_sith_master", 1, 1 },
}

greyRankingData = {
	{ 0, "rank_grey_novice", 0, -1 },
	{ 1, "rank_grey_1", 2000, 10 },
	{ 2, "rank_grey_2", 4000, 10 },
	{ 3, "rank_grey_3", 8000, 10 },
	{ 4, "rank_grey_4", 16000, 10 },
	{ 5, "rank_grey_5", 1, 9 },
	{ 6, "rank_grey_6", 40000, 9 },
	{ 7, "rank_grey_7", 50000, 9 },
	{ 8, "rank_grey_8", 75000, 8 },
	{ 9, "rank_grey_9", 100000, 8 },
	{ 10, "rank_grey_10", 200000, 11 },
	{ 11, "rank_grey_master", 1, 1 },
}

darkRankingData = {
	{ 0, "rank_dark_novice", 0, -1 },
	{ 1, "rank_dark_1", 2000, 10 },
	{ 2, "rank_dark_2", 4000, 10 },
	{ 3, "rank_dark_3", 8000, 10 },
	{ 4, "rank_dark_4", 16000, 10 },
	{ 5, "rank_dark_5", 1, 9 },
	{ 6, "rank_dark_6", 40000, 9 },
	{ 7, "rank_dark_7", 50000, 9 },
	{ 8, "rank_dark_8", 75000, 8 },
	{ 9, "rank_dark_9", 100000, 8 },
	{ 10, "rank_dark_10", 200000, 11 },
	{ 11, "rank_dark_master", 1, 1 },
}

deathWatchRankingData = {
	{ 0, "death_watch_novice", 0, -1 },
	{ 1, "death_watch_1", 2000, 10 },
	{ 2, "death_watch_2", 4000, 10 },
	{ 3, "death_watch_3", 8000, 10 },
	{ 4, "death_watch_4", 16000, 10 },
	{ 5, "death_watch_5", 1, 9 },
	{ 6, "death_watch_6", 40000, 9 },
	{ 7, "death_watch_7", 50000, 9 },
	{ 8, "death_watch_8", 75000, 8 },
	{ 9, "death_watch_9", 100000, 8 },
	{ 10, "death_watch_10", 200000, 11 },
	{ 11, "death_watch_master", 1, 1 },
}

blackSunRankingData = {
	{ 0, "black_sun_novice", 0, -1 },
	{ 1, "black_sun_1", 2000, 10 },
	{ 2, "black_sun_2", 4000, 10 },
	{ 3, "black_sun_3", 8000, 10 },
	{ 4, "black_sun_4", 16000, 10 },
	{ 5, "black_sun_5", 1, 9 },
	{ 6, "black_sun_6", 40000, 9 },
	{ 7, "black_sun_7", 50000, 9 },
	{ 8, "black_sun_8", 75000, 8 },
	{ 9, "black_sun_9", 100000, 8 },
	{ 10, "black_sun_10", 200000, 11 },
	{ 11, "black_sun_master", 1, 1 },
}

imperialRankingData = {
	{ 0, "imp_rank_novice", 0, -1 },
	{ 1, "imp_rank_1", 2000, 10 },
	{ 2, "imp_rank_2", 4000, 10 },
	{ 3, "imp_rank_3", 8000, 10 },
	{ 4, "imp_rank_4", 16000, 10 },
	{ 5, "imp_rank_5", 1, 9 },
	{ 6, "imp_rank_6", 40000, 9 },
	{ 7, "imp_rank_7", 50000, 9 },
	{ 8, "imp_rank_8", 75000, 8 },
	{ 9, "imp_rank_9", 100000, 8 },
	{ 10, "imp_rank_10", 200000, 11 },
	{ 11, "imp_rank_master", 1, 1 },
}

rebelRankingData = {
	{ 0, "reb_rank_novice", 0, -1 },
	{ 1, "reb_rank_1", 2000, 10 },
	{ 2, "reb_rank_2", 4000, 10 },
	{ 3, "reb_rank_3", 8000, 10 },
	{ 4, "reb_rank_4", 16000, 10 },
	{ 5, "reb_rank_5", 1, 9 },
	{ 6, "reb_rank_6", 40000, 9 },
	{ 7, "reb_rank_7", 50000, 9 },
	{ 8, "reb_rank_8", 75000, 8 },
	{ 9, "reb_rank_9", 100000, 8 },
	{ 10, "reb_rank_10", 200000, 11 },
	{ 11, "reb_rank_master", 1, 1 },
}

mandalorianRankingData = {
	{ 0, "mandalorian_novice", 0, -1 },
	{ 1, "mandalorian_1", 2000, 10 },
	{ 2, "mandalorian_2", 4000, 10 },
	{ 3, "mandalorian_3", 8000, 10 },
	{ 4, "mandalorian_4", 16000, 10 },
	{ 5, "mandalorian_master", 1, 1 },

}

enclaveRoomRequirements = {
	-- Light enclave
	{ 8525444, -1 }, -- entrancehall1
	{ 8525421, 1 }, -- tier1hall1
	{ 8525420, 1 }, -- tier1room
	{ 8525419, 1 }, -- tier1hall2
	{ 8525424, 5 }, -- tier2hall1
	{ 8525423, 5 }, -- tier2room
	{ 8525422, 5 }, -- tier2hall2
	{ 8525433, 8 }, -- tier3hall1
	{ 8525432, 8 }, -- tier3room
	{ 8525431, 8 }, -- tier3hall2
	{ 8525430, 10 }, -- tier4hall1
	{ 8525429, 10 }, -- tier4room
	{ 8525428, 10 }, -- tier4hall2
	{ 8525427, 11 }, -- tier5hall1
	{ 8525426, 11 }, -- tier5room
	{ 8525425, 11 }, -- tier5hall2
	-- Dark enclave
	{ 3435626, -1 }, -- ramp1
	{ 3435644, 1 }, -- hallwayb1
	{ 3435650, 1 }, -- council1
	{ 3435637, 5 }, -- hallwaya1
	{ 3435652, 5 }, -- council3
	{ 3435638, 8 }, -- hallwaya2
	{ 3435653, 8 }, -- council4
	{ 3435645, 8 }, -- hallwayb2
	{ 3435639, 10 }, -- hallwaya3
	{ 3435646, 10 }, -- hallwayb3
	{ 3435651, 10 }, -- council2
	{ 3435640, 11 }, -- hallwaya4
	{ 3435647, 11 }, -- hallwayb4
	{ 3435641, 11 }, -- chamberramp
	{ 3435642, 11 }, -- chamber
}

	-- Key followed by values for player rank 0 through 11
	-- Key references the player's target
	-- Ex: Rank 5 loses to BH, see "bh_lose" key and 6th integer value in same row
frsExperienceValues = {
	{ "nonjedi_win", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "nonjedi_lose", 1000, 1250, 1759, 2250, 3000, 3750, 4750, 5500, 6750, 7750, 8750, 10000 },
	{ "bh_win", 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500 },
	{ "bh_lose", 1000, 1250, 1759, 2250, 3000, 3750, 4750, 5500, 6750, 7750, 8750, 10000 },
	{ "padawan_win", 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200 },
	{ "padawan_lose", 500, 650, 1000, 1250, 1750, 2250, 2750, 2350, 4000, 4500, 5000, 6000 },
	{ "rank0_win", 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750 },
	{ "rank0_lose", 250, 500, 750, 1000, 1500, 2000, 2500, 3000, 3750, 4250, 5000, 5750 },
	{ "rank1_win", 900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900 },
	{ "rank1_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank2_win", 1250, 1250, 1250, 1250, 1250, 1250, 1250, 1250, 1250, 1250, 1250, 1250 },
	{ "rank2_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank3_win", 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250 },
	{ "rank3_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank4_win", 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000 },
	{ "rank4_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank5_win", 3750, 3750, 3750, 3750, 3750, 3750, 3750, 3750, 3750, 3750, 3750, 3750 },
	{ "rank5_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank6_win", 4500, 4500, 4500, 4500, 4500, 4500, 4500, 4500, 4500, 4500, 4500, 4500 },
	{ "rank6_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank7_win", 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500 },
	{ "rank7_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank8_win", 6500, 6500, 6500, 6500, 6500, 6500, 6500, 6500, 6500, 6500, 6500, 6500 },
	{ "rank8_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank9_win", 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500 },
	{ "rank9_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank10_win", 8750, 8750, 8750, 8750, 8750, 8750, 8750, 8750, 8750, 8750, 8750, 8750 },
	{ "rank10_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
	{ "rank11_win", 9750, 9750, 9750, 9750, 9750, 9750, 9750, 9750, 9750, 9750, 9750, 9750 },
	{ "rank11_lose", 100, 250, 500, 900, 1300, 1750, 2250, 2750, 3500, 4150, 4750, 5500 },
}
