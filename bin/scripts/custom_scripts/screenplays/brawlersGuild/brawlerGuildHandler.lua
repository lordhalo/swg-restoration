local ObjectManager = require("managers.object.object_manager")

brawlerGuildScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "brawlerGuildScreenPlay",

}

registerScreenPlay("brawlerGuildScreenPlay", true)

function brawlerGuildScreenPlay:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
	end
end

--Quest giver spawn
function brawlerGuildScreenPlay:spawnMobiles()
	spawnMobile("tatooine", "rafa", 0, 1.3, -0.4, -1.2, -130, 610000397) -- First Quest giver
	spawnMobile("tatooine", "jamgi", 0, 12, 2.5, 14, -87, 610000345) --Second Quest giver
	spawnMobile("tatooine", "trev", 0, 0.1, 2.5, 8.7, -175, 610000348) --Third Quest giver
	spawnMobile("tatooine", "lesk", 0, 3361, 5, -4507, -66, 0)--Jamgi's Quest target for credits.
end

--When you kill the proper target we track your status, and alert the player. Also remove rat waypoint on complete, and give rafa waypoint.
function brawlerGuildScreenPlay:killedTarget(pPlayer, pVictim)
	
	if (string.find(SceneObject(pVictim):getCustomObjectName(), "Basement Womp Rat")) then
		local targetGoal = tonumber(readScreenPlayData(pPlayer, "rafaQuest", "huntTargetGoal"))
		local targetCount = tonumber(readScreenPlayData(pPlayer, "rafaQuest", "huntTargetCount"))
		targetCount = targetCount + 1
		writeScreenPlayData(pPlayer, "rafaQuest", "huntTargetCount", targetCount)

		if (targetCount >= targetGoal) then
			helperScript:removeHunt(pPlayer, "rafaQuest")
			CreatureObject(pPlayer):sendSystemMessage("You've killed all the womp rats in the basement, go see Rafa.")
    		CreatureObject(pPlayer):setScreenPlayState(2, "rafaQuest")
			helperScript:removeQuestWaypoint(pPlayer)
			helperScript:addWaypoint(pPlayer, "tatooine", "Return to Rafa Devia", 3169 ,-4652, "yellow", ":currentQuestWaypointID")
    		return 1
		else
    		CreatureObject(pPlayer):sendSystemMessage("You killed a basement rat. " .. targetGoal - targetCount .. " remaining.")
		end
	end

	return 0
end

function brawlerGuildScreenPlay:spawnTrainer(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	
	local state = CreatureObject(pPlayer):getScreenPlayState("liar")
	local pGhost = CreatureObject(pPlayer):getPlayerObject()
	
	if (state >= 1) then
		local pTrainer = spawnMobile("tatooine", "jaxun", 0, -3.9, 1.1, -0.7, 91, 610000348)
		createObserver(DAMAGERECEIVED, "jamgi_handler", "trainerDamage", pTrainer)
		spatialChat(pTrainer, "Hey I heard someone needs a lesson on lying...")
		if (state < 4) then
			PlayerObject(pGhost):awardBadge(144)
		end
	else
		local pTrainer = spawnMobile("tatooine", "jaxun_easy", 0, -3.9, 1.1, -0.7, 91, 610000348)
		createObserver(DAMAGERECEIVED, "jamgi_handler", "trainerDamage", pTrainer)
		PlayerObject(pGhost):awardBadge(144)
		--awardSkill(pPlayer, "custom_title_brawler_honest") *Need to add to server
		spatialChat(pTrainer, "Whenever you're ready " .. SceneObject(pPlayer):getCustomObjectName() .. " we can begin our training.")
	end
	return
end

function brawlerGuildScreenPlay:trainerDamage(pTrainer, pPlayer)
	if (pTrainer == nil or pPlayer == nil) then
		return 1
	end
	
	local h = CreatureObject(pTrainer):getHAM(0)
	local a = CreatureObject(pTrainer):getHAM(3)
	local m = CreatureObject(pTrainer):getHAM(6)
	local maxH = CreatureObject(pTrainer):getMaxHAM(0)
	local maxA = CreatureObject(pTrainer):getMaxHAM(3)
	local maxM = CreatureObject(pTrainer):getMaxHAM(6)
	local firstHit = readData(SceneObject(pTrainer):getObjectID() .. ":jamgiQuest:spatialChat")
	if ((h <= (maxH * 0.3)) or (a <= (maxA * 0.3)) or (m <= (maxM * 0.3))) then
		CreatureObject(pPlayer):setScreenPlayState(32, "jamgiQuest")
		spatialChat(pTrainer, "Wait! I've had enough " .. SceneObject(pPlayer):getCustomObjectName() .. "! You are very impressive, go see Jam-Gi.")
		forcePeace(pPlayer)
		CreatureObject(pTrainer):setPvpStatusBitmask(0)
		createEvent(10000, "helperScript", "deleteObject", pTrainer, "")
		return 1
	elseif (((h <= maxH) or (a <= maxA) or (m <= maxM)) and (firstHit == 0)) then
		spatialChat(pTrainer, "I've been waiting for you " .. SceneObject(pPlayer):getCustomObjectName() .. "!")
		writeData(SceneObject(pTrainer):getObjectID() .. ":jamgiQuest:spatialChat", 1)
		return 0
	elseif (((h <= (maxH * 0.7)) or (a <= (maxA * 0.7))) and (firstHit == 1)) then
		spatialChat(pTrainer, "Well you should know my unarmed skills aren't the only thing to worry about!")
		if (h <= (maxH - 200)) then
			CreatureObject(pTrainer):setHAM(0, h + 200)
		end
		if (a <= (maxA - 200)) then
			CreatureObject(pTrainer):setHAM(3, a + 200)
		end
		CreatureObject(pTrainer):playEffect("clienteffect/healing_healwound.cef", "")
		writeData(SceneObject(pTrainer):getObjectID() .. ":jamgiQuest:spatialChat", 2)
		return 0
	elseif (((h <= (maxH * 0.5)) or (a <= (maxA * 0.5))) and (firstHit == 2)) then
		spatialChat(pTrainer, "My last stim... Let's make it count!")
		CreatureObject(pTrainer):setHAM(0, maxH - 100)
		CreatureObject(pTrainer):setHAM(3, maxA - 100)
		CreatureObject(pTrainer):setHAM(6, maxM - 100)
		CreatureObject(pTrainer):playEffect("clienteffect/healing_healwound.cef", "")
		writeData(SceneObject(pTrainer):getObjectID() .. ":jamgiQuest:spatialChat", 3)
		return 0
	end
	return 0
end

--Rafa Devia Handler
rafa_handler = conv_handler:new {}

--[[
Purpose: Intro to the brawler's guild.
Requirements: Novice Brawler
Task Details: 
	-Kill basement womp rats
	-return
Reward: Title, Badge, 1 tier 1 skill box in brawler.
]]--

function rafa_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)

	--If you're not a brawler, then you cannot do this quest line.
	if (not CreatureObject(pPlayer):hasSkill("combat_brawler_novice")) then
		return convoTemplate:getScreen("not_ready")
	end

	--[[
		If you have completed (4) the quest chain, then say completed
		If you have killed the rats (2) but not trained your skill yet, then offer training
		If you said you'd kill the rats in the basement, but haven't yet, then say get back to it
		Else we offer the quest
	]]--
	if (CreatureObject(pPlayer):hasScreenPlayState(4, "rafaQuest")) then
		return convoTemplate:getScreen("completed")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(2, "rafaQuest")) then
		return convoTemplate:getScreen("train")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "rafaQuest")) then
		return convoTemplate:getScreen("already")
	else
		return convoTemplate:getScreen("first")
	end
end

function rafa_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()
	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	
	--When you accept the quest to kill the rats.
	if (screenID == "accepted_quest") then
		helperScript:addWaypoint(pPlayer, "tatooine", "Basement Womp-rats", 3168, -4651, "orange", ":currentQuestWaypointID")
		CreatureObject(pPlayer):sendSystemMessage("You've been giving a waypoint to the basement womp-rats.") --Alert the player of the quest via system message.
		CreatureObject(pPlayer):setScreenPlayState(1, "rafaQuest") --We set their state to (1) because they are now on the quest.
		helperScript:setupHunt(pPlayer, "rafaQuest", 3)
		helperScript:handleKillTargets(pPlayer, "rafaQuest", "basement_rat", "tatooine", 0, -2, -5.5, -6.3, 610000392, 3)
		createObserver(KILLEDCREATURE, "brawlerGuildScreenPlay", "killedTarget", pPlayer)
	end
	--When you are done with the quest, time for training.
	if (screenID == "train") then
		helperScript:removeQuestWaypoint(pPlayer)
		--if you don't have a skill offer skill
		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_unarmed_01")) then
			clonedConversation:addOption("Unarmed tier 1 training.", "unarmed_1")
		end
		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_1handmelee_01")) then
			clonedConversation:addOption("1-handed tier 1 training.", "onehand_1")
		end
		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_2handmelee_01")) then
			clonedConversation:addOption("2-handed tier 1 training.", "twohand_1")
		end
		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_polearm_01")) then
			clonedConversation:addOption("polearm tier 1 training.", "polearm_1")
		end
	end
	--This is going to award the skill they pick, and set their state to (4) so they can complete the quest.
	if (screenID == "unarmed_1") then
		awardSkill(pPlayer, "combat_brawler_unarmed_01")
		CreatureObject(pPlayer):setScreenPlayState(4, "rafaQuest")
	end
	if (screenID == "onehand_1") then
		awardSkill(pPlayer, "combat_brawler_1handmelee_01")
		CreatureObject(pPlayer):setScreenPlayState(4, "rafaQuest")
	end
	if (screenID == "twohand_1") then
		awardSkill(pPlayer, "combat_brawler_2handmelee_01")
		CreatureObject(pPlayer):setScreenPlayState(4, "rafaQuest")
	end
	if (screenID == "polearm_1") then
		awardSkill(pPlayer, "combat_brawler_polearm_01")
		CreatureObject(pPlayer):setScreenPlayState(4, "rafaQuest")
	end
	--When you complete, we activate the function handleReward to give them their reward.
	if (screenID == "completed") then
		helperScript:handleReward(pPlayer, "rafa")
		local state = CreatureObject(pPlayer):getScreenPlayState("rafaQuest")
		if (state < 4) then
			CreatureObject(pPlayer):setScreenPlayState(4, "rafaQuest")
		end
	end
	
	return pConvScreen
end

--Jamgi Convo Handler
--[[
Purpose: Test the player on hoensty, and expose them to the easter egg titles/badges/misc rewards.
Requirements: Novice Brawler, Rafa Devia questline.
Task Details: 
	-Go obtain credits from Lesk
		-Possible fight
	-return
		-Possible title, and/or badge
	-Fight a trainer (Depending on your resposnes from previous quest will determine which trainer:Easy, Med, Hard)
	-return
Reward: Title?, Badge?, 1 tier 1 or 2 skill box in brawler.

1 - accept
2 - fight or flight from lesk
4 - collected credits from lesk
8  - returned, and accepted next quest
16 - sent to fight trainer
32 - offer training
64 - training complete, next quest.
]]--
jamgi_handler = conv_handler:new {}

function jamgi_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)

	--Have you completed rafa's quest line yet, and are you still a brawler?
	if (not CreatureObject(pPlayer):hasScreenPlayState(4, "rafaQuest") or (not CreatureObject(pPlayer):hasSkill("combat_brawler_novice"))) then
		return convoTemplate:getScreen("not_ready")
	end

	if (CreatureObject(pPlayer):hasScreenPlayState(64, "jamgiQuest")) then
		return convoTemplate:getScreen("completed")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(32, "jamgiQuest")) then
		return convoTemplate:getScreen("train")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(16, "jamgiQuest")) then
		return convoTemplate:getScreen("already_trainer")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(8, "jamgiQuest")) then --gave credits
		if (CreatureObject(pPlayer):hasScreenPlayState(4, "liar")) then --lied twice
			return convoTemplate:getScreen("liar_next")
		elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "liar")) then --lied once
			return convoTemplate:getScreen("whats_next_after_lying")
		else
			return convoTemplate:getScreen("credits_next") --didn't lie
		end
	elseif (CreatureObject(pPlayer):hasScreenPlayState(4, "jamgiQuest")) then
		return convoTemplate:getScreen("collected_credits")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "jamgiQuest") or CreatureObject(pPlayer):hasScreenPlayState(2, "jamgiQuest")) then
		return convoTemplate:getScreen("already")
	else
		helperScript:removeQuestWaypoint(pPlayer)
		return convoTemplate:getScreen("first")
	end
end

function jamgi_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()

	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	
	if (screenID == "accepted_quest_collect") then --Have you accepted the quest to collect credits from lesk?
		helperScript:addWaypoint(pPlayer, "tatooine", "Meet Lesk here, get my credits!", 3361, -4507, "orange", ":currentQuestWaypointID")
		CreatureObject(pPlayer):setScreenPlayState(1, "jamgiQuest")
		return pConvScreen
	end
	--returned from lesk
	if (screenID == "collected_credits") then
		CreatureObject(pPlayer):setScreenPlayState(8, "jamgiQuest")
		helperScript:removeQuestWaypoint(pPlayer)
		return pConvScreen
	end
	--lied but came clean
	if (screenID == "came_clean") then
		CreatureObject(pPlayer):setScreenPlayState(1, "liar")
		return pConvScreen
	end
	--lied twice
	if (screenID == "liar_2") then
		CreatureObject(pPlayer):setScreenPlayState(4, "liar")
		return pConvScreen
	end
	if (screenID == "credits_next" or screenID == "whats_next_after_lying" or screenID == "liar_next") then
		helperScript:addWaypoint(pPlayer, "tatooine", "Prove your worth.", 3193, -4582, "orange", ":currentQuestWaypointID")
		CreatureObject(pPlayer):setScreenPlayState(16, "jamgiQuest")
		jamgi_handler:spawnTrainer(pPlayer)
	end
	
	--If you are ready for training, then offer available skills to train.
	if (screenID == "train") then
		helperScript:removeQuestWaypoint(pPlayer)
		for u = 1, 2 do
    		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_unarmed_0" .. u)) then
        		clonedConversation:addOption("Unarmed tier " .. u ..  " training.", "unarmed_" .. u)
				writeScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxU", u)
        		break
    		end
		end
		for o = 1, 2 do
    		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_1handmelee_0" .. o)) then
        		clonedConversation:addOption("1-handed tier " .. o ..  " training.", "onehand_" .. o)
				writeScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxO", o)
        		break
    		end
		end
		for t = 1, 2 do
    		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_2handmelee_0" .. t)) then
        		clonedConversation:addOption("2-handed tier " .. t ..  " training.", "twohand_" .. t)
				writeScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxT", t)
        		break
    		end
		end
		for p = 1, 2 do
    		if (not CreatureObject(pPlayer):hasSkill("combat_brawler_polearm_0" .. p)) then
        		clonedConversation:addOption("Polearm tier " .. p ..  " training.", "polearm_" .. p)
				writeScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxP", p)
        		break
    		end
		end
	end
	
	--Train in the skill box you requested
	local boxU = tonumber(readScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxU"))
	local boxO = tonumber(readScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxO"))
	local boxT = tonumber(readScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxT"))
	local boxP = tonumber(readScreenPlayData(pPlayer, "jamgiQuest", "trainedBoxP"))
	if (boxU == nil) then
		boxU = 0
	end
	if (boxO == nil) then
		boxO = 0
	end
	if (boxT == nil) then
		boxT = 0
	end
	if (boxP == nil) then
		boxP = 0
	end
	if (screenID == "unarmed_" .. boxU) then
		awardSkill(pPlayer, "combat_brawler_unarmed_0" .. boxU)
		CreatureObject(pPlayer):setScreenPlayState(64, "jamgiQuest")
	end
	if (screenID == "onehand_" .. boxO) then
		awardSkill(pPlayer, "combat_brawler_1handmelee_0" .. boxO)
		CreatureObject(pPlayer):setScreenPlayState(64, "jamgiQuest")
	end
	if (screenID == "twohand_" .. boxT) then
		awardSkill(pPlayer, "combat_brawler_2handmelee_0" .. boxT)
		CreatureObject(pPlayer):setScreenPlayState(64, "jamgiQuest")
	end
	if (screenID == "polearm_" .. boxP) then
		awardSkill(pPlayer, "combat_brawler_polearm_0" .. boxP)
		CreatureObject(pPlayer):setScreenPlayState(64, "jamgiQuest")
	end
	
	if (screenID == "completed") then
		helperScript:handleReward(pPlayer, "jamgi")
		local state = CreatureObject(pPlayer):getScreenPlayState("jamgiQuest")
		if (state < 64) then
			CreatureObject(pPlayer):setScreenPlayState(64, "jamgiQuest")
		end
	end
	
	return pConvScreen
end

--Lesk Convo Handler
--[[
	Player is sent to collect credits from Lesk (which lesk is in the brawler's guild, just undercover)
	They can attempt to threaten Lesk, and he will send two thugs after the player.
	If they player doesn't threaten, then they can just take the credits from him.
	After the player obtains the credits, they will be given a waypoint back to Jamgi
]]--
lesk_handler = conv_handler:new {}

function lesk_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)

	if (not CreatureObject(pPlayer):hasScreenPlayState(1, "jamgiQuest")) then
		return convoTemplate:getScreen("not_ready")
	end

	if (CreatureObject(pPlayer):hasScreenPlayState(4, "jamgiQuest")) then
		return convoTemplate:getScreen("completed")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "lesk") and not CreatureObject(pPlayer):hasScreenPlayState(2, "lesk")) then
		return convoTemplate:getScreen("threat")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(2, "lesk")) then
		return convoTemplate:getScreen("after_jump")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(2, "jamgiQuest")) then
		return convoTemplate:getScreen("no_trouble")
	else
		return convoTemplate:getScreen("first")
	end
end

function lesk_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()

	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)

	if (screenID == "threat") then
		if (not CreatureObject(pPlayer):hasScreenPlayState(1, "lesk")) then
			lesk_handler:spawnEnemies(pPlayer)
			CreatureObject(pPlayer):setScreenPlayState(1, "lesk")
		end
		return pConvScreen
	end
	if (screenID == "no_trouble" or screenID == "after_jump") then
		CreatureObject(pPlayer):setScreenPlayState(4, "jamgiQuest")
		helperScript:removeQuestWaypoint(pPlayer)
		helperScript:addWaypoint(pPlayer, "tatooine", "Return to Jam-gi", 3171, -4585, "purple", ":currentQuestWaypointID")
	end
	return pConvScreen
end

--This function will spawn the enemies if they threaten lesk
function lesk_handler:spawnEnemies(pPlayer)
	local pThugOne = spawnMobile("tatooine", "lesk_thug", 0, 3363, 5, -4507, -66, 0)
	CreatureObject(pThugOne):setCustomObjectName("Jaq (Lesk thug)")
	local pThugTwo = spawnMobile("tatooine", "lesk_thug", 0, 3368, 5, -4507, -66, 0)
	CreatureObject(pThugTwo):setCustomObjectName("Lau (Lesk thug)")
	
	CreatureObject(pThugOne):engageCombat(pPlayer)
	CreatureObject(pThugTwo):engageCombat(pPlayer)
	
	spatialChat(pThugOne, "Let's do it Lau!")

	createObserver(OBJECTDESTRUCTION, "lesk_handler", "notifyMobileDied", pThugOne)
	createObserver(OBJECTDESTRUCTION, "lesk_handler", "notifyMobileDied", pThugTwo)
end

function lesk_handler:notifyMobileDied(pVictim, pKiller)
	if (pVictim == nil or pKiller == nil) then
		return 0
	end
	
	writeData(SceneObject(pKiller):getObjectID() .. ":lesk:mobileKilled", readData(SceneObject(pKiller):getObjectID() .. ":lesk:mobileKilled") + 1)
	
	local kill = readData(SceneObject(pKiller):getObjectID() .. ":lesk:mobileKilled")
	
	if (kill == 2) then
		CreatureObject(pKiller):setScreenPlayState(2, "lesk")
		CreatureObject(pKiller):setScreenPlayState(2, "jamgiQuest")
		CreatureObject(pKiller):sendSystemMessage("You killed both thugs, return to Lesk.")
		return 1
	end
	return 0
end

--[[
Trev Alask Convo Handler

1 - accepted patrol mission

]]--
trev_handler = conv_handler:new {}
--Need to add "complete" check
function trev_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)

	if (not CreatureObject(pPlayer):hasScreenPlayState(64, "jamgiQuest")) then
		return convoTemplate:getScreen("not_ready")
	end

	if (CreatureObject(pPlayer):hasScreenPlayState(2, "trevQuest")) then
		if (CreatureObject(pPlayer):hasScreenPlayState(1, "killedCriminal")) then
			return convoTemplate:getScreen("return_from_quest_special")
		else
			return convoTemplate:getScreen("return_from_quest")
		end
	elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "trevQuest")) then
		return convoTemplate:getScreen("already")
	else
		return convoTemplate:getScreen("first")
	end
end
--Add check for "special return" to add title/badge
function trev_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()

	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	
	if (screenID == "first") then
		helperScript:removeQuestWaypoint(pPlayer)
	end

	if (screenID == "accept_quest" or screenID == "accept_quest_inquire") then
		CreatureObject(pPlayer):setScreenPlayState(1, "trevQuest")
		trev_handler:setupAreas(pPlayer)
		CreatureObject(pPlayer):setScreenPlayState(1, "patrolPoint")
		helperScript:addWaypoint(pPlayer, "tatooine", "First Patrol Point", 3160, -4625, "purple", ":currentQuestWaypointID")
		return pConvScreen
	end

	return pConvScreen
end

function trev_handler:setupAreas(pPlayer)

	local pFirstPoint = spawnActiveArea("tatooine", "object/active_area.iff", 3160, 5, -4625, 5, 0)
	local pSecondPoint = spawnActiveArea("tatooine", "object/active_area.iff", 3144, 6, -4556, 5, 0)
	local pThirdPoint = spawnActiveArea("tatooine", "object/active_area.iff", 3333, 5, -4504, 5, 0)

	if (pFirstPoint ~= nil) then
		createObserver(ENTEREDAREA, "trev_handler", "notifyAtPatrol", pFirstPoint)
	end
	
	if (pSecondPoint ~= nil) then
		createObserver(ENTEREDAREA, "trev_handler", "notifyAtPatrol", pSecondPoint)
	end
	
	if (pThirdPoint ~= nil) then
		createObserver(ENTEREDAREA, "trev_handler", "notifyAtPatrol", pThirdPoint)
	end
end

function trev_handler:notifyAtPatrol(pArea, pPlayer)
	if (pPlayer == nil or pArea == nil) then
		return 0
	end
	
	local playerID = CreatureObject(pPlayer):getObjectID()
	local state = CreatureObject(pPlayer):getScreenPlayState("patrolPoint")
	
	if (state == 1) then
		helperScript:removeQuestWaypoint(pPlayer)
		CreatureObject(pPlayer):sendSystemMessage("You have arrived to the first patrol point. You should check inspect the area before moving on.")
		createEvent(10000, "trev_handler", "delayNextPatrol", pPlayer, "")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(2, "patrolPoint")
		local cords = { x = 3156.89, z = 5, y = -4629.48, r = math.rad(180), cell = 0}
		local pContainer = spawnSceneObject("tatooine", "object/tangible/container/general/tech_chest.iff", cords.x, cords.z, cords.y, cords.cell, cords.r)
		CreatureObject(pContainer):setCustomObjectName("Looted Storage Container")
		writeData(playerID .. ":trev_handler:containerID", SceneObject(pContainer):getObjectID())
		return 1
	elseif (state == 4) then
		helperScript:removeQuestWaypoint(pPlayer)
		removeQuestStatus(SceneObject(pPlayer):getObjectID() .. ":currentQuestWaypointID")
		CreatureObject(pPlayer):sendSystemMessage("You have arrived to the second patrol point. You should check inspect the area before moving on.")
		createEvent(10000, "trev_handler", "delayNextPatrol", pPlayer, "")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(8, "patrolPoint")
		local cords = { x = 3165.48, z = 5.00252, y = -4564.02, r = math.rad(180), cell = 0}
		local pContainer = spawnSceneObject("tatooine", "object/tangible/container/general/tech_chest.iff", cords.x, cords.z, cords.y, cords.cell, cords.r)
		CreatureObject(pContainer):setCustomObjectName("Looted Storage Container")
		writeData(playerID .. ":trev_handler:containerID", SceneObject(pContainer):getObjectID())
		return 1
	elseif (state == 16) then
		local pFigure = spawnMobile("tatooine", "lesk_thug", 0, 3317, 5, -4513, 115, 0)
		createEvent(15000, "helperScript", "deleteObject", pFigure, "")
		createObserver(OBJECTDESTRUCTION, "trev_handler", "theyKilledMe", pFigure)
		spatialChat(pFigure, "Hm, let's go, someone's here.")
		helperScript:removeQuestWaypoint(pPlayer)
		CreatureObject(pPlayer):sendSystemMessage("You have arrived to the last patrol point. You should check inspect the area before moving on.")
		createEvent(15000, "trev_handler", "delayNextPatrol", pPlayer, "")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(32, "patrolPoint")
		local cords = { x = 3318.71, z = 5, y = -4514.67, r = math.rad(180), cell = 0}
		local pContainer = spawnSceneObject("tatooine", "object/tangible/container/general/tech_chest.iff", cords.x, cords.z, cords.y, cords.cell, cords.r)
		CreatureObject(pContainer):setCustomObjectName("Looted Storage Container")
		writeData(playerID .. ":trev_handler:containerID", SceneObject(pContainer):getObjectID())
		return 1
	end
	return 0
end

function trev_handler:delayNextPatrol(pPlayer)
	if (pPlayer == nil) then
		return 0
	end
	
	local state = CreatureObject(pPlayer):getScreenPlayState("patrolPoint")
	
	if (state == 2) then
		CreatureObject(pPlayer):sendSystemMessage("Looks like you have inspected the area well enough. Head to the next patrol point.")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(4, "patrolPoint")
		helperScript:addWaypoint(pPlayer, "tatooine", "Second Patrol Point", 3144, -4556, "purple", ":currentQuestWaypointID")
		helperScript:removeObject(pPlayer, "trev_handler", "containerID")
	elseif (state == 8) then
		CreatureObject(pPlayer):sendSystemMessage("Looks like you have inspected the area well enough. Head to the last patrol point.")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(16, "patrolPoint")
		helperScript:addWaypoint(pPlayer, "tatooine", "Last Patrol Point", 3333, -4504, "purple", ":currentQuestWaypointID")
		helperScript:removeObject(pPlayer, "trev_handler", "containerID")
	elseif (state == 32) then
		CreatureObject(pPlayer):sendSystemMessage("Looks like you have inspected the area well enough. Head back to Trev Alask.")
		CreatureObject(pPlayer):removeScreenPlayState(state, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(64, "patrolPoint")
		CreatureObject(pPlayer):setScreenPlayState(2, "trevQuest")
		helperScript:addWaypoint(pPlayer, "tatooine", "Return to Trev Alask", 3184, -4587, "purple", ":currentQuestWaypointID")
		helperScript:removeObject(pPlayer, "trev_handler", "containerID")
	end
	return 0
end

function trev_handler:theyKilledMe(pFigure, pPlayer)
	if (pFigure == nil or pPlayer == nil) then
		return
	end
	CreatureObject(pPlayer):setScreenPlayState(1, "killedCriminal")
	return 1
end
