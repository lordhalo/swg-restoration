local ObjectManager = require("managers.object.object_manager")

TatooineDeepBunker = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "TatooineDeepBunker",

	buildingID = 9995371
}

registerScreenPlay("TatooineDeepBunker", true)

function TatooineDeepBunker:start()
	if isZoneEnabled("tatooine") then
		local pBuilding = getSceneObject(self.buildingID)
		createObserver(FACTIONBASEFLIPPED, "TatooineDeepBunker", "flipBase", pBuilding)

		if getRandomNumber(100) >= 50 then
			self:spawnRebels(pBuilding)
		else
			self:spawnImperials(pBuilding)
		end
	end
end

function TatooineDeepBunker:flipBase(pBuilding)
	if (pBuilding == nil) then
		return 1
	end

	BuildingObject(pBuilding):destroyChildObjects()

	if BuildingObject(pBuilding):getFaction() == FACTIONIMPERIAL then
		self:spawnRebels(pBuilding)
	elseif BuildingObject(pBuilding):getFaction() == FACTIONREBEL then
		self:spawnImperials(pBuilding)
	end

	return 0
end

function TatooineDeepBunker:spawnImperials(pBuilding)
		BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONIMPERIAL)

		--imperial decorations
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1130.5, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1137.0, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1153.8, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1117.3, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1133.9, 77.2, 4511.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1145.8, 89.0, 4552.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1136.4, 78.1, 4584.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", 6.7, -12.0, 41.4, 479820, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", 0.2, -12.0, 41.4, 479820, 1, 0, 0, 0)

		--imperials outside
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 300, -819, 9.0, -4566, 0, 0)

		--imperials inside
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 300, 0.0, 0.3, 8.9, 0,  479816)

end

function TatooineDeepBunker:spawnRebels(pBuilding)
		BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONREBEL)

		--rebel decorations
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1130.5, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1137.0, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1153.8, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1117.3, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1133.9, 77.2, 4511.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1145.8, 89.0, 4552.4, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1136.4, 78.1, 4584.3, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1231.9, 75.9, 4500.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/camp/camp_pavilion_s1.iff", -1231.9, 75.9, 4480.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", 6.7, -12.0, 41.4, 479820, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", 0.2, -12.0, 41.4, 479820, 1, 0, 0, 0)

		--rebels outside
		BuildingObject(pBuilding):spawnChildCreature("rebel_trooper", 300, -819, 9.0, -4566, 0, 0)

		--rebels inside
		BuildingObject(pBuilding):spawnChildCreature("rebel_trooper", 300, 0.0, 0.3, 8.9, 0, 479816)

end
