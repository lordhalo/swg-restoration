local ObjectManager = require("managers.object.object_manager")

antennaDestructionImperial = ScreenPlay:new {
	numberOfActs = 1,
	questString = "antennaDestructionImperial",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("antennaDestructionImperial", true)

function antennaDestructionImperial:start()

end

--Setup

function antennaDestructionImperial:getActivePlayerName()
	return self.questdata.activePlayerName
end

function antennaDestructionImperial:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function antennaDestructionImperial:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(antennaDestructionImperial.states.quest.phasetwo, antennaDestructionImperial.questString)
	player:removeScreenPlayState(antennaDestructionImperial.states.quest.phaseone, antennaDestructionImperial.questString)
	player:removeScreenPlayState(antennaDestructionImperial.states.quest.intro, antennaDestructionImperial.questString)

end


captain_okto_convo_handler = Object:new {
	
 }

function captain_okto_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "antennaDestructionImperial")
			local questTurnin = creature:hasScreenPlayState(4, "antennaDestructionImperial")
			local questComplete = creature:hasScreenPlayState(8, "antennaDestructionImperial")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function captain_okto_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, antennaDestructionImperial.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, antennaDestructionImperial.questString)
		antennaDestructionImperial:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("imperial_resuss", 8)
		PlayerObject(pGhost):increaseFactionStanding("imperial", 50)
		
	end
	


	return conversationScreen
end

