local ObjectManager = require("managers.object.object_manager")

totalWarRebel = ScreenPlay:new {
	numberOfActs = 1,
	questString = "totalWarRebel",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("totalWarRebel", true)

function totalWarRebel:start()

end

--Setup

function totalWarRebel:getActivePlayerName()
	return self.questdata.activePlayerName
end

function totalWarRebel:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function totalWarRebel:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(totalWarRebel.states.quest.phasetwo, totalWarRebel.questString)
	player:removeScreenPlayState(totalWarRebel.states.quest.phaseone, totalWarRebel.questString)
	player:removeScreenPlayState(totalWarRebel.states.quest.intro, totalWarRebel.questString)

end

function totalWarRebel:notifyKilledHuntTarget(pPlayer, pVictim)
	local player = LuaCreatureObject(pPlayer)
	local huntTarget = readScreenPlayData(pPlayer, "totalWarRebel", "huntTarget")
	local targetCount = tonumber(readScreenPlayData(pPlayer, "totalWarRebel", "huntTargetCount"))
	local targetGoal = tonumber(readScreenPlayData(pPlayer, "totalWarRebel", "huntTargetGoal"))
	local targetList = HelperFuncs:splitString(huntTarget, ";")
	if (huntTarget == SceneObject(pVictim):getObjectName() or HelperFuncs:tableContainsValue(targetList, SceneObject(pVictim):getObjectName())) then	
		targetCount = targetCount + 1
		writeScreenPlayData(pPlayer, "totalWarRebel", "huntTargetCount", targetCount)
		CreatureObject(pPlayer):sendSystemMessage("Total War Progress: " .. readScreenPlayData(pPlayer, "totalWarRebel", "huntTargetCount") .. "/ 20")

		if (targetCount >= targetGoal) then
			player:setScreenPlayState(4, totalWarRebel.questString)
			return 1
		end
	end

	return 0
end


captain_redding_convo_handler = Object:new {
	
 }

function captain_redding_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "totalWarRebel")
			local questTurnin = creature:hasScreenPlayState(4, "totalWarRebel")
			local questEliteTurnin = creature:hasScreenPlayState(4, "totalWarRebelElite")
			local questComplete = creature:hasScreenPlayState(8, "totalWarRebel")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questEliteTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_elite_screen")
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function captain_redding_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()
	local hasElite = player:hasScreenPlayState(2, "totalWarRebelElite")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, totalWarRebel.questString)

		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTarget", "stormtrooper_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTargetGoal", 20)
		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarRebel", "notifyKilledHuntTarget", conversingPlayer)
	end

	if ( screenID == "accept_screen_elite" ) then
		player:setScreenPlayState(2, totalWarRebelElite.questString)

		writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTarget", "dark_trooper_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTargetGoal", 5)
		writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarRebelElite", "notifyKilledHuntTarget", conversingPlayer)
	end


	if ( screenID == "restart_screen" ) then
		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTarget", "stormtrooper_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTargetGoal", 20)
		writeScreenPlayData(conversingPlayer, "totalWarRebel", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarRebel", "notifyKilledHuntTarget", conversingPlayer)

		if(hasElite == true) then
			writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTarget", "darktrooper_restuss")
			writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTargetGoal", 5)
			writeScreenPlayData(conversingPlayer, "totalWarRebelElite", "huntTargetCount", 0)
			createObserver(KILLEDCREATURE, "totalWarRebelElite", "notifyKilledHuntTarget", conversingPlayer)
		end
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, totalWarRebel.questString)
		totalWarRebel:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("rebel_restuss", 2)
		PlayerObject(pGhost):increaseFactionStanding("rebel", 50)
		
	end

	if ( screenID == "complete_elite_screen_final" ) then
		player:setScreenPlayState(8, totalWarRebelElite.questString)
		totalWarRebelElite:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("rebel_restuss", 4)
		PlayerObject(pGhost):increaseFactionStanding("rebel", 50)
		
	end
	


	return conversationScreen
end

