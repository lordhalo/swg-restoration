local ObjectManager = require("managers.object.object_manager")

totalWarImperial = ScreenPlay:new {
	numberOfActs = 1,
	questString = "totalWarImperial",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("totalWarImperial", true)

function totalWarImperial:start()

end

--Setup

function totalWarImperial:getActivePlayerName()
	return self.questdata.activePlayerName
end

function totalWarImperial:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function totalWarImperial:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(totalWarImperial.states.quest.phasetwo, totalWarImperial.questString)
	player:removeScreenPlayState(totalWarImperial.states.quest.phaseone, totalWarImperial.questString)
	player:removeScreenPlayState(totalWarImperial.states.quest.intro, totalWarImperial.questString)

end

function totalWarImperial:notifyKilledHuntTarget(pPlayer, pVictim)
	local player = LuaCreatureObject(pPlayer)
	local huntTarget = readScreenPlayData(pPlayer, "totalWarImperial", "huntTarget")
	local targetCount = tonumber(readScreenPlayData(pPlayer, "totalWarImperial", "huntTargetCount"))
	local targetGoal = tonumber(readScreenPlayData(pPlayer, "totalWarImperial", "huntTargetGoal"))
	local targetList = HelperFuncs:splitString(huntTarget, ";")
	if (huntTarget == SceneObject(pVictim):getObjectName() or HelperFuncs:tableContainsValue(targetList, SceneObject(pVictim):getObjectName())) then	
		targetCount = targetCount + 1
		writeScreenPlayData(pPlayer, "totalWarImperial", "huntTargetCount", targetCount)
		CreatureObject(pPlayer):sendSystemMessage("Total War Progress: " .. readScreenPlayData(pPlayer, "totalWarImperial", "huntTargetCount") .. "/ 20")

		if (targetCount >= targetGoal) then
			player:setScreenPlayState(4, totalWarImperial.questString)
			return 1
		end
	end

	return 0
end


captain_roth_convo_handler = Object:new {
	
 }

function captain_roth_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "totalWarImperial")
			local questTurnin = creature:hasScreenPlayState(4, "totalWarImperial")
			local questComplete = creature:hasScreenPlayState(8, "totalWarImperial")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function captain_roth_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()
	local hasElite = player:hasScreenPlayState(2, "totalWarImperialElite")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, totalWarImperial.questString)

		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTarget", "rebel_trooper_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTargetGoal", 20)
		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarImperial", "notifyKilledHuntTarget", conversingPlayer)
	end

	if ( screenID == "accept_screen_elite" ) then
		player:setScreenPlayState(2, totalWarRebelElite.questString)

		writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTarget", "rebel_commando_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTargetGoal", 5)
		writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarImperialElite", "notifyKilledHuntTarget", conversingPlayer)
	end

	if ( screenID == "restart_screen" ) then
		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTarget", "rebel_trooper_restuss")
		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTargetGoal", 20)
		writeScreenPlayData(conversingPlayer, "totalWarImperial", "huntTargetCount", 0)
		createObserver(KILLEDCREATURE, "totalWarImperial", "notifyKilledHuntTarget", conversingPlayer)

		if(hasElite == true) then
			writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTarget", "rebel_commando_restuss")
			writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTargetGoal", 5)
			writeScreenPlayData(conversingPlayer, "totalWarImperialElite", "huntTargetCount", 0)
			createObserver(KILLEDCREATURE, "totalWarImperialElite", "notifyKilledHuntTarget", conversingPlayer)
		end

	
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, totalWarImperial.questString)
		totalWarImperial:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("imperial_resuss", 2)
		PlayerObject(pGhost):increaseFactionStanding("imperial", 50)

		
	end
	


	return conversationScreen
end

