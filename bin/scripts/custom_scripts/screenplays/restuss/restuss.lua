local ObjectManager = require("managers.object.object_manager")

restuss = ScreenPlay:new {
	numberOfActs = 1,
 	screenplayName = "restuss",
	planet = "rori",

	invasionMobs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5330, 80, 5440,  1, 0, "", "npc1" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5330, 80, 5435,  1, 0, "", "npc2" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5330, 80, 5430,  1, 0, "", "npc3" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5335, 80, 5440,  1, 0, "", "npc4" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5335, 80, 5435,  1, 0, "", "npc5" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5335, 80, 5430,  1, 0, "", "npc6" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5332.5, 80, 5435,  1, 0, "The BOSS", "npc7" },

	},

	randomMobs = {
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5304, 80, 5610,  1, 0, ""},	
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5304, 80, 5605,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5518, 80, 5620,  1, 0, ""},
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5518, 80, 5615,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5520, 80, 5617,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5541, 80, 5820,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5541, 80, 5825,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5459, 80, 5910,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5444, 80, 5916,  1, 0, ""},
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5449, 80, 5901,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5363, 80, 5448,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5350, 80, 5451,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5411, 79, 5463,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5292, 80, 5527,  1, 0, ""},
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5292, 80, 5530,  1, 0, ""},
	},

	normalInvasion = {
		{ "at_st", "at_xt", 5172, 80, 5746,  1, 0, "", "npc1" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5172, 80, 5742,  1, 0, "", "npc2" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5172, 80, 5750,  1, 0, "", "npc3" }, -- Group 1 ^

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5600,  1, 0, "", "npc4" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5598,  1, 0, "", "npc5" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5596,  1, 0, "", "npc6" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5594,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5422, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5426, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5430, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5434, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5438, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5442, 80, 5700,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5558, 80, 5708,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5558, 80, 5704,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5505, 80, 5528,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5480, 80, 5524,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5325, 80, 5535,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5170, 80, 5546,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5166, 80, 5546,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5162, 80, 5546,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5216, 80, 5648,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5218, 80, 5648,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5216, 80, 5652,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5218, 80, 5652,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5325, 80, 5535,  1, 0, "", "patrolnpc" },
	},

	wildInvasion = {
		{ "at_st", "at_xt", 5172, 80, 5746,  1, 0, "", "npc1" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5172, 80, 5742,  1, 0, "", "npc2" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5172, 80, 5750,  1, 0, "", "npc3" }, -- Group 1 ^

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5600,  1, 0, "", "npc4" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5598,  1, 0, "", "npc5" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5596,  1, 0, "", "npc6" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5232, 80, 5594,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5422, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5426, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5430, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5434, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5438, 80, 5700,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5442, 80, 5700,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5558, 80, 5708,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5558, 80, 5704,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5505, 80, 5528,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5480, 80, 5524,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5325, 80, 5535,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5170, 80, 5546,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5166, 80, 5546,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5162, 80, 5546,  1, 0, "", "npc7" },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5216, 80, 5648,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5218, 80, 5648,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5216, 80, 5652,  1, 0, "", "npc7" },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5218, 80, 5652,  1, 0, "", "npc7" },
	},

	insaneInvasion = {
		{ "at_st", "at_xt", 5172, 80, 5746,  1, 0, "", "npc1" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5172, 80, 5742,  1, 0, "", "npc2" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5172, 80, 5750,  1, 0, "", "npc3" }, -- Group 1 ^

		{ "black_sun_assassin", "death_watch_ghost_new", 5232, 80, 5600,  1, 0, "", "npc4" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5232, 80, 5598,  1, 0, "", "npc5" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5232, 80, 5596,  1, 0, "", "npc6" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5232, 80, 5594,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5422, 80, 5700,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5426, 80, 5700,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5430, 80, 5700,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5434, 80, 5700,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5438, 80, 5700,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5442, 80, 5700,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5558, 80, 5708,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5558, 80, 5704,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5505, 80, 5528,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5480, 80, 5524,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5325, 80, 5535,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5170, 80, 5546,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5166, 80, 5546,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5162, 80, 5546,  1, 0, "", "npc7" },

		{ "black_sun_assassin", "death_watch_ghost_new", 5216, 80, 5648,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5218, 80, 5648,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5216, 80, 5652,  1, 0, "", "npc7" },
		{ "black_sun_assassin", "death_watch_ghost_new", 5218, 80, 5652,  1, 0, "", "npc7" },
	},
}
  
registerScreenPlay("restuss", true)
  
function restuss:start()

	local controllingFaction = getControllingFaction(self.planet)

    	self:spawnActiveAreas()
	self:spawnSceneObjects()
	self:spawnQuestGivers()
	self:spawnDogFight()
	self:spawnAmbushPoint()
	--self:spawnAntenna()
	--self:spawnSensorArray()
	restussHelper:spawnNorthComm()
	restussHelper:spawnEastComm()
	restussHelper:spawnSouthComm()
	restussHelper:spawnWestComm()

	restussHelper:spawnImperialAntenna()
	restussHelper:spawnRebelAntenna()

	--createEvent(25000, "restuss", "setUpInvasionEvent", nil, "")

	createEvent(25000, "restuss", "totalWarEvent", nil, "")

	createEvent(25000, "restuss", "spawnRandomMobs", nil, "")

	local random = math.random(100)
			
	if random >= 50 then
		self:spawnImperials()
	elseif random < 50 then
		self:spawnRebels()
	else
		self:spawnImperials()
	end

end
  
function restuss:spawnActiveAreas()

	local pActiveArea = spawnActiveArea("rori", "object/active_area.iff", 5330, 80, 5685, 384, 0)
    
	if (pActiveArea ~= nil) then
	        createObserver(ENTEREDAREA, "restuss", "notifySpawnArea", pActiveArea)
	        createObserver(EXITEDAREA, "restuss", "notifySpawnAreaLeave", pActiveArea)
	    end
end

function restuss:spawnAmbushPoint()

	local random = math.random(100)
			
	if random >= 75 then
		local pAmbushOne = spawnActiveArea("rori", "object/active_area.iff", 5322, 80, 5745, 15, 0)
	    
		if (pAmbushOne ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushOne", pAmbushOne)
		    end

		local pAmbushTwo = spawnActiveArea("rori", "object/active_area.iff", 4148, 80, 5548, 15, 0)
	    
		if (pAmbushTwo ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushTwo", pAmbushTwo)
		    end

		local pAmbushThree = spawnActiveArea("rori", "object/active_area.iff", 5190, 80, 5780, 15, 0)
	    
		if (pAmbushThree ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushThree", pAmbushThree)
		    end

		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushOne", pAmbushOne, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushTwo", pAmbushTwo, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushThree", pAmbushThree, "")
	elseif random > 35 then
		local pAmbushFour = spawnActiveArea("rori", "object/active_area.iff", 5488, 80, 5858, 15, 0)
	    
		if (pAmbushFour ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushFour", pAmbushFour)
		    end

		local pAmbushFive = spawnActiveArea("rori", "object/active_area.iff", 5400, 80, 5500, 15, 0)
	    
		if (pAmbushFive ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushFive", pAmbushFive)
		    end

		local pAmbushSix = spawnActiveArea("rori", "object/active_area.iff", 5610, 80, 5690, 25, 0)
	    
		if (pAmbushSix ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushSix", pAmbushSix)
		    end

		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushFour", pAmbushFour, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushFive", pAmbushFive, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushSix", pAmbushSix, "")
	else
		local pAmbushSeven = spawnActiveArea("rori", "object/active_area.iff", 5300, 80, 5590, 15, 0)
	    
		if (pAmbushSeven ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushSeven", pAmbushSeven)
		    end

		local pAmbushEight = spawnActiveArea("rori", "object/active_area.iff", 5010, 80, 5700, 25, 0)
	    
		if (pAmbushEight ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushEight", pAmbushEight)
		    end

		local pAmbushNine = spawnActiveArea("rori", "object/active_area.iff", 5532, 80, 5600, 15, 0)
	    
		if (pAmbushNine ~= nil) then
			createObserver(ENTEREDAREA, "restussAmbushHelper", "enterAmbushNine", pAmbushNine)
		    end

		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushSeven", pAmbushSeven, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushEight", pAmbushEight, "")
		createEvent(12000, "restussAmbushHelper", "deSpawnAmbushNine", pAmbushNine, "")
	end

end

function restuss:spawnQuestGivers()

	--Imperials
	spawnMobile("rori", "imperial_vendor", 0, 5936, 80, 5639,  1, 0)
	spawnMobile("rori", "lieutenant_gregor", 0, 5947, 84, 5614,  1, 0)
	spawnMobile("rori", "captain_okto", 0, 5955, 84, 5585,  1, 0)
	spawnMobile("rori", "captain_exov", 0, 5929, 84, 5610,  1, 0)
	spawnMobile("rori", "captain_roth", 0, 5929, 88, 5645,  1, 0)
	spawnMobile("rori", "imperial_recruiter", 0, 5924, 84, 5604,  1, 0)
	spawnMobile("rori", "imperial_frs_recruiter", 0, 5924, 84, 5619,  1, 0)

	--Rebels
	spawnMobile("rori", "rebel_vendor", 0, 4775, 83, 5965,  -179, 0)
	spawnMobile("rori", "lt_olvog", 0, 4812, 87, 5962,  123, 0)
	spawnMobile("rori", "captain_voldez", 0, 4801, 83, 5924,  5, 0)
	spawnMobile("rori", "captain_klork", 0, 4780, 83, 5940,  -2, 0)
	spawnMobile("rori", "captain_redding", 0, 4781, 83, 5952,  90, 0)
	spawnMobile("rori", "rebel_recruiter", 0, 4822, 83, 5936,  1, 0)
	spawnMobile("rori", "rebel_frs_recruiter", 0, 4790, 83, 5965,  1, 0)

end

function restuss:spawnDogFight()
	spawnSceneObject("rori", "object/static/particle/particle_distant_ships_dogfight_ti_vs_aw.iff", 5330, 80, 5685, 0, 0, 0, 0, 0)	
	spawnSceneObject("rori", "object/static/particle/particle_distant_ships_dogfight_t_vs_x_3.iff", 5330, 80, 5685, 0, 0, 0, 0, 0)	
	spawnSceneObject("rori", "object/static/particle/particle_distant_ships_dogfight_ti_vs_aw_2.iff", 5330, 80, 5585, 0, 0, 0, 0, 0)	
	spawnSceneObject("rori", "object/static/particle/particle_distant_ships_dogfight_tb_vs_bw.iff", 5430, 80, 5685, 0, 0, 0, 0, 0)
end

function restuss:spawnSensorArray()
	-- X, Y, Z, Cell
	spawnSceneObject("rori", "object/tangible/quest/restuss_comm_array.iff", 5169, 80, 5681, 0, 0, 0, 0, 0)
	spawnSceneObject("rori", "object/tangible/quest/restuss_comm_array.iff", 5255, 80, 5533, 0, 0, 0, 0, 0)
	spawnSceneObject("rori", "object/tangible/quest/restuss_comm_array.iff", 5435, 80, 5620, 0, 0, 0, 0, 0)
	spawnSceneObject("rori", "object/tangible/quest/restuss_comm_array.iff", 5361, 80, 5763, 0, 0, 0, 0, 0)

end

function restuss:flipRebel()

	writeData("restussManager:rebelOwned",1)
	writeData("restussManager:imperialOwned",0)
	restussHelper:spawnRebelAntenna()
	return 0
end

function restuss:flipImperial()

	writeData("restussManager:imperialOwned",1)
	writeData("restussManager:rebelOwned",0)
	restussHelper:spawnImperialAntenna()
	return 0
end

function restuss:respawnImpAntenna()

	writeData("restussManager:impAntDest",0)
	restussHelper:spawnImperialAntenna()
	return 0
end

function restuss:respawnRebAntenna()

	writeData("restussManager:rebAntDest",0)
	restussHelper:spawnRebelAntenna()
	return 0
end


function restuss:spawnImperials()
	
	writeData("restussManager:imperialOwned",1)
	local difficulty = 3
		
	for i = 1, #restussHelper.cityControlMobs do
		restussHelper:spawnCityControlMobs(i, difficulty)
	end


end

function restuss:spawnRebels()

	writeData("restussManager:rebelOwned",1)
	local difficulty = 3
		
	for i = 1, #restussHelper.cityControlMobs do
		restussHelper:spawnCityControlMobs(i, difficulty)
	end


end

 
--checks if player enters the zone, and what to do with them.
function restuss:notifySpawnArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent() or player:isSpeeder()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			createEvent(1, "restuss", "handlerestussZone", pMovingObject)
			CreatureObject(pMovingObject):setFactionStatus(2)
			player:sendSystemMessage("You have entered a battlefield!")
		else
			player:sendSystemMessage("You must be Rebel or Imperial to enter the battlefield!")
			player:teleport(5305, 80, 6188, 0)
		end

		--createObserver(OBJECTDESTRUCTION, "restuss", "killedPlayer", pMovingObject)
		return 0
	end)
end

function restuss:killedPlayer(pTarget, pKiller)
	local player = LuaCreatureObject(pKiller)

	if (player:isAiAgent()) then
		return 0
	end

	local hasImpSkill = player:hasSkill("imp_rank_novice")
	local hasRebSkill = player:hasSkill("reb_rank_novice")
	

	if (player:isRebel() == true) then
		ObjectManager.withCreatureAndPlayerObject(pKiller, function(player, playerObject)
		playerObject:increaseFactionStanding("rebel_overt", 15)
		end)
		if(hasRebSkill == true) then
			player:awardExperience("gcw_xp", 100)
		end
	end

	if (player:isImperial() == true) then
		ObjectManager.withCreatureAndPlayerObject(pKiller, function(player, playerObject)
		playerObject:increaseFactionStanding("imperial_overt", 15)
		end)
		if(hasImpSkill == true) then
			player:awardExperience("gcw_xp", 100)
		end
	end

	return 0
end

--Handles the setting of factional status
function restuss:handlerestussZone(pPlayer)
	ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		deleteData(player:getObjectID() .. ":changingFactionStatus")
		if (playerObject:isCovert() or playerObject:isOnLeave()) then
			playerObject:setFactionStatus(2)
		end
	end)

end

--Simply sends a system message
function restuss:notifySpawnAreaLeave(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			player:sendSystemMessage("You have left the battlefield!")
		end
		return 0
	end)
end

function restuss:spawnSceneObjects()

	local pTurret = spawnSceneObject("tatooine", "object/static/destructible/destructible_cave_wall_damprock.iff", -757, 5, -4461, 0, 0, 0, 0, 0)	

	--local pDebris = spawnSceneObject("talus", "object/battlefield_marker/battlefield_marker_384m.iff", -2445, 38, 3696, 0, 0, 0, 0, 0)			
	--if (pDebris ~= nil) then
	--	createObserver(OBJECTDESTRUCTION, "restuss", "notifyDebrisDestroyed", pDebris)
	--end

end

function restuss:notifyDebrisDestroyed(pDebris, pPlayer)
	if (pPlayer == nil or pDebris == nil) then
		return 0
	end

	SceneObject(pDebris):destroyObjectFromWorld()

	CreatureObject(pPlayer):clearCombatState(1)
	return 0
end

function restuss:setUpInvasionEvent()

	local random = math.random(100)
	local randomSpawn = math.random(100)

	if (randomSpawn >= 50) then
		createEvent(480000, "restuss", "setUpInvasionEvent", nil, "")
		return 0
	end

	local difficulty = 1
		
	for i = 1, #self.invasionMobs do
		self:spawnInvasionEvent(i, difficulty)
	end

	return 0

end

function restuss:spawnInvasionEvent(num, difficulty)

	local mobsTable = self.invasionMobs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""
	
	if (readData("restussManager:imperialOwned") == 1)then
		npcTemplate = mobTable[2] -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[8]
	elseif (readData("restussManager:rebelOwned") == 1) then
		npcTemplate = mobTable[1]  -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[9]
	end

	local scaling = ""
	if difficulty > 1 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])
	writeStringData(SceneObject(pNpc):getObjectID() .. ":name", mobTable[9])

	AiAgent(pNpc):setAiTemplate("manualescortwalk")
	AiAgent(pNpc):setFollowState(4)

	createEvent(60 * 1000, "restuss", "npcPatrol", pNpc, "")

	--createObserver(DESTINATIONREACHED, "restuss", "npcPatrol", pNpc)

	return 0

end

function restuss:npcPatrol(pMobile)
	if (pMobile == nil) then
		return
	end

	local nextLoc

	local name = readStringData(SceneObject(pMobile):getObjectID() .. ":name")

	if (name == "npc1") then
		nextLoc = { 5330.0, 80, 5620, 0 }

	end

	if (name == "npc2") then
		nextLoc = { 5330.0, 80, 5615, 0 }

	end

	if (name == "npc3") then
		nextLoc = { 5330.0, 80, 5610, 0 }

	end

	if (name == "npc4") then
		nextLoc = { 5335.0, 80, 5620, 0 }

	end

	if (name == "npc5") then
		nextLoc = { 5335.0, 80, 5615, 0 }

	end

	if (name == "npc6") then
		nextLoc = { 5335.0, 80, 5610, 0 }

	end

	if (name == "npc7") then
		nextLoc = { 5332.5, 80, 5615, 0 }

	end

	AiAgent(pMobile):stopWaiting()
	AiAgent(pMobile):setWait(0)
	AiAgent(pMobile):setNextPosition(nextLoc[1], nextLoc[2], nextLoc[3], nextLoc[4])
	AiAgent(pMobile):executeBehavior()
end

function restuss:totalWarEvent()

	local difficulty = 1

	local random = math.random(100)
	local randomSpawn = math.random(100)

	if (randomSpawn >= 50) then
		createEvent(480000, "restuss", "totalWarEvent", nil, "")
		return 0
	end

	if random >= 75 then

		createEvent(25000, "restuss", "normalInvasionBoss", nil, "")
		createEvent(1800000, "restuss", "totalWarEvent", nil, "")
		for i = 1, #self.normalInvasion do
			self:spawnTotalWar(i, difficulty, 1)
		end
	elseif random < 50 then

		createEvent(25000, "restuss", "normalInvasionBoss", nil, "")
		createEvent(1800000, "restuss", "totalWarEvent", nil, "")
		for i = 1, #self.wildInvasion do
			self:spawnTotalWar(i, difficulty, 2)
		end
	else
		createEvent(25000, "restuss", "normalInvasionBoss", nil, "")
		createEvent(1800000, "restuss", "totalWarEvent", nil, "")
		for i = 1, #self.insaneInvasion do
			self:spawnTotalWar(i, difficulty, 3)
		end
	end

	return 0

end

function restuss:spawnRandomMobs()

	local difficulty = 1

	local random = math.random(100)
	local randomSpawn = math.random(100)

	if (randomSpawn >= 35) then
		createEvent(900000, "restuss", "spawnRandomMobs", nil, "")
		return 0
	end

	for i = 1, #self.randomMobs do
		self:spawnMobsForRandom(i, difficulty, 1)
	end

	createEvent(900000, "restuss", "spawnRandomMobs", nil, "")

	return 0

end

function restuss:spawnTotalWar(num, difficulty, roll)

	local mobsTable

	if (roll == 1) then
		mobsTable = self.normalInvasion
	elseif (roll == 2) then
		mobsTable = self.wildInvasion
	else 
		mobsTable = self.insaneInvasion
	end

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""
	
	if (readData("restussManager:imperialOwned") == 1)then
		npcTemplate = mobTable[2] -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[8]
	elseif (readData("restussManager:rebelOwned") == 1) then
		npcTemplate = mobTable[1]  -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(100)
	if random >= 50 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])
	--writeStringData(SceneObject(pNpc):getObjectID() .. ":name", mobTable[9])

	--AiAgent(pNpc):setAiTemplate("manualescortwalk")
	--AiAgent(pNpc):setFollowState(4)

	createEvent(480000, "restuss", "despawnWar", pNpc, "")

	return 0

end

function restuss:spawnMobsForRandom(num, difficulty, roll)

	local mobsTable = self.randomMobs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""
	
	if (readData("restussManager:imperialOwned") == 1)then
		npcTemplate = mobTable[2] -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[8]
	elseif (readData("restussManager:rebelOwned") == 1) then
		npcTemplate = mobTable[1]  -- These must be reversed from standard operation, The current owner of the city gets oposite faction spawns to do boss quest
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(100)
	if random >= 50 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])
	--writeStringData(SceneObject(pNpc):getObjectID() .. ":name", mobTable[9])

	--AiAgent(pNpc):setAiTemplate("manualescortwalk")
	--AiAgent(pNpc):setFollowState(4)

	createEvent(480000, "restuss", "despawnWar", pNpc, "")

	return 0

end

function restuss:normalInvasionBoss(num, difficulty)

	local pNpc = nil
	
	if (readData("restussManager:imperialOwned") == 1)then
		pNpc = spawnMobile("rori", "rebel_commando_restuss", 0, 5234, 80, 5750, 0, 0)
	elseif (readData("restussManager:rebelOwned") == 1) then
		pNpc = spawnMobile("rori", "stormtrooper", 0, 5234, 80, 5750, 0, 0)
	end

	--writeStringData(SceneObject(pNpc):getObjectID() .. ":name", mobTable[9])

	AiAgent(pNpc):setAiTemplate("manualescortwalk")
	AiAgent(pNpc):setFollowState(4)

	createEvent(60 * 1000, "restuss", "bossPatrol", pNpc, "")

	createObserver(DESTINATIONREACHED, "restuss", "bossReachedPoint", pNpc)
	createObserver(DEFENDERADDED, "restuss", "bossAttacked", pNpc)

	createEvent(360 * 1000, "restuss", "despawnWar", pNpc, "")

	return 0

end

function restuss:bossPatrol(pMobile)
	if (pMobile == nil) then
		return
	end

	local curLoc = readData("bossNpc:currentLoc")
	local nextLoc

	if (curLoc == 2) then
		nextLoc = { 5331.0, 80, 5626.0, 0 }
	elseif (curLoc == 1) then
		nextLoc = { 5282.0, 80, 5690.0, 0 }
	else 
		nextLoc = { 5282.0, 80, 5743.0, 0 }
	end


	AiAgent(pMobile):stopWaiting()
	AiAgent(pMobile):setWait(0)
	AiAgent(pMobile):setNextPosition(nextLoc[1], nextLoc[2], nextLoc[3], nextLoc[4])
	AiAgent(pMobile):executeBehavior()

	return 0
end

function restuss:bossReachedPoint(pMobile)
	if (pMobile == nil) then
		return
	end

	local curLoc = readData("bossNpc:currentLoc")

	if (curLoc == 2) then
		writeData("bossNpc:currentLoc", 3)
	elseif (curLoc == 1) then
		writeData("bossNpc:currentLoc", 2)
	else 
		writeData("bossNpc:currentLoc", 1)
	end

	createEvent(60 * 1000, "restuss", "bossPatrol", pMobile, "")


	return 0
end

function restuss:bossAttacked(pMobile, pAttacker)
	if (pMobile == nil or pAttacker == nil) then
		return
	end

	CreatureObject(pMobile):engageCombat(pAttacker)


	return 0
end

function restuss:despawnWar(pMobile)
	if (pMobile == nil) then
		return
	end

	if pMobile ~= nil then
		SceneObject(pMobile):destroyObjectFromWorld()
	end

	return 0
end


function restuss:totalWarReachedPoint(pMobile)
	if (pMobile == nil) then
		return
	end

	local curLoc = readData("bossNpc:currentLoc")

	if (curLoc == 2) then
		writeData("bossNpc:currentLoc", 3)
	elseif (curLoc == 1) then
		writeData("bossNpc:currentLoc", 2)
	else 
		writeData("bossNpc:currentLoc", 1)
	end

	createObserver(OBJECTDESTRUCTION, "restuss", "killedPlayer", pMovingObject)
	createEvent(60 * 1000, "restuss", "despawnTotalWar", pMobile, "")


	return 0
end


function restuss:despawnTotalWar(pMobile)
	if (pMobile == nil) then
		return
	end

	if pMobile ~= nil then
		SceneObject(pMobile):destroyObjectFromWorld()
	end

	return 0
end


