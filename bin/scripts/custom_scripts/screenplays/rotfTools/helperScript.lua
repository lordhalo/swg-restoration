local ObjectManager = require("managers.object.object_manager")

helperScript = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "helperScript",

}

registerScreenPlay("helperScript", true)

--Sets up hunt data to player
function helperScript:setupHunt(pTarget, questString, goal)
	if (pTarget == nil) then
		return
	end
	
	--Clean the data first
	helperScript:removeHunt(pTarget, questString)
	
	writeScreenPlayData(pTarget, questString, "huntTargetGoal", goal)
	writeScreenPlayData(pTarget, questString, "huntTargetCount", 0)
end

--Removes hunt data from player
function helperScript:removeHunt(pTarget, questString)
	if (pTarget == nil) then
		return
	end
	
	deleteScreenPlayData(pTarget, questString, "huntTargetCount")
	deleteScreenPlayData(pTarget, questString, "huntTargetGoal")
end

--Assigning a waypoint to the target
function helperScript:addWaypoint(pTarget, planet, name, x, y, color, questString)
	local objectID = CreatureObject(pTarget):getObjectID()
	local pGhost = CreatureObject(pTarget):getPlayerObject()
	
	if (pTarget == nil or pGhost == nil) then
		return
	end
	
	--Check for color, and assign it.
	if (color == "blue") then
		color = WAYPOINTBLUE
	elseif (color == "green") then
		color = WAYPOINTGREEN
	elseif (color == "yellow") then
		color = WAYPOINTYELLOW
	elseif (color == "purple") then
		color = WAYPOINTPURPLE
	elseif (color == "white") then
		color = WAYPOINTWHITE
	elseif (color == "orange") then
		color = WAYPOINTORANGE
	else
		color = WAYPOINTBLUE
	end
	local wayPoint = PlayerObject(pGhost):addWaypoint(planet, name, "", x, y, color, true, true, 0)
	setQuestStatus(objectID .. questString, wayPoint)
end

--Removing a waypoint from the target
function helperScript:removeWaypoint(pTarget, questString)
	local objectID = CreatureObject(pTarget):getObjectID()
	local pGhost = CreatureObject(pTarget):getPlayerObject()
	
	if (pTarget == nil or pGhost == nil) then
		return
	end

	local waypointID = tonumber(getQuestStatus(objectID .. questString))
	PlayerObject(pGhost):removeWaypoint(waypointID, true)
	removeQuestStatus(objectID .. questString)
end

--Removing a waypoint from the target
function helperScript:removeQuestWaypoint(pTarget)
	local objectID = CreatureObject(pTarget):getObjectID()
	local pGhost = CreatureObject(pTarget):getPlayerObject()
	
	if (pTarget == nil or pGhost == nil) then
		return
	end

	local waypointID = tonumber(getQuestStatus(objectID .. ":currentQuestWaypointID"))
	PlayerObject(pGhost):removeWaypoint(waypointID, true)
	removeQuestStatus(objectID .. ":currentQuestWaypointID")
end

--Remove an object
function helperScript:removeObject(pTarget, questString, objectID)
	if (pTarget == nil) then
		return
	end
	local targetID = SceneObject(pTarget):getObjectID()

	local containerID = readData(targetID .. ":" .. questString .. ":" .. objectID)
	local pContainer = getSceneObject(containerID)

	if (pContainer ~= nil) then
		SceneObject(pContainer):destroyObjectFromWorld()
	end

	deleteData(targetID .. ":" .. questString .. ":" .. objectID)
end

--Spawn an object at the target's location
function helperScript:spawnObjectOnTarget(pTarget, questString, objectID, name, objectTemplate)
	if (pTarget == nil) then
		return
	end
	
	local playerID = CreatureObject(pTarget):getObjectID()
	local cords = { x = math.floor(SceneObject(pTarget):getWorldPositionX()), z = math.floor(SceneObject(pTarget):getWorldPositionZ()), y = math.floor(SceneObject(pTarget):getWorldPositionY()), r = math.rad(180), cell = 0}
	local pContainer = spawnSceneObject(SceneObject(pTarget):getZoneName(), objectTemplate, cords.x, cords.z, cords.y, cords.cell, cords.r)
	
	CreatureObject(pContainer):setCustomObjectName(name)
	writeData(playerID .. ":" .. questString .. ":" .. objectID, SceneObject(pContainer):getObjectID())
end

--Send a mail to target
function helperScript:mailTarget(pTarget, sender, subject, body)
	if (pTarget == nil) then
		return
	end
	
	sendMail(sender, subject, body, CreatureObject(pTarget):getFirstName())
end

--Removes object without data attached.
function helperScript:deleteObject(pObject)
	if (pObject == nil) then
		return
	end
	SceneObject(pObject):destroyObjectFromWorld()
end

--This function handles the spawning, and assigns an observer to each target.
function helperScript:handleKillTargets(pPlayer, questString, targetTemplate, planet, respawn, x, z, y, cell, rand)

	local targetGoal = tonumber(readScreenPlayData(pPlayer, questString, "huntTargetGoal"))
	local amount = 0
	for amount = targetGoal, 1,-1
		do spawnMobile(planet, targetTemplate, respawn, x + math.random(rand), z, y + math.random(rand), math.random(rand), cell) 
	end
end

function helperScript:handleReward(pPlayer, questType)
	local pGhost = CreatureObject(pPlayer):getPlayerObject()
	
	if (questType == "rafa") then
		if (PlayerObject(pGhost):hasBadge(143)) then
			return
		else
			PlayerObject(pGhost):awardBadge(143)
			awardSkill(pPlayer, "custom_title_brawler_recruit")
			helperScript:addWaypoint(pPlayer, "tatooine", "Rafa sent you to see Jam-gi", 3171, -4585, "purple", ":currentQuestWaypointID")
			return
		end
	elseif (questType == "jamgi") then
		if (PlayerObject(pGhost):hasBadge(145)) then
			return
		else
			PlayerObject(pGhost):awardBadge(145)
			awardSkill(pPlayer, "custom_title_brawler_cadet")
			helperScript:addWaypoint(pPlayer, "tatooine", "Jam-Gi sent you to see Trev Alask", 3184, -4587, "orange", ":currentQuestWaypointID")
			return
		end
	end
end


