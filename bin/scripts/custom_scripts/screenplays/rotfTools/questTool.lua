--This is a tool for quest lines.
questTool = {
	promptBody = {
		"Enter the name of the quest giver: rafa, jamgi, trev",
		"Enter state # for this quest giver. If you don't know type \"help\"."
	},

	promptTitle = { "Quest Giver", "Quest Giver", "Quest State" },

}

function questTool:suiQuestToolMainCallback(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed) then
		return
	end

	local playerID = SceneObject(pPlayer):getObjectID()

	if (args == "set") then
		writeData(playerID .. ":questTool:setupStep", 1)
		self:showSetupUI(pPlayer)
		return
	end

	self:showMainUI(pPlayer)
end

function questTool:showMainUI(pPlayer)
	local sui = SuiInputBox.new("questTool", "suiQuestToolMainCallback")

	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())

	local suiBody = "Enter \"set\" to set specific quest milestones."
	sui.setTitle("Information")
	sui.setPrompt(suiBody)
	sui.sendTo(pPlayer)
end

function questTool:showSetupUI(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()
	local curStep = readData(playerID .. ":questTool:setupStep")

	if (curStep == 0) then
		self:showMainUI(pPlayer)
		return
	end

	local sui = SuiInputBox.new("questTool", "suiQuestToolSetupCallback")

	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())

	sui.setTitle(self.promptTitle[curStep])
	sui.setPrompt(self.promptBody[curStep])

	sui.sendTo(pPlayer)
end

function questTool:suiQuestToolSetupCallback(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed) then
		return
	end

	local playerID = SceneObject(pPlayer):getObjectID()
	local curStep = readData(playerID .. ":questTool:setupStep")
	local qGiver = readData(playerID .. ":questTool:quest")

	if (curStep == 1) then
		if (args == "rafa") then
			writeData(playerID .. ":questTool:setupStep", 2)
			writeData(playerID .. ":questTool:quest", 1)
		elseif (args == "jamgi") then
			writeData(playerID .. ":questTool:setupStep", 2)
			writeData(playerID .. ":questTool:quest", 2)
		elseif (args == "trev" or args == "trev alask") then
			writeData(playerID .. ":questTool:setupStep", 2)
			writeData(playerID .. ":questTool:quest", 3)
		else
			CreatureObject(pPlayer):sendSystemMessage("Quest Tool: You must select the quest giver, by: First name only, Lower case only, and no special characters included.")
		end
	elseif (curStep == 2) then
		if (qGiver == 1) then
			if (args == "help") then
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: You MUST select between 0, 1, 2, and 4. 0 = not accepted, 1 = accepted quest, 2 = killed rats, 4 = completed training and quest.")
			else
				local stateOld = CreatureObject(pPlayer):getScreenPlayState("rafaQuest")
				CreatureObject(pPlayer):removeScreenPlayState(stateOld, "rafaQuest")
				CreatureObject(pPlayer):setScreenPlayState(args, "rafaQuest")
				local state = CreatureObject(pPlayer):getScreenPlayState("rafaQuest")
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: Your state has been set to: " .. state)
			end
		elseif (qGiver == 2) then
			if (args == "help") then
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: You must select between 1, 4, and 32. 1 = accepted quest, 4 = collected credits from lesk, 32 = completed training and quest.")
			else
				local stateOld = CreatureObject(pPlayer):getScreenPlayState("jamgiQuest")
				CreatureObject(pPlayer):removeScreenPlayState(stateJamgi, "jamgiQuest")
				local stateLesk = CreatureObject(pPlayer):getScreenPlayState("lesk")
				CreatureObject(pPlayer):removeScreenPlayState(stateLesk, "lesk")
				local stateLiar = CreatureObject(pPlayer):getScreenPlayState("liar")
				CreatureObject(pPlayer):removeScreenPlayState(stateLiar, "liar")
				CreatureObject(pPlayer):setScreenPlayState(args, "jamgiQuest")
				local statejamgi = CreatureObject(pPlayer):getScreenPlayState("jamgiQuest")
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: Your state has been set to: " .. statejamgi)
			end
		elseif (qGiver == 3) then
			if (args == "help") then
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: You must select between 0, 1. 0 = not accepted quest yet, 1 = accepted quest.")
			else
				local stateTrev = CreatureObject(pPlayer):getScreenPlayState("trevQuest")
				CreatureObject(pPlayer):removeScreenPlayState(stateTrev, "trevQuest")
				local statePatrol = CreatureObject(pPlayer):getScreenPlayState("patrolPoint")
				CreatureObject(pPlayer):removeScreenPlayState(statePatrol, "patrolPoint")
				local stateCriminal = CreatureObject(pPlayer):getScreenPlayState("killedCriminal")
				CreatureObject(pPlayer):removeScreenPlayState(stateCriminal, "killedCriminal")
				CreatureObject(pPlayer):setScreenPlayState(args, "trevQuest")
				local stateTrev = CreatureObject(pPlayer):getScreenPlayState("trevQuest")
				CreatureObject(pPlayer):sendSystemMessage("Quest Tool: Your state has been set to: " .. stateTrev)
			end
		end
	end
	self:showSetupUI(pPlayer)
end