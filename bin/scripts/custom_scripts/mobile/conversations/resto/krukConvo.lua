
--Made with Skyyyr's Wrench Tool 

krukConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "kruk_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "It's interesting you've come to me at a time of need. My name is Commander Kruk, I run security operations here on Talus. Would you be interested in helping us in an covert operation?",
	stopConversation = "false",
	options = {
		 {"Covert operation? I'm in! What're the details?","agree_mission"},
		 {"What kind of operation?","inquire_mission"},
		 {"No thanks.","deny"}
	 }
 }
krukConvoTemplate:addScreen(first);

deny = ConvoScreen:new {
	id = "deny",
	leftDialog = "",
	customDialogText = "Until next time then.",
	stopConversation = "true",
	options = {
	 }
 }
krukConvoTemplate:addScreen(deny);

agree_mission = ConvoScreen:new {
	id = "agree_mission",
	leftDialog = "",
	customDialogText = "Great news! I believe you're a perfect fit for this mission because... Well I haven't seen you before and that makes you valuable.",
	stopConversation = "false",
	options = {
		 {"Tell me more.","continue_1"}
	 }
 }
krukConvoTemplate:addScreen(agree_mission);

continue_1 = ConvoScreen:new {
	id = "continue_1",
	leftDialog = "",
	customDialogText = "Right. So we have a swoop gang problem and recently they've spread out their territories to Tatooine, Endor, and of course here on Talus. We need someone to infiltrate their gang, and I believe you are it!",
	stopConversation = "false",
	options = {
		 {"I like the sound of this. I'm in!","accept_mission"},
		 {"Sounds too dangerous for me.","deny"}
	 }
 }
krukConvoTemplate:addScreen(continue_1);

accept_mission = ConvoScreen:new {
	id = "accept_mission",
	leftDialog = "",
	customDialogText = "You are a real go getter! Alright so they usually recruit out of the cantinas on Tatooine. Our intel tells us one of their recruiters goes by the name 'Juri Batterfist'. I'll send you more info by holo-mail. Alright Agent, report back when you get accepted into the gang.",
	stopConversation = "true",
	options = {
	 }
 }
krukConvoTemplate:addScreen(accept_mission);

inquire_mission = ConvoScreen:new {
	id = "inquire_mission",
	leftDialog = "",
	customDialogText = "The mission is simple - Infiltrate the 'Blood Moon' swoop gang and report back with the details. We want to take them down from the inside. That's the only way. Are you in?",
	stopConversation = "false",
	options = {
		 {"I can get behind that. Continue commander.","agree_mission"},
		 {"Sounds too dangerous, sorry.","deny"}
	 }
 }
krukConvoTemplate:addScreen(inquire_mission);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "I've already sent you the details for your mission agent! Here I'll send them again. Just be careful out there.",
	stopConversation = "true",
	options = {
	 }
 }
krukConvoTemplate:addScreen(already);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "You've taken the gang down. I couldn't be happier! Talus is finally free!",
	stopConversation = "true",
	options = {
	 }
 }
krukConvoTemplate:addScreen(completed);


addConversationTemplate("krukConvoTemplate", krukConvoTemplate);