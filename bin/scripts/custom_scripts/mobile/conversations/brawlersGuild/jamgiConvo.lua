
--Made with Skyyyr's Wrench Tool 

jamgiConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "jamgi_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "I see you're ready for some more good deeds for the guild. Are you ready to begin?",
	stopConversation = "false",
	options = {
		 {"Yes, what's the task?","next"},
		 {"I need some time.","deny"}
	}
 }
jamgiConvoTemplate:addScreen(first);

deny = ConvoScreen:new {
	id = "deny",
	leftDialog = "",
	customDialogText = "Come back when you're ready to begin.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(deny);

next = ConvoScreen:new {
	id = "next",
	leftDialog = "",
	customDialogText = "I need you to go meet up with someone and collect credits from them. You can handle that?",
	stopConversation = "false",
	options = {
		 {"Sounds easy, give me the details.","accepted_quest_collect"},
		 {"That doesn't sound like a good deed.","deny_collect"}
	}
 }
jamgiConvoTemplate:addScreen(next);

deny_collect = ConvoScreen:new {
	id = "deny_collect",
	leftDialog = "",
	customDialogText = "You wont get far in the guild with that kind of attitude.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(deny_collect);

accepted_quest_collect = ConvoScreen:new {
	id = "accepted_quest_collect",
	leftDialog = "",
	customDialogText = "Great, so his name is Lesk. Here's the location to the meet up. Come back when you have my credits.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(accepted_quest_collect);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "I said go to the meet up and get the credits from Lesk.",
	stopConversation = "true",
	options = {
	 }
 }
jamgiConvoTemplate:addScreen(already);

not_ready = ConvoScreen:new {
	id = "not_ready",
	leftDialog = "",
	customDialogText = "You should go see Rafa Devia, you have to earn your way around here.",
	stopConversation = "true",
	options = {
	 }
 }
jamgiConvoTemplate:addScreen(not_ready);

collected_credits = ConvoScreen:new {
	id = "collected_credits",
	leftDialog = "",
	customDialogText = "I see you've returned. Do you have my credits?",
	stopConversation = "false",
	options = {
		 {"(LIE) No. I couldn't get any from him.","liar"},
		 {"Here's your credits.","gave_credits"}
	}
 }
jamgiConvoTemplate:addScreen(collected_credits);

liar = ConvoScreen:new {
	id = "liar",
	leftDialog = "",
	customDialogText = "Are you sure about that?",
	stopConversation = "false",
	options = {
		 {"I actually lied, here are your credits.","came_clean"},
		 {"Yeah I'm sure.","liar_2"}
	}
 }
jamgiConvoTemplate:addScreen(liar);

liar_2 = ConvoScreen:new {
	id = "liar_2",
	leftDialog = "",
	customDialogText = "Well I know you're lying because Lesk is a Brawler's Guild member and we run this exercise for every recruit. What a shame. Because of that you'll miss out on one of our special badges and titles. Maybe next time you'll be more honest.",
	stopConversation = "false",
	options = {
		 {"I don't care about that stuff. What's next?","liar_next"}
	}
 }
jamgiConvoTemplate:addScreen(liar_2);

came_clean = ConvoScreen:new {
	id = "came_clean",
	leftDialog = "",
	customDialogText = "Good thing you came clean because this is a routine exercise we send all our recruits on.. Unfortuantly because you lied at first you don't get the special title I can offer but here's the badge.",
	stopConversation = "false",
	options = {
		 {"Well I guess that's better than nothing. What's next?","whats_next_after_lying"}
	}
 }
jamgiConvoTemplate:addScreen(came_clean);

gave_credits = ConvoScreen:new {
	id = "gave_credits",
	leftDialog = "",
	customDialogText = "You passed the test. Here's my special badge and title! Usually we get a bunch of liars and we can't give them special treatment for that.",
	stopConversation = "false",
	options = {
		 {"Wow! Well good thing I didn't lie. What's next?","credits_next"}
	}
 }
jamgiConvoTemplate:addScreen(gave_credits);

liar_next = ConvoScreen:new {
	id = "liar_next",
	leftDialog = "",
	customDialogText = "Well since you lied, I'll have you face off against one of our tougher trainers. If you prove yourself in that fight I'll offer you some training in brawler. Here's the waypoint. Come back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(liar_next);

whats_next_after_lying = ConvoScreen:new {
	id = "whats_next_after_lying",
	leftDialog = "",
	customDialogText = "You did come clean but you still lied, so for the next task you'll be facing one of our trainers. Prove yourself with the trainer and I'll train you in brawler. Here's the waypoint, come back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(whats_next_after_lying);

credits_next = ConvoScreen:new {
	id = "credits_next",
	leftDialog = "",
	customDialogText = "Your next task will be to train with one of our trainers. Since you did the right thing, I'll put you up against one of our easier trainers. Prove yourself with the trainer and I'll train you in brawler. Here's the waypoint, come back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(credits_next);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "You're on your way to greatness young recruit. Go see Trev Alask if you haven't already. I'm sure he could use someone like you.",
	stopConversation = "true",
	options = {
	 }
 }
jamgiConvoTemplate:addScreen(completed);

unarmed_1 = ConvoScreen:new {
	id = "unarmed_1",
	leftDialog = "",
	customDialogText = "You want some training in unarmed? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thank you for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(unarmed_1);

unarmed_2 = ConvoScreen:new {
	id = "unarmed_2",
	leftDialog = "",
	customDialogText = "You want to further your training in unarmed? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(unarmed_2);

onehand_1 = ConvoScreen:new {
	id = "onehand_1",
	leftDialog = "",
	customDialogText = "You want some training in one hand? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(onehand_1);

onehand_2 = ConvoScreen:new {
	id = "onehand_2",
	leftDialog = "",
	customDialogText = "You want to further your training in one hand? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(onehand_2);

twohand_1 = ConvoScreen:new {
	id = "twohand_1",
	leftDialog = "",
	customDialogText = "You want some training in two hand? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(twohand_1);

twohand_2 = ConvoScreen:new {
	id = "twohand_2",
	leftDialog = "",
	customDialogText = "You want to further your training in two hand? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(twohand_2);

polearm_1 = ConvoScreen:new {
	id = "polearm_1",
	leftDialog = "",
	customDialogText = "You want to train in polearm? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(polearm_1);

polearm_2 = ConvoScreen:new {
	id = "polearm_2",
	leftDialog = "",
	customDialogText = "You want to further your training in polearm? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thanks for the training.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(polearm_2);

train = ConvoScreen:new {
	id = "train",
	leftDialog = "",
	customDialogText = "Great work with the trainer. Let me offer you some training.",
	stopConversation = "false",
	options = {
		 {"I need to think about this more.","come_back_train"},
		 {"I would rather train myself, thank you for the offer.","completed"}
	}
 }
jamgiConvoTemplate:addScreen(train);

come_back_train = ConvoScreen:new {
	id = "come_back_train",
	leftDialog = "",
	customDialogText = "Come back when you are ready for some training.",
	stopConversation = "true",
	options = {}
 }
jamgiConvoTemplate:addScreen(come_back_train);

already_trainer = ConvoScreen:new {
	id = "already_trainer",
	leftDialog = "",
	customDialogText = "I said go see the trainer and prove your worth before coming back to me. Get back to it recruit!",
	stopConversation = "true",
	options = {
	}
 }
jamgiConvoTemplate:addScreen(already_trainer);


addConversationTemplate("jamgiConvoTemplate", jamgiConvoTemplate);