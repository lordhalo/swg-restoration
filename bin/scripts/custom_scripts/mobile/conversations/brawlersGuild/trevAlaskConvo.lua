
--Made with Skyyyr's Wrench Tool 

trevAlaskConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "trev_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "Welcome to the Brawler's Guild, I am Trev Alask. I could use someone with your capabilities, are you ready?",
	stopConversation = "false",
	options = {
		 {"I am ready to begin.","accept"},
		 {"I need some time before I commit to anything.","return_later"}
	}
 }
trevAlaskConvoTemplate:addScreen(first);

return_later = ConvoScreen:new {
	id = "return_later",
	leftDialog = "",
	customDialogText = "Alright, come back whenever you're ready.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(return_later);


accept = ConvoScreen:new {
	id = "accept",
	leftDialog = "",
	customDialogText = "Ah good, so I understand you did some small things for the guild, but maybe now you're ready for something more challenging. We are looking for someone to do a simple patrol around our territory. Some of our storage vaults have been broken into recently. Can you handle this?",
	stopConversation = "false",
	options = {
		 {"Sounds simple enough for me.","accept_quest"},
		 {"Storage vaults?","inquire_storage"},
		 {"That sounds below me.","deny"}
	}
 }
trevAlaskConvoTemplate:addScreen(accept);

accept_quest = ConvoScreen:new {
	id = "accept_quest",
	leftDialog = "",
	customDialogText = "Great, here's your first waypoint. Report back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(accept_quest);

inquire_storage = ConvoScreen:new {
	id = "inquire_storage",
	leftDialog = "",
	customDialogText = "Yes, we have storage vaults that contain our supplies for new members. Normally we'd send someone of a higher status, but I think you can handle this. What do you say?",
	stopConversation = "false",
	options = {
		 {"Interesting, alright I'll do it.","accept_quest_inquire"},
		 {"I would rather not.","deny_inquire"}
	}
 }
trevAlaskConvoTemplate:addScreen(inquire_storage);

accept_quest_inquire = ConvoScreen:new {
	id = "accept_quest_inquire",
	leftDialog = "",
	customDialogText = "Now that I've told you what's going on, keep a good look out for anything suspecious. Here's the first waypoint, report back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(accept_quest_inquire);

deny = ConvoScreen:new {
	id = "deny",
	leftDialog = "",
	customDialogText = "You'll learn nothing is below you when it comes to serving this guild. Come back with an attitude change and we can talk.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(deny);

deny_inquire = ConvoScreen:new {
	id = "deny_inquire",
	leftDialog = "",
	customDialogText = "We need someone willing to do this for us, if you change your mind come back and talk to me.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(deny_inquire);

not_ready = ConvoScreen:new {
	id = "not_ready",
	leftDialog = "",
	customDialogText = "Sorry but you can't just walk up and talk to me. We have a chain of command.",
	stopConversation = "true",
	options = {
	 }
 }
trevAlaskConvoTemplate:addScreen(not_ready);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "Look, I sent you to patrol our territory don't come back until you finish doing that.",
	stopConversation = "true",
	options = {
	 }
 }
trevAlaskConvoTemplate:addScreen(already);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "I couldn't be happier with your performance. I've sent word to Ver Nul, you should go see her now. ",
	stopConversation = "true",
	options = {
	 }
 }
trevAlaskConvoTemplate:addScreen(completed);

return_from_quest = ConvoScreen:new {
	id = "return_from_quest",
	leftDialog = "",
	customDialogText = "I see you've returned. Give me your report of the patrol.",
	stopConversation = "false",
	options = {
		 {"The patrol was easy. I did see someone snooping around though.","nothing_to_report"},
		 {"Didn't notice anything unusual.","no_notice"}
	}
 }
trevAlaskConvoTemplate:addScreen(return_from_quest);

return_from_quest_special = ConvoScreen:new {
	id = "return_from_quest_special",
	leftDialog = "",
	customDialogText = "I see you've returned. Give me your report.",
	stopConversation = "false",
	options = {
		 {"I noticed someone stealing from the containers and killed them.","killed_enemy_patrol"},
		 {"Didn't notice anything unusual.","no_notice"}
	}
 }
trevAlaskConvoTemplate:addScreen(return_from_quest_special);

no_notice = ConvoScreen:new {
	id = "no_notice",
	leftDialog = "",
	customDialogText = "I see you aren't very observant. Before I offer you any training I need you to meet up with another trainee behind the starport. Here's the waypoint.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(no_notice);

nothing_to_report = ConvoScreen:new {
	id = "nothing_to_report",
	leftDialog = "",
	customDialogText = "I see... Before I offer you any training I need you to meet up with another trainee behind the starport. Here's the waypoint.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(nothing_to_report);

killed_enemy_patrol = ConvoScreen:new {
	id = "killed_enemy_patrol",
	leftDialog = "",
	customDialogText = "Interesting indeed. *Takes report datapad* I'll look into this further. Here is a special title I'm allowed to hand out once in a while.. You deserve it. Now before I offer any training I need you to meet up with another trainee behind the starport. Here's the waypoint.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(killed_enemy_patrol);

return_from_trainee = ConvoScreen:new {
	id = "return_from_trainee",
	leftDialog = "",
	customDialogText = "You've returned by yourself. What happened?",
	stopConversation = "false",
	options = {
		 {"The trainee wasn't there, but I found this *Hand over datapad*","hand_datapad"},
		 {"I showed up, but the trainee wasn't there.","no_trainee"}
	}
 }
trevAlaskConvoTemplate:addScreen(return_from_trainee);

hand_datapad = ConvoScreen:new {
	id = "hand_datapad",
	leftDialog = "",
	customDialogText = "Thank you for turning this in. Here is a bonus for that. You've done enough for me, let me train you then you need to go see Ver Nul, she will be your handler now.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(hand_datapad);

no_trainee = ConvoScreen:new {
	id = "no_trainee",
	leftDialog = "",
	customDialogText = "Interesting, you didn't find anything at the meet up location?",
	stopConversation = "false",
	options = {
		 {"Yes here's the datapad I found.","last_chance_datapad"},
		 {"No, there wasn't anything or anyone.","no_datapad"}
	}
 }
trevAlaskConvoTemplate:addScreen(no_trainee);

no_datapad = ConvoScreen:new {
	id = "no_datapad",
	leftDialog = "",
	customDialogText = "That's a shame, we could've used any valuable data to find out what has been going on. In any other case after I train you go see Ver Nul, she will be your handler from now on.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(no_datapad);

last_chance_datapad = ConvoScreen:new {
	id = "last_chance_datapad",
	leftDialog = "",
	customDialogText = "Interesting, a datapad was left behind. We will investigate this further. Go see Ver Nul after I train you, she will be your handler from now on.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(last_chance_datapad);

already_trainee = ConvoScreen:new {
	id = "already_trainee",
	leftDialog = "",
	customDialogText = "Quit wasting time and go meet up with the other trainee. Here's the waypoint again.",
	stopConversation = "true",
	options = {
	}
 }
trevAlaskConvoTemplate:addScreen(already_trainee);


addConversationTemplate("trevAlaskConvoTemplate", trevAlaskConvoTemplate);