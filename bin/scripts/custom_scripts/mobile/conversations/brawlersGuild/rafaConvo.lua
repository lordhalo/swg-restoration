
--Made with Skyyyr's Wrench Tool 

rafaConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "rafa_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "Hello, and welcome. If you're looking to join the guild you'll have to be accepted first. This is the Brawler's Guild afterall.",
	stopConversation = "false",
	options = {
		 {"Of course, how do I get accepted?","accept_1"},
		 {"The Brawler's guild?","inquire_guild"},
		 {"I seem to have stepped into the wrong building.","deny"}
	}
 }
rafaConvoTemplate:addScreen(first);

deny = ConvoScreen:new {
	id = "deny",
	leftDialog = "",
	customDialogText = "Aye I agree.",
	stopConversation = "true",
	options = {
	}
 }
rafaConvoTemplate:addScreen(deny);

accept_1 = ConvoScreen:new {
	id = "accept_1",
	leftDialog = "",
	customDialogText = "Hold on now, before you really think about joining you should know of our benefits first. We offer free training, and we can supply you with some gear too. So are you still wanting to join?",
	stopConversation = "false",
	options = {
		 {"Yes sir! That sounds great.","accept_2"},
		 {"Sounds too much for me, there has to be some sort of catch.","deny_2"}
	}
 }
rafaConvoTemplate:addScreen(accept_1);

accept_2 = ConvoScreen:new {
	id = "accept_2",
	leftDialog = "",
	customDialogText = "Great! First task on the list is to clear out these blasted rats in the basement! You can handle that right?",
	stopConversation = "false",
	options = {
		 {"Rats? I'm on it!","accepted_quest"},
		 {"I don't think I'm cut out for this.","deny_rats"}
	}
 }
rafaConvoTemplate:addScreen(accept_2);

accepted_quest = ConvoScreen:new {
	id = "accepted_quest",
	leftDialog = "",
	customDialogText = "Go kill the rats and come back when you're done.",
	stopConversation = "true",
	options = {
	}
 }
rafaConvoTemplate:addScreen(accepted_quest);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "I said go kill the rats in the basement, quit wasting time.",
	stopConversation = "true",
	options = {
	 }
 }
rafaConvoTemplate:addScreen(already);

not_ready = ConvoScreen:new {
	id = "not_ready",
	leftDialog = "",
	customDialogText = "We only accept recruits with proper minimal training, you aren't even a novice brawler. Either get trained and come back or buzz off lad!",
	stopConversation = "true",
	options = {
	 }
 }
rafaConvoTemplate:addScreen(not_ready);

deny_2 = ConvoScreen:new {
	id = "deny_2",
	leftDialog = "",
	customDialogText = "We offer benefits and you freak out... Sheesh...",
	stopConversation = "true",
	options = {
	}
 }
rafaConvoTemplate:addScreen(deny_2);

deny_rats = ConvoScreen:new {
	id = "deny_rats",
	leftDialog = "",
	customDialogText = "You can't handle a few rats?? Wow! Yeah this guild can't be messing around with bantha poodoo like you. Buzz off lad!",
	stopConversation = "true",
	options = {
	}
 }
rafaConvoTemplate:addScreen(deny_rats);

inquire_guild = ConvoScreen:new {
	id = "inquire_guild",
	leftDialog = "",
	customDialogText = "Yes we are the Brawler's Guild, one of many guilds in the galaxy. We are the elite of all melee combat, some of the best fighters in the known galaxy came from our guild. Eventually you might be able to talk to some of our more specialized members.. But for now just pay your dues. What do you say?",
	stopConversation = "false",
	options = {
		 {"Very Impressive, I'm in!","accept_1"}
	}
 }
rafaConvoTemplate:addScreen(inquire_guild);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "You've done all that I've asked of you. Here take this badge and new title as proof of your acceptance into our guild. Welcome to the Brawler's Guild!",
	stopConversation = "true",
	options = {
	 }
 }
rafaConvoTemplate:addScreen(completed);

train = ConvoScreen:new {
	id = "train",
	leftDialog = "",
	customDialogText = "Great work on the rats, let me offer you some training.",
	stopConversation = "false",
	options = {
		 {"I'd rather think about it some more.","wait_train"},
		 {"I can train myself, thanks for the offer though.","completed"}
	}
 }
rafaConvoTemplate:addScreen(train);


onehand_1 = ConvoScreen:new {
	id = "onehand_1",
	leftDialog = "",
	customDialogText = "You want some 1 handed training? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thank you for the training.","completed"}
	}
 }
rafaConvoTemplate:addScreen(onehand_1);

unarmed_1 = ConvoScreen:new {
	id = "unarmed_1",
	leftDialog = "",
	customDialogText = "You want some unarmed training? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thank you for the training.","completed"}
	}
 }
rafaConvoTemplate:addScreen(unarmed_1);



twohand_1 = ConvoScreen:new {
	id = "twohand_1",
	leftDialog = "",
	customDialogText = "You want some 2 handed training? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thank you for the training.","completed"}
	}
 }
rafaConvoTemplate:addScreen(twohand_1);

polearm_1 = ConvoScreen:new {
	id = "polearm_1",
	leftDialog = "",
	customDialogText = "You want some polearm training? Here you go.",
	stopConversation = "false",
	options = {
		 {"Thank you for the training.","completed"}
	}
 }
rafaConvoTemplate:addScreen(polearm_1);

wait_train = ConvoScreen:new {
	id = "wait_train",
	leftDialog = "",
	customDialogText = "Just remember I can only train you in only one of these skills: unarmed 1, polearm 1, 1hand 1, 2hand 1. Come back when you decide and I'll give you your badge and title aswell!",
	stopConversation = "true",
	options = {}
 }
rafaConvoTemplate:addScreen(wait_train);

addConversationTemplate("rafaConvoTemplate", rafaConvoTemplate);