--Brawlers Guild
includeFile("../custom_scripts/mobile/conversations/brawlersGuild/rafaConvo.lua")
includeFile("../custom_scripts/mobile/conversations/brawlersGuild/trevAlaskConvo.lua")
includeFile("../custom_scripts/mobile/conversations/brawlersGuild/jamgiConvo.lua")
includeFile("../custom_scripts/mobile/conversations/brawlersGuild/leskConvo.lua")

--Misc
includeFile("../custom_scripts/mobile/conversations/misc/tutorialDroidConvo.lua")

--Resto
includeFile("../custom_scripts/mobile/conversations/resto/vipBunkerConv.lua")
includeFile("../custom_scripts/mobile/conversations/resto/krukConvo.lua")