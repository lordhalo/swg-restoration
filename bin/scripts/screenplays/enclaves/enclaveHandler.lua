local ObjectManager = require("managers.object.object_manager")

enclaveHandler = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "enclaveHandler",

	jedi_roamers = {
		{"enclave_padawan", 60, 0.0, -19.3, 38.2, 90, 8525439, "a Jedi Padawan", "commoner1"},
		--{"commoner_naboo", 60, 0.0, 25.0, 45.2, 180, 1688859, "a Commoner", "commoner2"}, enclave_padawan
	},
}
registerScreenPlay("enclaveHandler", true)

function enclaveHandler:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
		self:spawnActiveAreas()
		self:setupHallBlockers()
	end
end

function enclaveHandler:spawnActiveAreas()
	-- Light Enclave area

	--local pAreaGuardian = spawnActiveArea("yavin4", "object/active_area.iff", -5575, 87.8, 4906, 1800, 0)
	local pInitiateTrial = spawnActiveArea("yavin4", "object/active_area.iff", -7050, 17, 5395, 120, 0)
	local pDarkInitiateTrial = spawnActiveArea("yavin4", "object/active_area.iff", 6053, 68, 602, 120, 0)

	--if (pAreaGuardian ~= nil) then
	--	createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterAreaGuardian", pAreaGuardian)
	--	end
	
	if (pInitiateTrial ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterInitiate", pInitiateTrial)
		 createObserver(EXITEDAREA, "enclaveHandler", "notifyLeaveInitiate", pInitiateTrial)
	   end

	if (pDarkInitiateTrial ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterDarkInitiate", pDarkInitiateTrial)
		 createObserver(EXITEDAREA, "enclaveHandler", "notifyLeaveDarkInitiate", pDarkInitiateTrial)
	   end
	--Dark Enclave area
end

function enclaveHandler:setupHallBlockers()

	
	local pMainHallJedi = spawnActiveArea("yavin4", "object/active_area.iff", -5576, 87, 4908, 3, 0)
    
	if (pMainHallJedi ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterMain", pMainHallJedi)
	    end

	local pMainHallSith = spawnActiveArea("yavin4", "object/active_area.iff", 5072, 78, 310, 3, 0)
    
	if (pMainHallSith ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterMainSith", pMainHallSith)
	    end

	local pActiveArea = spawnActiveArea("yavin4", "object/active_area.iff", -5483, 68, 4911, 3, 8525436)
    
	if (pActiveArea ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifySpawnArea", pActiveArea)
	    end

	local pPadawan = spawnActiveArea("yavin4", "object/active_area.iff", -5520, 65, 4868, 5, 8525419)
	
	if (pPadawan ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterPadawan", pPadawan)
	    end

	local pPadawan2 = spawnActiveArea("yavin4", "object/active_area.iff", -5520, 65.1, 4880, 5, 8525419)
	
	if (pPadawan2 ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterPadawan", pPadawan2)
	    end


	local pPadawanTwo = spawnActiveArea("yavin4", "object/active_area.iff", -5507, 65, 4874, 5, 8525422)
	
	if (pPadawanTwo ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterPadawanTwo", pPadawanTwo)
	    end


	local pKnight = spawnActiveArea("yavin4", "object/active_area.iff", -5517, 65, 4955, 5, 8525431)
	
	if (pKnight ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterKnight", pKnight)
	    end

	local pGuardian = spawnActiveArea("yavin4", "object/active_area.iff", -5507, 64.2, 4947, 5, 8525428)
	
	if (pGuardian ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterGuardian", pGuardian)
	    end

	-------------Sith Area

	--local pAdept = spawnActiveArea("yavin4", "object/active_area.iff", 5026, 35.6, 289, 2, 3435643)
	local pAdeptTwo = spawnActiveArea("yavin4", "object/active_area.iff", 4996, 36.6, 295.6, 6, 3435643)
	local pAdeptThree = spawnActiveArea("yavin4", "object/active_area.iff", 4996, 36.6, 324.7, 6, 3435643)
	local pAdeptFour = spawnActiveArea("yavin4", "object/active_area.iff", 4970, 37.6, 322.7, 6, 3435643)
	local pAdeptFive = spawnActiveArea("yavin4", "object/active_area.iff", 4971.5, 37.6, 298.8, 6, 3435643)
	local pAdeptSix = spawnActiveArea("yavin4", "object/active_area.iff", 4963.0, 39.6, 319, 6, 3435643)
	local pAdeptSeven = spawnActiveArea("yavin4", "object/active_area.iff", 4963.0, 39.6, 300.6, 6, 3435643)
	local pAdeptEight = spawnActiveArea("yavin4", "object/active_area.iff", 4955.0, 43.6, 310, 6, 3435643)
	
	if (pAdeptTwo ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterAdept", pAdeptTwo)
	    end

	if (pAdeptThree ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterAdeptTwo", pAdeptThree)
	    end

	if (pAdeptFour ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterLord", pAdeptFour)
	    end

	if (pAdeptFive ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterLord", pAdeptFive)
	    end

	if (pAdeptSix ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterDarth", pAdeptSix)
	    end

	if (pAdeptSeven ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterDarth", pAdeptSeven)
	    end

	if (pAdeptEight ~= nil) then
	        createObserver(ENTEREDAREA, "enclaveHandler", "notifyEnterMaster", pAdeptEight)
	    end

end

function enclaveHandler:notifySpawnArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_light_master")) then
			player:sendSystemMessage("Welcome Master Jedi")
		else
			player:teleport(-0, -18.9, -26.5, 8525436)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterAreaGuardian(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("fs_jedi_8")) then
			player:sendSystemMessage("Welcome")
		else
			player:teleport(-5576, 87.2, 4899, 0)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterMain(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end

	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("force_title_jedi_rank_02")) then
			return 0
		else
			player:teleport(-5576, 87.2, 4899, 0)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterMainSith(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("force_title_jedi_rank_02")) then
			return 0
		else
			player:teleport(5087, 79, 307, 0)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterPadawan(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		if (player:hasSkill("rank_light_novice")) then
			player:sendSystemMessage("Welcome")
		else
			player:sendSystemMessage("Only Jedi Padawans and higher may enter this room.")
			player:teleport(-22.3, -18.9, 13.5, 8525434)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterPadawanTwo(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_light_1")) then
			player:sendSystemMessage("Welcome")
		else
			player:sendSystemMessage("Only Jedi Tier 2 Padawans and higher may enter this room.")
			player:teleport(-24.2, -18.9, -12.1, 8525435)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterKnight(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_light_5")) then
			player:sendSystemMessage("Welcome")
		else
			player:sendSystemMessage("Only Jedi Knights and higher may enter this room.")
			player:teleport(23.2, -18.9, 11.7, 8525438)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterGuardian(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_light_8")) then
			player:sendSystemMessage("Welcome")
		else
			player:sendSystemMessage("Only Jedi Guardians and higher may enter this room.")
			player:teleport(23.2, -12.0, 11.7, 8525437)
		end
		return 0
	end)
end
--------------------Sith
function enclaveHandler:notifyEnterAdept(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_sith_novice")) then
			--player:sendSystemMessage("G")
		else
			player:sendSystemMessage("You don't have the rank required to enter this room.")
			player:teleport(0, -43.4, -31.1, 3435634)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterAdeptTwo(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_sith_1")) then
			--player:sendSystemMessage("G")
		else
			player:sendSystemMessage("You don't have the rank required to enter this room.")
			player:teleport(0, -43.4, -31.1, 3435634)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterLord(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_sith_5")) then
			--player:sendSystemMessage("G")
		else
			player:sendSystemMessage("You don't have the rank required to enter this room.")
			player:teleport(0, -43.4, -31.1, 3435634)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterDarth(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_sith_8")) then
			--player:sendSystemMessage("G")
		else
			player:sendSystemMessage("You don't have the rank required to enter this room.")
			player:teleport(0, -43.4, -31.1, 3435634)
		end
		return 0
	end)
end

function enclaveHandler:notifyEnterMaster(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isPlayerCreature()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)

		if (player:hasSkill("rank_sith_10")) then
			--player:sendSystemMessage("G")
		else
			player:sendSystemMessage("You don't have the rank required to enter this room.")
			player:teleport(0, -43.4, -31.1, 3435634)
		end
		return 0
	end)
end




-------------------Quest

function enclaveHandler:notifyEnterInitiate(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		--if (player:getObjectID() == readData("initiate:owner")) then
		--	player:sendSystemMessage("You own this area.")
		--elseif readData("initiate:owner" == 0)
		--	player:sendSystemMessage("You are entering the area")
		--else
		--	player:sendSystemMessage("Someone else is currently doing this quest")
		--	player:teleport(-6825, 20, 5329, 0)
		--end

		if (readData("initiate:owner") == 0)  then
			if (player:hasSkill("fs_jedi_master")) then
				player:sendSystemMessage("You have completed this trial.")
				player:teleport(-6825, 20, 5329, 0)
			elseif (player:hasSkill("fs_jedi_a_4") and player:hasSkill("fs_jedi_b_4") and player:hasSkill("fs_jedi_c_4") and player:hasSkill("fs_jedi_d_4")) then
				self:spawnLightQuestObjects(pMovingObject)
				--self:spawnLightQuestNPC(pMovingObject)
				writeData("initiate:owner",1)
			else
				player:sendSystemMessage("A Mysterious shield blocks you from entering this area.")
				player:teleport(-6825, 20, 5329, 0)
			end
		else
			player:sendSystemMessage("Someone else is currently doing this quest")
			player:teleport(-6825, 20, 5329, 0)
		end


		return 0
	end)
end

function enclaveHandler:notifyEnterDarkInitiate(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		if (readData("initiateDark:owner") == 0)  then
			if (player:hasSkill("fs_jedi_master")) then
				player:sendSystemMessage("You have completed this trial.")
				player:teleport(5961, 66, 522, 0)
			elseif (player:hasSkill("fs_jedi_a_4") and player:hasSkill("fs_jedi_b_4") and player:hasSkill("fs_jedi_c_4") and player:hasSkill("fs_jedi_d_4")) then
				self:spawnDarkQuestObjects(pMovingObject)
				--self:spawnLightQuestNPC(pMovingObject)
				writeData("initiateDark:owner",1)
			else
				player:sendSystemMessage("A Mysterious shield blocks you from entering this area.")
				player:teleport(5961, 66, 522, 0)
			end
		else
			player:sendSystemMessage("Someone else is currently doing this quest")
			player:teleport(5961, 66, 522, 0)
		end


		return 0
	end)
end

function enclaveHandler:notifyLeaveInitiate(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
			player:sendSystemMessage("You are leaving the area.")
			writeData("initiate:owner",0)
		return 0
	end)
end

function enclaveHandler:notifyLeaveDarkInitiate(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
			player:sendSystemMessage("You are leaving the area.")
			writeData("initiateDark:owner",0)
		return 0
	end)
end

--Mobile Spawning
function enclaveHandler:spawnMobiles()

	for i,v in ipairs(self.jedi_roamers) do
		local pMobile = spawnMobile("yavin4", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
		if (pMobile ~= nil) then
			writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 1)
			writeStringData(SceneObject(pMobile):getObjectID() .. ":name", v[9])
			CreatureObject(pMobile):setCustomObjectName(v[8])
			createEvent(1000, "enclaveHandler", "roamBuilding", pMobile, "")
		end
	end

end

function enclaveHandler:roamBuilding(pMobile)
	createEvent(getRandomNumber(30,45) * 1000, "enclaveHandler", "npcPatrol", pMobile, "")
	createObserver(DESTINATIONREACHED, "enclaveHandler", "npcDestReached", pMobile)
	AiAgent(pMobile):setAiTemplate("manualescortwalk")
	AiAgent(pMobile):setFollowState(4)
end

function enclaveHandler:npcPatrol(pMobile)
	if (pMobile == nil) then
		return
	end
	local name = readStringData(SceneObject(pMobile):getObjectID() .. ":name")
	local curLoc = readData(SceneObject(pMobile):getObjectID() .. ":currentLoc")
	local nextLoc

	if (name == "commoner1") then
		if (curLoc == 1) then 
			nextLoc = { 0.0, -15.1, 25.5, 8525418 }
		else
			nextLoc = { -2.5, -15.1, -25.5, 8525418 }
		end
	end

	if (name == "commoner2") then
		if (curLoc == 1) then
			nextLoc = { 0.0, 25.0, 45.2, 1688859 }
		else
			nextLoc = { 30.0, 23.0, -32.0, 1688861 }
		end
	end

	AiAgent(pMobile):stopWaiting()
	AiAgent(pMobile):setWait(0)
	AiAgent(pMobile):setNextPosition(nextLoc[1], nextLoc[2], nextLoc[3], nextLoc[4])
	AiAgent(pMobile):executeBehavior()
end

function enclaveHandler:npcDestReached(pMobile)
	if (pMobile == nil) then
		return 0
	end

	local curLoc = readData(SceneObject(pMobile):getObjectID() .. ":currentLoc")

	if (curLoc == 1) then
		writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 2)
	else
		writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 1)
	end

	createEvent(getRandomNumber(350,450) * 100, "enclaveHandler", "npcPatrol", pMobile, "")

	return 0
end

--Objects
function enclaveHandler:spawnSceneObjects()
	spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine.iff", -257, 35, 4857, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine.iff", -6975, 73, -5710, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/tangible/terminal/terminal_light_enclave_challenge.iff", -3.5, -15.1, 22.7, 8525418, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/tangible/terminal/terminal_light_enclave_voting.iff", 3.5, -15.1, 22.7, 8525418, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/tangible/crafting/station/weapon_station.iff", 8.4, -18.9, 32.2, 8525439, 0, 0, 0, 0)

	spawnSceneObject("yavin4", "object/tangible/crafting/station/weapon_station.iff", 17.5, -43.4, -37.7, 3435634, 0, 0, 0, 0)

	spawnSceneObject("yavin4", "object/tangible/terminal/terminal_dark_enclave_challenge.iff", -6.0, -43.4, -34.7, 3435634, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/tangible/terminal/terminal_dark_enclave_voting.iff", 6.0, -43.4, -34.7, 3435634, 0, 0, 0, 0)	
	
end

function enclaveHandler:spawnLightQuestNPC()

	local pFinalQuest = spawnMobile("yavin4", "initiate_quest", 0, -7054,20, 5386, 0, 0)
	--createObserver(DAMAGERECEIVED, "enclaveHandler", "npcDamageObserver", pFinalQuest)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "finalQuestDead", pFinalQuest)

end

function enclaveHandler:spawnDarkQuestNPC()

	local pFinalQuestDark = spawnMobile("yavin4", "initiate_quest", 0, 6049, 67, 598, 0, 0)
	--createObserver(DAMAGERECEIVED, "enclaveHandler", "npcDamageObserver", pFinalQuest)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "finalQuestDeadDark", pFinalQuestDark)

end

function enclaveHandler:finalQuestDead(pNPC, playerObject)
	local player = LuaCreatureObject(playerObject)
	writeData(player:getObjectID() .. ":initiate:npcDead", 1)
	--Reward Quest Item
	local pInventory = CreatureObject(playerObject):getSlottedObject("inventory")

	if pInventory == nil then
		return
	end

	createLoot(pInventory, "holocron_quest", 0, true)
	CreatureObject(playerObject):sendSystemMessage("@theme_park/messages:theme_park_reward")
	return 0
end

function enclaveHandler:finalQuestDeadDark(pNPC, playerObject)
	local player = LuaCreatureObject(playerObject)
	writeData(player:getObjectID() .. ":initiateDark:npcDead", 1)
	--Reward Quest Item
	local pInventory = CreatureObject(playerObject):getSlottedObject("inventory")

	if pInventory == nil then
		return
	end

	createLoot(pInventory, "holocron_quest", 0, true)
	CreatureObject(playerObject):sendSystemMessage("@theme_park/messages:theme_park_reward")
	return 0
end

function enclaveHandler:npcDamageObserver(bossObject, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if ((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) then

	end

	if ((health <= (maxHealth * 0.4)) or (action <= (maxAction * 0.4)) or (mind <= (maxMind * 0.4))) then

	end
end

function enclaveHandler:spawnLightQuestObjects(creatureObject)
	local player = LuaCreatureObject(creatureObject)

	local pCrystal = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", -7052, 8, 5468, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_lair_evil_fire_large_green.iff", -7052, 8, 5468, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilGreenDestroy", pCrystal) --destroy observer
	writeData(player:getObjectID() .. ":initiate:greenShrine", 1)

	local pCrystal2 = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", -7071, 9, 5455, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_lair_evil_fire_large_red.iff", -7071, 9, 5455, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilRedDestroy", pCrystal2) --destroy observer
	writeData(player:getObjectID() .. ":initiate:redShrine", 1)

	local pCrystal3 = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", -7028, 8, 5486, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_poi_electricity_2x2.iff", -7028, 8, 5486, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilElectricDestroy", pCrystal3) --destroy observer
	writeData(player:getObjectID() .. ":initiate:eleShrine", 1)
end

function enclaveHandler:spawnDarkQuestObjects(creatureObject)
	local player = LuaCreatureObject(creatureObject)

	local pCrystal = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", 6059, 69, 598, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_lair_evil_fire_large_green.iff", 6059, 69, 598, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilGreenDestroyDark", pCrystal) --destroy observer
	writeData(player:getObjectID() .. ":initiateDark:greenShrine", 1)

	local pCrystal2 = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", 6059, 68, 607, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_lair_evil_fire_large_red.iff", 6059, 68, 607, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilRedDestroyDark", pCrystal2) --destroy observer
	writeData(player:getObjectID() .. ":initiateDark:redShrine", 1)

	local pCrystal3 = spawnSceneObject("yavin4", "object/tangible/jedi/power_shrine_red.iff", 6039, 68, 589, 0, 0, 0, 0, 0)
	spawnSceneObject("yavin4", "object/static/particle/pt_poi_electricity_2x2.iff", 6039, 68, 589, 0, 0, 0, 0, 0)
	createObserver(OBJECTDESTRUCTION, "enclaveHandler", "evilElectricDestroyDark", pCrystal3) --destroy observer
	writeData(player:getObjectID() .. ":initiateDark:eleShrine", 1)
end

function enclaveHandler:evilGreenDestroy(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)


	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("You hear a whir eminate from the droids inards which turns into a slow steady hum.")

	writeData(player:getObjectID() .. ":initiate:greenShrine", 2)

	self:updateShrinesStatus(playerObject)
	return 1
end

function enclaveHandler:evilRedDestroy(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)


	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("The droids vocator spits and sputters a string of incomprehencible jibberish then falls silent.")

	writeData(player:getObjectID() .. ":initiate:redShrine", 2)

	self:updateShrinesStatus(playerObject)
	return 1
end

function enclaveHandler:evilElectricDestroy(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)

	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("The droids photo-receptors flicker on and off.")

	writeData(player:getObjectID() .. ":initiate:eleShrine", 2)

	self:updateShrinesStatus(playerObject)
	return 1
end

function enclaveHandler:updateShrinesStatus(playerObject)
	local player = LuaCreatureObject(playerObject)
	red = readData(player:getObjectID() .. ":initiate:greenShrine")
	green = readData(player:getObjectID() .. ":initiate:redShrine")
	ele = readData(player:getObjectID() .. ":initiate:eleShrine")
	if(red == 2 and green == 2 and ele == 2) then
		if(readData(player:getObjectID() .. ":initiate:npcDead") == 0) then
			self:spawnLightQuestNPC()
		else
			player:sendSystemMessage("You have already completed this part of the quest.")
		end
	end
end

function enclaveHandler:evilGreenDestroyDark(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)


	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("You hear a whir eminate from the droids inards which turns into a slow steady hum.")

	writeData(player:getObjectID() .. ":initiateDark:greenShrine", 2)

	self:updateShrinesStatusDark(playerObject)
	return 1
end

function enclaveHandler:evilRedDestroyDark(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)


	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("The droids vocator spits and sputters a string of incomprehencible jibberish then falls silent.")

	writeData(player:getObjectID() .. ":initiateDark:redShrine", 2)

	self:updateShrinesStatusDark(playerObject)
	return 1
end

function enclaveHandler:evilElectricDestroyDark(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local debris = LuaSceneObject(pCrystal)

	debris:destroyObjectFromWorld()
	
	player:clearCombatState(1)

	player:sendSystemMessage("The droids photo-receptors flicker on and off.")

	writeData(player:getObjectID() .. ":initiateDark:eleShrine", 2)

	self:updateShrinesStatusDark(playerObject)
	return 1
end

function enclaveHandler:updateShrinesStatusDark(playerObject)
	local player = LuaCreatureObject(playerObject)
	red = readData(player:getObjectID() .. ":initiateDark:greenShrine")
	green = readData(player:getObjectID() .. ":initiateDark:redShrine")
	ele = readData(player:getObjectID() .. ":initiateDark:eleShrine")
	if(red == 2 and green == 2 and ele == 2) then
		if(readData(player:getObjectID() .. ":initiateDark:npcDead") == 0) then
			self:spawnDarkQuestNPC()
		else
			player:sendSystemMessage("You have already completed this part of the quest.")
		end
	end
end

