local ObjectManager = require("managers.object.object_manager")

jedi_daily_two = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jedi_daily_two",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("jedi_daily_two", true)

function jedi_daily_two:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end


function jedi_daily_two:spawnMobiles()
	spawnMobile("yavin4", "guardian_lyshnal", 0, -9.2, -23.6, 6.6, -53, 8525418) -- quest giver
	local pNPC = spawnMobile("yavin4", "sith_acolyte", 300,  -6944, 50, 6554,  123, 0) --NPC to Kill
	createObserver(OBJECTDESTRUCTION, "jedi_daily_two", "firstQuestDead", pNPC)
end


function jedi_daily_two:firstQuestDead(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasQuest = player:hasScreenPlayState(2, "jedi_daily_two")

	if(hasQuest == true) then
		player:setScreenPlayState(4, jedi_daily_two.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("yavin4", "Guardian Lyshnal", "", -5520, 4902, 5, true, true, WAYPOINTTHEMEPARK, 1)
		CreatureObject(playerObject):sendSystemMessage("Return to Lyshnal")
	end

	return 0
end

function jedi_daily_two:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(jedi_daily_two.states.quest.phasetwo, jedi_daily_two.questString)
	player:removeScreenPlayState(jedi_daily_two.states.quest.phaseone, jedi_daily_two.questString)
	player:removeScreenPlayState(jedi_daily_two.states.quest.intro, jedi_daily_two.questString)

end
--Setup

function jedi_daily_two:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jedi_daily_two:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_daily_two_convo_handler = Object:new {
	
 }

function jedi_daily_two_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jedi_daily_two")
			local questTurnin = creature:hasScreenPlayState(4, "jedi_daily_two")
			local questComplete = creature:hasScreenPlayState(8, "jedi_daily_two")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_daily_two_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jedi_daily_two.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Quest Location", "", -6944, 6554, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Sith Acolyte", "Kill the Sith Acolyte", -6944, 6554, 5, true, true, 0)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jedi_daily_two.questString)

		player:awardExperience("force_rank_xp", 10, true)

		createEvent(true, 86400000, "jedi_daily_two", "removeDailyTimer", conversingPlayer)
	end
	


	return conversationScreen
end

