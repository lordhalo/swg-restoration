local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsTen = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsTen",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	commando = {
		{"elite_novatrooper", 1200, -2140, 20, 2228, 1, 0},
		{"storm_commando", 1200, -2142, 20, 2228, 1, 0},
		{"storm_commando", 1200, -2144, 20, 2228, 1, 0},
		{"storm_commando", 1200, -2146, 20, 2228, 1, 0},
		{"storm_commando", 1200, -2148, 20, 2228, 1, 0},
		{"storm_commando", 1200, -2140, 20, 2232, 1, 0},
		{"storm_commando", 1200, -2142, 20, 2232, 1, 0},
		{"storm_commando", 1200, -2144, 20, 2232, 1, 0},
		{"storm_commando", 1200, -2146, 20, 2232, 1, 0},
		{"storm_commando", 1200, -2148, 20, 2232, 1, 0},

	},

}
registerScreenPlay("jediKnightTrialsTen", true)

function jediKnightTrialsTen:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsTen:spawnMobiles()

		local random = math.random(100)

		local mobileTable = self.commando

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			--AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsTen", "commandoKilled", pCommando)
		end
end


function jediKnightTrialsTen:commandoKilled(pGraul, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsTen")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsTen")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":commandoKilled", readData(groupMember:getObjectID() .. ":commandoKilled") + 1)
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":commandoKilled") .. "/ 47")						
			
						if(readData(groupMember:getObjectID() .. ":commandoKilled") == 47) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsTen.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsTen")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":commandoKilled", readData(player:getObjectID() .. ":commandoKilled") + 1)	
						player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":commandoKilled") .. "/ 47")	

						if(readData(player:getObjectID() .. ":commandoKilled") == 47) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsTen.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsTen:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsTen:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_ten_convo_handler = Object:new {
	
 }

function jedi_knight_trials_ten_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsTen")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsTen")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsTen")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsEight")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_ten_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsTen.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Quest Location", "", -6944, 6554, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Sith Acolyte", "Kill the Sith Acolyte", -6944, 6554, 5, true, true, 0)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsTen.questString)

		player:awardExperience("force_rank_xp", 10)

	end
	


	return conversationScreen
end

