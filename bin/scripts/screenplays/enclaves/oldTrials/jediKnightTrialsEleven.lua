local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsEleven = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsEleven",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

}
registerScreenPlay("jediKnightTrialsEleven", true)

function jediKnightTrialsEleven:start()
	--if (isZoneEnabled("yavin4")) then
	--	self:spawnMobiles()
	--end
end

function jediKnightTrialsEleven:spawnMobiles()
	--local pAcklay = spawnMobile("yavin4", "acklay",7200,101.1,-34.3,-321.6,-136,1627823) --Randomized respawn
	--createObserver(OBJECTDESTRUCTION, "jediKnightTrialsEleven", "acklayKilled", pAcklay)
end


function jediKnightTrialsEleven:acklayKilled(pAcklay, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsEleven")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsEleven")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsEleven.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsEleven")
					
					if(hasState == true) then 
	
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, jediKnightTrialsEleven.questString)
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsEleven:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsEleven:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_eleven_convo_handler = Object:new {
	
 }

function jedi_knight_trials_eleven_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsEleven")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsEleven")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsEleven")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsTen")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_eleven_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsEleven.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Quest Location", "", -6944, 6554, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Sith Acolyte", "Kill the Sith Acolyte", -6944, 6554, 5, true, true, 0)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsEleven.questString)

		player:awardExperience("force_rank_xp", 10)

	end
	


	return conversationScreen
end

