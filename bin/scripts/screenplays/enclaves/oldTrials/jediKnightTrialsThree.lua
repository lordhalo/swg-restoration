local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsThree = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsThree",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	stintaril = {
		{"stintaril_prowler", 300, -6405, 787, 1868, 1, 0},
		{"stintaril_prowler", 300, -6378, 782, 1866, 1, 0},
		{"stintaril_prowler", 300, -6379, 783, 1851, 1, 0},
		{"stintaril_prowler", 300, -6296, 770, 1772, 1, 0},
		{"stintaril_prowler", 300, -6302, 769, 1764, 1, 0},
		{"stintaril_prowler", 300, -6313, 768, 1769, 1, 0},
		{"stintaril_prowler", 300, -6438, 791, 1685, 1, 0},
		{"stintaril_prowler", 300, -6448, 791, 1690, 1, 0},
		{"stintaril_prowler", 300, -6449, 784, 1710, 1, 0},
		{"stintaril_prowler", 300, -6503, 788, 1821, 1, 0},
		{"stintaril_prowler", 300, -6511, 791, 1815, 1, 0},
		{"stintaril_prowler", 300, -6511, 791, 1825, 1, 0},
		{"stintaril_prowler", 300, -6412, 783, 1780, 1, 0},
		{"stintaril_prowler", 300, -6410, 783, 1782, 1, 0},
	},
}
registerScreenPlay("jediKnightTrialsThree", true)

function jediKnightTrialsThree:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsThree:spawnMobiles()
		local mobileTable = self.stintaril
		for i = 1, table.getn(mobileTable), 1 do
			local pStintaril = spawnMobile("yavin4", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pStintaril):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsThree", "stintarilDead", pStintaril)
		end
end

function jediKnightTrialsThree:stintarilDead(pStintaril, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsThree")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsThree")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":stintarilKilled", readData(groupMember:getObjectID() .. ":stintarilKilled") + 1)							if(readData(groupMember:getObjectID() .. ":stintarilKilled") > 8) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":stintarilKilled") .. "/ 8")							end
			
						if(readData(groupMember:getObjectID() .. ":stintarilKilled") == 8) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsThree.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsThree")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":stintarilKilled", readData(player:getObjectID() .. ":stintarilKilled") + 1)
					
						if(readData(player:getObjectID() .. ":stintarilKilled") > 8) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":stintarilKilled") .. "/ 8")	
						end
						if(readData(player:getObjectID() .. ":stintarilKilled") == 8) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsThree.questString)
						end
				end
			end
		end)
	return 0
end


--Setup

function jediKnightTrialsThree:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsThree:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_three_convo_handler = Object:new {
	
 }

function jedi_knight_trials_three_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsThree")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsThree")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsThree")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsTwo")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_three_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsThree.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsThree.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

