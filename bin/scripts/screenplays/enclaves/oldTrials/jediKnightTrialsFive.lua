local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsFive = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsFive",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	kimogila = {
		{"enraged_kimogila", 600, 3, 11, 2575, 1, 0},
		{"enraged_kimogila", 600, -19, 12, 2802, 1, 0},
		{"enraged_kimogila", 600, -151, 12, 2750, 1, 0},

	},
}
registerScreenPlay("jediKnightTrialsFive", true)

function jediKnightTrialsFive:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsFive:spawnMobiles()
		local mobileTable = self.kimogila
		for i = 1, table.getn(mobileTable), 1 do
			local pKimogila = spawnMobile("lok", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pKimogila):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsFive", "kimogilaDead", pKimogila)
		end
end

function jediKnightTrialsFive:kimogilaDead(pKimogila, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsFive")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsFive")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":kimogilaKilled", readData(groupMember:getObjectID() .. ":kimogilaKilled") + 1)							if(readData(groupMember:getObjectID() .. ":kimogilaKilled") > 3) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":kimogilaKilled") .. "/ 3")							end
			
						if(readData(groupMember:getObjectID() .. ":kimogilaKilled") == 3) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsFive.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsFive")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":kimogilaKilled", readData(player:getObjectID() .. ":kimogilaKilled") + 1)
					
						if(readData(player:getObjectID() .. ":kimogilaKilled") > 3) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":kimogilaKilled") .. "/ 3")	
						end
						if(readData(player:getObjectID() .. ":kimogilaKilled") == 3) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsFive.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsFive:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsFive:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_five_convo_handler = Object:new {
	
 }

function jedi_knight_trials_five_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsFive")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsFive")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsFive")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsFour")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_five_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsFive.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsFive.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

