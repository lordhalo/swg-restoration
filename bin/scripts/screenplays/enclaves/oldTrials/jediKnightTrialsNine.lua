local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsNine = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsNine",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	groupOneNormal = {
		{"dark_trooper", 0, -2216, 20, 2368, 155, 0},
		{"stormtrooper_medic", 0, -2218, 20, 2368, 155, 0},
		{"stormtrooper_rifleman", 0, -2220, 20, 2368, 155, 0},
		{"stormtrooper", 0, -2222, 20, 2368, 155, 0},
		{"stormtrooper", 0, -2224, 20, 2368, 155, 0},
		{"dark_trooper", 0, -2216, 20, 2372, 155, 0},
		{"stormtrooper", 0, -2218, 20, 2372, 155, 0},
		{"stormtrooper_rifleman", 0, -2220, 20, 2372, 155, 0},
		{"stormtrooper", 0, -2222, 20, 2372, 155, 0},
		{"stormtrooper", 0, -2224, 20, 2372, 155, 0},

	},

	groupOneBoss = {
		{"dark_trooper", 0, -2216, 20, 2368, 155, 0},
		{"stormtrooper_medic", 0, -2218, 20, 2368, 155, 0},
		{"stormtrooper_rifleman", 0, -2220, 20, 2368, 155, 0},
		{"dark_trooper", 0, -2222, 20, 2368, 155, 0},
		{"dark_trooper", 0, -2224, 20, 2368, 155, 0},
		{"dark_trooper", 0, -2216, 20, 2372, 155, 0},
		{"dark_trooper", 0, -2218, 20, 2372, 155, 0},
		{"stormtrooper_rifleman", 0, -2220, 20, 2372, 155, 0},
		{"dark_trooper", 0, -2222, 20, 2372, 155, 0},
		{"dark_trooper", 0, -2224, 20, 2372, 155, 0},

	},

	groupTwoNormal = {
		{"dark_trooper", 0, -2070, 20, 2360, 155, 0},
		{"stormtrooper_medic", 0, -2072, 20, 2360, 155, 0},
		{"stormtrooper_rifleman", 0, -2074, 20, 2360, 155, 0},
		{"stormtrooper", 0, -2076, 20, 2360, 155, 0},
		{"stormtrooper", 0, -2078, 20, 2360, 155, 0},
		{"dark_trooper", 0, -2070, 20, 2363, 155, 0},
		{"stormtrooper", 0, -2072, 20, 2363, 155, 0},
		{"stormtrooper_rifleman", 0, -2074, 20, 2363, 155, 0},
		{"stormtrooper", 0, -2076, 20, 2363, 155, 0},
		{"stormtrooper", 0, -2078, 20, 2363, 155, 0},

	},

	groupTwoBoss = {
		{"dark_trooper", 0, -2070, 20, 2360, 155, 0},
		{"stormtrooper_medic", 0, -2072, 20, 2360, 155, 0},
		{"stormtrooper_rifleman", 0, -2074, 20, 2360, 155, 0},
		{"dark_trooper", 0, -2076, 20, 2360, 155, 0},
		{"dark_trooper", 0, -2078, 20, 2360, 155, 0},
		{"dark_trooper", 0, -2070, 20, 2363, 155, 0},
		{"dark_trooper", 0, -2072, 20, 2363, 155, 0},
		{"stormtrooper_rifleman", 0, -2074, 20, 2363, 155, 0},
		{"dark_trooper", 0, -2076, 20, 2363, 155, 0},
		{"dark_trooper", 0, -2078, 20, 2363, 155, 0},

	},

	groupThreeNormal = {
		{"dark_trooper", 0, -2600, 9, 2226, 155, 0},
		{"stormtrooper_medic", 0, -2602, 9, 2226, 155, 0},
		{"stormtrooper_rifleman", 0, -2604, 9, 2226, 155, 0},
		{"stormtrooper", 0, -2606, 9, 2226, 155, 0},
		{"stormtrooper", 0, -2608, 9, 2226, 155, 0},
		{"stormtrooper", 0, -2610, 9, 2226, 155, 0},
		{"stormtrooper", 0, -2612, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2600, 9, 2229, 155, 0},
		{"stormtrooper", 0, -2602, 9, 2229, 155, 0},
		{"stormtrooper_rifleman", 0, -2604, 9, 2229, 155, 0},
		{"stormtrooper", 0, -2606, 9, 2229, 155, 0},
		{"stormtrooper", 0, -2608, 9, 2229, 155, 0},
		{"stormtrooper", 0, -2610, 9, 2229, 155, 0},
		{"stormtrooper", 0, -2612, 9, 2229, 155, 0},

	},

	groupThreeBoss = {
		{"dark_trooper", 0, -2600, 9, 2226, 155, 0},
		{"stormtrooper_medic", 0, -2602, 9, 2226, 155, 0},
		{"stormtrooper_rifleman", 0, -2604, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2606, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2608, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2610, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2612, 9, 2226, 155, 0},
		{"dark_trooper", 0, -2600, 9, 2229, 155, 0},
		{"dark_trooper", 0, -2602, 9, 2229, 155, 0},
		{"stormtrooper_rifleman", 0, -2604, 9, 2229, 155, 0},
		{"dark_trooper", 0, -2606, 9, 2229, 155, 0},
		{"dark_trooper", 0, -2608, 9, 2229, 155, 0},
		{"dark_trooper", 0, -2610, 9, 2229, 155, 0},
		{"dark_trooper", 0, -2612, 9, 2229, 155, 0},
	},

	groupFourNormal = {
		{"dark_trooper", 0, -2740, 9, 2555, 155, 0},
		{"stormtrooper_medic", 0, -2742, 9, 2555, 155, 0},
		{"stormtrooper_rifleman", 0, -2744, 9, 2555, 155, 0},
		{"stormtrooper", 0, -2746, 9, 2555, 155, 0},
		{"stormtrooper", 0, -2748, 9, 2555, 155, 0},
		{"stormtrooper", 0, -2750, 9, 2555, 155, 0},
		{"stormtrooper", 0, -2752, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2740, 9, 2559, 155, 0},
		{"stormtrooper", 0, -2742, 9, 2559, 155, 0},
		{"stormtrooper_rifleman", 0, -2744, 9, 2559, 155, 0},
		{"stormtrooper", 0, -2746, 9, 2559, 155, 0},
		{"stormtrooper", 0, -2748, 9, 2559, 155, 0},
		{"stormtrooper", 0, -2750, 9, 2559, 155, 0},
		{"stormtrooper", 0, -2752, 9, 2559, 155, 0},

	},

	groupFourBoss = {
		{"dark_trooper", 0, -2740, 9, 2555, 155, 0},
		{"stormtrooper_medic", 0, -2742, 9, 2555, 155, 0},
		{"stormtrooper_rifleman", 0, -2744, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2746, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2748, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2750, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2752, 9, 2555, 155, 0},
		{"dark_trooper", 0, -2740, 9, 2559, 155, 0},
		{"dark_trooper", 0, -2742, 9, 2559, 155, 0},
		{"stormtrooper_rifleman", 0, -2744, 9, 2559, 155, 0},
		{"dark_trooper", 0, -2746, 9, 2559, 155, 0},
		{"dark_trooper", 0, -2748, 9, 2559, 155, 0},
		{"dark_trooper", 0, -2750, 9, 2559, 155, 0},
		{"dark_trooper", 0, -2752, 9, 2559, 155, 0},
	},

	groupFiveNormal = {
		{"dark_trooper", 0, -2300, 5, 2100, 155, 0},
		{"stormtrooper_medic", 0, -2302, 5, 2100, 155, 0},
		{"stormtrooper_rifleman", 0, -2304, 5, 2100, 155, 0},
		{"stormtrooper", 0, -2306, 5, 2100, 155, 0},
		{"stormtrooper", 0, -2308, 5, 2100, 155, 0},
		{"stormtrooper", 0, -2310, 5, 2100, 155, 0},
		{"stormtrooper", 0, -2312, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2300, 5, 2104, 155, 0},
		{"stormtrooper", 0, -2302, 5, 2104, 155, 0},
		{"stormtrooper_rifleman", 0, -2304, 5, 2104, 155, 0},
		{"stormtrooper", 0, -2306, 5, 2104, 155, 0},
		{"stormtrooper", 0, -2308, 5, 2104, 155, 0},
		{"stormtrooper", 0, -2310, 5, 2104, 155, 0},
		{"stormtrooper", 0, -2312, 5, 2104, 155, 0},

	},

	groupFiveBoss = {
		{"dark_trooper", 0, -2300, 5, 2100, 155, 0},
		{"stormtrooper_medic", 0, -2302, 5, 2100, 155, 0},
		{"stormtrooper_rifleman", 0, -2304, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2306, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2308, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2310, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2312, 5, 2100, 155, 0},
		{"dark_trooper", 0, -2300, 5, 2104, 155, 0},
		{"dark_trooper", 0, -2302, 5, 2104, 155, 0},
		{"stormtrooper_rifleman", 0, -2304, 5, 2104, 155, 0},
		{"dark_trooper", 0, -2306, 5, 2104, 155, 0},
		{"dark_trooper", 0, -2308, 5, 2104, 155, 0},
		{"dark_trooper", 0, -2310, 5, 2104, 155, 0},
		{"dark_trooper", 0, -2312, 5, 2104, 155, 0},
	},

}
registerScreenPlay("jediKnightTrialsNine", true)

function jediKnightTrialsNine:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
		self:spawnMobileTwo()
		self:spawnMobileThree()
		self:spawnMobileFour()
		self:spawnMobileFive()
	end
end

function jediKnightTrialsNine:spawnMobiles()

		local random = math.random(100)

		local mobileTable = self.groupOneNormal

		if random >= 80 then
			mobileTable = self.groupOneNormal
		elseif random < 80 then
			mobileTable = self.groupOneBoss
			local pGeneral = spawnMobile("talus", "imperial_general", 0, -2220, 20, 2375, 155, 0) -- quest giver
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneral)
		else
			mobileTable = self.groupOneNormal
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			--AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "fillerDead", pCommando)
		end
end

function jediKnightTrialsNine:spawnMobileTwo()

		local random = math.random(100)

		local mobileTable = self.groupTwoNormal

		if random >= 80 then
			mobileTable = self.groupTwoNormal
		elseif random < 80 then
			mobileTable = self.groupTwoBoss
			local pGeneral = spawnMobile("talus", "imperial_general", 0, -2075, 20, 2365, 155, 0) -- quest giver
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneral)
		else
			mobileTable = self.groupTwoNormal
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			--AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "fillerTwoDead", pCommando)
		end
end

function jediKnightTrialsNine:spawnMobileThree()

		local random = math.random(100)

		local mobileTable = self.groupThreeNormal

		if random >= 80 then
			mobileTable = self.groupThreeNormal
		elseif random < 80 then
			mobileTable = self.groupThreeBoss
			local pGeneral = spawnMobile("talus", "imperial_general", 0, -2608, 8, 2235, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneral)
			local pGeneralTwo = spawnMobile("talus", "imperial_general", 0, -2605, 8, 2235, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneralTwo)
		else
			mobileTable = self.groupThreeNormal
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "fillerThreeDead", pCommando)
		end
end

function jediKnightTrialsNine:spawnMobileFour()

		local random = math.random(100)

		local mobileTable = self.groupFourNormal

		if random >= 80 then
			mobileTable = self.groupFourNormal
		elseif random < 80 then
			mobileTable = self.groupFourBoss
			local pGeneral = spawnMobile("talus", "imperial_general", 0, -2730, 8, 2545, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneral)
			local pGeneralTwo = spawnMobile("talus", "imperial_general", 0, -2734, 8, 2545, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneralTwo)
		else
			mobileTable = self.groupFourNormal
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "fillerFourDead", pCommando)
		end
end

function jediKnightTrialsNine:spawnMobileFive()

		local random = math.random(100)

		local mobileTable = self.groupFiveNormal

		if random >= 80 then
			mobileTable = self.groupFiveNormal
		elseif random < 80 then
			mobileTable = self.groupFiveBoss
			local pGeneral = spawnMobile("talus", "imperial_general", 0, -2300, 5, 2108, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneral)
			local pGeneralTwo = spawnMobile("talus", "imperial_general", 0, -2300, 5, 2110, 0, 0)
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "generalDead", pGeneralTwo)
		else
			mobileTable = self.groupFiveNormal
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsNine", "fillerFiveDead", pCommando)
		end
end

function jediKnightTrialsNine:fillerDead(pCommando, playerObject)

	writeData("trialNine:fillerDead", readData("trialNine:fillerDead") + 1)

	if(readData("trialNine:fillerDead") == 10) then
		createEvent(600000, "jediKnightTrialsNine", "spawnMobiles", pCommando)
	end
	
	return 0
end

function jediKnightTrialsNine:fillerTwoDead(pCommando, playerObject)
	
	writeData("trialNine:fillerTwoDead", readData("trialNine:fillerTwoDead") + 1)

	if(readData("trialNine:fillerTwoDead") == 10) then
		createEvent(600000, "jediKnightTrialsNine", "spawnMobileTwo", pCommando)
	end
	
	return 0
end

function jediKnightTrialsNine:fillerThreeDead(pCommando, playerObject)
	
	writeData("trialNine:fillerThreeDead", readData("trialNine:fillerThreeDead") + 1)

	if(readData("trialNine:fillerThreeDead") == 14) then
		createEvent(600000, "jediKnightTrialsNine", "spawnMobileThree", pCommando)
	end
	
	return 0
end

function jediKnightTrialsNine:fillerFourDead(pCommando, playerObject)
	
	writeData("trialNine:fillerFourDead", readData("trialNine:fillerFourDead") + 1)

	if(readData("trialNine:fillerFourDead") == 14) then
		createEvent(600000, "jediKnightTrialsNine", "spawnMobileFour", pCommando)
	end
	
	return 0
end

function jediKnightTrialsNine:fillerFiveDead(pCommando, playerObject)
	
	writeData("trialNine:fillerFiveDead", readData("trialNine:fillerFiveDead") + 1)

	if(readData("trialNine:fillerFiveDead") == 14) then
		createEvent(600000, "jediKnightTrialsNine", "spawnMobileFive", pCommando)
	end
	
	return 0
end

function jediKnightTrialsNine:generalDead(pGeneral, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsNine")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsNine")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":generalKilled", readData(groupMember:getObjectID() .. ":generalKilled") + 1)				
						if(readData(groupMember:getObjectID() .. ":generalKilled") > 22) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":generalKilled") .. "/ 22")						end
			
						if(readData(groupMember:getObjectID() .. ":generalKilled") == 22) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsNine.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsNine")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":generalKilled", readData(player:getObjectID() .. ":generalKilled") + 1)

						if(readData(player:getObjectID() .. ":commandoKilled") > 22) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":generalKilled") .. "/ 22")						end

						if(readData(player:getObjectID() .. ":generalKilled") == 22) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsNine.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsNine:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsNine:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_nine_convo_handler = Object:new {
	
 }

function jedi_knight_trials_nine_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsNine")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsNine")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsNine")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsEight")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_nine_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsNine.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Quest Location", "", -6944, 6554, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Sith Acolyte", "Kill the Sith Acolyte", -6944, 6554, 5, true, true, 0)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsNine.questString)

		player:awardExperience("force_rank_xp", 10)

	end
	


	return conversationScreen
end

