local ObjectManager = require("managers.object.object_manager")

sithKnightTrialsOne = ScreenPlay:new {
	numberOfActs = 1,
	questString = "sithKnightTrialsOne",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

}
registerScreenPlay("sithKnightTrialsOne", true)

function sithKnightTrialsOne:start()
	--if (isZoneEnabled("yavin4")) then
	--end
end


function sithKnightTrialsOne:tuskenDead(pRancor, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "sithKnightTrialsOne")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "sithKnightTrialsOne")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":tuskenKilled", readData(groupMember:getObjectID() .. ":tuskenKilled") + 1)							if(readData(groupMember:getObjectID() .. ":tuskenKilled") > 19) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":tuskenKilled") .. "/ 19")							end
			
						if(readData(groupMember:getObjectID() .. ":tuskenKilled") == 19) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, sithKnightTrialsOne.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "sithKnightTrialsOne")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":tuskenKilled", readData(player:getObjectID() .. ":tuskenKilled") + 1)
					
						if(readData(player:getObjectID() .. ":tuskenKilled") > 19) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":tuskenKilled") .. "/ 19")	
						end
						if(readData(player:getObjectID() .. ":tuskenKilled") == 19) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, sithKnightTrialsOne.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function sithKnightTrialsOne:getActivePlayerName()
	return self.questdata.activePlayerName
end

function sithKnightTrialsOne:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

sith_knight_trials_one_convo_handler = Object:new {
	
 }

function sith_knight_trials_one_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "sithKnightTrialsOne")
			local questTurnin = creature:hasScreenPlayState(4, "sithKnightTrialsOne")
			local questComplete = creature:hasScreenPlayState(8, "sithKnightTrialsOne")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function sith_knight_trials_one_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, sithKnightTrialsOne.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, sithKnightTrialsOne.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

