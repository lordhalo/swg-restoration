local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsFour = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsFour",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	blurrg = {
		{"blurrg_raptor", 300, -5111, 21, 2112, 1, 0},
		{"blurrg_raptor", 300, -5657, 9, 2133, 1, 0},
		{"blurrg_raptor", 300, -5464, 7, 1713, 1, 0},
		{"blurrg_raptor", 300, -5391, 7, 1289, 1, 0},
		{"blurrg_raptor", 300, -5287, 19, 962, 1, 0},
		{"blurrg_raptor", 300, -4925, 8, 1059, 1, 0},
		{"blurrg_raptor", 300, -4333, 32, 1143, 1, 0},
		{"blurrg_raptor", 300, -4627, 11, 1382, 1, 0},

	},
}
registerScreenPlay("jediKnightTrialsFour", true)

function jediKnightTrialsFour:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsFour:spawnMobiles()
		local mobileTable = self.blurrg
		for i = 1, table.getn(mobileTable), 1 do
			local pBlurrg = spawnMobile("endor", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pBlurrg):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsFour", "blurrgDead", pBlurrg)
		end
end

function jediKnightTrialsFour:blurrgDead(pBlurrg, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsFour")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsFour")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":blurrgKilled", readData(groupMember:getObjectID() .. ":blurrgKilled") + 1)							if(readData(groupMember:getObjectID() .. ":blurrgKilled") > 3) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":blurrgKilled") .. "/ 3")							end
			
						if(readData(groupMember:getObjectID() .. ":blurrgKilled") == 3) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsFour.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsFour")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":blurrgKilled", readData(player:getObjectID() .. ":blurrgKilled") + 1)
					
						if(readData(player:getObjectID() .. ":blurrgKilled") > 3) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":blurrgKilled") .. "/ 3")	
						end
						if(readData(player:getObjectID() .. ":blurrgKilled") == 3) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsFour.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsFour:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsFour:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_four_convo_handler = Object:new {
	
 }

function jedi_knight_trials_four_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsFour")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsFour")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsFour")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialThreeComplete = creature:hasScreenPlayState(8, "jediKnightTrialsThree")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialThreeComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_four_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsFour.questString)
		local pGhost = player:getPlayerObject()
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsFour.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

