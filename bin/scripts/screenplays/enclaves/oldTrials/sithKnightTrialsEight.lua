local ObjectManager = require("managers.object.object_manager")

sithKnightTrialsEight = ScreenPlay:new {
	numberOfActs = 1,
	questString = "sithKnightTrialsEight",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	commandoOne = { --Weapons Depo
		{"rebel_commando", 900, -4910, 65, -3086, 1, 0},
		{"rebel_commando", 900, -4912, 65, -3086, 1, 0},
		{"rebel_commando", 900, -4914, 65, -3086, 1, 0},
		{"rebel_commando", 900, -4916, 65, -3086, 1, 0},
		{"rebel_commando", 900, -4918, 65, -3086, 1, 0},
		{"rebel_commando", 900, -4910, 65, -3088, 1, 0},
		{"rebel_commando", 900, -4912, 65, -3088, 1, 0},
		{"rebel_commando", 900, -4914, 65, -3088, 1, 0},
		{"rebel_commando", 900, -4916, 65, -3088, 1, 0},
		{"rebel_commando", 900, -4918, 65, -3088, 1, 0},
	},

	commandoTwo = { --Reb OP
		{"rebel_commando", 900, -2220, 20, 2228, 1, 0},
		{"rebel_commando", 900, -2222, 20, 2228, 1, 0},
		{"rebel_commando", 900, -2224, 20, 2228, 1, 0},
		{"rebel_commando", 900, -2226, 20, 2228, 1, 0},
		{"rebel_commando", 900, -2228, 20, 2228, 1, 0},
		{"rebel_commando", 900, -2220, 20, 2232, 1, 0},
		{"rebel_commando", 900, -2222, 20, 2232, 1, 0},
		{"rebel_commando", 900, -2224, 20, 2232, 1, 0},
		{"rebel_commando", 900, -2226, 20, 2232, 1, 0},
		{"rebel_commando", 900, -2228, 20, 2232, 1, 0},
	},

	commandoThree = { --Weapons Depo
		{"rebel_commando", 900, -4845, 50, -3270, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3268, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3266, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3264, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3262, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3260, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3258, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3256, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3254, 1, 0},
		{"rebel_commando", 900, -4845, 50, -3252, 1, 0},
	},

	commandoFour = { --Weapons Depo
		{"rebel_commando", 900, -4988, 24, -3332, 1, 0},
		{"rebel_commando", 900, -4988, 24, -3332, 1, 0},
		{"rebel_commando", 900, -4988, 24, -3332, 1, 0},
		{"rebel_commando", 900, -4988, 24, -3332, 1, 0},
		{"rebel_commando", 900, -4988, 24, -3332, 1, 0}
	},

	commandoFive = { --Weapons Depo
		{"rebel_commando", 900, -4870, 27, -3182, 1, 0},
		{"rebel_commando", 900, -4870, 27, -3182, 1, 0},
		{"rebel_commando", 900, -4870, 27, -3182, 1, 0},
		{"rebel_commando", 900, -4870, 27, -3182, 1, 0},
		{"rebel_commando", 900, -4870, 27, -3182, 1, 0}
	},

	commandoSix = { --Weapons Depo
		{"rebel_commando", 900, -4830, 36, -3113, 1, 0},
		{"rebel_commando", 900, -4830, 36, -3113, 1, 0},
		{"rebel_commando", 900, -4830, 36, -3113, 1, 0},
		{"rebel_commando", 900, -4830, 36, -3113, 1, 0},
		{"rebel_commando", 900, -4830, 36, -3113, 1, 0}
	},

}
registerScreenPlay("sithKnightTrialsEight", true)

function sithKnightTrialsEight:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
		--self:spawnMobilesTwo()
		self:spawnMobilesThree()
		self:spawnMobilesFour()
		self:spawnMobilesFive()
		self:spawnMobilesSix()
	end
end

function sithKnightTrialsEight:spawnMobiles()

		local mobileTable = self.commandoOne

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:spawnMobilesTwo()

		local mobileTable = self.commandoTwo

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])

			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:spawnMobilesThree()

		local mobileTable = self.commandoThree

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:spawnMobilesFour()

		local mobileTable = self.commandoFour

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:spawnMobilesFive()

		local mobileTable = self.commandoFive

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:spawnMobilesSix()

		local mobileTable = self.commandoSix

		for i = 1, table.getn(mobileTable), 1 do
			local pCommando = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pCommando):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsEight", "commandoKilled", pCommando)
		end
end

function sithKnightTrialsEight:commandoKilled(pGraul, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "sithKnightTrialsEight")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "sithKnightTrialsEight")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":commandoKilled", readData(groupMember:getObjectID() .. ":commandoKilled") + 1)
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":commandoKilled") .. "/ 47")						
			
						if(readData(groupMember:getObjectID() .. ":commandoKilled") == 47) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, sithKnightTrialsEight.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "sithKnightTrialsEight")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":commandoKilled", readData(player:getObjectID() .. ":commandoKilled") + 1)	
						player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":commandoKilled") .. "/ 47")	

						if(readData(player:getObjectID() .. ":commandoKilled") == 47) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, sithKnightTrialsEight.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function sithKnightTrialsEight:getActivePlayerName()
	return self.questdata.activePlayerName
end

function sithKnightTrialsEight:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

sith_knight_trials_eight_convo_handler = Object:new {
	
 }

function sith_knight_trials_eight_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "sithKnightTrialsEight")
			local questTurnin = creature:hasScreenPlayState(4, "sithKnightTrialsEight")
			local questComplete = creature:hasScreenPlayState(8, "sithKnightTrialsEight")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "sithKnightTrialsSeven")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function sith_knight_trials_eight_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, sithKnightTrialsEight.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Quest Location", "", -6944, 6554, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Sith Acolyte", "Kill the Sith Acolyte", -6944, 6554, 5, true, true, 0)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, sithKnightTrialsEight.questString)

		player:awardExperience("force_rank_xp", 10)

	end
	


	return conversationScreen
end

