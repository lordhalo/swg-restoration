local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsOne = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsOne",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	tuskens = {
		{"tusken_raider", 300, -2913, 5, 2611, 1, 0},
		{"tusken_raider", 300, -2873, 5, 2617, 1, 0},
		{"tusken_raider", 300, -2791, 5, 2665, 1, 0},
		{"tusken_raider", 300, -2779, 5, 2583, 1, 0},
		{"tusken_raider", 300, -2803, 5, 2495, 1, 0},
		{"tusken_raider", 300, -2917, 5, 2576, 1, 0},
		{"tusken_raider", 300, -3086, 5, 2487, 1, 0},
		{"tusken_raider", 300, -3115, 5, 2403, 1, 0},
		{"tusken_raider", 300, -3175, 5, 2341, 1, 0},
		{"tusken_raider", 300, -3178, 5, 2247, 1, 0},
		{"tusken_raider", 300, -3037, 5, 2353, 1, 0},
		{"tusken_raider", 300, -3212, 5, 2388, 1, 0},
		{"tusken_raider", 300, -3204, 5, 2487, 1, 0},
		{"tusken_raider", 300, -2913, 5, 2611, 1, 0},
		{"tusken_raider", 300, -2873, 5, 2617, 1, 0},
		{"tusken_raider", 300, -2791, 5, 2665, 1, 0},
		{"tusken_raider", 300, -2779, 5, 2583, 1, 0},
		{"tusken_raider", 300, -2803, 5, 2495, 1, 0},
		{"tusken_raider", 300, -2917, 5, 2576, 1, 0},
		{"tusken_raider", 300, -3086, 5, 2487, 1, 0},
		{"tusken_raider", 300, -3115, 5, 2403, 1, 0},
		{"tusken_raider", 300, -3175, 5, 2341, 1, 0},
		{"tusken_raider", 300, -3178, 5, 2247, 1, 0},
		{"tusken_raider", 300, -3037, 5, 2353, 1, 0},
		{"tusken_raider", 300, -3212, 5, 2388, 1, 0},
		{"tusken_raider", 300, -3204, 5, 2487, 1, 0},
	},
}
registerScreenPlay("jediKnightTrialsOne", true)

function jediKnightTrialsOne:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
		self:spawnQuestGiver()
	end
end

function jediKnightTrialsOne:spawnQuestGiver()
	spawnMobile("yavin4", "jedi_knight_one", 0, -17.3, -22.1, 10.7, 126, 8525418) -- quest giver
end

function jediKnightTrialsOne:spawnMobiles()
		local mobileTable = self.tuskens
		for i = 1, table.getn(mobileTable), 1 do
			local pTusken = spawnMobile("tatooine", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pTusken):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsOne", "tuskenDead", pTusken)
			createObserver(OBJECTDESTRUCTION, "sithKnightTrialsOne", "tuskenDead", pTusken)
		end
end

function jediKnightTrialsOne:tuskenDead(pRancor, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsOne")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsOne")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":tuskenKilled", readData(groupMember:getObjectID() .. ":tuskenKilled") + 1)							if(readData(groupMember:getObjectID() .. ":tuskenKilled") > 19) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":tuskenKilled") .. "/ 19")							end
			
						if(readData(groupMember:getObjectID() .. ":tuskenKilled") == 19) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsOne.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsOne")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":tuskenKilled", readData(player:getObjectID() .. ":tuskenKilled") + 1)
					
						if(readData(player:getObjectID() .. ":tuskenKilled") > 19) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":tuskenKilled") .. "/ 19")	
						end
						if(readData(player:getObjectID() .. ":tuskenKilled") == 19) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsOne.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsOne:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsOne:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_one_convo_handler = Object:new {
	
 }

function jedi_knight_trials_one_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsOne")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsOne")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsOne")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_one_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsOne.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsOne.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

