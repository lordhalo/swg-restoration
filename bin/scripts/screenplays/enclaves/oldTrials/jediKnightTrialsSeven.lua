local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsSeven = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsSeven",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	graul = {
		{"graul_marauder", 0, -3717, 2, -2880, 1, 0},
		{"graul", 0, -3717, 2, -2882, 1, 0},
		{"graul", 0, -3717, 2, -2884, 1, 0},

	},

	graultwo = {
		{"graul_marauder", 0, -2604, 1, -2781, 1, 0},
		{"graul", 0, -2604, 1, -2783, 1, 0},
		{"graul", 0, -2604, 1, -2785, 1, 0},

	},

	graulthree = {
		{"graul_marauder", 0, -2151, 2, -4760, 1, 0},
		{"graul", 0, -2151, 1, -4762, 1, 0},
		{"graul", 0, -2151, 1, -4764, 1, 0},

	},

	graulfour = {
		{"graul_marauder", 0, -4990, 0, -5168, 1, 0},
		{"graul", 0, -4990, 0, -5170, 1, 0},
		{"graul", 0, -4990, 0, -5172, 1, 0},

	},

	graulfive = {
		{"graul_marauder", 0, -4986, 0, -4162, 1, 0},
		{"graul", 0, -4986, 0, -4164, 1, 0},
		{"graul", 0, -4986, 0, -4166, 1, 0},

	},

	graulsix = {
		{"graul_marauder", 0, -3314, 2, -4899, 1, 0},
		{"graul", 0, -3314, 2, -4897, 1, 0},
		{"graul", 0, -3314, 2, -4895, 1, 0},

	},

	graulseven = {
		{"graul_marauder", 0, -4804, 1, -5036, 1, 0},
		{"graul", 0, -4804, 2, -5038, 1, 0},
		{"graul", 0, -4804, 2, -5040, 1, 0},

	},

}
registerScreenPlay("jediKnightTrialsSeven", true)

function jediKnightTrialsSeven:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsSeven:spawnMobiles()

		local random = math.random(100)

		local mobileTable = self.graul

		if random >= 15 then
			mobileTable = self.graul
		elseif random < 30 then
			mobileTable = self.graultwo
		elseif random < 45 then
			mobileTable = self.graulthree
		elseif random < 60 then
			mobileTable = self.graulfour
		elseif random < 75 then
			mobileTable = self.graulfive
		elseif random < 90 then
			mobileTable = self.graulsix
		else
			mobileTable = self.graulseven
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pGraul = spawnMobile("dantooine", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pGraul):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsSeven", "graulKilled", pGraul)
		end
end


function jediKnightTrialsSeven:graulKilled(pGraul, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsSeven")

	createEvent(900000, "jediKnightTrialsSeven", "spawnMobiles", pGraul)
	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsSeven")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":graulKilled", readData(groupMember:getObjectID() .. ":graulKilled") + 1)
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":graulKilled") .. "/ 6")						
			
						if(readData(groupMember:getObjectID() .. ":graulKilled") == 6) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsSeven.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsSeven")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":graulKilled", readData(player:getObjectID() .. ":graulKilled") + 1)	
						player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":graulKilled") .. "/ 6")	

						if(readData(player:getObjectID() .. ":graulKilled") == 6) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsSeven.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsSeven:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsSeven:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_seven_convo_handler = Object:new {
	
 }

function jedi_knight_trials_seven_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsSeven")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsSeven")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsSeven")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsSix")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_seven_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsSeven.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsSeven.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

