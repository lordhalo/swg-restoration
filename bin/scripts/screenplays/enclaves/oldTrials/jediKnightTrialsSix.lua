local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsSix = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsSix",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	peko = {
		{"peko_peko_albatross", 0, -5932, -197, 6435, 1, 0},

	},

	pekotwo = {
		{"peko_peko_albatross", 0, -6602, -194, 5335, 1, 0},

	},

	pekothree = {
		{"peko_peko_albatross", 0, -4738, -194, 5363, 1, 0},

	},

	pekofour = {
		{"peko_peko_albatross", 0, -3597, -209, 5568, 1, 0},

	},
}
registerScreenPlay("jediKnightTrialsSix", true)

function jediKnightTrialsSix:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsSix:spawnMobiles()

		local random = math.random(100)

		local mobileTable = self.peko

		if random >= 25 then
			mobileTable = self.peko
		elseif random < 50 then
			mobileTable = self.pekotwo
		elseif random < 75 then
			mobileTable = self.pekothree
		else
			mobileTable = self.pekofour
		end

		for i = 1, table.getn(mobileTable), 1 do
			local pPeko = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pPeko):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsSix", "pekoKilled", pPeko)
		end
end


function jediKnightTrialsSix:pekoKilled(pPeko, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsSix")

	createEvent(900000, "jediKnightTrialsSix", "spawnMobiles", pPeko)
	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsSix")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							--groupMember:sendSystemMessage("In Range")
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							--groupMember:sendSystemMessage("PekoPeko" + String:)
							
							writeData(groupMember:getObjectID() .. ":pekoKilled", readData(groupMember:getObjectID() .. ":pekoKilled") + 1)	
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":pekoKilled") .. "/ 2")						
			
						if(readData(groupMember:getObjectID() .. ":pekoKilled") == 2) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsSix.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsSix")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":pekoKilled", readData(player:getObjectID() .. ":pekoKilled") + 1)	
						player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":pekoKilled") .. "/ 2")	

						if(readData(player:getObjectID() .. ":pekoKilled") == 2) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsSix.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsSix:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsSix:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_six_convo_handler = Object:new {
	
 }

function jedi_knight_trials_six_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsSix")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsSix")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsSix")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsFive")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_six_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsSix.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsSix.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

