local ObjectManager = require("managers.object.object_manager")

jediKnightTrialsTwo = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediKnightTrialsTwo",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	tuskens = {
		{"ancient_bull_rancor", 1500, 397, 85, -4775, 1, 0},
		{"ancient_bull_rancor", 1500, 823, 83, -4019, 1, 0},
		{"ancient_bull_rancor", 1500, 820, 77, -4915, 1, 0},
		{"ancient_bull_rancor", 1500, 2114, 86, -4751, 1, 0},
		{"ancient_bull_rancor", 1500, 1651, 78, -5463, 1, 0},
		{"ancient_bull_rancor", 1500, 1769, 35, -5034, 1, 0},
		{"ancient_bull_rancor", 1500, -115, 47, -5636, 1, 0},
		{"ancient_bull_rancor", 1500, -596, 40, -5286, 1, 0},
		{"ancient_bull_rancor", 1500, -620, 39, -5311, 1, 0},
		{"ancient_bull_rancor", 1500, 2192, 78, -4285, 1, 0},
	},
}
registerScreenPlay("jediKnightTrialsTwo", true)

function jediKnightTrialsTwo:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function jediKnightTrialsTwo:spawnMobiles()
		local mobileTable = self.tuskens
		for i = 1, table.getn(mobileTable), 1 do
			local pRancor = spawnMobile("dathomir", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			AiAgent(pTusken):setAiTemplate("")
			createObserver(OBJECTDESTRUCTION, "jediKnightTrialsTwo", "rancorDead", pRancor)
		end
end


function jediKnightTrialsTwo:rancorDead(pRancor, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "jediKnightTrialsTwo")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "jediKnightTrialsTwo")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":rancorKilled", readData(groupMember:getObjectID() .. ":rancorKilled") + 1)							if(readData(groupMember:getObjectID() .. ":rancorKilled") > 8) then
							groupMember:sendSystemMessage("NPC's Killed " .. readData(groupMember:getObjectID() .. ":rancorKilled") .. "/ 8")							end
			
						if(readData(groupMember:getObjectID() .. ":rancorKilled") == 8) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, jediKnightTrialsTwo.questString)
						end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "jediKnightTrialsTwo")
					
					if(hasState == true) then 

						writeData(player:getObjectID() .. ":rancorKilled", readData(player:getObjectID() .. ":rancorKilled") + 1)
					
						if(readData(player:getObjectID() .. ":rancorKilled") > 8) then	
							player:sendSystemMessage("NPC's Killed " .. readData(player:getObjectID() .. ":rancorKilled") .. "/ 8")	
						end
						if(readData(player:getObjectID() .. ":rancorKilled") == 8) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, jediKnightTrialsTwo.questString)
						end
				end
			end
		end)
	return 0
end

--Setup

function jediKnightTrialsTwo:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediKnightTrialsTwo:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_knight_trials_two_convo_handler = Object:new {
	
 }

function jedi_knight_trials_two_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediKnightTrialsTwo")
			local questTurnin = creature:hasScreenPlayState(4, "jediKnightTrialsTwo")
			local questComplete = creature:hasScreenPlayState(8, "jediKnightTrialsTwo")
			local padawanComplete = creature:hasScreenPlayState(16, "padawanQuest")
			local trialTwoComplete = creature:hasScreenPlayState(8, "jediKnightTrialsOne")


				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (padawanComplete == true and trialTwoComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("not_ready")	
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_knight_trials_two_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediKnightTrialsTwo.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediKnightTrialsTwo.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 50)
		end)

	end
	


	return conversationScreen
end

