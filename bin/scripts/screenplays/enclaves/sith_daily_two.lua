local ObjectManager = require("managers.object.object_manager")

sith_daily_two = ScreenPlay:new {
	numberOfActs = 1,
	questString = "sith_daily_two",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("sith_daily_two", true)

function sith_daily_two:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end


function sith_daily_two:spawnMobiles()
	spawnMobile("yavin4", "lord_drazk", 0, 13.6, -42.4, -87.1, 41, 3435643) -- quest giver
	local pNPC = spawnMobile("yavin4", "jedi_knight_fool", 300, 3891, 203, -884, 61, 0) --NPC to Kill
	createObserver(OBJECTDESTRUCTION, "sith_daily_two", "firstQuestDead", pNPC)
end


function sith_daily_two:firstQuestDead(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasQuest = player:hasScreenPlayState(2, "sith_daily_two")

	if(hasQuest == true) then
		player:setScreenPlayState(4, sith_daily_two.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("yavin4", "Lord Drazk", "", 5073, 310, 5, true, true, WAYPOINTTHEMEPARK, 1)
		CreatureObject(playerObject):sendSystemMessage("Return to Lord Drazk")
	end

	return 0
end

function sith_daily_two:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(sith_daily_two.states.quest.phasetwo, sith_daily_two.questString)
	player:removeScreenPlayState(sith_daily_two.states.quest.phaseone, sith_daily_two.questString)
	player:removeScreenPlayState(sith_daily_two.states.quest.intro, sith_daily_two.questString)

end

--Setup

function sith_daily_two:getActivePlayerName()
	return self.questdata.activePlayerName
end

function sith_daily_two:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

sith_daily_two_convo_handler = Object:new {
	
 }

function sith_daily_two_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "sith_daily_two")
			local questTurnin = creature:hasScreenPlayState(4, "sith_daily_two")
			local questComplete = creature:hasScreenPlayState(8, "sith_daily_two")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function sith_daily_two_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, sith_daily_two.questString)
		local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("yavin4", "Jedi Knight", "", 3891, -844, 5, true, true, WAYPOINTTHEMEPARK, 1)
		PlayerObject(pGhost):addWaypoint("yavin4", "Jedi Knight", "Kill the Jedi Knight", 3891, -844, 5, true, true, 0)

	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, sith_daily_two.questString)

		player:awardExperience("force_rank_xp", 10, true)

		createEvent(true, 86400000, "sith_daily_two", "removeDailyTimer", conversingPlayer)
	end
	


	return conversationScreen
end

