factionRewardType = {
	armor = 1,
	weapon = 2,
	uniform = 3,
	furniture = 4,
	container = 5,
	terminal = 6,
	installation = 7,
	hireling = 8,
	deed = 9,
}

sithRewardData = {
	weaponsArmorList = {
		--"de_10_pistol", "armor_bountyhunter_helmet", "armor_bountyhunter_chest", "armor_bountyhunter_legs", "armor_bountyhunter_boots", "armor_bountyhunter_bicep_l", "armor_bountyhunter_bicep_r", "armor_bountyhunter_bracer_l", "armor_bountyhunter_bracer_r", "armor_bountyhunter_boots", "armor_bountyhunter_gloves", "armor_bountyhunter_belt",
	},

	weaponsArmor = {
		--de_10_pistol = { type=factionRewardType.weapon, display="DE-10 Pistol Schematic", item="object/tangible/loot/loot_schematic/death_watch_pistol_de10_schematic.iff", cost=2200},
		--armor_bountyhunter_helmet = { type=factionRewardType.armor, display="Bounty Hunter Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_helmet_schematic.iff", cost=1500},
		--armor_bountyhunter_chest = { type=factionRewardType.armor, display="Bounty Hunter Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_chest_plate_schematic.iff", cost=1500},
		--armor_bountyhunter_legs = { type=factionRewardType.armor, display="Bounty Hunter Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_leggings_schematic.iff", cost=1500},
		--armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1500},
		--armor_bountyhunter_bicep_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bicep", item="death_watch_bounty_hunter_bicep_l_schematic", cost=1500},
		--armor_bountyhunter_bicep_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bicep", item="death_watch_bounty_hunter_bicep_r_schematic", cost=1500},
		--armor_bountyhunter_bracer_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bracer", item="death_watch_bounty_hunter_bracer_l_schematic", cost=1500},
		--armor_bountyhunter_bracer_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bracer", item="death_watch_bounty_hunter_bracer_r_schematic", cost=1500},
		--armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="death_watch_bounty_hunter_boots_schematic", cost=1500},
		--armor_bountyhunter_gloves = { type=factionRewardType.armor, display="Bounty Hunter Armor Gloves", item="death_watch_bounty_hunter_gloves_schematic", cost=1500},
		--armor_bountyhunter_belt = { type=factionRewardType.armor, display="Bounty Hunter Armor Belt", item="death_watch_bounty_hunter_belt_schematic", cost=1500},

		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	furnitureList = {
		"art_dooku_bust", "art_chon_bust", "art_small_s01", "art_small_s02", "art_small_s03", "art_small_s04"
	},

	furniture = {
		art_dooku_bust = { type=factionRewardType.furniture, display="Count Dooku Bust", item="object/tangible/tcg/series1/decorative_dooku_bust.iff", cost=800},
		art_chon_bust = { type=factionRewardType.container, display="Chon Actrion Bust", item="object/tangible/tcg/series1/decorative_chon_bust.iff", cost=500},
		art_jedi_bookshelf = { type=factionRewardType.furniture, display="Jedi Library Bookshelf", item="object/tangible/tcg/series1/decorative_jedi_library_bookshelf.iff", cost=1000},
		art_jedi_crest = { type=factionRewardType.furniture, display="Jedi Crest Painting", item="object/tangible/tcg/series1/decorative_painting_jedi_crest.iff", cost=500},
	},

	hirelingList = {
		--"rebel_trooper",
	},

	hirelings = {
		--rebel_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:command_barc", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="rebel_trooper", cost=150},
	},

	deedListing = {
		--"command_barc",
	},

	deedList = {
		--command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/intangible/pet/pet_control.iff", cost=150}
	}
}

jediRewardData = {
	weaponsArmorList = {
		--"de_10_pistol", "armor_bountyhunter_helmet", "armor_bountyhunter_chest", "armor_bountyhunter_legs", "armor_bountyhunter_boots", "armor_bountyhunter_bicep_l", "armor_bountyhunter_bicep_r", "armor_bountyhunter_bracer_l", "armor_bountyhunter_bracer_r", "armor_bountyhunter_boots", "armor_bountyhunter_gloves", "armor_bountyhunter_belt",
	},

	weaponsArmor = {
		--de_10_pistol = { type=factionRewardType.weapon, display="DE-10 Pistol Schematic", item="object/tangible/loot/loot_schematic/death_watch_pistol_de10_schematic.iff", cost=2200},
		--armor_bountyhunter_helmet = { type=factionRewardType.armor, display="Bounty Hunter Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_helmet_schematic.iff", cost=1500},
		--armor_bountyhunter_chest = { type=factionRewardType.armor, display="Bounty Hunter Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_chest_plate_schematic.iff", cost=1500},
		--armor_bountyhunter_legs = { type=factionRewardType.armor, display="Bounty Hunter Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_leggings_schematic.iff", cost=1500},
		--armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1500},
		--armor_bountyhunter_bicep_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bicep", item="death_watch_bounty_hunter_bicep_l_schematic", cost=1500},
		--armor_bountyhunter_bicep_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bicep", item="death_watch_bounty_hunter_bicep_r_schematic", cost=1500},
		--armor_bountyhunter_bracer_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bracer", item="death_watch_bounty_hunter_bracer_l_schematic", cost=1500},
		--armor_bountyhunter_bracer_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bracer", item="death_watch_bounty_hunter_bracer_r_schematic", cost=1500},
		--armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="death_watch_bounty_hunter_boots_schematic", cost=1500},
		--armor_bountyhunter_gloves = { type=factionRewardType.armor, display="Bounty Hunter Armor Gloves", item="death_watch_bounty_hunter_gloves_schematic", cost=1500},
		--armor_bountyhunter_belt = { type=factionRewardType.armor, display="Bounty Hunter Armor Belt", item="death_watch_bounty_hunter_belt_schematic", cost=1500},

		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_imperial"
	},

	installations = {
		--hq_s01_pvp_imperial = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_imperial", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_imperial","hq_s01_imperial"} }
	},

	furnitureList = {
		"art_dooku_bust", "art_chon_bust", "art_small_s01", "art_small_s02", "art_small_s03", "art_small_s04"
	},

	furniture = {
		art_dooku_bust = { type=factionRewardType.furniture, display="Count Dooku Bust", item="object/tangible/tcg/series1/decorative_dooku_bust.iff", cost=800},
		art_chon_bust = { type=factionRewardType.furniture, display="Chon Actrion Bust", item="object/tangible/tcg/series1/decorative_chon_bust.iff", cost=500},
		art_jedi_bookshelf = { type=factionRewardType.furniture, display="Jedi Library Bookshelf", item="object/tangible/tcg/series1/decorative_jedi_library_bookshelf.iff", cost=1000},
		art_jedi_crest = { type=factionRewardType.furniture, display="Jedi Crest Painting", item="object/tangible/tcg/series1/decorative_painting_jedi_crest.iff", cost=500},
	},

	hirelingList = {
		"henchman"
	},

	hirelings = {
		henchman = { type=factionRewardType.hireling, display="@mob/creature_names:mand_bunker_dthwatch_grey", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="death_watch_ghost_pet", cost=700}
	},

	deedListing = {
		--"command_barc"
	},

	deedList = {
		--command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/intangible/pet/pet_control.iff", cost=150}
	}
}
