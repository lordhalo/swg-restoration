local ObjectManager = require("managers.object.object_manager")

jediDailyOne = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jediDailyOne",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	knight = {
		{"fallen_jedi_knight", 300, -4753, 66, 4956, 1, 0},
	},
}
registerScreenPlay("jediDailyOne", true)

function jediDailyOne:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnQuestGiver()
		self:spawnMobiles()
	end
end

--Setup

function jediDailyOne:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jediDailyOne:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function jediDailyOne:spawnQuestGiver()
	spawnMobile("yavin4", "sentinel_ryashk", 0, -17.3, -22.1, 10.7, 126, 8525418) -- quest giver
end

function jediDailyOne:spawnMobiles()

		local mobileTable = self.knight
		for i = 1, #mobileTable, 1 do
			local pFallenKnight = spawnMobile("yavin4", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "jediDailyOne", "fallenKnightDead", pFallenKnight)
		end
end


function jediDailyOne:fallenKnightDead(pFallenKnight, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasQuest = player:hasScreenPlayState(2, "jediDailyOne")
		if(hasQuest == true) then
			player:setScreenPlayState(4, jediDailyOne.questString)
			local pGhost = player:getPlayerObject()
			PlayerObject(pGhost):addWaypoint("yavin4", "Sentinel Ryashk", "", -5524, 4894, 5, true, true, WAYPOINTTHEMEPARK, 1)
			CreatureObject(playerObject):sendSystemMessage("Return to Ryashk")
		end
	return 0
end

function jediDailyOne:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(jediDailyOne.states.quest.phasetwo, jediDailyOne.questString)
	player:removeScreenPlayState(jediDailyOne.states.quest.phaseone, jediDailyOne.questString)
	player:removeScreenPlayState(jediDailyOne.states.quest.intro, jediDailyOne.questString)

end


jedi_daily_one_convo_handler = Object:new {
	
 }

function jedi_daily_one_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "jediDailyOne")
			local questTurnin = creature:hasScreenPlayState(4, "jediDailyOne")
			local questComplete = creature:hasScreenPlayState(8, "jediDailyOne")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function jedi_daily_one_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, jediDailyOne.questString)
		local pGhost = player:getPlayerObject()

		PlayerObject(pGhost):addWaypoint("yavin4", "Fallen Jedi Knight", "Kill the Fallen Jedi Knight", -4753, 4956, 5, true, true, 0)

	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, jediDailyOne.questString)

		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("light_enclave", 100)
		end)

		createEvent(true, 86400000, "jediDailyOne", "removeDailyTimer", conversingPlayer)
	end
	


	return conversationScreen
end

