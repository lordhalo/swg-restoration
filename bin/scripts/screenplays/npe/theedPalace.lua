local ObjectManager = require("managers.object.object_manager")

TheedPalaceScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "TheedPalaceScreenPlay",

	buildingID = 1688849,

	casual_commoners = {
		{"commoner_naboo", 60, 31.0, 12, 45.2, 90, 1688857, "a Commoner", "commoner1"},
		{"commoner_naboo", 60, 0.0, 25.0, 45.2, 180, 1688859, "a Commoner", "commoner2"},
	},
}

registerScreenPlay("TheedPalaceScreenPlay", true)

function TheedPalaceScreenPlay:start()
	if isZoneEnabled("naboo") then
		--self:spawnMobiles()
		--self:spawnSceneObjects()


		if getRandomNumber(100) >= 50 then
			self:royalParty(pBuilding)
		else
			self:royalParty(pBuilding)
		end
	end
end


function TheedPalaceScreenPlay:royalParty(pBuilding)

	for i,v in ipairs(self.casual_commoners) do
		local pMobile = spawnMobile("naboo", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
		if (pMobile ~= nil) then
			writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 1)
			writeStringData(SceneObject(pMobile):getObjectID() .. ":name", v[9])
			CreatureObject(pMobile):setCustomObjectName(v[8])
			createEvent(1000, "TheedPalaceScreenPlay", "casualWalk", pMobile, "")
		end
	end

	--BuildingObject(pBuilding):spawnChildCreature("commoner_naboo", 300, -18, 12.0, 80.2, 170, 1688854)
	--BuildingObject(pBuilding):spawnChildCreature("commoner_naboo", 300, -18, 12.0, 80.2, 170, 1688854)

	local pNpc = spawnMobile("naboo", "doikk_nats", 60, 44.0, 13.0, 41.0, -90, 1688857)
	self:setMoodString(pNpc, "themepark_music_3")
	pNpc = spawnMobile("naboo", "figrin_dan", 60, 44.0, 13.0, 42.0, -90, 1688857)
	self:setMoodString(pNpc, "themepark_music_3")
	pNpc = spawnMobile("naboo", "nalan_cheel", 60, 44.0, 13.0, 43.0, -90, 1688857)
	self:setMoodString(pNpc, "themepark_music_1")
	pNpc = spawnMobile("naboo", "tech_mor", 60, 44.0, 13.0, 45.0, -90, 1688857)
	self:setMoodString(pNpc, "themepark_music_2")
	pNpc = spawnMobile("naboo", "tedn_dahai", 60, 44.0, 13.0, 47.0, -90, 1688857)
	self:setMoodString(pNpc, "themepark_music_3")

	spawnSceneObject("naboo", "object/tangible/instrument/organ_max_rebo.iff", 44.0, 13.0, 45.0, 1688857, 0.5, 0, 0, 0)

end

function TheedPalaceScreenPlay:casualWalk(pMobile)
	createEvent(getRandomNumber(30,45) * 1000, "TheedPalaceScreenPlay", "npcPatrol", pMobile, "")
	createObserver(DESTINATIONREACHED, "TheedPalaceScreenPlay", "npcDestReached", pMobile)
	AiAgent(pMobile):setAiTemplate("manualescortwalk")
	AiAgent(pMobile):setFollowState(4)
end

function TheedPalaceScreenPlay:npcPatrol(pMobile)
	if (pMobile == nil) then
		return
	end
	local name = readStringData(SceneObject(pMobile):getObjectID() .. ":name")
	local curLoc = readData(SceneObject(pMobile):getObjectID() .. ":currentLoc")
	local nextLoc

	if (name == "commoner1") then
		if (curLoc == 1) then
			nextLoc = { 31.0, 12.0, 45.2, 1688857 }
		else
			nextLoc = { -31.0, 12.0, 45.2, 1688858 }
		end
	end

	if (name == "commoner2") then
		if (curLoc == 1) then
			nextLoc = { 0.0, 25.0, 45.2, 1688859 }
		else
			nextLoc = { 30.0, 23.0, -32.0, 1688861 }
		end
	end

	AiAgent(pMobile):stopWaiting()
	AiAgent(pMobile):setWait(0)
	AiAgent(pMobile):setNextPosition(nextLoc[1], nextLoc[2], nextLoc[3], nextLoc[4])
	AiAgent(pMobile):executeBehavior()
end

function TheedPalaceScreenPlay:npcDestReached(pMobile)
	if (pMobile == nil) then
		return 0
	end

	local curLoc = readData(SceneObject(pMobile):getObjectID() .. ":currentLoc")

	if (curLoc == 1) then
		writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 2)
	else
		writeData(SceneObject(pMobile):getObjectID() .. ":currentLoc", 1)
	end

	createEvent(getRandomNumber(350,450) * 100, "TheedPalaceScreenPlay", "npcPatrol", pMobile, "")

	return 0
end
