local ObjectManager = require("managers.object.object_manager")

nabooHouseQuest = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "nabooHouseQuest",

	quest_npcs = {
		{"svoni_bilosto", 60, -5708, 6, 4267, 90, 0, "", "commoner1"}
	},
}
registerScreenPlay("nabooHouseQuest", true)

function nabooHouseQuest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end


--Mobile Spawning
function nabooHouseQuest:spawnMobiles()
	local pNpc = spawnMobile("naboo", "svoni_bilosto", 1, -4951, 6, 4062,  35, 0)
	writeData("breechNpcID", CreatureObject(pNpc):getObjectID())
	local pActiveArea = spawnActiveArea("naboo", "object/active_area.iff", -4951, 6, 4062, 20, 0)
    
	if (pActiveArea ~= nil) then
	        createObserver(ENTEREDAREA, "nabooHouseQuest", "nearNpc", pActiveArea)
	    end

end

--NPC had a safe house, Bandits raided it. If you help Defeat the NPCs, he will give you a peice of realestate (a House) in return

function nabooHouseQuest:nearNpc()
	local npcID = readData("breechNpcID")
	local pNpc = getSceneObject(npcID)
	local pMobile = LuaCreatureObject(pNpc)

	if getRandomNumber(100) >= 50 then
		spatialChat(pNpc, "Hello There!") -- 
		pMobile:doAnimation("wave")
	else
		spatialChat(pNpc, "Help, Please!")
		pMobile:doAnimation("explain")
	end

	return 0
end
