local ObjectManager = require("managers.object.object_manager")

newPlayerCraftingQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "newPlayerCraftingQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, nine = 1024, ten = 2048, elevin = 4096, twelve = 8192, thirteen = 16384, fourteen = 32768}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

}
registerScreenPlay("newPlayerCraftingQuest", true)

function newPlayerCraftingQuest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnNPCs()
	end
end



function newPlayerCraftingQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function newPlayerCraftingQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function newPlayerCraftingQuest:spawnNPCs()

end

new_player_crafting_convo_handler = Object:new {
	
 }

function new_player_crafting_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--local nabooOcupation = readData("newPlayerCraftingQuest:ocupationStatus") -- 0 == Nabooian / 1 == Trade Federation
			local hasState = creature:hasScreenPlayState(2, "newPlayerCraftingQuest") -- Fork From New Player Intro

			local firstQuestComplete = creature:hasScreenPlayState(4, "newPlayerCraftingQuest") -- Selected First Box Trigger
			local secondQuestComplete = creature:hasScreenPlayState(8, "newPlayerCraftingQuest")

				if (secondQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("phase_three_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("phase_two_screen")	
				elseif (hasState == true) then
					nextConversationScreen = conversation:getScreen("greet_screen")
				else
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function new_player_crafting_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local questNPC = LuaCreatureObject(conversingNPC)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "first_ark_complete" ) then
		player:setScreenPlayState(4, newPlayerCraftingQuest.questString) -- Accepted First Skill Box
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_mineral.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_liquid.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_moisture.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_solar.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_gas.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_wind.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_lumber.iff", -1)
		player:playMusicMessage("sound/skill_qualify.snd")
		questNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "phase_two_screen_final" ) then
		player:setScreenPlayState(8, newPlayerCraftingQuest.questString) -- Accepted First Skill Box
		player:playMusicMessage("sound/ui_button_random.snd")
		questNPC:doAnimation("salute1")
		awardSkill(conversingPlayer, "crafting_artisan_engineering_01")
		createLoot(pInventory, "crafting_tool_npe", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
	end
	


	return conversationScreen
end
