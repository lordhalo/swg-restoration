local ObjectManager = require("managers.object.object_manager")

newPlayerQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "newPlayerQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	junkBattleDroids = {
		{ "npe_junk_battledroid", 60, -4902, 4, 3234, 0, 0},
		{ "npe_junk_battledroid", 60, -4907, 4, 3231, 0, 0},
		{ "npe_junk_battledroid", 60, -4892, 4, 3219, 0, 0},
	},

}
registerScreenPlay("newPlayerQuest", true)

function newPlayerQuest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnQuestGivers()
		self:spawnCommonQuestNPCS()
		--self:spawnTargetNPC()
		--self:spawnCityNPCs()
		--self:spawnMakeCityAliveNPCs()
	end
end


function newPlayerQuest:spawnQuestGivers()
	spawnMobile("naboo", "recruiter_larrick", 0, -5481.5, 6.0, 4305.0,  -90, 0)-- Recruiter
	spawnMobile("naboo", "officer_miller", 0, -5455.0, 6.0, 4140.0,  90, 0)-- Melee Fork -5455, 4140
	spawnMobile("naboo", "officer_joll", 0, -5720.0, 6.0, 4020.0,  -170, 0)-- Ranged Fork
	spawnMobile("naboo", "engineer_race", 0, -4951.0, 6.0, 4127.0,  150, 0)-- Crafting Fork
	spawnMobile("naboo", "doctor_hue", 0, -28.2, 0.3, 5.6, 90, 1697364)-- Medic Fork
	spawnMobile("naboo", "ranger_rick", 0, 24.4, -61.4, -302.8,  -22, 5996355)-- Scout Fork
	spawnMobile("naboo", "dancer_danny", 0, 24.4, -61.4, -302.8,  -22, 5996355)-- Entertainer Fork
end

function newPlayerQuest:spawnCommonQuestNPCS()
	local mobileTable = self.junkBattleDroids

	for i = 1, #mobileTable, 1 do
		local pDroid = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
		AiAgent(pDroid):setAiTemplate("")
		createObserver(OBJECTDESTRUCTION, "newPlayerMeleeQuest", "rustyDroidDestroyed", pDroid)

	end
	
end

function newPlayerQuest:spawnCityNPCs()
	--Current City Control based flag
	--City can be placed under Nabooian Occupation or Trade Federation
	--Move to Helper Script container
end

function newPlayerQuest:spawnMakeCityAliveNPCs()
	--NPCs that roam the city, talk and bring life
	--Swap to ocupation based on current ownership
	--Move to Helper 
end

function newPlayerQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function newPlayerQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


new_player_recruiter_quest_convo_handler = Object:new {
	
 }

function new_player_recruiter_quest_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--local nabooOcupation = readData("newPlayerQuest:ocupationStatus") -- 0 == Nabooian / 1 == Trade Federation
			local skillsAwarded = creature:hasScreenPlayState(2, "newPlayerQuest")
			local firstQuestComplete = creature:hasScreenPlayState(4, "newPlayerQuest")
				if (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_complete_screen")	
				elseif (skillsAwarded == true) then
					nextConversationScreen = conversation:getScreen("third_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function new_player_recruiter_quest_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "award_skills" ) then
		player:setScreenPlayState(2, newPlayerQuest.questString)
		awardSkill(conversingPlayer, "combat_marksman_novice")
		awardSkill(conversingPlayer, "combat_brawler_novice")
		awardSkill(conversingPlayer, "crafting_artisan_novice")
		awardSkill(conversingPlayer, "science_medic_novice")
		awardSkill(conversingPlayer, "outdoors_scout_novice")
		awardSkill(conversingPlayer, "social_entertainer_novice")
	end

	if ( screenID == "leave_quest_screen" ) then
		player:setScreenPlayState(2, newPlayerQuest.questString) -- Restarts you after the award loop
	end

	if ( screenID == "melee_screen_final" ) then
		player:setScreenPlayState(4, newPlayerQuest.questString)
		player:setScreenPlayState(2, newPlayerMeleeQuest.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Officer Miller", "", -5455, 4140, 5, true, true, 0)
	end

	if ( screenID == "range_screen_final" ) then
		player:setScreenPlayState(4, newPlayerQuest.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Officer Joll", "", -5720, 4020, 5, true, true, 0)
	end

	if ( screenID == "medic_screen_final" ) then
		player:setScreenPlayState(4, newPlayerQuest.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Doctor Hue", "", -5035, 4185, 5, true, true, 0)
	end

	if ( screenID == "crafting_screen_final" ) then
		player:setScreenPlayState(4, newPlayerQuest.questString)
		player:setScreenPlayState(2, newPlayerCraftingQuest.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Engineer Race", "", -5035, 4185, 5, true, true, 0)
	end
	


	return conversationScreen
end

