local ObjectManager = require("managers.object.object_manager")

nabooCurator = ScreenPlay:new {

}

registerScreenPlay("nabooCurator", true)

function nabooCurator:start()
	if (isZoneEnabled("naboo")) then
		self:spawnNPCs()
		self:spawnTrainers()
		--self:spawnCityNPCs()
		--self:spawnMakeCityAliveNPCs()
	end
end

function nabooCurator:spawnNPCs()

	spawnMobile("naboo", "naboo_curator", 0, -4885.0, 6.0, 4145.0,  35, 0)
	

end

function nabooCurator:spawnTrainers()

	spawnMobile("naboo", "trainer_rifleman", 0, -5445.0, 6.0, 4175.0,  -170, 0)
	spawnMobile("naboo", "trainer_carbine", 0, -5495.0, 6.0, 3816.0,  -170, 0) -- Blaster Range (Target Practice)
	spawnMobile("naboo", "trainer_pistol", 0, -5250.0, 6.0, 4616.0,  -170, 0)
	spawnMobile("naboo", "trainer_commando", 0, -5475.0, 6.0, 4405.0,  -170, 0)
	spawnMobile("naboo", "trainer_creaturehandler", 0, -5540.0, 6.0, 3860.0,  -170, 0)
	spawnMobile("naboo", "trainer_smuggler", 0, -5138.0, 6.0, 4408.0,  -170, 0)
	spawnMobile("naboo", "trainer_squadleader", 0, -5484.0, 6.0, 4134.0,  170, 0)
	spawnMobile("naboo", "trainer_bountyhunter", 0, -5588.0, 6.0, 4503.0,  170, 0)
	spawnMobile("naboo", "trainer_dancer", 0, 20.0, -0.9, -14.8,  0, 92)
	spawnMobile("naboo", "trainer_musician", 0, 20.0, -0.9, 14.8,  179, 93)
	spawnMobile("naboo", "trainer_entertainer", 0, 34.8, 0.1, 1.9,  -90, 90)
	spawnMobile("naboo", "trainer_merchant", 0, -5102.0, 6.0, 4179.0,  40, 0)
	spawnMobile("naboo", "trainer_armorsmith", 0, -5057.0, 6.0, 4129.0,  40, 0)
	spawnMobile("naboo", "trainer_weaponsmith", 0, -5063.0, 6.0, 4164.0,  40, 0)
	spawnMobile("naboo", "trainer_droidengineer", 0, -4958.0, 6.0, 4115.0,  40, 0)
	spawnMobile("naboo", "trainer_tailor", 0, -5058.0, 6.0, 4129.0,  40, 0)
	spawnMobile("naboo", "trainer_chef", 0, -5722.0, 6.0, 4207.0,  0, 0)
	spawnMobile("naboo", "trainer_bioengineer", 0, -5720.0, 6.0, 4207.0,  0, 0)
	spawnMobile("naboo", "trainer_architect", 0, -5659.0, 6.0, 4194.0,  0, 0)
	spawnMobile("naboo", "trainer_imagedesigner", 0, -5.2, 6.0, -4.2,  0, 189545)

end

naboo_curator_convo_handler = Object:new {
	
 }

function naboo_curator_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

				nextConversationScreen = conversation:getScreen("greet_screen")

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function naboo_curator_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pNPC = LuaCreatureObject(conversingNPC)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "swordsman_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Swordsman Trainer", "", -5382, 4327, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "fencer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Fencer Trainer", "", -5565, 4304, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "pikeman_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Pikeman Trainer", "", -5375, 4310, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "teraskasi_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Teras Kasi Trainer", "", -5649, 4206, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "brawler_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Brawler Trainer", "", -5942, 4253, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "rifleman_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Rifleman Trainer", "", -5445, 4175, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "carbineer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Carbineer Trainer", "", -5495, 3816, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "pistoleer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Pistoleer Trainer", "", -5250, 4616, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "commando_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Commando Trainer", "", -5475, 4405, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "marksman_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Marksman Trainer", "", -5982, 4254, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "doctor_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Doctor Trainer", "", -5005, 4152, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "combat_medic_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Combat Medic Trainer", "", -4994, 4149, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "medic_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Combat Medic Trainer", "", -5004, 4156, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "creature_handler_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Creature Handler Trainer", "", -5540, 3860, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "smuggler_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Smuggler Trainer", "", -5138, 4408, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "squad_leader_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Squad Leader Trainer", "", -5484, 4134, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "bounty_hunter_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Bounty Hunter Trainer", "", -5588, 4503, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "dancer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Dancer Trainer", "", -5165, 4264, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "musician_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Musician Trainer", "", -5189, 4282, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "merchant_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Merchant Trainer", "", -5102, 4179, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "entertainer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Entertainer Trainer", "", -5169, 4286, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "armorsmith_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Armorsmith Trainer", "", -5058, 4129, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "weaponsmith_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Weaponsmith Trainer", "", -5063, 4164, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "droid_engineer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Droid Engineer Trainer", "", -4958, 4115, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "tailor_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Tailor Trainer", "", -5058, 4129, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "mechanic_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Mechanic Trainer", "", -4951, 4127, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "chef_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Chef Trainer", "", -5722, 4207, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "architect_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Architect Trainer", "", -5659, 4194, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "artisan_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Artisan Trainer", "", -4946, 4131, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "image_designer_training" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Image Designer Trainer", "", -5130, 4256, 2, true, true, 0)
		player:playMusicMessage("sound/ui_button_arrow_forward.snd")
	end

	if ( screenID == "jedi_screen" ) then
		spatialChat(conversingNPC, "Oh my.. Please forgive my Outburst.")
		pNPC:doAnimation("apologize")
	end
	


	return conversationScreen
end
