local ObjectManager = require("managers.object.object_manager")

newPlayerMeleeQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "newPlayerMeleeQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, nine = 1024, ten = 2048, elevin = 4096, twelve = 8192, thirteen = 16384, fourteen = 32768}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

}
registerScreenPlay("newPlayerMeleeQuest", true)

function newPlayerMeleeQuest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnTrainerDroids()
		self:spawnNPCs()
		--self:spawnTargetNPC()
		--self:spawnCityNPCs()
		--self:spawnMakeCityAliveNPCs()
	end
end



function newPlayerMeleeQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function newPlayerMeleeQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function newPlayerMeleeQuest:spawnNPCs()
	local pYuah = spawnMobile("naboo", "yuah_vulstin", 180, 6.5, -0.9, -17.8,  -50, 94)
	self:setMoodString(pYuah, "conversation")
	createObserver(DAMAGERECEIVED, "newPlayerMeleeQuest", "yuahDefeated", pYuah)
end

function newPlayerMeleeQuest:spawnTrainerDroids()
	spawnMobile("naboo", "npe_trainer_droid", 0, -5478, 6, 4135,  0, 0)
	spawnMobile("naboo", "npe_trainer_droid", 0, -5479, 6, 4135,  0, 0)
	spawnMobile("naboo", "npe_trainer_droid", 0, -5480, 6, 4135,  0, 0)
	spawnMobile("naboo", "npe_trainer_droid", 0, -5478, 6, 4133,  0, 0)
	spawnMobile("naboo", "npe_trainer_droid", 0, -5479, 6, 4133,  0, 0)
	spawnMobile("naboo", "npe_trainer_droid", 0, -5480, 6, 4133,  0, 0)
end

function newPlayerMeleeQuest:yuahDefeated(pNPC, pPlayer)
	local npc = LuaCreatureObject(pNPC)
	local player = LuaCreatureObject(pPlayer)

	health = npc:getHAM(0)
	maxHealth = npc:getMaxHAM(0)

	if (health <= (maxHealth * 0.5)) then
		spatialChat(pNPC, "Ouch! OK! I'll stop.. Forgive my behavior")
		CreatureObject(pNPC):setOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(pNPC):setPvpStatusBitmask(0)
		CreatureObject(pNPC):clearCombatState(1)
		AiAgent(pNPC):setFollowState(0)
		player:setScreenPlayState(16, newPlayerMeleeQuest.questString)
		CreatureObject(pPlayer):sendSystemMessage("Return to Miller")
		CreatureObject(pPlayer):playMusicMessage("sound/ui_button_random.snd")
	end
	return 0
end

function newPlayerMeleeQuest:rustyDroidDestroyed(pNPC, pPlayer)
	local player = LuaCreatureObject(pPlayer)

	writeData("rustyDroid:npcsDead",readData("rustyDroid:npcsDead") + 1)

	if(readData("rustyDroid:npcsDead") == 3) then

	ObjectManager.withCreatureObject(pPlayer, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = creature:hasScreenPlayState(64, "newPlayerMeleeQuest")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Return to Miller")
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:setScreenPlayState(128, newPlayerMeleeQuest.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(64, "newPlayerMeleeQuest")
					
					if(hasState == true) then 
						player:sendSystemMessage("You Discover a Data Spike, Deliver it to Miller")
						player:playMusicMessage("sound/ui_button_random.snd")
						player:setScreenPlayState(128, newPlayerMeleeQuest.questString)
				end
			end
		end)
	
	end

	return 1
end


new_player_melee_quest_convo_handler = Object:new {
	
 }

function new_player_melee_quest_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--local nabooOcupation = readData("newPlayerMeleeQuest:ocupationStatus") -- 0 == Nabooian / 1 == Trade Federation
			local hasState = creature:hasScreenPlayState(2, "newPlayerMeleeQuest") -- Fork From New Player Intro

			local hasPolearm = creature:hasSkill("combat_brawler_polearm_01")
			local hasFencer = creature:hasSkill("combat_brawler_1handmelee_01")
			local hasSword = creature:hasSkill("combat_brawler_2handmelee_01")
			local hasUnarmed = creature:hasSkill("combat_brawler_unarmed_01")

			local firstQuestComplete = creature:hasScreenPlayState(4, "newPlayerMeleeQuest") -- Selected First Box Trigger
			local negotationQuestStarted = creature:hasScreenPlayState(8, "newPlayerMeleeQuest") 
			local negotationQuestComplete = creature:hasScreenPlayState(16, "newPlayerMeleeQuest") 
			local scoutQuestIntro = creature:hasScreenPlayState(32, "newPlayerMeleeQuest") -- Selected Second Box Trigger
			local scoutQuestIntroAccepted = creature:hasScreenPlayState(64, "newPlayerMeleeQuest") -- Selected Second Box Trigger
			local scoutQuestComplete = creature:hasScreenPlayState(128, "newPlayerMeleeQuest")
			local spikeVendorStarted = creature:hasScreenPlayState(256, "newPlayerMeleeQuest")

				if (spikeVendorStarted == true) then
					nextConversationScreen = conversation:getScreen("spike_vendor_wait_screen")
				elseif (scoutQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("scout_quest_complete")
				elseif (scoutQuestIntroAccepted == true) then
					nextConversationScreen = conversation:getScreen("scout_quest_wait_screen")
				elseif (negotationQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("negotiation_quest_complete")
				elseif (negotationQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("negotiation_wait_screen")
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("box_one_complete_screen")	
				elseif (hasState == true) then
					nextConversationScreen = conversation:getScreen("greet_screen")
				else
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function new_player_melee_quest_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local questNPC = LuaCreatureObject(conversingNPC)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "pikeman_train_one_screen" ) then
		player:setScreenPlayState(4, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		awardSkill(conversingPlayer, "combat_brawler_polearm_01")
		player:playMusicMessage("sound/skill_qualify.snd")
		questNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "fencer_train_one_screen" ) then
		player:setScreenPlayState(4, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		awardSkill(conversingPlayer, "combat_brawler_1handmelee_01")
		player:playMusicMessage("sound/skill_qualify.snd")
		questNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "swordsman_train_one_screen" ) then
		player:setScreenPlayState(4, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		awardSkill(conversingPlayer, "combat_brawler_2handmelee_01")
		player:playMusicMessage("sound/skill_qualify.snd")
		questNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "unarmed_train_one_screen" ) then
		player:setScreenPlayState(4, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		awardSkill(conversingPlayer, "combat_brawler_unarmed_01")
		player:playMusicMessage("sound/skill_qualify.snd")
		questNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "negotiation_quest_one" ) then
		player:setScreenPlayState(8, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		player:playMusicMessage("sound/ui_button_random.snd")
		questNPC:doAnimation("salute1")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Yuah Vulstin", "", -5172, 4252, 5, true, true, WAYPOINTTHEMEPARK, 1)
	end

	if ( screenID == "negotiation_quest_reward_complete" ) then
		local hasPolearm = player:hasSkill("combat_brawler_polearm_01")
		local hasFencer = player:hasSkill("combat_brawler_1handmelee_01")
		local hasSword = player:hasSkill("combat_brawler_2handmelee_01")
		local hasUnarmed = player:hasSkill("combat_brawler_unarmed_01")
		player:setScreenPlayState(32, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		player:playMusicMessage("sound/skill_qualify.snd")
		if (hasPolearm == true) then
			awardSkill(conversingPlayer, "combat_brawler_polearm_02")
		elseif (hasFencer == true) then
			awardSkill(conversingPlayer, "combat_brawler_1handmelee_02")
		elseif (hasSword == true) then
			awardSkill(conversingPlayer, "combat_brawler_2handmelee_02")
		elseif (hasUnarmed == true) then
			awardSkill(conversingPlayer, "combat_brawler_unarmed_02")
		else
			CreatureObject(player):sendSystemMessage("Skill Learn failed, report to Staff")
		end
	end

	if ( screenID == "scout_droid_slayer_quest_start" ) then
		player:setScreenPlayState(64, newPlayerMeleeQuest.questString) -- Accepted First Skill Box
		player:playMusicMessage("sound/ui_button_random.snd")
		questNPC:doAnimation("salute1")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Scout Reported Droids", "", -4900, 3222, 5, true, true, WAYPOINTTHEMEPARK, 1)
	end

	if ( screenID == "scout_quest_reward" ) then
		player:setScreenPlayState(256, newPlayerMeleeQuest.questString)
		player:playMusicMessage("sound/ui_button_random.snd")
		questNPC:doAnimation("salute1")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Danlun", "", -5126, 4241, 5, true, true, WAYPOINTTHEMEPARK, 1)
	end
	


	return conversationScreen
end

yuah_vulstin_convo_handler = Object:new {
	
 }

function yuah_vulstin_convo_handler :getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--local nabooOcupation = readData("newPlayerMeleeQuest:ocupationStatus") -- 0 == Nabooian / 1 == Trade Federation
			local hasState = creature:hasScreenPlayState(8, "newPlayerMeleeQuest") --

			local firstQuestComplete = creature:hasScreenPlayState(4, "newPlayerMeleeQuest") -- Selected First Box Trigger
			local QuestComplete = creature:hasScreenPlayState(16, "newPlayerMeleeQuest") -- Selected First Box Trigger

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")
				elseif (hasState == true) then
					nextConversationScreen = conversation:getScreen("greet_screen")
				else
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function yuah_vulstin_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local questNPC = LuaCreatureObject(conversingNPC)
	local pInventory = player:getSlottedObject("inventory")


	if ( screenID == "dark_second_screen" or screenID == "light_third_screen") then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer) 
	end
	


	return conversationScreen
end

