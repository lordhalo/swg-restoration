local ObjectManager = require("managers.object.object_manager")
--includeFile("themepark/marauder/vendor/marauderPerkData.lua")

marauderScreenplay = Object:new {
	minimumFactionStanding = 100,

	errorCodes =  {
		SUCCESS = 0, INVENTORYFULL = 1,  NOTENOUGHFACTION = 2, GENERALERROR = 3, ITEMCOST = 4, INVENTORYERROR = 5,
		TEMPLATEPATHERROR = 6, GIVEERROR = 7, DATAPADFULL = 8, DATAPADERROR = 9, TOOMANYHIRELINGS = 10,
	}
}

function marauderScreenplay:getFactionDataTable()
	return marauderRewardData
end

function marauderScreenplay:getMinimumFactionStanding()
	return self.minimumFactionStanding
end

function marauderScreenplay:isWeapon(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.weaponsArmor[strItem] ~= nil and factionRewardData.weaponsArmor[strItem].type == factionRewardType.weapon
end

function marauderScreenplay:isArmor(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.weaponsArmor[strItem] ~= nil and factionRewardData.weaponsArmor[strItem].type == factionRewardType.armor
end


function marauderScreenplay:isUniform(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.uniforms[strItem] ~= nil
end

function marauderScreenplay:isHireling(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.hirelings[strItem] ~= nil
end

function marauderScreenplay:isSchematic(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.deedList[strItem] ~= nil
end

function marauderScreenplay:isFurniture(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.furniture[strItem] ~= nil
end

function marauderScreenplay:isContainer(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.furniture[strItem] ~= nil and factionRewardData.furniture[strItem].type == factionRewardType.container
end

function marauderScreenplay:isTerminal(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.furniture[strItem] ~= nil and factionRewardData.furniture[strItem].type == factionRewardType.terminal
end

function marauderScreenplay:isInstallation(strItem)
	local factionRewardData = self:getFactionDataTable()
	return factionRewardData.installations[strItem] ~= nil and factionRewardData.installations[strItem].type == factionRewardType.installation
end

function marauderScreenplay:getWeaponsArmorOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.weaponsArmorList) do
		if ( factionRewardData.weaponsArmor[v] ~= nil and factionRewardData.weaponsArmor[v].display ~= nil and factionRewardData.weaponsArmor[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.weaponsArmor[v].display, math.ceil(factionRewardData.weaponsArmor[v].cost)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:getFurnitureOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.furnitureList) do
		if ( factionRewardData.furniture[v] ~= nil and factionRewardData.furniture[v].display ~= nil and factionRewardData.furniture[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.furniture[v].display, math.ceil(factionRewardData.furniture[v].cost)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:getInstallationsOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.installationsList) do
		if ( factionRewardData.installations[v] ~= nil and factionRewardData.installations[v].display ~= nil and factionRewardData.installations[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.installations[v].display, math.ceil(factionRewardData.installations[v].cost * gcwDiscount * smugglerDiscount)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:getHirelingsOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.hirelingList) do
		if ( factionRewardData.hirelings[v] ~= nil and factionRewardData.hirelings[v].display ~= nil and factionRewardData.hirelings[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.hirelings[v].display, math.ceil(factionRewardData.hirelings[v].cost)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:getUniformsOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.uniformList) do
		if ( factionRewardData.uniforms[v] ~= nil and factionRewardData.uniforms[v].display ~= nil and factionRewardData.uniforms[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.uniforms[v].display, math.ceil(factionRewardData.uniforms[v].cost)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:getSchematicOptions()
	local optionsTable = { }
	local factionRewardData = self:getFactionDataTable()
	for k,v in pairs(factionRewardData.deedListing) do
		if ( factionRewardData.deedListing[v] ~= nil and factionRewardData.deedListing[v].display ~= nil and factionRewardData.deedListing[v].cost ~= nil ) then
			local option = {self:generateSuiString(factionRewardData.deedListing[v].display, math.ceil(factionRewardData.deedListing[v].cost)), 0}
			table.insert(optionsTable, option)
		end
	end
	return optionsTable
end

function marauderScreenplay:generateSuiString(item, cost)
	--print("hasAccepted() is " .. item)  
	--return getStringId(item) .. " (Cost: " .. cost .. ")"
	return item .. " (Cost: " .. cost .. ")"
end

function marauderScreenplay:getItemCost(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isWeapon(faction, itemString) or self:isArmor(itemString)  and factionRewardData.weaponsArmor[itemString] ~= nil and factionRewardData.weaponsArmor[itemString].cost ~= nil then
		return factionRewardData.weaponsArmor[itemString].cost
	elseif self:isUniform(itemString) and factionRewardData.uniforms[itemString].cost ~= nil then
		return factionRewardData.uniforms[itemString].cost
	elseif self:isFurniture(itemString) and factionRewardData.furniture[itemString].cost ~= nil then
		return factionRewardData.furniture[itemString].cost
	elseif self:isInstallation(itemString) and factionRewardData.installations[itemString].cost ~= nil then
		return factionRewardData.installations[itemString].cost
	elseif self:isHireling(itemString) and factionRewardData.hirelings[itemString].cost ~= nil then
		return factionRewardData.hirelings[itemString].cost
	elseif self:isSchematic(itemString) and factionRewardData.deedList[itemString].cost ~= nil then
		return factionRewardData.deedList[itemString].cost
	end
	return nil
end

function marauderScreenplay:getTemplatePath(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isWeapon(faction, itemString) or self:isArmor(itemString) then
		return factionRewardData.weaponsArmor[itemString].item
	elseif self:isUniform(itemString) then
		return factionRewardData.uniforms[itemString].item
	elseif self:isFurniture(itemString) then
		return factionRewardData.furniture[itemString].item
	elseif self:isInstallation(itemString) then
		return factionRewardData.installations[itemString].item
	elseif self:isHireling(itemString) then
		return factionRewardData.hirelings[itemString].item
	elseif self:isSchematic(itemString) then
		return factionRewardData.deedList[itemString].item
	end
	return nil
end

function marauderScreenplay:getDisplayName(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isWeapon(itemString) or self:isArmor(itemString) then
		return factionRewardData.weaponsArmor[itemString].display
	elseif self:isUniform(itemString) then
		return factionRewardData.uniforms[itemString].display
	elseif self:isFurniture(itemString) then
		return factionRewardData.furniture[itemString].display
	elseif self:isInstallation(itemString) then
		return factionRewardData.installations[itemString].display
	elseif self:isHireling(itemString) then
		return factionRewardData.hirelings[itemString].display
	elseif self:isSchematic(itemString) then
		return factionRewardData.deedList[itemString].display
	end
	return nil
end


function marauderScreenplay:getGeneratedObjectTemplate(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isInstallation(faction, itemString) and factionRewardData.installations[itemString].generatedObjectTemplate ~= nil then
		return factionRewardData.installations[itemString].generatedObjectTemplate
	end
	return nil
end

function marauderScreenplay:getControlledObjectTemplate(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isHireling(faction, itemString) and factionRewardData.hirelings[itemString].controlledObjectTemplate ~= nil then
		return factionRewardData.hirelings[itemString].controlledObjectTemplate
	end
	return nil
end

function marauderScreenplay:getBonusItems(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isInstallation(faction, itemString) and factionRewardData.installations[itemString].bonus ~= nil then
		return factionRewardData.installations[itemString].bonus
	end
	return nil
end

function marauderScreenplay:getBonusItemCount(itemString)
	local factionRewardData = self:getFactionDataTable()
	if self:isInstallation(faction, itemString) and factionRewardData.installations[itemString].bonus ~= nil then
		return #factionRewardData.installations[itemString].bonus
	end
	return 0
end

function marauderScreenplay:sendPurchaseSui(pNpc, pPlayer, screenID)
	if (pNpc == nil or pPlayer == nil) then
		return
	end

	writeStringData(CreatureObject(pPlayer):getObjectID() .. ":faction_purchase", screenID)
	local suiManager = LuaSuiManager()
	local options = { }
	if screenID == "fp_furniture" then
		options = self:getFurnitureOptions()
	elseif screenID == "fp_weapons_armor" then
		options = self:getWeaponsArmorOptions()
	elseif screenID == "fp_installations" then
		options = self:getInstallationsOptions()
	elseif screenID == "fp_uniforms" then
		options = self:getUniformsOptions()
	elseif screenID == "fp_hirelings" then
		options = self:getHirelingsOptions()
	elseif screenID == "fp_schematics" then
		options = self:getSchematicOptions()
	end

	suiManager:sendListBox(pNpc, pPlayer, "@faction_recruiter:faction_purchase", "@faction_recruiter:select_item_purchase", 2, "@cancel", "", "@ok", "marauderScreenplay", "handleSuiPurchase", 32, options)
	--realObject->sendListBox(usingObject, targetPlayer, title, text, numOfButtons, cancelButton, otherButton, okButton, options, screenplay, callback, forceCloseDist);

	--suiManager:sendListBox(pNpc, pPlayer, "@faction_recruiter:faction_purchase", "@faction_recruiter:select_item_purchase", 2, "@cancel", "", "@ok", "marauderScreenplay", "handleSuiPurchase", 32, options)
end

function marauderScreenplay:handleSuiPurchase(pCreature, pSui, eventIndex, arg0)
	local cancelPressed = (eventIndex == 1)

	if pCreature == nil then
		return
	end

	if cancelPressed then
		deleteStringData(CreatureObject(pCreature):getObjectID() .. ":faction_purchase")
		return
	end

	local playerID = SceneObject(pCreature):getObjectID()
	local purchaseCategory = readStringData(playerID .. ":faction_purchase")

	if purchaseCategory == "" then
		return
	end

	local purchaseIndex = arg0 + 1
	local dataTable = self:getFactionDataTable(faction)
	local itemListTable = self:getItemListTable(faction, purchaseCategory)
	local itemString = itemListTable[purchaseIndex]
	deleteStringData(playerID .. ":faction_purchase")

	local awardResult = nil

	if (self:isHireling(faction, itemString)) then
		awardResult = self:awardData(pCreature, itemString)
	else
		awardResult = self:awardItem(pCreature, itemString)
	end

	if (awardResult == self.errorCodes.SUCCESS) then
		return
	elseif (awardResult == self.errorCodes.INVENTORYFULL) then
		CreatureObject(pCreature):sendSystemMessage("@dispenser:inventory_full") -- Your inventory is full. You must make some room before you can purchase.
	elseif (awardResult == self.errorCodes.DATAPADFULL) then
		CreatureObject(pCreature):sendSystemMessage("@faction_recruiter:datapad_full") -- Your datapad is full. You must first free some space.
	elseif (awardResult == self.errorCodes.TOOMANYHIRELINGS) then
		CreatureObject(pCreature):sendSystemMessage("@faction_recruiter:too_many_hirelings") -- You already have too much under your command.
	elseif (awardResult == self.errorCodes.NOTENOUGHFACTION) then
		CreatureObject(pCreature):sendSystemMessage("Not Enough Faction")
	elseif (awardResult == self.errorCodes.ITEMCOST) then
		CreatureObject(pCreature):sendSystemMessage("Error determining cost of item. Please post a bug report regarding the item you attempted to purchase.")
	elseif (awardResult == self.errorCodes.INVENTORYERROR or awardResult == self.DATAPADERROR) then
		CreatureObject(pCreature):sendSystemMessage("Error finding location to put item. Please post a report.")
	elseif (awardResult == self.errorCodes.TEMPLATEPATHERROR) then
		CreatureObject(pCreature):sendSystemMessage("Error determining data for item. Please post a bug report regarding the item you attempted to purchase..")
	end
end

function marauderScreenplay:awardItem(pPlayer, faction, itemString)
	return ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")

		if (pInventory == nil) then
			return self.errorCodes.INVENTORYERROR
		end

		local factionStanding = playerObject:getFactionStanding("endor_marauder")
		local itemCost = self:getItemCost(faction, itemString)

		if itemCost == nil then
			return self.errorCodes.ITEMCOST
		end

		itemCost  = math.ceil(itemCost)

		if (factionStanding < (itemCost + self.minimumFactionStanding)) then
			return self.errorCodes.NOTENOUGHFACTION
		end

		local slotsremaining = SceneObject(pInventory):getContainerVolumeLimit() - SceneObject(pInventory):getContainerObjectsSize()

		local bonusItemCount = self:getBonusItemCount(faction, itemString)

		if (slotsremaining < (1 + bonusItemCount)) then
			return self.errorCodes.INVENTORYFULL
		end

		local transferResult =  self:transferItem(player, pInventory, faction, itemString)

		if(transferResult ~= self.errorCodes.SUCCESS) then
			return transferResult
		end

		playerObject:decreaseFactionStanding("endor_marauder", itemCost)

		if bonusItemCount then
			local bonusItems = self:getBonusItems(faction, itemString)
			if bonusItems ~= nil then
				for k, v in pairs(bonusItems) do
					transferResult = self:transferItem(pPlayer, pInventory, faction, v)
					if(transferResult ~= self.errorCodes.SUCCESS) then
						return transferResult
					end
				end
			end
		end

		return self.errorCodes.SUCCESS
	end)
end

function marauderScreenplay:toTitleCase(str)
	local buf = {}
	for word in string.gfind(str, "%S+") do
		local first, rest = string.sub(word, 1, 1), string.sub(word, 2)
		table.insert(buf, string.upper(first) .. string.lower(rest))
	end
	return table.concat(buf, " ")
end

function marauderScreenplay:awardData(pPlayer, faction, itemString)
	return ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		local pDatapad = SceneObject(pPlayer):getSlottedObject("datapad")

		if pDatapad == nil then
			return self.errorCodes.DATAPADERROR
		end

		local gcwFaction = self:getFactionBH(faction)

		local factionStanding = playerObject:getFactionStanding(gcwFaction)
		local itemCost = self:getItemCost(faction, itemString)

		if itemCost == nil then
			return self.errorCodes.ITEMCOST
		end

		itemCost = math.ceil(itemCost)

		if factionStanding < (itemCost + self.minimumFactionStanding) then
			return self.errorCodes.NOTENOUGHFACTION
		end

		local slotsRemaining = SceneObject(pDatapad):getContainerVolumeLimit() - SceneObject(pDatapad):getContainerObjectsSize()
		local bonusItemCount = self:getBonusItemCount(faction, itemString)

		if (slotsRemaining < (1 + bonusItemCount)) then
			return self.errorCodes.DATAPADFULL
		end

		local transferResult = self:transferData(pPlayer, pDatapad, faction, itemString)

		if(transferResult ~= self.errorCodes.SUCCESS) then
			return transferResult
		end


		playerObject:decreaseFactionStanding(gcwFaction, itemCost)

		if bonusItemCount then
			local bonusItems = self:getBonusItems(faction, itemString)
			if bonusItems ~= nil then
				for k, v in pairs(bonusItems) do
					transferResult = self:transferData(pPlayer, pDatapad, faction, v)
					if (transferResult ~= self.errorCodes.SUCCESS) then
						return transferResult
					end
				end
			end
		end
		return self.errorCodes.SUCCESS
	end)
end

function marauderScreenplay:transferData(pPlayer, pDatapad, faction, itemString)
	local pItem
	local templatePath = self:getTemplatePath(faction, itemString)

	if templatePath == nil then
		return self.errorCodes.TEMPLATEPATHERROR
	end

	local genPath = self:getControlledObjectTemplate(faction, itemString)

	if genPath == nil then
		return self.errorCodes.TEMPLATEPATHERROR
	end

	if (self:isHireling(faction, itemString)) then
		if (checkTooManyHirelings(pDatapad)) then
			return self.errorCodes.TOOMANYHIRELINGS
		end

		pItem = giveControlDevice(pDatapad, templatePath, genPath, -1, true)
	else
		pItem = giveControlDevice(pDatapad, templatePath, genPath, -1, false)
	end

	if pItem ~= nil then
		SceneObject(pItem):sendTo(pPlayer)
	else
		return self.errorCodes.GIVEERROR
	end

	return self.errorCodes.SUCCESS
end

function marauderScreenplay:transferItem(pPlayer, pInventory, faction, itemString)
	local templatePath = self:getTemplatePath(faction, itemString)

	if templatePath == nil then
		return self.errorCodes.TEMPLATEPATHERROR
	end

	local pItem = giveItem(pInventory, templatePath, -1)

	if (pItem == nil) then
		return self.errorCodes.GIVEERROR
	end

	if (self:isInstallation(faction, itemString)) then
		SceneObject(pItem):setObjectName("deed", itemString)
		local deed = LuaDeed(pItem)
		local genPath = self:getGeneratedObjectTemplate(faction, itemString)

		if genPath == nil then
			return self.errorCodes.TEMPLATEPATHERROR
		end

		deed:setGeneratedObjectTemplate(genPath)

		if TangibleObject(pItem) ~= nil then
			TangibleObject(pItem):setFaction(faction)
		end
	end

	return self.errorCodes.SUCCESS
end


function marauderScreenplay:getItemListTable(faction, screenID)
	local dataTable = self:getFactionDataTable(faction)
	if screenID == "fp_furniture" then
		return dataTable.furnitureList
	elseif screenID == "fp_weapons_armor" then
		return dataTable.weaponsArmorList
	elseif screenID == "fp_installations" then
		return dataTable.installationsList
	elseif screenID == "fp_uniforms" then
		return dataTable.uniformList
	elseif screenID == "fp_hirelings" then
		return dataTable.hirelingList
	elseif screenID == "fp_schematics" then
		return dataTable.deedListing
	end
end

function marauderScreenplay:handleBinaryLiquid(pNPC, pPlayer)
	return ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
		local faction = self:getFactionFromHashCode(CreatureObject(pPlayer):getFaction())

		if (pInventory == nil) then
			return self.errorCodes.INVENTORYERROR
		end

		local gcwFaction = self:getFactionBH(faction)

		local factionStanding = playerObject:getFactionStanding(gcwFaction)
		local itemCost = 30000

		if itemCost == nil then
			return self.errorCodes.ITEMCOST
		end

		if (factionStanding < (itemCost + self.minimumFactionStanding)) then
			return self.errorCodes.NOTENOUGHFACTION
		end

		local slotsremaining = SceneObject(pInventory):getContainerVolumeLimit() - SceneObject(pInventory):getContainerObjectsSize()


		if (slotsremaining < 1) then
			return self.errorCodes.INVENTORYFULL
		end

		createLoot(pInventory, "death_watch_bunker_ingredient_binary", 0, true)


		playerObject:decreaseFactionStanding(gcwFaction, itemCost)


		return self.errorCodes.SUCCESS
	end)
end
