factionRewardType = {
	armor = 1,
	weapon = 2,
	uniform = 3,
	furniture = 4,
	container = 5,
	terminal = 6,
	installation = 7,
	hireling = 8,
	deed = 9,
}

marauderRewardData = {
	weaponsArmorList = {
	"armor_marauder_helmet", "armor_marauder_chest", "armor_marauder_legs", "armor_marauder_boots", "armor_marauder_bicep_l", "armor_marauder_bicep_r", "armor_marauder_bracer_l", "armor_marauder_bracer_r", "armor_marauder_boots", "armor_marauder_gloves", "armor_marauder_belt", "armor_marauder_s02_helmet", "armor_marauder_s02_chest", "armor_marauder_s02_legs", "armor_marauder_s02_boots", "armor_marauder_s02_bicep_l", "armor_marauder_s02_bicep_r", "armor_marauder_s02_bracer_l", "armor_marauder_s02_bracer_r", "armor_marauder_s02_boots", "armor_marauder_s02_gloves", "armor_marauder_s02_belt", "armor_marauder_s03_helmet", "armor_marauder_s03_chest", "armor_marauder_s03_legs", "armor_marauder_s03_boots", "armor_marauder_s03_bicep_l", "armor_marauder_s03_bicep_r", "armor_marauder_s03_bracer_l", "armor_marauder_s03_bracer_r", "armor_marauder_s03_boots", "armor_marauder_s03_gloves", "armor_marauder_s03_belt"
	},

	weaponsArmor = {
		armor_marauder_helmet = { type=factionRewardType.armor, display="Marauder Armor Helmet", item="object/tangible/loot/loot_schematic/marauder_s01_helmet_schematic.iff", cost=2600},
		armor_marauder_chest = { type=factionRewardType.armor, display="Marauder Armor Chestplate", item="object/tangible/loot/loot_schematic/marauder_s01_chest_plate_schematic.iff", cost=2600},
		armor_marauder_legs = { type=factionRewardType.armor, display="Marauder Armor Legs", item="object/tangible/loot/loot_schematic/marauder_s01_leggings_schematic.iff", cost=2000},
		armor_marauder_boots = { type=factionRewardType.armor, display="Marauder Armor Boots", item="object/tangible/loot/loot_schematic/marauder_s01_boots_schematic.iff", cost=1600},
		armor_marauder_bicep_l = { type=factionRewardType.armor, display="Marauder Armor Left Bicep", item="object/tangible/loot/loot_schematic/marauder_s01_bicep_l_schematic.iff", cost=1600},
		armor_marauder_bicep_r = { type=factionRewardType.armor, display="Marauder Armor Right Bicep", item="object/tangible/loot/loot_schematic/marauder_s01_bicep_r_schematic.iff", cost=1600},
		armor_marauder_bracer_l = { type=factionRewardType.armor, display="Marauder Armor Left Bracer", item="object/tangible/loot/loot_schematic/marauder_s01_bracer_l_schematic.iff", cost=1600},
		armor_marauder_bracer_r = { type=factionRewardType.armor, display="Marauder Armor Right Bracer", item="object/tangible/loot/loot_schematic/marauder_s01_bracer_r_schematic.iff", cost=1600},
		armor_marauder_boots = { type=factionRewardType.armor, display="Marauder Armor Boots", item="object/tangible/loot/loot_schematic/marauder_s01_boots_schematic.iff", cost=1600},
		armor_marauder_gloves = { type=factionRewardType.armor, display="Marauder Armor Gloves", item="object/tangible/loot/loot_schematic/marauder_s01_gloves_schematic.iff", cost=1600},
		armor_marauder_belt = { type=factionRewardType.armor, display="Marauder Armor Belt", item="object/tangible/loot/loot_schematic/marauder_s01_belt_schematic.iff", cost=1600},
		armor_marauder_s02_helmet = { type=factionRewardType.armor, display="Marauder MK II Helmet", item="object/tangible/loot/loot_schematic/marauder_s02_helmet_schematic.iff", cost=2600},
		armor_marauder_s02_chest = { type=factionRewardType.armor, display="Marauder MK II Chestplate", item="object/tangible/loot/loot_schematic/marauder_s02_chest_plate_schematic.iff", cost=2600},
		armor_marauder_s02_legs = { type=factionRewardType.armor, display="Marauder MK II Legs", item="object/tangible/loot/loot_schematic/marauder_s02_leggings_schematic.iff", cost=2000},
		armor_marauder_s02_boots = { type=factionRewardType.armor, display="Marauder MK II Boots", item="object/tangible/loot/loot_schematic/marauder_s02_boots_schematic.iff", cost=1600},
		armor_marauder_s02_bicep_l = { type=factionRewardType.armor, display="Marauder MK II Left Bicep", item="object/tangible/loot/loot_schematic/marauder_s02_bicep_l_schematic.iff", cost=1600},
		armor_marauder_s02_bicep_r = { type=factionRewardType.armor, display="Marauder MK II Right Bicep", item="object/tangible/loot/loot_schematic/marauder_s02_bicep_r_schematic.iff", cost=1600},
		armor_marauder_s02_bracer_l = { type=factionRewardType.armor, display="Marauder MK II Left Bracer", item="object/tangible/loot/loot_schematic/marauder_s02_bracer_l_schematic.iff", cost=1600},
		armor_marauder_s02_bracer_r = { type=factionRewardType.armor, display="Marauder MK II Right Bracer", item="object/tangible/loot/loot_schematic/marauder_s02_bracer_r_schematic.iff", cost=1600},
		armor_marauder_s02_boots = { type=factionRewardType.armor, display="Marauder MK II Boots", item="object/tangible/loot/loot_schematic/marauder_s02_boots_schematic.iff", cost=1600},
		armor_marauder_s02_gloves = { type=factionRewardType.armor, display="Marauder MK II Gloves", item="object/tangible/loot/loot_schematic/marauder_s02_gloves_schematic.iff", cost=1600},
		armor_marauder_s02_belt = { type=factionRewardType.armor, display="Marauder MK II Belt", item="object/tangible/loot/loot_schematic/marauder_s02_belt_schematic.iff", cost=1600},
	armor_marauder_s03_helmet = { type=factionRewardType.armor, display="Marauder MK III Helmet", item="object/tangible/loot/loot_schematic/marauder_s03_helmet_schematic.iff", cost=2600},
		armor_marauder_s03_chest = { type=factionRewardType.armor, display="Marauder MK III Chestplate", item="object/tangible/loot/loot_schematic/marauder_s03_chest_plate_schematic.iff", cost=2600},
		armor_marauder_s03_legs = { type=factionRewardType.armor, display="Marauder MK III Legs", item="object/tangible/loot/loot_schematic/marauder_s03_leggings_schematic.iff", cost=2000},
		armor_marauder_s03_boots = { type=factionRewardType.armor, display="Marauder MK III Boots", item="object/tangible/loot/loot_schematic/marauder_s03_boots_schematic.iff", cost=1600},
		armor_marauder_s03_bicep_l = { type=factionRewardType.armor, display="Marauder MK III Left Bicep", item="object/tangible/loot/loot_schematic/marauder_s03_bicep_l_schematic.iff", cost=1600},
		armor_marauder_s03_bicep_r = { type=factionRewardType.armor, display="Marauder MK III Right Bicep", item="object/tangible/loot/loot_schematic/marauder_s03_bicep_r_schematic.iff", cost=1600},
		armor_marauder_s03_bracer_l = { type=factionRewardType.armor, display="Marauder MK III Left Bracer", item="object/tangible/loot/loot_schematic/marauder_s03_bracer_l_schematic.iff", cost=1600},
		armor_marauder_s03_bracer_r = { type=factionRewardType.armor, display="Marauder MK III Right Bracer", item="object/tangible/loot/loot_schematic/marauder_s03_bracer_r_schematic.iff", cost=1600},
		armor_marauder_s03_boots = { type=factionRewardType.armor, display="Marauder MK III Boots", item="object/tangible/loot/loot_schematic/marauder_s03_boots_schematic.iff", cost=1600},
		armor_marauder_s03_gloves = { type=factionRewardType.armor, display="Marauder MK III Gloves", item="object/tangible/loot/loot_schematic/marauder_s03_gloves_schematic.iff", cost=1600},
		armor_marauder_s03_belt = { type=factionRewardType.armor, display="Marauder MK III Belt", item="object/tangible/loot/loot_schematic/marauder_s03_belt_schematic.iff", cost=1600}
		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_s03"
	},

	installations = {
		--hq_s01_pvp_s03 = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_s03", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_s03","hq_s01_s03"} }
	},

	furnitureList = {
		"art_large", "art_large_s02", "art_small_s01", "art_small_s02", "art_small_s03", "art_small_s04"
	},

	furniture = {
		art_large = { type=factionRewardType.container, display="marauder Art Style 1", item="object/tangible/furniture/all/frn_all_decorative_lg_s1.iff", cost=500},
		art_large_s02 = { type=factionRewardType.container, display="marauder Art Style 2", item="object/tangible/furniture/all/frn_all_decorative_lg_s2.iff", cost=500},
		art_small_s01 = { type=factionRewardType.furniture, display="marauder Art Style 3", item="object/tangible/furniture/all/frn_all_decorative_sm_s1.iff", cost=200},
		art_small_s02 = { type=factionRewardType.furniture, display="marauder Art Style 4", item="object/tangible/furniture/all/frn_all_decorative_sm_s2.iff", cost=200},
		art_small_s03 =  { type=factionRewardType.container, display="marauder Art Style 5", item="object/tangible/furniture/all/frn_all_decorative_sm_s3.iff", cost=200},
		art_small_s04 = { type=factionRewardType.furniture, display="marauder Art Style 6", item="object/tangible/furniture/all/frn_all_decorative_sm_s4.iff", cost=200},
	},

	hirelingList = {
		--"marauder",
	},

	hirelings = {
		--marauder = { type=factionRewardType.hireling, display="Marauder Berserker", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="berserk_marauder", cost=150},
	},

	deedListing = {
		"mandolorian_jetpack", "merr_sonn", "vip_bunker"
	},

	deedList = {
		mandolorian_jetpack = { type=factionRewardType.deed, display="Mitrinomon Z-6 Jetpack Schematic", item="object/tangible/loot/loot_schematic/marauder_s01_jetpack_schematic.iff", cost=24000},
		merr_sonn = { type=factionRewardType.deed, display="Merr Sonn JT-12 Jetpack Schematic", item="object/tangible/loot/loot_schematic/marauder_s01_jetpack_s02_schematic.iff", cost=24000},
		vip_bunker = { type=factionRewardType.deed, display="VIP Bunker Deed", item="object/tangible/deed/player_house_deed/vipbunker_house_deed.iff", cost=12500},
	},
}
