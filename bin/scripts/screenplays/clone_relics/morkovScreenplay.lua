local ObjectManager = require("managers.object.object_manager")

morkovScreenplay = ScreenPlay:new {
	numberOfActs = 1,
	questString = "morkovScreenplay",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, phasenine = 1024}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("morkovScreenplay", true)

function morkovScreenplay:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
	end
end


function morkovScreenplay:spawnMobiles()
	local pMorkov = spawnMobile("naboo", "morkov", 180, -23.5, 1.6,  -3.3, 179, 1393870)
	createObserver(OBJECTDESTRUCTION, "morkovScreenplay", "morkovDead", pMorkov)
	spawnMobile("naboo", "morkov_droids", 180, -25.2, 1.6,  -4.4, 175, 1393870)
	spawnMobile("naboo", "morkov_droids", 180, -21.7, 1.6,  -4.3, 175, 1393870)
	local pUwo = spawnMobile("naboo", "uwo", 240, -8.3, 6.1,  -8.2, 90, 1685124)
	createObserver(OBJECTDESTRUCTION, "morkovScreenplay", "uwoDead", pUwo)
end

function morkovScreenplay:uwoDead(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "morkovScreenplay")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "morkovScreenplay")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("You find Transaction logs on Morkovs body, Return them to Mort")
							groupMember:setScreenPlayState(4, morkovScreenplay.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "morkovScreenplay")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("You find Transaction logs on Morkovs body, Return them to Mort")
						player:setScreenPlayState(4, morkovScreenplay.questString)
				end
			end
		end)
	return 0

end

function morkovScreenplay:morkovDead(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(32, "mortPhaseTwoNeutral")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(32, "mortPhaseTwoNeutral")
					local hasRebel = groupMember:hasScreenPlayState(4, "mortPhaseTwoRebel")
					local hasImperial = groupMember:hasScreenPlayState(16, "mortPhaseTwoImperial")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Return to Jabba")
							groupMember:setScreenPlayState(64, mortPhaseTwoNeutral.questString)
						end

						if(hasRebel == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Return to Leia")
							groupMember:setScreenPlayState(8, mortPhaseTwoRebel.questString)
						end

						if(hasImperial == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Return to The Emperor")
							groupMember:setScreenPlayState(32, mortPhaseTwoImperial.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(32, "mortPhaseTwoNeutral")
				local hasRebel = player:hasScreenPlayState(4, "mortPhaseTwoRebel")
				local hasImperial = player:hasScreenPlayState(16, "mortPhaseTwoImperial")
	
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Return to Jabba")
						player:setScreenPlayState(64, mortPhaseTwoNeutral.questString)
				end
					if(hasRebel == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Return to Leia")
						player:setScreenPlayState(8, mortPhaseTwoRebel.questString)
				end

					if(hasImperial == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Return to Major Raev")
						player:setScreenPlayState(32, mortPhaseTwoImperial.questString)
				end
			end
		end)
	return 0

end


function morkovScreenplay:getActivePlayerName()
	return self.questdata.activePlayerName
end

function morkovScreenplay:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function morkovScreenplay:firstQuestDead(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasQuest = player:hasScreenPlayState(2, "morkovScreenplay")

	if(hasQuest == true) then
		player:setScreenPlayState(8, morkovScreenplay.questString)
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("talus", "Mort", "", 436.4,  5.1, -2935.3, true, true, WAYPOINTTHEMEPARK, 1)
		CreatureObject(playerObject):sendSystemMessage("Return to Mort")
	end

	return 1
end

function morkovScreenplay:secondQuestDead(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasQuest = player:hasScreenPlayState(2, "morkovScreenplay")

	if(hasQuest == true) then
		player:setScreenPlayState(64, morkovScreenplay.questString)
		CreatureObject(playerObject):sendSystemMessage("Return to your faction representative")
	end

	return 1
end

morkov_convo_handler = Object:new {
	
 }

function morkov_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local mortStarted = creature:hasScreenPlayState(2, "cloneRelic1") --spoken to mort
			local questStarted = creature:hasScreenPlayState(2, "morkovScreenplay") 
			local questComplete = creature:hasScreenPlayState(4, "morkovScreenplay") 
			local neutralKillQuest = creature:hasScreenPlayState(32, "mortPhaseTwoNeutral") 
			local rebelKillQuest = creature:hasScreenPlayState(4, "mortPhaseTwoRebel") 
	
				if(neutralKillQuest == true or rebelKillQuest == true) then
					nextConversationScreen = conversation:getScreen("kill_quest")
				elseif (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (mortStarted == true) then
					nextConversationScreen = conversation:getScreen("greetings")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function morkov_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local mortNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "kill_quest" ) then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer) 
	end

	if ( screenID == "screen_two" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_3" ) then
		player:doAnimation("huh")
	end

	if ( screenID == "screen_five" ) then
		player:doAnimation("apologize")
	end

	if ( screenID == "screen_six" ) then
		player:doAnimation("shrug_hands")
	end

	if ( screenID == "s_22" ) then
		player:doAnimation("shake_head_no")
	end

	if ( screenID == "s_31" ) then
		player:doAnimation("nervous")
	end

	if ( screenID == "s_41" ) then
		player:doAnimation("rub_chin_thoughtful")
	end

	if ( screenID == "s_42" ) then
		player:doAnimation("shrug_hands")
	end

	if ( screenID == "s_43" ) then
		player:doAnimation("shake_head_no")
	end


	if ( screenID == "screen_seven" ) then
		player:setScreenPlayState(2, morkovScreenplay.questString)
		local pGhost = player:getPlayerObject()
		CreatureObject(conversingPlayer):sendSystemMessage("Meet with the Courier")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		PlayerObject(pGhost):addWaypoint("naboo", "Courier", "", 5126, -1535, 5, true, true, WAYPOINTTHEMEPARK, 1)

	end

	return conversationScreen
end

courier_convo_handler = Object:new {
	
 }

function courier_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "morkovScreenplay") 
			local questComplete = creature:hasScreenPlayState(4, "morkovScreenplay") 
				
				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("greetings")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function courier_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)


	if ( screenID == "s_4" ) then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer) 
		local pGuardOne = spawnMobile("naboo", "uwo_guard", 240, -8.3, 6.1,  -9.2, 90, 1685124)
		local pGuardTwo = spawnMobile("naboo", "uwo_guard", 240, -8.3, 6.1,  -7.2, 90, 1685124)
		CreatureObject(pGuardOne):engageCombat(conversingPlayer)
		CreatureObject(pGuardTwo):engageCombat(conversingPlayer)

	end

	return conversationScreen
end
