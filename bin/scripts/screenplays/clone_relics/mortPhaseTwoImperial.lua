local ObjectManager = require("managers.object.object_manager")

mortPhaseTwoImperial = ScreenPlay:new {
	numberOfActs = 1,
	questString = "mortPhaseTwoImperial",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, phasenine = 1024}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("mortPhaseTwoImperial", true)

function mortPhaseTwoImperial:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
	end
end


function mortPhaseTwoImperial:spawnMobiles()
	spawnMobile("naboo", "major_raev", 120, -10.6, 1.7, -8.9, -10, 1393881)
	spawnMobile("naboo", "stormtrooper", 120, -7.3, 2.3, 5.9, -179, 1393881)
	spawnMobile("naboo", "stormtrooper", 120, 7.3, 2.3, 5.9, -179, 1393881)
	spawnMobile("naboo", "imperial_corporal", 120, -10.6, 1.7, -6.6, 179, 1393881)
	local palpatine = spawnMobile("naboo", "palpatine_holo", 120, -8.3, -9.0, -8.7, 2, 1393887)
	spawnMobile("naboo", "stormtrooper", 120, -3.5, -9.0, -1.4, 90, 1393883)
	spawnMobile("naboo", "stormtrooper", 120, -3.5, -9.0, 2.9, 90, 1393883)
end


function mortPhaseTwoImperial:getActivePlayerName()
	return self.questdata.activePlayerName
end

function mortPhaseTwoImperial:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function mortPhaseTwoImperial:playerKilled(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	local palp = getCreatureObject(readData("palpid:"))
	player:killPlayer(palp, conversingPlayer, 1, false)
end

clone_relics_raev_convo_handler = Object:new {
	
 }

function clone_relics_raev_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local questDone = creature:hasScreenPlayState(64, "mortPhaseTwoImperial")
			local questTurnin = creature:hasScreenPlayState(32, "mortPhaseTwoImperial")
			local taskWait = creature:hasScreenPlayState(16, "mortPhaseTwoImperial")
			local waitForHoloCall = creature:hasScreenPlayState(8, "mortPhaseTwoImperial")
			local radioCycle = creature:hasScreenPlayState(4, "mortPhaseTwoImperial")
			local questReady = creature:hasScreenPlayState(2, "mortPhaseTwoImperial") --Killed Uwo and talked to Mort

				if (questDone == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin_screen")
				elseif (taskWait == true) then
					nextConversationScreen = conversation:getScreen("quest_wait_screen")
				elseif (waitForHoloCall == true) then
					nextConversationScreen = conversation:getScreen("holo_wait")	
				elseif (radioCycle == true) then
					nextConversationScreen = conversation:getScreen("s_4")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("greetings")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function clone_relics_raev_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "s_3" ) then
		pNPC:doAnimation("shrug_hands")
		player:setScreenPlayState(4, mortPhaseTwoImperial.questString)
	end

	if ( screenID == "s_4" ) then
		pNPC:doAnimation("nervous")
	end

	if ( screenID == "s_5" ) then
		pNPC:doAnimation("pound_fist_palm")
		player:doAnimation("slow_down")
		player:setScreenPlayState(8, mortPhaseTwoImperial.questString)
	end

	if ( screenID == "s_6" ) then
		player:doAnimation("salute2")
		player:setScreenPlayState(4, mortPhaseTwoImperial.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Travel to Naboo")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Morkov", "", 1763.2, 2669.7, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location
	end
	
	if ( screenID == "quest_complete_screen_final" ) then
		pNPC:doAnimation("nod")
		player:doAnimation("salute2")
		player:setScreenPlayState(64, mortPhaseTwoImperial.questString)
		createLoot(pInventory, "tiefighter_holo", 0, true)
		PlayerObject(pGhost):increaseFactionStanding("imperial", 3000)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		CreatureObject(conversingPlayer):sendSystemMessage("Return to Mort")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")

	end


	return conversationScreen
end

clone_relics_palpatine_convo_handler = Object:new {
	
 }

function clone_relics_palpatine_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local questDone = creature:hasScreenPlayState(32, "mortPhaseTwoImperial") 
			local questWait = creature:hasScreenPlayState(16, "mortPhaseTwoImperial") 
			local questReady = creature:hasScreenPlayState(8, "mortPhaseTwoImperial") 

				if (questDone == true) then
					nextConversationScreen = conversation:getScreen("quest_done")	
				elseif (questWait == true) then
					nextConversationScreen = conversation:getScreen("quest_wait_screen")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("greetings")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function clone_relics_palpatine_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "s_2" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_3" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_4" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_5" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_6" ) then
		pNPC:doAnimation("nod_head_once")
		player:doAnimation("bow5")
		player:setScreenPlayState(16, mortPhaseTwoImperial.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Kill Morkov")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Morkov", "", 1763.2, 2669.7, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location
	end

	if ( screenID == "s_6_b" ) then
		player:doAnimation("shake_head_no")
	end

	if ( screenID == "s_8" ) then
		player:doAnimation("apologize")
	end

	if ( screenID == "s_kill" ) then
		player:doAnimation("squirm")
		pNPC:doAnimation("point_forward")
		writeData("palpid:", pNPC:getObjectID())
		createEvent(1000, "mortPhaseTwoImperial", "playerKilled", conversingPlayer, "")
		--player:killPlayer(conversingNPC, conversingPlayer, 1, false)
	end


	return conversationScreen
end
