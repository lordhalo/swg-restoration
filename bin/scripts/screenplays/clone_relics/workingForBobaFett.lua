local ObjectManager = require("managers.object.object_manager")

workingForBobaFett = ScreenPlay:new {
	numberOfActs = 1,
	questString = "workingForBobaFett",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, phasenine = 1024}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("workingForBobaFett", true)

function workingForBobaFett:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
	end
end


function workingForBobaFett:spawnMobiles()
	spawnMobile("talus", "mort", 0, -3.6, -0.9, -5.3, 8, 3175391)

end


function workingForBobaFett:getActivePlayerName()
	return self.questdata.activePlayerName
end

function workingForBobaFett:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

clone_relics_1_convo_handler = Object:new {
	
 }

function clone_relics_1_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "workingForBobaFett") --spoken to mort
			local questTurnin = creature:hasScreenPlayState(4, "workingForBobaFett")  --spoken to morkov
			local uwoComplete = creature:hasScreenPlayState(4, "morkovScreenplay") --killed uwo
			local questDone = creature:hasScreenPlayState(64, "workingForBobaFett") --DoneDone

			local neutralSelectionWait = creature:hasScreenPlayState(2, "mortPhaseTwoNeutral") --Jabba Ark
			local neutralSelectionComplete = creature:hasScreenPlayState(128, "mortPhaseTwoNeutral") --Jabba Complete
			local imperialSelectionWait = creature:hasScreenPlayState(2, "mortPhaseTwoImperial") --Imperial Arl
			local imperialSelectionComplete = creature:hasScreenPlayState(64, "mortPhaseTwoImperial") --Imperial Complete
			local rebelSelectionWait = creature:hasScreenPlayState(2, "mortPhaseTwoRebel") --Rebel Ark
			local rebelSelectionComplete = creature:hasScreenPlayState(16, "mortPhaseTwoRebel") --Rebel Complete

				if (questDone == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (neutralSelectionComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen_jabba")
				elseif (neutralSelectionWait == true) then
					nextConversationScreen = conversation:getScreen("c_s_2_wait")
				elseif (imperialSelectionComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen_imp")
				elseif (imperialSelectionWait == true) then
					nextConversationScreen = conversation:getScreen("c_s_1_wait")
				elseif (rebelSelectionComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen_reb")
				elseif (rebelSelectionWait == true) then
					nextConversationScreen = conversation:getScreen("c_s_wait")	
				elseif (uwoComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_uwo_screen")	
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function clone_relics_1_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local mortNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "greet_friend" ) then
		player:doAnimation("apologize")
	end

	if ( screenID == "second_screen" ) then
		player:doAnimation("shrug_shoulders")
		mortNPC:doAnimation("snap_finger1")
	end

	if ( screenID == "third_screen" ) then
		player:doAnimation("rub_chin_thoughtful")
	end

	if ( screenID == "fourth_screen" ) then
		player:doAnimation("huh")
	end

	if ( screenID == "fifth_screen" ) then
		mortNPC:doAnimation("slump_head")
	end

	if ( screenID == "sixth_screen" ) then
		player:doAnimation("shake_head_no")
		mortNPC:doAnimation("point_to_self")
	end

	if ( screenID == "seventh_screen" ) then
		mortNPC:doAnimation("slump_head")
	end

	if ( screenID == "seventh_screen" ) then
		mortNPC:doAnimation("slump_head")
	end

	if ( screenID == "ninth_screen" ) then
		mortNPC:doAnimation("nod")
	end

	if ( screenID == "eleventh_screen" ) then
		mortNPC:doAnimation("slump_head")
	end

	if ( screenID == "twelfth_screen" ) then
		player:doAnimation("shrug_hands")
	end

	if ( screenID == "nineteenth_screen" ) then
		mortNPC:doAnimation("rub_chin_thoughtful")
	end

	if ( screenID == "c_5" or screenID == "c_4" or screenID == "c3" ) then
		mortNPC:doAnimation("belly_laugh")
	end

	if ( screenID == "accept_screen" ) then
		mortNPC:doAnimation("nod")
		player:setScreenPlayState(2, workingForBobaFett.questString)
		local pGhost = player:getPlayerObject()
		CreatureObject(conversingPlayer):sendSystemMessage("Travel to Naboo")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		PlayerObject(pGhost):addWaypoint("naboo", "Morkov", "", 1763.2, 2669.7, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location

	end

	if ( screenID == "c_s" ) then --Rebel
		player:doAnimation("nod")
		mortNPC:doAnimation("belly_laugh")
		player:setScreenPlayState(2, mortPhaseTwoRebel.questString)

	end

	if ( screenID == "c_s_1" ) then -- Imperial
		player:doAnimation("nod")
		mortNPC:doAnimation("belly_laugh")
		player:setScreenPlayState(16, workingForBobaFett.questString)
	end

	if ( screenID == "c_s_2" ) then --Neutral
		player:doAnimation("nod")
		mortNPC:doAnimation("belly_laugh")
		player:setScreenPlayState(2, mortPhaseTwoNeutral.questString)

	end

	if ( screenID == "complete_screen_51" ) then
		mortNPC:doAnimation("huh")
	end

	if ( screenID == "complete_screen_52" ) then
		player:doAnimation("nod")
	end

	if ( screenID == "complete_screen_34" ) then
		player:doAnimation("nod")
		mortNPC:doAnimation("nod")
	end

	if ( screenID == "complete_screen_final" ) then
		player:doAnimation("handshake_tandem")
		mortNPC:doAnimation("handshake_tandem")

		player:setScreenPlayState(64, workingForBobaFett.questString)
		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		
		createLoot(pInventory, "clone_gloves", 0, true)
		createLoot(pInventory, "clone_helmet", 0, true)
		createLoot(pInventory, "clone_chest_plate", 0, true)
		createLoot(pInventory, "clone_leggings", 0, true)
		createLoot(pInventory, "clone_boots", 0, true)
		createLoot(pInventory, "clone_belt", 0, true)
		createLoot(pInventory, "clone_bicep_l", 0, true)
		createLoot(pInventory, "clone_bicep_r", 0, true)
		createLoot(pInventory, "clone_bracer_l", 0, true)
		createLoot(pInventory, "clone_bracer_r", 0, true)

		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
	end

	return conversationScreen
end
