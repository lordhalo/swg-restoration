local ObjectManager = require("managers.object.object_manager")

mortPhaseTwoRebel = ScreenPlay:new {
	numberOfActs = 1,
	questString = "mortPhaseTwoRebel",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, phasenine = 1024}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("mortPhaseTwoRebel", true)

function mortPhaseTwoRebel:start()
	if (isZoneEnabled("dantooine")) then
		self:spawnMobiles()
	end
end


function mortPhaseTwoRebel:spawnMobiles()
	spawnMobile("dantooine", "ep3_leia", 0, -13.1, 1.0, -18.5, -78, 6555566)

end


function mortPhaseTwoRebel:getActivePlayerName()
	return self.questdata.activePlayerName
end

function mortPhaseTwoRebel:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

clone_relics_leia_convo_handler = Object:new {
	
 }

function clone_relics_leia_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local questDone = creature:hasScreenPlayState(16, "mortPhaseTwoRebel")
			local MorkovDead = creature:hasScreenPlayState(8, "mortPhaseTwoRebel")
			local questWait = creature:hasScreenPlayState(4, "mortPhaseTwoRebel")
			local questReady = creature:hasScreenPlayState(2, "mortPhaseTwoRebel") --Killed Uwo and talked to Mort

				if (questDone == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (MorkovDead == true) then
					nextConversationScreen = conversation:getScreen("s_7")	
				elseif (questWait == true) then
					nextConversationScreen = conversation:getScreen("quest_wait_screen")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("greetings")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function clone_relics_leia_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "s_3" ) then
		player:doAnimation("nod")
	end

	if ( screenID == "s_4" ) then
		pNPC:doAnimation("pound_fist_palm")
	end

	if ( screenID == "s_6" ) then
		player:doAnimation("salute2")
		player:setScreenPlayState(4, mortPhaseTwoRebel.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Travel to Naboo")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Morkov", "", 1763.2, 2669.7, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location
	end
	
	if ( screenID == "complete_screen_final" ) then
		pNPC:doAnimation("nod")
		player:doAnimation("salute2")
		player:setScreenPlayState(16, mortPhaseTwoRebel.questString)
		createLoot(pInventory, "xwing_holo", 0, true)
		PlayerObject(pGhost):increaseFactionStanding("rebel", 3000)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		CreatureObject(conversingPlayer):sendSystemMessage("Return to Mort")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")

	end


	return conversationScreen
end
