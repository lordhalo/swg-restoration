local ObjectManager = require("managers.object.object_manager")

mortPhaseTwoNeutral = ScreenPlay:new {
	numberOfActs = 1,
	questString = "mortPhaseTwoNeutral",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, phasefive = 64, phasesix = 128, phaseseven = 256, phaseeight = 512, phasenine = 1024}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("mortPhaseTwoNeutral", true)

function mortPhaseTwoNeutral:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
	end
end


function mortPhaseTwoNeutral:spawnMobiles()
	spawnMobile("tatooine", "jawl", 0, -4.9, 0.2, 124.3, 15, 1177466)
	spawnMobile("tatooine", "walda", 0, 8.6, -0.9, 5.3, 74, 1134560)

end


function mortPhaseTwoNeutral:getActivePlayerName()
	return self.questdata.activePlayerName
end

function mortPhaseTwoNeutral:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jawl_convo_handler = Object:new {
	
 }

function jawl_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local phaseThree = creature:hasScreenPlayState(16, "mortPhaseTwoNeutral") --Jabba Reaqdy
			local phaseTwo = creature:hasScreenPlayState(8, "mortPhaseTwoNeutral") --Walda complete
			local phaseOne = creature:hasScreenPlayState(4, "mortPhaseTwoNeutral") --Walda waiting
			local questReady = creature:hasScreenPlayState(2, "mortPhaseTwoNeutral") --Killed Uwo and talked to Mort

				if (phaseThree == true) then
					nextConversationScreen = conversation:getScreen("jawl_complete_screen")
				elseif (phaseTwo == true) then
					nextConversationScreen = conversation:getScreen("walda_complete_screen")	
				elseif (phaseOne == true) then
					nextConversationScreen = conversation:getScreen("jawl_waiting_screen")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("jawl_first_screen")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function jawl_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "s_3" ) then
		player:doAnimation("rub_chin_thoughtful")
		pNPC:doAnimation("shrug_shoulders")
	end

	if ( screenID == "s_4" ) then
		pNPC:doAnimation("nod")
	end

	if ( screenID == "s_5" ) then
		pNPC:doAnimation("rub_chin_thoughtful")
	end

	if ( screenID == "s_6" ) then
		player:doAnimation("nervous")
	end

	if ( screenID == "s_7" ) then
		pNPC:doAnimation("huh")
	end

	if ( screenID == "s_10" ) then
		pNPC:doAnimation("rub_chin_thoughtful")
	end

	if ( screenID == "s_11" ) then
		player:doAnimation("shrug_hands")
	end

	if ( screenID == "s_12" ) then
		player:doAnimation("goodbye")
		pNPC:doAnimation("nod")
		player:setScreenPlayState(4, mortPhaseTwoNeutral.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Meet Walda")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("tatooine", "Walda", "", -5164, -6565, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location
	end

	if ( screenID == "walda_complete_screen" ) then
		player:doAnimation("tiphat")
		pNPC:doAnimation("nod_head_once")
	end

	if ( screenID == "s_13" ) then
		player:doAnimation("nod")
	end

	
	if ( screenID == "s_14" ) then
		pNPC:doAnimation("nod")
		player:setScreenPlayState(16, mortPhaseTwoNeutral.questString)
		local pGhost = player:getPlayerObject()
		CreatureObject(conversingPlayer):sendSystemMessage("Speak with Jabba")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")

	end


	return conversationScreen
end

walda_convo_handler = Object:new {
	
 }

function walda_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			

			local phaseTwo = creature:hasScreenPlayState(8, "mortPhaseTwoNeutral") --Walda complete
			local questReady = creature:hasScreenPlayState(4, "mortPhaseTwoNeutral") --Walda waiting

				if (phaseTwo == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("walda_first_screen")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function walda_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "walda_first_screen" ) then
		pNPC:doAnimation("nod_head_once")
	end

	if ( screenID == "s_2" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_4" ) then
		pNPC:doAnimation("gesticulate_wildly")
	end

	if ( screenID == "s_5" ) then
		player:doAnimation("slow_down")
	end

	if ( screenID == "walda_final_screen" ) then
		player:doAnimation("huh")
		pNPC:doAnimation("laugh")
		player:setScreenPlayState(8, mortPhaseTwoNeutral.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Return to Jawl")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
	end



	return conversationScreen
end

jabba_convo_handler = Object:new {
	
 }

function jabba_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local questDone = creature:hasScreenPlayState(128, "mortPhaseTwoNeutral") --Ready
			local phaseThree = creature:hasScreenPlayState(64, "mortPhaseTwoNeutral") -- Waiting
			local phaseTwo = creature:hasScreenPlayState(32, "mortPhaseTwoNeutral") -- Waiting
			local questReady = creature:hasScreenPlayState(16, "mortPhaseTwoNeutral") --Ready

				if (questDone == true) then
					nextConversationScreen = conversation:getScreen("jabba_complete_screen")
				elseif (phaseThree == true) then
					nextConversationScreen = conversation:getScreen("morkov_dead_screen")
				elseif (phaseTwo == true) then
					nextConversationScreen = conversation:getScreen("jabba_waiting_screen")	
				elseif (questReady == true) then
					nextConversationScreen = conversation:getScreen("jabba_first_screen")	
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end


function jabba_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pNPC = LuaCreatureObject(conversingNPC)
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "s_3" ) then
		player:doAnimation("nod_head_once")
	end

	if ( screenID == "s_5" ) then
		player:doAnimation("shrug_hands")
	end

	if ( screenID == "s_6") then
		player:doAnimation("bow5")
	end

	if ( screenID == "s_7") then
		player:doAnimation("bow5")
		player:setScreenPlayState(32, mortPhaseTwoNeutral.questString)
		CreatureObject(conversingPlayer):sendSystemMessage("Travel to Naboo")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Morkov", "", 1763.2, 2669.7, 5, true, true, WAYPOINTTHEMEPARK, 1) -- Morkov's location
	end

	if ( screenID == "s_13" ) then
		player:doAnimation("bow5")
		player:setScreenPlayState(128, mortPhaseTwoNeutral.questString)
		createLoot(pInventory, "yt1300_holo", 0, true)
		PlayerObject(pGhost):increaseFactionStanding("jabba", 3000)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		CreatureObject(conversingPlayer):sendSystemMessage("Return to Mort")
		CreatureObject(conversingPlayer):playMusicMessage("sound/ui_button_random.snd")
	end



	return conversationScreen
end
