nabooTradeFederation = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "nabooTradeFederation",
	
	normalNPC = {
		{ "trade_federation_loyalist_bunker", 0, -13.9,-12.0,93.9,-87,9895380},
	},

	eliteNPC = {
		{ "trade_federation_loyalist_bunker", 0, -13.9,-12.0,93.9,-87,9895380}, -- Change to "Boss" like NPC
	},

	vrNPC = {
		{ "trade_federation_vr_bunker", 0, -13.9,-12.0,93.9,-87,9895380}, -- Change to "Boss" like NPC
	},


	droidekaPhaseOne = {
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -88.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -88.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -88.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -88.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -18.0, 0, 9895394},
	},

	droidekaPhaseTwo = {
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -18.0, 0, 9895394},
	},

	droidekaPhaseThree = {
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, 0.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -6.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -12.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -96.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -92.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_s_battle_droid", 0, -88.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -84.0, -44.0, -18.0, 0, 9895394},
		{ "trade_federation_battle_droid", 0, -80.0, -44.0, -18.0, 0, 9895394},

		{ "trade_federation_droideka", 0, -65.0, -44.0, 0.0, -90, 9895394},
		{ "trade_federation_droideka", 0, -65.0, -44.0, -6.0, -90, 9895394},
		{ "trade_federation_droideka", 0, -65.0, -44.0, -12.0, -90, 9895394},
		{ "trade_federation_droideka", 0, -65.0, -44.0, -18.0, -90, 9895394},
		{ "trade_federation_vr_bunker", 0, -65.0, -44.0, -8.0, -90, 9895394},

	},



}

registerScreenPlay("nabooTradeFederation", true)

function nabooTradeFederation:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		--self:rollSpawn()
		self:spawnDroidekaRoom()
	end
end

function nabooTradeFederation:spawnMobiles()
	
	--8 Pirates
	--9 Loyalist
	--3 Zealot
	--1 Avenger

	--First Room
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,0.0,-12.0,50.5,155,9895367)
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,8.0,-12.0,50.5,-155,9895367)
	spawnMobile("naboo", "trade_federation_battle_droid",360,3.5,-12.0,30.0,-179,9895367)
	--Second Room
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,10.0,-16.0,75.0,90,9895369)
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,25.0,-16.0,75.0,-90,9895369)
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,10.0,-16.0,79.0,90,9895369)
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,25.0,-16.0,79.0,-90,9895369)
	--Connecting hall
	spawnMobile("naboo", "trade_federation_battle_droid",360,17.5,-16.0,93.0,-90,9895370)
	--Larged room
	spawnMobile("naboo", "trade_federation_s_battle_droid",360,3.0,-16.0,103.0,0,9895372)
	spawnMobile("naboo", "trade_federation_s_battle_droid",360,3.0,-16.0,138.0,-179,9895372)
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,23.0,-16.0,120.0,-90,9895372)
	spawnMobile("naboo", "trade_federation_zealot_bunker",360,20.0,-16.0,120.0,90,9895372)
	--Add Computer
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,-16.0,-16.0,120.0,-90,9895372)
	--
	spawnMobile("naboo", "trade_federation_battle_droid",360,-24.0,-12.0,170.0,-179,9895374)
	spawnMobile("naboo", "trade_federation_battle_droid",360,-30.0,-12.0,170.0,-179,9895374)
	spawnMobile("naboo", "trade_federation_battle_droid",360,-24.0,-12.0,162.0,-179,9895374)
	spawnMobile("naboo", "trade_federation_battle_droid",360,-30.0,-12.0,162.0,-179,9895374)
	spawnMobile("naboo", "trade_federation_s_battle_droid",360,-27.0,-12.0,144.0,0,9895374)
	--Droideka Guard
	spawnMobile("naboo", "trade_federation_droideka",360,-36.0,-12.0,100.0,-179,9895382)
	-- RNG Spawn Room
	local eliteTrigger = spawnMobile("naboo", "trade_federation_loyalist_bunker",0 ,-36.0,-12.0,130.0,179,9895383) 
	createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "rollSpawn", eliteTrigger)
	--Circle room
	spawnMobile("naboo", "trade_federation_zealot_bunker",360,-11.5,-12.0,100.0,-179,9895380)
	spawnMobile("naboo", "trade_federation_zealot_bunker",360,-16.0,-12.0,95.0,90,9895380)
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,-9.0,-12.0,88.0,-40,9895380)
	--Hall
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,-13.5,-16.0,63.0,0,9895384)
	--Hall 2
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,-45.5,-24.0,63.0,-90,9895385)
	--Pre Group Rooms
	spawnMobile("naboo", "trade_federation_avenger_bunker",360,-73.5,-28.0,72.0,-90,9895388)
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,-73.5,-28.0,68.0,-90,9895388)
	spawnMobile("naboo", "trade_federation_pirate_bunker",360,-87.5,-28.0,72.0,90,9895388)
	spawnMobile("naboo", "trade_federation_loyalist_bunker",360,-87.5,-28.0,68.0,90,9895388)
	--Larger Room
	--Hall
	spawnMobile("naboo", "trade_federation_s_battle_droid",360,-60.0,-20.0,151.0,90,9895377)
	--Pre Group Room
	spawnMobile("naboo", "trade_federation_avenger_bunker",360,-75.0,-20.0,110.0,-90,9895379)
	spawnMobile("naboo", "trade_federation_avenger_bunker",360,-102.0,-20.0,110.0,90,9895379)
	--Pre Group Room Hall
	spawnMobile("naboo", "trade_federation_avenger_bunker",360,-112.0,-36.0,77.0,-90,9895390)

end

function nabooTradeFederation:spawnDroidekaRoom()

	local mobileTable = self.droidekaPhaseOne

	for i = 1, #mobileTable, 1 do
		local normalTrigger = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
		createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "droidekaRoomPhaseTwoTrigger", normalTrigger)
	end

end

function nabooTradeFederation:spawnDroidekaRoomTwo()

	local mobileTable = self.droidekaPhaseTwo

	for i = 1, #mobileTable, 1 do
		local phaseTwo = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
		createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "droidekaRoomPhaseThreeTrigger", phaseTwo)
	end

end

function nabooTradeFederation:spawnDroidekaRoomThree()

	local mobileTable = self.droidekaPhaseThree

	for i = 1, #mobileTable, 1 do
		local normalTrigger = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
		createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "droidekaRoomPhaseResetTrigger", normalTrigger)
	end

end

function nabooTradeFederation:droidekaRoomPhaseTwoTrigger()
	writeData("tradeFedBunker:droidekaRoom",readData("tradeFedBunker:droidekaRoom") + 1)

	if(readData("tradeFedBunker:droidekaRoom") == 20) then
		createEvent(2 * 60 * 1000, "nabooTradeFederation", "spawnDroidekaRoomTwo", pMobile, "")
		writeData("tradeFedBunker:droidekaRoom",0)
	end
	return 0
end

function nabooTradeFederation:droidekaRoomPhaseThreeTrigger()
	writeData("tradeFedBunker:phaseTwo",readData("tradeFedBunker:phaseTwo") + 1)

	if(readData("tradeFedBunker:phaseTwo") == 20) then
		createEvent(2 * 60 * 1000, "nabooTradeFederation", "spawnDroidekaRoomThree", pMobile, "")
		writeData("tradeFedBunker:phaseTwo",0)
	end
	return 0
end

function nabooTradeFederation:droidekaRoomPhaseResetTrigger()
	writeData("tradeFedBunker:phaseThree",readData("tradeFedBunker:phaseThree") + 1)

	if(readData("tradeFedBunker:phaseThree") == 24) then
		createEvent(10 * 60 * 1000, "nabooTradeFederation", "spawnDroidekaRoom", pMobile, "")
		writeData("tradeFedBunker:phaseThree",0)
	end
	return 0
end

function nabooTradeFederation:rollSpawn(eliteTrigger)
	local pNPC = eliteTrigger
	local random = math.random(100)
			
	if random > 30 then
		createEvent(600000, "nabooTradeFederation", "spawnNormal", pNPC, "")
	elseif random < 3 then
		createEvent(600000, "nabooTradeFederation", "spawnVR", pNPC, "")
	elseif random <= 30 then
		createEvent(600000, "nabooTradeFederation", "spawnElite", pNPC, "")
	else
		createEvent(600000, "nabooTradeFederation", "spawnNormal", pNPC, "")
	end
	return 0
end

function nabooTradeFederation:spawnNormal()

	local normalTrigger = spawnMobile("naboo", "trade_federation_loyalist_bunker",0 ,-36.0,-12.0,130.0,179,9895383) 
	createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "rollSpawn", normalTrigger)
	return 0

end

function nabooTradeFederation:spawnElite()

	local eliteTrigger = spawnMobile("naboo", "trade_federation_droideka",0 ,-36.0,-12.0,130.0,179,9895383) 
	createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "rollSpawn", eliteTrigger)
	return 0
end

function nabooTradeFederation:spawnVR()

	local vrTrigger = spawnMobile("naboo", "trade_federation_vr_bunker_easy",0 ,-36.0,-12.0,130.0,179,9895383) 
	spatialChat(vrTrigger, "What is the meaning of this?")
	createObserver(OBJECTDESTRUCTION, "nabooTradeFederation", "rollSpawn", vrTrigger)
	return 0

end
