local ObjectManager = require("managers.object.object_manager")

totalWarImperialElite = ScreenPlay:new {
	numberOfActs = 1,
	questString = "totalWarImperialElite",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("totalWarImperialElite", true)

function totalWarImperialElite:start()

end

--Setup

function totalWarImperialElite:getActivePlayerName()
	return self.questdata.activePlayerName
end

function totalWarImperialElite:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function totalWarImperialElite:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(totalWarImperialElite.states.quest.phasetwo, totalWarImperialElite.questString)
	player:removeScreenPlayState(totalWarImperialElite.states.quest.phaseone, totalWarImperialElite.questString)
	player:removeScreenPlayState(totalWarImperialElite.states.quest.intro, totalWarImperialElite.questString)

end

function totalWarImperialElite:notifyKilledHuntTarget(pPlayer, pVictim)
	local player = LuaCreatureObject(pPlayer)
	local huntTarget = readScreenPlayData(pPlayer, "totalWarImperialElite", "huntTarget")
	local targetCount = tonumber(readScreenPlayData(pPlayer, "totalWarImperialElite", "huntTargetCount"))
	local targetGoal = tonumber(readScreenPlayData(pPlayer, "totalWarImperialElite", "huntTargetGoal"))
	local targetList = HelperFuncs:splitString(huntTarget, ";")
	if (huntTarget == SceneObject(pVictim):getObjectName() or HelperFuncs:tableContainsValue(targetList, SceneObject(pVictim):getObjectName())) then	
		targetCount = targetCount + 1
		writeScreenPlayData(pPlayer, "totalWarImperialElite", "huntTargetCount", targetCount)
		CreatureObject(pPlayer):sendSystemMessage("Total War Progress: " .. readScreenPlayData(pPlayer, "totalWarImperialElite", "huntTargetCount") .. "/ 5")

		if (targetCount >= targetGoal) then
			player:setScreenPlayState(4, totalWarImperialElite.questString)
			return 1
		end
	end

	return 0
end
