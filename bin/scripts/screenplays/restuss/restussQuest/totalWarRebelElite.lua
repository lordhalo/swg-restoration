local ObjectManager = require("managers.object.object_manager")

totalWarRebelElite = ScreenPlay:new {
	numberOfActs = 1,
	questString = "totalWarRebelElite",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("totalWarRebelElite", true)

function totalWarRebelElite:start()

end

--Setup

function totalWarRebelElite:getActivePlayerName()
	return self.questdata.activePlayerName
end

function totalWarRebelElite:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function totalWarRebelElite:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(totalWarRebelElite.states.quest.phasetwo, totalWarRebelElite.questString)
	player:removeScreenPlayState(totalWarRebelElite.states.quest.phaseone, totalWarRebelElite.questString)
	player:removeScreenPlayState(totalWarRebelElite.states.quest.intro, totalWarRebelElite.questString)

end

function totalWarRebelElite:notifyKilledHuntTarget(pPlayer, pVictim)
	local player = LuaCreatureObject(pPlayer)
	local huntTarget = readScreenPlayData(pPlayer, "totalWarRebelElite", "huntTarget")
	local targetCount = tonumber(readScreenPlayData(pPlayer, "totalWarRebelElite", "huntTargetCount"))
	local targetGoal = tonumber(readScreenPlayData(pPlayer, "totalWarRebelElite", "huntTargetGoal"))
	local targetList = HelperFuncs:splitString(huntTarget, ";")
	if (huntTarget == SceneObject(pVictim):getObjectName() or HelperFuncs:tableContainsValue(targetList, SceneObject(pVictim):getObjectName())) then	
		targetCount = targetCount + 1
		writeScreenPlayData(pPlayer, "totalWarRebelElite", "huntTargetCount", targetCount)
		CreatureObject(pPlayer):sendSystemMessage("Total War Progress: " .. readScreenPlayData(pPlayer, "totalWarRebelElite", "huntTargetCount") .. "/ 5")

		if (targetCount >= targetGoal) then
			player:setScreenPlayState(4, totalWarRebelElite.questString)
			return 1
		end
	end

	return 0
end
