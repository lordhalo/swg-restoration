local ObjectManager = require("managers.object.object_manager")

sensorArrayImperial = ScreenPlay:new {
	numberOfActs = 1,
	questString = "sensorArrayImperial",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("sensorArrayImperial", true)

function sensorArrayImperial:start()

end

--Setup

function sensorArrayImperial:getActivePlayerName()
	return self.questdata.activePlayerName
end

function sensorArrayImperial:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function sensorArrayImperial:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(sensorArrayImperial.states.quest.phasetwo, sensorArrayImperial.questString)
	player:removeScreenPlayState(sensorArrayImperial.states.quest.phaseone, sensorArrayImperial.questString)
	player:removeScreenPlayState(sensorArrayImperial.states.quest.intro, sensorArrayImperial.questString)

end


captain_exov_convo_handler = Object:new {
	
 }

function captain_exov_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "sensorArrayImperial")
			local questTurnin = creature:hasScreenPlayState(4, "sensorArrayImperial")
			local questComplete = creature:hasScreenPlayState(8, "sensorArrayImperial")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function captain_exov_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, sensorArrayImperial.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, sensorArrayImperial.questString)
		sensorArrayImperial:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("imperial_restuss", 10)
		PlayerObject(pGhost):increaseFactionStanding("imperial", 50)
	
	end
	


	return conversationScreen
end

