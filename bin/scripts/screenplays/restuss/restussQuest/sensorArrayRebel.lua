local ObjectManager = require("managers.object.object_manager")

sensorArrayRebel = ScreenPlay:new {
	numberOfActs = 1,
	questString = "sensorArrayRebel",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},


}
registerScreenPlay("sensorArrayRebel", true)

function sensorArrayRebel:start()

end

--Setup

function sensorArrayRebel:getActivePlayerName()
	return self.questdata.activePlayerName
end

function sensorArrayRebel:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


function sensorArrayRebel:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(sensorArrayRebel.states.quest.phasetwo, sensorArrayRebel.questString)
	player:removeScreenPlayState(sensorArrayRebel.states.quest.phaseone, sensorArrayRebel.questString)
	player:removeScreenPlayState(sensorArrayRebel.states.quest.intro, sensorArrayRebel.questString)

end


captain_klork_convo_handler = Object:new {
	
 }

function captain_klork_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local questStarted = creature:hasScreenPlayState(2, "sensorArrayRebel")
			local questTurnin = creature:hasScreenPlayState(4, "sensorArrayRebel")
			local questComplete = creature:hasScreenPlayState(8, "sensorArrayRebel")

				if (questComplete == true) then
					nextConversationScreen = conversation:getScreen("quest_done")
				elseif (questTurnin == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (questStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function captain_klork_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, sensorArrayRebel.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, sensorArrayRebel.questString)
		sensorArrayRebel:removeDailyTimer(conversingPlayer)
		PlayerObject(pGhost):increaseFactionStanding("rebel_restuss", 10)
		PlayerObject(pGhost):increaseFactionStanding("rebel", 50)
		
	end
	


	return conversationScreen
end

