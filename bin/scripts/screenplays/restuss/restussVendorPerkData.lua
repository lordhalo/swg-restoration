factionRewardType = {
	armor = 1,
	weapon = 2,
	uniform = 3,
	furniture = 4,
	container = 5,
	terminal = 6,
	installations = 7,
	hireling = 8,
	deed = 9,
}

restussRebelRewardData = {
	weaponsArmorList = {
		"armor_infiltrator_s01_helmet", "armor_infiltrator_s01_chest", "armor_infiltrator_s01_legs", "armor_infiltrator_s01_boots", 		"armor_infiltrator_s01_bicep_l", "armor_infiltrator_s01_bicep_r", "armor_infiltrator_s01_bracer_l", "armor_infiltrator_s01_bracer_r", 			"armor_infiltrator_s01_boots", "armor_infiltrator_s01_gloves", "armor_infiltrator_s01_belt","armor_infiltrator_s02_helmet", "armor_infiltrator_s02_chest", "armor_infiltrator_s02_legs", "armor_infiltrator_s02_boots", "armor_infiltrator_s02_bicep_l", "armor_infiltrator_s02_bicep_r", "armor_infiltrator_s02_bracer_l", "armor_infiltrator_s02_bracer_r", "armor_infiltrator_s02_boots", "armor_infiltrator_s02_gloves", "armor_infiltrator_s02_belt",

	},

	weaponsArmor = {
		armor_infiltrator_s01_helmet = { type=factionRewardType.deed, display="Infiltrator Armor Helmet", item="object/tangible/loot/loot_schematic/infiltrator_s01_helmet_schematic.iff", cost=1500},
		armor_infiltrator_s01_chest = { type=factionRewardType.armor, display="Infiltrator Armor Chestplate", item="object/tangible/loot/loot_schematic/infiltrator_s01_chest_plate_schematic.iff", cost=1500},
		armor_infiltrator_s01_legs = { type=factionRewardType.armor, display="Infiltrator Armor Legs", item="object/tangible/loot/loot_schematic/infiltrator_s01_leggings_schematic.iff", cost=1500},
		armor_infiltrator_s01_boots = { type=factionRewardType.armor, display="Infiltrator Armor Boots", item="object/tangible/loot/loot_schematic/infiltrator_s01_boots_schematic.iff", cost=1500},
		armor_infiltrator_s01_bicep_l = { type=factionRewardType.armor, display="Infiltrator Armor Left Bicep", item="object/tangible/loot/loot_schematic/infiltrator_s01_bicep_l_schematic", cost=1500},
		armor_infiltrator_s01_bicep_r = { type=factionRewardType.armor, display="Infiltrator Armor Right Bicep", item="object/tangible/loot/loot_schematic/infiltrator_s01_bicep_r_schematic", cost=1500},
		armor_infiltrator_s01_bracer_l = { type=factionRewardType.armor, display="Infiltrator Armor Left Bracer", item="object/tangible/loot/loot_schematic/infiltrator_s01_bracer_l_schematic", cost=1500},
		armor_infiltrator_s01_bracer_r = { type=factionRewardType.armor, display="Infiltrator Armor Right Bracer", item="object/tangible/loot/loot_schematic/infiltrator_s01_bracer_r_schematic", cost=1500},
		armor_infiltrator_s01_boots = { type=factionRewardType.armor, display="Infiltrator Armor Boots", item="object/tangible/loot/loot_schematic/infiltrator_s01_boots_schematic", cost=1500},
		armor_infiltrator_s01_gloves = { type=factionRewardType.armor, display="Infiltrator Armor Gloves", item="object/tangible/loot/loot_schematic/infiltrator_s01_gloves_schematic", cost=1500},
		armor_infiltrator_s01_belt = { type=factionRewardType.armor, display="Infiltrator Armor Belt", item="object/tangible/loot/loot_schematic/infiltrator_s01_belt_schematic", cost=1500},
		armor_infiltrator_s02_helmet = { type=factionRewardType.armor, display="Infiltrator Armor Helmet Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_helmet_schematic.iff", cost=1500},
		armor_infiltrator_s02_chest = { type=factionRewardType.armor, display="Infiltrator Armor Chestplate Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_chest_plate_schematic.iff", cost=1500},
		armor_infiltrator_s02_legs = { type=factionRewardType.armor, display="Infiltrator Armor Legs Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_leggings_schematic.iff", cost=1500},
		armor_infiltrator_s02_boots = { type=factionRewardType.armor, display="Infiltrator Armor Boots Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_boots_schematic.iff", cost=1500},
		armor_infiltrator_s02_bicep_l = { type=factionRewardType.armor, display="Infiltrator Armor Left Bicep Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_bicep_l_schematic", cost=1500},
		armor_infiltrator_s02_bicep_r = { type=factionRewardType.armor, display="Infiltrator Armor Right Bicep Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_bicep_r_schematic", cost=1500},
		armor_infiltrator_s02_bracer_l = { type=factionRewardType.armor, display="Infiltrator Armor Left Bracer Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_bracer_l_schematic", cost=1500},
		armor_infiltrator_s02_bracer_r = { type=factionRewardType.armor, display="Infiltrator Armor Right Bracer Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_bracer_r_schematic", cost=1500},
		armor_infiltrator_s02_boots = { type=factionRewardType.armor, display="Infiltrator Armor Boots Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_boots_schematic", cost=1500},
		armor_infiltrator_s02_gloves = { type=factionRewardType.armor, display="Infiltrator Armor Gloves Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_gloves_schematic", cost=1500},
		armor_infiltrator_s02_belt = { type=factionRewardType.armor, display="Infiltrator Armor Belt Style 2", item="object/tangible/loot/loot_schematic/infiltrator_s02_belt_schematic", cost=1500}
		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	furnitureList = {
		--"tech_armoire"
	},

	furniture = {
		--tech_armoire = { type=factionRewardType.container, display="@container_name:tech_armoire", item="object/tangible/furniture/technical/armoire_s01.iff", cost=1400},
	},

	hirelingList = {
		--"rebel_trooper",
	},

	hirelings = {
		--rebel_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:command_barc", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="rebel_trooper", cost=150},
	},

	deedListing = {
		"command_barc", "rebel_spire", "resource_deed"
	},

	deedList = {
		command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/tangible/deed/vehicle_deed/barc_speeder_deed_reb.iff", cost=100},	
		rebel_spire = { type=factionRewardType.deed, display="Rebel Spire", item="object/tangible/deed/player_house_deed/rebel_house_deed.iff", cost=200},
		resource_deed = { type=factionRewardType.deed, display="Gambling Crate of Free Resources", item="object/tangible/veteran_reward/resource.iff", cost=150}
	},
}

restussImperialRewardData = {
	weaponsArmorList = {
		--"armor_stormtrooper_helmet", 
		"armor_galactic_marine_helmet", "armor_galactic_marine_chest", "armor_galactic_marine_legs", "armor_galactic_marine_boots", 		"armor_galactic_marine_bicep_l", "armor_galactic_marine_bicep_r", "armor_galactic_marine_bracer_l", "armor_galactic_marine_bracer_r", 			"armor_galactic_marine_boots", "armor_galactic_marine_gloves", "armor_galactic_marine_belt",
	},

	weaponsArmor = {
		--armor_stormtrooper_bicep_l = { type=factionRewardType.armor, display="@wearables_name:armor_stormtrooper_bicep_l", item="object/tangible/loot/factional_schematic/imperial_stormtrooper_bicep_l_schematic.iff",cost=1400}
		armor_galactic_marine_helmet = { type=factionRewardType.armor, display="Galactic Marine Armor Helmet", item="object/tangible/loot/loot_schematic/galactic_marine_helmet_schematic.iff", cost=1500},
		armor_galactic_marine_chest = { type=factionRewardType.armor, display="Galactic Marine Armor Chestplate", item="object/tangible/loot/loot_schematic/galactic_marine_chest_plate_schematic.iff", cost=1500},
		armor_galactic_marine_legs = { type=factionRewardType.armor, display="Galactic Marine Armor Legs", item="object/tangible/loot/loot_schematic/galactic_marine_leggings_schematic.iff", cost=1500},
		armor_galactic_marine_boots = { type=factionRewardType.armor, display="Galactic Marine Armor Boots", item="object/tangible/loot/loot_schematic/galactic_marine_boots_schematic.iff", cost=1500},
		armor_galactic_marine_bicep_l = { type=factionRewardType.armor, display="Galactic Marine Armor Left Bicep", item="object/tangible/loot/loot_schematic/galactic_marine_bicep_l_schematic", cost=1500},
		armor_galactic_marine_bicep_r = { type=factionRewardType.armor, display="Galactic Marine Armor Right Bicep", item="object/tangible/loot/loot_schematic/galactic_marine_bicep_r_schematic", cost=1500},
		armor_galactic_marine_bracer_l = { type=factionRewardType.armor, display="Galactic Marine Armor Left Bracer", item="object/tangible/loot/loot_schematic/galactic_marine_bracer_l_schematic", cost=1500},
		armor_galactic_marine_bracer_r = { type=factionRewardType.armor, display="Galactic Marine Armor Right Bracer", item="object/tangible/loot/loot_schematic/galactic_marine_bracer_r_schematic", cost=1500},
		armor_galactic_marine_boots = { type=factionRewardType.armor, display="Galactic Marine Armor Boots", item="object/tangible/loot/loot_schematic/galactic_marine_boots_schematic", cost=1500},
		armor_galactic_marine_gloves = { type=factionRewardType.armor, display="Galactic Marine Armor Gloves", item="object/tangible/loot/loot_schematic/galactic_marine_gloves_schematic", cost=1500},
		armor_galactic_marine_belt = { type=factionRewardType.armor, display="Galactic Marine Armor Belt", item="object/tangible/loot/loot_schematic/galactic_marine_belt_schematic", cost=1500},
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_imperial"
	},

	installations = {
		--hq_s01_pvp_imperial = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_imperial", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_imperial","hq_s01_imperial"} }
	},

	furnitureList = {
		--"data_terminal_s1"
	},

	furniture = {
		--data_terminal_s1 = { type=factionRewardType.terminal, display="@frn_n:frn_data_terminal", item="object/tangible/furniture/imperial/data_terminal_s1.iff",cost=560}
	},

	hirelingList = {
		--"assault_trooper"
	},

	hirelings = {
		--assault_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:assault_trooper", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="assault_trooper", cost=420}
	},

	deedListing = {
		"command_barc", "imperial_spire", "resource_deed"
	},

	deedList = {
		command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/tangible/deed/vehicle_deed/barc_speeder_deed_imp.iff", cost=100},	
		imperial_spire = { type=factionRewardType.deed, display="Imperial Spire", item="object/tangible/deed/player_house_deed/emperors_house_deed.iff", cost=200},
		resource_deed = { type=factionRewardType.deed, display="Gambling Crate of Free Resources", item="object/tangible/veteran_reward/resource.iff", cost=150}
	},
}
