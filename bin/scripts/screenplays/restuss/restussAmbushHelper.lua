local ObjectManager = require("managers.object.object_manager")

restussAmbushHelper = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "restussAmbushHelper",

}
  
registerScreenPlay("restussAmbushHelper", true)


function restussAmbushHelper:enterAmbushOne(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushOne") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5330, 80, 5750,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5348, 80, 5720,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5338, 80, 5778,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushOne", 1)
		createEvent(12000, "restussHelper", "resetAmbushOne", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5330, 80, 5750,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5348, 80, 5720,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5338, 80, 5778,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushOne", 1)
		createEvent(12000, "restussHelper", "resetAmbushOne", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushOne()
	writeData("restussManager:ambushOne", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushOne(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushTwo(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushTwo") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5128, 80, 5548,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5158, 80, 5534,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5160, 80, 5562,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushTwo", 1)
		createEvent(12000, "restussHelper", "resetAmbushTwo", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5128, 80, 5548,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5158, 80, 5534,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5160, 80, 5562,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushTwo", 1)
		createEvent(12000, "restussHelper", "resetAmbushTwo", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushTwo()
	writeData("restussManager:ambushTwo", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushTwo(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushThree(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushThree") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5210, 80, 5770,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5192, 80, 5758,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5180, 80, 5788,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushThree", 1)
		createEvent(12000, "restussHelper", "resetAmbushThree", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5210, 80, 5770,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5192, 80, 5758,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5180, 80, 5788,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushThree", 1)
		createEvent(12000, "restussHelper", "resetAmbushThree", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushThree()
	writeData("restussManager:ambushThree", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushThree(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushFour(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushFour") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5482, 80, 5830,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5472, 80, 5868,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5528, 80, 5850,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushFour", 1)
		createEvent(12000, "restussHelper", "resetAmbushFour", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5482, 80, 5830,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5472, 80, 5868,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5528, 80, 5850,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushFour", 1)
		createEvent(12000, "restussHelper", "resetAmbushFour", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushFour()
	writeData("restussManager:ambushFour", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushFour(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushFive(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushFive") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5370, 80, 5510,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5375, 80, 5510,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5380, 80, 5510,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushFive", 1)
		createEvent(12000, "restussHelper", "resetAmbushFive", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5370, 80, 5510,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5375, 80, 5510,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5380, 80, 5510,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushFive", 1)
		createEvent(12000, "restussHelper", "resetAmbushFive", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushFive()
	writeData("restussManager:ambushFive", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushFive(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushSix(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushSix") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5618, 80, 5678,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5586, 80, 5694,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5610, 80, 5750,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushSix", 1)
		createEvent(12000, "restussHelper", "resetAmbushSix", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5618, 80, 5678,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5586, 80, 5694,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5610, 80, 5750,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushSix", 1)
		createEvent(12000, "restussHelper", "resetAmbushSix", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushSix()
	writeData("restussManager:ambushSix", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushSix(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushSeven(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushSeven") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5298, 80, 5590,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5310, 80, 5604,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5290, 80, 5620,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushSeven", 1)
		createEvent(12000, "restussHelper", "resetAmbushSeven", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5298, 80, 5590,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5310, 80, 5604,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5290, 80, 5620,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushSeven", 1)
		createEvent(12000, "restussHelper", "resetAmbushSeven", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushSeven()
	writeData("restussManager:ambushSeven", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushSeven(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushEight(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushEight") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5010, 80, 5680,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5010, 80, 5685,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5010, 80, 5690,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushEight", 1)
		createEvent(12000, "restussHelper", "resetAmbushEight", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5010, 80, 5680,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5010, 80, 5685,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5010, 80, 5690,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushEight", 1)
		createEvent(12000, "restussHelper", "resetAmbushEight", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushEight()
	writeData("restussManager:ambushEight", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushEight(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end

function restussAmbushHelper:enterAmbushNine(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (readData("restussManager:ambushNine") == 1) then
		return 0
	end

	local pPlayer = LuaCreatureObject(pMovingObject)
	
	if (pPlayer:isAiAgent()) then
		return 0
	end

	local random = math.random(100)

	if random >= 50 then
		return 0
	end


	if (readData("restussManager:imperialOwned") == 1 and pPlayer:isRebel()) then
		local pNpc = spawnMobile("rori", "scout_trooper_restuss", 0, 5536, 80, 5600,  1, 0)
		local pNpc2 = spawnMobile("rori", "stormtrooper_restuss", 0, 5510, 80, 5626,  1, 0)
		local pNpc3 = spawnMobile("rori", "stormtrooper_restuss", 0, 5536, 80, 5632,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushNine", 1)
		createEvent(12000, "restussHelper", "resetAmbushNine", "", "")
	elseif (readData("restussManager:rebelOwned") == 1 and pPlayer:isImperial()) then
		local pNpc = spawnMobile("rori", "rebel_scout_restuss", 0, 5536, 80, 5600,  1, 0)
		local pNpc2 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5510, 80, 5626,  1, 0)
		local pNpc3 = spawnMobile("rori", "rebel_trooper_restuss", 0, 5536, 80, 5632,  1, 0)
		CreatureObject(pNpc):engageCombat(pMovingObject)
		CreatureObject(pNpc2):engageCombat(pMovingObject)
		CreatureObject(pNpc3):engageCombat(pMovingObject)

		local message
		local int = getRandomNumber(1,6)
		if int == 1 then message = "Stop Right There!"
		elseif int == 2 then message = "Get Him!"
		elseif int == 3 then message = "Cover Me!"
		elseif int == 4 then message = "Intruder Alert"
		elseif int == 5 then message = "Open Fire"
		elseif int == 6 then message = "Atack!" end

		spatialChat(pNpc, message)

		writeData("restussManager:ambushNine", 1)
		createEvent(12000, "restussHelper", "resetAmbushNine", "", "")
	end


	return 0
end

function restussAmbushHelper:resetAmbushNine()
	writeData("restussManager:ambushNine", 0)
	return 0
end

function restussAmbushHelper:deSpawnAmbushNine(pObject)
	local activeArea = LuaSceneObject(pObject)

	activeArea:destroyObjectFromWorld()

	return 0
end
