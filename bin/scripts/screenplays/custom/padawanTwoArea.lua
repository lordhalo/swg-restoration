local ObjectManager = require("managers.object.object_manager")

padawanTwoArea = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "padawanTwoArea",
}
registerScreenPlay("padawanTwoArea", true)

function padawanTwoArea:start()
	if (isZoneEnabled("lok")) then
		self:spawnMobiles()
	end
end



function padawanTwoArea:spawnMobiles()

	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3100, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3097, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3094, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3091, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3088, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1685, 12, -3085, -100, 0)

	spawnMobile("lok", "rebel_lieutenant_general", 260, -1730, 12, -3100, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1730, 12, -3097, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1730, 12, -3094, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1730, 12, -3091, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1730, 12, -3088, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1730, 12, -3085, -100, 0)


	spawnMobile("lok", "rebel_lieutenant_general", 260, -1705, 12, -3024, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1707, 11, -3026, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1709, 12, -3028, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1711, 12, -3030, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1713, 12, -3032, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1715, 12, -3034, -100, 0)

	spawnMobile("lok", "rebel_specforce_captain", 300, -2072.5, 11.9, -2795.5, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.0, 11.9, -2798.7, 155, 0)
	spawnMobile("lok", "specforce_technician", 300, -2072.5, 11.9, -2801.9, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.0, 11.9, -2805.1, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.5, 11.9, -2808.3, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.0, 11.9, -2811.5, 155, 0)
	spawnMobile("lok", "specforce_technician", 300, -2072.5, 11.9, -2814.7, 155, 0)
	spawnMobile("lok", "specforce_technician", 300, -2072.0, 11.9, -2817.9, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.5, 11.9, -2821.1, 155, 0)
	spawnMobile("lok", "specforce_marine", 300, -2072.0, 11.9, -2824.3, 155, 0)

	spawnMobile("lok", "at_xt", 300, -2038.0, 11.9, -2974.3, 131, 0)
	spawnMobile("lok", "specforce_technician", 300, -2034.0, 11.9, -2974.3, 131, 0)
	spawnMobile("lok", "specforce_technician", 300, -2030.0, 11.9, -2974.3, 131, 0)
	spawnMobile("lok", "specforce_marine", 300, -2042.0, 11.9, -2974.3, 131, 0)
	spawnMobile("lok", "specforce_marine", 300, -2046.0, 11.9, -2974.3, 131, 0)

	spawnMobile("lok", "jedi_knight", 300, -2086.0, 3, -3050.3, 63, 0)

	spawnMobile("lok", "at_xt", 300, -1938.0, 13, -2935, 172, 0)
	spawnMobile("lok", "rebel_commando", 260, -1932, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1930, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1928, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1926, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1924, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1922, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1934, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1936, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1938, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1940, 12, -2944, -100, 0)
	spawnMobile("lok", "rebel_commando", 260, -1942, 12, -2944, -100, 0)

	spawnMobile("lok", "specforce_marine", 300, -1990.0, 11, -3226.3, 4, 0)
	spawnMobile("lok", "specforce_marine", 300, -1986.0, 11, -3226.3, 4, 0)
	spawnMobile("lok", "jedi_padawan", 300, -1995.0, 11, -3226.3, 4, 0)


	-------------------Quest mobs--------------------------

	spawnMobile("lok", "imperial_general", 260, -1788, 12, -3024, 67, 0)

	spawnMobile("lok", "imperial_general", 260, -2025, 11, -3031, -100, 0)


	spawnMobile("lok", "rebel_general", 260, -1898, 31, -2861, 67, 0)
	

	spawnMobile("lok", "rebel_general", 260, -2152, 11, -3116, 79, 0)
end
