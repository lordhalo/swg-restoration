SelectDark = ScreenPlay:new {
	numberOfActs = 1,
	questString = "SelectDark",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32, rohak = 64, mission = 128, missionComp = 256, jedi = 512 }
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("SelectDark", true)

function SelectDark:start()
	if (isZoneEnabled("dathomir")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
		self:spawnActiveAreas()
	end
end

function SelectDark:spawnActiveAreas()

end

--Mobile Spawning
function SelectDark:spawnMobiles()

	spawnMobile("dathomir","mellichaetalk", 1, 5413.2, 78.5, -4202, -68, 0)	
	spawnMobile("dathomir","injuredcommander", 1, 5416.2, 78.5, -4202, -68, 0)	
	spawnMobile("dathomir","injuredcommander", 1, 5499.2, 78.4, -3991, -68, 0)
	spawnMobile("dathomir","campcommander", 1, 5509.2, 122.9, -3520, 85, 0)	

end


--Objects
function SelectDark:spawnSceneObjects()

	spawnSceneObject("dathomir", "object/tangible/camp/camp_tent_s6.iff", 5510, 126, -3513, 174, 0, 0, 0, 0)
	spawnSceneObject("dathomir", "object/tangible/camp/camp_pavilion_s2.iff", 5510, 123, -3513, 174, 0, 0, 0, 0)

end

--Setup
function SelectDark:getActivePlayerName()
	return self.questdata.activePlayerName
end

function SelectDark:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

select_dark_convo_handler = Object:new {
	
 }

function select_dark_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local isDark = creature:hasScreenPlayState(force_sensitive.states.quest.dark, force_sensitive.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			if ( isDark == 1 ) then
				nextConversationScreen = conversation:getScreen("dark_first_screen")--First convo screen to pull.

			else
				nextConversationScreen = conversation:getScreen("dark_first_screen")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function select_dark_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	--if (screenID == "defFinal") then
	--	player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
	--end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

select_dark_injured_convo_handler = Object:new {
	
 }

function select_dark_injured_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local isJedi = creature:hasScreenPlayState(force_sensitive.states.quest.jedi, force_sensitive.questString)
	local isGrey = creature:hasScreenPlayState(force_sensitive.states.quest.grey, force_sensitive.questString)
	local isDark = creature:hasScreenPlayState(force_sensitive.states.quest.dark, force_sensitive.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			if ( isDark == 1) then
				nextConversationScreen = conversation:getScreen("dark_injured_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("dark_injured_leave")--End of the road.
				if (readData("SelectDark:thugAlive") == 0) then				
					local pThug = spawnMobile("dathomir","sith_shadow_mercenary", 1, 5417.2, 78.5, -4202, -68, 0)
					createObserver(OBJECTDESTRUCTION, "SelectDark", "thugDead", pThug)
					writeData("SelectDark:thugAlive",1)
				end	
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function select_dark_injured_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	--if (screenID == "defFinal") then
	--	player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
	--end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function SelectDark:thugDead(pThug, pKiller)
	writeData("SelectDark:thugAlive",0)
end

select_dark_path_convo_handler = Object:new {
	
 }

function select_dark_path_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local isJedi = creature:hasScreenPlayState(force_sensitive.states.quest.jedi, force_sensitive.questString)
	local isGrey = creature:hasScreenPlayState(force_sensitive.states.quest.grey, force_sensitive.questString)
	local isDark = creature:hasScreenPlayState(force_sensitive.states.quest.dark, force_sensitive.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			if ( isDark == 1) then
				nextConversationScreen = conversation:getScreen("dark_path_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("dark_path_screen")--End of the road.	
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function select_dark_path_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	--if (screenID == "defFinal") then
	--	player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
	--end
	
	
	--print("returning convosvreen")
	return conversationScreen
end
