local ObjectManager = require("managers.object.object_manager")

myyydril_Cave = ScreenPlay:new {
	numberOfActs = 1,

}

registerScreenPlay("myyydril_Cave", true)

function myyydril_Cave:start()
	if (isZoneEnabled("taanab")) then	
		self:spawnActiveAreas()
		--self:spawnSceneObjects()
	end
end
--16777303
function myyydril_Cave:spawnActiveAreas()

	local pActiveArea = spawnActiveArea("taanab", "object/active_area.iff", 1830, 42, 4108, 5, 0)
    
	if (pActiveArea ~= nil) then
	        createObserver(ENTEREDAREA, "myyydril_Cave", "notifySpawnArea", pActiveArea)
	    end

	local pActiveArea2 = spawnActiveArea("taanab", "object/active_area.iff", 1828, 40, 4099, 5, 16777303)
    
	if (pActiveArea2 ~= nil) then
	        createObserver(ENTEREDAREA, "myyydril_Cave", "notifyLeaveArea", pActiveArea2)
	    end
end

function myyydril_Cave:notifySpawnArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
			player:teleport(-5, 1, -14, 16777303)
		return 0
	end)
end

function myyydril_Cave:notifyLeaveArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
			player:teleport(1920, 42, 4126, 0)
		return 0
	end)
end

function myyydril_Cave:spawnMobiles()
	
	spawnMobile("taanab", "placeholder", 1, 5354.6, 78.5, -4067.6,  -160,0)
end


