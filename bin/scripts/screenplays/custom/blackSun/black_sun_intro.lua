local ObjectManager = require("managers.object.object_manager")

blackSunIntro = ScreenPlay:new {
	numberOfActs = 1,
	questString = "blackSunIntro",
	states = {
		quest = { intro = 1, phaseone = 2, phasetwo = 4, phasethree = 8, phasefour = 16, phasefive = 32, phasesix = 64, phaseseven = 128, phaseeight = 256, phasenine = 512, phaseten = 1024}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("blackSunIntro", true)

function blackSunIntro:start()
	if (isZoneEnabled("naboo")) then
		blackSunSpawner:spawnActiveAreas()
		blackSunSpawner:resetAll()
	end
end

function blackSunIntro:InsurgentKilled(pPlayer)
	local player = LuaCreatureObject(pPlayer)

	ObjectManager.withCreatureObject(pPlayer, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(1, "blackSunIntro")
					local phaseComplete = groupMember:hasScreenPlayState(2, "blackSunIntro")
					local wasInRange = false

						if (CreatureObject(pPlayer):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true and phaseComplete == false) then
							blackSunIntro:notifyInsurgentKilled(groupMember)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(1, "blackSunIntro")
				local phaseComplete = creature:hasScreenPlayState(2, "blackSunIntro")


					if(hasState == true and phaseComplete == false) then 
						blackSunIntro:notifyInsurgentKilled(pPlayer)
				end
			end
		end)

	return 0
end

function blackSunIntro:OfficerKilled(pPlayer)
	local player = LuaCreatureObject(pPlayer)

	ObjectManager.withCreatureObject(pPlayer, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(16, "blackSunIntro")
					local phaseComplete = groupMember:hasScreenPlayState(32, "blackSunIntro")
					local wasInRange = false

						if (CreatureObject(pPlayer):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true and phaseComplete == false) then
							blackSunIntro:notifyOfficerKilled(groupMember)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(16, "blackSunIntro")
				local phaseComplete = creature:hasScreenPlayState(32, "blackSunIntro")


					if(hasState == true and phaseComplete == false) then 
						blackSunIntro:notifyOfficerKilled(pPlayer)
				end
			end
		end)

	return 0
end

function blackSunIntro:BaseGroupKilled(pPlayer)
	local player = LuaCreatureObject(pPlayer)

	ObjectManager.withCreatureObject(pPlayer, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(64, "blackSunIntro")
					local phaseComplete = groupMember:hasScreenPlayState(128, "blackSunIntro")
					local wasInRange = false

						if (CreatureObject(pPlayer):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true and phaseComplete == false) then
							blackSunIntro:notifyBaseGroupKilled(groupMember)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(64, "blackSunIntro")
				local phaseComplete = creature:hasScreenPlayState(128, "blackSunIntro")


					if(hasState == true and phaseComplete == false) then 
						blackSunIntro:notifyBaseGroupKilled(pPlayer)
				end
			end
		end)

	return 0
end

function blackSunIntro:deadDarkJedi(pPlayer)
	local player = LuaCreatureObject(pPlayer)

	ObjectManager.withCreatureObject(pPlayer, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = creature:hasScreenPlayState(64, "blackSunIntro")
					local wasInRange = false

						if (CreatureObject(pPlayer):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							local pGhost = groupMember:getPlayerObject()
							groupMember:sendSystemMessage("With the Guardian dead, you destroy the generator. Return to Rell")
							PlayerObject(pGhost):addWaypoint("naboo", "Rell", "", 2112, 2546, 5, true, true, WAYPOINTTHEMEPARK, 1)
							groupMember:setScreenPlayState(128, blackSunIntro.questString)
							
						end
					end
				end
			else
				local hasState = creature:hasScreenPlayState(64, "blackSunIntro")
					
					if(hasState == true) then 
						local pGhost = player:getPlayerObject()
						player:sendSystemMessage("With the Guardian dead, you destroy the generator. Return to Rell")
						PlayerObject(pGhost):addWaypoint("naboo", "Rell", "", 2112, 2546, 5, true, true, WAYPOINTTHEMEPARK, 1)
						player:setScreenPlayState(128, blackSunIntro.questString)
				end
			end
		end)

	return 1
end

function blackSunIntro:notifyInsurgentKilled(pPlayer)

	if (pPlayer == nil) then
		return
	end

	local player = LuaCreatureObject(pPlayer)

	local targetCount = tonumber(readScreenPlayData(pPlayer, "blackSunIntro", "InsurgentKilled"))
	local targetGoal = 5

		if(targetCount ~= nil) then
			targetCount = targetCount + 1
			writeScreenPlayData(pPlayer, "blackSunIntro", "InsurgentKilled", targetCount)
			CreatureObject(pPlayer):sendSystemMessage("Insurgents: " .. readScreenPlayData(pPlayer, "blackSunIntro", "InsurgentKilled") .. "/ 5")
			player:playMusicMessage("sound/ui_button_random.snd")
			if (targetCount >= targetGoal) then
					player:sendSystemMessage("You have defeated the Insurgents, Return to Rell")
					player:playMusicMessage("sound/ui_button_random.snd")
					player:setScreenPlayState(2, blackSunIntro.questString)
				return 1
			end
		end

	return 0
end

function blackSunIntro:notifyOfficerKilled(pPlayer)

	if (pPlayer == nil) then
		return
	end

	local player = LuaCreatureObject(pPlayer)

	local targetCount = tonumber(readScreenPlayData(pPlayer, "blackSunIntro", "OfficerKilled"))
	local targetGoal = 5

		if(targetCount ~= nil) then
			targetCount = targetCount + 1
			writeScreenPlayData(pPlayer, "blackSunIntro", "OfficerKilled", targetCount)
			CreatureObject(pPlayer):sendSystemMessage("Officers: " .. readScreenPlayData(pPlayer, "blackSunIntro", "OfficerKilled") .. "/ 5")
			player:playMusicMessage("sound/ui_button_random.snd")
			if (targetCount >= targetGoal) then
					player:sendSystemMessage("You have defeated the Officer, Return to Rell")
					player:playMusicMessage("sound/ui_button_random.snd")
					player:setScreenPlayState(32, blackSunIntro.questString)
				return 1
			end
		end

	return 0
end

function blackSunIntro:notifyBaseGroupKilled(pPlayer)

	if (pPlayer == nil) then
		return
	end

	local player = LuaCreatureObject(pPlayer)

	local targetCount = tonumber(readScreenPlayData(pPlayer, "blackSunIntro", "BaseGroupKilled"))
	local targetGoal = 14

		if(targetCount ~= nil) then
			targetCount = targetCount + 1
			writeScreenPlayData(pPlayer, "blackSunIntro", "BaseGroupKilled", targetCount)
			CreatureObject(pPlayer):sendSystemMessage("Targets Killed: " .. readScreenPlayData(pPlayer, "blackSunIntro", "BaseGroupKilled") .. "/ 14")
			player:playMusicMessage("sound/ui_button_random.snd")
			if (targetCount >= targetGoal) then
					player:sendSystemMessage("Find the Bio-Scanner")
					PlayerObject(pPlayer):addWaypoint("naboo", "Bio-Scanner", "", 6590, 3244, 5, true, true, WAYPOINTTHEMEPARK, 1)
					player:playMusicMessage("sound/ui_button_random.snd")
					writeData("baseGroup:spawn",2)
				return 1
			end
		end

	return 0
end

function blackSunIntro:npcDamageObserver(pTyber, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(pTyber)
	local npc = LuaSceneObject(pTyber)
	
	health = boss:getHAM(0)
	maxHealth = boss:getMaxHAM(0)

	local hasState = player:hasScreenPlayState(256, "blackSunIntro")

	if (health <= (maxHealth * 0.1)) then
		spatialChat(pTyber, "Where I have failed, the Rebellion will continue our work! Watch your back, I won't soon forget this!")
		npc:switchZone("character_farm", -200, 0, -200, 0)
		npc:destroyObjectFromWorld()

		ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(256, "blackSunIntro")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							local pGhost = groupMember:getPlayerObject()
							groupMember:sendSystemMessage("Tyber flees the fight, Return to Rell")
							PlayerObject(pGhost):addWaypoint("naboo", "Rell", "", 2112, 2546, 5, true, true, WAYPOINTTHEMEPARK, 1)
							groupMember:setScreenPlayState(512, blackSunIntro.questString)
							writeData("tyber:spawn",0)
						end
					end
				end
			else
				local hasState = creature:hasScreenPlayState(256, "blackSunIntro")
					
					if(hasState == true) then 
						local pGhost = player:getPlayerObject()
						player:sendSystemMessage("Tyber flees the fight, Return to Rell")
						PlayerObject(pGhost):addWaypoint("naboo", "Rell", "", 2112, 2546, 5, true, true, WAYPOINTTHEMEPARK, 1)
						player:setScreenPlayState(512, blackSunIntro.questString)
						writeData("tyber:spawn",0)
				end
			end
		end)
	end

	return 0

end
--Setup
function blackSunIntro:getActivePlayerName()
	return self.questdata.activePlayerName
end

function blackSunIntro:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

blackSunIntroConvo_handler = Object:new {
	
 }

function blackSunIntroConvo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local creature = LuaCreatureObject(conversingPlayer)
			local hasSkill = creature:hasSkill("fs_jedi_novice")
			local hasRebel = creature:hasSkill("reb_rank_novice")
			local hasImperial = creature:hasSkill("imp_rank_novice")
			local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local firstQuestStarted = creature:hasScreenPlayState(1, "blackSunIntro")
			local firstQuestComplete = creature:hasScreenPlayState(2, "blackSunIntro")
			local secondQuestComplete = creature:hasScreenPlayState(8, "blackSunIntro")
			local secondQuestStarted = creature:hasScreenPlayState(4, "blackSunIntro")
			local thirdQuestComplete = creature:hasScreenPlayState(32, "blackSunIntro")
			local thirdQuestStarted = creature:hasScreenPlayState(16, "blackSunIntro")
			local forthQuestComplete = creature:hasScreenPlayState(128, "blackSunIntro")
			local forthQuestStarted = creature:hasScreenPlayState(64, "blackSunIntro")
			local fifthQuestComplete = creature:hasScreenPlayState(512, "blackSunIntro")
			local fifthQuestStarted = creature:hasScreenPlayState(256, "blackSunIntro")
			local questChainComplete = creature:hasScreenPlayState(1024, "blackSunIntro")


				--### KEY CONVO PARTS, AND STATES ###--
				if (hasSkill == true or hasRebel == true or hasImperial == true or hasDeathWatch == true) then
					nextConversationScreen = conversation:getScreen("jedi_screen")	
				elseif (questChainComplete == true) then
					nextConversationScreen = conversation:getScreen("chain_complete_screen")
				elseif (fifthQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("fifth_quest_complete_screen")
				elseif (fifthQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("fifth_quest_waiting")
				elseif (forthQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("fourth_quest_complete")
				elseif (forthQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("fourth_quest_waiting")
				elseif (thirdQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("third_quest_complete_screen")
				elseif (thirdQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("third_quest_waiting")
				elseif (secondQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("second_quest_complete_screen")
				elseif (secondQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("second_quest_waiting")
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("first_quest_complete_screen")
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("accepted_screen")
				else
					nextConversationScreen = conversation:getScreen("first_screen")
				end				
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function blackSunIntroConvo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "seventh_screen") then
		player:setScreenPlayState(1, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Swoop Gang", "", 3307, -5001, true, true, WAYPOINTTHEMEPARK, 1)	
			PlayerObject(pGhost):addWaypoint("naboo", "Insurgents", "", 4660, -4566, 5, true, true, WAYPOINTTHEMEPARK, 1)
			writeScreenPlayData(conversingPlayer, "blackSunIntro", "InsurgentKilled", 0)
		end
	end

	if (screenID == "second_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("black_sun", 10)
		end)

		player:setScreenPlayState(4, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", -1381, -3656, true, true, WAYPOINTTHEMEPARK, 1)
			PlayerObject(pGhost):addWaypoint("naboo", "Tyber", "", -5186, 4273, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "third_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("black_sun", 10)
		end)

		player:setScreenPlayState(16, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", -1381, -3656, true, true, WAYPOINTTHEMEPARK, 1)
			PlayerObject(pGhost):addWaypoint("naboo", "Officer", "", -3387, 159, 5, true, true, WAYPOINTTHEMEPARK, 1)
			writeScreenPlayData(conversingPlayer, "blackSunIntro", "OfficerKilled", 0)		
		end
	end

	if (screenID == "fourth_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("black_sun", 10)
		end)

		player:setScreenPlayState(64, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("naboo", "Death Watch Base", "", 6591, 3238, 5, true, true, WAYPOINTTHEMEPARK, 1)
			writeScreenPlayData(conversingPlayer, "blackSunIntro", "BaseGroupKilled", 0)		
		end
	end

	if (screenID == "fifth_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("black_sun", 10)
		end)

		player:setScreenPlayState(256, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("naboo", "Tyber", "", -5369, 4097, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "fifth_quest_outro") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("black_sun", 50)
		end)

		player:setScreenPlayState(1024, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("lok", "Black Sun Bunker", "", -6374, 2851, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end
	
	return conversationScreen
end


zannFirstConvo_handler = Object:new {
	
 }

function zannFirstConvo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)
			local firstQuestComplete = creature:hasScreenPlayState(4, "blackSunIntro")
			local jeekComplete = creature:hasScreenPlayState(8, "blackSunIntro")
				if (jeekComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("first_screen")
				else
					nextConversationScreen = conversation:getScreen("goaway_screen")
				end				
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function zannFirstConvo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "final_screen") then
		player:setScreenPlayState(8, blackSunIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("naboo", "Return to Rell", "", 2112, 2546, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	return conversationScreen
end
