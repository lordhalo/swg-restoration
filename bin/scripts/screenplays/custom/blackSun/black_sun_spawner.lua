local ObjectManager = require("managers.object.object_manager")

blackSunSpawner = ScreenPlay:new {
	numberOfActs = 1,
	questString = "blackSunSpawner",
	states = {
		quest = { intro = 1, phaseone = 2, phasetwo = 4, phasethree = 8, phasefour = 16, phasefive = 32, phasesix = 64, phaseseven = 128, phaseeight = 256, phasenine = 512, phaseten = 1024}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	insurgentGroup = {
		{ "insurgent_leader", 0, 4665, 4, -4570,  77, 0 },
		{ "insurgents", 0, 4663, 4, -4572,  -61, 0 },
		{ "insurgents", 0, 4666, 4, -4563,  -162, 0 },
		{ "insurgents", 0, 4656, 4, -4565,  123, 0 },
		{ "insurgents", 0, 4669, 4, -4565,  -136, 0 },
	},

	officerGroup = {
		{ "death_watch_ghost_quest", 0, -3393, 15, 175,  1, 0 },
		{ "death_watch_ghost_quest", 0, -3393, 15, 177,  1, 0 },
		{ "death_watch_ghost_quest", 0, -3393, 15, 179,  1, 0 },
		{ "death_watch_ghost_quest", 0, -3391, 15, 176,  1, 0 },
		{ "death_watch_bloodguard_quest", 0, -3391, 15, 178,  1, 0 }

	},

	baseGroup = {
		{ "death_watch_ghost_quest", 0, 6594, 13, 3267,  3, 0 },
		{ "death_watch_ghost_quest", 0, 6587, 13, 3267,  3, 0 },
		{ "death_watch_ghost_quest", 0, 6525, 11, 3260,  -176, 0 },
		{ "death_watch_ghost_quest", 0, 6535, 12, 3238,  -170, 0 },
		{ "death_watch_ghost_quest", 0, 6515, 11, 3240,  172, 0 },
		{ "death_watch_ghost_quest", 0, 6568, 15, 3215,  154, 0 },
		{ "death_watch_ghost_quest", 0, 6572, 15, 3211,  154, 0 },
		{ "death_watch_ghost_quest", 0, 6566, 15, 3208,  154, 0 },
		{ "death_watch_ghost_quest", 0, 6603, 15, 3230,  108, 0 },
		{ "death_watch_ghost_quest", 0, 6573, 15, 3230,  -127, 0 },
		{ "death_watch_ghost_quest", 0, 6621, 14, 3265,  -36, 0 },
		{ "death_watch_ghost_quest", 0, 6601, 11, 3307,  -19, 0 },
		{ "death_watch_ghost_quest", 0, 6630, 14, 3198,  84, 0 },
		{ "death_watch_bloodguard_quest", 0, 6590, 15, 3238,  70, 0 },--commander here

	},

	baseWall = { -- saved but unused
		{ "object/static/structure/military/military_wall_weak_rebl_16_style_01.iff", -238, 7, 6125, 0, 1, 0, 0, 0 }
	}
}
registerScreenPlay("blackSunSpawner", true)

function blackSunSpawner:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
	end
end

function blackSunSpawner:spawnMobiles()
	
	spawnMobile("naboo", "rell", 0, 20.2, -0.9, -15.1,  -85, 6)

end

function blackSunSpawner:spawnActiveAreas()

	local pLeader = spawnActiveArea("naboo", "object/active_area.iff", 4664, 4, -4570, 32, 0)
	if pLeader ~= nil then
		createObserver(ENTEREDAREA, "blackSunSpawner", "enterInsurgentArea", pLeader)
	end

	local pOfficer = spawnActiveArea("naboo", "object/active_area.iff", -3387, 14, 156, 32, 0)
	if pLeader ~= nil then
		createObserver(ENTEREDAREA, "blackSunSpawner", "enterOfficerArea", pOfficer)
	end

	local pBase = spawnActiveArea("naboo", "object/active_area.iff", 6590, 14, 3244, 62, 0)
	if pLeader ~= nil then
		createObserver(ENTEREDAREA, "blackSunSpawner", "enterBaseArea", pBase)
	end

	local pBioScann = spawnActiveArea("naboo", "object/active_area.iff", 6635, 12, 3297, 10, 0)
	if pLeader ~= nil then
		createObserver(ENTEREDAREA, "blackSunSpawner", "enterBioScan", pBioScann)
	end

	local pTyber = spawnActiveArea("naboo", "object/active_area.iff", -5369, 5, 4097, 5, 0)
	if pLeader ~= nil then
		createObserver(ENTEREDAREA, "blackSunSpawner", "enterTyberArea", pTyber)
	end
end

function blackSunSpawner:completeBaseGroup(pMobile, pKiller)
	local player = LuaCreatureObject(pKiller)

	writeData("baseGroup:npcsDead",readData("baseGroup:npcsDead") + 1)

	if(readData("baseGroup:npcsDead") == 14) then

		writeData("baseGroup:spawn",2)
		--writeData("swoopGang:npcsDead",0)
	end
	return 0
end

function blackSunSpawner:enterInsurgentArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		local hasState = player:hasScreenPlayState(1, "blackSunIntro")
		local completed = player:hasScreenPlayState(2, "blackSunIntro")

		if (readData("insurgentGroup:spawn") == 0 and hasState == true and completed == false)  then
			self:spawnInsurgentGroup()
			writeData("insurgentGroup:spawn",1)
		end


		return 0
	end)
end

function blackSunSpawner:enterOfficerArea(pActiveArea, pMovingObject)
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end

	local player = LuaCreatureObject(pMovingObject)
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		local hasState = player:hasScreenPlayState(16, "blackSunIntro")
		local hasComplete = player:hasScreenPlayState(32, "blackSunIntro")

		if (readData("officerGroup:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnOfficer(pMovingObject)
			writeData("officerGroup:spawn",1)
		end


		return 0
	end)
end

function blackSunSpawner:enterBaseArea(pActiveArea, pMovingObject)
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end

	local player = LuaCreatureObject(pMovingObject)
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		local hasState = player:hasScreenPlayState(64, "blackSunIntro")
		local hasComplete = player:hasScreenPlayState(128, "blackSunIntro")

		if (readData("baseGroup:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnBaseGroup(pMovingObject)
			writeData("baseGroup:spawn",1)
		end


		return 0
	end)
end

function blackSunSpawner:enterBioScan(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		local hasState = player:hasScreenPlayState(64, "blackSunIntro")
		local hasComplete = player:hasScreenPlayState(128, "blackSunIntro")

		if (readData("baseGroup:spawn") == 2 and readData("jediGuard:spawn") == 0 and hasState == true and hasComplete == false)  then
			CreatureObject(pMovingObject):sendSystemMessage("Bioscan complete, Turret disengages.")
			self:spawnDarkJedi(pMovingObject)
			writeData("jediGuard:spawn",1)
		end


		return 0
	end)
end

function blackSunSpawner:resetAll() 
	self:insurgentReset()
	self:officerReset()
	self:baseReset()
	self:tyberReset()
	return 0
end

function blackSunSpawner:insurgentReset() 
	writeData("insurgentGroup:spawn",0)
	return 0
end

function blackSunSpawner:officerReset() 
	writeData("officerGroup:spawn",0)
	return 0
end

function blackSunSpawner:baseReset() 
	writeData("baseGroup:spawn",0)
	writeData("jediGuard:spawn",0)
	return 0
end

function blackSunSpawner:tyberReset() 
	writeData("tyber:spawn",0)
	return 0
end

function blackSunSpawner:spawnInsurgentGroup()

		local mobileTable = self.insurgentGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "blackSunIntro", "InsurgentKilled", pMobile)
			createEvent(1800000, "blackSunSpawner", "despawnNpc", pMobile, "")
		end
		createEvent(1800000, "blackSunSpawner", "insurgentReset", nil, "")
end

function blackSunSpawner:spawnOfficer(pMovingObject)

		local mobileTable = self.officerGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			CreatureObject(pMobile):engageCombat(pMovingObject)
			createObserver(OBJECTDESTRUCTION, "blackSunIntro", "OfficerKilled", pMobile)
			createEvent(1800000, "blackSunSpawner", "despawnNpc", pNpc, "")
		end
		createEvent(1800000, "blackSunSpawner", "officerReset", nil, "")
end

function blackSunSpawner:spawnBaseGroup(pMovingObject)

		local mobileTable = self.officerGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("naboo", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			CreatureObject(pMobile):engageCombat(pMovingObject)
			createObserver(OBJECTDESTRUCTION, "blackSunIntro", "BaseGroupKilled", pMobile)
			createEvent(1800000, "blackSunSpawner", "despawnNpc", pNpc, "")
		end
		createEvent(1800000, "blackSunSpawner", "baseReset", nil, "")
end

function blackSunSpawner:enterTyberArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		local hasState = player:hasScreenPlayState(256, "blackSunIntro")
		local hasComplete = player:hasScreenPlayState(512, "blackSunIntro")

		if (readData("tyber:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnTyber(pMovingObject)
			writeData("tyber:spawn",1)
		end


		return 0
	end)
end

function blackSunSpawner:spawnDarkJedi(pMovingObject)
	local dJedi = spawnMobile("naboo", "death_watch_jedi_quest", 0, 6634, 12, 3287,  0, 0)
	spatialChat(dJedi, "You trick may work on droids but I know who you are!")
	createObserver(OBJECTDESTRUCTION, "blackSunIntro", "deadDarkJedi", dJedi)

end

function blackSunSpawner:spawnTyber(pMovingObject)
	local pTyber = spawnMobile("naboo", "tyber_combat", 0, -5369, 5, 4097,  0, 0)
	createEvent(1800000, "blackSunSpawner", "despawnNpc", pTyber, "")
	createEvent(1800000, "blackSunSpawner", "tyberReset", nil, "")
	spatialChat(pTyber, "The Deathwatch is not done in this sector. This is just a setback!")
	createObserver(DAMAGERECEIVED, "blackSunIntro", "npcDamageObserver", pTyber)

end

function blackSunSpawner:spawnSceneObjects()
	spawnSceneObject("naboo", "object/mobile/vehicle/speederbike_swoop.iff", 4664, 4, -4562, 0, 1, 0, 0, 0)	
	spawnSceneObject("naboo", "object/static/item/item_container_inorganic_chemicals.iff", 4660, 4, -4566, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_tato_tent_s01.iff", -3387, 15.0, 159, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_powerdroid_powerdown.iff", -3382, 14.7, 156, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_tato_farm_64x64_s01.iff", 6591,15, 3241, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_tato_tent_s01.iff", 6535, 12, 3245, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_tato_tent_s01.iff", 6515, 11, 3245, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_tato_tent_s01.iff", 6525, 11, 3265, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/military/bunker_crate_style_01.iff", 6578, 14, 3242, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/structure/general/poi_all_corral_half_32x32_s05.iff", 6635, 13, 3280, 0, 1, 0, 0, 0)
	--spawnSceneObject("naboo", "object/static/structure/military/military_wall_weak_rebl_16_style_01.iff", 6635, 13, 3290, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/installation/faction_perk/turret/tower_lg.iff", 6635, 12, 3297, 0, 1, 0, 0, 0)
	spawnSceneObject("naboo", "object/static/installation/mockup_power_generator_wind_style_1.iff", 6634, 13, 3278, 0, 1, 0, 0, 0)

end

function blackSunSpawner:despawnNpc(pMobile)
	if (pMobile == nil) then
		return
	end

	if pMobile ~= nil then
		SceneObject(pMobile):destroyObjectFromWorld()
	end

	return 0
end
