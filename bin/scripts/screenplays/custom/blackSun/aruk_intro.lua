local ObjectManager = require("managers.object.object_manager")

arukIntro = ScreenPlay:new {
	numberOfActs = 1,
	questString = "arukIntro",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("arukIntro", true)

function arukIntro:start()
	if (isZoneEnabled("lok")) then
		--self:spawnMobiles()
	end
end


function arukIntro:spawnMobiles()
	spawnMobile("naboo", "theed_smuggler", 0, -5069, 6, 4286,  -107, 0)
end

function arukIntro:getActivePlayerName()
	return self.questdata.activePlayerName
end

function arukIntro:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


aruk_thegreat_convo_handler = Object:new {
	
 }

function aruk_thegreat_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local introComplete = creature:hasScreenPlayState(1024, "blackSunIntro")
			local arukIntro = creature:hasScreenPlayState(2, "arukIntro")
				if (arukIntro == true) then
					nextConversationScreen = conversation:getScreen("return_friend")
				elseif (introComplete == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function aruk_thegreat_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "final_screen" ) then
		local pGhost = player:getPlayerObject()
		player:setScreenPlayState(2, arukIntro.questString)
		awardSkill(conversingPlayer, "black_sun_novice")
		PlayerObject(pGhost):setFrsCouncil(6)
		PlayerObject(pGhost):setFrsRank(0)
		player:setFaction(FACTIONIMPERIAL)
		CreatureObject(conversingPlayer):setFactionStatus(2)

	end

	return conversationScreen
end

