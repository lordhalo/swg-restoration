local ObjectManager = require("managers.object.object_manager")

padawanQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "padawanQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phase4 = 32}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("padawanQuest", true)

function padawanQuest:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end


--Mobile Spawning
function padawanQuest:spawnMobiles()
	spawnMobile("yavin4", "jedi_padawan_one", 1, -0.3, -15.1, 22.8, -3, 8525418)
	--spawnMobile("tatooine", "secuirty_recruiter", 1, -2907, 5.0, 2132,  40, 0)
	--spawnMobile("dathomir", "fs_follower", 1, 5304.2, 78.5, -4187.4,  -60, 0)


end

--Setup
function padawanQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function padawanQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

padawanQuestLight_handler = Object:new {
	
 }

function padawanQuestLight_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local hasInitiate = creature:hasSkill("fs_jedi_master")
	local hasSkillOne = creature:hasSkill("jedi_force_4")
	local hasSkillTwo = creature:hasSkill("jedi_healing_4")
	local hasSkillThree = creature:hasSkill("jedi_lightsaber_4")
	local hasSkillFour = creature:hasSkill("jedi_defender_4")
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  
			
			local deathReturn = creature:hasScreenPlayState(padawanQuest.states.quest.phase4, padawanQuest.questString)
			local threeComplete = creature:hasScreenPlayState(padawanQuest.states.quest.phasethree, padawanQuest.questString)
			local twoComplete = creature:hasScreenPlayState(padawanQuest.states.quest.phasetwo, padawanQuest.questString)
			local hasAccepted = creature:hasScreenPlayState(padawanQuest.states.quest.phaseone, padawanQuest.questString)
			
			if (deathReturn == true) then
				nextConversationScreen = conversation:getScreen("padawan_failure_screen")
			elseif (threeComplete == true) then
				nextConversationScreen = conversation:getScreen("padawan_complete_screen")
			elseif (twoComplete == true) then
				nextConversationScreen = conversation:getScreen("padawan_return_screen")
			elseif (hasAccepted == true) then
				nextConversationScreen = conversation:getScreen("padawan_accepted_screen")
			elseif (hasSkillOne == true and hasSkillTwo == true and hasSkillThree == true and hasSkillFour == true) then
				nextConversationScreen = conversation:getScreen("padawan_start_screen")
			elseif ( hasInitiate == true ) then
				nextConversationScreen = conversation:getScreen("padawan_not_ready")
			else
				nextConversationScreen = conversation:getScreen("hello_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function padawanQuestLight_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local hasSith = player:hasSkill("rank_dark_novice")
	if (screenID == "padawan_fourth_screen") then
		player:setScreenPlayState(padawanQuest.states.quest.phaseone, padawanQuest.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/kits/padawan_holocron.iff", -1)

		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Fragment Location", "", -956, -2042, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
		
	end

	if (screenID == "padawan_return_seven" and hasSith == false) then
		local pGhost = player:getPlayerObject()
		player:setScreenPlayState(padawanQuest.states.quest.phasethree, padawanQuest.questString)
		awardSkill(conversingPlayer, "rank_light_novice")
		player:awardExperience("death_exp", 1, true)
		--player:awardExperience("death_exp", 1)
		setJediState(conversingPlayer, 4)
		player:setFaction(FACTIONREBEL)
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:setFactionStatus(1)
		giveItem(pInventory, "object/tangible/loot/cls/tool/robe_p.iff", -1)
		end)
	end

	if (screenID == "padawan_failure_two" and hasSith == false) then
		local pGhost = player:getPlayerObject()
		player:removeScreenPlayState(padawanQuest.states.quest.phase4, padawanQuest.questString)
		awardSkill(conversingPlayer, "rank_light_novice")
		player:awardExperience("death_exp", 1, true)
		--player:awardExperience("death_exp", 1)
		setJediState(conversingPlayer, 4)
		player:setFaction(FACTIONREBEL)
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:setFactionStatus(1)
		end)
		
	end
	
	return conversationScreen
end

trial_two_convo_handler = Object:new {
	
 }

function trial_two_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local oneComplete = creature:hasScreenPlayState(padawanQuest.states.quest.phaseone, padawanQuest.questString)
	
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local creature = LuaCreatureObject(conversingPlayer)

			nextConversationScreen = conversation:getScreen("first_screen")--End of the road.	

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function trial_two_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "second_screen") then
		if (conversingPlayer == nil) then
			return
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "color_crystals", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
	end
	

	return conversationScreen
end




