local ObjectManager = require("managers.object.object_manager")

theedSmuggler = ScreenPlay:new {
	numberOfActs = 1,
	questString = "theedSmuggler",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("theedSmuggler", true)

function theedSmuggler:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		self:spawnSmuggler()
	end
end


function theedSmuggler:spawnMobiles()
	spawnMobile("naboo", "theed_smuggler", 0, -5069, 6, 4286,  -107, 0)
	spawnMobile("naboo", "male_twilek_thug_quest", 0, -5244, 6, 4243,  -50, 0)
	spawnMobile("naboo", "male_zabrak_thug_quest", 0, -5242, 6, 4243,  -50, 0)
	local pBoss = spawnMobile("naboo", "male_rodian_thug_quest", 0, -5243, 6, 4242,  -50, 0)
	createObserver(OBJECTDESTRUCTION, "theedSmuggler", "bossDead", pBoss)
end

function theedSmuggler:spawnSmuggler()
	spawnMobile("naboo", "theed_smuggler", 0, -5069, 6, 4286,  -107, 0)
end

--Setup

function theedSmuggler:bossDead(pBoss, pPlayer)
	local player = LuaCreatureObject(pPlayer)
	local  hasComplete = player:hasScreenPlayState(theedSmuggler.states.quest.phaseone, theedSmuggler.questString)
	if (hasComplete == true) then
		player:setScreenPlayState(theedSmuggler.states.quest.phasetwo, theedSmuggler.questString)
	end
	createEvent(2 * 60 * 1000, "theedSmuggler", "spawnMobiles", player, "")
	return 1
end
function theedSmuggler:getActivePlayerName()
	return self.questdata.activePlayerName
end

function theedSmuggler:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


theed_smuggler_convo_handler = Object:new {
	
 }

function theed_smuggler_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)

			local hasAccepted = creature:hasScreenPlayState(4, "padawanQuest")

			local hasAcceptedDark = creature:hasScreenPlayState(4, "padawanQuestDark")
			
			local  hasAcceptedTwo = creature:hasScreenPlayState(4, "theedSmuggler")
			
			local  hasComplete = creature:hasScreenPlayState(theedSmuggler.states.quest.phasetwo, theedSmuggler.questString)
			
			local  hasCompleteTwo = creature:hasScreenPlayState(theedSmuggler.states.quest.phasethree, theedSmuggler.questString)
			
			local  hasCompleteThree = creature:hasScreenPlayState(theedSmuggler.states.quest.phasefour, theedSmuggler.questString)
			if ( hasCompleteTwo == true ) then
				nextConversationScreen = conversation:getScreen("complete_screen")--First convo screen to pull.
			elseif ( hasCompleteThree == true ) then
				nextConversationScreen = conversation:getScreen("fear_screen")--First convo screen to pull.
			elseif ( hasComplete == true ) then
				nextConversationScreen = conversation:getScreen("last_screen")--First convo screen to pull.
			elseif ( hasAcceptedTwo == true ) then
				nextConversationScreen = conversation:getScreen("help_screen")--First convo screen to pull.
			elseif ( hasAccepted == true or hasAcceptedDark == true ) then
				nextConversationScreen = conversation:getScreen("first_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("hello_screen")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function theed_smuggler_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "eighth_screen" ) then
		player:setScreenPlayState(theedSmuggler.states.quest.phaseone, theedSmuggler.questString)
	end

	if ( screenID == "last_screen2" ) then
		player:setScreenPlayState(theedSmuggler.states.quest.phasethree, theedSmuggler.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_05.iff", -1)
	end
	
	if ( screenID == "sixth_sceen_c" ) then
		player:setScreenPlayState(theedSmuggler.states.quest.phasefour, theedSmuggler.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_05.iff", -1)
	end

	return conversationScreen
end
