local ObjectManager = require("managers.object.object_manager")

tuskenRaidLeaderEncounter = ScreenPlay:new {
	numberOfActs = 1,
}
registerScreenPlay("tuskenRaidLeaderEncounter", true)

function tuskenRaidLeaderEncounter:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
	end
end

function tuskenRaidLeaderEncounter:spawnSceneObjects()
	--spawnSceneObject("tatooine", "object/mobile/lair/npc_dynamic/tatooine_tusken_raider_patrol_neutral_none.iff", -6168, 7, 1794, 0, 1, 0, 0, 0)

end

function tuskenRaidLeaderEncounter:spawnMobiles()
	
	local bossNpc = spawnMobile("tatooine", "tusken_raid_leader_boss", 3600, -6092.4, 6.1, 1856.9,  -108, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6120.4, 6.6, 1865,  -39, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6110.5, 9.6, 1897,  -20, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6086.7, 9.9, 1899.4,  72, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6081.1, 7.4, 1880,  102, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6139.7, 6.6, 1860.3,  -99, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6164.7, 7.8, 1871,  -7, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6154, 9.4, 1890.5,  5, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6138.7, 6.4, 1810.6,  111, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6171.7, 6.7, 1846.6,  -17, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6124.7, 9.4, 1893.6,  -63, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6139.7, 6.9, 1801.6,  65, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6139.7, 6.9, 1797.6,  65, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6124.7, 9.4, 1866.6,  -71, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6141.7, 8.4, 1885.6,  -19, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6115.7, 6, 1832.6,  66, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6124.7, 9.4, 1893.6,  -63, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6104.7, 10, 1908.6,  -57, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6116.7, 9.8, 1902.6,  8, 0)
	spawnMobile("tatooine", "tusken_raider", 30, -6107.7, 6.2, 1847.6,  112, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6215.8, 9.2, 1868.6,  135, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6216.5, 9.2, 1860.6,  125, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6214.7, 9.2, 1855.6,  121, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6216.7, 9.2, 1845.6,  105, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6225.8, 9.2, 1868.6,  135, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6226.5, 9.2, 1860.6,  125, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6224.7, 9.2, 1855.6,  121, 0)
	spawnMobile("tatooine", "tusken_bantha", 100, -6226.7, 9.2, 1845.6,  105, 0)
	--createObserver(DAMAGERECEIVED, "tuskenRaidLeaderEncounter", "npcDamageObserver", bossNpc)
	--createObserver(OBJECTINRANGEMOVED, "tuskenRaidLeaderEncounter", "MovedAway", bossNpc)
	--createObserver(DEFENDERADDED, "tuskenRaidLeaderEncounter", "attackerAdded", bossNpc)
	--Jawa Defenders
	--spawnMobile("tatooine", "jawa_protector_boss", 60, -6180, 6.9, 1827, -133, 0)
	--spawnMobile("tatooine", "jawa_protector_boss", 60, -6182, 6.9, 1825, -133, 0)
	--spawnMobile("tatooine", "jawa_protector_boss", 60, -6180, 6.9, 1823, -133, 0)
	--spawnMobile("tatooine", "jawa_protector_boss", 60, -6184, 6.9, 1829, -133, 0)
	--spawnMobile("tatooine", "jawa_protector_boss", 60, -6180, 6.9, 1831, -133, 0)
end


