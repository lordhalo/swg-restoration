local ObjectManager = require("managers.object.object_manager")

MistyFalls = ScreenPlay:new {
	numberOfActs = 1,
	questString = "MistyFalls",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("MistyFalls", true)

function MistyFalls:start()
	if (isZoneEnabled("dathomir")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
	end
end

function MistyFalls:spawnSceneObjects()
	spawnSceneObject("dathomir", "object/static/structure/dathomir/nsister_tent_house_style_01.iff", 3511, 24, 1552, 0, 1, 0, -60, -60)
end

function MistyFalls:spawnMobiles()
	local pNS = spawnMobile("dathomir", "nightsister_initiate_quest", 300, 3511, 24, 1552,  -60, 0)
	createObserver(DAMAGERECEIVED, "MistyFalls", "npcDamageObserver", pNS)
	createObserver(OBJECTDESTRUCTION, "MistyFalls", "deadNS", pNS)
end

function MistyFalls:deadNS(pNS, pKiller)
	local player = LuaCreatureObject(pKiller)

	if (player:hasScreenPlayState(4, "padawanQuest") or player:hasScreenPlayState(4, "padawanQuestDark")) then
		local pInventory = player:getSlottedObject("inventory")
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_04.iff", -1)
	end

	 return 0
end

function MistyFalls:npcDamageObserver(bossObject, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.99)) or (action <= (maxAction * 0.99)) or (mind <= (maxMind * 0.99))) and readData("mistyfalls:nsdamage") == 0) then
		spatialChat(bossObject, "DO NOT DISTURB ME!! I MUST UNLOCK THE SECRET!!")
		writeData("mistyfalls:nsdamage",1)
	elseif (((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) and readData("mistyfalls:nsdamage2") == 0) then
		spatialChat(bossObject, "You will not take it!")
		writeData("mistyfalls:nsdamage2",1)
	elseif (((health <= (maxHealth * 0.4)) or (action <= (maxAction * 0.4)) or (mind <= (maxMind * 0.4))) and readData("mistyfalls:nsdamage3") == 0) then
		spatialChat(bossObject, "I will not be defeated!")
		writeData("mistyfalls:nsdamage3",1)
	elseif ((health <= (maxHealth * 0.01)) or (action <= (maxAction * 0.01)) or (mind <= (maxMind * 0.01))) then
		spatialChat(bossObject, "...Why did I exist?")
		writeData("mistyfalls:nsdamage",0)
		writeData("mistyfalls:nsdamage2",0)
		writeData("mistyfalls:nsdamage3",0)
	end

	return 0
end
