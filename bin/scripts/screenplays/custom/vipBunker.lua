local ObjectManager = require("managers.object.object_manager")

vipBunkerScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "vipBunkerScreenPlay",

}

registerScreenPlay("vipBunkerScreenPlay", true)

function vipBunkerScreenPlay:start()
	if (isZoneEnabled("talus")) then
		self:spawnMobiles()
	end
end

function vipBunkerScreenPlay:spawnMobiles()
	spawnMobile("talus", "bonKar", 0, 11, 1, 5, 93, 4265393)
end


--vip_bunker_handler
--wp 4215, 1040 (dead body)

vip_bunker_handler = conv_handler:new {}
function vip_bunker_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)

	if (CreatureObject(pPlayer):hasScreenPlayState(4, "vipQuest")) then
		return convoTemplate:getScreen("completed")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(2, "vipQuest")) then
		return convoTemplate:getScreen("already")
	elseif (CreatureObject(pPlayer):hasScreenPlayState(1, "vipQuest")) then
		return convoTemplate:getScreen("already")
	else
		return convoTemplate:getScreen("first")
	end
end

function vip_bunker_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local pGhost = CreatureObject(pPlayer):getPlayerObject()
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()
	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	local playerID = CreatureObject(pPlayer):getObjectID()
	
	if (pGhost == nil) then
		return 0
	end
	
	if (screenID == "partner") then
		CreatureObject(pPlayer):setScreenPlayState(1, "vipQuest")
		local questWaypoint = PlayerObject(pGhost):addWaypoint("talus", "Bon Kar's Mysterious Contact", "", 4215, 1040, WAYPOINTBLUE, true, true, 0)
		setQuestStatus(playerID .. ":currentQuestWaypointID", questWaypoint)
		local pContact = spawnActiveArea("talus", "object/active_area.iff", 4215, 5, 1040, 5, 0)
		if (pContact ~= nil) then
			createObserver(ENTEREDAREA, "vip_bunker_handler", "notifyAtContact", pContact)
		end
	end
	
	return pConvScreen
end

function vip_bunker_handler:notifyAtContact(pArea, pPlayer)
	if (pPlayer == nil or pArea == nil) then
		return 0
	end
	local pGhost = CreatureObject(pPlayer):getPlayerObject()
	local contactWaypointID = tonumber(getQuestStatus(SceneObject(pPlayer):getObjectID() .. ":currentQuestWaypointID"))
	local playerID = CreatureObject(pPlayer):getObjectID()
	local state = CreatureObject(pPlayer):getScreenPlayState("vipQuest")
	
	if (state == 1) then
		PlayerObject(pGhost):removeWaypoint(contactWaypointID, true)
		removeQuestStatus(SceneObject(pPlayer):getObjectID() .. ":currentQuestWaypointID")
		CreatureObject(pPlayer):sendSystemMessage("You arrive to the contact location, but find a corpse. As you glimpse at the corpse you see a datapad with coordinates leading to one of the nearby buildings.")
		createEvent(10000, "trev_handler", "delayNextPatrol", pPlayer, "")
		CreatureObject(pPlayer):removeScreenPlayState(state, "vipQuest")
		CreatureObject(pPlayer):setScreenPlayState(2, "vipQuest")
		local cords = { x = 4317, z = 6, y = 999, r = math.rad(180), cell = 0}
		local pDeed = spawnSceneObject("talus", "object/tangible/tcg/series7/shared_structure_deed_vip_bunker.iff", cords.x, cords.z, cords.y, cords.cell, cords.r)
		CreatureObject(pDeed):setCustomObjectName("Stolen Bunk Deed.")
		
		if (pDeed ~= nil) then
			SceneObject(pDeed):setObjectMenuComponent("vipBunker")
			writeData(playerID .. ":vip_bunker_handler:vipBunkerDeedID", SceneObject(pDeed):getObjectID())
		end
		return 1
	else
		return 0
	end
end

vipBunker = {  }

function vipBunker:fillObjectMenuResponse(pSceneObject, pMenuResponse, pPlayer)
	local response = LuaObjectMenuResponse(pMenuResponse)
	local state = CreatureObject(pPlayer):getScreenPlayState("vipQuest")
	
	if (state == 2) then
		response:addRadialMenuItem(20, 3, "Take Deed")
	else
		return 0
	end
end

function vipBunker:handleObjectMenuSelect(pSceneObject, pPlayer, selectedID)
	local state = CreatureObject(pPlayer):getScreenPlayState("vipQuest")
	
	if (state == 2) then
		writeData(SceneObject(pPlayer):getObjectID() .. ":vip_bunker_handler:vipBunkerUsed", 1)
		createEvent(1000, "vipBunker", "handleReward", pPlayer, "")
		CreatureObject(pPlayer):sendSystemMessage("You take the deed.")
	end
	
	return 0
end

function vipBunker:handleReward(pPlayer)
	if (pPlayer == nil) then
		return 0
	end
	
	local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
	local reward = "object/tangible/deed/player_house_deed/vipbunker_house_deed.iff"
	local state = CreatureObject(pPlayer):getScreenPlayState("vipQuest")

	if (pInventory == nil) then
		return
	end
	
	if (SceneObject(pInventory):isContainerFullRecursive()) then
		CreatureObject(pPlayer):sendSystemMessage("Your inventory is full. Make some space.")
		return
	end
	
	if (readData(SceneObject(pPlayer):getObjectID() .. ":vip_bunker_handler:vipBunkerUsed") >= 1 and state == 2) then
		CreatureObject(pPlayer):removeScreenPlayState(state, "vipQuest")
		CreatureObject(pPlayer):setScreenPlayState(4, "vipQuest")
		giveItem(pInventory, reward, -1)
		CreatureObject(pPlayer):sendSystemMessage("You have obtained the V.I.P. Bunker. Maybe you should keep it, instead of bringing it back to Bon Kar?")
		local playerID = SceneObject(pPlayer):getObjectID()
		local deedID = readData(playerID .. ":vip_bunker_handler:vipBunkerDeedID")
		local pDeed = getSceneObject(deedID)

		if (pDeed ~= nil) then
			SceneObject(pDeed):destroyObjectFromWorld()
			deleteData(playerID .. ":vip_bunker_handler:vipBunkerDeedID")
			deleteData(playerID .. ":vip_bunker_handler:vipBunkerUsed")
		end
		
	end
end
