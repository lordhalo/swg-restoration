village = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "village",
}
registerScreenPlay("village", true)

function village:start()
	if (isZoneEnabled("dathomir")) then
		self:spawnMobiles()
		self:spawnActiveAreas()
	end
end

function village:spawnActiveAreas()
	--local pArea = spawnSceneObject("dathomir", "object/active_area.iff", 5301, 78, -3984, 0, 0, 0, 0, 0)
	--local pArea2 = spawnSceneObject("dathomir", "object/active_area.iff", 5246, 78, -4173, 0, 0, 0, 0, 0)
	local pArea = spawnActiveArea("dathomir", "object/active_area.iff", 5301, 78, -3984, 25, 0)
      	local pArea2 = spawnActiveArea("dathomir", "object/active_area.iff", 5246, 78, -4173, 25, 0)		
	if (pArea ~= nil) then
		local activeArea = LuaActiveArea(pArea)
		--activeArea:setRadius(25)
		createObserver(ENTEREDAREA, "village", "EnterArea1", pArea)
	end

	if (pArea2 ~= nil) then
		local activeArea = LuaActiveArea(pArea2)
		--activeArea:setRadius(5)
		createObserver(ENTEREDAREA, "village", "EnterArea2", pArea2)
	end
end

--Mobile Spawning
function village:spawnMobiles()

	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5358.9, 78.5, -4045.1,  9,0)
	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5342.9, 78.5, -4036.1,  20,0)
	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5323.9, 78.5, -4029.1,  27,0)
	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5369.9, 78.5, -4005.1,  27,0)
	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5340.9, 78.5, -3997.1,  27,0)
	spawnMobile("dathomir", "fs_protecter", 1, math.random(20) + 5270.9, 78.5, -4246.1,  27,0)
	spawnMobile("dathomir", "fs_protecter", 1, 5265.9, 78.5, math.random(20) + -4225.1,  178,0)
	spawnMobile("dathomir", "fs_protecter", 1, 5280.9, 78.5, math.random(20) + -4226.1,  -166,0)
	spawnMobile("dathomir", "fs_protecter", 1, 5288.9, 78.5, math.random(20) + -4230.1,  -164,0)

	local pProt = spawnMobile("dathomir", "fs_protecter", 210, math.random(20) + 5288.9, 78.5, -4240.1,  -164,0)
 	createObserver(OBJECTDESTRUCTION, "village", "protDead", pProt)

	local pComm = spawnMobile("dathomir", "sith_shadow_commander", 210, 5355.85195, 78, -3955, -162, 0)
 	createObserver(OBJECTDESTRUCTION, "village", "heDead", pComm)


	--Sith Shadows
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5391.85294,  78,  math.random(30) + -3987, -136, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5395.85281,  78,  math.random(30) + -4002, -120, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5346.85240,  78,  math.random(30) + -3986, -157, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5329.85193,  78,  math.random(10) + -3969, -179, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(30) + 30, math.random(30) + 5322.85221,  78,  math.random(30) + -3978, 177, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(30) + 30, math.random(30) + 5351.85230, 78.7, math.random(30) + -4001, -149, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5370.85279, 78.5, math.random(30) + -3981, -152, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5380.85254, 78.5, math.random(30) + -4008, -123, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5295.85306, 78.5, math.random(30) + -4274, -25, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5388.85319, 78.5, math.random(30) + -4021, -97, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(40) + 30, math.random(30) + 5257.85365, 78.5, math.random(30) + -4263,  22, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(40) + 30, math.random(30) + 5280.85499, 79.4, math.random(30) + -4265,  -13, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5291.85514, 79,   math.random(30) + -4281, -20, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave", math.random(10) + 30, math.random(30) + 5264.85449, 78.6, math.random(30) + -4283, 8, 0)
	spawnMobile("dathomir","sith_shadow_commander", math.random(100) + 30, math.random(30) + 5276.85436, 80.5, math.random(30) + -4302, 1, 0)

	--Sith Shadow Mercs
	spawnMobile("dathomir","sith_shadow_mercenary", math.random(10) + 30, math.random(30) + 5482.85294,  78,  math.random(30) + -4035, -114, 0)
	spawnMobile("dathomir","sith_shadow_mercenary", math.random(10) + 30, math.random(30) + 5496.85294,  78,  math.random(30) + -4051, -110, 0)
	local pMerc = spawnMobile("dathomir", "sith_shadow_mercenary", 210, 5505.9, 79.5, -4212.8, -72, 0)
 	createObserver(OBJECTDESTRUCTION, "village", "mercDead", pMerc)

	--Camps
	--Camp 1
	spawnMobile("dathomir","sith_shadow_slave", 180, 5040, 87, -4193, 43, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5043, 87, -4187, 30, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5046, 87, -4190, 53, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5043, 87, -4194, 8, 0)

	--Camp 2
	spawnMobile("dathomir","sith_shadow_slave", 180, 5029, 81.5, -4100, -91, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5025, 81.5, -4100, 98, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5031, 81.8, -4108, -120, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5020, 82.4, -4109, 81, 0)

	--Camp 3
	spawnMobile("dathomir","sith_shadow_slave", 180, 5189, 78, -3911, 118, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5190, 78, -3916, 173, 0)

	--Camp 4
	spawnMobile("dathomir","sith_shadow_slave", 180, 5536, 78.6, -4080, 29, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5541, 78.6, -4081, -32, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5541, 78.6, -4075, -141, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5536, 78.6, -4075, 142, 0)

	--Camp 5
	spawnMobile("dathomir","sith_shadow_slave", 180, 5449, 80.8, -4284, 96, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5467, 80, -4287, -100, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5458, 80.3, -4277, -175, 0)

	--Main Camp
	spawnMobile("dathomir","sith_shadow_slave", 180, 5294, 89.3, -4350, 7, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5259, 90.8, -4347, -48, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5279, 88.5, -4334, 16, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5277, 88.8, -4381, 120, 0)
	spawnMobile("dathomir","sith_shadow_slave", 180, 5273, 90.8, -4358, 168, 0)


end

function village:heDead(pComm, pKiller)
	if (readData("village:slaves") == 0) then
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5268.85461, 78.7, math.random(30) + -4292, 2, 0)
	local pSlave = spawnMobile("dathomir","sith_shadow_slave",0, math.random(40) + 5268.85461, 78.7, math.random(40) + -4292, 2, 0)
	createObserver(OBJECTDESTRUCTION, "village", "deadSlave", pSlave)
	writeData("village:slaves",1)

     end
     
     return 0
end

function village:protDead(pProt, pKiller)
	if (readData("village:jedi") == 0) then
	local pJedi = spawnMobile("dathomir","fs_protecter_elite",0, math.random(10) + 5355.85195, 78, -3955, -162, 0)
	createObserver(OBJECTDESTRUCTION, "village", "deadJedi", pJedi)
	writeData("village:jedi",1)

     end
     
     return 0
end

function village:mercDead(pMerc, pKiller)
	if (readData("village:merc") == 0) then
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5494.85461, 79.4, math.random(30) + -4221, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5494.85461, 79.4, math.random(30) + -4221, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5494.85461, 79.4, math.random(30) + -4221, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5494.85461, 79.4, math.random(30) + -4221, 2, 0)
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5482.85294,  78,  math.random(30) + -4035, -114, 0) --Spawn with other 2 mercs
	spawnMobile("dathomir","sith_shadow_slave",0, math.random(30) + 5482.85294,  78,  math.random(30) + -4035, -114, 0) --
	local pMerc2 = spawnMobile("dathomir","sith_shadow_mercenary",0, math.random(10) + 5494.85195, 78, -4221, -162, 0)
	createObserver(OBJECTDESTRUCTION, "village", "deadMerc2", pMerc2)
	writeData("village:merc",1)

     end
     
     return 0
end


function village:EnterArea1(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("village:hesalive") == 0) then
			local pKray = spawnMobile("dathomir", "sith_shadow_slave", 0, math.random(40) + 5277, 78,  math.random(40) + -3983, 0, 0)
			spawnMobile("dathomir","sith_shadow_mercenary",0, math.random(30) + 5482.85294,  78,  math.random(30) + -4035, -114, 0) -- spawn with merc group 2
			createObserver(OBJECTDESTRUCTION, "village", "heDead2", pKray)
			writeData("village:hesalive",1)
		end
	end
	
	return 0
end

function village:EnterArea2(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("village:hesalive2") == 0) then

			spawnMobile("dathomir", "sith_shadow_slave", 0, math.random(3) + 5246, 78,  math.random(3) + -4173, 0, 0)

			local pSith = spawnMobile("dathomir", "sith_shadow_slave", 0, math.random(25) + 5388, 78,  math.random(25) + -3990, 0, 0)
			createObserver(OBJECTDESTRUCTION, "village", "heDead3", pSith)
			writeData("village:hesalive2",1)
		end
	end
	
	return 0
end

function village:heDead2(pKray, pKiller)
	writeData("village:hesalive",0)
     return 1
end

function village:heDead3(pKray, pKiller)
	writeData("village:hesalive2",0)
     return 1
end

function village:deadJedi(pJedi, pKiller)
	writeData("village:jedi",0)
     return 1
end

function village:deadMerc2(pMerc2, pKiller)
	writeData("village:merc",0)
     return 1
end

