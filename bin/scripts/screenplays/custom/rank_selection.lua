rank = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "rank",
}
registerScreenPlay("rank", true)

function rank:start()
	if (isZoneEnabled("taanab")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
		self:spawnActiveAreas()
	end
end

function rank:spawnActiveAreas()

end

--Mobile Spawning
function rank:spawnMobiles()	

end


--Objects
function rank:spawnSceneObjects()

	spawnSceneObject("taanab", "object/tangible/camp/camp_tent_s6.iff", 2409, 39.5, -511.1, 0, 0, 0, 0, 0)
	spawnSceneObject("taanab", "object/tangible/camp/camp_pavilion_s2.iff", 2409, 39.5, -511.1, 0, 0, 0, 0, 0)
	spawnSceneObject("taanab", "object/tangible/camp/camp_light_s2.iff", 2387, 38.1, -512.1, 0, 0, 0, 0, 0)
	spawnSceneObject("taanab", "object/tangible/camp/camp_lawn_chair.iff", 2405, 39.6, -504.1, 0, 0, 0, 0, 0)
end
