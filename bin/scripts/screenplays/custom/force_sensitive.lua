local QuestManager = require("managers.quest.quest_manager")

--[[
TODO: UPDATE THIS INFORMATION

DESC ABOUT QUEST

Quest Details:

You start the quest by talking to Thiel, it will then send you to talk to all the mentors and answering questions. 
Eventually leading to Rohak, after that you will need to find a holocron, this holocron will send you back to rohak.
Rohak will talk about sith shadows, and mellichae. You will then be given the choice of who you want to help... Paemos, or Sarguillo.
Whatever quest line you pick you will be tested in ways to determine what you will be: Light, Dark, or Exile.
After you complete the quest you will be sent to the according "enclave" to join that faction.



TODO:

-Paemos convo
-Captain convo * fix
-Lost FS convo
-Mellichae convo
	-Lightside done
	-Exile = need information from halo
	-Darkside more work needed

-Darkside side note
use sendTo:player
]]

local ObjectManager = require("managers.object.object_manager")
force_sensitive = ScreenPlay:new { 
	numberOfActs = 1, 
	questString = "ForceSensitive",
	states = {
		quest = { intro = 2, defense = 4, craft = 8, forced = 16, force = 32, forcetwo = 64, deftwo = 128, aulunone = 256, auluntwo = 512, aulunthree = 1024, aulunfour = 2048, aulunfive = 4096, aulunsix = 8192, aulunseven = 16384, auluneight = 32768 }
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
	
}

registerScreenPlay("force_sensitive", true)

function force_sensitive:start() 
	local pThiel = spawnMobile("yavin4","thiel", 1, 2.3, -18.9, 33.3, 1, 8525439)
	if ( pThiel ~= nil ) then
	end
	local pCaptain = spawnMobile("yavin4","captain_sarguillo", 1, 46.9, -22.1, 24.3, -118, 8525432)
	if (pCaptain ~= nil) then
	end
	local pCraft = spawnMobile("yavin4","laguo", 1, 47.5, -22.1, -24.7, -61, 8525429)
	if (pCraft ~= nil) then
	end
	local pForced = spawnMobile("yavin4","ithah", 1, -47.7, -22.1, -24.6, 61, 8525423)
	if (pForced ~= nil) then
	end	
	local pForce = spawnMobile("yavin4","joki", 1, -59.7, -22.1, 30.1, 115, 8525420)
	if (pForce ~= nil) then
	end
	local pAulun = spawnMobile("yavin4", "aulun", 1, 25.9, -24.1, 70.4, -88, 8525441)
	if (pAulun ~= nil) then
	end
	local pAulunTwo = spawnMobile("yavin4", "aulun_two", 1, -5502, 105.8, 4849, 102, 0)
	if (pAulunTwo ~= nil) then
	end
	local pAulunThree = spawnMobile("yavin4", "aulun_three", 1, -5793, 83.6, 4837, -9, 0)
	if (pAulunThree ~= nil) then
	end
	local pKliknik = spawnMobile("yavin4", "kliknik_questtwo", 1, -5794, 83.5, 4837.6, -35, 0)
	if (pKliknik ~= nil) then
	end
	local pCommander = spawnMobile("dathomir", "sith_shadow_commander", 1, 5276, 83.6, -4358, -166, 0)
        createObserver(OBJECTDESTRUCTION, "force_sensitive", "notifyCommanderDead", pCommander)
	if (pCommander ~= nil) then
	end

	self:SpawnSceneObjects()

end

function force_sensitive:SpawnSceneObjects()

	spawnSceneObject("yavin4", "object/tangible/camp/campfire_logs_fresh.iff", -5793, 83.5, 4838.9, 0, 0, 0, 0, 0)

end



--Commander + Mellichae
--TODO: Exploit check *repeating the quest and changing alignment*
function force_sensitive:notifyCommanderDead(pMellichae, pKiller)
	local creature = LuaCreatureObject(pKiller)
	local isReady = creature:hasScreenPlayState(force_sensitive.states.quest.mission, force_sensitive.questString)
	if isReady == 1 then
		if (readData("force_sensitive:forceSensitiveMellichae") == 0 ) then
	        	local pMellichae = spawnMobile("dathomir", "mellichae", 0, 5276, 90.6, -4359, -166, 0)
	        	createObserver(OBJECTDESTRUCTION, "force_sensitive", "notifyCommanderDead", pMellichae)
			createObserver(DAMAGERECEIVED, "force_sensitive", "mellichae_damage", pMellichae)
	        	writeData("force_sensitive:forceSensitiveMellichae",1)
			creature:sendSystemMessage("A Cloud of Darkness tunnels around the tent, As you aproach, The pillar of Darkness dissipates as if you interupted something.")
			--[[ doesn't work
			local pTerminal = spawnSceneObject("dathomir", "object/tangible/wearables/ring/ring_s04.iff", 5276, 90, -4359, 0, 0, 0, 1, 0)
			local term = LuaSceneObject(pTerminal)
			term:setCustomObjectName("Ring")
			createObserver(OBJECTINRANGEMOVED, "force_sensitive", "mellichaePulled", pTerminal)]]
		end
	end
	     
     return 0
end

--[[
doesn't work.
function force_sensitive:mellichaePulled(pMellichae, pTerminal)
	local mel = LuaCreatureObject(pMellichae)
	local term = LuaSceneObject(pTerminal)
	distance = mel:getDistanceTo(pTerminal)

	if distance >= 30 then
		writeData("force_sensitive:escapeMellichae",1)print("escape")
	end

end]]

function force_sensitive:mellichae_damage(creatureObject, playerObject, damage)
	if creatureObject == nil or playerObject == nil then
		return 0
	end
	
	
	local player = LuaCreatureObject(playerObject)
	local creature = LuaCreatureObject(creatureObject)
	local random = math.random(2)
	
	--This is the setup for the triggers for chat
	health = creature:getHAM(0)
	action = creature:getHAM(3)
	mind = creature:getHAM(6)
	
	maxHealth = creature:getMaxHAM(0)
	maxAction = creature:getMaxHAM(3)
	maxMind = creature:getMaxHAM(6)

	if (((health <= (maxHealth * 0.9)) or (action <= (maxAction * 0.9)) or (mind <= (maxMind * 0.9))) and readData("force_sensitive:spawnFirstGroup") == 0) then
		spatialChat(creatureObject, "What a unexpected suprise.")
		creature:playEffect("clienteffect/pl_force_feedback_self.cef", "")
		writeData("force_sensitive:spawnFirstGroup",1)
		if (readData("force_sensitive:spawnFirstGroup") == 1) then
			local pForce = spawnMobile("dathomir","sith_shadow_slave", 0, player.getPositionX, player.getPositionY, player.getPositionZ, 0, 0)
			local firstGroup = LuaCreatureObject(pForce)
			firstGroup:engageCombat(playerObject)
		end
		
	elseif (((health <= (maxHealth * 0.7)) or (action <= (maxAction * 0.7)) or (mind <= (maxMind * 0.7))) and readData("force_sensitive:spawnSecondGroup") == 0) then
		spatialChat(creatureObject, "Impressive, but your ability to manipulate the force does not make you a Jedi.")
		--player:setState(CreatureState:IMMOBILIZED)
		writeData("force_sensitive:spawnSecondGroup",1)
		if (readData("force_sensitive:spawnSecondGroup") == 1) then
			local pForce1 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local pForce2 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local secondGroup = LuaCreatureObject(pForce1)
			local secondGroup1 = LuaCreatureObject(pForce2)
			secondGroup1:engageCombat(playerObject)
			secondGroup:engageCombat(playerObject)
		end

	elseif (((health <= (maxHealth * 0.4)) or (action <= (maxAction * 0.4)) or (mind <= (maxMind * 0.4))) and readData("force_sensitive:spawnThirdGroup") == 0) then
		spatialChat(creatureObject, "A Worthy adversary.")
		creature:playEffect("clienteffect/pl_force_heal_self.cef", "")
		creature:setHAM(0,health + 150)
		writeData("force_sensitive:spawnThirdGroup",1)
		if (readData("force_sensitive:spawnThirdGroup") == 1) then
			local pForce3 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local pForce4 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local pForce5 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local pForce6 = spawnMobile("dathomir","sith_shadow_slave", 0, 5296, 78.5, -4172, 0, 0)
			local thirdGroup = LuaCreatureObject(pForce3)
			local thirdGroup1 = LuaCreatureObject(pForce4)
			local thirdGroup2 = LuaCreatureObject(pForce5)
			local thirdGroup3 = LuaCreatureObject(pForce6)
			thirdGroup:engageCombat(playerObject)
			thirdGroup1:engageCombat(playerObject)
			thirdGroup2:engageCombat(playerObject)
			thirdGroup3:engageCombat(playerObject)
		end
	elseif (((health <= (maxHealth * 0.3)) or (action <= (maxAction * 0.3)) or (mind <= (maxMind * 0.3))) and readData("force_sensitive:talkThree") == 0) then
		spatialChat(creatureObject, "Such potential, I could teach you untold knowedge.")
		writeData("force_sensitive:talkThree",1)

	elseif (((health <= (maxHealth * 0.2)) or (action <= (maxAction * 0.2)) or (mind <= (maxMind * 0.2))) and readData("force_sensitive:escapeMellichae") == 0) then
		spatialChat(creatureObject, "This brawl is over for now, I leave you with a offer. Join me and help crush these pathetic Force wielders and tap into the power you have yet control.")
		writeData("force_sensitive:escapeMellichae",1)

	elseif (((health <= (maxHealth * 0.19)) or (action <= (maxAction * 0.19)) or (mind <= (maxMind * 0.19))) and readData("force_sensitive:escapeMellichae") == 1) then



		if (readData("force_sensitive:escapeMellichae") == 1) then
			forcePeace(playerObject)
			local creo = LuaSceneObject(creatureObject)
			creo:switchZone("character_farm", -200, 0, -200, 0)
			creo:destroyObjectFromWorld()
			player:setScreenPlayState(force_sensitive.states.quest.missionComp, force_sensitive.questString)

		end	

		return 1
	end

	return 0
end

function force_sensitive:updateWaypoint(pConversingPlayer, planetName, x, y, direction)
	ObjectManager.withCreatureObject(pConversingPlayer, function(creature)
		local pGhost = creature:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint(planetName, self:getMissionDescription(pConversingPlayer, direction), "", x, y, WAYPOINT_COLOR_PURPLE, true, true, WAYPOINTTHEMEPARK, 0)
		end
	end)
end


--Setup
function force_sensitive:getActivePlayerName()
	return self.questdata.activePlayerName
end

function force_sensitive:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end




--Thiels Conv
force_sensitive_convo_handler = Object:new {
	
 }

function force_sensitive_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)

			local  hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)

			local  hasAcceptedTwo = creature:hasScreenPlayState(force_sensitive.states.quest.aulunseven, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_b_4")

			if ( hasAcceptedTwo == true and hasSkill == true) then
				nextConversationScreen = conversation:getScreen("return_screen")--First convo screen to pull.
			elseif ( hasAcceptedTwo == true and hasSkill == false) then
				nextConversationScreen = conversation:getScreen("crystal_screen")--First convo screen to pull.
			elseif ( hasAccepted == false ) then
				nextConversationScreen = conversation:getScreen("intro_first_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("complete")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)	
	local pInventory = player:getSlottedObject("inventory")
	if ( screenID == "thielFinal" ) then
		player:setScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
		giveItem(pInventory, "object/tangible/loot/quest/force_sensitive/force_crystal.iff", -1)
		giveItem(pInventory, "object/tangible/crafting/station/generic_tool.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_mineral.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_liquid.iff", -1)
		giveItem(pInventory, "object/weapon/melee/baton/baton_fs.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/medicine/crafted/crafted_stimpack_sm_s1_a.iff", -1)
		giveItem(pInventory, "object/tangible/medicine/crafted/crafted_stimpack_sm_s1_a.iff", -1)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Captain Sarguillo", "", -5537, 4958, 5, true, true, WAYPOINTTHEMEPARK, 1)
			--QuestManager.activateQuest(conversingPlayer, QuestManager.quests.TwO_MILITARY)
		end
	end

	if (screenID == "fs_done" ) then
		player:setScreenPlayState(force_sensitive.states.quest.auluneight, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Max Suran", "", -5536, 4927, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	--print("returning convosvreen")
	return conversationScreen
end


--##############################################################################################################################################
--###################################################################  CAPTAIN	################################################################
--##############################################################################################################################################
--captain

force_sensitive_defense_conv_handler = Object:new {

}


function force_sensitive_defense_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
			local partTwo = creature:hasScreenPlayState(force_sensitive.states.quest.forcetwo, force_sensitive.questString)
			local partTwoComplete = creature:hasScreenPlayState(force_sensitive.states.quest.deftwo, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_d_2")  
			
			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("not_yet")
			elseif hasAccepted == true and hasComplete == false then 
				nextConversationScreen = conversation:getScreen("defense_first_screen")	
			elseif partTwo == true and hasSkill == true then
				nextConversationScreen = conversation:getScreen("def_part2")	
			elseif partTwo == true and hasSkill == false then
				nextConversationScreen = conversation:getScreen("def_part2_crystal")
			elseif partTwoComplete == true then
				nextConversationScreen = conversation:getScreen("complete2")	
			elseif hasComplete == true then
				nextConversationScreen = conversation:getScreen("defFinalOne")		
			else				
				nextConversationScreen = conversation:getScreen("complete")	--edit to different reply	
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end
	end			
	return nextConversationScreen	
end


function force_sensitive_defense_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local creature = LuaCreatureObject(conversingNPC)

	if (screenID == "info2") then
		creature:playEffect("clienteffect/pl_force_armor_self.cef", "")
	end 
	

	if (screenID == "def6") then
		--player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			local pOne = spawnMobile("yavin4", "jedi_trainer_droid", 0, 44.1, -22.1, 27.1, -166, 8525432)
			writeData("force_sensitive:trainerOneKill",0)
       			createObserver(OBJECTDESTRUCTION, "force_sensitive", "trainerOneDead", pOne)	
		end
	end

	if (screenID == "defFinal") then
		--player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "La'Guo", "", -5489, 4959, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end

	if (screenID == "def_part4") then
		player:setScreenPlayState(force_sensitive.states.quest.deftwo, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Aulun", "", -5584, 4937, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive:trainerOneDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive:trainerOneKill") == 0 ) then

	        	writeData("force_sensitive:trainerOneKill",1)
			creature:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)

		end
	     
     return 0
end


--##############################################################################################################################################
--###################################################################  	LAGUO	################################################################
--##############################################################################################################################################
--La'guo

force_sensitive_craft_conv_handler = Object:new {

}

function force_sensitive_craft_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_1")

			--print("hasAccepted() is " .. hasAccepted)   
			
			if ((hasAccepted == false and hasComplete == false)) then
				nextConversationScreen = conversation:getScreen("craft_not_yet")--First convo screen to pull.
				
			else
				--### KEY CONVO PARTS, AND STATES ###--
				if (hasSkill == true) then
					if ((hasAccepted == true) and (hasComplete == false)) then 
						nextConversationScreen = conversation:getScreen("craft_first_screen")
					elseif (hasComplete == true) then
						nextConversationScreen = conversation:getScreen("complete")			
					else
						nextConversationScreen = conversation:getScreen("complete")
					end
				else
					nextConversationScreen = conversation:getScreen("craft_not_yet")
				end			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_craft_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)	
	if (screenID == "craftFinal") then
		player:setScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Ithah", "", -5489, 4863, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end


--##############################################################################################################################################
--###################################################################  PAEMOS	################################################################
--##############################################################################################################################################
--paemos

force_sensitive_forced_conv_handler = Object:new {

}


function force_sensitive_forced_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.forced, force_sensitive.questString) 
			local hasSkill = creature:hasSkill("fs_jedi_b_1") 

			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("forced_not_yet")
			elseif hasAccepted == true and hasComplete == false  then 
				if(hasSkill == true) then 
					nextConversationScreen = conversation:getScreen("forced_first_screen")	
				else
					nextConversationScreen = conversation:getScreen("forced_not_yet")
				end							
			else			
				nextConversationScreen = conversation:getScreen("forced_thank_you")			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end
	end			
	return nextConversationScreen	
end

function force_sensitive_forced_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.forced, force_sensitive.questString)	
	if (screenID == "forced_complete") then
		player:setScreenPlayState(force_sensitive.states.quest.forced, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Joki", "", -5543, 4852, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end

	return conversationScreen
end



--##############################################################################################################################################
--###################################################################  NOLDAN	################################################################
--##############################################################################################################################################

--noldan
force_sensitive_force_conv_handler = Object:new {

}

function force_sensitive_force_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.forced, force_sensitive.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.force, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_c_1")   
			local hasSkillTwo = creature:hasSkill("fs_jedi_d_1")   

			if ((hasAccepted == false and hasComplete == false)) then
				nextConversationScreen = conversation:getScreen("force_not_yet")--First convo screen to pull.
				
			else
				--### KEY CONVO PARTS, AND STATES ###--
				if ((hasAccepted == true) and (hasComplete == false)) then 
					if (hasSkill == true) then
					nextConversationScreen = conversation:getScreen("force_first_screen")
					else
					nextConversationScreen = conversation:getScreen("force_not_yet")
					end
				elseif (hasComplete == true and hasSkillTwo == true) then
					nextConversationScreen = conversation:getScreen("force_part2")			
				else
					nextConversationScreen = conversation:getScreen("force_thank_you")
				end			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_force_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.force, force_sensitive.questString)	

	if (screenID == "force_continue") then
		player:playEffect("clienteffect/pl_force_absorb_self.cef", "")
	end 

	if (screenID == "force1") then
		player:playEffect("clienteffect/pl_force_heal_self.cef", "")
	end 

	if (screenID == "force2") then
		player:setScreenPlayState(force_sensitive.states.quest.force, force_sensitive.questString)
	end

	if (screenID == "force_part5") then
		player:setScreenPlayState(force_sensitive.states.quest.forcetwo, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Captain Sarguillo", "", -5537, 4958, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end	
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end






--##############################################################################################################################################
--###################################################################  ROHAK	################################################################
--##############################################################################################################################################


force_sensitive_rohak_conv_handler = Object:new {

}

function force_sensitive_rohak_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local conversation = LuaConversationTemplate(conversationTemplate)
	local convosession = creature:getConversationSession()
	local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.force, force_sensitive.questString)
	local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.rohak, force_sensitive.questString)
	local missionStart = creature:hasScreenPlayState(force_sensitive.states.quest.mission, force_sensitive.questString)
	local missionComp = creature:hasScreenPlayState(force_sensitive.states.quest.missionComp, force_sensitive.questString)
	local isJedi = creature:hasScreenPlayState(force_sensitive.states.quest.jedi, force_sensitive.questString)

	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then 
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then 
			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("rohak_not_yet")
			elseif hasAccepted == true and hasComplete == false then 
				nextConversationScreen = conversation:getScreen("rohak_first_screen")
			elseif hasComplete == true and missionStart == false then
				nextConversationScreen = conversation:getScreen("rohak_thank_you")
				if creature:hasSkill("fs_jedi_a_4") and creature:hasSkill("fs_jedi_b_4") and creature:hasSkill("fs_jedi_c_4") and creature:hasSkill("fs_jedi_d_4") and missionStart == false then
					nextConversationScreen = conversation:getScreen("rohak_holocron")
				end
			elseif missionComp == true and isJedi == false then
				nextConversationScreen = conversation:getScreen("jedi")
			else
				nextConversationScreen = conversation:getScreen("rohak_thank_you")			
			end	
		else	
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen
end


function force_sensitive_rohak_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if (screenID == "rohak_complete") then
		player:setScreenPlayState(force_sensitive.states.quest.rohak, force_sensitive.questString)
	elseif (screenID == "capture") then
		player:setScreenPlayState(force_sensitive.states.quest.mission, force_sensitive.questString)
	elseif (screenID == "completelight") then
		player:setScreenPlayState(force_sensitive.states.quest.jedi, force_sensitive.questString)
	elseif (screenID == "completedark3") then
		player:setScreenPlayState(force_sensitive.states.quest.dark, force_sensitive.questString)
		player:sendSystemMessage("You have chosen your path and must now find your way Mellichae.")
		--local creo = LuaSceneObject(conversingPlayer)
		--creo:switchZone("dathomir", 5469, 78.5, -3996, 0)
	elseif (screenID == "completegrey") then
		player:setScreenPlayState(force_sensitive.states.quest.grey, force_sensitive.questString)
	elseif (screenID == "leavejedi") then
		player:setScreenPlayState(force_sensitive.states.quest.quit, force_sensitive.questString)
		giveItem(pInventory, "object/tangible/jedi/no_drop_jedi_holocron_dark.iff", -1)
	end

	return conversationScreen
end

force_sensitive_aulun_conv_handler = Object:new {

}

function force_sensitive_aulun_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.deftwo, force_sensitive.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive.states.quest.aulunone, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_2")  

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("aulun_parttwo")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("aulun_first_screen")
				elseif (hasComplete == true and hasSkill == false)then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("hello_screen")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_aulun_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)	
	if (screenID == "aulun4") then
			local pDroid = spawnMobile("yavin4", "trainer_droids_quest", 0, math.random(2) + 21.7, -24.1, 63.6, -88, 8525441)
			writeData("force_sensitive:aulun1",0)
       			createObserver(OBJECTDESTRUCTION, "force_sensitive", "aulunOneDead", pDroid)	
	end

	if (screenID == "aulun_parttwo") then
		player:setScreenPlayState(force_sensitive.states.quest.auluntwo, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Meet outside", "", -5502, 4849, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive:aulunOneDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive:aulun1") == 0 ) then

	        	writeData("force_sensitive:aulun1",1)
			creature:setScreenPlayState(force_sensitive.states.quest.aulunone, force_sensitive.questString)

		end
	     
     return 0
end

force_sensitive_auluntwo_conv_handler = Object:new {

}

function force_sensitive_auluntwo_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.auluntwo, force_sensitive.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive.states.quest.aulunthree, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_3")  

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("turnin")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("auluntwo_first_screen")
				elseif (hasComplete == true and hasSkill == false) then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_auluntwo_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)	
	if (screenID == "fork4") then

		if(math.random(2) == 2) then
			local pKlik1 = spawnMobile("yavin4", "kliknik_quest", 0, -5371, 84.1, 4899.0, -98, 0)

			local pKlik2 = spawnMobile("yavin4", "kliknik_quest", 0, -5362, 83.3, 4894.0, -98, 0)

			local pKlik3 = spawnMobile("yavin4", "kliknik_quest", 0, -5361, 83.4, 4904.0, -98, 0)
			writeData("force_sensitive:klik3",0)
			createObserver(OBJECTDESTRUCTION, "force_sensitive", "klikThreeDead", pKlik3)
			
			local pGhost = player:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Kliknik's", "", -5371, 4899, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end
		else
			local pKlik1 = spawnMobile("yavin4", "kliknik_quest", 0, -5362, 83.4, 4826.0, -98, 0)

			local pKlik2 = spawnMobile("yavin4", "kliknik_quest", 0, -5357, 83.1, 4819.0, -98, 0)

			local pKlik3 = spawnMobile("yavin4", "kliknik_quest", 0, -5345, 83.4, 4831.0, -98, 0)
			writeData("force_sensitive:klik3",0)
			createObserver(OBJECTDESTRUCTION, "force_sensitive", "klikThreeDead", pKlik3)

			local pGhost = player:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Kliknik's", "", -5362, 4826, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end
		end	
	end

	if (screenID == "aulun_parttwo") then
		player:setScreenPlayState(force_sensitive.states.quest.auluntwo, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Meet outside", "", -5502, 4849, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end

	if (screenID == "turnin") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunfour, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Captured Kliknik", "", -5793, 4838, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive:klikThreeDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive:klik3") == 0 ) then

	        	writeData("force_sensitive:klik3",1)
			creature:setScreenPlayState(force_sensitive.states.quest.aulunthree, force_sensitive.questString)

			local pGhost = creature:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Return", "", -5502, 4849, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end

		end
	     
     return 0
end

force_sensitive_aulunthree_conv_handler = Object:new {

}

function force_sensitive_aulunthree_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.aulunfour, force_sensitive.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive.states.quest.aulunfive, force_sensitive.questString)
			local hasCompleteThree = creature:hasScreenPlayState(force_sensitive.states.quest.aulunsix, force_sensitive.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_4")  


				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteThree == true) then
					nextConversationScreen = conversation:getScreen("aulun_returned")
				elseif (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("aulun_waiting")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("aulun_kliknik")
				elseif (hasComplete == true and hasSkill == false) then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_aulunthree_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)	
	if (screenID == "aulun_kliknik3") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunfive, force_sensitive.questString)
	end

	if (screenID == "aulun_returned4a") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunseven, force_sensitive.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Janakus", "", -5547, 4914, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

force_sensitive_kliknik_conv_handler = Object:new {

}

function force_sensitive_kliknik_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive.states.quest.aulunfive, force_sensitive.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive.states.quest.aulunsix, force_sensitive.questString)

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("aulun_waiting")			
				elseif (hasComplete == true) then
					nextConversationScreen = conversation:getScreen("kliknik_one")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_kliknik_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.craft, force_sensitive.questString)
	local creature = LuaCreatureObject(conversingNPC)	
	if (screenID == "kliknik_3a") then
		player:playEffect("clienteffect/pl_force_heal_self.cef", "")
	end

	if (screenID == "kliknik_3b") then
		player:playEffect("clienteffect/pl_force_heal_dark.cef", "")
	end

	if (screenID == "kliknik_4a") then
		creature:playEffect("clienteffect/pl_force_absorb_self.cef", "")
	end

	if(screenID == "kliknik_5a") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunsix, force_sensitive.questString)
	end

	if(screenID == "kliknik_5b") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunsix, force_sensitive.questString)
	end

	if(screenID == "kliknik_5c") then
		player:setScreenPlayState(force_sensitive.states.quest.aulunsix, force_sensitive.questString)
	end
	
	--print("returning convosvreen")
	return conversationScreen
end


