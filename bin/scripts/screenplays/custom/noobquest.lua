jedi_intro = ScreenPlay:new {
	numberOfActs = 1,
	questString = "jedi_intro",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("jedi_intro", true)

function jedi_intro:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end


--Mobile Spawning
function jedi_intro:spawnMobiles()
	spawnMobile("dathomir", "old_man", 1, 5298, 78.5, -4156,  -111, 0)
	spawnMobile("dathomir", "utao_wiagh", 1, 1653, 103, -5763,  -14, 0)
	spawnMobile("dathomir", "mellichaetalk", 1, 5547, 101, -1523,  46, 0)
	--spawnMobile("dathomir", "fs_follower", 1, 5304.2, 78.5, -4187.4,  -60, 0)


end

--Setup
function jedi_intro:getActivePlayerName()
	return self.questdata.activePlayerName
end

function jedi_intro:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

jedi_intro_convo_handler = Object:new {
	
 }

function jedi_intro_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local accepted = creature:hasScreenPlayState(jedi_intro.states.quest.phaseone, jedi_intro.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			if ( accepted == true ) then
				nextConversationScreen = conversation:getScreen("done_screen")--First convo screen to pull.

			else
				nextConversationScreen = conversation:getScreen("jedi_intro_screen")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function jedi_intro_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "fourth_screen") then
		player:setScreenPlayState(jedi_intro.states.quest.phaseone, jedi_intro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("dathomir", "Utao", "", 1653, -5763, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end

	if (screenID == "dark_screen") then
		player:playEffect("clienteffect/pl_force_choke.cef", "")
		player:setScreenPlayState(jedi_intro.states.quest.phaseone, jedi_intro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			player:sendSystemMessage("You have chosen your path and must now find your way Mellichae.")
			PlayerObject(pGhost):addWaypoint("dathomir", "Mellichae", "", 5546, -1523, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

utato_convo_handler = Object:new {
	
 }

function utato_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local isReady = creature:hasScreenPlayState(jedi_intro.states.quest.phaseone, jedi_intro.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
	
			local creature = LuaCreatureObject(conversingPlayer)

			
			if ( isReady == true) then
				nextConversationScreen = conversation:getScreen("first_screen")--End of the road.	
			else
				nextConversationScreen = conversation:getScreen("leave_screen")--End of the road.	
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function utato_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "third_screen") then
		player:setScreenPlayState(jedi_intro.states.quest.phasetwo, jedi_intro.questString)
	end
	
	return conversationScreen
end

mellichae_convo_handler = Object:new {
	
 }

function mellichae_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local isReady = creature:hasScreenPlayState(jedi_intro.states.quest.phaseone, jedi_intro.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
	
			local creature = LuaCreatureObject(conversingPlayer)

			
			if ( isReady == true) then
				nextConversationScreen = conversation:getScreen("first_screen")--End of the road.	
			else
				nextConversationScreen = conversation:getScreen("leave_screen")--End of the road.	
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function mellichae_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "third_screen") then
		player:setScreenPlayState(jedi_intro.states.quest.phasetwo, jedi_intro.questString)
	end
	
	return conversationScreen
end
