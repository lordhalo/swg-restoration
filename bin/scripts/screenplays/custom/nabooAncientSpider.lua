local ObjectManager = require("managers.object.object_manager")

nabooAncientSpider = ScreenPlay:new {
	numberOfActs = 1,
}
registerScreenPlay("nabooAncientSpider", true)

function nabooAncientSpider:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
	end
end

function nabooAncientSpider:spawnMobiles()
	
	local pSpider = spawnMobile("naboo", "naboo_ancient_spider", 10800, -7396, 2, 915,  106, 0)
	createObserver(DAMAGERECEIVED, "nabooAncientSpider", "npcDamageObserver", pSpider)
end

function nabooAncientSpider:npcDamageObserver(bossObject, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.8)) or (action <= (maxAction * 0.8)) or (mind <= (maxMind * 0.8))) and readData("nabooSpider:npcDamaged") == 0) then
		self:spawnGuards(playerObject)
		if(readData("nabooSpider:npcDamaged") == 0) then
			createEvent(60000, "nabooAncientSpider", "startHealing", bossObject, "")
		end
		writeData("nabooSpider:npcDamaged",1)
	end

	return 0

end

function nabooAncientSpider:startHealing(bossObject)

	local boss = LuaCreatureObject(bossObject)


	if (readData("spiderBoss:guardOneAlive") == 1 and readData("spiderBoss:guardTwoAlive") == 1) then  
		createEvent(600, "nabooAncientSpider", "startHealing", bossObject, "")
		boss:healDamage(10000, 0)
		boss:healDamage(10000, 3)
		boss:healDamage(10000, 6)
	elseif (readData("spiderBoss:guardOneAlive") == 1 or readData("spiderBoss:guardTwoAlive") == 1) then
		createEvent(600, "nabooAncientSpider", "startHealing", bossObject, "")
		boss:healDamage(5000, 0)
		boss:healDamage(5000, 3)
		boss:healDamage(5000, 6)

	end	

	return 0
end

function nabooAncientSpider:clearEvent()
	writeData("bikerBoss:npcDamaged",0)
	writeData("bikerBoss:npcDamagedTwo",0)
	writeData("bikerBoss:npcDamagedThree",0)
end

function nabooAncientSpider:spawnGuards(playerObject)
	--local player = LuaCreatureObject(playerObject)

	local spiderOne = spawnMobile("naboo", "ancient_spider_guard", 0, -7393, 3, 904,  178, 0)
	local spiderTwo = spawnMobile("naboo", "ancient_spider_guard", 0, -7393, 3, 920,  178, 0)
	createObserver(OBJECTDESTRUCTION, "nabooAncientSpider", "guardOneDead", spiderOne)
	createObserver(OBJECTDESTRUCTION, "nabooAncientSpider", "guardTwoDead", spiderTwo)

	CreatureObject(spiderOne):engageCombat(playerObject)
	CreatureObject(spiderTwo):engageCombat(playerObject)

	writeData("spiderBoss:guardOneAlive",1)
	writeData("spiderBoss:guardTwoAlive",1)

	--AiAgent(thugOne):stopWaiting()
	--AiAgent(thugOne):setWait(0)
	--AiAgent(thugOne):setNextPosition(-272, 28, -4851, 0)
	--AiAgent(thugOne):executeBehavior()
	

end

function nabooAncientSpider:guardOneDead()
	writeData("spiderBoss:guardOneAlive",0)

	return 1
end

function nabooAncientSpider:guardTwoDead()
	writeData("spiderBoss:guardTwoAlive",0)

	return 1
end
