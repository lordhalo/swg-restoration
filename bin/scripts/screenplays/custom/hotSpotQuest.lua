local ObjectManager = require("managers.object.object_manager")

hotSpotQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "hotSpotQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("hotSpotQuest", true)

function hotSpotQuest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
	end
end


function hotSpotQuest:spawnMobiles()
	spawnMobile("naboo", "theed_smuggler", 0, -5069, 6, 4286,  -107, 0)
end



--Setup

function hotSpotQuest:bossDead(pBoss, pPlayer)
	local player = LuaCreatureObject(pPlayer)
	local  hasComplete = player:hasScreenPlayState(hotSpotQuest.states.quest.phaseone, hotSpotQuest.questString)
	if (hasComplete == true) then
		player:setScreenPlayState(hotSpotQuest.states.quest.phasetwo, hotSpotQuest.questString)
	end
	createEvent(2 * 60 * 1000, "hotSpotQuest", "spawnMobiles", player, "")
	return 1
end
function hotSpotQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function hotSpotQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function hotSpotQuest:getSpawnPoints(numberOfSpawns, x, y, pConversingPlayer)
	if (pConversingPlayer == nil) then
		return {}
	end

	local spawnPoints = {}

	if (numberOfSpawns == 0) then
		return spawnPoints
	end

	local firstSpawnPoint

	printf("X", x);
	printf("Y", y);
	firstSpawnPoint = getSpawnPoint(pConversingPlayer, x, y, 15, 25, true)

	--if firstSpawnPoint ~= nil then
	--	table.insert(spawnPoints, firstSpawnPoint)
	--	for i = 2, numberOfSpawns, 1 do
	--		local nextSpawnPoint
	--			nextSpawnPoint = getSpawnPoint(pConversingPlayer, firstSpawnPoint[1], firstSpawnPoint[3], 10, 20, true)
	--		if nextSpawnPoint ~= nil then
	--			table.insert(spawnPoints, nextSpawnPoint)
	--		end
	--	end
	--end

	return spawnPoints
end

function hotSpotQuest:spawnNpc(npcTemplate, position, pConversingPlayer, spawnNumber)
	if pConversingPlayer == nil then
		return nil
	end

	local planetName
	--if (npcTemplate.planetName == "generic") then
		planetName = CreatureObject(pConversingPlayer):getZoneName()
	--else
	--	planetName = npcTemplate.planetName
	--end

	local pNpc = spawnMobile(planetName, "dark_trooper_pvp", 0, position[1], position[2], position[3], getRandomNumber(360) - 180, position[4])

	--if pNpc ~= nil and SceneObject(pNpc):isCreatureObject() then
	--	local npcName = self:getNpcName(npcTemplate.npcName)
	--	CreatureObject(pNpc):setCustomObjectName(npcName)
	--	writeData(CreatureObject(pConversingPlayer):getObjectID() .. ":missionSpawn:no" .. spawnNumber, CreatureObject(pNpc):getObjectID())
	--end

	return pNpc
end


hot_spot_quest_rebel_convo_handler = Object:new {
	
 }

function hot_spot_quest_rebel_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

				nextConversationScreen = conversation:getScreen("greet_friend")

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function hot_spot_quest_rebel_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then

		local spawnPoints
		local planetName

		planetName = CreatureObject(conversingPlayer):getZoneName()

		--spawnPoints = hotSpotQuest:getSpawnPoints(1, 300, player:getPositionY(), conversingPlayer)
		spawnPoints = getSpawnPoint(pConversingPlayer, player:getPositionX(), player:getPositionY(), 15, 25, true)
		player:setScreenPlayState(hotSpotQuest.states.quest.phaseone, hotSpotQuest.questString)
		hotSpotQuest:spawnNpc("dark_trooper_pvp", spawnPoints, conversingPlayer, 1)
		
		ThemeParkLogic:updateWaypoint(conversingPlayer, planetName, spawnPoints[1], spawnPoints[3], "target")
	end

	if ( screenID == "last_screen2" ) then
		player:setScreenPlayState(hotSpotQuest.states.quest.phasethree, hotSpotQuest.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_05.iff", -1)
	end
	


	return conversationScreen
end

