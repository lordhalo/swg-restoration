local ObjectManager = require("managers.object.object_manager")

nabooForceSurvivor = ScreenPlay:new {
	numberOfActs = 1,
}
registerScreenPlay("nabooForceSurvivor", true)

function nabooForceSurvivor:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
	end
end

function nabooForceSurvivor:spawnMobiles()
	
	local pSpider = spawnMobile("naboo", "force_survivor", 10800, -4867, 404, 811,  106, 0)
	createObserver(DAMAGERECEIVED, "nabooForceSurvivor", "npcDamageObserver", pSpider)
end

function nabooForceSurvivor:npcDamageObserver(bossObject, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.8)) or (action <= (maxAction * 0.8)) or (mind <= (maxMind * 0.8))) and readData("forceSurvivor:npcDamaged") == 0) then
		self:spawnGuards(playerObject)
		if(readData("forceSurvivor:npcDamaged") == 0) then
			createEvent(60000, "nabooForceSurvivor", "startHealing", bossObject, "")
		end
		writeData("forceSurvivor:npcDamaged",1)
	end

	return 0

end

--Create Object Out of Range check

--Create Lightning spawns randomly that deal damage.

function nabooForceSurvivor:startHealing(bossObject)

	local boss = LuaCreatureObject(bossObject)


	if (readData("spiderBoss:guardOneAlive") == 1 and readData("spiderBoss:guardTwoAlive") == 1) then  
		createEvent(600, "nabooForceSurvivor", "startHealing", bossObject, "")
		boss:healDamage(10000, 0)
		boss:healDamage(10000, 3)
		boss:healDamage(10000, 6)
	elseif (readData("spiderBoss:guardOneAlive") == 1 or readData("spiderBoss:guardTwoAlive") == 1) then
		createEvent(600, "nabooForceSurvivor", "startHealing", bossObject, "")
		boss:healDamage(5000, 0)
		boss:healDamage(5000, 3)
		boss:healDamage(5000, 6)

	end	

	return 0
end

function nabooForceSurvivor:clearEvent()
	writeData("bikerBoss:npcDamaged",0)
	writeData("bikerBoss:npcDamagedTwo",0)
	writeData("bikerBoss:npcDamagedThree",0)
end

function nabooForceSurvivor:spawnGuards(playerObject)
	--local player = LuaCreatureObject(playerObject)

	local spiderOne = spawnMobile("naboo", "ancient_spider_guard", 0, -7393, 3, 904,  178, 0)
	local spiderTwo = spawnMobile("naboo", "ancient_spider_guard", 0, -7393, 3, 920,  178, 0)
	createObserver(OBJECTDESTRUCTION, "nabooForceSurvivor", "guardOneDead", spiderOne)
	createObserver(OBJECTDESTRUCTION, "nabooForceSurvivor", "guardTwoDead", spiderTwo)

	CreatureObject(spiderOne):engageCombat(playerObject)
	CreatureObject(spiderTwo):engageCombat(playerObject)

	writeData("spiderBoss:guardOneAlive",1)
	writeData("spiderBoss:guardTwoAlive",1)

	--AiAgent(thugOne):stopWaiting()
	--AiAgent(thugOne):setWait(0)
	--AiAgent(thugOne):setNextPosition(-272, 28, -4851, 0)
	--AiAgent(thugOne):executeBehavior()
	

end

function nabooForceSurvivor:guardOneDead()
	writeData("spiderBoss:guardOneAlive",0)

	return 1
end

function nabooForceSurvivor:guardTwoDead()
	writeData("spiderBoss:guardTwoAlive",0)

	return 1
end
