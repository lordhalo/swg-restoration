local ObjectManager = require("managers.object.object_manager")

initiateQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "initiateQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("initiateQuest", true)

function initiateQuest:start()

end

--Setup
function initiateQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function initiateQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

initiateQuestLight_handler = Object:new {
	
 }

function initiateQuestLight_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuest.states.quest.phaseone, initiateQuest.questString)
	--local completedFS = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
	local completedFS = creature:hasScreenPlayState(32768, "ForceSensitive")
	local hasSkill = creature:hasSkill("fs_jedi_c_4")
	local hasSkillTwo = creature:hasSkill("fs_jedi_d_4")
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  

			if (completedFS == false) then
				nextConversationScreen = conversation:getScreen("initiate_not_ready")
			
			elseif ( completedQuest == false ) then
				if(hasSkill == true) and (hasSkillTwo == true) then
					nextConversationScreen = conversation:getScreen("initiate_first_screen")
				else
					nextConversationScreen = conversation:getScreen("lightsaber_screen")	
				end		

			elseif ( completedQuest == true ) then
				nextConversationScreen = conversation:getScreen("next_npc_screen")

			else
				nextConversationScreen = conversation:getScreen("hello_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function initiateQuestLight_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "initiate_complete_screen") then
		player:setScreenPlayState(initiateQuest.states.quest.phaseone, initiateQuest.questString)
	end
	
	return conversationScreen
end

initiateQuestLightTwo_handler = Object:new {
	
 }

function initiateQuestLightTwo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuest.states.quest.phaseone, initiateQuest.questString)
	local completedQuestTwo = creature:hasScreenPlayState(initiateQuest.states.quest.phasetwo, initiateQuest.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  
			if (completedQuestTwo == true) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			elseif (completedQuest == false) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			else
				nextConversationScreen = conversation:getScreen("first_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
	
end

function initiateQuestLightTwo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "final_screen") then
		if (conversingPlayer == nil) then
			return
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "color_crystals", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		player:setScreenPlayState(initiateQuest.states.quest.phasetwo, initiateQuest.questString)
	end
	

	return conversationScreen
end

initiateQuestLightThree_handler = Object:new {
	
 }

function initiateQuestLightThree_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuest.states.quest.phasetwo, initiateQuest.questString)
	local hasSkill = creature:hasSkill("fs_jedi_master")
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  
			if(hasSkill == true) then
				nextConversationScreen = conversation:getScreen("initiate_screen")
			elseif (completedQuest == false) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			else
				nextConversationScreen = conversation:getScreen("first_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
	
end

function initiateQuestLightThree_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "screen_3a") then
		player:setScreenPlayState(initiateQuest.states.quest.phasethree, initiateQuest.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Final Trial", "", -7050, 5396, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	

	return conversationScreen
end



