local ObjectManager = require("managers.object.object_manager")

deathWatchIntro = ScreenPlay:new {
	numberOfActs = 1,
	questString = "deathWatchIntro",
	states = {
		quest = { intro = 1, phaseone = 2, phasetwo = 4, phasethree = 8, phasefour = 16, phasefive = 32, phasesix = 64, phaseseven = 128, phaseeight = 256, phasenine = 512, phaseten = 1024}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	commanderGroup = {
		{ "black_sun_thug_quest", 0, -4762, 20, -7486,  1, 0 },
		{ "black_sun_thug_quest", 0, -4769, 20, -7486,  1, 0 },
		{ "black_sun_thug_quest", 0, -4759, 20, -7497,  1, 0 },
		{ "black_sun_thug_quest", 0, -4749, 20, -7499,  1, 0 },
		{ "black_sun_henchman_quest", 0, -4738, 20, -7486,  1, 0 }

	},

	baseGroup = {
		{ "black_sun_thug_quest", 0, -205, 6, 6163,  3, 0 },
		{ "black_sun_thug_quest", 0, -198, 6, 6163,  3, 0 },
		{ "black_sun_thug_quest", 0, -232, 6, 6176,  1, 0 },
		{ "black_sun_thug_quest", 0, -236, 7, 6176,  1, 0 },
		{ "black_sun_thug_quest", 0, -232, 7, 6179,  1, 0 },
		{ "black_sun_thug_quest", 0, -222, 7, 6129,  -160, 0 },
		{ "black_sun_thug_quest", 0, -224, 7, 6125,  -160, 0 },
		{ "black_sun_thug_quest", 0, -220, 7, 6125,  -160, 0 },
		{ "black_sun_thug_quest", 0, -183, 6, 6137,  120, 0 },
		{ "black_sun_thug_quest", 0, -188, 6, 6124,  112, 0 },
		{ "black_sun_thug_quest", 0, -260, 6, 6128,  -170, 0 },
		{ "black_sun_thug_quest", 0, -169, 6, 6081,  -170, 0 },
		{ "black_sun_thug_quest", 0, -153, 5, 6136,  180, 0 },
		{ "black_sun_henchman_quest", 0, -204, 6, 6138,  180, 0 },--commander here

	},

	baseWall = { -- saved but unused
		{ "object/static/structure/military/military_wall_weak_rebl_16_style_01.iff", -238, 7, 6125, 0, 1, 0, 0, 0 }
	}
}
registerScreenPlay("deathWatchIntro", true)

function deathWatchIntro:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
		self:spawnActiveAreas()
	end
end

function deathWatchIntro:spawnMobiles()
	
	spawnMobile("tatooine", "zeek", 0, 18.0, -0.9, 18.7,  172, 1028649)

end

function deathWatchIntro:spawnActiveAreas()

	local pSwoopLeader = spawnActiveArea("tatooine", "object/active_area.iff", 3307, 5, -5001, 32, 0)
	if pSwoopLeader ~= nil then
		createObserver(ENTEREDAREA, "deathWatchIntro", "enterThugArea", pSwoopLeader)
	end

	local pCommander = spawnActiveArea("tatooine", "object/active_area.iff", -4762, 20, -7486, 32, 0)
	if pCommander ~= nil then
		createObserver(ENTEREDAREA, "deathWatchIntro", "enterCommanderArea", pCommander)
	end

	local pBase = spawnActiveArea("tatooine", "object/active_area.iff", -202, 6, 6137, 62, 0)
	if pBase ~= nil then
		createObserver(ENTEREDAREA, "deathWatchIntro", "enterBaseArea", pBase)
	end

	local pBioScann = spawnActiveArea("tatooine", "object/active_area.iff", -235, 7, 6125, 10, 0)
	if pBioScann ~= nil then
		createObserver(ENTEREDAREA, "deathWatchIntro", "enterBioScan", pBioScann)
	end

	local pZeek = spawnActiveArea("tatooine", "object/active_area.iff", 3537, 5, -4987, 5, 0)
	if pZeek ~= nil then
		createObserver(ENTEREDAREA, "deathWatchIntro", "enterZeekArea", pZeek)
	end
end

function deathWatchIntro:spawnGang()
	local pLeader = spawnMobile("tatooine", "eisley_gang_leader", 0, 3307, 5, -5001,  77, 0)
	createObserver(OBJECTDESTRUCTION, "deathWatchIntro", "firstQuestDead", pLeader)
	spawnMobile("tatooine", "eisley_gang", 0, 3313, 5, -5003,  -61, 0)
	spawnMobile("tatooine", "eisley_gang", 0, 3311, 5, -4998,  -162, 0)
	spawnMobile("tatooine", "eisley_gang", 0, 3306, 5, -4999,  123, 0)
	spawnMobile("tatooine", "eisley_gang", 0, 3305, 5, -5001,  -136, 0)


end

function deathWatchIntro:spawnCommander(pMovingObject)

		local mobileTable = self.commanderGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("tatooine", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			CreatureObject(pMobile):engageCombat(pMovingObject)
			createObserver(OBJECTDESTRUCTION, "deathWatchIntro", "completeCommander", pMobile)
		end
end

function deathWatchIntro:completeCommander(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)

	writeData("commanderGroup:npcsDead",readData("commanderGroup:npcsDead") + 1)

	if(readData("commanderGroup:npcsDead") == 5) then
		--player:setScreenPlayState(32, deathWatchIntro.questString)
		--local pGhost = player:getPlayerObject()
		--PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
		--CreatureObject(pKiller):sendSystemMessage("Return to Jekk")
		writeData("commanderGroup:spawn",0)
		writeData("commanderGroup:npcsDead",0)
	--end
	--return 0

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = creature:hasScreenPlayState(16, "deathWatchIntro")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							local pGhost = groupMember:getPlayerObject()
							groupMember:sendSystemMessage("Return to Jekk")
							PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
							groupMember:setScreenPlayState(32, deathWatchIntro.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(16, "deathWatchIntro")
					
					if(hasState == true) then 
						local pGhost = player:getPlayerObject()
						player:sendSystemMessage("Return to Jekk")
						PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
						player:setScreenPlayState(32, deathWatchIntro.questString)
				end
			end
		end)
	
	end

	return 1
end

function deathWatchIntro:spawnBaseGroup(pMovingObject)

		local mobileTable = self.baseGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("tatooine", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "deathWatchIntro", "completeBaseGroup", pMobile)
		end
end

function deathWatchIntro:completeBaseGroup(pMobile, pKiller)
	local player = LuaCreatureObject(pKiller)

	writeData("baseGroup:npcsDead",readData("baseGroup:npcsDead") + 1)

	if(readData("baseGroup:npcsDead") == 14) then

		writeData("baseGroup:spawn",2)
		--writeData("swoopGang:npcsDead",0)
	end
	return 0
end

function deathWatchIntro:enterThugArea(pActiveArea, pMovingObject)
	
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		if (readData("swoopGang:spawn") == 0)  then
			self:spawnGang()
			writeData("swoopGang:spawn",1)
		end


		return 0
	end)
end

function deathWatchIntro:enterBioScan(pActiveArea, pMovingObject)
	
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		local hasState = player:hasScreenPlayState(64, "deathWatchIntro")
		local hasComplete = player:hasScreenPlayState(128, "deathWatchIntro")

		if (readData("baseGroup:spawn") == 2 and readData("darkJedi:spawn") == 0 and hasState == true and hasComplete == false)  then
			CreatureObject(pMovingObject):sendSystemMessage("Bioscan complete, Turret disengages.")
			self:spawnDarkJedi(pMovingObject)
			writeData("darkJedi:spawn",1)
		end


		return 0
	end)
end

function deathWatchIntro:enterZeekArea(pActiveArea, pMovingObject)
	
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

		local hasState = player:hasScreenPlayState(256, "deathWatchIntro")
		local hasComplete = player:hasScreenPlayState(512, "deathWatchIntro")

		if (readData("zeek:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnZeek(pMovingObject)
			writeData("zeek:spawn",1)
		end


		return 0
	end)
end

function deathWatchIntro:spawnDarkJedi(pMovingObject)
	local dJedi = spawnMobile("tatooine", "black_sun_assassin_quest", 0, -234, 7, 6123,  0, 0)
	spatialChat(dJedi, "Your trick may work on droids but I know who you are!")
	createObserver(OBJECTDESTRUCTION, "deathWatchIntro", "deadDarkJedi", dJedi)

end

function deathWatchIntro:spawnZeek(pMovingObject)
	local pZeek = spawnMobile("tatooine", "zeek_combat", 0, 3537, 5, -4987,  0, 0)
	spatialChat(pZeek, "Your momentary death will be the same result for anyone else that gets in my way!")
	createObserver(DAMAGERECEIVED, "deathWatchIntro", "npcDamageObserver", pZeek)

end

function deathWatchIntro:npcDamageObserver(pZeek, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(pZeek)
	local creo = LuaSceneObject(pZeek)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	local hasState = player:hasScreenPlayState(256, "deathWatchIntro")

	if ((health <= (maxHealth * 0.1)) or (action <= (maxAction * 0.1)) or (mind <= (maxMind * 0.1))) then
		spatialChat(pZeek, "You got lucky, the Empire will continue our work! Be on guard,  I won't forget this!")
		--if(hasState == true) then
		--	CreatureObject(playerObject):sendSystemMessage("Return to Jekk")
		--	local pGhost = player:getPlayerObject()
		----	PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
			player:setScreenPlayState(512, deathWatchIntro.questString)
		--end
		writeData("zeek:spawn",0)
		creo:switchZone("character_farm", -200, 0, -200, 0)
		creo:destroyObjectFromWorld()

			ObjectManager.withCreatureObject(playerObject, function(creature)
			-- screenplaystates for login/logout
			if (creature:isGrouped()) then
				local groupSize = creature:getGroupSize()

				for i = 0, groupSize - 1, 1 do
					local pMember = creature:getGroupMember(i)
					if pMember ~= nil then
					
						local groupMember = LuaCreatureObject(pMember)
						local hasState = groupMember:hasScreenPlayState(256, "deathWatchIntro")
						local wasInRange = false

							if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
								wasInRange = true
							end

							if(hasState == true and wasInRange == true) then
								local pGhost = groupMember:getPlayerObject()
								groupMember:sendSystemMessage("Zeek flees the fight, Return to Jekk")
								PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
								groupMember:setScreenPlayState(512, deathWatchIntro.questString)
							end
						end
					end
				else
					local hasState = creature:hasScreenPlayState(256, "deathWatchIntro")
					
						if(hasState == true) then 
							local pGhost = player:getPlayerObject()
							player:sendSystemMessage("Zeek flees the fight, Return to Jekk")
							PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
							player:setScreenPlayState(512, deathWatchIntro.questString)
					end
				end
			end)
	end

	return 0

end

function deathWatchIntro:deadDarkJedi(pJedi, playerObject)
	local player = LuaCreatureObject(playerObject)
	--CreatureObject(playerObject):sendSystemMessage("With the Dark Assassin dead, you destroy the generator. Return to Jekk")
	--local pGhost = player:getPlayerObject()
	--PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
	--player:setScreenPlayState(128, deathWatchIntro.questString)
	writeData("baseGroup:spawn",0)
	writeData("darkJedi:spawn",0)
	--return 0

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(64, "deathWatchIntro")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							local pGhost = groupMember:getPlayerObject()
							CreatureObject(playerObject):sendSystemMessage("With the Dark Assassin dead, you destroy the generator. Return to Jekk")
							PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
							groupMember:setScreenPlayState(128, deathWatchIntro.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(64, "deathWatchIntro")
					
					if(hasState == true) then 
						local pGhost = player:getPlayerObject()
						CreatureObject(playerObject):sendSystemMessage("With the Dark Assassin dead, you destroy the generator. Return to Jekk")
						PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
						player:setScreenPlayState(128, deathWatchIntro.questString)
				end
			end
		end)

	return 1
end

function deathWatchIntro:enterCommanderArea(pActiveArea, pMovingObject)
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end

	local player = LuaCreatureObject(pMovingObject)
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		local hasState = player:hasScreenPlayState(16, "deathWatchIntro")
		local hasComplete = player:hasScreenPlayState(32, "deathWatchIntro")

		if (readData("commanderGroup:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnCommander(pMovingObject)
			writeData("commanderGroup:spawn",1)
		end


		return 0
	end)
end

function deathWatchIntro:enterBaseArea(pActiveArea, pMovingObject)
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end

	local player = LuaCreatureObject(pMovingObject)
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		local hasState = player:hasScreenPlayState(64, "deathWatchIntro")
		local hasComplete = player:hasScreenPlayState(128, "deathWatchIntro")

		if (readData("baseGroup:spawn") == 0 and hasState == true and hasComplete == false)  then
			self:spawnBaseGroup(pMovingObject)
			writeData("baseGroup:spawn",1)
		end


		return 0
	end)
end


function deathWatchIntro:firstQuestDead(pCrystal, playerObject)
	local player = LuaCreatureObject(playerObject)
	--local hasQuest = player:hasScreenPlayState(1, "deathWatchIntro")

	--if(hasQuest == true) then
	--	player:setScreenPlayState(2, deathWatchIntro.questString)
	--	local pGhost = player:getPlayerObject()
	--	PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
	--	CreatureObject(playerObject):sendSystemMessage("Return to Jekk")
	--	writeData("swoopGang:spawn",0)
	--end

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(1, "deathWatchIntro")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							local pGhost = groupMember:getPlayerObject()
							PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
							groupMember:sendSystemMessage("Return to Jekk")
							groupMember:setScreenPlayState(2, deathWatchIntro.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(1, "deathWatchIntro")
					
					if(hasState == true) then 
						local pGhost = player:getPlayerObject()
						PlayerObject(pGhost):addWaypoint("tatooine", "Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)
						player:sendSystemMessage("Return to Jekk")
						player:setScreenPlayState(2, deathWatchIntro.questString)
				end
			end
		end)

	return 1
end

function deathWatchIntro:spawnSceneObjects()
	spawnSceneObject("tatooine", "object/mobile/vehicle/speederbike_swoop.iff", 3307, 5, -5008, 0, 1, 0, 0, 0)	
	spawnSceneObject("tatooine", "object/static/item/item_container_inorganic_chemicals.iff", 3308, 5, -4996, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_tato_tent_s01.iff", -4765, 22.5, -7521, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_powerdroid_powerdown.iff", -4759, 22, -7515, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_tato_farm_64x64_s01.iff", -202, 6.4, 6137, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_tato_tent_s01.iff", -153, 6, 6142, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_tato_tent_s01.iff", -169, 6, 6087, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_tato_tent_s01.iff", -261, 7, 6133, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/military/bunker_crate_style_01.iff", -220, 7, 6143, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/structure/general/poi_all_corral_half_32x32_s05.iff", -238, 7, 6115, 0, 1, 0, 0, 0)
	--spawnSceneObject("tatooine", "object/static/structure/military/military_wall_weak_rebl_16_style_01.iff", -238, 7, 6125, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/installation/faction_perk/turret/tower_lg.iff", -240, 7, 6127, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/static/installation/mockup_power_generator_wind_style_1.iff", -237, 8, 6110, 0, 1, 0, 0, 0)

end

--Setup
function deathWatchIntro:getActivePlayerName()
	return self.questdata.activePlayerName
end

function deathWatchIntro:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

deathWatchIntroConvo_handler = Object:new {
	
 }

function deathWatchIntroConvo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local creature = LuaCreatureObject(conversingPlayer)
			local hasSkill = creature:hasSkill("fs_jedi_novice")
			local hasTrooper = creature:hasSkill("imp_rank_novice")
			local hasRebel = creature:hasSkill("reb_rank_novice")
			local hasBlackSun = creature:hasSkill("black_sun_novice")
			local firstQuestStarted = creature:hasScreenPlayState(1, "deathWatchIntro")
			local firstQuestComplete = creature:hasScreenPlayState(2, "deathWatchIntro")
			local secondQuestComplete = creature:hasScreenPlayState(8, "deathWatchIntro")
			local secondQuestStarted = creature:hasScreenPlayState(4, "deathWatchIntro")
			local thirdQuestComplete = creature:hasScreenPlayState(32, "deathWatchIntro")
			local thirdQuestStarted = creature:hasScreenPlayState(16, "deathWatchIntro")
			local forthQuestComplete = creature:hasScreenPlayState(128, "deathWatchIntro")
			local forthQuestStarted = creature:hasScreenPlayState(64, "deathWatchIntro")
			local fifthQuestComplete = creature:hasScreenPlayState(512, "deathWatchIntro")
			local fifthQuestStarted = creature:hasScreenPlayState(256, "deathWatchIntro")
			local questChainComplete = creature:hasScreenPlayState(1024, "deathWatchIntro")


				--### KEY CONVO PARTS, AND STATES ###--
				if (hasSkill == true or hasTrooper == true or hasRebel == true or hasBlackSun) then
					nextConversationScreen = conversation:getScreen("jedi_screen")
				elseif (questChainComplete == true) then
					nextConversationScreen = conversation:getScreen("chain_complete_screen")	
				elseif (fifthQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("fifth_quest_complete_screen")
				elseif (fifthQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("fifth_quest_waiting")
				elseif (forthQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("fourth_quest_complete_screen")
				elseif (forthQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("fourth_quest_waiting")
				elseif (thirdQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("third_quest_complete_screen")
				elseif (thirdQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("third_quest_waiting")
				elseif (secondQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("second_quest_complete_screen")
				elseif (secondQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("second_quest_waiting")
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("first_quest_complete_screen")
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("accepted_screen")
				else
					nextConversationScreen = conversation:getScreen("first_screen")
				end				
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function deathWatchIntroConvo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "seventh_screen") then
		player:setScreenPlayState(1, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Swoop Gang", "", 3307, -5001, true, true, WAYPOINTTHEMEPARK, 1)	
			PlayerObject(pGhost):addWaypoint("tatooine", "Swoop Gang", "", 3307, -5001, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end

	if (screenID == "second_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("death_watch", 10)
		end)

		player:setScreenPlayState(4, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", -1381, -3656, true, true, WAYPOINTTHEMEPARK, 1)
			PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", -1381, -3656, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "third_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("death_watch", 10)
		end)

		player:setScreenPlayState(16, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", -1381, -3656, true, true, WAYPOINTTHEMEPARK, 1)
			PlayerObject(pGhost):addWaypoint("tatooine", "Commander", "", -4762, -7486, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "fourth_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("death_watch", 10)
		end)

		player:setScreenPlayState(64, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("tatooine", "Black Sun Base", "", -202, 6137, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "fifth_quest_begin") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("death_watch", 10)
		end)

		player:setScreenPlayState(256, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("tatooine", "Zeek", "", 3537, -4986, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end

	if (screenID == "fifth_quest_outro") then
		ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		playerObject:increaseFactionStanding("death_watch", 50)
		end)

		player:setScreenPlayState(1024, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("endor", "Death Watch Bunker", "", -4674, 4332, 5, true, true, WAYPOINTTHEMEPARK, 1)		
		end
	end
	
	return conversationScreen
end


zeekFirstConvo_handler = Object:new {
	
 }

function zeekFirstConvo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)
			local firstQuestComplete = creature:hasScreenPlayState(4, "deathWatchIntro")
			local jeekComplete = creature:hasScreenPlayState(8, "deathWatchIntro")

				if (jeekComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("first_screen")
				else
					nextConversationScreen = conversation:getScreen("goaway_screen")
				end				
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function zeekFirstConvo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "final_screen") then
		player:setScreenPlayState(8, deathWatchIntro.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			--PlayerObject(pGhost):addWaypoint("tatooine", "Return to Jekk", "", 3307, -5001, true, true, WAYPOINTTHEMEPARK, 1)
			PlayerObject(pGhost):addWaypoint("tatooine", "Return to Jekk", "", 1695, 3022, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	return conversationScreen
end
