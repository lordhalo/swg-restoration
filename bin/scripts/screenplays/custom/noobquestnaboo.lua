noobquestnaboo = ScreenPlay:new {
	numberOfActs = 1,
	questString = "noobquestnaboo",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("noobquestnaboo", true)

function noobquestnaboo:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end


--Mobile Spawning
function noobquestnaboo:spawnMobiles()
	spawnMobile("naboo", "bright_pals", 1, 2001, 12.9, 2496,  -117, 0)
	--spawnMobile("tatooine", "secuirty_recruiter", 1, -2907, 5.0, 2132,  40, 0)
	--spawnMobile("dathomir", "fs_follower", 1, 5304.2, 78.5, -4187.4,  -60, 0)


end

--Setup
function noobquestnaboo:getActivePlayerName()
	return self.questdata.activePlayerName
end

function noobquestnaboo:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

naboo_noob_convo_handler = Object:new {
	
 }

function naboo_noob_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local acceptedTatooine = creature:hasScreenPlayState(noobquest.states.quest.phaseone, noobquest.questString)
	local acceptedNaboo = creature:hasScreenPlayState(noobquestnaboo.states.quest.phaseone, noobquestnaboo.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			if ( acceptedNaboo == 1 ) then
				nextConversationScreen = conversation:getScreen("naboo_noob_accepted_screen")--First convo screen to pull.

			else
				nextConversationScreen = conversation:getScreen("naboo_noob_screen")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function naboo_noob_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "naboo_noob_screen") then
		player:setScreenPlayState(noobquest.states.quest.phaseone, noobquest.questString)
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

select_job_path_naboo_convo_handler = Object:new {
	
 }

function select_job_path_naboo_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	--local isJedi = creature:hasScreenPlayState(force_sensitive.states.quest.jedi, force_sensitive.questString)
	--local isGrey = creature:hasScreenPlayState(force_sensitive.states.quest.grey, force_sensitive.questString)
	--local isDark = creature:hasScreenPlayState(force_sensitive.states.quest.dark, force_sensitive.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			--local hasAccepted = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
			--print("hasAccepted() is " .. hasAccepted)   
			
			--if ( isDark == 1) then
			--	nextConversationScreen = conversation:getScreen("dark_path_screen")--First convo screen to pull.
			--else
				nextConversationScreen = conversation:getScreen("job_screen")--End of the road.	
			--end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function select_job_path_naboo_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	--if (screenID == "defFinal") then
	--	player:setScreenPlayState(force_sensitive.states.quest.defense, force_sensitive.questString)
	--end
	
	
	--print("returning convosvreen")
	return conversationScreen
end
