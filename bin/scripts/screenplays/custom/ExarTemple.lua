local ObjectManager = require("managers.object.object_manager")

ExarTemple = ScreenPlay:new {
	numberOfActs = 1,
}
registerScreenPlay("ExarTemple", true)

function ExarTemple:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
	end
end

function ExarTemple:spawnSceneObjects()
	spawnSceneObject("yavin4", "object/static/particle/particle_magic_sparks.iff", 4827, 103, 5631, 0, 1, 0, -60, -60)
	spawnSceneObject("yavin4", "object/static/structure/dantooine/dant_large_communal_dest.iff", 4827, 102, 5631, 0, 1, 0, -60, -60)
end

function ExarTemple:spawnMobiles()
	--local pNS = spawnMobile("dathomir", "nightsister_initiate_quest", 300, 3511, 24, 1552,  -60, 0)
	--createObserver(DAMAGERECEIVED, "MistyFalls", "npcDamageObserver", pNS)
	local pSane = spawnMobile("yavin4", "junk_scavenger_sane", 300, 4786, 112, 5606,  -60, 0)
	local scavenger = spawnMobile("yavin4", "junk_scavenger", 300, 4838, 105, 5652,  -60, 0)
	local scavengerTwo = spawnMobile("yavin4", "junk_scavenger", 120, 4827, 102, 5602,  -60, 0)
	local scavengerThree = spawnMobile("yavin4", "junk_scavenger", 120, 4847, 104, 5620,  -60, 0)
	local scavengerFour = spawnMobile("yavin4", "junk_scavenger", 120, 4802, 121, 5649,  -60, 0)
	spawnMobile("yavin4", "junk_scavenger", 120, 4800, 107, 5557,  -60, 0)
	spawnMobile("yavin4", "junk_scavenger", 120, 4850, 102, 5551,  -60, 0)
	createObserver(DAMAGERECEIVED, "ExarTemple", "npcDamageObserver", scavenger)
	createObserver(OBJECTDESTRUCTION, "ExarTemple", "deadScavenger", scavenger)
	createObserver(OBJECTDESTRUCTION, "ExarTemple", "deadScavenger", scavengerTwo)
	createObserver(OBJECTDESTRUCTION, "ExarTemple", "deadScavenger", scavengerThree)
	createObserver(OBJECTDESTRUCTION, "ExarTemple", "deadScavenger", scavengerFour)
	--AiAgent(scavenger):setAiTemplate("idlewander")
end

function ExarTemple:deadScavenger(scavenger, pKiller)
	local player = LuaCreatureObject(pKiller)
	
	if(math.random(5) > 2) then
		if (player:hasScreenPlayState(4, "padawanQuest") or player:hasScreenPlayState(4, "padawanQuestDark")) then
			local pInventory = player:getSlottedObject("inventory")
			giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_06.iff", -1)
		end
	end

	 return 0
end

function ExarTemple:npcDamageObserver(scavenger, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(scavenger)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.99)) or (action <= (maxAction * 0.99)) or (mind <= (maxMind * 0.99))) and readData("exar:scavengerdamage") == 0) then
		spatialChat(scavenger, "I must dig")
		writeData("exar:scavengerdamage",1)
	elseif (((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) and readData("exar:scavengerdamage2") == 0) then
		spatialChat(scavenger, "You must have taken the Holocron!")
		writeData("exar:scavengerdamage2",1)
	elseif ((health <= (maxHealth * 0.01)) or (action <= (maxAction * 0.01)) or (mind <= (maxMind * 0.01))) then
		spatialChat(scavenger, "...He will be reborn")
		writeData("exar:scavengerdamage",0)
		writeData("exar:scavengerdamage2",0)
		writeData("exar:scavengerdamage3",0)
	end

	return 0
end

scavenger_convo_handler = Object:new {
	
 }

function scavenger_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			nextConversationScreen = conversation:getScreen("first_screen")--End of the road.
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function scavenger_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	

	return conversationScreen
end
