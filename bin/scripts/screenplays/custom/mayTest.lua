mayTest = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "mayTest",
}
registerScreenPlay("mayTest", true)

function mayTest:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end

--Mobile Spawning
function mayTest:spawnMobiles()
	
	--Trainer
	--TODO:CHANGE LOCATION OF TRAINERS
	--Initiate
	spawnMobile("naboo", "fs_jedi", 1, 5388, 78.5, -4154, 0, 0)

	--Lightsabers
	spawnMobile("naboo", "lightsaber_master", 1, 1955, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "lightsaber_novice", 1, 1960, 12.0, 2336, 0, 0)

	--Healing
	spawnMobile("naboo", "healing_master", 1, 1965, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "healing_novice", 1, 1970, 12.0, 2336, 0, 0)

	--Defender
	spawnMobile("naboo", "defender_master", 1, 1975, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "defender_novice", 1, 1980, 12.0, 2336, 0, 0)

	--Powers
	spawnMobile("naboo", "forcemaster_master", 1, 1985, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "force_expert",       1, 1990, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "force_power_expert", 1, 1995, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "force_power_master", 1, 2000, 12.0, 2336, 0, 0)

	spawnMobile("naboo", "rank_grey_trainer", 1, 2002, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "rank_dark_trainer", 1, 2004, 12.0, 2336, 0, 0)
	spawnMobile("naboo", "rank_light_trainer", 1, 2006, 12.0, 2336, 0, 0)

	spawnMobile("naboo", "sith_shadow_mercenary", 1, 1895, 12, 2333.1, 0, 0)
	spawnMobile("naboo", "sith_shadow_mercenary", 1, 1898, 12, 2333.1, 0, 0)
	spawnMobile("naboo", "sith_shadow_mercenary", 1, 1895, 12, 2337.1, 0, 0)

	spawnMobile("naboo", "rebel_recruiter", 1, 1972, 12, 2373.1, 0, 0)
	spawnMobile("naboo", "imperial_recruiter", 1, 1975, 12, 2373.1, 0, 0)

	spawnMobile("naboo", "enraged_bull_rancor", 120, 2054, 10.4, 2156.1, 0, 0)
	spawnMobile("naboo", "mutant_rancor", 120, 1904, 7.9, 2137.1, 0, 0)

	spawnMobile("naboo", "bol", 30, 1969, 9.7, 2091, 0, 0)
	spawnMobile("naboo", "bol", 30, 1994, 9.3, 2077, 0, 0)
	spawnMobile("naboo", "bol", 30, 1986, 10.6, 2063, 0, 0)
--sith_shadow_mercenary" Change back to old loot
end

--Objects
function mayTest:spawnSceneObjects()

	--spawnSceneObject("naboo", "object/tangible/jedi/force_shrine_stone.iff", 1945, 12, 2365.1, 0, 0, 0, 0, 0)
	spawnSceneObject("naboo", "object/tangible/jedi/power_shrine_red.iff", 1985, 12, 2365.1, 0, 0, 0, 0, 0)
	spawnSceneObject("naboo", "object/tangible/camp/campfire_logs_fresh.iff", 1985, 12, 2375.1, 0, 0, 0, 0, 0)
	spawnSceneObject("naboo", "object/tangible/terminal/terminal_character_builder.iff", 1915, 12, 2375.1, 0, 0, 0, 0, 0)
end
