TatooineOldtrackScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

}

registerScreenPlay("TatooineOldtrackScreenPlay", true)

function TatooineOldtrackScreenPlay:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnActiveAreas()
	end
end

function TatooineOldtrackScreenPlay:spawnActiveAreas()

	local pAmbush = spawnSceneObject("tatooine", "object/active_area.iff", -348.2, 0, 4056, 0, 0, 0, 0, 0)
	
	if (pAmbush ~= nil) then
		local activeArea = LuaActiveArea(pAmbush)
		activeArea:setRadius(25)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterTuskenAmbush", pAmbush)
	end

	local pAmbush2 = spawnSceneObject("tatooine", "object/active_area.iff", -170.2, 0, 3966, 0, 0, 0, 0, 0)
	
	if (pAmbush2 ~= nil) then
		local activeArea = LuaActiveArea(pAmbush2)
		activeArea:setRadius(25)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterTuskenAmbush2", pAmbush2)
	end

	local pBantha = spawnSceneObject("tatooine", "object/active_area.iff", -238, 0, 3851, 0, 0, 0, 0, 0)
	
	if (pBantha ~= nil) then
		local activeArea = LuaActiveArea(pBantha)
		activeArea:setRadius(25)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterBantha", pBantha)
	end

	local pAmbush3 = spawnSceneObject("tatooine", "object/active_area.iff", -298, 0, 3929, 0, 0, 0, 0, 0)
	
	if (pAmbush3 ~= nil) then
		local activeArea = LuaActiveArea(pAmbush3)
		activeArea:setRadius(50)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterTuskenAmbush3", pAmbush3)
	end

	local pAmbush4 = spawnSceneObject("tatooine", "object/active_area.iff", -548, 0, 3859, 0, 0, 0, 0, 0) -- warlord or krayt
	
	if (pAmbush4 ~= nil) then
		local activeArea = LuaActiveArea(pAmbush4)
		activeArea:setRadius(50)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterTuskenAmbush4", pAmbush4)
	end

	spawnSceneObject("tatooine", "object/tangible/camp/camp_tent_s1.iff", -983.2, 1, 4249.8, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/tangible/camp/camp_stool_short.iff", -987.2, 1, 4249.8, 0, 1, 0, 0, 0)
	spawnSceneObject("tatooine", "object/tangible/camp/campfire_logs_fresh.iff", -991.2, 1, 4249.8, 0, 1, 0, 0, 0)

	local pBattle = spawnSceneObject("tatooine", "object/active_area.iff", 69, 0, 3686, 0, 0, 0, 0, 0) -- Jawa or Tusken
	
	if (pBattle ~= nil) then
		local activeArea = LuaActiveArea(pBattle)
		activeArea:setRadius(50)
		createObserver(ENTEREDAREA, "TatooineOldtrackScreenPlay", "enterBattle", pBattle)
	end

	spawnSceneObject("tatooine", "object/tangible/camp/camp_tent_s6.iff", -306.2, 0, 4175.8, 0, 1, 0, 0, 0)


end

function TatooineOldtrackScreenPlay:enterTuskenAmbush(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("tusken:hesalive") == 0) then
		
			spawnMobile("tatooine", "tusken_warrior", 0, -342, 0.0, 4062, -148, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -336, 0.0, 4065, -144, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -345, 0.0, 4048, -14, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -327, 0.0, 4064, -54, 0)
			
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(30) + 3727, 5.0,  math.random(30) + -4808, -92, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(30) + 3727, 5.0,  math.random(30) + -4808, -92, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(30) + 3727, 5.0,  math.random(30) + -4808, -92, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(30) + 3727, 5.0,  math.random(30) + -4808, -92, 0)
			local pWarlord = spawnMobile("tatooine", "tusken_warlord", 0, math.random(20) + 3727, 5.0,  math.random(20) + -4808, -92, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead", pWarlord)
			writeData("tusken:hesalive",1)
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead(pKray, pKiller)
	writeData("tusken:hesalive",0)
     return 1
end

function TatooineOldtrackScreenPlay:enterTuskenAmbush2(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("tusken:hesalive2") == 0) then
		
			spawnMobile("tatooine", "tusken_warrior", 0, -101, 0.0, 3732, -122, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -109, 0.0, 3711, -8, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -129, 0.0, 3718, 72, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -123, 0.0, 3746, 113, 0)
			
			local pSniper = spawnMobile("tatooine", "tusken_sniper", 0, -172, 0.0, 3459, -23, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead", pSniper)
			writeData("tusken:hesalive2",1)
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead2(pSniper, pKiller)
	writeData("tusken:hesalive2",0)
     return 1
end

function TatooineOldtrackScreenPlay:enterBantha(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("tusken:hesalive3") == 0) then
		
			spawnMobile("tatooine", "dune_bantha", 0, -314, 0.0, 3739, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -324, 0.0, 3739, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -341, 0.0, 3739, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -304, 0.0, 3739, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -314, 0.0, 3719, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -324, 0.0, 3729, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -354, 0.0, 3709, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -324, 0.0, 3711, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -301, 0.0, 3742, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -344, 0.0, 3719, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -314, 0.0, 3749, math.random(100), 0)
			spawnMobile("tatooine", "dune_bantha", 0, -304, 0.0, 3711, math.random(100), 0)
			
			local pBantha = spawnMobile("tatooine", "bonecracker_bantha", 0, math.random(20) + -304, 0.0, math.random(20) + 3711, math.random(100), 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead3", pBantha)
			writeData("tusken:hesalive3",1)
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead3(pBantha, pKiller)
	writeData("tusken:hesalive3",0)
     return 1
end

function TatooineOldtrackScreenPlay:enterTuskenAmbush3(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
		 if (readData("tusken:hesalive4") == 0) then
		
			spawnMobile("tatooine", "tusken_warrior", 0, -238, 0.0, 3851, 28, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -247, 0.0, 3860, 40, 0)
			spawnMobile("tatooine", "dune_bantha", 0, -238, 0.0, 3881, -28, 0)
			spawnMobile("tatooine", "dune_bantha", 0, -238, 0.0, 3881, -28, 0)

			local pBantha2 = spawnMobile("tatooine", "dune_bantha", 0, math.random(25) + 238, 5.0,  math.random(25) + -3881, -92, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead4", pBantha2)
			writeData("tusken:hesalive4",1)
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead4(pBantha2, pKiller)
	writeData("tusken:hesalive4",0)
     return 1
end

function TatooineOldtrackScreenPlay:enterTuskenAmbush4(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
			num = math.random(1, 100)
		 if (readData("boss1:hesalive") == 0) then
			if num < 50 then
			spawnMobile("tatooine", "tusken_warrior", 0, -985, 2.0, 4252, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -997, 2.0, 4252, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -991, 2.0, 4272, 0, 0)

			local pWarlord1 = spawnMobile("tatooine", "tusken_warlord", 0, -991, 2.0, 4252, 0, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead5", pWarlord1)
			writeData("boss1:hesalive",1)
			end
			if num >= 50 then
			local pKray1 = spawnMobile("tatooine", "krayt_dragon_ancient", 0, 466, -130, 5497, -100, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead6", pKray1)
			writeData("boss1:hesalive",1)
			end
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead5(pWarlord1, pKiller)
	writeData("boss1:hesalive",0)
     return 1
end

function TatooineOldtrackScreenPlay:heDead6(pKray1, pKiller)
	writeData("boss1:hesalive",0)
     return 1
end

function TatooineOldtrackScreenPlay:enterBattle(pActiveArea, pMovingObject)
	local movingObject = LuaSceneObject(pMovingObject)
	
	if (movingObject:isCreatureObject()) then
			num = math.random(1, 100)
		 if (readData("tusken:hesalive6") == 0) then
			if num < 50 then
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4132, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4136, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4140, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4144, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4148, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4152, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, math.random(20) + -317, 0, 4156, 0, 0)

			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4132, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4136, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4140, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4144, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4148, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4152, 0, 0)
			spawnMobile("tatooine", "jawa_avenger", 0, -313, 0, math.random(15) + 4156, 0, 0)

			local pWarlord2 = spawnMobile("tatooine", "jawa_warlord", -290, 0, math.random(10) + 4156, 0, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead6", pWarlord2)
			writeData("tusken:hesalive6",1)
			end
			if num >= 52 then
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4132, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4136, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4140, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4144, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4148, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 2.0, 4152, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, math.random(20) + -317, 0, 4156, 0, 0)

			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4132, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4136, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4140, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4144, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4148, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4152, 0, 0)
			spawnMobile("tatooine", "tusken_warrior", 0, -313, 0, math.random(15) + 4156, 0, 0)
			local pWarlord3 = spawnMobile("tatooine", "tusken_warlord", -290, 0, math.random(10) + 4156, 0, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead6", pWarlord3)
			writeData("tusken:hesalive6",1)
			end
			if num == 51 then
			local pCanyon = spawnMobile("tatooine", "canyon_krayt_dragon", -300, 0, math.random(10) + 4156, 0, 0)
			createObserver(OBJECTDESTRUCTION, "TatooineOldtrackScreenPlay", "heDead6", pCanyon)
			writeData("tusken:hesalive6",1)
			end
		end
	end
	
	return 0
end

function TatooineOldtrackScreenPlay:heDead7(pWarlord2, pKiller)
	writeData("tusken:hesalive6",0)
     return 1
end

function TatooineOldtrackScreenPlay:heDead8(pWarlord3, pKiller)
	writeData("tusken:hesalive6",0)
     return 1
end

function TatooineOldtrackScreenPlay:heDead9(pCanyon, pKiller)
	writeData("tusken:hesalive6",0)
     return 1
end
