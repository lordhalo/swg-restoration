local ObjectManager = require("managers.object.object_manager")

eisleyGang = ScreenPlay:new {
	numberOfActs = 1,
	questString = "eisleyGang",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("eisleyGang", true)

function eisleyGang:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
	end
end


function eisleyGang:spawnMobiles()
	spawnMobile("tatooine", "eisley_gang_leader", 1, 3307, 5, -5001,  77, 0)
	spawnMobile("naboo", "male_twilek_thug_quest", 1, -5244, 6, 4243,  -50, 0)
	spawnMobile("naboo", "male_zabrak_thug_quest", 1, -5242, 6, 4243,  -50, 0)
	local pBoss = spawnMobile("naboo", "male_rodian_thug_quest", 1, -5243, 6, 4242,  -50, 0)
	createObserver(OBJECTDESTRUCTION, "eisleyGang", "bossDead", pBoss)
end

--Setup

function eisleyGang:bossDead(pBoss, pPlayer)
	local player = LuaCreatureObject(pPlayer)
	local  hasComplete = player:hasScreenPlayState(eisleyGang.states.quest.phaseone, eisleyGang.questString)
	if (hasComplete == true) then
		player:setScreenPlayState(eisleyGang.states.quest.phasetwo, eisleyGang.questString)
	end
	return 1
end
function eisleyGang:getActivePlayerName()
	return self.questdata.activePlayerName
end

function eisleyGang:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end


theed_smuggler_convo_handler = Object:new {
	
 }

function theed_smuggler_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)

			local hasAccepted = creature:hasScreenPlayState(4, "padawanQuest")
			
			local  hasAcceptedTwo = creature:hasScreenPlayState(4, "eisleyGang")
			
			local  hasComplete = creature:hasScreenPlayState(eisleyGang.states.quest.phasetwo, eisleyGang.questString)
			
			local  hasCompleteTwo = creature:hasScreenPlayState(eisleyGang.states.quest.phasethree, eisleyGang.questString)
			
			local  hasCompleteThree = creature:hasScreenPlayState(eisleyGang.states.quest.phasefour, eisleyGang.questString)
			if ( hasCompleteTwo == true ) then
				nextConversationScreen = conversation:getScreen("complete_screen")--First convo screen to pull.
			elseif ( hasCompleteThree == true ) then
				nextConversationScreen = conversation:getScreen("fear_screen")--First convo screen to pull.
			elseif ( hasComplete == true ) then
				nextConversationScreen = conversation:getScreen("last_screen")--First convo screen to pull.
			elseif ( hasAcceptedTwo == true ) then
				nextConversationScreen = conversation:getScreen("help_screen")--First convo screen to pull.
			elseif ( hasAccepted == true ) then
				nextConversationScreen = conversation:getScreen("first_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("hello_screen")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function theed_smuggler_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "eighth_screen" ) then
		player:setScreenPlayState(eisleyGang.states.quest.phaseone, eisleyGang.questString)
	end

	if ( screenID == "last_screen2" ) then
		player:setScreenPlayState(eisleyGang.states.quest.phasethree, eisleyGang.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_05.iff", -1)
	end
	
	if ( screenID == "sixth_sceen_c" ) then
		player:setScreenPlayState(eisleyGang.states.quest.phasefour, eisleyGang.questString)
		giveItem(pInventory, "object/tangible/loot/collectible/collectible_parts/padawan_holocron_05.iff", -1)
	end

	return conversationScreen
end
