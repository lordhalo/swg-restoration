shrine_bosses_talusScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_talusScreenPlay",
}
registerScreenPlay("shrine_bosses_talusScreenPlay", true)
function shrine_bosses_talusScreenPlay:start()
	if (isZoneEnabled("talus")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

	end
end



--sith
function shrine_bosses_talusScreenPlay:spawnsith1()
	local sith1 = spawnMobile("talus", "sith_shrine_guardian", 0, -5785.6, 226.3, 4479.5, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_talusScreenPlay:spawnsith2()
	local sith2 = spawnMobile("talus", "sith_shrine_guardian", 0, -8494.4, 106.5, -3237.6, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_talusScreenPlay:spawnsith3()
	local sith3 = spawnMobile("talus", "sith_shrine_guardian", 0, 318, 12.9, 5843.3, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_talusScreenPlay:spawnsith4()
	local sith4 = spawnMobile("talus", "sith_shrine_guardian", 0, 5760, 653.3, -5206.7, -11, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchsith4", sith4)
end

--jedi
function shrine_bosses_talusScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("talus", "jedi_shrine_guardian", 0, -5785.6, 226.3, 4479.5, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_talusScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("talus", "jedi_shrine_guardian", 0, -8494.4, 106.5, -3237.6, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_talusScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("talus", "jedi_shrine_guardian", 0, 318, 12.9, 5843.3, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_talusScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("talus", "jedi_shrine_guardian", 0, 5760, 653.3, -5206.7, -11, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_talusScreenPlay", "switchjedi4", jedi4)
end




function shrine_bosses_talusScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_talusScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_talusScreenPlay", "spawnsith4", nil, "")
	return 0
end
