shrine_bosses_yavinScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_yavinScreenPlay",
}
registerScreenPlay("shrine_bosses_yavinScreenPlay", true)
function shrine_bosses_yavinScreenPlay:start()
	if (isZoneEnabled("yavin4")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

	end
end



--sith
function shrine_bosses_yavinScreenPlay:spawnsith1()
	local sith1 = spawnMobile("yavin4", "sith_shrine_guardian", 0, -3362.1, 236.4, 6916.8, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_yavinScreenPlay:spawnsith2()
	local sith2 = spawnMobile("yavin4", "sith_shrine_guardian", 0, 6455.1, 18.9, 6427.6, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_yavinScreenPlay:spawnsith3()
	local sith3 = spawnMobile("yavin4", "sith_shrine_guardian", 0, -4584.8, 588.4, -3758.6, 5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_yavinScreenPlay:spawnsith4()
	local sith4 = spawnMobile("yavin4", "sith_shrine_guardian", 0, 2389.9, 80.8, -4932.4, -7, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchsith4", sith4)
end

--jedi
function shrine_bosses_yavinScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("yavin4", "jedi_shrine_guardian", 0, -3362.1, 236.4, 6916.8, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_yavinScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("yavin4", "jedi_shrine_guardian", 0, 6455.1, 18.9, 6427.6, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_yavinScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("yavin4", "jedi_shrine_guardian", 0, -4584.8, 588.4, -3758.6, 5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_yavinScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("yavin4", "jedi_shrine_guardian", 0, 2389.9, 80.8, -4932.4, -7, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_yavinScreenPlay", "switchjedi4", jedi4)
end




function shrine_bosses_yavinScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_yavinScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_yavinScreenPlay", "spawnsith4", nil, "")
	return 0
end
