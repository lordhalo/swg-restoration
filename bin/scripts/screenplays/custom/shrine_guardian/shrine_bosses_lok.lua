shrine_bosses_lokScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_lokScreenPlay",
}
registerScreenPlay("shrine_bosses_lokScreenPlay", true)
function shrine_bosses_lokScreenPlay:start()
	if (isZoneEnabled("lok")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_lokScreenPlay:spawnsith1()
	local sith1 = spawnMobile("lok", "sith_shrine_guardian", 0, -2132.6, 105.2, 5940.4, 3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_lokScreenPlay:spawnsith2()
	local sith2 = spawnMobile("lok", "sith_shrine_guardian", 0, 5454.9, 17.1, 3806.7, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_lokScreenPlay:spawnsith3()
	local sith3 = spawnMobile("lok", "sith_shrine_guardian", 0, -5806.9, 34.1, 1980.7, -5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_lokScreenPlay:spawnsith4()
	local sith4 = spawnMobile("lok", "sith_shrine_guardian", 0, -3641.5, 17.4, -6027.8, -3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_lokScreenPlay:spawnsith5()
	local sith5 = spawnMobile("lok", "sith_shrine_guardian", 0, 4979.1, 116.4, -5674.1, 12, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchsith5", sith5)
end



--jedi
function shrine_bosses_lokScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("lok", "jedi_shrine_guardian", 0, -2132.6, 105.2, 5940.4, 3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_lokScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("lok", "jedi_shrine_guardian", 0, 5454.9, 17.1, 3806.7, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_lokScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("lok", "jedi_shrine_guardian", 0, -5806.9, 34.1, 1980.7, -5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_lokScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("lok", "jedi_shrine_guardian", 0, -3641.5, 17.4, -6027.8, -3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_lokScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("lok", "jedi_shrine_guardian", 0, 4979.1, 116.4, -5674.1, 12, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_lokScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_lokScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_lokScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_lokScreenPlay", "spawnsith5", nil, "")
	return 0
end
