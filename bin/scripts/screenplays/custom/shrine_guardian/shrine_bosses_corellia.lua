shrine_bosses_corelliaScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_corelliaScreenPlay",
}
registerScreenPlay("shrine_bosses_corelliaScreenPlay", true)
function shrine_bosses_corelliaScreenPlay:start()
	if (isZoneEnabled("corellia")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_corelliaScreenPlay:spawnsith1()
	local sith1 = spawnMobile("corellia", "sith_shrine_guardian", 0, -7391.7, 236.0, -3936, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_corelliaScreenPlay:spawnsith2()
	local sith2 = spawnMobile("corellia", "sith_shrine_guardian", 0, 6093.1, 342.8, -5577.9, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_corelliaScreenPlay:spawnsith3()
	local sith3 = spawnMobile("corellia", "sith_shrine_guardian", 0, -6907, 448, 4527, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_corelliaScreenPlay:spawnsith4()
	local sith4 = spawnMobile("corellia", "sith_shrine_guardian", 0, -2384, 220.2, 6393, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_corelliaScreenPlay:spawnsith5()
	local sith5 = spawnMobile("corellia", "sith_shrine_guardian", 0, 6301, 352.2, 6688, 3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchsith5", sith5)
end

--jedi
function shrine_bosses_corelliaScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("corellia", "jedi_shrine_guardian", 0, -7391.7, 236.0, -3936, -2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_corelliaScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("corellia", "jedi_shrine_guardian", 0, 6093.1, 342.8, -5577.9, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_corelliaScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("corellia", "jedi_shrine_guardian", 0, -6907, 448, 4527, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_corelliaScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("corellia", "jedi_shrine_guardian", 0, -2384, 220.2, 6393, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_corelliaScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("corellia", "jedi_shrine_guardian", 0, 6301, 352.2, 6688, 3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_corelliaScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_corelliaScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_corelliaScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_corelliaScreenPlay", "spawnsith5", nil, "")
	return 0
end
