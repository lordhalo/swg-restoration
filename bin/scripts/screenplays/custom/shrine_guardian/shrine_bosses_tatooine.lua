shrine_bosses_tatooineScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_tatooineScreenPlay",
}
registerScreenPlay("shrine_bosses_tatooineScreenPlay", true)
function shrine_bosses_tatooineScreenPlay:start()
	if (isZoneEnabled("tatooine")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_tatooineScreenPlay:spawnsith1()
	local sith1 = spawnMobile("tatooine", "sith_shrine_guardian", 0, -6506.9, 73.6, -3666.7, -3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_tatooineScreenPlay:spawnsith2()
	local sith2 = spawnMobile("tatooine", "sith_shrine_guardian", 0, 5264.4, 188.8, 113.8, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_tatooineScreenPlay:spawnsith3()
	local sith3 = spawnMobile("tatooine", "sith_shrine_guardian", 0, 5633.2, 194.6, 6016.2, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_tatooineScreenPlay:spawnsith4()
	local sith4 = spawnMobile("tatooine", "sith_shrine_guardian", 0, 5958.4, 155.1, -5683.4, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_tatooineScreenPlay:spawnsith5()
	local sith5 = spawnMobile("tatooine", "sith_shrine_guardian", 0, -3623.2, 248.4, 5281.5, -5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchsith5", sith5)
end

--jedi
function shrine_bosses_tatooineScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("tatooine", "jedi_shrine_guardian", 0, -6506.9, 73.6, -3666.7, -3, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_tatooineScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("tatooine", "jedi_shrine_guardian", 0, 5264.4, 188.8, 113.8, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_tatooineScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("tatooine", "jedi_shrine_guardian", 0, 5633.2, 194.6, 6016.2, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_tatooineScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("tatooine", "jedi_shrine_guardian", 0, 5958.4, 155.1, -5683.4, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_tatooineScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("tatooine", "jedi_shrine_guardian", 0, -3623.2, 248.4, 5281.5, -5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_tatooineScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_tatooineScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_tatooineScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_tatooineScreenPlay", "spawnsith5", nil, "")
	return 0
end
