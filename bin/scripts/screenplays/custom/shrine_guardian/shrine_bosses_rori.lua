shrine_bosses_roriScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_roriScreenPlay",
}
registerScreenPlay("shrine_bosses_roriScreenPlay", true)
function shrine_bosses_roriScreenPlay:start()
	if (isZoneEnabled("rori")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_roriScreenPlay:spawnsith1()
	local sith1 = spawnMobile("rori", "sith_shrine_guardian", 0, -6375.2, 75.3, 6404.9, 7, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_roriScreenPlay:spawnsith2()
	local sith2 = spawnMobile("rori", "sith_shrine_guardian", 0, -4496.5, 180.5, -7529.3, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_roriScreenPlay:spawnsith3()
	local sith3 = spawnMobile("rori", "sith_shrine_guardian", 0, 307.1, 80.8, -976.5, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_roriScreenPlay:spawnsith4()
	local sith4 = spawnMobile("rori", "sith_shrine_guardian", 0, -925.9, 83.8, 6048, 5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_roriScreenPlay:spawnsith5()
	local sith5 = spawnMobile("rori", "sith_shrine_guardian", 0, 6854.7, 85.9, -1219.0, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchsith5", sith5)
end

--jedi
function shrine_bosses_roriScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("rori", "jedi_shrine_guardian", 0, -6375.2, 75.3, 6404.9, 7, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_roriScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("rori", "jedi_shrine_guardian", 0, -4496.5, 180.5, -7529.3, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_roriScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("rori", "jedi_shrine_guardian", 0, 307.1, 80.8, -976.5, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_roriScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("rori", "jedi_shrine_guardian", 0, -925.9, 83.8, 6048, 5, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_roriScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("rori", "jedi_shrine_guardian", 0, 6854.7, 85.9, -1219.0, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_roriScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_roriScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_roriScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_roriScreenPlay", "spawnsith5", nil, "")
	return 0
end
