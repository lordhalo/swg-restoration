shrine_bosses_dantooineScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_dantooineScreenPlay",
}
registerScreenPlay("shrine_bosses_dantooineScreenPlay", true)
function shrine_bosses_dantooineScreenPlay:start()
	if (isZoneEnabled("dantooine")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_dantooineScreenPlay:spawnsith1()
	local sith1 = spawnMobile("dantooine", "sith_shrine_guardian", 0, -6173, 35, 4120, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_dantooineScreenPlay:spawnsith2()
	local sith2 = spawnMobile("dantooine", "sith_shrine_guardian", 0, 2163.5, 162, 7551.7, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_dantooineScreenPlay:spawnsith3()
	local sith3 = spawnMobile("dantooine", "sith_shrine_guardian", 0, 2640.9, 11.5, -1534.4, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_dantooineScreenPlay:spawnsith4()
	local sith4 = spawnMobile("dantooine", "sith_shrine_guardian", 0, -6999.4, 10.9, -5268.7, 1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_dantooineScreenPlay:spawnsith5()
	local sith5 = spawnMobile("dantooine", "sith_shrine_guardian", 0, -1814, 8.6, -6201.1, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchsith5", sith5)
end



--jedi
function shrine_bosses_dantooineScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("dantooine", "jedi_shrine_guardian", 0, -6173, 35, 4120, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_dantooineScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("dantooine", "jedi_shrine_guardian", 0, 2163.5, 162, 7551.7, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_dantooineScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("dantooine", "jedi_shrine_guardian", 0, 2640.9, 11.5, -1534.4, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_dantooineScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("dantooine", "jedi_shrine_guardian", 0, -6999.4, 10.9, -5268.7, 1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_dantooineScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("dantooine", "jedi_shrine_guardian", 0, -1814, 8.6, -6201.1, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dantooineScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_dantooineScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_dantooineScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_dantooineScreenPlay", "spawnsith5", nil, "")
	return 0
end
