shrine_bosses_dathomirScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_dathomirScreenPlay",
}
registerScreenPlay("shrine_bosses_dathomirScreenPlay", true)
function shrine_bosses_dathomirScreenPlay:start()
	if (isZoneEnabled("dathomir")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_dathomirScreenPlay:spawnsith1()
	local sith1 = spawnMobile("dathomir", "sith_shrine_guardian", 0, -4148.2, 119.3, 5928.5, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_dathomirScreenPlay:spawnsith2()
	local sith2 = spawnMobile("dathomir", "sith_shrine_guardian", 0, 1654.9, 103.9, -5763.1, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_dathomirScreenPlay:spawnsith3()
	local sith3 = spawnMobile("dathomir", "sith_shrine_guardian", 0, 3087.5, 125.4, 4890.4, -6, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_dathomirScreenPlay:spawnsith4()
	local sith4 = spawnMobile("dathomir", "sith_shrine_guardian", 0, 5570.9, 101.1, -1511.6, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_dathomirScreenPlay:spawnsith5()
	local sith5 = spawnMobile("dathomir", "sith_shrine_guardian", 0, -4961.5, 130.4, -3492.7, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchsith5", sith5)
end



--jedi
function shrine_bosses_dathomirScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("dathomir", "jedi_shrine_guardian", 0, -4148.2, 119.3, 5928.5, -1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_dathomirScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("dathomir", "jedi_shrine_guardian", 0, 1654.9, 103.9, -5763.1, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_dathomirScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("dathomir", "jedi_shrine_guardian", 0, 3087.5, 125.4, 4890.4, -6, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_dathomirScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("dathomir", "jedi_shrine_guardian", 0, 5570.9, 101.1, -1511.6, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_dathomirScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("dathomir", "jedi_shrine_guardian", 0, -4961.5, 130.4, -3492.7, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_dathomirScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_dathomirScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_dathomirScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_dathomirScreenPlay", "spawnsith5", nil, "")
	return 0
end
