shrine_bosses_nabooScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_nabooScreenPlay",
}
registerScreenPlay("shrine_bosses_nabooScreenPlay", true)
function shrine_bosses_nabooScreenPlay:start()
	if (isZoneEnabled("naboo")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

	end
end



--sith
function shrine_bosses_nabooScreenPlay:spawnsith1()
	local sith1 = spawnMobile("naboo", "sith_shrine_guardian", 0, -6859, 475.7, -1937, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_nabooScreenPlay:spawnsith2()
	local sith2 = spawnMobile("naboo", "sith_shrine_guardian", 0, -2582, 4.2, -6184, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_nabooScreenPlay:spawnsith3()
	local sith3 = spawnMobile("naboo", "sith_shrine_guardian", 0, 2377.5, 291.1, -472.1, -9, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_nabooScreenPlay:spawnsith4()
	local sith4 = spawnMobile("naboo", "sith_shrine_guardian", 0, 7182, 331.3, -234, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchsith4", sith4)
end

--jedi
function shrine_bosses_nabooScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("naboo", "jedi_shrine_guardian", 0, -6859, 475.7, -1937, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_nabooScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("naboo", "jedi_shrine_guardian", 0, -2582, 4.2, -6184, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_nabooScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("naboo", "jedi_shrine_guardian", 0, 2377.5, 291.1, -472.1, -9, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_nabooScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("naboo", "jedi_shrine_guardian", 0, 7182, 331.3, -234, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_nabooScreenPlay", "switchjedi4", jedi4)
end




function shrine_bosses_nabooScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_nabooScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_nabooScreenPlay", "spawnsith4", nil, "")
	return 0
end
