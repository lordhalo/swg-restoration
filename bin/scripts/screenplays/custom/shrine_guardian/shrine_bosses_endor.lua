shrine_bosses_endorScreenPlay = ScreenPlay:new {
	screenplayName = "shrine_bosses_endorScreenPlay",
}
registerScreenPlay("shrine_bosses_endorScreenPlay", true)
function shrine_bosses_endorScreenPlay:start()
	if (isZoneEnabled("endor")) then

		local rand
		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith1()
		else
			self:spawnjedi1()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith2()
		else
			self:spawnjedi2()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith3()
		else
			self:spawnjedi3()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith4()
		else
			self:spawnjedi4()
		end

		rand = getRandomNumber(2)
		if (rand == 2) then
			self:spawnsith5()
		else
			self:spawnjedi5()
		end

	end
end



--sith
function shrine_bosses_endorScreenPlay:spawnsith1()
	local sith1 = spawnMobile("endor", "sith_shrine_guardian", 0, -5055.4, 201, -1700.9, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchsith1", sith1)
end

function shrine_bosses_endorScreenPlay:spawnsith2()
	local sith2 = spawnMobile("endor", "sith_shrine_guardian", 0, -5627.3, 400, 4815.6, 1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchsith2", sith2)
end

function shrine_bosses_endorScreenPlay:spawnsith3()
	local sith3 = spawnMobile("endor", "sith_shrine_guardian", 0, -3871, 0, -4467.7, -12, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchsith3", sith3)
end

function shrine_bosses_endorScreenPlay:spawnsith4()
	local sith4 = spawnMobile("endor", "sith_shrine_guardian", 0, 670.8, 205.2, 5550.6, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchsith4", sith4)
end

function shrine_bosses_endorScreenPlay:spawnsith5()
	local sith5 = spawnMobile("endor", "sith_shrine_guardian", 0, 5117.1, 18.3, 1926.4, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchsith5", sith5)
end



--jedi
function shrine_bosses_endorScreenPlay:spawnjedi1()
	local jedi1 = spawnMobile("endor", "jedi_shrine_guardian", 0, -5055.4, 201, -1700.9, 2, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchjedi1", jedi1)
end

function shrine_bosses_endorScreenPlay:spawnjedi2()
	local jedi2 = spawnMobile("endor", "jedi_shrine_guardian", 0, -5627.3, 400, 4815.6, 1, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchjedi2", jedi2)
end

function shrine_bosses_endorScreenPlay:spawnjedi3()
	local jedi3 = spawnMobile("endor", "jedi_shrine_guardian", 0, -3871, 0, -4467.7, -12, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchjedi3", jedi3)
end

function shrine_bosses_endorScreenPlay:spawnjedi4()
	local jedi4 = spawnMobile("endor", "jedi_shrine_guardian", 0, 670.8, 205.2, 5550.6, 4, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchjedi4", jedi4)
end

function shrine_bosses_endorScreenPlay:spawnjedi5()
	local jedi5 = spawnMobile("endor", "jedi_shrine_guardian", 0, 5117.1, 18.3, 1926.4, 0, 0)
	createObserver(OBJECTDESTRUCTION, "shrine_bosses_endorScreenPlay", "switchjedi5", jedi5)
end




function shrine_bosses_endorScreenPlay:switchsith1(sith1, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnjedi1", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchsith2(sith2, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnjedi2", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchsith3(sith3, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnjedi3", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchsith4(sith4, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnjedi4", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchsith5(sith5, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnjedi5", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchjedi1(jedi1, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnsith1", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchjedi2(jedi2, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnsith2", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchjedi3(jedi3, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnsith3", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchjedi4(jedi4, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnsith4", nil, "")
	return 0
end

function shrine_bosses_endorScreenPlay:switchjedi5(jedi5, pKiller)
createEvent(1800000, "shrine_bosses_endorScreenPlay", "spawnsith5", nil, "")
	return 0
end
