local ObjectManager = require("managers.object.object_manager")

GcwJoinFactionConvo = Object:new {}


gcwJoinFaction_handler = Object:new {
	
 }

function gcwJoinFaction_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local isDeathWatch = creature:hasSkill("death_watch_novice")
			local isBlackSun = creature:hasSkill("black_sun_novice")
			local isJedi = creature:hasSkill("fs_jedi_novice")
			local isStormtrooper = creature:hasSkill("imp_rank_novice")
			local isSpecForce = creature:hasSkill("reb_rank_novice")

			if(isSpecForce == true or isStormtrooper == true) then
				nextConversationScreen = conversation:getScreen("go_away")
			elseif(isDeathWatch == false and isBlackSun == false and isJedi == false) then
				if (gcwScreenplay:getFactionFromHashCode(creature:getFaction()) == "rebel") then
					nextConversationScreen = conversation:getScreen("greet_friend")
				elseif (gcwScreenplay:getFactionFromHashCode(creature:getFaction()) == "imperial") then
					nextConversationScreen = conversation:getScreen("greet_imperial")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end
			else
				nextConversationScreen = conversation:getScreen("go_away")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function gcwJoinFaction_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)

		if (screenID == "join_rank") then
			awardSkill(conversingPlayer, "reb_rank_novice")
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:setFactionStatus(2)
		end)
		end

		if (screenID == "join_imperial") then
			awardSkill(conversingPlayer, "imp_rank_novice")
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:setFactionStatus(2)
			end)
		end
	
	return conversationScreen
end
