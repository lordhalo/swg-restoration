Dark_Enclave = ScreenPlay:new {
	numberOfActs = 1,

}

registerScreenPlay("Dark_Enclave", true)

function Dark_Enclave:start()
	if (isZoneEnabled("yavin4")) then

		local random = math.random(100)

		if random >= 30 then
			self:spawnImperials()
		elseif random < 5 then
			self:spawnRebels()
		else
			self:spawnTuskens()
		end

	end
end

function Dark_Enclave:cellPlayerPermissionsObserver(pCreature, pCreature2)
	return ObjectManager.withSceneObject(pCreature, function(creatureSceneObject)
		if creatureSceneObject:isCreatureObject() then
			for i = 1, # self.permissionMap, 1 do
				self:setCellPermissions(self.permissionMap[i], pCreature)
			end
		end
		return 0
	end)
end
	
