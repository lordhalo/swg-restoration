combatTest = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "combatTest",
}
registerScreenPlay("combatTest", true)

function combatTest:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end

--Mobile Spawning
function combatTest:spawnMobiles()
	
	--Trainer
	--TODO:CHANGE LOCATION OF TRAINERS
	--Initiate
	spawnMobile("tatooine", "fs_jedi", 1, 5388, 78.5, -4154, 0, 0)

	--Lightsabers
	spawnMobile("tatooine", "lightsaber_master", 1, 1955, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "lightsaber_novice", 1, 1960, 12.0, 2336, 0, 0)

	--Healing
	spawnMobile("tatooine", "healing_master", 1, 1965, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "healing_novice", 1, 1970, 12.0, 2336, 0, 0)

	--Defender
	spawnMobile("tatooine", "defender_master", 1, 1975, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "defender_novice", 1, 1980, 12.0, 2336, 0, 0)

	--Powers
	spawnMobile("tatooine", "forcemaster_master", 1, 1985, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "force_expert",       1, 1990, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "force_power_expert", 1, 1995, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "force_power_master", 1, 2000, 12.0, 2336, 0, 0)

	--spawnMobile("tatooine", "rank_grey_trainer", 1, 2002, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "rank_dark_trainer", 1, 2004, 12.0, 2336, 0, 0)
	spawnMobile("tatooine", "rank_light_trainer", 1, 2006, 12.0, 2336, 0, 0)

	spawnMobile("tatooine", "enraged_bull_rancor", 120, -2668, 0, -6794, 0, 0)
	spawnMobile("tatooine", "mutant_rancor", 120, -2787, 0, -6711, 0, 0)
	spawnMobile("tatooine", "rancor", 120, -2671, 0, -6635, 0, 0)

	spawnMobile("tatooine", "canyon_krayt_dragon", 120, -2613, 0, -6490, 0, 0)

	spawnMobile("tatooine", "bol", 120, -2868, 0, -6786, 0, 0)

	spawnMobile("tatooine", "nightsister_initiate", 120, -2557, 0, -6651, 0, 0)
--sith_shadow_mercenary" Change back to old loot
end
