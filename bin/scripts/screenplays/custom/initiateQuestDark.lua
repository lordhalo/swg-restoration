local ObjectManager = require("managers.object.object_manager")

initiateQuestDark = ScreenPlay:new {
	numberOfActs = 1,
	questString = "initiateQuestDark",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
}
registerScreenPlay("initiateQuestDark", true)

function initiateQuestDark:start()

end

--Setup
function initiateQuestDark:getActivePlayerName()
	return self.questdata.activePlayerName
end

function initiateQuestDark:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

initiateQuestDark_handler = Object:new {
	
 }

function initiateQuestDark_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuestDark.states.quest.phaseone, initiateQuestDark.questString)
	--local completedFS = creature:hasScreenPlayState(force_sensitive.states.quest.intro, force_sensitive.questString)
	local completedFS = creature:hasScreenPlayState(32768, "ForceSensitiveDark")
	local hasSkill = creature:hasSkill("fs_jedi_c_4")
	local hasSkillTwo = creature:hasSkill("fs_jedi_d_4")
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  

			if (completedFS == false) then
				nextConversationScreen = conversation:getScreen("initiate_not_ready")
			
			elseif ( completedQuest == false ) then
				if(hasSkill == true) and (hasSkillTwo == true) then
					nextConversationScreen = conversation:getScreen("initiate_first_screen")
				else
					nextConversationScreen = conversation:getScreen("lightsaber_screen")	
				end		

			elseif ( completedQuest == true ) then
				nextConversationScreen = conversation:getScreen("next_npc_screen")

			else
				nextConversationScreen = conversation:getScreen("hello_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function initiateQuestDark_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "initiate_complete_screen") then
		player:setScreenPlayState(initiateQuestDark.states.quest.phaseone, initiateQuestDark.questString)
	end
	
	return conversationScreen
end

initiateQuestDarkTwo_handler = Object:new {
	
 }

function initiateQuestDarkTwo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuestDark.states.quest.phaseone, initiateQuestDark.questString)
	local completedQuestTwo = creature:hasScreenPlayState(initiateQuestDark.states.quest.phasetwo, initiateQuestDark.questString)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  
			if (completedQuestTwo == true) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			elseif (completedQuest == false) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			else
				nextConversationScreen = conversation:getScreen("first_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
	
end

function initiateQuestDarkTwo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "final_screen") then
		if (conversingPlayer == nil) then
			return
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "color_crystals", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		player:setScreenPlayState(initiateQuestDark.states.quest.phasetwo, initiateQuestDark.questString)
	end
	

	return conversationScreen
end

initiateQuestDarkThree_handler = Object:new {
	
 }

function initiateQuestDarkThree_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local completedQuest = creature:hasScreenPlayState(initiateQuestDark.states.quest.phasetwo, initiateQuestDark.questString)
	local hasSkill = creature:hasSkill("fs_jedi_master")
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)  
			if(hasSkill == true) then
				nextConversationScreen = conversation:getScreen("initiate_screen")
			elseif (completedQuest == false) then
				nextConversationScreen = conversation:getScreen("hello_screen")
			else
				nextConversationScreen = conversation:getScreen("first_screen")
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
	
end

function initiateQuestDarkThree_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	if (screenID == "screen_3a") then
		player:setScreenPlayState(initiateQuestDark.states.quest.phasethree, initiateQuestDark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Final Trial", "", 6051, 598, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	

	return conversationScreen
end



