local QuestManager = require("managers.quest.quest_manager")

--[[
TODO: UPDATE THIS INFORMATION

DESC ABOUT QUEST

Quest Details:

You start the quest by talking to Thiel, it will then send you to talk to all the mentors and answering questions. 
Eventually leading to Rohak, after that you will need to find a holocron, this holocron will send you back to rohak.
Rohak will talk about sith shadows, and mellichae. You will then be given the choice of who you want to help... Paemos, or Sarguillo.
Whatever quest line you pick you will be tested in ways to determine what you will be: Light, Dark, or Exile.
After you complete the quest you will be sent to the according "enclave" to join that faction.



TODO:

-Paemos convo
-Captain convo * fix
-Lost FS convo
-Mellichae convo
	-Lightside done
	-Exile = need information from halo
	-Darkside more work needed

-Darkside side note
use sendTo:player
]]

local ObjectManager = require("managers.object.object_manager")
force_sensitive_dark = ScreenPlay:new { 
	numberOfActs = 1, 
	questString = "ForceSensitiveDark",
	states = {
		quest = { intro = 2, defense = 4, craft = 8, forced = 16, force = 32, forcetwo = 64, deftwo = 128, lulzione = 256, lulzitwo = 512, lulzithree = 1024, lulzifour = 2048, lulzifive = 4096, lulzisix = 8192, lulziseven = 16384, lulzieight = 32768 }
	}, 
	questdata = Object:new {
		
		activePlayerName = "initial",
	}
	
}

registerScreenPlay("force_sensitive_dark", true)

function force_sensitive_dark:start() 
	if (isZoneEnabled("yavin4")) then
		self:SpawnMobiles()
		self:SpawnSceneObjects()
	end

end

function force_sensitive_dark:SpawnSceneObjects()

	spawnSceneObject("yavin4", "object/tangible/camp/campfire_logs_fresh.iff", -5793, 83.5, 4838.9, 0, 0, 0, 0, 0)

end

function force_sensitive_dark:SpawnMobiles()

	spawnMobile("yavin4","tikqot", 1, 7.6, -43.4, -34.6, 0, 3435634) --Q1
	spawnMobile("yavin4","whikul", 1, 50.7, -42.4, -74.4, -90, 3435652) --Q2
	spawnMobile("yavin4","yanon", 1, 49.8, -41.4, -94.9, -90, 3435653) --Q3
	spawnMobile("yavin4","juwu", 1, -46.0, -42.4, -73.9, 90, 3435650) --Q4
	spawnMobile("yavin4","shisil", 1, -44.1, -39.4, -106.9, 90, 3435651) --Q5
	spawnMobile("yavin4","lulzi", 1, 9.7,-47.4,-84.6,-52,3435643)
	spawnMobile("yavin4","lulzi_two", 1, 5136,95,284,95,0)
	spawnMobile("yavin4","lulzi_three", 1, 4923,94,570,144,0)
	spawnMobile("yavin4","dark_quest_two", 1, 4925,94,568,96,0)

end

--Setup
function force_sensitive_dark:getActivePlayerName()
	return self.questdata.activePlayerName
end

function force_sensitive_dark:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end




--Tikqot Conv
force_sensitive_dark_convo_handler = Object:new {
	
 }

function force_sensitive_dark_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			
			local creature = LuaCreatureObject(conversingPlayer)

			local  hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.intro, force_sensitive_dark.questString)

			local  hasAcceptedTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulziseven, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_b_4")

			if ( hasAcceptedTwo == true and hasSkill == true) then
				nextConversationScreen = conversation:getScreen("return_screen")--First convo screen to pull.
			elseif ( hasAcceptedTwo == true and hasSkill == false) then
				nextConversationScreen = conversation:getScreen("crystal_screen")--First convo screen to pull.
			elseif ( hasAccepted == false ) then
				nextConversationScreen = conversation:getScreen("intro_first_screen")--First convo screen to pull.
			else
				nextConversationScreen = conversation:getScreen("complete")--End of the road.
			end
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.intro, force_sensitive_dark.questString)	
	local pInventory = player:getSlottedObject("inventory")
	if ( screenID == "tikqotFinal" ) then
		player:setScreenPlayState(force_sensitive_dark.states.quest.intro, force_sensitive_dark.questString)
		giveItem(pInventory, "object/tangible/loot/quest/force_sensitive/force_crystal.iff", -1)
		giveItem(pInventory, "object/tangible/crafting/station/generic_tool.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_mineral.iff", -1)
		giveItem(pInventory, "object/tangible/survey_tool/survey_tool_liquid.iff", -1)
		giveItem(pInventory, "object/weapon/melee/baton/baton_fs.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/food/crafted/dish_rations.iff", -1)
		giveItem(pInventory, "object/tangible/medicine/crafted/crafted_stimpack_sm_s1_a.iff", -1)
		giveItem(pInventory, "object/tangible/medicine/crafted/crafted_stimpack_sm_s1_a.iff", -1)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Lord Whikul", "", 4995, 259, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end

	if (screenID == "fs_done" ) then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzieight, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Vlixu", "", 5012, 307, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	--print("returning convosvreen")
	return conversationScreen
end


--##############################################################################################################################################
--###################################################################  CAPTAIN	################################################################
--##############################################################################################################################################
--captain

force_sensitive_dark_defense_conv_handler = Object:new {

}


function force_sensitive_dark_defense_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.intro, force_sensitive_dark.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.defense, force_sensitive_dark.questString)
			local partTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.forcetwo, force_sensitive_dark.questString)
			local partTwoComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.deftwo, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_d_2")  
			
			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("not_yet")
			elseif hasAccepted == true and hasComplete == false then 
				nextConversationScreen = conversation:getScreen("defense_first_screen")	
			elseif partTwo == true and hasSkill == true then
				nextConversationScreen = conversation:getScreen("def_part2")	
			elseif partTwo == true and hasSkill == false then
				nextConversationScreen = conversation:getScreen("def_part2_crystal")
			elseif partTwoComplete == true then
				nextConversationScreen = conversation:getScreen("complete2")	
			elseif hasComplete == true then
				nextConversationScreen = conversation:getScreen("defFinalOne")		
			else				
				nextConversationScreen = conversation:getScreen("complete")	--edit to different reply	
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end
	end			
	return nextConversationScreen	
end


function force_sensitive_dark_defense_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local creature = LuaCreatureObject(conversingNPC)

	if (screenID == "info2") then
		creature:playEffect("clienteffect/pl_force_armor_self.cef", "")
	end 
	

	if (screenID == "def6") then
		--player:setScreenPlayState(force_sensitive_dark.states.quest.defense, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			local pOne = spawnMobile("yavin4", "sith_shadow_slave", 0, 45.8, -42.4, -70.8, -100, 3435652)
			writeData("force_sensitive_dark:trainerOneKill",0)
			createObserver(DAMAGERECEIVED, "force_sensitive_dark", "slave_damage", pOne)
       			createObserver(OBJECTDESTRUCTION, "force_sensitive_dark", "trainerOneDead", pOne)	
		end
	end

	if (screenID == "defFinal") then
		--player:setScreenPlayState(force_sensitive_dark.states.quest.defense, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Yanon", "", 4975, 260, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end

	if (screenID == "def_part4") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.deftwo, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Lulzi", "", 4985, 300, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive_dark:slave_damage(creatureObject, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(creatureObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.99)) or (action <= (maxAction * 0.99)) or (mind <= (maxMind * 0.99))) and readData("fsdark:slavedamage") == 0) then
		spatialChat(creatureObject, "No, Please don't!")
		writeData("fsdark:slavedamage",1)
	elseif (((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) and readData("fsdark:slavedamage2") == 0) then
		spatialChat(creatureObject, "Ill do anything, stop! stop!")
		writeData("fsdark:slavedamage2",1)
	elseif ((health <= (maxHealth * 0.01)) or (action <= (maxAction * 0.01)) or (mind <= (maxMind * 0.01))) then
		spatialChat(creatureObject, "...It hurts..I was nothing..")
		writeData("fsdark:slavedamage",0)
		writeData("fsdark:slavedamage2",0)
	end

	return 0

end

function force_sensitive_dark:trainerOneDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive_dark:trainerOneKill") == 0 ) then

	        	writeData("force_sensitive_dark:trainerOneKill",1)
			creature:setScreenPlayState(force_sensitive_dark.states.quest.defense, force_sensitive_dark.questString)

		end
	     
     return 0
end


--##############################################################################################################################################
--###################################################################  	LAGUO	################################################################
--##############################################################################################################################################
--La'guo

force_sensitive_dark_craft_conv_handler = Object:new {

}

function force_sensitive_dark_craft_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.defense, force_sensitive_dark.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_1")

			--print("hasAccepted() is " .. hasAccepted)   
			
			if ((hasAccepted == false and hasComplete == false)) then
				nextConversationScreen = conversation:getScreen("craft_not_yet")--First convo screen to pull.
				
			else
				--### KEY CONVO PARTS, AND STATES ###--
				if (hasSkill == true) then
					if ((hasAccepted == true) and (hasComplete == false)) then 
						nextConversationScreen = conversation:getScreen("craft_first_screen")
					elseif (hasComplete == true) then
						nextConversationScreen = conversation:getScreen("complete")			
					else
						nextConversationScreen = conversation:getScreen("complete")
					end
				else
					nextConversationScreen = conversation:getScreen("craft_not_yet")
				end			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_craft_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)	
	if (screenID == "craftFinal") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Shisil", "", 4963, 354, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end


--##############################################################################################################################################
--###################################################################  PAEMOS	################################################################
--##############################################################################################################################################
--paemos

force_sensitive_dark_forced_conv_handler = Object:new {

}


function force_sensitive_dark_forced_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.forced, force_sensitive_dark.questString) 
			local hasSkill = creature:hasSkill("fs_jedi_b_1") 

			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("forced_not_yet")
			elseif hasAccepted == true and hasComplete == false  then 
				if(hasSkill == true) then 
					nextConversationScreen = conversation:getScreen("forced_first_screen")	
				else
					nextConversationScreen = conversation:getScreen("forced_not_yet")
				end							
			else			
				nextConversationScreen = conversation:getScreen("forced_thank_you")			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end
	end			
	return nextConversationScreen	
end

function force_sensitive_dark_forced_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.forced, force_sensitive_dark.questString)	
	if (screenID == "forced_complete") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.forced, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Juwu", "", 4996, 356, 5, true, true, WAYPOINTTHEMEPARK, 1)	
		end
	end

	return conversationScreen
end



--##############################################################################################################################################
--###################################################################  NOLDAN	################################################################
--##############################################################################################################################################

--noldan
force_sensitive_dark_force_conv_handler = Object:new {

}

function force_sensitive_dark_force_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.forced, force_sensitive_dark.questString)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.force, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_c_1")   
			local hasSkillTwo = creature:hasSkill("fs_jedi_d_1")   

			if ((hasAccepted == false and hasComplete == false)) then
				nextConversationScreen = conversation:getScreen("force_not_yet")--First convo screen to pull.
				
			else
				--### KEY CONVO PARTS, AND STATES ###--
				if ((hasAccepted == true) and (hasComplete == false)) then 
					if (hasSkill == true) then
					nextConversationScreen = conversation:getScreen("force_first_screen")
					else
					nextConversationScreen = conversation:getScreen("force_not_yet")
					end
				elseif (hasComplete == true and hasSkillTwo == true) then
					nextConversationScreen = conversation:getScreen("force_part2")			
				else
					nextConversationScreen = conversation:getScreen("force_thank_you")
				end			
			end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_force_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.force, force_sensitive_dark.questString)	

	if (screenID == "force_continue") then
		player:playEffect("clienteffect/pl_force_absorb_self.cef", "")
	end 

	if (screenID == "force1") then
		player:playEffect("clienteffect/pl_force_heal_dark.cef", "")
	end 

	if (screenID == "force2") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.force, force_sensitive_dark.questString)
	end

	if (screenID == "force_part5") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.forcetwo, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Lord Whikul", "", 4995, 259, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end	
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end






--##############################################################################################################################################
--###################################################################  ROHAK	################################################################
--##############################################################################################################################################


force_sensitive_dark_rohak_conv_handler = Object:new {

}

function force_sensitive_dark_rohak_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local conversation = LuaConversationTemplate(conversationTemplate)
	local convosession = creature:getConversationSession()
	local hasAccepted = creature:hasScreenPlayState(force_sensitive_dark.states.quest.force, force_sensitive_dark.questString)
	local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.rohak, force_sensitive_dark.questString)
	local missionStart = creature:hasScreenPlayState(force_sensitive_dark.states.quest.mission, force_sensitive_dark.questString)
	local missionComp = creature:hasScreenPlayState(force_sensitive_dark.states.quest.missionComp, force_sensitive_dark.questString)
	local isJedi = creature:hasScreenPlayState(force_sensitive_dark.states.quest.jedi, force_sensitive_dark.questString)

	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then 
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then 
			if hasAccepted == false and hasComplete == false then
				nextConversationScreen = conversation:getScreen("rohak_not_yet")
			elseif hasAccepted == true and hasComplete == false then 
				nextConversationScreen = conversation:getScreen("rohak_first_screen")
			elseif hasComplete == true and missionStart == false then
				nextConversationScreen = conversation:getScreen("rohak_thank_you")
				if creature:hasSkill("fs_jedi_a_4") and creature:hasSkill("fs_jedi_b_4") and creature:hasSkill("fs_jedi_c_4") and creature:hasSkill("fs_jedi_d_4") and missionStart == false then
					nextConversationScreen = conversation:getScreen("rohak_holocron")
				end
			elseif missionComp == true and isJedi == false then
				nextConversationScreen = conversation:getScreen("jedi")
			else
				nextConversationScreen = conversation:getScreen("rohak_thank_you")			
			end	
		else	
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen
end


function force_sensitive_dark_rohak_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if (screenID == "rohak_complete") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.rohak, force_sensitive_dark.questString)
	elseif (screenID == "capture") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.mission, force_sensitive_dark.questString)
	elseif (screenID == "completelight") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.jedi, force_sensitive_dark.questString)
	elseif (screenID == "completedark3") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.dark, force_sensitive_dark.questString)
		player:sendSystemMessage("You have chosen your path and must now find your way Mellichae.")
		--local creo = LuaSceneObject(conversingPlayer)
		--creo:switchZone("dathomir", 5469, 78.5, -3996, 0)
	elseif (screenID == "completegrey") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.grey, force_sensitive_dark.questString)
	elseif (screenID == "leavejedi") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.quit, force_sensitive_dark.questString)
		giveItem(pInventory, "object/tangible/jedi/no_drop_jedi_holocron_dark.iff", -1)
	end

	return conversationScreen
end

force_sensitive_dark_lulzi_conv_handler = Object:new {

}

function force_sensitive_dark_lulzi_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.deftwo, force_sensitive_dark.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzione, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_2")  

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("lulzi_parttwo")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("lulzi_first_screen")
				elseif (hasComplete == true and hasSkill == false)then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("hello_screen")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_lulzi_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)	
	if (screenID == "lulzi4") then
			local pDroid = spawnMobile("yavin4", "sith_shadow_slave_two", 0, math.random(2) + 0.8, -47.4, -76.5, -88, 3435643)
			writeData("force_sensitive_dark:lulzi1",0)
			createObserver(DAMAGERECEIVED, "force_sensitive_dark", "slave_damage_two", pDroid)
       			createObserver(OBJECTDESTRUCTION, "force_sensitive_dark", "lulziOneDead", pDroid)	
	end

	if (screenID == "lulzi_parttwo") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzitwo, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Meet outside", "", 5136, 284, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive_dark:lulziOneDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive_dark:lulzi1") == 0 ) then

	        	writeData("force_sensitive_dark:lulzi1",1)
			creature:setScreenPlayState(force_sensitive_dark.states.quest.lulzione, force_sensitive_dark.questString)

		end
	     
     return 0
end

function force_sensitive_dark:slave_damage_two(creatureObject, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(creatureObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.99)) or (action <= (maxAction * 0.99)) or (mind <= (maxMind * 0.99))) and readData("fsdark:slavetwodamage") == 0) then
		spatialChat(creatureObject, "Let me out!")
		writeData("fsdark:slavetwodamage",1)
	elseif (((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) and readData("fsdark:slavetwodamage2") == 0) then
		spatialChat(creatureObject, "Oww, I am to weak to fight.")
		writeData("fsdark:slavetwodamage2",1)
	elseif ((health <= (maxHealth * 0.01)) or (action <= (maxAction * 0.01)) or (mind <= (maxMind * 0.01))) then
		spatialChat(creatureObject, "..free at last")
		writeData("fsdark:slavetwodamage",0)
		writeData("fsdark:slavetwodamage2",0)
	end

	return 0

end

force_sensitive_dark_lulzitwo_conv_handler = Object:new {

}

function force_sensitive_dark_lulzitwo_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzitwo, force_sensitive_dark.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzithree, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_3")  

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("turnin")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("lulzitwo_first_screen")
				elseif (hasComplete == true and hasSkill == false) then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_lulzitwo_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)	
	if (screenID == "fork4") then

		if(math.random(2) == 2) then
			local pKlik1 = spawnMobile("yavin4", "dark_quest", 0, 5310, 67, 214.0, -77, 0)

			local pKlik2 = spawnMobile("yavin4", "dark_quest", 0, 5319, 67, 208, -77, 0)

			local pKlik3 = spawnMobile("yavin4", "dark_quest", 0, 5313, 67, 226.0, -77, 0)
			writeData("force_sensitive_dark:klik3",0)
			createObserver(OBJECTDESTRUCTION, "force_sensitive_dark", "klikThreeDead", pKlik3)
			
			local pGhost = player:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Jedi", "", 5313, 226, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end
		else
			local pKlik1 = spawnMobile("yavin4", "dark_questt", 0, 5219, 82, 98.0, -47, 0)

			local pKlik2 = spawnMobile("yavin4", "dark_quest", 0, 5229, 82.9, 111.0, -33, 0)

			local pKlik3 = spawnMobile("yavin4", "dark_quest", 0, 5231, 82.3, 90.0, -26, 0)
			writeData("force_sensitive_dark:klik3",0)
			createObserver(OBJECTDESTRUCTION, "force_sensitive_dark", "klikThreeDead", pKlik3)

			local pGhost = player:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Jedi", "", 5231, 90, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end
		end	
	end

	if (screenID == "lulzi_parttwo") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzitwo, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Meet outside", "", -5502, 4849, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end

	if (screenID == "turnin") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzifour, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Captured Jedi", "", 4925, 568, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

function force_sensitive_dark:klikThreeDead(pOne, pKiller)
	local creature = LuaCreatureObject(pKiller)
	
		if (readData("force_sensitive_dark:klik3") == 0 ) then

	        	writeData("force_sensitive_dark:klik3",1)
			creature:setScreenPlayState(force_sensitive_dark.states.quest.lulzithree, force_sensitive_dark.questString)

			local pGhost = creature:getPlayerObject()
			if pGhost ~= nil then
			PlayerObject(pGhost):addWaypoint("yavin4", "Return", "", 5136, 284, 5, true, true, WAYPOINTTHEMEPARK, 1)
			end

		end
	     
     return 0
end

force_sensitive_dark_lulzithree_conv_handler = Object:new {

}

function force_sensitive_dark_lulzithree_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzifour, force_sensitive_dark.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzifive, force_sensitive_dark.questString)
			local hasCompleteThree = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzisix, force_sensitive_dark.questString)
			local hasSkill = creature:hasSkill("fs_jedi_a_4")  


				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteThree == true) then
					nextConversationScreen = conversation:getScreen("lulzi_returned")
				elseif (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("lulzi_waiting")			
				elseif (hasComplete == true and hasSkill == true) then
					nextConversationScreen = conversation:getScreen("lulzi_capturedjedi")
				elseif (hasComplete == true and hasSkill == false) then
					nextConversationScreen = conversation:getScreen("crystal_screen")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_lulzithree_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)	
	if (screenID == "lulzi_capturedjedi3") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzifive, force_sensitive_dark.questString)
	end

	if (screenID == "lulzi_returned4a") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulziseven, force_sensitive_dark.questString)
		local pGhost = player:getPlayerObject()
		if pGhost ~= nil then
		PlayerObject(pGhost):addWaypoint("yavin4", "Tikqot", "", 5035, 302, 5, true, true, WAYPOINTTHEMEPARK, 1)
		end
	end
	
	
	--print("returning convosvreen")
	return conversationScreen
end

force_sensitive_dark_capturedjedi_conv_handler = Object:new {

}

function force_sensitive_dark_capturedjedi_conv_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	--print("getNextConversation() called")
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		-- checking to see if we have a next screen
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	--print("casting getlastconversationsreen()")
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			--print("Last conversation is null.  let's try to get the first screen")
			
			local creature = LuaCreatureObject(conversingPlayer)
			local hasComplete = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzifive, force_sensitive_dark.questString)
			local hasCompleteTwo = creature:hasScreenPlayState(force_sensitive_dark.states.quest.lulzisix, force_sensitive_dark.questString)

				--### KEY CONVO PARTS, AND STATES ###--
				if (hasCompleteTwo == true) then
					nextConversationScreen = conversation:getScreen("lulzi_waiting")			
				elseif (hasComplete == true) then
					nextConversationScreen = conversation:getScreen("capturedjedi_one")
				else
					nextConversationScreen = conversation:getScreen("complete")
				end			
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			--print("optionLink fromn the last screen is .. " .. optionLink)		
			--local thiscreen = conversation:getScreen(optionLink)
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end	
	--print("returning screen")		
	return nextConversationScreen	
end

function force_sensitive_dark_capturedjedi_conv_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	--print("\ntest_convo_handler:runScreenHandlers() called\n")
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	--print("screenID is " .. screenID	)
	local player = LuaCreatureObject(conversingPlayer)
	local completed = player:getScreenPlayState(force_sensitive_dark.states.quest.craft, force_sensitive_dark.questString)
	local creature = LuaCreatureObject(conversingNPC)	
	if (screenID == "capturedjedi_3a") then
		player:playEffect("clienteffect/pl_force_heal_self.cef", "")
	end

	if (screenID == "capturedjedi_3b") then
		player:playEffect("clienteffect/pl_force_heal_dark.cef", "")
	end

	if (screenID == "capturedjedi_4a") then
		creature:playEffect("clienteffect/pl_force_absorb_self.cef", "")
	end

	if(screenID == "capturedjedi_5a") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzisix, force_sensitive_dark.questString)
	end

	if(screenID == "capturedjedi_5b") then
		player:setScreenPlayState(force_sensitive_dark.states.quest.lulzisix, force_sensitive_dark.questString)
	end

	--if(screenID == "capturedjedi_5c") then
	--	player:setScreenPlayState(force_sensitive_dark.states.quest.lulzisix, force_sensitive_dark.questString)
	--end
	
	--print("returning convosvreen")
	return conversationScreen
end


