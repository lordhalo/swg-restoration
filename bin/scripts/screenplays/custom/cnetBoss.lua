local ObjectManager = require("managers.object.object_manager")

cnetBoss = ScreenPlay:new {
	numberOfActs = 1,
}
registerScreenPlay("cnetBoss", true)

function cnetBoss:start()
	if (isZoneEnabled("corellia")) then
		self:spawnMobiles()
		self:spawnSceneObjects()
	end
end

function cnetBoss:spawnSceneObjects()
	spawnSceneObject("corellia", "object/mobile/vehicle/speederbike_swoop.iff", -293, 28, -4844, 0, 1, 0, 0, 0)
	spawnSceneObject("corellia", "object/mobile/vehicle/speederbike_swoop.iff", -290, 28, -4844, 0, 1, 0, 0, 0)
	spawnSceneObject("corellia", "object/mobile/vehicle/speederbike.iff", -287, 28, -4844, 0, 1, 0, 0, 0)
	spawnSceneObject("corellia", "object/mobile/vehicle/speederbike_swoop.iff", -284, 28, -4844, 0, 1, 0, 0, 0)
	spawnSceneObject("corellia", "object/mobile/vehicle/flare_s_swoop_crafted.iff", -281, 28, -4844, 0, 1, 0, 0, 0)
end

function cnetBoss:spawnMobiles()
	
	local bossNpc = spawnMobile("corellia", "bikerBoss", 10800, -293, 28, -4848,  178, 0)
	createObserver(DAMAGERECEIVED, "cnetBoss", "npcDamageObserver", bossNpc)
	--createObserver(OBJECTINRANGEMOVED, "cnetBoss", "MovedAway", bossNpc)
	--createObserver(DEFENDERADDED, "cnetBoss", "attackerAdded", bossNpc)
end

function cnetBoss:MovedAway(creatureObject, movingCreature)
	local boss = LuaSceneObject(creatureObject)
	
	local movedObject = LuaSceneObject(movingCreature)

	local spawnEgg = spawnSceneObject("corellia", "object/tangible/spawning/spawn_egg.iff", -293, 18, -4848, 0, 1, 0, 0, 0)
	
	if movedObject:isCreatureObject() == false then
		return 0	
	end
	
	local player = LuaCreatureObject(movingCreature)
	
	distance = boss:getDistanceTo(spawnEgg)
	
	if distance == 0 then
		return 0
	end
	
	if distance > 68 then
			CreatureObject(creatureObject):clearCombatState(1)
			AiAgent(creatureObject):setOblivious()
		return 0
	else
		return 0
	end
end


function cnetBoss:npcDamageObserver(bossObject, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)

	if (((health <= (maxHealth * 0.99)) or (action <= (maxAction * 0.99)) or (mind <= (maxMind * 0.99))) and readData("bikerBoss:npcDamaged") == 0) then
		spatialChat(bossObject, "You picked a fight with the wrong guy!")
		spatialChat(bossObject, "Jump this fool!")
		self:spawnGuards(playerObject)
		local targetID = boss:getTargetID()
		--self:updateTargetID(targetID)
		writeData("bikerBoss:npcDamaged",1)
	elseif (((health <= (maxHealth * 0.6)) or (action <= (maxAction * 0.6)) or (mind <= (maxMind * 0.6))) and readData("bikerBoss:npcDamagedTwo") == 0) then
		spatialChat(bossObject, "Franz, Get out here and throw the gas!")
		writeData("bikerBoss:npcDamagedTwo",1)
		self:spawnFranz(playerObject)
	elseif (((health <= (maxHealth * 0.4)) or (action <= (maxAction * 0.4)) or (mind <= (maxMind * 0.4))) and readData("bikerBoss:npcDamagedThree") == 0 and readData("bikerBoss:franzAlive") == 0) then
		spatialChat(bossObject, "Damnit Franz!")
		writeData("bikerBoss:npcDamagedThree",1)
	elseif ((health <= (maxHealth * 0.001)) or (action <= (maxAction * 0.001)) or (mind <= (maxMind * 0.001))) then
		spatialChat(bossObject, "...Just don't scratch my Speeder..")
		createEvent(60, "cnetBoss", "clearEvent", bossObject, "")
	end

	return 0

end

function cnetBoss:clearEvent()
	writeData("bikerBoss:npcDamaged",0)
	writeData("bikerBoss:npcDamagedTwo",0)
	writeData("bikerBoss:npcDamagedThree",0)
end

function cnetBoss:updateTargetID(target)
	local targetID = target
	writeData("bikerBoss:targetID",target)
end

function cnetBoss:getTargetID()
	return readData("bikerBoss:targetID")
end

function cnetBoss:spawnFranz(playerObject)

	local franzNpc = spawnMobile("corellia", "bikerThugFranz", 0, -253, 28, -4826,  178, 0)
	createObserver(OBJECTDESTRUCTION, "cnetBoss", "franzDead", franzNpc)
	writeData("bikerBoss:franzAlive",1)
	spatialChat(franzNpc, "Sorry Boss! You know I can't resist a cheap trick")
	CreatureObject(franzNpc):engageCombat(playerObject)
	createEvent(2 * 60 * 1000, "cnetBoss", "poisonEvent", playerObject, "")

end

function cnetBoss:spawnGuards(playerObject)
	--local player = LuaCreatureObject(playerObject)

	local thugOne = spawnMobile("corellia", "bikerThug", 0, -290, 28, -4822,  178, 0)
	local thugTwo = spawnMobile("corellia", "bikerThug", 0, -287, 28, -4822,  178, 0)
	local thugThree = spawnMobile("corellia", "bikerThug", 0, -284, 28, -4822,  178, 0)

	CreatureObject(thugOne):engageCombat(playerObject)
	CreatureObject(thugTwo):engageCombat(playerObject)
	CreatureObject(thugThree):engageCombat(playerObject)

	--AiAgent(thugOne):stopWaiting()
	--AiAgent(thugOne):setWait(0)
	--AiAgent(thugOne):setNextPosition(-272, 28, -4851, 0)
	--AiAgent(thugOne):executeBehavior()
	

end

function cnetBoss:franzDead()
	writeData("bikerBoss:franzAlive",0)

	return 1
end

function cnetBoss:poisonEvent(playerObject)
	
	if (readData("bikerBoss:franzAlive") == 1) then
		self:spawnGasCloud(playerObject)
	else
		return 0
	end

end

function cnetBoss:spawnGasCloud(playerObject)
	local player = LuaCreatureObject(playerObject)

	local pGasArea = spawnActiveArea("corellia", "object/active_area.iff", player:getPositionX(), player:getPositionZ(), player:getPositionY(), 20, 0)
	if pGasArea ~= nil then
		createObserver(OBJECTINRANGEMOVED, "cnetBoss", "notifyEnteredPoisonGas", pGasArea)
	end
	createEvent(30000, "cnetBoss", "despawnPoison", pGasArea, "")

	local cloud = spawnSceneObject("corellia", "object/static/particle/pt_miasma_of_fog_greenish.iff", player:getPositionX(), player:getPositionZ(), player:getPositionY(), 0, 0, 0, 0, 0)

	createEvent(30000, "cnetBoss", "despawnPoison", cloud, "")
end

function cnetBoss:notifyEnteredPoisonGas(pActiveArea, pMovingObject)
	local object = LuaSceneObject(pActiveArea)
	if (pMovingObject == nil or pActiveArea == nil or not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	if (CreatureObject(pMovingObject):isAiAgent() and not AiAgent(pMovingObject):isPet()) then
		return 0
	end

	distance = object:getDistanceTo(pMovingObject)
	hasState = readData(CreatureObject(pMovingObject):getObjectID() .. ":cnetBoss:hasPoison")

	if (distance < 20 and hasState == 0) then

	local objectID = CreatureObject(pMovingObject):getObjectID()

	writeData(objectID .. ":cnetBoss:hasPoison", 1)
	createEvent(2 * 60 * 1000, "cnetBoss", "removePoisonEvent", pMovingObject, "")

	if (self:hasRebreather(pMovingObject)) then
		CreatureObject(pMovingObject):sendSystemMessage("@dungeon/geonosian_madbio:gasmask") --Your gasmask diffuses the poison gas and you are able to breathe with no difficulty.
	else
		local areaID = SceneObject(pActiveArea):getObjectID()

		CreatureObject(pMovingObject):addDotState(pMovingObject, POISONED, getRandomNumber(100) + 500, HEALTH, 1000, 2000, areaID, 0)
		
		CreatureObject(pMovingObject):sendSystemMessage("@dungeon/geonosian_madbio:toxic_fumes") --You breathe in toxic fumes!
	end

	end
	return 0
end

function cnetBoss:despawnPoison(pGasCloud)

	local gasCloud = LuaSceneObject(pGasCloud)

	gasCloud:switchZone("character_farm", -200, 0, -200, 0)
	gasCloud:destroyObjectFromWorld()
	
end

function cnetBoss:removePoisonEvent(pMovingObject)
	writeData(CreatureObject(pMovingObject):getObjectID() .. ":cnetBoss:hasPoison", 0)
end

function cnetBoss:hasRebreather(pCreature)
	if (pCreature == nil) then
		return false
	end

	--TODO: Change this to be a skill mod check for private_poison_rebreather
	local pRebreather = CreatureObject(pCreature):getSlottedObject("eyes")

	if pRebreather == nil then
		return false
	end

	local headSlot = SceneObject(pRebreather):getTemplateObjectPath()
	return (headSlot == "object/tangible/wearables/goggles/rebreather.iff" or headSlot == "object/tangible/wearables/armor/mandalorian/armor_mandalorian_helmet.iff")
end

