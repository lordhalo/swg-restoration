local ObjectManager = require("managers.object.object_manager")

DarkOrLight = ScreenPlay:new {
	numberOfActs = 1,
	questString = "DarkOrLight",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	rumor_npcs = {
		{"patron", 60, -11.5, -0.9, -21.0, 90, 4265380, "durbin1"},
		{"farmer", 60, -8.5, -0.9, -21.0, -90, 4265380, "durbin2"},
	},

	rumortwo_npcs = {
		{"commoner_fat", 60, -3.5, -0.9, 16.5, 0, 3175397, "detainment1"},
		{"businessman", 60, -3.5, -0.9, 19.5, -180, 3175397, "detainment2"},
	},

}
registerScreenPlay("DarkOrLight", true)

function DarkOrLight:start()
	if (isZoneEnabled("talus")) then
		self:spawnNpc()
		self:spawnRumorNpc()
		self:spawnDurbinHint()
		self:spawnPadawan()
	end
end

function DarkOrLight:spawnRumorNpc()

	for i,v in ipairs(self.rumor_npcs) do
		local pMobile = spawnMobile("talus", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
		if (pMobile ~= nil) then
			writeStringData(SceneObject(pMobile):getObjectID() .. ":name", v[8])
			if(v[8] == "durbin1") then
				createEvent(60000, "DarkOrLight", "rumorTalk", pMobile, "")
			elseif (v[8] == "durbin2") then
				createEvent(65000, "DarkOrLight", "rumorTalk", pMobile, "")
			end
		end
	end

	for i,v in ipairs(self.rumortwo_npcs) do
		local pMobile = spawnMobile("talus", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
		if (pMobile ~= nil) then
			writeStringData(SceneObject(pMobile):getObjectID() .. ":name", v[8])
			if(v[8] == "detainment1") then
				createEvent(60000, "DarkOrLight", "rumorTalk", pMobile, "")
			elseif (v[8] == "detainment2") then
				createEvent(65000, "DarkOrLight", "rumorTalk", pMobile, "")
			end
		end
	end

end

function DarkOrLight:rumorTalk(pNpc)
	if (pNpc == nil) then
		return
	end

	local name = readStringData(SceneObject(pNpc):getObjectID() .. ":name")
	local random = math.random(5)

	if(name == "durbin1") then
		if (random > 1) then
			spatialChat(pNpc, "Hey did you hear about that Sith who wiped out the Village?")
			createEvent(120000, "DarkOrLight", "rumorTalk", pNpc, "")
			writeData("dlconvo:flip", 1)
		else
			spatialChat(pNpc, "My pal told me he shared a Drink with a Clone trooper in Dearic")
			createEvent(120000, "DarkOrLight", "rumorTalk", pNpc, "")
			writeData("dlconvo:flip", 0)
		end
	end

	if(name == "durbin2") then
		if (readData("dlconvo:flip") == 1) then
			spatialChat(pNpc, "Yes, Very unsettling. The entire Village gone overnight.")
			createEvent(123000, "DarkOrLight", "rumorTalk", pNpc, "")
		else
			spatialChat(pNpc, "What? Thats impossible. All the Clones are dead")
			createEvent(123000, "DarkOrLight", "rumorTalk", pNpc, "")
		end
	end

	if(name == "detainment1") then
		if (random > 1) then
			spatialChat(pNpc, "The breach at the Detainment Center has the whole planet on edge, How did this happen?")
			createEvent(120000, "DarkOrLight", "rumorTalk", pNpc, "")
		else
			spatialChat(pNpc, "That Swoop Gang on Corellia is causing a lot of problems.")
			createEvent(120000, "DarkOrLight", "rumorTalk", pNpc, "")
		end
	end

	if(name == "detainment2") then
		if (readData("dlconvo:flip") == 1) then
			spatialChat(pNpc, "Im not sure but its costing me Money! My supply transports don't want to dock here.")
			createEvent(123000, "DarkOrLight", "rumorTalk", pNpc, "")
		else
			spatialChat(pNpc, "The Leader Bracks owes me Money! I sold him that Swoop years ago!")
			createEvent(123000, "DarkOrLight", "rumorTalk", pNpc, "")
		end
	end


end

function DarkOrLight:spawnNpc()

	local pNpc = spawnMobile("talus", "erym_kafialp", 300, 4234, 6, 1061,  -100, 0) --Durbin (Dark Rumors)
	local pLok = spawnMobile("lok", "erym_kafialp", 300, 2877, 304, -4731,  70, 0) --Durbin (Dark Rumors)
	createObserver(OBJECTDESTRUCTION, "DarkOrLight", "masterDead", pLok)

end

function DarkOrLight:spawnPadawan()
	local random = math.random(3)

	if (random == 3) then
		local pNpc = spawnMobile("yavin4", "oowe_nonage", 300, -0.0, -1.3, -25.5, 0, 468319)
		createObserver(OBJECTDESTRUCTION, "DarkOrLight", "padawanDead", pNpc)
		createEvent(7200 * 1000, "DarkOrLight", "despawnPadawan", pNpc, "")
	elseif(random == 2) then
		local pNpc = spawnMobile("yavin4", "oowe_nonage", 300, 902, 107, -590, 180, 0)
		createObserver(OBJECTDESTRUCTION, "DarkOrLight", "padawanDead", pNpc)
		createEvent(7200 * 1000, "DarkOrLight", "despawnPadawan", pNpc, "")
	else
		local pNpc = spawnMobile("yavin4", "oowe_nonage", 300, 5206, 99, 5544,  -100, 0)
		createObserver(OBJECTDESTRUCTION, "DarkOrLight", "padawanDead", pNpc)
		createEvent(7200 * 1000, "DarkOrLight", "despawnPadawan", pNpc, "")
	end
end

function DarkOrLight:despawnPadawan(pMobile)
	if (pMobile == nil) then
		return
	end

	if pMobile ~= nil then
		SceneObject(pMobile):destroyObjectFromWorld()
		createEvent(6000, "DarkOrLight", "spawnPadawan", nil, "")
	end

	return 0
end

function DarkOrLight:padawanDead(pMobile, playerObject)
	if (pMobile == nil) then
		return
	end

	local player = LuaCreatureObject(playerObject)

	if player ~= nil then
		local hasState = player:hasScreenPlayState(16, "DarkOrLight")	
			if(hasState == true) then 
				player:sendSystemMessage("Find Master Erym..")
				player:setScreenPlayState(32, DarkOrLight.questString)
			end
	end
	return 0
end

function DarkOrLight:masterDead(pMobile, playerObject)
	if (pMobile == nil) then
		return
	end

	local player = LuaCreatureObject(playerObject)
	local pInventory = player:getSlottedObject("inventory")

	if player ~= nil then
		local hasState = player:hasScreenPlayState(32, "DarkOrLight")	
			if(hasState == true) then 
				player:setScreenPlayState(64, DarkOrLight.questString)
				KnightTrials:handleCouncilChoice(playerObject, 0)
				createLoot(pInventory, "krayt_pearls", 143, true)
				CreatureObject(playerObject):sendSystemMessage("@theme_park/messages:theme_park_reward")
			end
	end
	return 0
end

function DarkOrLight:lightEnding(playerObject)
	if (playerObject == nil) then
		return
	end

	local player = LuaCreatureObject(playerObject)
	local pInventory = player:getSlottedObject("inventory")

	if player ~= nil then
		local hasState = player:hasScreenPlayState(32, "DarkOrLight")	
			if(hasState == true) then 
				player:setScreenPlayState(64, DarkOrLight.questString)
				KnightTrials:handleCouncilChoice(playerObject, 1)
				local pCrystal = giveItem(pInventory, "object/tangible/component/weapon/lightsaber/lightsaber_module_force_crystal.iff", -1, true)

				if (pCrystal == nil) then
					CreatureObject(playerObject):sendSystemMessage("Error: Unable to generate item. Report to Halo")
				else
					local colorCrystal = LuaLightsaberCrystalComponent(pCrystal)
					colorCrystal:setColor(26)
					colorCrystal:updateCrystal(26)
				end
			end
	end
	return 0
end

function DarkOrLight:spawnDurbinHint()
	local pOne = spawnSceneObject("talus","object/tangible/wearables/necklace/necklace_s09.iff", 4119.5, 4.5, 929.2, 0, 0.0707,0,0.0707,0)
		if (pOne ~= nil) then
			SceneObject(pOne):setCustomObjectName("a Damaged necklace with 'F' visible")
		end

	local pTwo = spawnSceneObject("talus","object/tangible/sign/all_sign_city_s03.iff", 4273.5, 6.0, 1052.5, 0, 0.0707,0,0.0707,0)
		if (pTwo ~= nil) then
			SceneObject(pTwo):setCustomObjectName("Si...")
		end

	spawnSceneObject("talus","object/weapon/ranged/rifle/rifle_t21.iff", 4230.5, 5.9, 982.5, 0, 0.0707,0,0.0707,0)
	

	spawnSceneObject("talus","object/tangible/loot/misc/holocron_splinters_sith_s01.iff", -1.6, 0.8, 2.0, 2735675, 0,0,0,0)
	spawnSceneObject("talus","object/tangible/loot/misc/loot_skull_krayt1.iff", -1.6, 0.5, 2.1, 2735675, 0,0,0.9,0)

	local pDnaOne = spawnSceneObject("talus","object/tangible/component/dna/dna_sample_very_high.iff", -1.5, 0.1, 4.7, 2735647, 0,0,0,0)
		if (pDnaOne ~= nil) then
			SceneObject(pDnaOne):setCustomObjectName("High Quality Kahmurra DNA")
		end

	local pDnaTwo = spawnSceneObject("talus","object/tangible/component/dna/dna_sample_very_high.iff", -1.5, 0.1, 3.1, 2735647, 0,0,0,0)
		if (pDnaTwo ~= nil) then
			SceneObject(pDnaTwo):setCustomObjectName("High Quality Kahmurra DNA")
		end

	local pDnaThree = spawnSceneObject("talus","object/tangible/component/dna/dna_sample_very_high.iff", 1.1, 0.1, 4.7, 2735647, 0,0,0,0)
		if (pDnaThree ~= nil) then
			SceneObject(pDnaThree):setCustomObjectName("High Quality Kahmurra DNA")
		end


	local pBodysuit = spawnSceneObject("talus","object/tangible/wearables/bodysuit/bodysuit_s13.iff", 4301.6, 4.7, 1005.5, 0, 0, 0, 0, 0.5)
		if (pBodysuit ~= nil) then
			SceneObject(pBodysuit):setCustomObjectName("Detainment Center bodysuit")
		end

end

dark_or_light_convo_handler = Object:new {
	
 }

function dark_or_light_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local completed = creature:hasScreenPlayState(64, "DarkOrLight")
			local padawanDead = creature:hasScreenPlayState(32, "DarkOrLight")
			local clueClose = creature:hasScreenPlayState(8, "DarkOrLight")
			local hasTrack = creature:hasScreenPlayState(4, "DarkOrLight")
			local hasTrials = creature:hasScreenPlayState(2, "DarkOrLight")

				if (completed == true) then
					nextConversationScreen = conversation:getScreen("go_away")
				elseif (padawanDead == true) then
					nextConversationScreen = conversation:getScreen("phasetwo_screen_one")
				elseif (clueClose == true) then
					nextConversationScreen = conversation:getScreen("clue_close")
				elseif (hasTrack == true) then
					nextConversationScreen = conversation:getScreen("tracking_screen")
				elseif (hasTrials == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function dark_or_light_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "screen_five" ) then
		player:setScreenPlayState(4, DarkOrLight.questString)

	end

	if ( screenID == "clue_close" ) then
		player:setScreenPlayState(8, DarkOrLight.questString)

	end

	if ( screenID == "sith_close" ) then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer)
	end

	if ( screenID == "light_five" ) then
		createEvent(10000, "DarkOrLight", "lightEnding", conversingPlayer, "")
	end

	return conversationScreen
end

oowe_nonage_convo_handler = Object:new {
	
 }

function oowe_nonage_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local completePhase = creature:hasScreenPlayState(32, "DarkOrLight")
			local readyToTalk = creature:hasScreenPlayState(16, "DarkOrLight")
				
				if (completePhase == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")
				elseif (readyToTalk == true) then
					nextConversationScreen = conversation:getScreen("greet_player")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function oowe_nonage_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)	
	local pInventory = player:getSlottedObject("inventory")
	local questNpc = LuaCreatureObject(conversingNPC)

	if ( screenID == "screen_three" ) then
		questNpc:doAnimation("slump_head")
	end

	if ( screenID == "screen_five_dark" ) then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer)
	end

	if ( screenID == "screen_seven_light" ) then
		CreatureObject(conversingNPC):clearOptionBit(CONVERSABLE) -- Set to only Ai.
		CreatureObject(conversingNPC):setPvpStatusBitmask(1) -- Make attackable.
		CreatureObject(conversingNPC):engageCombat(conversingPlayer)
	end

	return conversationScreen
end
