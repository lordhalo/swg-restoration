local ObjectManager = require("managers.object.object_manager")

LokGreatMaze = ScreenPlay:new {}

function LokGreatMaze:start(pPlayer)
	if (isZoneEnabled("lok")) then
		self:spawnActiveArea()
	end
end

function LokGreatMaze:spawnActiveArea()

	local pMazeCenter = spawnActiveArea("lok", "object/active_area.iff", 3826, 63, -541, 3, 0)
	if pMazeCenter ~= nil then
		createObserver(OBJECTINRANGEMOVED, "LokGreatMaze", "enterMiddleMaze", pMazeCenter)
	end

end

function LokGreatMaze:enterMiddleMaze(pActiveArea, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end

	player:sendSystemMessage("Ok")

	return 0

end
