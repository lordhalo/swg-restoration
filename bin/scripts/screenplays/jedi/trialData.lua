TRIAL_LIGHTSABER = 1
TRIAL_TALK = 2
TRIAL_KILL = 3
TRIAL_HUNT = 4
TRIAL_HUNT_FACTION = 5
TRIAL_COUNCIL = 6

padawanTrialQuests = {
	{
		trialName = "architect",
		trialType = TRIAL_TALK,
		trialNpc = "devaronian_male",
		trialNpcName = "Kant Graf (an architect)",
		targetNpc = "gungan_captain",
		targetKillable = true
	},
	{
		trialName = "artist",
		trialType = TRIAL_TALK,
		trialNpc = "bestine_rumor12",
		trialNpcName = "Sathme Forr (an artist)",
		targetNpc = "commoner",
		targetKillable = false
	},
	{
		trialName = "bad_cat",
		trialType = TRIAL_KILL,
		trialNpc = "bestine_capitol02",
		trialNpcName = "Yvana Bailer (an actor)",
		targetNpc = "bloodstained_prowler",
		targetKillable = true
	},
	{
		trialName = "chef",
		trialType = TRIAL_TALK,
		trialNpc = "dannik_malaan",
		trialNpcName = "Seevi Nyed (a chef)",
		targetNpc = "neo_cobral_overlord",
		targetKillable = true
	},
	{
		trialName = "craft_lightsaber",
		trialType = TRIAL_LIGHTSABER,
	},
	{
		trialName = "kill_baz_nitch",
		trialType = TRIAL_HUNT,
		trialNpc = "sullustan_male",
		trialNpcName = "Menchi (an environmentalist)",
		huntTarget = "baz_nitch",
		huntGoal = 20
	},
	{
		trialName = "kill_falumpaset",
		trialType = TRIAL_HUNT,
		trialNpc = "irenez",
		trialNpcName = "Braganta (a naturalist)",
		huntTarget = "falumpaset",
		huntGoal = 20
	},
	{
		trialName = "kill_sludge_panther",
		trialType = TRIAL_HUNT,
		trialNpc = "kima_nazith",
		trialNpcName = "Luha Kellaro (an ecologist)",
		huntTarget = "sludge_panther",
		huntGoal = 20
	},
	{
		trialName = "old_musician",
		trialType = TRIAL_TALK,
		trialNpc = "grobber",
		trialNpcName = "Grizzlo (a retired musician)",
		targetNpc = nil,
		targetLoc = { 3468, 5, -4852, "tatooine" },
		thirdTargetNpc = nil,
		thirdTargetLoc = { 469, 12, 5021, "lok" }
	},
	{
		trialName = "pannaqa",
		trialType = TRIAL_TALK,
		trialNpc = nil,
		trialLoc = { 5291.3, 78.5, -4037.8, "dathomir", "Aurilia" },
		targetNpc = "commoner",
		targetKillable = false,
		thirdTargetNpc = "commoner_male",
		thirdTargetName = "Shendo",
		thirdTargetKillable = false
	},
	{
		trialName = "peoples_soldier",
		trialType = TRIAL_KILL,
		trialNpc = "marco_vahn",
		trialNpcName = "Torin Gundo (an old soldier)",
		targetNpc = "brigand_leader",
		targetKillable = true
	},
	{
		trialName = "politician",
		trialType = TRIAL_KILL,
		trialNpc = "dorn_gestros",
		trialNpcName = "Kaul Dysen (a politician)",
		targetNpc = "bloodseeker_mite",
		targetKillable = true
	},
	{
		trialName = "sob_story",
		trialType = TRIAL_TALK,
		trialNpc = "karena_keer",
		trialNpcName = "Erim Thelcar",
		targetNpc = "object/tangible/jedi/padawan_trials_skeleton.iff",
		targetNpcName = "The remains of Josef Thelcar",
		targetKillable = false
	},
	{
		trialName = "spice_mom",
		trialType = TRIAL_TALK,
		trialNpc = "bestine_rumor10",
		trialNpcName = "Sola Nosconda",
		targetNpc = "devaronian_male",
		targetNpcName = "Evif Sulp",
		targetKillable = false
	},
	{
		trialName = "surveyor",
		trialType = TRIAL_KILL,
		trialNpc = "bestine_rumor08",
		trialNpcName = "Par Doiae (a surveyor)",
		targetNpc = "sharnaff_bull",
		targetKillable = true
	},
	{
		trialName = "the_ring",
		trialType = TRIAL_TALK,
		trialNpc = "giaal_itotr",
		trialNpcName = "Keicho",
		targetNpc = "dread_pirate",
		killMessage = "@jedi_trials:padawan_trials_received_the_ring",
		targetKillable = true
	},
}

knightTrialQuests = {
	{
		trialName = "bol_stamina",
		trialType = TRIAL_HUNT,
		huntTarget = "bol;lesser_plains_bol",
		huntGoal = 200
	},
	{
		trialName = "ancient_bull_rancor",
		trialType = TRIAL_HUNT,
		huntTarget = "ancient_bull_rancor",
		huntGoal = 3
	},
	{
		trialName = "giant_dune_kimogila",
		trialType = TRIAL_HUNT,
		huntTarget = "giant_dune_kimogila",
		huntGoal = 2
	},
	{
		trialName = "krayt_dragon_ancient",
		trialType = TRIAL_HUNT,
		huntTarget = "krayt_dragon_ancient",
		huntGoal = 1
	},
	{
		trialName = "geonosian_bunker_acklay",
		trialType = TRIAL_HUNT,
		huntTarget = "geonosian_acklay_bunker_boss",
		huntGoal = 1
	},
	{
		trialName = "light_or_dark",
		trialType = TRIAL_COUNCIL
	},
	{
		trialName = "enemy_soldier",
		trialType = TRIAL_HUNT_FACTION,
		rebelTarget = "storm_commando",
		imperialTarget = "rebel_commando",
		huntGoal = 47
	},
	{
		trialName = "high_general",
		trialType = TRIAL_HUNT_FACTION,
		rebelTarget = "imperial_surface_marshal;imperial_high_general;imperial_general",
		imperialTarget = "rebel_high_general;rebel_surface_marshal;rebel_general",
		huntGoal = 22
	},
	{
		trialName = "corvette_officer",
		trialType = TRIAL_HUNT_FACTION,
		rebelTarget = "stormtrooper_novatrooper_elite_commander",
		imperialTarget = "corvette_rebel_rear_admiral",
		huntGoal = 4
	},
	{
		trialName = "restuss_pvp",
		trialType = TRIAL_HUNT_FACTION,
		rebelTarget = "dark_trooper_restuss",
		imperialTarget = "rebel_commando_restuss",
		huntGoal = 5
	},
}

trialsCivilizedPlanets = { "corellia", "naboo", "rori", "talus", "tatooine" }

trialsCivilizedPlanetCities = {
	corellia = { "coronet", "tyrena", "kor_vella", "doaba_guerfel", "bela_vistal" },
	naboo = { "theed", "moenia", "keren", "kaadara", "deeja_peak" },
	rori = { "narmle"},
	talus = { "dearic", "nashal" },
	tatooine = { "bestine", "mos_espa", "mos_eisley", "mos_entha" }
}

-- x, y, radius
trialsCivilizedNpcSpawnPoints = {
	corellia = {
		coronet = {
			{ 134, -4616, 5 },
			{ 4, -4441, 1 },
			{ -13, -4255, 1 },
			{ -205, -4086, 5 }
		},
		tyrena = {
			{ -3238, -2407, 5 },
			{ -5515, -2646, 3 }
		},
		kor_vella = {
			{ -3111, 2785, 5 },
			{ -3036, 2842, 5 },
			{ -3108, 2992, 5 },
			{ -3260, 3125, 3 },
			{ -3427, 3125, 5 },
			{ -3613, 3213, 5 }
		},
		doaba_guerfel = {
			{ 3325, 5504, 5 },
			{ 3165, 5351, 3 },
			{ 3147, 5149, 1 },
			{ 3180, 4985, 3 }
		},
		bela_vistal = {
			{ 6707, -5894, 5 },
			{ 6793, -5733, 5 },
			{ 6712, -5574, 1 },
			{ 6929, -5551, 1 }
		}
	},

	naboo = {
		theed = {
			{ -4675, 3970, 50 },
			{ -4607, 4110, 40 },
			{ -4926, 4027, 40 },
			{ -5173, 4225, 40 },
			{ -5426, 4150, 75 },
			{ -5538, 4307, 60 },
			{ -5948, 4308, 50 },
			{ -5801, 4131, 45 }
		},
		moenia = {
			{ 4892, -4842, 50 },
			{ 4961, -4948, 40 },
			{ 4755, -4948, 60 },
			{ 4698, -4889, 30 }
		},
		keren = {
			{ 1677, 2950, 20 },
			{ 1714, 2664, 75 },
			{ 1780, 2519, 60 },
			{ 1934, 2711, 50 },
			{ 1986, 2479, 60 }
		},
		kaadara = {
			{ 5138, 6630, 40 },
			{ 4987, 6761, 5 },
			{ 5164, 6776, 60 }
		},
		deeja_peak = {
			{ 5114, -1503, 30 },
			{ 5027, -1444, 30 },
			{ 4959, -1517, 30 }
		}
	},

	rori = {
		narmle = {
			{ -5361, -2087, 2 },
			{ -5412, -2238, 2 },
			{ -5215, -2252, 2 },
			{ -5067, -2303, 2 },
			{ -5151, -2461, 2 }
		}
	},

	talus = {
		dearic = {
			{ 202, -2846, 2 },
			{ 195, -3040, 2 },
			{ 435, -3063, 2 },
			{ 526, -2946, 2 },
			{ 667, -3066, 2 }
		},
		nashal = {
			{ 4364, 5340, 2 },
			{ 4491, 5188, 2 },
			{ 4365, 5163, 2 },
			{ 4223, 5132, 2 },
			{ 4095, 5280, 2 }
		}
	},

	tatooine = {
		bestine = {
			{ -1401, -3725, 2 },
			{ -1344, -3911, 2 },
			{ -1223, -3568, 2 },
			{ -1095, -3584, 2 },
			{ -1064, -3692, 2 }
		},
		mos_espa = {
			{ -2896, 2009, 2 },
			{ -3057, 2161, 2 },
			{ -2905, 2340, 2 }
		},
		mos_eisley = {
			{ 3504, -4863, 2 },
			{ 3457, -4955, 2 },
			{ 3387, -4715, 2 }
		},
		mos_entha = {
			{ 1332, 3107, 2 },
			{ 1352, 3259, 2 },
			{ 1413, 3341, 2 },
			{ 1507, 3097, 2 },
			{ 1702, 3090, 2 }
		}
	}
}
