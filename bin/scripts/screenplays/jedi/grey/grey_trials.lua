local ObjectManager = require("managers.object.object_manager")

greyJediTrials = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "greyJediTrials",

}
registerScreenPlay("greyJediTrials", true)

function greyJediTrials:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end


--Mobile Spawning
function greyJediTrials:spawnMobiles()
	local pNpc = spawnMobile("naboo", "grey_recruiter", 1, -4900, 6, 4110,  35, 0)

end

grey_trials_convo_handler = Object:new {
	
 }

function grey_trials_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local hasKnight = CreatureObject(conversingPlayer):hasSkill("force_title_jedi_rank_03")
			local hasSith = CreatureObject(conversingPlayer):hasSkill("rank_sith_novice")
			local hasLight = CreatureObject(conversingPlayer):hasSkill("rank_light_novice")

				if (hasSith == true or hasLight == true) then
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				elseif (hasKnight == true) then
					nextConversationScreen = conversation:getScreen("grey_intro_screen")
				else
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function grey_trials_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()
	local questNPC = LuaCreatureObject(conversingNPC)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "grey_trial_done" ) then

		--PlayerObject(pGhost):addWaypoint(enclaveLoc[3], enclaveName, "", enclaveLoc[1], enclaveLoc[2], WAYPOINTYELLOW, true, true, 0)
		PlayerObject(pGhost):setJediState(2)
		PlayerObject(pGhost):setFrsCouncil(4)
		PlayerObject(pGhost):setFrsRank(0)
		awardSkill(conversingPlayer, "rank_grey_novice")
		--CreatureObject(pPlayer):setFactionStatus(2) -- Overt
		--CreatureObject(pPlayer):setFaction(setFactionVal)

		--local sui = SuiMessageBox.new("JediTrials", "emptyCallback") -- No callback
		--sui.setTitle("@jedi_trials:knight_trials_title")
		--sui.setPrompt(unlockString)
		--sui.sendTo(conversingPlayer)
	end


	return conversationScreen
end
