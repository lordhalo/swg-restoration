TalusErranSifScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "TalusErranSifScreenPlay",

	lootContainers = {
		134417,
		134416,
		134414,
		134415,
		134413,
		134412,
		9815510
	},

	lootLevel = 26,

	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 160000},
				{group = "junk", chance = 8240000},
				{group = "weapons_all", chance = 1000000},
				{group = "clothing_attachments", chance = 300000},
				{group = "armor_attachments", chance = 300000}
			},
			lootChance = 8000000
		}
	},

	lootContainerRespawn = 1800
}
 
registerScreenPlay("TalusErranSifScreenPlay", true)
 
function TalusErranSifScreenPlay:start()
	if (isZoneEnabled("talus")) then
                self:spawnMobiles()
                self:initializeLootContainers()
        end
end

function TalusErranSifScreenPlay:spawnMobiles()
	local pErran = spawnMobile("talus", "erran_sif", 300, -4.9, -13.8, -16.5, 14, 9895492)
	createObserver(DAMAGERECEIVED, "TalusErranSifScreenPlay", "erranAttacked", pErran)

	spawnMobile("talus", "hirsch_sif", 300, -8.0, -13.8, -12.5, 63, 9895492)
	spawnMobile("talus", "doak_sif", 300, 1.9, -13.8, -12.9, -38, 9895492)
	spawnMobile("talus", "rodian_sif_02", 300, -7.0, -13.8, 1.7, 110, 9895492)
	spawnMobile("talus", "trandoshan_sif_02", 300, 4.1, -6.8, -2.2, -48, 9895490)
	spawnMobile("talus", "trandoshan_sif_02", 300, 7.0, -6.8, -2.6, 164, 9895490)
	spawnMobile("talus", "rodian_sif_02", 300, 4.8, -6.8, -9.0, 164, 9895490)	
	spawnMobile("talus", "trandoshan_sif_02", 300, 4.2, -6.8, -15.3, -112, 9895489)
	spawnMobile("talus", "rodian_sif", 300, 6.3, -6.8, -15.7, -92, 9895489)
	spawnMobile("talus", "trandoshan_sif_02", 300, -1.1, -6.8, -6.3, -63, 9895488)
	spawnMobile("talus", "rodian_sif_02", 300, -3.7, 0.3, -2.2, -9, 9895485)
	spawnMobile("talus", "sif_mercenary", 300, 2143.4, 122.0, -5610.2, -44, 0)
	spawnMobile("talus", "trandoshan_sif_02", 300, 2152.0, 122.0, -5611.0, 68, 0)
end

function TalusErranSifScreenPlay:erranAttacked(npc, playerObject, damage)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(8, "DarkOrLight")

	if (npc ~= nil) then
		if(hasState == true) then
			CreatureObject(npc):setPvpStatusBitmask(0)
			--CreatureObject(npc):setOptionsBitmask(0)
			CreatureObject(npc):clearCombatState(1)
			CreatureObject(playerObject):clearCombatState(1)
			player:setScreenPlayState(16, DarkOrLight.questString)
			spatialChat(npc, "No! Please stop! You came with the Sith didn't you!")
			createEvent(10000, "TalusErranSifScreenPlay", "tellStory", npc, "")
		end
	end 
	
	return 0
end

function TalusErranSifScreenPlay:tellStory(pNpc)
	if (pNpc ~= nil) then
		spatialChat(pNpc, "Listen, I don't know if you are here looking for the Sith or hunting him, Just leave me out of it.")
		createEvent(10000, "TalusErranSifScreenPlay", "tellStoryTwo", pNpc, "")
	end

end

function TalusErranSifScreenPlay:tellStoryTwo(pNpc)
	if (pNpc ~= nil) then
		spatialChat(pNpc, "He killed 3 of my men with one swing and Demanded an Object thats been in my family for decades")
		createEvent(10000, "TalusErranSifScreenPlay", "tellStoryThree", pNpc, "")
	end
end

function TalusErranSifScreenPlay:tellStoryThree(pNpc)
	if (pNpc ~= nil) then
		spatialChat(pNpc, "I value the lives of my men over tangibles I know nothing about")
		createEvent(10000, "TalusErranSifScreenPlay", "tellStoryFour", pNpc, "")
	end
end

function TalusErranSifScreenPlay:tellStoryFour(pNpc)
	if (pNpc ~= nil) then
	spatialChat(pNpc, "Your 'friend' mentioned Yavin4, I suspect you will find him there..Take that information as a peace offering. Now, Leave my home.")
	createEvent(60000, "TalusErranSifScreenPlay", "resetErran", pNpc, "")
	end
end

function TalusErranSifScreenPlay:resetErran(pNpc)
	if (pNpc ~= nil) then
		CreatureObject(pNpc):setPvpStatusBitmask(1)
	end
end


