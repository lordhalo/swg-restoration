local ObjectManager = require("managers.object.object_manager")

bobaDailyEasy = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bobaDailyEasy",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bobaDailyEasy", true)

function bobaDailyEasy:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnTargets()
	end
end

function bobaDailyEasy:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bobaDailyEasy:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bobaDailyEasy:spawnTargets()

	local pWife = spawnMobile("tatooine", "boba_wife_easy", math.random(600) + 120, -7.3, -0.9, 20.9,  80, 1082883)
	self:setMoodString(pWife, "happy")
	local pMale = spawnMobile("tatooine", "ishitib_male", math.random(600) + 120, -5.6, -0.9, 21.2,  -65, 1082883)
	self:setMoodString(pMale, "entertained")
	createObserver(OBJECTDESTRUCTION, "bobaDailyEasy", "npcKilled", pWife)

end

function bobaDailyEasy:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bobaDailyEasy")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bobaDailyEasy")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bobaDailyEasy.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bobaDailyEasy")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bobaDailyEasy.questString)
				end
			end
		end)
	return 0
end

function bobaDailyEasy:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bobaDailyEasy.states.quest.phasetwo, bobaDailyEasy.questString)
	player:removeScreenPlayState(bobaDailyEasy.states.quest.phaseone, bobaDailyEasy.questString)
	player:removeScreenPlayState(bobaDailyEasy.states.quest.intro, bobaDailyEasy.questString)

end

boba_daily_easy_convo_handler = Object:new {
	
 }

function boba_daily_easy_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bobaDailyEasy")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bobaDailyEasy")
			local dailyReset = creature:hasScreenPlayState(8, "bobaDailyEasy")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function boba_daily_easy_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Target One", "", 7373, 115, 5, true, true, 0)	
		player:setScreenPlayState(2, bobaDailyEasy.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, bobaDailyEasy.questString)
		CreatureObject(conversingPlayer):addCashCredits(50000, true)
		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 300)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 300)
			end)
		end
		createEvent(82800000, "bobaDailyEasy", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bobaDailyEasy.states.quest.phasethree, bobaDailyEasy.questString)
	end
	


	return conversationScreen
end

