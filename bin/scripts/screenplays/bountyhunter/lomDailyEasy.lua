local ObjectManager = require("managers.object.object_manager")

lomDailyEasy = ScreenPlay:new {
	numberOfActs = 1,
	questString = "lomDailyEasy",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("lomDailyEasy", true)

function lomDailyEasy:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnTargets()
	end
end

function lomDailyEasy:getActivePlayerName()
	return self.questdata.activePlayerName
end

function lomDailyEasy:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function lomDailyEasy:spawnTargets()

	local pDroid = spawnMobile("tatooine", "lom_droid_easy", math.random(600) + 120, 4.1, -0.5, -3.8, -63, 1255997)
	createObserver(OBJECTDESTRUCTION, "lomDailyEasy", "npcKilled", pDroid)

end

function lomDailyEasy:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "lomDailyEasy")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "lomDailyEasy")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, lomDailyEasy.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "lomDailyEasy")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, lomDailyEasy.questString)
				end
			end
		end)
	return 0
end

function lomDailyEasy:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(lomDailyEasy.states.quest.phasetwo, lomDailyEasy.questString)
	player:removeScreenPlayState(lomDailyEasy.states.quest.phaseone, lomDailyEasy.questString)
	player:removeScreenPlayState(lomDailyEasy.states.quest.intro, lomDailyEasy.questString)

end

lom_daily_easy_convo_handler = Object:new {
	
 }

function lom_daily_easy_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "lomDailyEasy")
			local firstQuestComplete = creature:hasScreenPlayState(4, "lomDailyEasy")
			local dailyReset = creature:hasScreenPlayState(8, "lomDailyEasy")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function lom_daily_easy_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "accept_screen" ) then
		questNPC:doAnimation("bow2")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("tatooine", "Droid", "", -2890, 2423, 5, true, true, 0)	
		player:setScreenPlayState(2, lomDailyEasy.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		questNPC:doAnimation("bow2")
		player:setScreenPlayState(8, lomDailyEasy.questString)
		if (player:isRebel() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end

		if (player:isImperial() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end
		createEvent(82800000, "lomDailyEasy", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(lomDailyEasy.states.quest.phasethree, lomDailyEasy.questString)
	end
	


	return conversationScreen
end

