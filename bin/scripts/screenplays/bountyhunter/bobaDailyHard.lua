local ObjectManager = require("managers.object.object_manager")

bobaDailyHard = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bobaDailyHard",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bobaDailyHard", true)

function bobaDailyHard:start()
	if (isZoneEnabled("dantooine")) then
		self:spawnTargets()
	end
end

function bobaDailyHard:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bobaDailyHard:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bobaDailyHard:spawnTargets()

	local pJedi = spawnMobile("dantooine", "boba_jedi_hard", math.random(600) + 120, 6083, 2, -6473,  1, 0)
	createObserver(DAMAGERECEIVED, "bobaDailyHard", "npcDamageObserver", pJedi)
	createObserver(OBJECTDESTRUCTION, "bobaDailyHard", "npcKilled", pJedi)

end

function bobaDailyHard:npcDamageObserver(bossObject, playerObject, damage)

	local player = LuaCreatureObject(playerObject)
	local boss = LuaCreatureObject(bossObject)
	
	health = boss:getHAM(0)
	action = boss:getHAM(3)
	mind = boss:getHAM(6)
	
	maxHealth = boss:getMaxHAM(0)
	maxAction = boss:getMaxHAM(3)
	maxMind = boss:getMaxHAM(6)


	if (health <= (maxHealth * 0.6) and readData("bobaHard:helperAlive") == 0) then
		writeData("bobaHard:helperAlive",1)
		spatialChat(bossObject, "Y'rall! Never late for a meeting?")
		self:spawnSupport(playerObject)
	end

	return 0

end

function bobaDailyHard:spawnSupport(playerObject)

	local pGuard = spawnMobile("dantooine", "boba_jedi_guard", 0, 6100, 0, -6497,  -60, 0)
	spatialChat(pGuard, "What in the blazes is going on?!")
	CreatureObject(pGuard):engageCombat(playerObject)
end

function bobaDailyHard:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bobaDailyHard")
	writeData("bobaHard:helperAlive",0)
	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bobaDailyHard")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bobaDailyHard.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bobaDailyHard")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bobaDailyHard.questString)
				end
			end
		end)
	return 0
end

function bobaDailyHard:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bobaDailyHard.states.quest.phasetwo, bobaDailyHard.questString)
	player:removeScreenPlayState(bobaDailyHard.states.quest.phaseone, bobaDailyHard.questString)
	player:removeScreenPlayState(bobaDailyHard.states.quest.intro, bobaDailyHard.questString)

end

boba_daily_hard_convo_handler = Object:new {
	
 }

function boba_daily_hard_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bobaDailyHard")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bobaDailyHard")
			local dailyReset = creature:hasScreenPlayState(8, "bobaDailyHard")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function boba_daily_hard_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("dantooine", "Jedi", "", 6100, -6497, 5, true, true, 0)	
		player:setScreenPlayState(2, bobaDailyHard.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		questNPC:doAnimation("bow2")
		player:setScreenPlayState(8, bobaDailyHard.questString)
		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 1250)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 1250)
			end)
		end

		createLoot(pInventory, "boba_hard_reward", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		local amount = 100000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "bobaDailyHard", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bobaDailyHard.states.quest.phasethree, bobaDailyHard.questString)
	end
	


	return conversationScreen
end

