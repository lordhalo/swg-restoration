local ObjectManager = require("managers.object.object_manager")

bosskDailyHard = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bosskDailyHard",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bosskDailyHard", true)

function bosskDailyHard:start()
	if (isZoneEnabled("dathomir")) then
		self:spawnTargets()
	end
end

function bosskDailyHard:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bosskDailyHard:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bosskDailyHard:spawnTargets()

	local pKahmurra = spawnMobile("dathomir", "bossk_jedi_hard", math.random(600) + 120, 50.1, -56.5, -172.5,  -60, 3695706)
	createObserver(OBJECTDESTRUCTION, "bosskDailyHard", "npcKilled", pKahmurra)

end

function bosskDailyHard:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bosskDailyHard")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bosskDailyHard")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bosskDailyHard.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bosskDailyHard")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bosskDailyHard.questString)
				end
			end
		end)
	return 0
end

function bosskDailyHard:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bosskDailyHard.states.quest.phasetwo, bosskDailyHard.questString)
	player:removeScreenPlayState(bosskDailyHard.states.quest.phaseone, bosskDailyHard.questString)
	player:removeScreenPlayState(bosskDailyHard.states.quest.intro, bosskDailyHard.questString)

end

bossk_daily_hard_convo_handler = Object:new {
	
 }

function bossk_daily_hard_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bosskDailyHard")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bosskDailyHard")
			local dailyReset = creature:hasScreenPlayState(8, "bosskDailyHard")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function bossk_daily_hard_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("dathomir", "Force Renegade", "", -995, 6245, 5, true, true, 0)	
		player:setScreenPlayState(2, bosskDailyHard.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, bosskDailyHard.questString)
		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 2000)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 2000)
			end)
		end

		local amount = 115000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)

		createEvent(82800000, "bosskDailyHard", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bosskDailyHard.states.quest.phasethree, bosskDailyHard.questString)
	end
	


	return conversationScreen
end

