local ObjectManager = require("managers.object.object_manager")

deBarrelDaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "deBarrelDaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	kobola = {
		{"kobola_assassin", 360,  7346.7, 76.0, 97.9, -85, 0},
		{"kobola_assassin", 360,  7347.0, 75.1, 94.9, -83, 0},
		{"kobola_guard", 360,  -15.0, -0.6, 2.6, -16, 479723},
		{"kobola_guard", 360,  -19.7, -0.7, 2.8, -8, 479723},
		{"kobola_miner", 360,  14.2, -22.3, -5.4, -40, 479723},
		{"kobola_miner", 360,  12.3, -22.2, -5.3, 14, 479723},
		{"kobola_guard", 360,  25.7, -28.6, -11.3, -50, 479723},
		{"kobola_guard", 360,  26.4, -27.7, -8.5, -96, 479723},
		{"kobola_miner", 360,  22.7, -41.6, -71.1, -143, 479724},
		{"kobola_miner", 360,  22.6, -42.1, -67.9, -129, 479724},
		{"kobola_guard", 360,  23.9, -42.3, -68.8, -121, 479724},
		{"kobola_guard", 360,  52.9, -48.9, -55.3, -137, 479724},
		{"kobola_guard", 360,  55.2, -48.5, -57.5, -111, 479724},
		{"kobola_assassin", 360,  49.0, -47.4, -12.2, -179, 479724},
		{"kobola_guard", 360,  46.1, -47.0, -15.1, -174, 479724},
		{"kobola_foreman", 360,  56.9, -68.5, -41.7, -104, 479724},
		{"kobola_miner", 360,  55.2, -68.4, -40.4, -97, 479724},
		{"kobola_miner", 360,  54.8, -68.2, -43.5, -112, 479724},
		{"kobola_foreman", 360,  47.3, -49.0, -99.8, -115, 479732},
		{"kobola_miner", 360,  92.1, -46.4, -96.2, 3, 479727},
		{"kobola_miner", 360,  95.7, -45.6, -98.1, 68, 479727},
		{"kobola_miner", 360,  94.9, -45.7, -101.2, 161, 479727},
		{"kobola_pitboss", 360,  79.3, -46.5, -142.0, 73, 479733},
		{"kobola_miner", 360,  77.8, -46.2, -136.0, -42, 479733},
		{"kobola_miner", 360,  77.3, -45.8, -147.3, -160, 479733},
		{"kobola_guard", 360,  95.4, -62.0, -13.2, -89, 479725},
		{"kobola_assassin", 360,  95.3, -62.2, -16.0, -81, 479725},
		{"kobola_guard", 360,  94.9, -62.6, -19.4, -77, 479725},
		{"kobola_miner", 360,  94.2, -74.2, -56.8, 35, 479726},
		{"kobola_miner", 360,  96.5, -74.0, -57.4, 65, 479726},
		{"kobola_miner", 360,  96.8, -74.9, -60.9, 86, 479726},
		{"kobola_foreman", 360,  93.6, -75.4, -60.2, 40, 479726},
		{"kobola_miner", 360,  92.8, -76.4, -88.7, 113, 479726},
		{"kobola_miner", 360,  95.4, -76.2, -85.3, 115, 479726},
		{"kobola_foreman", 360,  91.6, -76.3, -84.4, 139, 479726},
		{"kobola_assassin", 360,  65.6, -76.8, -88.3, -3, 479726},
		{"kobola_assassin", 360,  60.8, -76.1, -81.0, 4, 479726},
		{"kobola_miner", 360,  54.6, -70.1, -121.2, -137, 479728},
		{"kobola_miner", 360,  54.1, -70.5, -118.3, -134, 479728},
		{"kobola_foreman", 360,  57.4, -70.6, -119.0, -128, 479728},
		{"kobola_foreman", 360,  86.5, -66.8, -142.2, -81, 479729},
		{"kobola_assassin", 360,  89.2, -66.8, -141.7, -24, 479729},
		{"kobola_assassin", 360,  86.9, -66.9, -140.4, -42, 479729},
		{"kobola_miner", 360,  112.7, -67.2, -89.7, -53, 479730},
		{"kobola_miner", 360,  114.4, -67.0, -86.5, -35, 479730},
		{"kobola_miner", 360,  142.7, -66.4, -85.5, 34, 479730},
		{"kobola_miner", 360,  144.7, -66.3, -87.6, 65, 479730},
		{"kobola_pitboss", 360,  133.5, -66.5, -105.6, -103, 479730},
		{"kobola_assassin", 360,  136.0, -66.2, -102.9, -95, 479730},
		{"kobola_assassin", 360,  135.9, -66.2, -107.7, -88, 479730},
		{"kobola_miner", 360,  155.1, -65.6, -126.1, 114, 479730},
		{"kobola_miner", 360,  155.6, -65.6, -130.6, 128, 479730},
		{"kobola_miner", 360,  151.4, -66.1, -130.5, 165, 479730},
		{"kobola_foreman", 360,  150.9, -66.6, -125.2, 133, 479730},
		{"kobola_pitboss", 360,  189.8, -66.2, -106.1, -71, 479731},
		{"kobola_pitboss", 360,  190.1, -66.4, -99.2, -83, 479731},
		{"kobola_underboss", 360,  187.1, -66.1, -101.6, -84, 479731},
	},

}
registerScreenPlay("deBarrelDaily", true)

function deBarrelDaily:start()
	if (isZoneEnabled("rori")) then
		self:spawnKobola()
	end
end

function deBarrelDaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function deBarrelDaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function deBarrelDaily:spawnKobola()

		local mobileTable = self.kobola
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("rori", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "deBarrelDaily", "npcKilled", pMobile)
		end
end

function deBarrelDaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "deBarrelDaily")

		if(hasState == true) then
			writeData(player:getObjectID() .. ":kobolaKilled", readData(player:getObjectID() .. ":kobolaKilled") + 1)
			
			if(readData(player:getObjectID() .. ":kobolaKilled") == 30) then
				CreatureObject(playerObject):sendSystemMessage("Mission Complete")
				player:setScreenPlayState(4, deBarrelDaily.questString)
			end
		end
	return 0
end

function deBarrelDaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":kobolaKilled", 0)
	player:removeScreenPlayState(deBarrelDaily.states.quest.phasetwo, deBarrelDaily.questString)
	player:removeScreenPlayState(deBarrelDaily.states.quest.phaseone, deBarrelDaily.questString)
	player:removeScreenPlayState(deBarrelDaily.states.quest.intro, deBarrelDaily.questString)

end

de_barrel_daily_convo_handler = Object:new {
	
 }

function de_barrel_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local hasBlackSun = creature:hasSkill("black_sun_novice")
			local firstQuestStarted = creature:hasScreenPlayState(2, "deBarrelDaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "deBarrelDaily")
			local dailyReset = creature:hasScreenPlayState(8, "deBarrelDaily")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (hasBlackSun == true or hasDeathWatch == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function de_barrel_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("rori", "Kobola Cave", "Kill 30 Kobola", 7373, 115, 5, true, true, 0)
		player:setScreenPlayState(2, deBarrelDaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, deBarrelDaily.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 25)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 25)
			end)
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "de_10_barrel", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		createEvent(true, 82800000, "deBarrelDaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(deBarrelDaily.states.quest.phasethree, deBarrelDaily.questString)
	end
	


	return conversationScreen
end

