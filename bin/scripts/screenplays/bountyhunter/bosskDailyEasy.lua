local ObjectManager = require("managers.object.object_manager")

bosskDailyEasy = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bosskDailyEasy",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bosskDailyEasy", true)

function bosskDailyEasy:start()
	if (isZoneEnabled("lok")) then
		self:spawnTargets()
	end
end

function bosskDailyEasy:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bosskDailyEasy:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bosskDailyEasy:spawnTargets()

	local pCorsair = spawnMobile("lok", "bossk_corsair_easy", math.random(600) + 120, -3911, 12, -3799,  145, 0)
	createObserver(OBJECTDESTRUCTION, "bosskDailyEasy", "npcKilled", pCorsair)

end

function bosskDailyEasy:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bosskDailyEasy")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bosskDailyEasy")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bosskDailyEasy.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bosskDailyEasy")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bosskDailyEasy.questString)
				end
			end
		end)
	return 0
end

function bosskDailyEasy:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bosskDailyEasy.states.quest.phasetwo, bosskDailyEasy.questString)
	player:removeScreenPlayState(bosskDailyEasy.states.quest.phaseone, bosskDailyEasy.questString)
	player:removeScreenPlayState(bosskDailyEasy.states.quest.intro, bosskDailyEasy.questString)

end

bossk_daily_easy_convo_handler = Object:new {
	
 }

function bossk_daily_easy_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 

	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bosskDailyEasy")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bosskDailyEasy")
			local dailyReset = creature:hasScreenPlayState(8, "bosskDailyEasy")

				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function bossk_daily_easy_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("lok", "Engineer", "", -3910, -3799, 5, true, true, 0)	
		player:setScreenPlayState(2, bosskDailyEasy.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, bosskDailyEasy.questString)
		if (player:isRebel() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end

		if (player:isImperial() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end

		local amount = 35000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "bosskDailyEasy", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bosskDailyEasy.states.quest.phasethree, bosskDailyEasy.questString)
	end
	


	return conversationScreen
end

