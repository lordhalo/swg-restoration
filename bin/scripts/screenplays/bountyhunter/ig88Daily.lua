local ObjectManager = require("managers.object.object_manager")

igDailyEasy = ScreenPlay:new {
	numberOfActs = 1,
	questString = "igDailyEasy",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("igDailyEasy", true)

function igDailyEasy:start()
	if (isZoneEnabled("naboo")) then
		self:spawnTargets()
	end
end

function igDailyEasy:getActivePlayerName()
	return self.questdata.activePlayerName
end

function igDailyEasy:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function igDailyEasy:spawnTargets()

	local pGungan = spawnMobile("naboo", "ig_gungan_easy", math.random(600) + 120, 4.9, -0.9, -15.9,  79, 94)
	createObserver(OBJECTDESTRUCTION, "igDailyEasy", "npcKilled", pGungan)

end

function igDailyEasy:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "igDailyEasy")

		if(hasState == true) then	
			CreatureObject(playerObject):sendSystemMessage("Mission Complete")
			player:playMusicMessage("sound/ui_button_random.snd")
			player:setScreenPlayState(4, igDailyEasy.questString)

		end

	return 0
end

function igDailyEasy:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":igTarget", 0)
	player:removeScreenPlayState(igDailyEasy.states.quest.phasetwo, igDailyEasy.questString)
	player:removeScreenPlayState(igDailyEasy.states.quest.phaseone, igDailyEasy.questString)
	player:removeScreenPlayState(igDailyEasy.states.quest.intro, igDailyEasy.questString)

end

ig88_daily_convo_handler = Object:new {
	
 }

function ig88_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "igDailyEasy")
			local firstQuestComplete = creature:hasScreenPlayState(4, "igDailyEasy")
			local dailyReset = creature:hasScreenPlayState(8, "igDailyEasy")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function ig88_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Target One", "", 7373, 115, 5, true, true, 0)	
		player:setScreenPlayState(2, igDailyEasy.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, igDailyEasy.questString)
		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 100)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 100)
			end)
		end
		createEvent(5 * 60 * 1000, "igDailyEasy", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(igDailyEasy.states.quest.phasethree, igDailyEasy.questString)
	end
	


	return conversationScreen
end

