local ObjectManager = require("managers.object.object_manager")

lomDailyHard = ScreenPlay:new {
	numberOfActs = 1,
	questString = "lomDailyHard",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("lomDailyHard", true)

function lomDailyHard:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnTargets()
	end
end

function lomDailyHard:getActivePlayerName()
	return self.questdata.activePlayerName
end

function lomDailyHard:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function lomDailyHard:spawnTargets()

	local pTuskin = spawnMobile("tatooine", "lom_tusken_hard", math.random(600) + 120, 38.3, 22.8, 14.2,  -168, 1189182)
	createObserver(OBJECTDESTRUCTION, "lomDailyHard", "npcKilled", pTuskin)

end

function lomDailyHard:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "lomDailyHard")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "lomDailyHard")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, lomDailyHard.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "lomDailyHard")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, lomDailyHard.questString)
				end
			end
		end)
	return 0
end

function lomDailyHard:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(lomDailyHard.states.quest.phasetwo, lomDailyHard.questString)
	player:removeScreenPlayState(lomDailyHard.states.quest.phaseone, lomDailyHard.questString)
	player:removeScreenPlayState(lomDailyHard.states.quest.intro, lomDailyHard.questString)

end

lom_daily_hard_convo_handler = Object:new {
	
 }

function lom_daily_hard_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "lomDailyHard")
			local firstQuestComplete = creature:hasScreenPlayState(4, "lomDailyHard")
			local dailyReset = creature:hasScreenPlayState(8, "lomDailyHard")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function lom_daily_hard_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "second_screen" ) then
		player:doAnimation("nod")
	end

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		questNPC:doAnimation("nod")
		PlayerObject(pGhost):addWaypoint("tatooine", "Tusken Raider", "", -4005, 6225, 5, true, true, 0)	
		player:setScreenPlayState(2, lomDailyHard.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, lomDailyHard.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 500)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 500)
			end)
		end
		createLoot(pInventory, "bossk_medium_reward", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		local amount = 70000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "lomDailyHard", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(lomDailyHard.states.quest.phasethree, lomDailyHard.questString)
	end
	


	return conversationScreen
end

