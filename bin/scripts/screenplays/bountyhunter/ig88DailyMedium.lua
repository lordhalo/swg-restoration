local ObjectManager = require("managers.object.object_manager")

igDailyMedium = ScreenPlay:new {
	numberOfActs = 1,
	questString = "igDailyMedium",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("igDailyMedium", true)

function igDailyMedium:start()
	if (isZoneEnabled("naboo")) then
		self:spawnTargets()
	end
end

function igDailyMedium:getActivePlayerName()
	return self.questdata.activePlayerName
end

function igDailyMedium:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function igDailyMedium:spawnTargets()

	local pSmuggler = spawnMobile("corellia", "ig_corsec_medium", math.random(600) + 120, 4.7, -20.8, 33.9, -136, 1855730)
	createObserver(OBJECTDESTRUCTION, "igDailyMedium", "npcKilled", pSmuggler)

end

function igDailyMedium:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "igDailyMedium")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "igDailyMedium")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, igDailyMedium.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "igDailyMedium")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, igDailyMedium.questString)
				end
			end
		end)
	return 0
end

function igDailyMedium:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(igDailyMedium.states.quest.phasetwo, igDailyMedium.questString)
	player:removeScreenPlayState(igDailyMedium.states.quest.phaseone, igDailyMedium.questString)
	player:removeScreenPlayState(igDailyMedium.states.quest.intro, igDailyMedium.questString)

end

ig88_daily_medium_convo_handler = Object:new {
	
 }

function ig88_daily_medium_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "igDailyMedium")
			local firstQuestComplete = creature:hasScreenPlayState(4, "igDailyMedium")
			local dailyReset = creature:hasScreenPlayState(8, "igDailyMedium")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function ig88_daily_medium_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("corellia", "Rogue Corsec Base", "", 5290, 1494, 5, true, true, 0)	
		player:setScreenPlayState(2, igDailyMedium.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, igDailyMedium.questString)

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "ig88_medium_reward", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		local amount = 50000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)

		createEvent(82800000, "igDailyMedium", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(igDailyMedium.states.quest.phasethree, igDailyMedium.questString)
	end
	


	return conversationScreen
end

