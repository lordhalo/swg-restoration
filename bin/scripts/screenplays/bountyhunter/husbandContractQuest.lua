local ObjectManager = require("managers.object.object_manager")

husbandContractQuest = ScreenPlay:new {
	numberOfActs = 1,
	questString = "husbandContractQuest",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	mineRats = {
		{"death_watch_mine_rat", 300, -181.0, -60.3, -231.9, 108, 5996376},
		{"death_watch_mine_rat", 300, -172.7, -60.0, -237.9, -16, 5996376},
		{"death_watch_mine_rat", 300, -159.9, -59.9, -243.6, -82, 5996376},
	},
}
registerScreenPlay("husbandContractQuest", true)

function husbandContractQuest:start()
	if (isZoneEnabled("corellia")) then
		--self:spawnRats()
		--self:spawnActiveAreas()
		local pHotel = getSceneObject(1935522)
		createObserver(ENTEREDBUILDING, "husbandContractQuest", "onEnterHotel", pHotel)
	end
end

function husbandContractQuest:getActivePlayerName()
	return self.questdata.activePlayerName
end

function husbandContractQuest:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function husbandContractQuest:spawnActiveAreas()

	--enter, find expired ticket to next location
end

function husbandContractQuest:onEnterHotel(sceneObject, pMovingObject)
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		local player = LuaCreatureObject(pMovingObject)
		local hasState = player:hasScreenPlayState(2, "husbandContractQuest")

		if (hasState == true) then
			createEvent(2 * 60 * 1000, "husbandContractQuest", "sendSystemMessage", pMovingObject)
		end

		return 0
	end)

end

function husbandContractQuest:sendSystemMessage(pMovingObject)
	local player = LuaCreatureObject(pMovingObject)
	player:sendSystemMessage("After searching the Hotel, you find an expired travel ticket.........")
end

function husbandContractQuest:spawnRats()

		local mobileTable = self.mineRats
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("endor", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "husbandContractQuest", "npcKilled", pMobile)
		end
end

function husbandContractQuest:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "husbandContractQuest")

		if(hasState == true) then
			writeData(player:getObjectID() .. ":ratKilled", readData(player:getObjectID() .. ":ratKilled") + 1)
			
			if(readData(player:getObjectID() .. ":ratKilled") == 10) then
				CreatureObject(playerObject):sendSystemMessage("Mission Complete")
				player:setScreenPlayState(4, husbandContractQuest.questString)
			end
		end
	return 0
end

function husbandContractQuest:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":ratKilled", 0)
	player:removeScreenPlayState(husbandContractQuest.states.quest.phasetwo, husbandContractQuest.questString)
	player:removeScreenPlayState(husbandContractQuest.states.quest.phaseone, husbandContractQuest.questString)
	player:removeScreenPlayState(husbandContractQuest.states.quest.intro, husbandContractQuest.questString)

end

husband_contract_convo_handler = Object:new {
	
 }

function husband_contract_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "husbandContractQuest")
			local firstQuestComplete = creature:hasScreenPlayState(4, "husbandContractQuest")
			local dailyReset = creature:hasScreenPlayState(8, "husbandContractQuest")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function husband_contract_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, husbandContractQuest.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		giveItem(pInventory, "object/tangible/loot/dungeon/death_watch_bunker/mining_drill_reward.iff", -1)
		player:setScreenPlayState(8, husbandContractQuest.questString)
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 20)
			end)
		createEvent(5 * 60 * 1000, "husbandContractQuest", "removeDailyTimer", conversingPlayer)
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(husbandContractQuest.states.quest.phasethree, husbandContractQuest.questString)
	end
	


	return conversationScreen
end

