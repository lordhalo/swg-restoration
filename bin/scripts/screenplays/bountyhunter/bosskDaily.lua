local ObjectManager = require("managers.object.object_manager")

bosskDaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bosskDaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	mineRats = {
		{"death_watch_mine_rat", 300, -181.0, -60.3, -231.9, 108, 5996376},
		{"death_watch_mine_rat", 300, -172.7, -60.0, -237.9, -16, 5996376},
		{"death_watch_mine_rat", 300, -159.9, -59.9, -243.6, -82, 5996376},
	},
}
registerScreenPlay("bosskDaily", true)

function bosskDaily:start()
	if (isZoneEnabled("endor")) then
		self:spawnRats()
	end
end

function bosskDaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bosskDaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bosskDaily:spawnRats()

		local mobileTable = self.mineRats
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("endor", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "bosskDaily", "npcKilled", pMobile)
		end
end

function bosskDaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bosskDaily")

		if(hasState == true) then
			writeData(player:getObjectID() .. ":ratKilled", readData(player:getObjectID() .. ":ratKilled") + 1)
			
			if(readData(player:getObjectID() .. ":ratKilled") == 10) then
				CreatureObject(playerObject):sendSystemMessage("Mission Complete")
				player:setScreenPlayState(4, bosskDaily.questString)
			end
		end
	return 0
end

function bosskDaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":ratKilled", 0)
	player:removeScreenPlayState(bosskDaily.states.quest.phasetwo, bosskDaily.questString)
	player:removeScreenPlayState(bosskDaily.states.quest.phaseone, bosskDaily.questString)
	player:removeScreenPlayState(bosskDaily.states.quest.intro, bosskDaily.questString)

end

bossk_daily_convo_handler = Object:new {
	
 }

function bossk_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bosskDaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bosskDaily")
			local dailyReset = creature:hasScreenPlayState(8, "bosskDaily")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function bossk_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, bosskDaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		giveItem(pInventory, "object/tangible/loot/dungeon/death_watch_bunker/mining_drill_reward.iff", -1)
		player:setScreenPlayState(8, bosskDaily.questString)
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 20)
			end)
		createEvent(5 * 60 * 1000, "bosskDaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bosskDaily.states.quest.phasethree, bosskDaily.questString)
	end
	


	return conversationScreen
end

