local ObjectManager = require("managers.object.object_manager")

igDailyEpic = ScreenPlay:new {
	numberOfActs = 1,
	questString = "igDailyEpic",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("igDailyEpic", true)

function igDailyEpic:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnTargets()
	end
end

function igDailyEpic:getActivePlayerName()
	return self.questdata.activePlayerName
end

function igDailyEpic:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function igDailyEpic:spawnTargets()

	local pGeonosian = spawnMobile("yavin4", "ig_geonosian_epic", math.random(600) + 120, -131.7, -34.0, -184.0, -159, 1627815)
	createObserver(OBJECTDESTRUCTION, "igDailyEpic", "npcKilled", pGeonosian)

end

function igDailyEpic:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "igDailyEpic")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "igDailyEpic")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, igDailyEpic.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "igDailyEpic")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, igDailyEpic.questString)
				end
			end
		end)
	return 0
end

function igDailyEpic:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(igDailyEpic.states.quest.phasetwo, igDailyEpic.questString)
	player:removeScreenPlayState(igDailyEpic.states.quest.phaseone, igDailyEpic.questString)
	player:removeScreenPlayState(igDailyEpic.states.quest.intro, igDailyEpic.questString)

end

ig88_daily_epic_convo_handler = Object:new {
	
 }

function ig88_daily_epic_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "igDailyEpic")
			local firstQuestComplete = creature:hasScreenPlayState(4, "igDailyEpic")
			local dailyReset = creature:hasScreenPlayState(8, "igDailyEpic")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function ig88_daily_epic_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("yavin4", "Geonosian Lab", "", -6497, -420, 5, true, true, 0)	
		player:setScreenPlayState(2, igDailyEpic.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, igDailyEpic.questString)

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "ig88_epic_reward", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		createEvent(82800000, "igDailyEpic", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(igDailyEpic.states.quest.phasethree, igDailyEpic.questString)
	end
	


	return conversationScreen
end

