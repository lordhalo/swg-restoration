factionRewardType = {
	armor = 1,
	weapon = 2,
	uniform = 3,
	furniture = 4,
	container = 5,
	terminal = 6,
	installation = 7,
	hireling = 8,
	deedList = 9,
}

blackSunRewardData = {
	weaponsArmorList = { --Pistol, 2HSword, Knuckler, Rifle, Carbine, Heavy, Sword, Polearm
		"de_10_pistol", "executioner_hack", "razor_knuckler", "dl_19_rifle", "ngant_carbine", "cmfrag_shotgun", "junti_sword", "shock_lance", "armor_bountyhunter_helmet", "armor_bountyhunter_chest", "armor_bountyhunter_legs", "armor_bountyhunter_boots", "armor_bountyhunter_bicep_l", "armor_bountyhunter_bicep_r", "armor_bountyhunter_bracer_l", "armor_bountyhunter_bracer_r", "armor_bountyhunter_boots", "armor_bountyhunter_gloves", "armor_bountyhunter_belt", "armor_mandalorian_helmet", "armor_mandalorian_chest", "armor_mandalorian_legs", "armor_mandalorian_boots", "armor_mandalorian_bicep_l", "armor_mandalorian_bicep_r", "armor_mandalorian_bracer_l", "armor_mandalorian_bracer_r", "armor_mandalorian_boots", "armor_mandalorian_gloves", "armor_mandalorian_belt", "armor_mandalorian_rebel_helmet", "armor_mandalorian_rebel_chest", "armor_mandalorian_rebel_legs", "armor_mandalorian_rebel_boots", "armor_mandalorian_rebel_bicep_l", "armor_mandalorian_rebel_bicep_r", "armor_mandalorian_rebel_bracer_l", "armor_mandalorian_rebel_bracer_r", "armor_mandalorian_rebel_boots", "armor_mandalorian_rebel_gloves", "armor_mandalorian_rebel_belt", "armor_mandalorian_imperial_helmet", "armor_mandalorian_imperial_chest", "armor_mandalorian_imperial_legs", "armor_mandalorian_imperial_boots", "armor_mandalorian_imperial_bicep_l", "armor_mandalorian_imperial_bicep_r", "armor_mandalorian_imperial_bracer_l", "armor_mandalorian_imperial_bracer_r", "armor_mandalorian_imperial_boots", "armor_mandalorian_imperial_gloves", "armor_mandalorian_imperial_belt"
	},

	weaponsArmor = {
		de_10_pistol = { type=factionRewardType.weapon, display="DE-10 Pistol", item="object/tangible/loot/loot_schematic/death_watch_pistol_de10_schematic.iff", cost=1200},
		executioner_hack = { type=factionRewardType.weapon, display="Executioners Hack", item="object/tangible/loot/loot_schematic/death_watch_executioners_hack_schematic.iff", cost=1200},
		razor_knuckler = { type=factionRewardType.weapon, display="Razor Knuckler", item="object/tangible/loot/loot_schematic/death_watch_razor_knuckler_schematic.iff", cost=1200},
		dl_19_rifle = { type=factionRewardType.weapon, display="DL-19 Rifle", item="object/tangible/loot/weapons/rifle_dl19_schematic.iff", cost=1200},
		ngant_carbine = { type=factionRewardType.weapon, display="9118 Carbine", item="object/tangible/loot/weapons/carbine_9118_schematic.iff", cost=1200},
		cmfrag_shotgun = { type=factionRewardType.weapon, display="C-M Frag Storm Heavy Shotgun", item="object/tangible/loot/weapons/heavy_cmfrag_schematic.iff", cost=1200},	junti_sword = { type=factionRewardType.weapon, display="Junti Baton", item="object/tangible/loot/weapons/sword_junti_schematic.iff", cost=1200},
		shock_lance = { type=factionRewardType.weapon, display="Shock Lance", item="object/tangible/loot/weapons/lance_shock_schematic.iff", cost=1200},
		armor_bountyhunter_helmet = { type=factionRewardType.armor, display="Bounty Hunter Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_helmet_schematic.iff", cost=1600},
		armor_bountyhunter_chest = { type=factionRewardType.armor, display="Bounty Hunter Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_chest_plate_schematic.iff", cost=1600},
		armor_bountyhunter_legs = { type=factionRewardType.armor, display="Bounty Hunter Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_leggings_schematic.iff", cost=1600},
		armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1600},
		armor_bountyhunter_bicep_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bicep", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bicep_l_schematic.iff", cost=1600},
		armor_bountyhunter_bicep_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bicep", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bicep_r_schematic.iff", cost=1600},
		armor_bountyhunter_bracer_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bracer", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bracer_l_schematic.iff", cost=1600},
		armor_bountyhunter_bracer_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bracer", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bracer_r_schematic.iff", cost=1600},
		armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1600},
		armor_bountyhunter_gloves = { type=factionRewardType.armor, display="Bounty Hunter Armor Gloves", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_gloves_schematic.iff", cost=1600},
		armor_bountyhunter_belt = { type=factionRewardType.armor, display="Bounty Hunter Armor Belt", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_belt_schematic.iff", cost=1600},
		armor_mandalorian_helmet = { type=factionRewardType.armor, display="Mandalorian Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_helmet_schematic.iff", cost=2600},
		armor_mandalorian_chest = { type=factionRewardType.armor, display="Mandalorian Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_legs = { type=factionRewardType.armor, display="Mandalorian Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_leggings_schematic.iff", cost=2000},
		armor_mandalorian_boots = { type=factionRewardType.armor, display="Mandalorian Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_boots_schematic.iff", cost=1600},
		armor_mandalorian_bicep_l = { type=factionRewardType.armor, display="Mandalorian Armor Left Bicep", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_bicep_r = { type=factionRewardType.armor, display="Mandalorian Armor Right Bicep", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_bracer_l = { type=factionRewardType.armor, display="Mandalorian Armor Left Bracer", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_bracer_r = { type=factionRewardType.armor, display="Mandalorian Armor Right Bracer", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_boots = { type=factionRewardType.armor, display="Mandalorian Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_boots_schematic.iff", cost=1600},
		armor_mandalorian_gloves = { type=factionRewardType.armor, display="Mandalorian Armor Gloves", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_gloves_schematic.iff", cost=1600},
		armor_mandalorian_belt = { type=factionRewardType.armor, display="Mandalorian Armor Belt", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_belt_schematic.iff", cost=1600},
		armor_mandalorian_rebel_helmet = { type=factionRewardType.armor, display="Crusader MK II Helmet", item="object/tangible/loot/loot_schematic/mandalorian_rebel_helmet_schematic.iff", cost=2600},
		armor_mandalorian_rebel_chest = { type=factionRewardType.armor, display="Crusader MK II Chestplate", item="object/tangible/loot/loot_schematic/mandalorian_rebel_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_rebel_legs = { type=factionRewardType.armor, display="Crusader MK II Legs", item="object/tangible/loot/loot_schematic/mandalorian_rebel_leggings_schematic.iff", cost=2000},
		armor_mandalorian_rebel_boots = { type=factionRewardType.armor, display="Crusader MK II Boots", item="object/tangible/loot/loot_schematic/mandalorian_rebel_boots_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bicep_l = { type=factionRewardType.armor, display="Crusader MK II Left Bicep", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bicep_r = { type=factionRewardType.armor, display="Crusader MK II Right Bicep", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bracer_l = { type=factionRewardType.armor, display="Crusader MK II Left Bracer", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bracer_r = { type=factionRewardType.armor, display="Crusader MK II Right Bracer", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_rebel_boots = { type=factionRewardType.armor, display="Crusader MK II Boots", item="object/tangible/loot/loot_schematic/mandalorian_rebel_boots_schematic.iff", cost=1600},
		armor_mandalorian_rebel_gloves = { type=factionRewardType.armor, display="Crusader MK II Gloves", item="object/tangible/loot/loot_schematic/mandalorian_rebel_gloves_schematic.iff", cost=1600},
		armor_mandalorian_rebel_belt = { type=factionRewardType.armor, display="Crusader MK II Belt", item="object/tangible/loot/loot_schematic/mandalorian_rebel_belt_schematic.iff", cost=1600},
	armor_mandalorian_imperial_helmet = { type=factionRewardType.armor, display="Crusader MK III Helmet", item="object/tangible/loot/loot_schematic/mandalorian_imperial_helmet_schematic.iff", cost=2600},
		armor_mandalorian_imperial_chest = { type=factionRewardType.armor, display="Crusader MK III Chestplate", item="object/tangible/loot/loot_schematic/mandalorian_imperial_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_imperial_legs = { type=factionRewardType.armor, display="Crusader MK III Legs", item="object/tangible/loot/loot_schematic/mandalorian_imperial_leggings_schematic.iff", cost=2000},
		armor_mandalorian_imperial_boots = { type=factionRewardType.armor, display="Crusader MK III Boots", item="object/tangible/loot/loot_schematic/mandalorian_imperial_boots_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bicep_l = { type=factionRewardType.armor, display="Crusader MK III Left Bicep", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bicep_r = { type=factionRewardType.armor, display="Crusader MK III Right Bicep", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bracer_l = { type=factionRewardType.armor, display="Crusader MK III Left Bracer", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bracer_r = { type=factionRewardType.armor, display="Crusader MK III Right Bracer", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_imperial_boots = { type=factionRewardType.armor, display="Crusader MK III Boots", item="object/tangible/loot/loot_schematic/mandalorian_imperial_boots_schematic.iff", cost=1600},
		armor_mandalorian_imperial_gloves = { type=factionRewardType.armor, display="Crusader MK III Gloves", item="object/tangible/loot/loot_schematic/mandalorian_imperial_gloves_schematic.iff", cost=1600},
		armor_mandalorian_imperial_belt = { type=factionRewardType.armor, display="Crusader MK III Belt", item="object/tangible/loot/loot_schematic/mandalorian_imperial_belt_schematic.iff", cost=1600}
		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_imperial"
	},

	installations = {
		--hq_s01_pvp_imperial = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_imperial", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_imperial","hq_s01_imperial"} }
	},

	furnitureList = {
		"art_large", "art_large_s02", "art_small_s01", "art_small_s02", "art_small_s03", "art_small_s04"
	},

	furniture = {
		art_large = { type=factionRewardType.container, display="Mandalorian Art Style 1", item="object/tangible/furniture/all/frn_all_decorative_lg_s1.iff", cost=500},
		art_large_s02 = { type=factionRewardType.container, display="Mandalorian Art Style 2", item="object/tangible/furniture/all/frn_all_decorative_lg_s2.iff", cost=500},
		art_small_s01 = { type=factionRewardType.furniture, display="Mandalorian Art Style 3", item="object/tangible/furniture/all/frn_all_decorative_sm_s1.iff", cost=200},
		art_small_s02 = { type=factionRewardType.furniture, display="Mandalorian Art Style 4", item="object/tangible/furniture/all/frn_all_decorative_sm_s2.iff", cost=200},
		art_small_s03 =  { type=factionRewardType.container, display="Mandalorian Art Style 5", item="object/tangible/furniture/all/frn_all_decorative_sm_s3.iff", cost=200},
		art_small_s04 = { type=factionRewardType.furniture, display="Mandalorian Art Style 6", item="object/tangible/furniture/all/frn_all_decorative_sm_s4.iff", cost=200},
	},

	hirelingList = {
		--"rebel_trooper",
	},

	hirelings = {
		--rebel_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:command_barc", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="rebel_trooper", cost=150},
	},

	deedListing = {
		"mandolorian_jetpack", "merr_sonn", "vip_bunker"
	},

	deedList = {
		mandolorian_jetpack = { type=factionRewardType.deed, display="Mitrinomon Z-6 Jetpack Schematic", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_jetpack_schematic.iff", cost=24000},
		merr_sonn = { type=factionRewardType.deed, display="Merr Sonn JT-12 Jetpack Schematic", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_jetpack_s02_schematic.iff", cost=24000},
		vip_bunker = { type=factionRewardType.deed, display="VIP Bunker Deed", item="object/tangible/deed/player_house_deed/vipbunker_house_deed.iff", cost=12500},
	},
}

deathWatchRewardData = {
	weaponsArmorList = { --Pistol, 2HSword, Knuckler, Rifle, Carbine, Heavy, Sword, Polearm
		"de_10_pistol", "executioner_hack", "razor_knuckler", "dl_19_rifle", "ngant_carbine", "cmfrag_shotgun", "junti_sword", "shock_lance", "armor_bountyhunter_helmet", "armor_bountyhunter_chest", "armor_bountyhunter_legs", "armor_bountyhunter_boots", "armor_bountyhunter_bicep_l", "armor_bountyhunter_bicep_r", "armor_bountyhunter_bracer_l", "armor_bountyhunter_bracer_r", "armor_bountyhunter_boots", "armor_bountyhunter_gloves", "armor_bountyhunter_belt", "armor_mandalorian_helmet", "armor_mandalorian_chest", "armor_mandalorian_legs", "armor_mandalorian_boots", "armor_mandalorian_bicep_l", "armor_mandalorian_bicep_r", "armor_mandalorian_bracer_l", "armor_mandalorian_bracer_r", "armor_mandalorian_boots", "armor_mandalorian_gloves", "armor_mandalorian_belt", "armor_mandalorian_rebel_helmet", "armor_mandalorian_rebel_chest", "armor_mandalorian_rebel_legs", "armor_mandalorian_rebel_boots", "armor_mandalorian_rebel_bicep_l", "armor_mandalorian_rebel_bicep_r", "armor_mandalorian_rebel_bracer_l", "armor_mandalorian_rebel_bracer_r", "armor_mandalorian_rebel_boots", "armor_mandalorian_rebel_gloves", "armor_mandalorian_rebel_belt", "armor_mandalorian_imperial_helmet", "armor_mandalorian_imperial_chest", "armor_mandalorian_imperial_legs", "armor_mandalorian_imperial_boots", "armor_mandalorian_imperial_bicep_l", "armor_mandalorian_imperial_bicep_r", "armor_mandalorian_imperial_bracer_l", "armor_mandalorian_imperial_bracer_r", "armor_mandalorian_imperial_boots", "armor_mandalorian_imperial_gloves", "armor_mandalorian_imperial_belt"
	},

	weaponsArmor = {
		de_10_pistol = { type=factionRewardType.weapon, display="DE-10 Pistol", item="object/tangible/loot/loot_schematic/death_watch_pistol_de10_schematic.iff", cost=1200},
		executioner_hack = { type=factionRewardType.weapon, display="Executioners Hack", item="object/tangible/loot/loot_schematic/death_watch_executioners_hack_schematic.iff", cost=1200},
		razor_knuckler = { type=factionRewardType.weapon, display="Razor Knuckler", item="object/tangible/loot/loot_schematic/death_watch_razor_knuckler_schematic.iff", cost=1200},
		dl_19_rifle = { type=factionRewardType.weapon, display="DL-19 Rifle", item="object/tangible/loot/weapons/rifle_dl19_schematic.iff", cost=1200},
		ngant_carbine = { type=factionRewardType.weapon, display="9118 Carbine", item="object/tangible/loot/weapons/carbine_9118_schematic.iff", cost=1200},
		cmfrag_shotgun = { type=factionRewardType.weapon, display="C-M Frag Storm Heavy Shotgun", item="object/tangible/loot/weapons/heavy_cmfrag_schematic.iff", cost=1200},	junti_sword = { type=factionRewardType.weapon, display="Junti Baton", item="object/tangible/loot/weapons/sword_junti_schematic.iff", cost=1200},
		shock_lance = { type=factionRewardType.weapon, display="Shock Lance", item="object/tangible/loot/weapons/lance_shock_schematic.iff", cost=1200},
		armor_bountyhunter_helmet = { type=factionRewardType.armor, display="Bounty Hunter Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_helmet_schematic.iff", cost=1600},
		armor_bountyhunter_chest = { type=factionRewardType.armor, display="Bounty Hunter Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_chest_plate_schematic.iff", cost=1600},
		armor_bountyhunter_legs = { type=factionRewardType.armor, display="Bounty Hunter Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_leggings_schematic.iff", cost=1600},
		armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1600},
		armor_bountyhunter_bicep_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bicep", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bicep_l_schematic.iff", cost=1600},
		armor_bountyhunter_bicep_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bicep", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bicep_r_schematic.iff", cost=1600},
		armor_bountyhunter_bracer_l = { type=factionRewardType.armor, display="Bounty Hunter Armor Left Bracer", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bracer_l_schematic.iff", cost=1600},
		armor_bountyhunter_bracer_r = { type=factionRewardType.armor, display="Bounty Hunter Armor Right Bracer", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_bracer_r_schematic.iff", cost=1600},
		armor_bountyhunter_boots = { type=factionRewardType.armor, display="Bounty Hunter Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_boots_schematic.iff", cost=1600},
		armor_bountyhunter_gloves = { type=factionRewardType.armor, display="Bounty Hunter Armor Gloves", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_gloves_schematic.iff", cost=1600},
		armor_bountyhunter_belt = { type=factionRewardType.armor, display="Bounty Hunter Armor Belt", item="object/tangible/loot/loot_schematic/death_watch_bounty_hunter_belt_schematic.iff", cost=1600},
		armor_mandalorian_helmet = { type=factionRewardType.armor, display="Mandalorian Armor Helmet", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_helmet_schematic.iff", cost=2600},
		armor_mandalorian_chest = { type=factionRewardType.armor, display="Mandalorian Armor Chestplate", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_legs = { type=factionRewardType.armor, display="Mandalorian Armor Legs", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_leggings_schematic.iff", cost=2000},
		armor_mandalorian_boots = { type=factionRewardType.armor, display="Mandalorian Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_boots_schematic.iff", cost=1600},
		armor_mandalorian_bicep_l = { type=factionRewardType.armor, display="Mandalorian Armor Left Bicep", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_bicep_r = { type=factionRewardType.armor, display="Mandalorian Armor Right Bicep", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_bracer_l = { type=factionRewardType.armor, display="Mandalorian Armor Left Bracer", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_bracer_r = { type=factionRewardType.armor, display="Mandalorian Armor Right Bracer", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_boots = { type=factionRewardType.armor, display="Mandalorian Armor Boots", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_boots_schematic.iff", cost=1600},
		armor_mandalorian_gloves = { type=factionRewardType.armor, display="Mandalorian Armor Gloves", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_gloves_schematic.iff", cost=1600},
		armor_mandalorian_belt = { type=factionRewardType.armor, display="Mandalorian Armor Belt", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_belt_schematic.iff", cost=1600},
		armor_mandalorian_rebel_helmet = { type=factionRewardType.armor, display="Crusader MK II Helmet", item="object/tangible/loot/loot_schematic/mandalorian_rebel_helmet_schematic.iff", cost=2600},
		armor_mandalorian_rebel_chest = { type=factionRewardType.armor, display="Crusader MK II Chestplate", item="object/tangible/loot/loot_schematic/mandalorian_rebel_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_rebel_legs = { type=factionRewardType.armor, display="Crusader MK II Legs", item="object/tangible/loot/loot_schematic/mandalorian_rebel_leggings_schematic.iff", cost=2000},
		armor_mandalorian_rebel_boots = { type=factionRewardType.armor, display="Crusader MK II Boots", item="object/tangible/loot/loot_schematic/mandalorian_rebel_boots_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bicep_l = { type=factionRewardType.armor, display="Crusader MK II Left Bicep", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bicep_r = { type=factionRewardType.armor, display="Crusader MK II Right Bicep", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bracer_l = { type=factionRewardType.armor, display="Crusader MK II Left Bracer", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_rebel_bracer_r = { type=factionRewardType.armor, display="Crusader MK II Right Bracer", item="object/tangible/loot/loot_schematic/mandalorian_rebel_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_rebel_boots = { type=factionRewardType.armor, display="Crusader MK II Boots", item="object/tangible/loot/loot_schematic/mandalorian_rebel_boots_schematic.iff", cost=1600},
		armor_mandalorian_rebel_gloves = { type=factionRewardType.armor, display="Crusader MK II Gloves", item="object/tangible/loot/loot_schematic/mandalorian_rebel_gloves_schematic.iff", cost=1600},
		armor_mandalorian_rebel_belt = { type=factionRewardType.armor, display="Crusader MK II Belt", item="object/tangible/loot/loot_schematic/mandalorian_rebel_belt_schematic.iff", cost=1600},
	armor_mandalorian_imperial_helmet = { type=factionRewardType.armor, display="Crusader MK III Helmet", item="object/tangible/loot/loot_schematic/mandalorian_imperial_helmet_schematic.iff", cost=2600},
		armor_mandalorian_imperial_chest = { type=factionRewardType.armor, display="Crusader MK III Chestplate", item="object/tangible/loot/loot_schematic/mandalorian_imperial_chest_plate_schematic.iff", cost=2600},
		armor_mandalorian_imperial_legs = { type=factionRewardType.armor, display="Crusader MK III Legs", item="object/tangible/loot/loot_schematic/mandalorian_imperial_leggings_schematic.iff", cost=2000},
		armor_mandalorian_imperial_boots = { type=factionRewardType.armor, display="Crusader MK III Boots", item="object/tangible/loot/loot_schematic/mandalorian_imperial_boots_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bicep_l = { type=factionRewardType.armor, display="Crusader MK III Left Bicep", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bicep_l_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bicep_r = { type=factionRewardType.armor, display="Crusader MK III Right Bicep", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bicep_r_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bracer_l = { type=factionRewardType.armor, display="Crusader MK III Left Bracer", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bracer_l_schematic.iff", cost=1600},
		armor_mandalorian_imperial_bracer_r = { type=factionRewardType.armor, display="Crusader MK III Right Bracer", item="object/tangible/loot/loot_schematic/mandalorian_imperial_bracer_r_schematic.iff", cost=1600},
		armor_mandalorian_imperial_boots = { type=factionRewardType.armor, display="Crusader MK III Boots", item="object/tangible/loot/loot_schematic/mandalorian_imperial_boots_schematic.iff", cost=1600},
		armor_mandalorian_imperial_gloves = { type=factionRewardType.armor, display="Crusader MK III Gloves", item="object/tangible/loot/loot_schematic/mandalorian_imperial_gloves_schematic.iff", cost=1600},
		armor_mandalorian_imperial_belt = { type=factionRewardType.armor, display="Crusader MK III Belt", item="object/tangible/loot/loot_schematic/mandalorian_imperial_belt_schematic.iff", cost=1600}
		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_imperial"
	},

	installations = {
		--hq_s01_pvp_imperial = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_imperial", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_imperial","hq_s01_imperial"} }
	},

	furnitureList = {
		"art_large", "art_large_s02", "art_small_s01", "art_small_s02", "art_small_s03", "art_small_s04"
	},

	furniture = {
		art_large = { type=factionRewardType.container, display="Mandalorian Art Style 1", item="object/tangible/furniture/all/frn_all_decorative_lg_s1.iff", cost=500},
		art_large_s02 = { type=factionRewardType.container, display="Mandalorian Art Style 2", item="object/tangible/furniture/all/frn_all_decorative_lg_s2.iff", cost=500},
		art_small_s01 = { type=factionRewardType.furniture, display="Mandalorian Art Style 3", item="object/tangible/furniture/all/frn_all_decorative_sm_s1.iff", cost=200},
		art_small_s02 = { type=factionRewardType.furniture, display="Mandalorian Art Style 4", item="object/tangible/furniture/all/frn_all_decorative_sm_s2.iff", cost=200},
		art_small_s03 =  { type=factionRewardType.container, display="Mandalorian Art Style 5", item="object/tangible/furniture/all/frn_all_decorative_sm_s3.iff", cost=200},
		art_small_s04 = { type=factionRewardType.furniture, display="Mandalorian Art Style 6", item="object/tangible/furniture/all/frn_all_decorative_sm_s4.iff", cost=200},
	},

	hirelingList = {
		--"henchman"
	},

	hirelings = {
		--henchman = { type=factionRewardType.hireling, display="@mob/creature_names:mand_bunker_dthwatch_grey", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="death_watch_ghost_pet", cost=700}
	},

	deedListing = {
		"mandolorian_jetpack", "merr_sonn", "vip_bunker"
	},

	deedList = {
		mandolorian_jetpack = { type=factionRewardType.deed, display="Mitrinomon Z-6 Jetpack Schematic", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_jetpack_schematic.iff", cost=24000},
		merr_sonn = { type=factionRewardType.deed, display="Merr Sonn JT-12 Jetpack Schematic", item="object/tangible/loot/loot_schematic/death_watch_mandalorian_jetpack_s02_schematic.iff", cost=24000},
		vip_bunker = { type=factionRewardType.deed, display="VIP Bunker Deed", item="object/tangible/deed/player_house_deed/vipbunker_house_deed.iff", cost=12500},
	},
}
