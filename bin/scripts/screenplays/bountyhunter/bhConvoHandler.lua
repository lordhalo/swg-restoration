local ObjectManager = require("managers.object.object_manager")

BhConvoHandler = Object:new {}

bhVendor_handler = Object:new {
	
 }

function bhVendor_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local hasBlackSun = creature:hasSkill("black_sun_novice")

				if (hasDeathWatch == true or hasBlackSun == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("greet_hated")
				end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function bhVendor_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)

		if (screenID == "greet_friend") then
			if (bhScreenplay:getFactionFromHashCode(player:getFaction()) == "rebel") then
				--screen:addOption("@conversation/faction_recruiter_rebel:s_480", "faction_purchase")
			else
				--screen:addOption("@conversation/faction_recruiter_imperial:s_324", "faction_purchase")
			end
		end
			
		if (screenID == "fp_furniture" or screenID == "fp_weapons_armor" or screenID == "fp_installations" or screenID == "fp_uniforms" or screenID == "fp_hirelings" or screenID == "fp_schematics") then
			bhScreenplay:sendPurchaseSui(conversingNPC, conversingPlayer, screenID)
		end

		--if (screenID == "binary_liquid") then

		--	bhScreenplay:handleBinaryLiquid(conversingNPC, conversingPlayer)

		--end
	
	return conversationScreen
end
