local ObjectManager = require("managers.object.object_manager")

deathWatchBunker = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "deathWatchBunker",

	lockedCells = { 5996316, 5996326, 5996338, 5996352, 5996374, 5996370, 5996368 },
	lockedRoom = { 5996320, 5996322},

	doorData = {
		{ cellAccess = 5996316, lockTime = 10, doorType = 1 }, -- Entrance
		{ cellAccess = 5996326, lockTime = 10, doorType = 2 }, -- Area A
		{ cellAccess = 5996338, lockTime = 10, doorType = 2 }, -- Area B
		{ cellAccess = 5996352, lockTime = 10, doorType = 2 }, -- Mines
		{ cellAccess = 5996374, lockTime = 6, doorType = 3 }, -- Armorsmith area
		{ cellAccess = 5996370, lockTime = 6, doorType = 3 }, -- DE area
		{ cellAccess = 5996368, lockTime = 6, doorType = 3 } -- Tailor area
	},

}
registerScreenPlay("deathWatchBunker", true)

function deathWatchBunker:start()
	if (isZoneEnabled("endor")) then
		local pBunker = getSceneObject(5996314)
		self:spawnMobiles()
		--self:spawnLootBoxes()
		self:spawnIG()
		self:spawnBossk()
		self:spawnBoba()
		--self:spawnMega()
		self:spawnLom()
		self:spawnFiller()
		self:spawnCommonerQuest()
		self:spawnSceneObjects()

		createObserver(ENTEREDBUILDING, "deathWatchBunker", "onEnterDWB", pBunker)


	end
end

function deathWatchBunker:spawnSceneObjects()
	spawnSceneObject("endor", "object/tangible/furniture/modern/bar_piece_straight_s2.iff", 115.0, -64.0, -129.8, 5996348, 1, 0, 0, 0)
	spawnSceneObject("endor", "object/tangible/food/crafted/drink_mandalorian_wine.iff", 115.0, -50.0, -129.8, 5996348, 1, 0, 0, 0)
end

function deathWatchBunker:spawnFiller()
	local pComOne = spawnMobile("endor", "judge", 0, -127.5, -20.0, -58.2, 0, 5996325)
	self:setMoodString(pComOne, "npc_sitting_chair")

	spawnMobile("endor", "r4", 0, -31.1, -20.0, -57.2, -10, 5996331)
	spawnMobile("endor", "eg6_power_droid", 0, -13.8, -22.0, -53.1, -90, 5996331)

	local pComTwo = spawnMobile("endor", "chiss_male", 0, 10.3, -22.0, -59.8, -90, 5996332)
	self:setMoodString(pComTwo, "conversation")

	local pComThree = spawnMobile("endor", "ithorian_male", 0, 8.2, -22.0, -59.2, 98, 5996332)
	self:setMoodString(pComThree, "conversation")

	local pComFour = spawnMobile("endor", "entertainer", 0, 106.2, -64.0, -136.9, 1, 5996348)
	self:setMoodString(pComFour, "entertained")

	local pComFive = spawnMobile("endor", "bartender", 0, 113.5, -64.0, -129.0, -180, 5996348)
	self:setMoodString(pComFive, "npc_standing_drinking")

	local pComSix = spawnMobile("endor", "patron", 0, 113.5, -64.0, -131.0, 1, 5996348)
	self:setMoodString(pComSix, "npc_standing_drinking")

	local pComSeven = spawnMobile("endor", "businessman", 0, 107.5, -64.0, -145.0, -75, 5996348)
	self:setMoodString(pComSeven, "worried")

	local pComEight = spawnMobile("endor", "contractor", 0, 104.5, -64.0, -143.0, 120, 5996348)
	self:setMoodString(pComEight, "conversation")

	local pComNine = spawnMobile("endor", "businessman", 0, 106.5, -64.0, -133.0, 177, 5996348)
	self:setMoodString(pComNine, "conversation")

	local pNPC = spawnMobile("endor", "entertainer", 0, 103.5, -64.0, -135.0, 90, 5996348)
	self:setMoodString(pNPC, "themepark_oola")

	local pNPC = spawnMobile("endor", "patron_ithorian", 0, 111.5, -64.0, -130.0, 90, 5996348)
	self:setMoodString(pNPC, "npc_standing_drinking")

	spawnMobile("endor", "r3", 0, 113.5, -64.0, -138.0, -90, 5996348)
end

function deathWatchBunker:spawnCommonerQuest()
		local random = math.random(99)

		if random < 10 then
		    self:spawnCommonOne()
		elseif random < 40 then
		    self:spawnCommonTwo()
		elseif random < 98 then
		    self:spawnCommonThree()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnCommon", nil, "")
		end
end

function deathWatchBunker:spawnCommonOne()
	local pQuestGiver = spawnMobile("endor", "common_daily_one", 0, 102.6, -64.0, -150.5, 1, 5996348)
	self:setMoodString(pQuestGiver, "npc_sitting_ground")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnCommon", pQuestGiver, "")
end

function deathWatchBunker:spawnCommonTwo()
	local pQuestGiver = spawnMobile("endor", "common_daily_two", 0, 114.6, -64.0, -131.0, 1, 5996348)
	self:setMoodString(pQuestGiver, "npc_standing_drinking")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnCommon", pQuestGiver, "")
end

function deathWatchBunker:spawnCommonThree()
	local pQuestGiver = spawnMobile("endor", "common_daily_three", 0, 104.9, -64.0, -133.9, -133, 5996348)
	self:setMoodString(pQuestGiver, "entertained")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnCommon", pQuestGiver, "")
end

function deathWatchBunker:respawnCommon(pNPC)
	if pNPC ~=  nil then
		SceneObject(pNPC):destroyObjectFromWorld()
	end

	self:spawnCommonerQuest()
end

function deathWatchBunker:spawnMega()

		local random = math.random(99)

		if random < 10 then
		    self:spawnIgEpic()
		elseif random < 40 then
		    self:spawnIgEpic()
		elseif random < 90 then
		    self:spawnIgEpic()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnMega", nil, "")
		end

end

function deathWatchBunker:spawnIgEpic()
	local pQuestGiver = spawnMobile("endor", "ig88_daily_medium", 0, 68.9, -52.0, -135.5, -90, 5996348)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnMega", pQuestGiver, "")
end

function deathWatchBunker:respawnMega(pNPC)
	if pNPC ~=  nil then
		SceneObject(pNPC):destroyObjectFromWorld()
	end

	self:spawnMega()
end

function deathWatchBunker:spawnIG()

		local random = math.random(99)

		if random < 10 then
		    self:spawnIGRare()
		elseif random < 40 then
		    self:spawnIGUncommon()
		elseif random < 90 then
		    self:spawnIGCommon()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnIG", nil, "")
		end

end

function deathWatchBunker:spawnIGRare()
	local pQuestGiver = spawnMobile("endor", "ig88_daily_hard", 0, -30.0, -20.0, -55.1, -90, 5996331)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnIG", pQuestGiver, "")
end

function deathWatchBunker:spawnIGUncommon()
	local pQuestGiver = spawnMobile("endor", "ig88_daily_medium", 0, -30.0, -20.0, -55.1, -90, 5996331)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnIG", pQuestGiver, "")
end

function deathWatchBunker:spawnIGCommon()
	local pQuestGiver = spawnMobile("endor", "ig88_daily_easy", 0, -30.0, -20.0, -55.1, -90, 5996331)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnIG", pQuestGiver, "")
end

function deathWatchBunker:respawnIG(pIG)
	if pIG ~=  nil then
		SceneObject(pIG):destroyObjectFromWorld()
	end
	self:spawnIG()
end

function deathWatchBunker:spawnBossk()

		local random = math.random(99)

		if random < 10 then
		    self:spawnBosskRare()
		elseif random < 40 then
		    self:spawnBosskUncommon()
		elseif random < 90 then
		    self:spawnBosskCommon()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBossk", nil, "")
		end

end

function deathWatchBunker:spawnBosskRare()
	local pQuestGiver = spawnMobile("endor", "bossk_daily_hard", 0, -129.1, -20.0, -56.6, 90, 5996325)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBossk", pQuestGiver, "")
end

function deathWatchBunker:spawnBosskUncommon()
	local pQuestGiver = spawnMobile("endor", "bossk_daily_medium", 0, -129.1, -20.0, -56.6, 90, 5996325)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBossk", pQuestGiver, "")
end

function deathWatchBunker:spawnBosskCommon()
	local pQuestGiver = spawnMobile("endor", "bossk_daily_easy", 0, -129.1, -20.0, -56.6, 90, 5996325)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBossk", pQuestGiver, "")
end

function deathWatchBunker:respawnBossk(pBossk)
	if pBossk ~=  nil then
		SceneObject(pBossk):destroyObjectFromWorld()
	end
	self:spawnBossk()
end

function deathWatchBunker:spawnBoba()

		local random = math.random(99)

		if random < 10 then
		    self:spawnBobaRare()
		elseif random < 40 then
		    self:spawnBobaUncommon()
		elseif random < 90 then
		    self:spawnBobaCommon()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBoba", nil, "")
		end

end

function deathWatchBunker:spawnBobaRare()
	local pQuestGiver = spawnMobile("endor", "boba_daily_hard", 0, 110.8, -64.0, -135.3, -90, 5996348)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBoba", pQuestGiver, "")
	self:setMoodString(pQuestGiver, "bored")
end

function deathWatchBunker:spawnBobaUncommon()
	local pQuestGiver = spawnMobile("endor", "boba_daily_medium", 0, 110.8, -64.0, -135.3, -90, 5996348)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBoba", pQuestGiver, "")
	self:setMoodString(pQuestGiver, "bored")
end

function deathWatchBunker:spawnBobaCommon()
	local pQuestGiver = spawnMobile("endor", "boba_daily_easy", 0, 110.8, -64.0, -135.3, -90, 5996348)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnBoba", pQuestGiver, "")
end


function deathWatchBunker:respawnBoba(pBoba)
	if pBoba ~=  nil then
		SceneObject(pBoba):destroyObjectFromWorld()
	end
	self:spawnBoba()
end

function deathWatchBunker:spawnLom()

		local random = math.random(99)

		if random < 10 then
		    self:spawnLomRare()
		elseif random < 40 then
		    self:spawnLomUncommon()
		elseif random < 90 then
		    self:spawnLomCommon()
		else
		    createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnLom", nil, "")
		end

end

function deathWatchBunker:spawnLomRare()
	local pQuestGiver = spawnMobile("endor", "lom_daily_hard", 0, 8.1, -22.0, -59.9, 80, 5996332)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnLom", pQuestGiver, "")
end

function deathWatchBunker:spawnLomUncommon()
	local pQuestGiver = spawnMobile("endor", "lom_daily_medium", 0, 8.1, -22.0, -59.9, 80, 5996332)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnLom", pQuestGiver, "")
end

function deathWatchBunker:spawnLomCommon()
	local pQuestGiver = spawnMobile("endor", "lom_daily_easy", 0, 8.1, -22.0, -59.9, 80, 5996332)
	createEvent(720 * 60 * 1000, "deathWatchBunker", "respawnLom", pQuestGiver, "")
end

function deathWatchBunker:respawnLom(pLom)
	if pLom ~=  nil then
		SceneObject(pLom):destroyObjectFromWorld()
	end
	self:spawnLom()
end

function deathWatchBunker:spawnMobiles()

	spawnMobile("endor", "mandalorian_vendor_rebel", 0, -2.9, -12.0, 35.8, 176, 5996318)

	--spawnMobile("endor", "deathwatchrankone", 0, -37.5, -12.0, -6.9, 90, 5996315)

	spawnMobile("endor", "tungo_li", 0, -0.5, -12.0, 26.3, 1, 5996318)

	spawnMobile("endor", "jekk_bunker", 0, -33.1, -20.0, -55.2, 88, 5996331)

	spawnMobile("endor", "death_watch_mha_quest", 0, -85.4, -20.0, -120.9, 1, 5996329)

	for i,v in ipairs(deathWatchStaticSpawns) do
		spawnMobile("endor", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
	end
	--for i,v in ipairs(deathWatchQuestNpcs) do
	--	spawnMobile("endor", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
	--end
end

function deathWatchBunker:givePermission(pPlayer, permissionGroup)
	ObjectManager.withCreaturePlayerObject(pPlayer, function(ghost)
		ghost:addPermissionGroup(permissionGroup, true)
	end)
end

function deathWatchBunker:removePermission(pPlayer, permissionGroup)
	ObjectManager.withCreaturePlayerObject(pPlayer, function(ghost)
		if (ghost:hasPermissionGroup(permissionGroup)) then
			ghost:removePermissionGroup(permissionGroup, true)
		end
	end)
end

function deathWatchBunker:onEnterDWB(sceneObject, pMovingObject)

	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end

	--self:setupPermissionGroups()
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			createEvent(1, "hotspot", "handlehotspotZone", pMovingObject)
			--player:sendSystemMessage("You have entered a battlefield!")
		else
			player:sendSystemMessage("You must have a faction to enter.")
			player:teleport(-4711, 15, 4335, 0)
		end

		createObserver(OBJECTDESTRUCTION, "hotspot", "killedPlayer", pMovingObject)
		return 0
	end)
end

function deathWatchBunker:setupPermissionGroups()
	for i = 1, #self.lockedCells, 1 do
		local pCell = getSceneObject(self.lockedCells)
		if pCell ~= nil then
			SceneObject(pCell):setContainerInheritPermissionsFromParent(true)

		end
	end
end

function deathWatchBunker:spawnLootBoxes()
	local spawnedScneObject

	local pBox = spawnSceneObject("endor", "object/tangible/dungeon/coal_bin_container.iff",6.01353,-32,-102.05,5996337,0.707107,0,0.707107,0)
	--spawnedSceneObject:_setObject(pBox)
	self:setLootBoxPermissions(pBox)
	--writeData(pBox:getObjectID() .. ":dwb:lootbox", 2)
	createEvent(1000, "deathWatchBunker", "refillContainer", pBox)
	createObserver(OBJECTRADIALUSED, "deathWatchBunker", "boxLooted", pBox)
end

function deathWatchBunker:refillContainer(pSceneObject)
	if (pSceneObject == nil) then
		return
	end

	--writeData(SceneObject(pSceneObject):getObjectID() .. ":dwb:spawned", 0)

	if (SceneObject(pSceneObject):getContainerObjectsSize() == 0) then
		createLoot(pSceneObject, "death_watch_bunker_lootbox", 1, false)
		if getRandomNumber(100) > 95 then
			createLoot(pSceneObject, "death_watch_bunker_lootbox", 1, false)
		end
	end
end

function deathWatchBunker:setLootBoxPermissions(pContainer)
	ObjectManager.withSceneObject(pContainer, function(container)
		container:setContainerInheritPermissionsFromParent(false)
		container:setContainerDefaultDenyPermission(MOVEIN)
		container:setContainerDefaultAllowPermission(OPEN + MOVEOUT)
	end)
end

function deathWatchBunker:boxLooted(pSceneObject, pCreature, selectedID)
	if selectedID ~= 16 then
		return 0
	end

	local creature = LuaCreatureObject(pCreature)

	--local hasDeathWatch = creature:hasSkill("death_watch_novice")
	--local hasdeathWatch = creature:hasSkill("death_watch_novice")

	--if(hasDeathWatch == true) then
	--	return 0
	--end


	local objectID = SceneObject(pSceneObject):getObjectID()
	--if readData(objectID .. ":dwb:spawned") ~= 1 then
	--	local boxId = readData(objectID .. ":dwb:lootbox")
		--writeData(objectID .. ":dwb:spawned", 1)

		--spawn enemies
		--if boxId == 1 then
		--	local spawn = deathWatchSpecialSpawns["lootbox1mob1"]
		--	spawnMobile("endor", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--elseif boxId == 2 then
		--	local spawn = deathWatchSpecialSpawns["lootbox2mob1"]
		--	spawnMobile("endor", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--elseif boxId == 3 then
		--	local spawn = deathWatchSpecialSpawns["lootbox3mob1"]
		--	spawnMobile("endor", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--	local spawn = deathWatchSpecialSpawns["lootbox3mob2"]
		--	spawnMobile("endor", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
	--	end

		createEvent(self.containerRespawnTime, "DeathWatchBunkerScreenPlay", "refillContainer", pSceneObject)
	--end

	return 0
end

