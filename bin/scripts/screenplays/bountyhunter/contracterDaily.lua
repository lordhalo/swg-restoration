local ObjectManager = require("managers.object.object_manager")

contracterDaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "contracterDaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	valarin = {
		{"valarian_swooper_quest", 300, 4727, 52, 2133, 1, 0},
		{"valarian_swooper_quest", 300, 4785, 50, 2221, 1, 0},
		{"valarian_swooper_quest", 300, 5001, 56, 2008, 1, 0},
		{"valarian_swooper_quest", 300, 4999, 39, 1820, 1, 0},
		{"valarian_swooper_quest", 300, 5363, 37, 1852, 1, 0},
		{"valarian_swooper_quest", 300, 5438, 47, 1964, 1, 0},
		{"valarian_swooper_quest", 300, 5487, 46, 1947, 1, 0},
		{"valarian_swooper_quest", 300, 5466, 26, 2131, 1, 0},
		{"valarian_swooper_quest", 300, 5448, 27, 2139, 1, 0},
		{"valarian_swooper_quest", 300, 5462, 27, 2158, 1, 0},
		{"valarian_swooper_quest", 300, 5459, 28, 2115, 1, 0},
		{"valarian_swooper_quest", 300, 5501, 23, 2292, 1, 0},
		{"valarian_swooper_quest", 300, 5381, 29, 2305, 1, 0},
		{"valarian_swooper_quest", 300, 5191, 43, 2229, 1, 0},
		{"valarian_swooper_quest", 300, 5080, 33, 2185, 1, 0},
		{"valarian_swooper_quest", 300, 4917, 40, 2293, 1, 0},
	},
}
registerScreenPlay("contracterDaily", true)

function contracterDaily:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnValarin()
	end
end

function contracterDaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function contracterDaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function contracterDaily:spawnValarin()

		local mobileTable = self.valarin
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("tatooine", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "contracterDaily", "npcKilled", pMobile)
		end
end

function contracterDaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "contracterDaily")
		if(hasState == true) then
			writeData(player:getObjectID() .. ":valarinKilled", readData(player:getObjectID() .. ":valarinKilled") + 1)
			
			if(readData(player:getObjectID() .. ":valarinKilled") == 5) then
				CreatureObject(playerObject):sendSystemMessage("Mission Complete")
				player:setScreenPlayState(4, contracterDaily.questString)
			end
		end
	return 0
end

function contracterDaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":valarinKilled", 0)
	player:removeScreenPlayState(contracterDaily.states.quest.phasetwo, contracterDaily.questString)
	player:removeScreenPlayState(contracterDaily.states.quest.phaseone, contracterDaily.questString)
	player:removeScreenPlayState(contracterDaily.states.quest.intro, contracterDaily.questString)

end

contracter_daily_convo_handler = Object:new {
	
 }

function contracter_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local hasBlackSun = creature:hasSkill("black_sun_novice")
			local firstQuestStarted = creature:hasScreenPlayState(2, "contracterDaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "contracterDaily")
			local dailyReset = creature:hasScreenPlayState(8, "contracterDaily")

				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (hasBlackSun == true or hasDeathWatch == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function contracter_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("tatooine", "Valerian Bounty", "Kill 5 Valerian", 5260, 1915, 5, true, true, 0)
		player:setScreenPlayState(2, contracterDaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, contracterDaily.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 100)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 100)
			end)
		end

		CreatureObject(conversingPlayer):addCashCredits(50000, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", 50000) 
		createEvent(true, 86400000, "contracterDaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(contracterDaily.states.quest.phasethree, contracterDaily.questString)
	end
	


	return conversationScreen
end

