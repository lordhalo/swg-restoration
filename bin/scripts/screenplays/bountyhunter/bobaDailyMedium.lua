local ObjectManager = require("managers.object.object_manager")

bobaDailyMedium = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bobaDailyMedium",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bobaDailyMedium", true)

function bobaDailyMedium:start()
	if (isZoneEnabled("dantooine")) then
		self:spawnTargets()
	end
end

function bobaDailyMedium:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bobaDailyMedium:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bobaDailyMedium:spawnTargets()

	local pKahmurra = spawnMobile("dantooine", "boba_warren_medium", math.random(600) + 120, 20.5, -84.0, -27.0,  -20, 8575745)
	createObserver(OBJECTDESTRUCTION, "bobaDailyMedium", "npcKilled", pKahmurra)

end

function bobaDailyMedium:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bobaDailyMedium")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bobaDailyMedium")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bobaDailyMedium.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bobaDailyMedium")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bobaDailyMedium.questString)
				end
			end
		end)
	return 0
end

function bobaDailyMedium:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bobaDailyMedium.states.quest.phasetwo, bobaDailyMedium.questString)
	player:removeScreenPlayState(bobaDailyMedium.states.quest.phaseone, bobaDailyMedium.questString)
	player:removeScreenPlayState(bobaDailyMedium.states.quest.intro, bobaDailyMedium.questString)

end

boba_daily_medium_convo_handler = Object:new {
	
 }

function boba_daily_medium_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bobaDailyMedium")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bobaDailyMedium")
			local dailyReset = creature:hasScreenPlayState(8, "bobaDailyMedium")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function boba_daily_medium_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("dantooine", "The Warren", "", -580, -3763, 5, true, true, 0)	
		player:setScreenPlayState(2, bobaDailyMedium.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		questNPC:doAnimation("nod")
		player:doAnimation("rub_chin_thoughtful")

		player:setScreenPlayState(8, bobaDailyMedium.questString)
		if (player:isRebel() == true) then
			player:awardExperience("force_rank_xp", 375, true)
		end

		if (player:isImperial() == true) then
			player:awardExperience("force_rank_xp", 375, true)
		end
		local amount = 200000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "bobaDailyMedium", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bobaDailyMedium.states.quest.phasethree, bobaDailyMedium.questString)
	end
	


	return conversationScreen
end

