local ObjectManager = require("managers.object.object_manager")

commonDailyOne = ScreenPlay:new {
	numberOfActs = 1,
	questString = "commonDailyOne",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("commonDailyOne", true)

function commonDailyOne:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnTargets()
	end
end

function commonDailyOne:getActivePlayerName()
	return self.questdata.activePlayerName
end

function commonDailyOne:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function commonDailyOne:spawnTargets()

	local pNPC = spawnMobile("tatooine", "common_daily_tusken", math.random(600) + 120, -6237, 10.8, 1832,  70, 0)
	createObserver(OBJECTDESTRUCTION, "commonDailyOne", "npcKilled", pNPC)

end

function commonDailyOne:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "commonDailyOne")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "commonDailyOne")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, commonDailyOne.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "commonDailyOne")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, commonDailyOne.questString)
				end
			end
		end)
	return 0
end

function commonDailyOne:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(commonDailyOne.states.quest.phasetwo, commonDailyOne.questString)
	player:removeScreenPlayState(commonDailyOne.states.quest.phaseone, commonDailyOne.questString)
	player:removeScreenPlayState(commonDailyOne.states.quest.intro, commonDailyOne.questString)

end

common_daily_one_convo_handler = Object:new {
	
 }

function common_daily_one_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "commonDailyOne")
			local firstQuestComplete = creature:hasScreenPlayState(4, "commonDailyOne")
			local dailyReset = creature:hasScreenPlayState(8, "commonDailyOne")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function common_daily_one_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("tatooine", "Warlord", "", 7373, 115, 5, true, true, 0)	
		player:setScreenPlayState(2, commonDailyOne.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, commonDailyOne.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 300)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 300)
			end)
		end
		createEvent(82800000, "commonDailyOne", "removeDailyTimer", conversingPlayer, "")

		local amount = 30000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(commonDailyOne.states.quest.phasethree, commonDailyOne.questString)
	end
	


	return conversationScreen
end

