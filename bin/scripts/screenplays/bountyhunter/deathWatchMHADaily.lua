local ObjectManager = require("managers.object.object_manager")

deathWatchMHADaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "deathWatchMHADaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	kobola = {
		{"black_sun_henchman_quest", 360,  -4593.0, 15.0, -383.0, -5, 0},
		{"black_sun_henchman_quest", 360,  -4596.0, 15.0, -383.0, -15, 0},
		{"black_sun_henchman_quest", 360,  -4599.0, 15.0, -383.0, -15, 0},
		{"black_sun_henchman_quest", 360,  -4591.0, 15.0, -402.0, 115, 0},
		{"black_sun_henchman_quest", 360,  -4606.0, 15.0, -424.0, -154, 0},
		{"black_sun_henchman_quest", 360,  -4630.0, 15.0, -423.0, -70, 0},
		{"black_sun_henchman_quest", 360,  -4635.0, 15.0, -391.0, -22, 0},
		{"black_sun_henchman_quest", 360,  -4636.0, 15.0, -371.0, -30, 0},
		{"black_sun_henchman_quest", 360,  -4585.0, 15.0, -354, -160, 0},
		{"black_sun_henchman_quest", 360,  -4567.0, 15.0, -367, -134, 0},
		{"black_sun_henchman_quest", 360,  -4554.0, 15.0, -397, 128, 0},
		
	},

}
registerScreenPlay("deathWatchMHADaily", true)

function deathWatchMHADaily:start()
	if (isZoneEnabled("endor")) then
		self:spawnDeathWatch()
	end
end

function deathWatchMHADaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function deathWatchMHADaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function deathWatchMHADaily:spawnDeathWatch()

		local mobileTable = self.kobola
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("endor", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "deathWatchMHADaily", "npcKilled", pMobile)
		end
end

function deathWatchMHADaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "deathWatchMHADaily")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":bsHenchmenKilled", readData(groupMember:getObjectID() .. ":bsHenchmenKilled") + 1)
							
							if(readData(groupMember:getObjectID() .. ":bsHenchmenKilled") == 10) then
								groupMember:sendSystemMessage("Mission Complete")
								groupMember:setScreenPlayState(4, deathWatchMHADaily.questString)
								groupMember:playMusicMessage("sound/ui_button_random.snd")
							end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "deathWatchMHADaily")
					
					if(hasState == true) then 
						writeData(player:getObjectID() .. ":bsHenchmenKilled", readData(player:getObjectID() .. ":bsHenchmenKilled") + 1)

						if(readData(player:getObjectID() .. ":bsHenchmenKilled") == 10) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, deathWatchMHADaily.questString)
							player:playMusicMessage("sound/ui_button_random.snd")
						end
					end
			end
		end)
	return 0
end

function deathWatchMHADaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":bsHenchmenKilled", 0)
	player:removeScreenPlayState(deathWatchMHADaily.states.quest.phasetwo, deathWatchMHADaily.questString)
	player:removeScreenPlayState(deathWatchMHADaily.states.quest.phaseone, deathWatchMHADaily.questString)
	player:removeScreenPlayState(deathWatchMHADaily.states.quest.intro, deathWatchMHADaily.questString)

end

death_watch_mha_daily_convo_handler = Object:new {
	
 }

function death_watch_mha_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local hasdeathWatch = creature:hasSkill("death_watch_novice")
			local firstQuestStarted = creature:hasScreenPlayState(2, "deathWatchMHADaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "deathWatchMHADaily")
			local dailyReset = creature:hasScreenPlayState(8, "deathWatchMHADaily")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("done_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (hasdeathWatch == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function death_watch_mha_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("endor", "Black Sun Camp", "Kill 10 Black Sun", -4633, -423, 5, true, true, 0)
		player:setScreenPlayState(2, deathWatchMHADaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, deathWatchMHADaily.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 25)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 25)
			end)
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "hardening_agent", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		createEvent(true, 82800000, "deathWatchMHADaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(deathWatchMHADaily.states.quest.phasethree, deathWatchMHADaily.questString)
	end
	


	return conversationScreen
end

