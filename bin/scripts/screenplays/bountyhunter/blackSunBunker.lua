local ObjectManager = require("managers.object.object_manager")

blackSunBunker = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "blackSunBunker",

	lockedCells = { 5996316, 5996326, 5996338, 5996352, 5996374, 5996370, 5996368 },
	lockedRoom = { 5996320, 5996322},

}
registerScreenPlay("blackSunBunker", true)

function blackSunBunker:start()
	if (isZoneEnabled("lok")) then
		local pBunker = getSceneObject(9685999)
		self:spawnMobiles()
		--self:spawnLootBoxes()
		self:spawnIG()
		self:spawnBossk()
		self:spawnBoba()
		--self:spawnMega()
		self:spawnLom()
		self:spawnFiller()
		self:spawnCommonerQuest()
		self:spawnSceneObjects()

		createObserver(ENTEREDBUILDING, "blackSunBunker", "onEnterDWB", pBunker)
	end
end

function blackSunBunker:spawnSceneObjects()
	spawnSceneObject("lok", "object/tangible/furniture/modern/bar_piece_straight_s2.iff", 115.0, -64.0, -129.8, 9686033, 1, 0, 0, 0)
	spawnSceneObject("lok", "object/tangible/food/crafted/drink_mandalorian_wine.iff", 115.0, -50.0, -129.8, 9686033, 1, 0, 0, 0)
end

function blackSunBunker:spawnFiller()
	local pComOne = spawnMobile("lok", "judge", 0, -127.5, -20.0, -58.2, 0, 9686010)
	self:setMoodString(pComOne, "npc_sitting_chair")

	spawnMobile("lok", "r4", 0, -31.1, -20.0, -57.2, -10, 9686016)
	spawnMobile("lok", "eg6_power_droid", 0, -13.8, -22.0, -53.1, -90, 9686016)

	local pComTwo = spawnMobile("lok", "chiss_male", 0, 10.3, -22.0, -59.8, -90, 9686017)
	self:setMoodString(pComTwo, "conversation")

	local pComThree = spawnMobile("lok", "ithorian_male", 0, 8.2, -22.0, -59.2, 98, 9686017)
	self:setMoodString(pComThree, "conversation")

	local pComFour = spawnMobile("lok", "entertainer", 0, 106.2, -64.0, -136.9, 1, 9686033)
	self:setMoodString(pComFour, "entertained")

	local pComFive = spawnMobile("lok", "bartender", 0, 113.5, -64.0, -129.0, -180, 9686033)
	self:setMoodString(pComFive, "npc_standing_drinking")

	local pComSix = spawnMobile("lok", "patron", 0, 113.5, -64.0, -131.0, 1, 9686033)
	self:setMoodString(pComSix, "npc_standing_drinking")

	local pComSeven = spawnMobile("lok", "businessman", 0, 107.5, -64.0, -145.0, -75, 9686033)
	self:setMoodString(pComSeven, "worried")

	local pComEight = spawnMobile("lok", "contractor", 0, 104.5, -64.0, -143.0, 120, 9686033)
	self:setMoodString(pComEight, "conversation")

	local pComNine = spawnMobile("lok", "businessman", 0, 106.5, -64.0, -133.0, 177, 9686033)
	self:setMoodString(pComNine, "conversation")

	local pNPC = spawnMobile("lok", "entertainer", 0, 103.5, -64.0, -135.0, 90, 9686033)
	self:setMoodString(pNPC, "themepark_oola")

	local pNPC = spawnMobile("lok", "patron_ithorian", 0, 111.5, -64.0, -130.0, 90, 9686033)
	self:setMoodString(pNPC, "npc_standing_drinking")

	spawnMobile("lok", "r3", 0, 113.5, -64.0, -138.0, -90, 9686033)
end

function blackSunBunker:spawnCommonerQuest()
		local random = math.random(99)

		if random < 10 then
		    self:spawnCommonOne()
		elseif random < 40 then
		    self:spawnCommonTwo()
		elseif random < 98 then
		    self:spawnCommonThree()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnCommon", nil, "")
		end
end

function blackSunBunker:spawnCommonOne()
	local pQuestGiver = spawnMobile("lok", "common_daily_one", 0, 102.6, -64.0, -150.5, 1, 5996348)
	self:setMoodString(pQuestGiver, "npc_sitting_ground")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnCommon", pQuestGiver, "")
end

function blackSunBunker:spawnCommonTwo()
	local pQuestGiver = spawnMobile("lok", "common_daily_two", 0, 114.6, -64.0, -131.0, 1, 5996348)
	self:setMoodString(pQuestGiver, "npc_standing_drinking")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnCommon", pQuestGiver, "")
end

function blackSunBunker:spawnCommonThree()
	local pQuestGiver = spawnMobile("lok", "common_daily_three", 0, 104.9, -64.0, -133.9, -133, 5996348)
	self:setMoodString(pQuestGiver, "entertained")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnCommon", pQuestGiver, "")
end

function blackSunBunker:respawnCommon(pNPC)
	if pNPC ~=  nil then
		SceneObject(pNPC):destroyObjectFromWorld()
	end

	self:spawnCommonerQuest()
end

function blackSunBunker:spawnIG()

		local random = math.random(99)

		if random < 10 then
		    self:spawnIGRare()
		elseif random < 40 then
		    self:spawnIGUncommon()
		elseif random < 90 then
		    self:spawnIGCommon()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnIG", nil, "")
		end

end

function blackSunBunker:spawnIGRare()
	local pQuestGiver = spawnMobile("lok", "ig88_daily_hard", 0, -30.0, -20.0, -55.1, -90, 9686016)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnIG", pQuestGiver, "")
end

function blackSunBunker:spawnIGUncommon()
	local pQuestGiver = spawnMobile("lok", "ig88_daily_medium", 0, -30.0, -20.0, -55.1, -90, 9686016)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnIG", pQuestGiver, "")
end

function blackSunBunker:spawnIGCommon()
	local pQuestGiver = spawnMobile("lok", "ig88_daily_easy", 0, -30.0, -20.0, -55.1, -90, 9686016)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnIG", pQuestGiver, "")
end

function blackSunBunker:respawnIG(pIG)
	if pIG ~=  nil then
		SceneObject(pIG):destroyObjectFromWorld()
	end
	self:spawnIG()
end

function blackSunBunker:spawnMega()

		local random = math.random(99)

		if random < 10 then
		    self:spawnIgEpic()
		elseif random < 40 then
		    self:spawnIgEpic()
		elseif random < 90 then
		    self:spawnIgEpic()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnMega", nil, "")
		end

end

function blackSunBunker:spawnIgEpic()
	local pQuestGiver = spawnMobile("endor", "ig88_daily_medium", 0, 68.9, -52.0, -135.5, -90, 5996348)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnMega", pQuestGiver, "")
end

function blackSunBunker:respawnMega(pNPC)
	if pNPC ~=  nil then
		SceneObject(pNPC):destroyObjectFromWorld()
	end

	self:spawnMega()
end

function blackSunBunker:spawnBoba()

		local random = math.random(99)

		if random < 10 then
		    self:spawnBobaRare()
		elseif random < 40 then
		    self:spawnBobaUncommon()
		elseif random < 90 then
		    self:spawnBobaCommon()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBoba", nil, "")
		end

end

function blackSunBunker:spawnBobaRare()
	local pQuestGiver = spawnMobile("lok", "boba_daily_hard", 0, 110.8, -64.0, -135.3, -90, 9686033)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBoba", pQuestGiver, "")
	self:setMoodString(pQuestGiver, "bored")
end

function blackSunBunker:spawnBobaUncommon()
	local pQuestGiver = spawnMobile("lok", "boba_daily_medium", 0, 110.8, -64.0, -135.3, -90, 9686033)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBoba", pQuestGiver, "")
	self:setMoodString(pQuestGiver, "bored")
end

function blackSunBunker:spawnBobaCommon()
	local pQuestGiver = spawnMobile("lok", "boba_daily_easy", 0, 110.8, -64.0, -135.3, -90, 9686033)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBoba", pQuestGiver, "")
end


function blackSunBunker:respawnBoba(pBoba)
	if pBoba ~=  nil then
		SceneObject(pBoba):destroyObjectFromWorld()
	end
	self:spawnBoba()
end

function blackSunBunker:spawnBossk()

		local random = math.random(99)

		if random < 10 then
		    self:spawnBosskRare()
		elseif random < 40 then
		    self:spawnBosskUncommon()
		elseif random < 90 then
		    self:spawnBosskCommon()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBossk", nil, "")
		end

end

function blackSunBunker:spawnBosskRare()
	local pQuestGiver = spawnMobile("lok", "bossk_daily_hard", 0, -129.1, -20.0, -56.6, 90, 9686010)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBossk", pQuestGiver, "")
end

function blackSunBunker:spawnBosskUncommon()
	local pQuestGiver = spawnMobile("lok", "bossk_daily_medium", 0, -129.1, -20.0, -56.6, 90, 9686010)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBossk", pQuestGiver, "")
end

function blackSunBunker:spawnBosskCommon()
	local pQuestGiver = spawnMobile("lok", "bossk_daily_easy", 0, -129.1, -20.0, -56.6, 90, 9686010)
	self:setMoodString(pQuestGiver, "npc_sitting_chair")
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnBossk", pQuestGiver, "")
end

function blackSunBunker:respawnBossk(pBossk)
	if pBossk ~=  nil then
		SceneObject(pBossk):destroyObjectFromWorld()
	end
	self:spawnBossk()
end

function blackSunBunker:spawnLom()

		local random = math.random(99)

		if random < 10 then
		    self:spawnLomRare()
		elseif random < 40 then
		    self:spawnLomUncommon()
		elseif random < 90 then
		    self:spawnLomCommon()
		else
		    createEvent(720 * 60 * 1000, "blackSunBunker", "respawnLom", nil, "")
		end

end

function blackSunBunker:spawnLomRare()
	local pQuestGiver = spawnMobile("lok", "lom_daily_hard", 0, 8.1, -22.0, -59.9, 80, 9686017)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnLom", pQuestGiver, "")
end

function blackSunBunker:spawnLomUncommon()
	local pQuestGiver = spawnMobile("lok", "lom_daily_medium", 0, 8.1, -22.0, -59.9, 80, 9686017)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnLom", pQuestGiver, "")
end

function blackSunBunker:spawnLomCommon()
	local pQuestGiver = spawnMobile("lok", "lom_daily_easy", 0, 8.1, -22.0, -59.9, 80, 9686017)
	createEvent(720 * 60 * 1000, "blackSunBunker", "respawnLom", pQuestGiver, "")
end

function blackSunBunker:respawnLom(pLom)
	if pLom ~=  nil then
		SceneObject(pLom):destroyObjectFromWorld()
	end
	self:spawnLom()
end

function blackSunBunker:spawnMobiles()

	spawnMobile("lok", "mandalorian_vendor", 0, -2.9, -12.0, 35.8, 176, 9686003)

	--spawnMobile("lok", "blacksunrankone", 0, -37.5, -12.0, -6.9, 90, 9686000)

	spawnMobile("lok", "aruk_thegreat", 0, -0.5, -12.0, 26.3, 1, 9686003)

	spawnMobile("lok", "rell_bunker", 0, -33.1, -20.0, -55.2, 88, 9686016)

	spawnMobile("lok", "black_sun_mha_quest", 0, -85.4, -20.0, -120.9, 1, 9686014)

	for i,v in ipairs(blackSunStaticSpawns) do
		spawnMobile("lok", v[1], v[2], v[3], v[4], v[5], v[6], v[7])
	end

end

function blackSunBunker:givePermission(pPlayer, permissionGroup)
	ObjectManager.withCreaturePlayerObject(pPlayer, function(ghost)
		ghost:addPermissionGroup(permissionGroup, true)
	end)
end

function blackSunBunker:removePermission(pPlayer, permissionGroup)
	ObjectManager.withCreaturePlayerObject(pPlayer, function(ghost)
		if (ghost:hasPermissionGroup(permissionGroup)) then
			ghost:removePermissionGroup(permissionGroup, true)
		end
	end)
end

function blackSunBunker:onEnterDWB(sceneObject, pMovingObject)
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			createEvent(1, "hotspot", "handlehotspotZone", pMovingObject)
			--player:sendSystemMessage("You have entered a battlefield!")
		else
			player:sendSystemMessage("You must have a faction to enter.")
			player:teleport(-6400, 92, 2871, 0)
		end

		createObserver(OBJECTDESTRUCTION, "hotspot", "killedPlayer", pMovingObject)
		return 0
	end)
end

function blackSunBunker:spawnLootBoxes()
	local spawnedScneObject

	local pBox = spawnSceneObject("lok", "object/tangible/dungeon/coal_bin_container.iff",6.01353,-32,-102.05,9686022,0.707107,0,0.707107,0)
	--spawnedSceneObject:_setObject(pBox)
	self:setLootBoxPermissions(pBox)
	--writeData(pBox:getObjectID() .. ":dwb:lootbox", 2)
	createEvent(1000, "deathWatchBunker", "refillContainer", pBox)
	createObserver(OBJECTRADIALUSED, "deathWatchBunker", "boxLooted", pBox)
end

function blackSunBunker:refillContainer(pSceneObject)
	if (pSceneObject == nil) then
		return
	end

	--writeData(SceneObject(pSceneObject):getObjectID() .. ":dwb:spawned", 0)

	if (SceneObject(pSceneObject):getContainerObjectsSize() == 0) then
		createLoot(pSceneObject, "death_watch_bunker_lootbox", 1, false)
		if getRandomNumber(100) > 95 then
			createLoot(pSceneObject, "death_watch_bunker_lootbox", 1, false)
		end
	end
end

function blackSunBunker:setLootBoxPermissions(pContainer)
	ObjectManager.withSceneObject(pContainer, function(container)
		container:setContainerInheritPermissionsFromParent(false)
		container:setContainerDefaultDenyPermission(MOVEIN)
		container:setContainerDefaultAllowPermission(OPEN + MOVEOUT)
	end)
end

function blackSunBunker:boxLooted(pSceneObject, pCreature, selectedID)
	if selectedID ~= 16 then
		return 0
	end

	local creature = LuaCreatureObject(pCreature)

	--local hasBlackSun = creature:hasSkill("black_sun_novice")
	--local hasdeathWatch = creature:hasSkill("death_watch_novice")

	--if(hasBlackSun == true) then
	--	return 0
	--end

	local objectID = SceneObject(pSceneObject):getObjectID()
	--if readData(objectID .. ":dwb:spawned") ~= 1 then
	--	local boxId = readData(objectID .. ":dwb:lootbox")
		--writeData(objectID .. ":dwb:spawned", 1)

		--spawn enemies
		--if boxId == 1 then
		--	local spawn = deathWatchSpecialSpawns["lootbox1mob1"]
		--	spawnMobile("lok", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--elseif boxId == 2 then
		--	local spawn = deathWatchSpecialSpawns["lootbox2mob1"]
		--	spawnMobile("lok", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--elseif boxId == 3 then
		--	local spawn = deathWatchSpecialSpawns["lootbox3mob1"]
		--	spawnMobile("lok", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
		--	local spawn = deathWatchSpecialSpawns["lootbox3mob2"]
		--	spawnMobile("lok", spawn[1], spawn[2], spawn[3], spawn[4], spawn[5], spawn[6], spawn[7])
	--	end

		createEvent(self.containerRespawnTime, "DeathWatchBunkerScreenPlay", "refillContainer", pSceneObject)
	--end

	return 0
end
