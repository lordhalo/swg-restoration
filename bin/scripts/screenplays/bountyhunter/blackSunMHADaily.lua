local ObjectManager = require("managers.object.object_manager")

blackSunMHADaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "blackSunMHADaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	kobola = {
		{"death_watch_ghost_quest", 360,  -4639.0, 13.0, -406.0, -15, 0},
		{"death_watch_ghost_quest", 360,  -4636.0, 13.0, -406.0, -15, 0},
		{"death_watch_ghost_quest", 360,  -4633.0, 13.0, -406.0, -15, 0},
		{"death_watch_ghost_quest", 360,  -4630.0, 13.0, -406.0, -15, 0},
		{"death_watch_ghost_quest", 360,  -4653.0, 13.0, -431.0, -150, 0},
		{"death_watch_ghost_quest", 360,  -4635.0, 13.0, -442.0, 150, 0},
		{"death_watch_ghost_quest", 360,  -4606.0, 13.0, -441.0, 101, 0},
		{"death_watch_ghost_quest", 360,  -4595.0, 13.0, -425.0, 66, 0},
		{"death_watch_ghost_quest", 360,  -4620.0, 13.0, -423, 102, 0},
		{"death_watch_ghost_quest", 360,  -4650.0, 13.0, -378, -20, 0},
		{"death_watch_ghost_quest", 360,  -4675.0, 13.0, -405, -87, 0},
		
	},

}
registerScreenPlay("blackSunMHADaily", true)

function blackSunMHADaily:start()
	if (isZoneEnabled("lok")) then
		self:spawnDeathWatch()
	end
end

function blackSunMHADaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function blackSunMHADaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function blackSunMHADaily:spawnDeathWatch()

		local mobileTable = self.kobola
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("lok", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "blackSunMHADaily", "npcKilled", pMobile)
		end
end

function blackSunMHADaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "blackSunMHADaily")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							writeData(groupMember:getObjectID() .. ":dwGhostKilled", readData(groupMember:getObjectID() .. ":dwGhostKilled") + 1)
							
							if(readData(groupMember:getObjectID() .. ":dwGhostKilled") == 10) then
								groupMember:sendSystemMessage("Mission Complete")
								groupMember:setScreenPlayState(4, blackSunMHADaily.questString)
								groupMember:playMusicMessage("sound/ui_button_random.snd")
							end
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "blackSunMHADaily")
					
					if(hasState == true) then 
						writeData(player:getObjectID() .. ":dwGhostKilled", readData(player:getObjectID() .. ":dwGhostKilled") + 1)

						if(readData(player:getObjectID() .. ":dwGhostKilled") == 10) then
							player:sendSystemMessage("Mission Complete")
							player:setScreenPlayState(4, blackSunMHADaily.questString)
							player:playMusicMessage("sound/ui_button_random.snd")
						end
					end
			end
		end)
	return 0
end

function blackSunMHADaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	writeData(player:getObjectID() .. ":dwGhostKilled", 0)
	player:removeScreenPlayState(blackSunMHADaily.states.quest.phasetwo, blackSunMHADaily.questString)
	player:removeScreenPlayState(blackSunMHADaily.states.quest.phaseone, blackSunMHADaily.questString)
	player:removeScreenPlayState(blackSunMHADaily.states.quest.intro, blackSunMHADaily.questString)

end

black_sun_mha_daily_convo_handler = Object:new {
	
 }

function black_sun_mha_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			--local hasDeathWatch = creature:hasSkill("death_watch_novice")
			local hasBlackSun = creature:hasSkill("black_sun_novice")
			local firstQuestStarted = creature:hasScreenPlayState(2, "blackSunMHADaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "blackSunMHADaily")
			local dailyReset = creature:hasScreenPlayState(8, "blackSunMHADaily")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("done_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				elseif (hasBlackSun == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				else
					nextConversationScreen = conversation:getScreen("go_away")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function black_sun_mha_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("lok", "Death Watch Camp", "Kill 10 Death Watch", -4633, -423, 5, true, true, 0)
		player:setScreenPlayState(2, blackSunMHADaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, blackSunMHADaily.questString)

		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 25)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 25)
			end)
		end

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "hardening_agent", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		createEvent(true, 82800000, "blackSunMHADaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(blackSunMHADaily.states.quest.phasethree, blackSunMHADaily.questString)
	end
	


	return conversationScreen
end

