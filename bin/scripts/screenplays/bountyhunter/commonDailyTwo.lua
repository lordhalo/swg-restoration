local ObjectManager = require("managers.object.object_manager")

commonDailyTwo = ScreenPlay:new {
	numberOfActs = 1,
	questString = "commonDailyTwo",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("commonDailyTwo", true)

function commonDailyTwo:start()
	if (isZoneEnabled("naboo")) then
		self:spawnTargets()
	end
end

function commonDailyTwo:getActivePlayerName()
	return self.questdata.activePlayerName
end

function commonDailyTwo:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function commonDailyTwo:spawnTargets()

	local pNPC = spawnMobile("naboo", "common_daily_naboo", math.random(600) + 120, 45.9, 33.0, -120.9,  -86, 1688869)
	self:setMoodString(pNPC, "npc_sitting_chair")
	createObserver(OBJECTDESTRUCTION, "commonDailyTwo", "npcKilled", pNPC)
	local pSlut = spawnMobile("naboo", "entertainer", 0, 45.0, 33.0, -120.9,  -86, 1688869)
	self:setMoodString(pSlut, "themepark_oola")

end

function commonDailyTwo:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "commonDailyTwo")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "commonDailyTwo")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, commonDailyTwo.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "commonDailyTwo")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, commonDailyTwo.questString)
				end
			end
		end)
	return 0
end

function commonDailyTwo:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(commonDailyTwo.states.quest.phasetwo, commonDailyTwo.questString)
	player:removeScreenPlayState(commonDailyTwo.states.quest.phaseone, commonDailyTwo.questString)
	player:removeScreenPlayState(commonDailyTwo.states.quest.intro, commonDailyTwo.questString)

end

common_daily_two_convo_handler = Object:new {
	
 }

function common_daily_two_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "commonDailyTwo")
			local firstQuestComplete = creature:hasScreenPlayState(4, "commonDailyTwo")
			local dailyReset = creature:hasScreenPlayState(8, "commonDailyTwo")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function common_daily_two_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Thief", "", -5541, 4706, 5, true, true, 0)	
		player:setScreenPlayState(2, commonDailyTwo.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, commonDailyTwo.questString)

		if (player:isRebel() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end

		if (player:isImperial() == true) then
			player:awardExperience("force_rank_xp", 75, true)
		end
		createEvent(82800000, "commonDailyTwo", "removeDailyTimer", conversingPlayer, "")

		local amount = 10000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(commonDailyTwo.states.quest.phasethree, commonDailyTwo.questString)
	end
	


	return conversationScreen
end

