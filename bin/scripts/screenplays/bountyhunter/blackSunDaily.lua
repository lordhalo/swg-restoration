local ObjectManager = require("managers.object.object_manager")

blackSunDaily = ScreenPlay:new {
	numberOfActs = 1,
	questString = "blackSunDaily",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},

	mineRats = {
		{"black_sun_mine_rat", 300, -181.0, -60.3, -231.9, 108, 9686061},
		{"black_sun_mine_rat", 300, -172.7, -60.0, -237.9, -16, 9686061},
		{"black_sun_mine_rat", 300, -159.9, -59.9, -243.6, -82, 9686061},
		{"black_sun_mine_rat", 300, -170.9, -60.0, -246.8, -19, 9686061},
		{"black_sun_mine_rat", 300, -183.0, -60.5, -237.7, 50, 9686061},
		{"black_sun_mine_rat", 300, -167.1, -60.5, -256.3, -70, 9686061},
		{"black_sun_mine_rat", 300, -168.5, -59.8, -264.2, -152, 9686061},
		{"black_sun_mine_rat", 300, -177.8, -60.2, -259.9, 19, 9686061},
		{"black_sun_mine_rat", 300, -156.6, -60.2, -312.6, -110, 9686042},
		{"black_sun_mine_rat", 300, -147.3, -60.2, -319.7, -74, 9686042},
		{"black_sun_mine_rat", 300, -129.5, -60.1, -318.3, 19, 9686042},
		{"black_sun_mine_rat", 300, -130.7, -60.0, -339.8, -22, 9686042},
		{"black_sun_mine_rat", 300, -109.3, -59.5, -375.7, 103, 9686042},
		{"black_sun_mine_rat", 300, -6.6, -60.1, -222.5, -35, 9686039},
		{"black_sun_mine_rat", 300, -13.1, -60.0, -216.9, 3, 9686039},
		{"black_sun_mine_rat", 300, -16.9, -60.4, -247.1, 9, 9686039},
		{"black_sun_mine_rat", 300, -8.1, -59.7, -279.9, -33, 9686039},
		{"black_sun_mine_rat", 300, -71.1, -60.1, -315.1, 95, 9686042},
		{"black_sun_mine_rat", 300, -113.1, -59.9, -321.3, -96, 9686042},
		{"black_sun_mine_rat", 300, -97.9, -59.8, -307.0, 29, 9686042},
		{"black_sun_mine_rat", 300, -90.0, -60.5, -287.3, -3, 9686042},
		{"black_sun_mine_rat", 300, -91.7, -60.0, -258.1, -153, 9686042},
		--{"black_sun_mine_rat", 300, -129.2, -60.1, -163.1, 155, 9686045},
		--{"black_sun_mine_rat", 300, -124.3, -60.1, -150.7, 175, 9686045},
		--{"black_sun_mine_rat", 300, -120.4, -60.0, -136.9, 111, 9686045},
		--{"black_sun_mine_rat", 300, -127.8, -60.0, -137.8, 111, 9686045},
		--{"black_sun_mine_rat", 300, -93.7, -59.7, -150.1, -65, 9686046},
		--{"black_sun_mine_rat", 300, -75.9, -60.2, -152, -50, 9686046},
		--{"black_sun_mine_rat", 300, -63.6, -60.1, -148.2, -81, 9686046},
		--{"black_sun_mine_rat", 300, -58.3, -59.9, -136.6, -142, 9686046},
		--{"black_sun_mine_rat", 300, -78.9, -59.9, -129.1, 72, 9686046},
		--{"black_sun_mine_rat", 300, -71.6, -59.5, -124.7, -140, 9686046},
		--{"black_sun_mine_rat", 300, -50.1, -59.5, -123.4, 128, 9686046},
		--{"black_sun_mine_rat", 300, -39.1, -60.1, -136.6, -116, 9686046},
		--{"black_sun_mine_rat", 300, -142.1, -60.1, -164.2, -86, 9686049},
		--{"black_sun_mine_rat", 300, -143.7, -59.9, -175.8, -96, 9686049},
		--{"black_sun_mine_rat", 300, -153, -60.2, -184.8, 131, 9686049},
		--{"black_sun_mine_rat", 300, -142.8, -60.7, -198.2, -89, 9686049},
		--{"black_sun_mine_rat", 300, -152.9, -60.1, -210.9, 24, 9686049},
		--{"black_sun_mine_rat", 300, -171.9, -60.1, -140.7, -49, 9686054},
		--{"black_sun_mine_rat", 300, -179.6, -60.2, -142.6, -3, 9686054},
		--{"black_sun_mine_rat", 300, -177.0, -60.0, -150.3, -14, 9686054},
		--{"black_sun_mine_rat", 300, -202.7, -60.1, -141.4, -149, 9686057},
		--{"black_sun_mine_rat", 300, -212.6, -59.9, -157.8, 22, 9686057},
		--{"black_sun_mine_rat", 300, -210.7, -60.1, -194.4, 164, 9686057}

	},
}
registerScreenPlay("blackSunDaily", true)

function blackSunDaily:start()
	if (isZoneEnabled("endor")) then
		self:spawnMobiles()
		self:spawnRats()
	end
end


function blackSunDaily:spawnMobiles()
	spawnMobile("lok", "death_bunker_crazed_miner_converse", 0, 24.4, -61.4, -302.8,  -22, 9686040)
end


function blackSunDaily:getActivePlayerName()
	return self.questdata.activePlayerName
end

function blackSunDaily:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function blackSunDaily:spawnRats()

		local mobileTable = self.mineRats
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("lok", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], mobileTable[i][7])
			createObserver(OBJECTDESTRUCTION, "blackSunDaily", "npcKilled", pMobile)
		end
end

function blackSunDaily:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "blackSunDaily")

		if(hasState == true) then
			writeData(player:getObjectID() .. ":ratKilled", readData(player:getObjectID() .. ":ratKilled") + 1)
			
			if(readData(player:getObjectID() .. ":ratKilled") == 10) then
				CreatureObject(playerObject):sendSystemMessage("Mission Complete")
				player:setScreenPlayState(4, blackSunDaily.questString)
			end
		end
	return 0
end

function blackSunDaily:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	print("Resetting Daily")
	writeData(player:getObjectID() .. ":ratKilled", 0)
	player:removeScreenPlayState(blackSunDaily.states.quest.phasetwo, blackSunDaily.questString)
	player:removeScreenPlayState(blackSunDaily.states.quest.phaseone, blackSunDaily.questString)
	player:removeScreenPlayState(blackSunDaily.states.quest.intro, blackSunDaily.questString)

end

black_sun_daily_convo_handler = Object:new {
	
 }

function black_sun_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "blackSunDaily")
			local firstQuestComplete = creature:hasScreenPlayState(4, "blackSunDaily")
			local dailyReset = creature:hasScreenPlayState(8, "blackSunDaily")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("quest_turnin")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function black_sun_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		player:setScreenPlayState(2, blackSunDaily.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		createLoot(pInventory, "death_watch_bunker_alum_minerals", 0, true)
		--giveItem(pInventory, "object/tangible/loot/dungeon/black_sun_bunker/mining_drill_reward.iff", -1)
		player:setScreenPlayState(8, blackSunDaily.questString)
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 100)
			end)
		createEvent(true, 86400000, "blackSunDaily", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(blackSunDaily.states.quest.phasethree, blackSunDaily.questString)
	end
	


	return conversationScreen
end

