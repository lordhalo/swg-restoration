local ObjectManager = require("managers.object.object_manager")

igDailyEasy = ScreenPlay:new {
	numberOfActs = 1,
	questString = "igDailyEasy",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("igDailyEasy", true)

function igDailyEasy:start()
	if (isZoneEnabled("naboo")) then
		self:spawnTargets()
	end
end

function igDailyEasy:getActivePlayerName()
	return self.questdata.activePlayerName
end

function igDailyEasy:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function igDailyEasy:spawnTargets()

	local pGungan = spawnMobile("naboo", "ig_gungan_easy", math.random(600) + 120, 4.9, -0.9, -15.9,  79, 94)
	createObserver(OBJECTDESTRUCTION, "igDailyEasy", "npcKilled", pGungan)

end

function igDailyEasy:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "igDailyEasy")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "igDailyEasy")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, igDailyEasy.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "igDailyEasy")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, igDailyEasy.questString)
				end
			end
		end)
	return 0
end

function igDailyEasy:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(igDailyEasy.states.quest.phasetwo, igDailyEasy.questString)
	player:removeScreenPlayState(igDailyEasy.states.quest.phaseone, igDailyEasy.questString)
	player:removeScreenPlayState(igDailyEasy.states.quest.intro, igDailyEasy.questString)

end

ig88_daily_convo_handler = Object:new {
	
 }

function ig88_daily_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "igDailyEasy")
			local firstQuestComplete = creature:hasScreenPlayState(4, "igDailyEasy")
			local dailyReset = creature:hasScreenPlayState(8, "igDailyEasy")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function ig88_daily_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "accept_screen" ) then
		questNPC:doAnimation("nod")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("naboo", "Gungan", "", -5174, 4252, 5, true, true, 0)	
		player:setScreenPlayState(2, igDailyEasy.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, igDailyEasy.questString)
		if (player:isRebel() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("death_watch", 300)
			end)
		end

		if (player:isImperial() == true) then
			ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
			playerObject:increaseFactionStanding("black_sun", 300)
			end)
		end
		local amount = 35000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "igDailyEasy", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(igDailyEasy.states.quest.phasethree, igDailyEasy.questString)
	end
	


	return conversationScreen
end

