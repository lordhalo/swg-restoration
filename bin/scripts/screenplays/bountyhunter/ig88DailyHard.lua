local ObjectManager = require("managers.object.object_manager")

igDailyHard = ScreenPlay:new {
	numberOfActs = 1,
	questString = "igDailyHard",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("igDailyHard", true)

function igDailyHard:start()
	if (isZoneEnabled("dathomir")) then
		self:spawnTargets()
	end
end

function igDailyHard:getActivePlayerName()
	return self.questdata.activePlayerName
end

function igDailyHard:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function igDailyHard:spawnTargets()

	local pNightsister = spawnMobile("dathomir", "ig_nightsister_hard", 600, -78.0, -99.5, -159.8, 1, 4115626)
	createObserver(OBJECTDESTRUCTION, "igDailyHard", "npcKilled", pNightsister)

end

function igDailyHard:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "igDailyHard")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "igDailyHard")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, igDailyHard.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "igDailyHard")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, igDailyHard.questString)
				end
			end
		end)
	return 0
end

function igDailyHard:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(igDailyHard.states.quest.phasetwo, igDailyHard.questString)
	player:removeScreenPlayState(igDailyHard.states.quest.phaseone, igDailyHard.questString)
	player:removeScreenPlayState(igDailyHard.states.quest.intro, igDailyHard.questString)

end

ig88_daily_hard_convo_handler = Object:new {
	
 }

function ig88_daily_hard_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "igDailyHard")
			local firstQuestComplete = creature:hasScreenPlayState(4, "igDailyHard")
			local dailyReset = creature:hasScreenPlayState(8, "igDailyHard")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function ig88_daily_hard_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("dathomir", "Nightsister Stronghold", "", -3946, -49, 5, true, true, 0)	
		player:setScreenPlayState(2, igDailyHard.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, igDailyHard.questString)
		if (player:isRebel() == true) then
			player:awardExperience("force_rank_xp", 750, true)
		end

		if (player:isImperial() == true) then
			player:awardExperience("force_rank_xp", 750, true)
		end
		local amount = 75000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)
		createEvent(82800000, "igDailyHard", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(igDailyHard.states.quest.phasethree, igDailyHard.questString)
	end
	


	return conversationScreen
end

