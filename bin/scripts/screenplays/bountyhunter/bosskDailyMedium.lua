local ObjectManager = require("managers.object.object_manager")

bosskDailyMedium = ScreenPlay:new {
	numberOfActs = 1,
	questString = "bosskDailyMedium",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("bosskDailyMedium", true)

function bosskDailyMedium:start()
	if (isZoneEnabled("talus")) then
		self:spawnTargets()
	end
end

function bosskDailyMedium:getActivePlayerName()
	return self.questdata.activePlayerName
end

function bosskDailyMedium:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function bosskDailyMedium:spawnTargets()

	local pKahmurra = spawnMobile("talus", "bossk_kahmurra_medium", math.random(600) + 120, 0, -13.8, -14.2,  80, 4795365)
	createObserver(OBJECTDESTRUCTION, "bosskDailyMedium", "npcKilled", pKahmurra)

end

function bosskDailyMedium:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "bosskDailyMedium")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "bosskDailyMedium")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, bosskDailyMedium.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "bosskDailyMedium")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, bosskDailyMedium.questString)
				end
			end
		end)
	return 0
end

function bosskDailyMedium:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(bosskDailyMedium.states.quest.phasetwo, bosskDailyMedium.questString)
	player:removeScreenPlayState(bosskDailyMedium.states.quest.phaseone, bosskDailyMedium.questString)
	player:removeScreenPlayState(bosskDailyMedium.states.quest.intro, bosskDailyMedium.questString)

end

bossk_daily_medium_convo_handler = Object:new {
	
 }

function bossk_daily_medium_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "bosskDailyMedium")
			local firstQuestComplete = creature:hasScreenPlayState(4, "bosskDailyMedium")
			local dailyReset = creature:hasScreenPlayState(8, "bosskDailyMedium")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function bossk_daily_medium_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "accept_screen" ) then
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("talus", "Droid", "", -4834, -4751, 5, true, true, 0)	
		player:setScreenPlayState(2, bosskDailyMedium.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		player:setScreenPlayState(8, bosskDailyMedium.questString)

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "bossk_medium_reward", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")
		local amount = 35000
		CreatureObject(conversingPlayer):addCashCredits(amount, true)
		CreatureObject(conversingPlayer):sendSystemMessageWithDI("@theme_park/messages:theme_park_credits_pp", amount)

		createEvent(82800000, "bosskDailyMedium", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(bosskDailyMedium.states.quest.phasethree, bosskDailyMedium.questString)
	end
	


	return conversationScreen
end

