local ObjectManager = require("managers.object.object_manager")

commonDailyThree = ScreenPlay:new {
	numberOfActs = 1,
	questString = "commonDailyThree",
	states = {
		quest = { intro = 2, phaseone = 4, phasetwo = 8, phasethree = 16, phasefour = 32}
	},
	questdata = Object:new {
		
		activePlayerName = "initial",
	},
}
registerScreenPlay("commonDailyThree", true)

function commonDailyThree:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnTargets()
		self:spawnActiveArea()
	end
end

function commonDailyThree:getActivePlayerName()
	return self.questdata.activePlayerName
end

function commonDailyThree:setActivePlayerName(playerName)
	self.questdata.activePlayerName = playerName	
end

function commonDailyThree:spawnActiveArea()
	local pArea = spawnActiveArea("tatooine", "object/active_area.iff", 2200.0, 0.0, -4476.8, 90, 0)
	if pArea ~= nil then
		createObserver(OBJECTINRANGEMOVED, "commonDailyThree", "enterArea", pArea)
	end
end

function commonDailyThree:enterArea(pArea, pPlayer)
	local player = LuaCreatureObject(pPlayer)
	local hasState = player:hasScreenPlayState(2, "commonDailyThree")
	
	if (hasState == true and (readData("commonDailyThree:droidSpawn") == 0)) then
		local pDroid = spawnMobile("tatooine", "common_droid_guard", 0, 2200.0, 0.0, -4476,  178, 0)
		spatialChat(pDroid, "**WARNING** Engaging Enemy Target!")
		CreatureObject(pDroid):engageCombat(pPlayer)
		writeData("commonDailyThree:droidSpawn",1)
		createEvent(300000, "commonDailyThree", "clearDroid", pDroid, "")
	end

	return 0
end

function commonDailyThree:clearDroid(pDroid)

	if pDroid ~=  nil then
		SceneObject(pDroid):destroyObjectFromWorld()
	end
	writeData("commonDailyThree:droidSpawn",0)
end

function commonDailyThree:spawnTargets()

	local pNPC = spawnMobile("tatooine", "common_daily_farmer", math.random(600) + 120, 2200.0, 0.0, -4476.8, 118, 0)
	self:setMoodString(pNPC, "npc_use_terminal_high")
	createObserver(OBJECTDESTRUCTION, "commonDailyThree", "npcKilled", pNPC)

end

function commonDailyThree:npcKilled(pMobile, playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "commonDailyThree")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "commonDailyThree")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 30)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:playMusicMessage("sound/ui_button_random.snd")
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, commonDailyThree.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "commonDailyThree")
					
					if(hasState == true) then 
						player:playMusicMessage("sound/ui_button_random.snd")
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, commonDailyThree.questString)
				end
			end
		end)
	return 0
end

function commonDailyThree:removeDailyTimer(conversingPlayer)
	local player = LuaCreatureObject(conversingPlayer)
	player:removeScreenPlayState(commonDailyThree.states.quest.phasetwo, commonDailyThree.questString)
	player:removeScreenPlayState(commonDailyThree.states.quest.phaseone, commonDailyThree.questString)
	player:removeScreenPlayState(commonDailyThree.states.quest.intro, commonDailyThree.questString)

end

common_daily_three_convo_handler = Object:new {
	
 }

function common_daily_three_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then
			local firstQuestStarted = creature:hasScreenPlayState(2, "commonDailyThree")
			local firstQuestComplete = creature:hasScreenPlayState(4, "commonDailyThree")
			local dailyReset = creature:hasScreenPlayState(8, "commonDailyThree")
				if (dailyReset == true) then
					nextConversationScreen = conversation:getScreen("tomorrow_screen")	
				elseif (firstQuestComplete == true) then
					nextConversationScreen = conversation:getScreen("complete_screen")	
				elseif (firstQuestStarted == true) then
					nextConversationScreen = conversation:getScreen("waiting_screen")
				else
					nextConversationScreen = conversation:getScreen("greet_friend")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function common_daily_three_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pInventory = player:getSlottedObject("inventory")
	local questNPC = LuaCreatureObject(conversingNPC)

	if ( screenID == "second_screen" ) then
		questNPC:doAnimation("apologize")
	end

	if ( screenID == "accept_screen" ) then
		questNPC:doAnimation("snap_finger1")
		player:doAnimation("rub_chin_thoughtful")
		local pGhost = player:getPlayerObject()
		PlayerObject(pGhost):addWaypoint("tatooine", "Farmer", "", 2200, -4476, 5, true, true, 0)
		player:setScreenPlayState(2, commonDailyThree.questString)
	end

	if ( screenID == "complete_screen_final" ) then
		questNPC:doAnimation("bow2")
		player:setScreenPlayState(8, commonDailyThree.questString)

		local pInventory = CreatureObject(conversingPlayer):getSlottedObject("inventory")

		if pInventory == nil then
			return
		end

		createLoot(pInventory, "death_watch_bunker_ingredient_binary", 0, true)
		CreatureObject(conversingPlayer):sendSystemMessage("@theme_park/messages:theme_park_reward")

		createEvent(82800000, "commonDailyThree", "removeDailyTimer", conversingPlayer, "")
	end

	if ( screenID == "restart_quest" ) then
		player:setScreenPlayState(commonDailyThree.states.quest.phasethree, commonDailyThree.questString)
	end
	


	return conversationScreen
end

