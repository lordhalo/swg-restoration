WebweaverCaveScreenPlay = ScreenPlay:new {
    numberOfActs = 1,

    screenplayName = "WebweaverCaveScreenPlay",

    lootContainers = {
	9815402,
        9815403,
        9815404,
        9815405,
        9815406,
        9815412
	},

	lootLevel = 200,

	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 2500000},
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 2500000}
			},
			lootChance = 8000000
		}
	},

	lootContainerRespawn = 1800
}

registerScreenPlay("WebweaverCaveScreenPlay", true)

function WebweaverCaveScreenPlay:start()
	if (isZoneEnabled("kashyyyk_dead_forest")) then
       		self:spawnMobiles()
		--self:initializeLootContainers()
	end
end

function WebweaverCaveScreenPlay:spawnMobiles()

        --r1 4

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, -1.98358, -11.8603, -7.07566, 86.8473, 16779051)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 23.1662, -28.1276, -10.1829, -174.795, 16779051)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 26.4671, -26.4333, -6.25681, -93.7979, 16779051)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, -15.5229, -4.56978, -8.02114, -36.8618, 16779051)

        --r2 5

        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 23.0, -37.2, -29.9, -179.3, 16779052)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 24.0, -40.9, -50.5, 175.7, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 28.0063, -43.1017, -65.82, 99.8679, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 50.8272, -48.0031, -68.3053, -4.12044, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300,45.1771, -47.6516, -39.2092, -1.30541, 16779052)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 55.7022, -49.6387, -15.5055, 81.3925, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 57.3017, -68.3859, -36.9043, -80.3083, 16779052)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 50.8239, -67.4948, -47.3695, 50.073, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 56.7341, -68.4113, -43.4215, -126.787, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 52.1823, -67.9702, -36.79, -20.2683, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 44.3398, -47.1838, -10.0972, -16.9468, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 41.2219, -46.3072, -28.5425, -145.138, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 52.3357, -48.8913, -61.4344, -157.731, 16779052)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 20.8388, -41.7931, -65.7011, -2.10513, 16779052)

        --r3 6

        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 84.8727, -62.9168, -18.5431, 165.322, 16777222)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 83.1835, -68.7035, -41.0718, -174.619, 16777222)

        --r4 7

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 67.6305, -75.4073, -59.7072, -133.446, 16777223)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 63.7554, -76.6355, -82.2412, 123.446, 16777223)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 84.0108, -76.6423, -89.1168, 93.0057, 16777223)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 90.5749, -76.2048, -66.7016, -28.7539, 16777223)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_mother_vesad", 420,	 79.9971, -76.1955, -65.019, 4.67729, 16777223)

        --r5 8

        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 71.9626, -46.4406, -107.322, 94.4131, 16777224)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 93.4225, -45.8405, -100.164, -169.868, 16777224)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 94.236, -66.1407, -115.167, 24.384, 16777224)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 94.5955, -66.5879, -105.821, -95.5706, 16777224)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 94.7456, -46.8375, -113.249, 62.967, 16777224)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 84.6227, -46.6022, -116.124, 6.72038, 16777224)

        --r6 9

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 53.0008, -75.6514, -98.3109, 170.953, 16777225)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 55.4087, -71.3504, -115.079, 169.897, 16777225)

        --r7 10

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 69.7797, -66.1897, -139.039, 105.322, 16777226)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 89.2114, -67.0698, -133.896, 12.9469, 16777226)

        --r8 11

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 113.629, -66.4289, -118.855, 166.906, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 130.18, -66.636, -104.879, 60.4542, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 151.899, -66.6226, -123.969, 9.95569, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 155.316, -66.3611, -96.1209, -132.39, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_rhiek", 300, 146.524, -67.2326, -123.671, 26.3458, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_aveso", 300, 140.026, -67.1334, -91.2939, 160.712, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_cryl", 300, 135.742, -66.2344, -109.189, -82.6612, 16777227)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_dealer", 300, 131.937, -66.9065, -86.9163, -142.323, 16777227)

        --r9 12

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_dahlia", 300, 180.136, -66.1165, -96.7275, -90.5224, 16777228)

        --r10 13

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_bloodseeker", 300, 49.6811, -51.6588, -90.8699, -178.138, 16777229)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 62.97, -47.2055, -106.43, 98.4601, 16777229)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 48.8202, -48.9123, -101.045, 166.339, 16777229)

        --r11 14

        spawnMobile("kashyyyk_dead_forest", "ep3_forest_webweaver_gravespinner", 300, 92.2971, -46.9176, -137.273, -126.232, 16777230)
        spawnMobile("kashyyyk_dead_forest", "webweaver_tombsinger", 300, 79, -46.5529, -141.928, 63.2695,  16777230)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 90.0074, -46.3734, -147.788, -89.6229, 16777230)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 82.3697, -46.2066, -138.215, -39.877, 16777230)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_outcast_assassin", 300, 73.6104, -46.0966, -141.437, -98.0497, 16777230)
        spawnMobile("kashyyyk_dead_forest", "ep3_forest_risyl", 300, 82.9025, -46.4097, -143.164, -33.5273, 16777230)




end
