KachirhoScreenPlay = ScreenPlay:new {
    numberOfActs = 1,

    screenplayName = "KachirhoScreenPlay",

}

registerScreenPlay("KachirhoScreenPlay", true)

function KachirhoScreenPlay:start()
	if (isZoneEnabled("kashyyyk")) then
       		self:spawnMobiles()
	end
end

function KachirhoScreenPlay:spawnMobiles()

        spawnMobile("kashyyyk", "dr_farnsworth", 300, -504, 18, -97, 0, 0)
	spawnMobile("kashyyyk", "junk_dealer", 300, -548, 19.3, -108, 0, 0)
	spawnMobile("kashyyyk", "chewbacca_kashyyyk", 300, -587, 18, -103, 0, 0)
	spawnMobile("kashyyyk", "vryyyr", 300, -589, 18, -103, 0, 0)
	spawnMobile("kashyyyk", "ikvizi", 300, -513, 19, -109, 0, 0)
	spawnMobile("kashyyyk", "ovarra", 300, -425, 179.5, -84, 0, 0)


end
