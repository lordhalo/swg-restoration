MyyydrilCavernSpawns = ScreenPlay:new {
    numberOfActs = 1,

    screenplayName = "MyyydrilCavernSpawns",

	warlCavePlants = {					-- X Y Z CELL DIR(CONFUSING IN CORE3, LEAVE BLANK FOR NOW?)
		{ "object/tangible/quest/warl_cave_plant.iff", 147.374, -41.3962, -11.4858, 6296366, 0, 0, 0, 0 },
		{ "object/tangible/quest/warl_cave_plant.iff", 146.5, -41.7284, -9.18872, 6296366, 0, 0, 0, 0 },
	},

	naktraCrystals = {
		{ "object/tangible/quest/naktra_crystals.iff", 147.374, -41.3962, -11.4858, 6296366, 0, 0, 0, 0 },
		{ "object/tangible/quest/naktra_crystals.iff", 146.5, -41.7284, -9.18872, 6296366, 0, 0, 0, 0 },
	},

	bars = {
		{ "object/building/kashyyyk/cave_myyydril_bars.iff", -356.451, -145.339, 172.206, 6296403, 0 },
		{ "object/building/kashyyyk/cave_myyydril_bars.iff", -180.771, -92.7021, 128.166, 6296405, -90 },
		--{ "object/building/kashyyyk/cave_myyydril_bars.iff", -163.937, -66.0238, 61.1523, 6296406, -80.1 }
	},
}

registerScreenPlay("MyyydrilCavernSpawns", true)

function MyyydrilCavernSpawns:start()
	if (isZoneEnabled("kashyyyk")) then
       		self:spawnMobiles()
		self:spawnWarlCavePlants()
		self:spawnBars()
		--self:initializeLootContainers()
	end
end


function MyyydrilCavernSpawns:spawnWarlCavePlants()

		local sceneObject = self.warlCavePlants
		for i = 1, #sceneObject, 1 do
			local pBolotaur = spawnSceneObject("kashyyyk", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "collectingSurveyData", "bolotaurActivated", pBolotaur)
		end

end

function MyyydrilCavernSpawns:spawnBars()

		local sceneObject = self.bars
		for i = 1, #sceneObject, 1 do
			local pBar = spawnSceneObject("kashyyyk", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6])
		end

end

function MyyydrilCavernSpawns:spawnMobiles()

        spawnMobile("kashyyyk", "myyydril_guard", 300, 133.38, -44.8591, -100.662, -7.23094, 6296367)-- Hall 19
	spawnMobile("kashyyyk", "myyydril_guard", 300, 122.598, -45.0342, -101.058, 9.6458, 6296367)-- Hall 19

	spawnMobile("kashyyyk", "myyydril_farmer", 300, 130.511, -46.6596, -146.823, -164.533, 6296370)-- Dungeon22
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 108.135, -46.4668, -151.602, 175.584, 6296370)-- Dungeon22
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 115.75, -46.4627, -123.021, -77.0845, 6296370)-- Dungeon22
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 156.743, -46.8204, -116.01, 80.7454, 6296370)-- Dungeon22
	
	spawnMobile("kashyyyk", "myyydril_guard", 300, 153.435, -55.3663, -77.2239, 156.933, 6296371)-- Hall 23

	spawnMobile("kashyyyk", "myyydril_farmer", 300, 186.976, -63.8147, -84.7525, 166.082, 6296372)-- Dungeon24
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 201.045, -63.6227, -75.103, 158.165, 6296372)-- Dungeon24
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 210.649, -63.5615, -92.8005, -162.07, 6296372)-- Dungeon24
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 209.059, -63.6617, -95.7795, 32.5343, 6296372)-- Dungeon24
	spawnMobile("kashyyyk", "myyydril_farmer", 300, 228.335, -63.3402, -91.9031, 171.537, 6296372)-- Dungeon24

	spawnMobile("kashyyyk", "myyydril_guard", 300, 258.457, -69.0329, -93.5637, -177.026, 6296373)-- Hall 25
	spawnMobile("kashyyyk", "myyydril_guard", 300, 255.935, -80.3757, -132.061, 9.66016, 6296373)-- Hall 25

	spawnMobile("kashyyyk", "myyydril_guard", 300, 229.922, -91.107, -132.751, 5.43727, 6296377)-- OilPits
	spawnMobile("kashyyyk", "myyydril_guard", 300, 229.494, -91.1845, -122.488, 168.798, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 211.894, -97.1481, -148.783, -169.108, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 209.908, -97.0143, -152.412, -154.263, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 208.037, -97.0118, -148.132, -45.941, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 206.314, -96.8221, -152.979, -149.577, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 207.679, -96.4376, -157.211, 170.305, 6296377)-- OilPits
	spawnMobile("kashyyyk", "myyydril_herder", 300, 200.737, -95.002, -156.945, 65.7892, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 178.133, -91.7585, -163.64, -108.756, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 176.172, -91.4556, -160.618, -32.9205, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 179.213, -91.8415, -157.137, 43.0913, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 183.002, -92.4174, -161.502, 129.484, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 187.948, -92.9268, -161.238, 86.3757, 6296377)-- OilPits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 171.97, -91.666, -156.314, 124.382, 6296377)-- OilPits
	
	--added by binks below. check for clumsiness
	spawnMobile("kashyyyk", "myyydril_guard", 300, 136.339, -90.405, -212.999, 0.686426, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_guard", 300, 120.851, -90.4859, -212.548, 0.510454, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_guard", 300, 160.607, -90.4312, -49.3582, -126.745, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_herder", 300, 125.801, -90.6663, -145.778, -42.2462, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_herder", 300, 132.315, -94.141, -189.193, 21.9768, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_herder", 300, 146.319, -93.9385, -78.3572, -113.331, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "myyydril_herder", 300, 158.212, -91.2208, -88.1021, -110.34, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 118.13, -91.1044, -136.731, -80.4279, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 117.342, -91.2804, -143.355, 50.2369, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 125.301, -90.7835, -138.652, -130.817, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 123.697, -90.9375, -151.423, -65.6022, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 130.843, -90.7821, -145.593, 59.4547, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 138.271, -93.9722, -181.884, 42.5633, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 139.739, -93.5805, -171.531, 21.6248, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 132.133, -94.0566, -178.126, -128.991, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 145.529, -94.2098, -98.1417, -145.179, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 137.268, -96.1155, -98.2057, -103.126, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 122.367, -94.9597, -104.549, -29.482, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 113.671, -95.2492, -97.1501, -47.1727, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 119.869, -96.9768, -85.7943, 27.4314, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 124.154, -96.6454, -91.5439, 142.505, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 137.196, -96.5338, -87.5184, 71.5956, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 129.077, -96.7714, -96.9842, -125.296, 6296377)  --oil_pits
	spawnMobile("kashyyyk", "uwari_domestic", 300, 109.836, -95.2659, -106.584, -1.95276, 6296377)  --oil_pits

	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -220.282, -252.899, -100.07, 14.7642, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -184.424, -254.152, -64.4766, 71.421, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -133.319, -261.871, -73.2864, 137.051, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -227.026, -269.325, -228.67, -72.6843, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -224.363, -268.731, -218.952, 40.2776, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -243.565, -223.005, -275.678, 115.078, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -203.464, -263.327, -229.324, -78.7059, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -204.053, -264.465, -219.638, -94.9377, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -228.032, -222.821, -273.303, -98.9023, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -125.534, -173.371, -86.994, 32.0078, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -123.959, -196.273, -38.1244, -113.154, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -142.349, -196.288, -37.5862, -138.734, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -142.115, -198.717, -63.4563, -160.661, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -152.459, -204.76, -92.6718, -178.256, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -232.158, -251.901, -181.587, 13.1809, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -234.745, -253.108, -153.528, 24.4417, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -215.911, -252.974, -119.88, 59.8083, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -197.541, -252.807, -95.7474, -162.95, 6296388)  --bigroom40
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -379.283, -255.592, -122.147, 42.0375, 6296397)  --borglestatue49
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -365.933, -255.644, -107.159, 53.8264, 6296397)  --borglestatue49
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -384.104, -254.041, -98.9196, -46.8188, 6296397)  --borglestatue49
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -380.54, -247.825, -84.2129, 34.8234, 6296397)  --borglestatue49
	spawnMobile("kashyyyk", "clone_droid", 300, 0.3, -1.9, -7.5, -90, 6296349)  --hall1
	spawnMobile("kashyyyk", "uwari_aggro", 360, 49.2284, -26.8235, -18.6088, 107.469, 6296358)  --hall10
	spawnMobile("kashyyyk", "uwari_aggro", 360, 53.2921, -26.9289, -3.76058, 13.702, 6296358)  --hall10
	spawnMobile("kashyyyk", "uwari_aggro", 360, 62.2331, -26.6181, -15.0585, -71.679, 6296358)  --hall10
	spawnMobile("kashyyyk", "uwari_aggro", 360, 71.3232, -29.0831, 5.46864, 112.448, 6296358)  --hall10
	spawnMobile("kashyyyk", "uwari_aggro", 360, 83.8386, -34.332, -1.06825, 121.887, 6296358)  --hall10
	spawnMobile("kashyyyk", "uwari_aggro", 360, 78.8226, -29.2769, -35.7567, -100.895, 6296359)  --hall11
	spawnMobile("kashyyyk", "uwari_aggro", 360, 61.12, -14.6268, -80.9186, 79.7441, 6296360)  --hall12
	spawnMobile("kashyyyk", "uwari_aggro", 360, 72.1958, -16.3647, -72.5835, -118.146, 6296360)  --hall12
	spawnMobile("kashyyyk", "uwari_aggro", 360, 69.2942, -14.6515, -83.6647, -47.6996, 6296360)  --hall12
	spawnMobile("kashyyyk", "uwari_aggro", 360, 76.7341, -29.6746, -108.228, -45.203, 6296361)  --hall13
	spawnMobile("kashyyyk", "uwari_aggro", 360, 65.7218, -29.1426, -101.079, -74.3695, 6296361)  --hall13
	spawnMobile("kashyyyk", "uwari_aggro", 360, 109.229, -36.9041, -95.0925, -33.7448, 6296362)  --hall14
	spawnMobile("kashyyyk", "uwari_aggro", 360, 96.9874, -32.7555, -83.196, 140.364, 6296362)  --hall14
	spawnMobile("kashyyyk", "uwari_aggro", 360, 92.3512, -32.2533, -93.9745, -107.46, 6296362)  --hall14
	spawnMobile("kashyyyk", "uwari_aggro", 360, 82.9956, -30.3763, -95.5, 159.225, 6296362)  --hall14
	spawnMobile("kashyyyk", "uwari_aggro", 360, 80.9088, -21.5786, -59.8052, 39.5459, 6296363)  --hall15
	spawnMobile("kashyyyk", "uwari_aggro", 360, 85.7393, -22.0325, -52.1538, -148.237, 6296363)  --hall15
	spawnMobile("kashyyyk", "uwari_aggro", 360, 99.1537, -27.4516, -58.9261, 63.013, 6296364)  --hall16
	spawnMobile("kashyyyk", "uwari_aggro", 360, 113.472, -33.5387, -53.1615, -115.981, 6296364)  --hall16
	spawnMobile("kashyyyk", "uwari_aggro", 360, 104.069, -41.7739, 1.90033, 169.889, 6296365)  --hall17
	spawnMobile("kashyyyk", "uwari_aggro", 360, 96.4989, -39.7595, -13.1013, -135.452, 6296365)  --hall17
	spawnMobile("kashyyyk", "uwari_aggro", 360, 91.5685, -37.1475, -27.276, -115.579, 6296365)  --hall17
	spawnMobile("kashyyyk", "myyydril_guard", 300, 130.408, -44.3392, -11.4392, 110, 6296366)  --hall18
	spawnMobile("kashyyyk", "uwari_aggro", 360, 115.308, -44.313, -33.4093, 35.5923, 6296366)  --hall18
	spawnMobile("kashyyyk", "uwari_aggro", 360, 129.205, -44.4045, -21.4959, 81.202, 6296366)  --hall18
	spawnMobile("kashyyyk", "uwari_aggro", 360, 146.792, -41.6294, -15.6817, 77.6646, 6296366)  --hall18
	spawnMobile("kashyyyk", "uwari_aggro", 360, 124.172, -36.3608, -43.1886, 108.523, 6296367)  --hall19
	spawnMobile("kashyyyk", "uwari_aggro", 360, 131.384, -36.6657, -54.4793, -15.8947, 6296367)  --hall19
	spawnMobile("kashyyyk", "uwari_aggro", 360, 133.044, -41.5636, -70.2756, -130.673, 6296367)  --hall19
	spawnMobile("kashyyyk", "uwari_aggro", 360, 122.4, -40.1499, -82.6049, -176.253, 6296367)  --hall19
	spawnMobile("kashyyyk", "myyydril_guard", 300, 153.711, -54.9012, 2.71724, 3.81246, 6296369)  --hall21
	spawnMobile("kashyyyk", "myyydril_guard", 300, 160.065, -54.8491, 3.07508, 3.46052, 6296369)  --hall21
	spawnMobile("kashyyyk", "uwari_aggro", 360, 10.282, -17.2765, -41.346, -2.19559, 6296351)  --hall3
	spawnMobile("kashyyyk", "uwari_aggro", 360, 1.09388, -16.4583, -38.4621, 81.1276, 6296351)  --hall3
	spawnMobile("kashyyyk", "uwari_aggro", 360, 34.8386, -21.2498, -46.2606, -36.2087, 6296351)  --hall3
	spawnMobile("kashyyyk", "uwari_aggro", 360, 33.7738, -21.1431, -34.7444, -5.14588, 6296351)  --hall3
	spawnMobile("kashyyyk", "myyydril_herder", 300, 142.368, -90.7822, -22.0194, -163.654, 6296378)  --hall30
	spawnMobile("kashyyyk", "myyydril_herder", 300, 131.662, -90.568, -30.3875, 46.7862, 6296378)  --hall30
	spawnMobile("kashyyyk", "myyydril_herder", 300, 146.032, -90.9052, -33.8932, -27.6245, 6296378)  --hall30
	spawnMobile("kashyyyk", "myyydril_guard", 300, 128.859, -90.7804, -233.048, 93.3335, 6296379)  --hall31
	spawnMobile("kashyyyk", "myyydril_guard", 300, 226.647, -123.409, -213.627, 178.399, 6296381)  --hall33
	spawnMobile("kashyyyk", "myyydril_guard", 300, 229.949, -123.221, -253.165, -89.5774, 6296381)  --hall33
	spawnMobile("kashyyyk", "myyydril_guard", 300, 218.915, -123.177, -254.112, 90.2467, 6296381)  --hall33
	spawnMobile("kashyyyk", "ep3_myyydril_kiaanta", 300, 231.423, -123.363, -233.263, -41, 6296381)  --hall33
	spawnMobile("kashyyyk", "ep3_myyydril_kivvaaa", 300, 230.436, -123.529, -235.86, -41, 6296381)  --hall33
	spawnMobile("kashyyyk", "ep3_myyydril_talaoree", 300, 230.962, -123.39, -239.278, -41, 6296381)  --hall33
	spawnMobile("kashyyyk", "myyydril_guard", 300, 90.7353, -153.011, -246.253, 55.4087, 6296383)  --hall35
	spawnMobile("kashyyyk", "myyydril_guard", 300, 115.836, -152.993, -246.965, -8.28622, 6296383)  --hall35
	spawnMobile("kashyyyk", "ep3_myyydril_miner", 300, 129.176, -159.715, -210.516, -53.8581, 6296384)  --hall36
	spawnMobile("kashyyyk", "ep3_myyydril_miner", 300, 137.584, -160.284, -183.772, -98.5501, 6296384)  --hall36
	spawnMobile("kashyyyk", "ep3_myyydril_miner", 300, 108.445, -160.217, -196.787, 141.486, 6296384)  --hall36
	spawnMobile("kashyyyk", "ep3_myyydril_miner", 300, 77.8634, -188.084, -135.119, -0.192075, 6296386)  --hall38
	spawnMobile("kashyyyk", "ep3_myyydril_miner", 300, 73.9995, -196.095, -166.608, -112.978, 6296386)  --hall38
	spawnMobile("kashyyyk", "myyydril_guard", 300, 42.3038, -198.182, -195.802, -8.81384, 6296387)  --hall39
	spawnMobile("kashyyyk", "myyydril_guard", 300, 40.9426, -197.826, -171.114, 174.353, 6296387)  --hall39
	spawnMobile("kashyyyk", "uwari_aggro", 360, 13.2659, -21.1207, -57.8446, -117.862, 6296352)  --hall4
	spawnMobile("kashyyyk", "uwari_aggro", 360, 4.2932, -21.9665, -60.0962, 78.9006, 6296352)  --hall4
	spawnMobile("kashyyyk", "urnsoris_royal_guards", 800, -207.426, -187.975, -356.064, -98.5502, 6296389)  --hall41
	spawnMobile("kashyyyk", "urnsoris_royal_guards", 800, -225.826, -188.65, -355.831, 118.576, 6296389)  --hall41
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -210.342, -177.409, -382.155, -144.298, 6296390)  --hall42
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -179.621, -169.212, -403.305, -27.8171, 6296390)  --hall42
	spawnMobile("kashyyyk", "urnsoris_royal_guards", 800, -213.468, -183.031, -371.014, -94.8552, 6296390)  --hall42
	spawnMobile("kashyyyk", "urnsoris_royal_guards", 800, -204.869, -174.955, -391.839, -64.2391, 6296390)  --hall42
	spawnMobile("kashyyyk", "urnsoris_handmaiden", 800, -160.104, -169.084, -413.562, 123.679, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_handmaiden", 800, -166.636, -168.889, -429.96, 13.004, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_handmaiden", 800, -162.182, -169.373, -425.266, -60.4983, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_queen", 1800, -149.489, -172.906, -421.358, -81.6587, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_royal_guards", 800, -183.866, -168.613, -426.173, 75.9953, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_young", 600, -181.83, -169.08, -424.707, -123.764, 6296391)  --hall43
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -221.596, -221.117, 18.1722, 84.0884, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -210.891, -221.049, 18.5336, -90.2811, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -178.716, -221.6, 66.9749, -121.776, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -194.707, -221.788, 44.6385, -129.166, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -213.214, -221.185, 43.3101, -179.313, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -194.231, -221.009, 60.5784, 104.323, 6296392)  --hall44
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -146.482, -220.098, 83.2689, -36.4392, 6296393)  --hall45
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -166.189, -221.558, 82.3109, -164.357, 6296393)  --hall45
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -63.545, -222.07, 96.3889, -7.17393, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -63.6293, -221.649, 116.499, -69.4608, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -85.3694, -221.658, 121.309, -11.3567, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -97.8823, -221.644, 114.605, 108.579, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -102.747, -221.722, 123.599, -22.3489, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -119.649, -197.633, 81.9757, 129.497, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -109.946, -197.818, 86.6549, 95.0595, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myydril_refugee", 300, -93.3029, -221.826, 85.1953, 1.89417, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -109.723, -222.243, 127.402, 116.64, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -116.599, -222.665, 132.686, 161.338, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -108.923, -197.932, 92.7883, -15.8527, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -123.679, -197.04, 81.1694, 49.25, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -108.148, -197.379, 73.6902, 48.24, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -93.3514, -196.78, 75.2431, 89.3673, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -82.4134, -222.145, 116.72, 110.13, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_refugee", 300, -74.6354, -221.927, 132.074, 123.502, 6296394)  --hall46
	spawnMobile("kashyyyk", "ep3_myyydril_rensalla", 300, -93.834, -196.729, 75.8661, -89.5601, 6296394)  --hall46
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -127.171, -221.342, 67.367, -64.0638, 6296394)  --hall46
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -326.153, -273.106, -196.546, 173.298, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -325.43, -273.247, -208.26, -97.1415, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -336.93, -272.54, -211.368, -147.816, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -340.282, -271.306, -186.639, -10.3967, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -350.215, -272.294, -200.579, -164.883, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -348.644, -267.069, -164.439, 13.3571, 6296395)  --hall47
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -363.35, -263.985, -161.197, -139.546, 6296396)  --hall48
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -372.264, -259.451, -148.895, -35.7338, 6296396)  --hall48
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -364.401, -254.24, -133.716, 40.4538, 6296396)  --hall48
	spawnMobile("kashyyyk", "uwari_aggro", 360, -5.45612, -23.3196, -69.7975, -171.924, 6296353)  --hall5
	spawnMobile("kashyyyk", "uwari_aggro", 360, -6.20541, -23.4719, -85.2956, 92.4764, 6296353)  --hall5
	spawnMobile("kashyyyk", "uwari_aggro", 360, 2.77548, -21.8867, -85.0015, -77.7648, 6296353)  --hall5
	spawnMobile("kashyyyk", "uwari_aggro", 360, 24.4632, -15.6197, -80.0502, 121.461, 6296353)  --hall5
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -348.102, -228.234, -68.1254, -165.763, 6296398)  --hall50
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -351.118, -219.052, -88.3319, -166.115, 6296398)  --hall50
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -349.161, -217.333, -98.6776, -93.7983, 6296398)  --hall50
	spawnMobile("kashyyyk", "urnsoris_worker", 600, -380.546, -241.697, -66.2874, -27.4639, 6296398)  --hall50
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -378.803, -206.584, -99.3908, -43.6519, 6296399)  --hall51
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -381.949, -202.735, -84.4611, -19.7223, 6296399)  --hall51
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -369.066, -197.726, -63.8687, -155.03, 6296400)  --hall52
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -352.762, -188.962, -68.5721, 82.6145, 6296400)  --hall52
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -339.183, -187.025, -55.2882, -149.882, 6296400)  --hall52
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -333.341, -187.504, -55.7555, 25.6833, 6296400)  --hall52
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -267.033, -184.564, 167.379, 57.7063, 6296402)  --hall54
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -265.113, -178.616, 187.724, 150.258, 6296402)  --hall54
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -274.199, -177.763, 206.615, -131.091, 6296402)  --hall54
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -295.924, -177.963, 210.362, -96.9565, 6296402)  --hall54
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -324.807, -177.663, 207.5, -66.8685, 6296402)  --hall54
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -346.48, -163.812, 207.777, -94.3172, 6296402)  --hall54
	spawnMobile("kashyyyk", "myyydril_lorn_servant", 300, -361, -145, 169, 12, 6296403)  --hall55
	spawnMobile("kashyyyk", "myyydril_lorn", 300, -217.655, -92.8194, 149.159, -86.8496, 6296405)  --hall57
	spawnMobile("kashyyyk", "uwari_aggro", 360, 33.6506, -22.8741, -63.9208, 43.0353, 6296354)  --hall6
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -163.793, -66.0753, 56.3436, 138.299, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -143.439, -68.0613, 49.7888, 111.906, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -133.926, -66.2461, 56.2187, 27.2728, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -131.021, -66.5682, 36.5596, 159.765, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -119.907, -65.3093, 41.4883, 64.2229, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -112.023, -61.5444, 28.0883, 157.126, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -100.821, -57.9983, 26.6354, 101.349, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -94.8129, -57.339, 17.3569, 130.733, 6296409)  --hall61
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -64.7754, -42.9853, 23.7196, 152.903, 6296410)  --hall62
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -64.3535, -41.1795, 10.4504, 159.765, 6296410)  --hall62
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -59.5068, -34.975, -4.11938, 117.889, 6296410)  --hall62
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -57.5454, -42.959, 18.6899, 40.2933, 6296410)  --hall62
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -43.3808, -34.5157, -12.2606, -169.104, 6296411)  --hall63
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -51.9838, -25.1847, -41.5529, -164.311, 6296411)  --hall63
	spawnMobile("kashyyyk", "ep3_forgotten_creation", 480, -22.8156, -19.2073, -40.2493, 93.611, 6296411)  --hall63
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -156.794, -168.21, -157.36, -69.1657, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -151.103, -168.211, -130.718, -11.8051, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -140.598, -168.187, -157.224, -64.6406, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -130.29, -167.374, -124.412, 44.5565, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -112.523, -167.361, -140.462, 136.699, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -122.058, -167.827, -122.194, 10.1803, 6296412)  --hall64
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -96.1435, -167.281, -162.019, 160.101, 6296413)  --hall65
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -98.063, -167.756, -183.395, -96.9666, 6296413)  --hall65
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -114.041, -167.493, -181.021, -76.0282, 6296413)  --hall65
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -95.4395, -168.357, -148.951, -154.866, 6296413)  --hall65
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -99.4722, -168.254, -137.614, 164.081, 6296413)  --hall65
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -111.494, -167.411, -225.508, -117.025, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -122.596, -168.476, -241.871, 134.764, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -138.301, -168.278, -250.928, -1.60013, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -125.074, -169.256, -231.314, 95.8778, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -96.7567, -168.634, -221.173, 125.999, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -135.71, -167.462, -212.942, 174.219, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -121.516, -167.229, -219.276, 117.997, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_young", 600, -116.178, -167.062, -254.353, -48.4987, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_young", 600, -132.778, -169.211, -229.632, -12.2667, 6296414)  --hall66
	spawnMobile("kashyyyk", "urnsoris_eviscerater", 1800, -177.962, -167.785, -191.879, 83.2814, 6296415)  --hall67
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -161.711, -166.06, -185.628, -123.36, 6296415)  --hall67
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -127.161, -167.486, -191.225, -119.131, 6296415)  --hall67
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -143.379, -168.024, -193.258, -49.3304, 6296415)  --hall67
	spawnMobile("kashyyyk", "urnsoris_young", 600, -128.992, -167.097, -200.207, 99.8573, 6296415)  --hall67
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -115.841, -199.579, -129.859, -63.0075, 6296417)  --hall69
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -137.862, -200.275, -121.499, -46.8199, 6296417)  --hall69
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -96.1835, -200.326, -130.106, -79.0667, 6296417)  --hall69
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -121.792, -199.511, -124.364, 24.8569, 6296417)  --hall69
	spawnMobile("kashyyyk", "uwari_aggro", 360, 27.7918, -21.096, -16.6394, 51.6105, 6296355)  --hall7
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -140.595, -199.865, -175.559, -51.3945, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -105.728, -200.065, -164.843, -140.475, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -130.776, -199.984, -150.521, -108.723, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -122.387, -199.874, -186.053, 16.2026, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -141.232, -200.13, -192.321, -19.728, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -123.892, -199.937, -167.536, 64.3826, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -97.7126, -199.993, -144.229, 0.511662, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -95.8281, -200.01, -162.515, -175.993, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -107.449, -200.197, -152.32, -112.303, 6296418)  --hall70
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -90.5615, -200.052, -177.59, 166.084, 6296419)  --hall71
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -90.766, -200.045, -224.311, -15.8523, 6296419)  --hall71
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -101.152, -199.833, -181.722, 94.9981, 6296419)  --hall71
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -97.6878, -200.184, -189.65, 33.6849, 6296419)  --hall71
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -95.9804, -199.778, -214.871, 56.6784, 6296419)  --hall71
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -71.658, -200.267, -212.096, -31.8641, 6296420)  --hall72
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -62.0804, -199.834, -210.382, -174.738, 6296420)  --hall72
	spawnMobile("kashyyyk", "urnsoris_nurse", 600, -74.2529, -200.016, -225.482, -99.606, 6296420)  --hall72
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -80.5427, -200.077, -184.47, 31.3041, 6296421)  --hall73
	spawnMobile("kashyyyk", "urnsoris_soldier", 600, -72.7851, -200.641, -172.819, 39.9843, 6296421)  --hall73
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -134.645, -263.905, -120.932, -72.1567, 6296423)  --hall75
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -158.261, -263.287, -127.462, 29.7816, 6296424)  --hall76
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -162.665, -263.886, -142.235, 51.6794, 6296424)  --hall76
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -147.039, -263.454, -150.734, 97.0733, 6296424)  --hall76
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -114.844, -263.566, -152.889, -155.858, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -86.3763, -260.827, -176.253, -62.8441, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -82.4326, -263.14, -148.514, -1.79468, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -109.452, -265.576, -190.976, -153.591, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -97.0835, -262.577, -203.522, 151.153, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -109.16, -265.908, -232.217, -113.419, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -85.4961, -263.052, -137.062, -39.179, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -93.4207, -263.578, -139.965, -103.621, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -91.4506, -263.467, -147.723, 169.984, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -89.4495, -262.024, -162.771, -141.533, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -99.4975, -263.064, -170.897, -129.193, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -119.005, -263.922, -224.187, -45.9008, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -128.417, -263.751, -214.226, 26.7147, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -117.947, -262.411, -213.213, 95.5103, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -130.536, -263.775, -178.448, 20.8024, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -121.887, -262.913, -179.783, 102.385, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -123.149, -263.828, -158.205, 11.9089, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -107.621, -263.447, -145.902, 76.0419, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -77.2086, -262.479, -192.218, -123.735, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -79.6007, -263.027, -228.093, -26.9605, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -94.413, -261.799, -240.832, -68.151, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_young", 600, -122.708, -263.344, -243.734, -137.07, 6296425)  --hall77
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -143.513, -263.931, -258.537, -27.2736, 6296426)  --hall78
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -152.582, -264.206, -241.53, -65.9981, 6296426)  --hall78
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -178.121, -263.586, -221.024, -104.708, 6296427)  --hall79
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -168.819, -262.934, -239.951, -14.0703, 6296427)  --hall79
	spawnMobile("kashyyyk", "uwari_aggro", 360, 36.1459, -13.1518, -88.5054, 10.6147, 6296356)  --hall8
	spawnMobile("kashyyyk", "uwari_aggro", 360, 46.7311, -13.0149, -93.3726, 82.031, 6296356)  --hall8
	spawnMobile("kashyyyk", "uwari_aggro", 360, 43.0243, -29.9735, -102.618, -38.3462, 6296356)  --hall8
	spawnMobile("kashyyyk", "uwari_aggro", 360, 44.7351, -29.8074, -86.6357, 32.3373, 6296356)  --hall8
	spawnMobile("kashyyyk", "ep3_myyydril_deathswarm", 600, 68.9688, -18.5871, -44.2902, -96.8756, 6296357)  --hall9
	spawnMobile("kashyyyk", "uwari_aggro", 360, 61.4853, -26.7455, -35.7015, -84.9094, 6296357)  --hall9
	spawnMobile("kashyyyk", "uwari_aggro", 360, 57.8007, -26.8113, -50.5588, -103.526, 6296357)  --hall9
	spawnMobile("kashyyyk", "ep3_myyydril_chasuli", 300, 228.008, -123.109, -299.456, -57.1849, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_compactor", 300, 208.863, -122.809, -323.031, 34.4489, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_greeter_guard", 300, 187.976, -123.284, -356.324, -52.2582, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_isdan", 300, 178, -117, -374, 0, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_ivesa", 300, 215.747, -123.503, -309.579, 51.7302, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_kallaarac", 300, 108, -123, -308, 160, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_kinesworthy", 300, 134, -155, -294, 31, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_kirrir", 300, 228, 110, -339, -138, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_mystic", 300, 114.374, -123.528, -319.023, -84.6334, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_nawika", 300, 224.398, -111.58, -353.986, -68.5096, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_old_warris", 300, 211.593, -122.919, -326.858, 20.4105, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_pers", 300, 165.486, -147.825, -286.364, -101.727, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_serileo", 300, 205.77, -114.578, -280.313, 146.393, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_treesh", 300, 105, -128, -358, 31, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "ep3_myyydril_yraka", 300, 185, -115, -317, 104, 6296382)  --lightningroom34
	spawnMobile("kashyyyk", "myyydril_erriya", 300, -388.898, -178.761, 175.349, -179.126, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -327.648, -193.512, 35.9007, 98.3814, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -358.279, -191.987, -24.3221, 16.7391, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -351.508, -183.641, 9.31434, -11.9412, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -351.349, -184.386, 20.1809, -4.5512, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -375.858, -192.969, 35.4495, 144.129, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -327.158, -190.19, 81.2808, -20.211, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -319.444, -189.231, 95.585, -157.278, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -336.972, -193.424, 109.427, -71.7651, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -385.543, -186.819, 121.203, -42.0289, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -386.172, -190.264, 105.224, -145.665, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -398.806, -190.779, 58.5673, 151.49, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -399.372, -191.382, 101.748, -25.5188, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -402.934, -186.789, 141.902, 31.8419, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -392.371, -189.679, 158.708, 26.3873, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -414.037, -191.639, 122.902, -124.932, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -317.388, -183.311, 65.9697, 137.59, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -384.542, -190.937, 156.198, 86.7394, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -361.438, -187.467, 191.655, 109.613, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -346.378, -188.162, 190.403, 91.8423, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -306.971, -188.21, 188.575, -59.6536, 6296401)  --morag53
	spawnMobile("kashyyyk", "myyydril_sick", 300, -299.424, -190.363, 167.224, 159.936, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -338.327, -185.646, -29.9445, 163.308, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -389.421, -186.188, 21.8683, -102.909, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -311.893, -178.82, 55.9735, -16.1641, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -354.376, -190.02, 115.88, -48.1873, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -388.036, -183.492, 133.571, 173.308, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -415.258, -192.434, 110.468, -120.182, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -418.987, -182.892, 67.0751, -165.225, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -321.613, -190.744, 27.722, -39.5946, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -325.542, -188.371, 192.278, 133.895, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_assassin", 600, -285.869, -188.864, 166.816, 93.6014, 6296401)  --morag53
	spawnMobile("kashyyyk", "urnsoris_nefarious", 1800, -362.731, -174.723, 68.1374, 169.966, 6296401)  --morag53

end
