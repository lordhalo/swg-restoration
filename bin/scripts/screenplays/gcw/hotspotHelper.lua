local ObjectManager = require("managers.object.object_manager")

hotspotHelper = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "hotspotHelper",


	imperialGroupOne = {
		{ "stormtrooper", 180, -2423, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2421, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2419, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2417, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2423, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2421, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2419, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2417, 38, 3825,  1, 0 }
	},

	imperialGroupTwo = {
		{ "stormtrooper", 180, -2360, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2362, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2364, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2366, 38, 3822,  1, 0 },
		{ "stormtrooper", 180, -2360, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2362, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2364, 38, 3825,  1, 0 },
		{ "stormtrooper", 180, -2366, 38, 3825,  1, 0 }
	},

	rebelGroupOne = {
		{ "rebel_assault_trooper", 180, -2423, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2421, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2419, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2417, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2423, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2421, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2419, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2417, 38, 3825,  1, 0 }
	},

	rebelGroupTwo = {
		{ "rebel_assault_trooper", 180, -2360, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2362, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2364, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2366, 38, 3822,  1, 0 },
		{ "rebel_assault_trooper", 180, -2360, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2362, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2364, 38, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2366, 38, 3825,  1, 0 }
	},

	rebelPatrol = {
		{ "rebel_assault_trooper", 180, -2472, 42, 3873,  1, 0 },
		{ "rebel_assault_trooper", 180, -2506, 37, 3825,  1, 0 },
		{ "rebel_assault_trooper", 180, -2524, 38, 3764,  1, 0 },
		{ "rebel_assault_trooper", 180, -2527, 38, 3698,  1, 0 },
		{ "rebel_assault_trooper", 180, -2486, 38, 3697,  1, 0 },
		{ "rebel_assault_trooper", 180, -2429, 38, 3685,  1, 0 },
		{ "rebel_assault_trooper", 180, -2385, 38, 3710,  1, 0 },
		{ "rebel_assault_trooper", 180, -2459, 38, 3750,  1, 0 },
		{ "rebel_assault_trooper", 180, -2317, 38, 3746,  1, 0 },
		{ "rebel_assault_trooper", 180, -2293, 38, 3795,  1, 0 },
		{ "rebel_assault_trooper", 180, -2270, 38, 3721,  1, 0 },
		{ "at_xt", 180, -2369, 38, 3638,  1, 0 },
		{ "rebel_assault_trooper", 180, -2347, 38, 3688,  1, 0 }
	},

	imperialPatrol = {
		{ "stormtrooper", 180, -2472, 42, 3873,  1, 0 },
		{ "stormtrooper", 180, -2506, 37, 3825,  1, 0 },
		{ "stormtrooper", 180, -2524, 38, 3764,  1, 0 },
		{ "stormtrooper", 180, -2527, 38, 3698,  1, 0 },
		{ "stormtrooper", 180, -2486, 38, 3697,  1, 0 },
		{ "stormtrooper", 180, -2429, 38, 3685,  1, 0 },
		{ "stormtrooper", 180, -2385, 38, 3710,  1, 0 },
		{ "stormtrooper", 180, -2459, 38, 3750,  1, 0 },
		{ "stormtrooper", 180, -2317, 38, 3746,  1, 0 },
		{ "stormtrooper", 180, -2293, 38, 3795,  1, 0 },
		{ "stormtrooper", 180, -2270, 38, 3721,  1, 0 },
		{ "at_st", 180, -2369, 38, 3638,  1, 0 },
		{ "stormtrooper", 180, -2347, 38, 3688,  1, 0 }
	},

	rebelFarm = {
		{ "dark_trooper_pvp", 180, -2485, 53, 3549,  1, 0 },
		{ "dark_trooper_pvp", 180, -2393, 38, 3583,  1, 0 },
		{ "dark_trooper_pvp", 180, -2347, 144, 3485,  1, 0 },
		{ "dark_trooper_pvp", 180, -2323, 142, 3404,  1, 0 }

	},

	imperialFarm = {
		{ "rebel_specforce_pvp", 180, -2375, 43, 3985,  1, 0 },
		{ "rebel_specforce_pvp", 180, -2474, 35, 4031,  1, 0 },
		{ "rebel_specforce_pvp", 180, -2283, 17, 3992,  1, 0 },
		{ "rebel_specforce_pvp", 180, -2341, 44, 3906,  1, 0 }

	}
}
  
registerScreenPlay("hotspotHelper", true)

function hotspotHelper:spawnImperialGroupOne()

	if(readData("hotspot:imperialOwned") == 1) then

		local mobileTable = self.imperialGroupOne
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnNPC", pMobile)
		end

	end

end

function hotspotHelper:spawnImperialGroupTwo()

	if(readData("hotspot:imperialOwned") == 1) then

		local mobileTable = self.imperialGroupTwo
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnNPCTwo", pMobile)
		end

	end

end

function hotspotHelper:spawnRebelGroupOne()

	if(readData("hotspot:rebelOwned") == 1) then

		local mobileTable = self.rebelGroupOne
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnNPCRebel", pMobile)
		end

	end

end

function hotspotHelper:spawnRebelGroupTwo()

	if(readData("hotspot:rebelOwned") == 1) then

		local mobileTable = self.rebelGroupTwo
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnNPCRebelTwo", pMobile)
		end

	end

end

function hotspotHelper:spawnPatrol()

	if(readData("hotspot:imperialOwned") == 1) then

		local mobileTable = self.imperialPatrol
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			if (pMobile ~= nil and CreatureObject(pMobile):isAiAgent()) then
				AiAgent(pMobile):setAiTemplate("")
			end
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnPatrol", pMobile)
		end

	end

end

function hotspotHelper:spawnRebelPatrol()

	if(readData("hotspot:rebelOwned") == 1) then

		local mobileTable = self.rebelPatrol
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			if (pMobile ~= nil and CreatureObject(pMobile):isAiAgent()) then
				AiAgent(pMobile):setAiTemplate("")
			end
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnPatrolRebel", pMobile)
		end

	end

end

function hotspotHelper:spawnRebelFarm()

	if(readData("hotspot:rebelOwned") == 1) then

		local mobileTable = self.rebelFarm
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			if (pMobile ~= nil and CreatureObject(pMobile):isAiAgent()) then
				AiAgent(pMobile):setAiTemplate("")
			end
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnFarmRebel", pMobile)
		end

	end

end

function hotspotHelper:spawnImperialFarm()

	if(readData("hotspot:imperialOwned") == 1) then

		local mobileTable = self.imperialFarm
		for i = 1, table.getn(mobileTable), 1 do
			local pMobile = spawnMobile("talus", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			if (pMobile ~= nil and CreatureObject(pMobile):isAiAgent()) then
				AiAgent(pMobile):setAiTemplate("")
			end
			createObserver(OBJECTDESTRUCTION, "hotspotHelper", "respawnFarmImperial", pMobile)
		end

	end

end

function hotspotHelper:respawnNPC(pMobile)
	writeData("hotspot:npcsDead",readData("hotspot:npcsDead") + 1)

	if(readData("hotspot:npcsDead") == 8) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnImperialGroupOne", pMobile)
		writeData("hotspot:npcsDead",0)
	end
	return 0
end

function hotspotHelper:respawnNPCTwo(pMobile)
	writeData("hotspot:npcsTwoDead",readData("hotspot:npcsTwoDead") + 1)

	if(readData("hotspot:npcsTwoDead") == 8) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnImperialGroupTwo", pMobile)
		writeData("hotspot:npcsTwoDead",0)
	end
	return 0
end

function hotspotHelper:respawnNPCRebel(pMobile)
	writeData("hotspot:npcsRebelDead",readData("hotspot:npcsRebelDead") + 1)

	if(readData("hotspot:npcsRebelDead") == 8) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnRebelGroupOne", pMobile)
		writeData("hotspot:npcsRebelDead",0)
	end
	return 0
end

function hotspotHelper:respawnNPCRebelTwo(pMobile)
	writeData("hotspot:npcsRebelTwoDead",readData("hotspot:npcsRebelTwoDead") + 1)

	if(readData("hotspot:npcsRebelTwoDead") == 8) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnRebelGroupTwo", pMobile)
		writeData("hotspot:npcsRebelTwoDead",0)
	end
	return 0
end

function hotspotHelper:respawnPatrol(pMobile)
	writeData("hotspot:npcsPatrolDead",readData("hotspot:npcsPatrolDead") + 1)

	if(readData("hotspot:npcsPatrolDead") == 13) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnPatrol", pMobile)
		writeData("hotspot:npcsPatrolDead",0)
	end
	return 0
end

function hotspotHelper:respawnPatrolRebel(pMobile)
	writeData("hotspot:npcsRebelPatrolDead",readData("hotspot:npcsRebelPatrolDead") + 1)

	if(readData("hotspot:npcsRebelPatrolDead") == 13) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnRebelPatrol", pMobile)
		writeData("hotspot:npcsRebelPatrolDead",0)
	end
	return 0
end

function hotspotHelper:respawnFarmRebel(pMobile)
	writeData("hotspot:npcsRebelFarmDead",readData("hotspot:npcsRebelFarmDead") + 1)

	if(readData("hotspot:npcsRebelFarmDead") == 4) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnRebelFarm", pMobile)
		writeData("hotspot:npcsRebelFarmDead",0)
	end
	return 0
end

function hotspotHelper:respawnFarmImperial(pMobile)
	writeData("hotspot:npcsImperialFarmDead",readData("hotspot:npcsImperialFarmDead") + 1)

	if(readData("hotspot:npcsImperialFarmDead") == 4) then
		--self:spawnImperialGroupOne()
		createEvent(2 * 60 * 1000, "hotspotHelper", "spawnImperialFarm", pMobile)
		writeData("hotspot:npcsImperialFarmDead",0)
	end
	return 0
end
