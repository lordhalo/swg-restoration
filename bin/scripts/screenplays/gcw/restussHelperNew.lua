local ObjectManager = require("managers.object.object_manager")

restussHelperNew = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "restussHelperNew",

	imperialNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	imperialEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	imperialSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	imperialWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	rebelNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	rebelEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	rebelSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	rebelWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	northCommNPCs = {
		{ "dark_trooper", "rebel_commando", 5365, 80, 5760,  132, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5357, 80, 5760,  173, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5357, 80, 5767,  -49, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5365, 80, 5767,  38, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5392, 80, 5721,  149, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5361, 80, 5718,  165, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5398, 80, 5783,  71, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5409, 80, 5766,  112, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5429, 80, 5805,  -92, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5435, 80, 5823,  -92, 0, "", ""  },

	},

	eastCommNPCs = {
		{ "dark_trooper", "rebel_commando", 5429, 80, 5622,  -2, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5438, 80, 5624,  -2, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5438, 80, 5614,  -179, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5432, 80, 5616,  -107, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5428, 80, 5590,  -127, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5410, 80, 5590,  -131, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5402, 80, 5615,  -39, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5401, 80, 5562,  -70, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5418, 80, 5555,  163, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5378, 80, 5591,  -148, 0, "", ""  },

	},

	southCommNPCs = {
		{ "dark_trooper", "rebel_commando", 5258, 80, 5537,  1, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5250, 80, 5537,  1, 0, "", "" },
		{ "stormtrooper", "rebel_trooper", 5250, 80, 5530,  1, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5256, 80, 5539,  1, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5263, 80, 5544,  55, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5231, 80, 5545,  -121, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5231, 80, 5568,  1, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5287, 80, 5561,  75, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5199, 80, 5553,  -90, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5212, 80, 5564,  1, 0, "", ""  },

	},

	westCommNPCs = {
		{ "dark_trooper", "rebel_commando", 5165, 80, 5685,  -61, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5173, 80, 5685,  52, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5174, 80, 5678,  109, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5165, 80, 5678,  -62, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5144, 80, 5688,  -139, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5137, 80, 5706,  -133, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5135, 80, 5719,  -50, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5187, 80, 5702,  35, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5185, 80, 5717,  34, 0, "", ""  },
		{ "stormtrooper", "rebel_trooper", 5186, 80, 5734,  -63, 0, "", ""  },

	},

	rebelAntenna = { 
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5193, 80, 5763, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5185, 80, 5543, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5297, 80, 5592, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5248, 80, 5848, 0, 1, 0, 0, 0 }
	},

	imperialAntenna = { 
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5476, 80, 5778, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5462, 80, 5638, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5355, 80, 5555, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5384, 80, 5854, 0, 1, 0, 0, 0 }
	},

	northCommArray = {
		{ "object/tangible/quest/restuss_comm_array.iff", 5361, 80, 5763, 0, 1, 0, 0, 0 }
	},

	eastCommArray = {
		{ "object/tangible/quest/restuss_comm_array.iff", 5255, 80, 5533, 0, 1, 0, 0, 0 }
	},

	southCommArray = {
		{ "object/tangible/quest/restuss_comm_array.iff", 5435, 80, 5620, 0, 1, 0, 0, 0 }
	},

	westCommArray = {
		{ "object/tangible/quest/restuss_comm_array.iff", 5169, 80, 5681, 0, 1, 0, 0, 0 }
	}

}
  
registerScreenPlay("restussHelperNew", true)

function restussHelperNew:spawnNorthComm()

	--if(readData("hotspot:imperialOwned") == 1) then DID IT MATTER WHO OWNED FOR ANTENNA?

		local sceneObject = self.northCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelperNew", "northCommArrayHacked", pComm)
		end

	--end

end

function restussHelperNew:spawnWestComm()

	--if(readData("hotspot:imperialOwned") == 1) then DID IT MATTER WHO OWNED FOR ANTENNA?

		local sceneObject = self.westCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelperNew", "westCommArrayHacked", pComm)
		end

	--end

end

function restussHelperNew:spawnImperialAntenna()

	--if(readData("hotspot:imperialOwned") == 1) then DID IT MATTER WHO OWNED FOR ANTENNA?

		local sceneObject = self.imperialAntenna
		for i = 1, #sceneObject, 1 do
			local pAntenna = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTDESTRUCTION, "restussHelperNew", "imperialAntennaDestroyed", pAntenna)
		end

	--end

end

function restussHelperNew:spawnRebelAntenna()

	--if(readData("hotspot:imperialOwned") == 1) then DID IT MATTER WHO OWNED FOR ANTENNA?

		local sceneObject = self.rebelAntenna
		for i = 1, #sceneObject, 1 do
			local pAntenna = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTDESTRUCTION, "restussHelperNew", "rebelAntennaDestroyed", pAntenna)
		end

	--end

end


function restussHelperNew:spawnImperialNorth()

	--if(readData("restussManager:imperialOwned") == 1) then

		local mobileTable = self.northCommNPCs
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("rori", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "restussHelperNew", "respawnImperialNorth", pMobile)
		end

	--end	

end

function restussHelperNew:spawnImperialWest()

	--if(readData("restussManager:imperialOwned") == 1) then

		local mobileTable = self.imperialWestGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("rori", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "restussHelperNew", "respawnImperialWest", pMobile)
		end

	--end	

end

function restussHelperNew:spawnRebelWest()

	--if(readData("restussManager:imperialOwned") == 1) then

		local mobileTable = self.rebelWestGroup
		for i = 1, #mobileTable, 1 do
			local pMobile = spawnMobile("rori", mobileTable[i][1], mobileTable[i][2], mobileTable[i][3], mobileTable[i][4], mobileTable[i][5], mobileTable[i][6], 0)
			createObserver(OBJECTDESTRUCTION, "restussHelperNew", "respawnRebelWest", pMobile)
		end

	--end	

end

function restussHelperNew:spawnNorthMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.northCommNPCs
		
		for i = 1, #self.northCommNPCs do
			restussCommSpawners:spawnNorthMob(i, controllingFaction, difficulty)
		end

end

function restussHelperNew:spawnWestMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.westCommNPCs
		
		for i = 1, #self.westCommNPCs do
			restussCommSpawners:spawnWestMob(i, controllingFaction, difficulty)
		end

end


function restussHelperNew:rebelAntennaDestroyed(pTower, playerObject)
	local towerObject = LuaSceneObject(pTower)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "antennaDestructionImperial")


	towerObject:playEffect("clienteffect/combat_explosion_lair_large.cef", "")

	towerObject:destroyObjectFromWorld()

	player:clearCombatState(1)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "antennaDestructionImperial")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, antennaDestructionImperial.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "antennaDestructionImperial")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, antennaDestructionImperial.questString)
					end
			end
		end)
	return 0
end

function restussHelperNew:imperialAntennaDestroyed(pTower, playerObject)
	local towerObject = LuaSceneObject(pTower)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "antennaDestructionRebel")

	towerObject:playEffect("clienteffect/combat_explosion_lair_large.cef", "")

	towerObject:destroyObjectFromWorld()

	player:clearCombatState(1)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "antennaDestructionRebel")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, antennaDestructionRebel.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "antennaDestructionRebel")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, antennaDestructionRebel.questString)
					end
			end
		end)
	return 0
end

function restussHelperNew:northCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if (faction == FACTIONIMPERIAL and (readData("restuss:northBannerImperial") == 0)) then
		restussHelperNew:spawnNorthMobs(faction)
		restussHelperNew:spawnNorthBanner(faction)
		restussHelperNew:destroyNorthBanner()
		writeData("restuss:northBannerImperial", 1)
	elseif (faction == FACTIONREBEL) then
		restussHelperNew:spawnNorthMobs(faction)
		restussHelperNew:spawnNorthBanner(faction)
		restussHelperNew:destroyNorthBanner()
		writeData("restuss:northBannerImperial", 0)
	end

	return 0
end

function restussHelperNew:westCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if CreatureObject(playerObject):isInCombat() or CreatureObject(playerObject):isIncapacitated() or CreatureObject(playerObject):isDead() then
		player:sendSystemMessage("You must exit Combat.")
		return 0
	end

	if (faction == FACTIONIMPERIAL and (readData("restuss:westBannerImperial") == 0)) then
		restussHelperNew:spawnWestMobs(faction)
		restussHelperNew:spawnWestBanner(faction)
		restussHelperNew:destroyWestBanner()
		writeData("restuss:westBannerImperial", 1)
	elseif (faction == FACTIONREBEL) then
		restussHelperNew:spawnWestMobs(faction)
		restussHelperNew:spawnWestBanner(faction)
		restussHelperNew:destroyWestBanner()
		writeData("restuss:westBannerImperial", 0)
	end

	return 0
end

function restussHelperNew:spawnNorthBanner(faction)

	local sceneObject = nil

	if (faction == FACTIONIMPERIAL) then
		sceneObject = self.imperialNorthBanner

	elseif (faction == FACTIONREBEL) then
		sceneObject = self.rebelNorthBanner

	end

		for i = 1, #sceneObject, 1 do
			local pBanner = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			local bannerID = SceneObject(pBanner):getObjectID()
			--writeData("northBanner" .. bannerID)
			writeData("restuss:northBanner", bannerID)
		end
end

function restussHelperNew:spawnEastBanner(faction)

	local sceneObject = nil

	if (faction == FACTIONIMPERIAL) then
		sceneObject = self.imperialEastBanner

	elseif (faction == FACTIONREBEL) then
		sceneObject = self.rebelEastBanner

	end

		for i = 1, #sceneObject, 1 do
			local pBanner = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			--writeData("restuss:northBanner", SceneObject(pBanner):getObjectID())
		end

end

function restussHelperNew:spawnSouthBanner(faction)

	local sceneObject = nil

	if (faction == FACTIONIMPERIAL) then
		sceneObject = self.imperialSouthBanner

	elseif (faction == FACTIONREBEL) then
		sceneObject = self.rebelSouthBanner

	end

		for i = 1, #sceneObject, 1 do
			local pBanner = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			local bannerID = SceneObject(pBanner):getObjectID()
			writeData("restuss:southBanner", bannerID)
		end

end

function restussHelperNew:spawnWestBanner(faction)

	local sceneObject = nil

	if (faction == FACTIONIMPERIAL) then
		sceneObject = self.imperialWestBanner

	elseif (faction == FACTIONREBEL) then
		sceneObject = self.rebelWestBanner

	end

		for i = 1, #sceneObject, 1 do
			local pBanner = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			local bannerID = SceneObject(pBanner):getObjectID()
			--writeData("restuss:westBanner", bannerID)
		end

end


function restussHelperNew:destroyNorthBanner()

		--writeData("northBanner:" .. bannerID, 0)
		--local bannerID = readData("northBanner")
		local bannerID = 0

		bannerID = readData("restuss:northBanner")

		local pBanner = getSceneObject(bannerID)

		if (pBanner ~= nil) then
			SceneObject(pBanner):destroyObjectFromWorld()
			deleteData(bannerID)
		end
		return 0

end

function restussHelperNew:destroyEastBanner()

		local bannerID = readData("restuss:eastBanner")

		local pBanner = getSceneObject(bannerID)

		if (pBanner ~= nil) then
			SceneObject(pBanner):destroyObjectFromWorld()
		end
		return 0

end

function restussHelperNew:destroySouthBanner()

		local bannerID = readData("restuss:southBanner")

		local pBanner = getSceneObject(bannerID)

		if (pBanner ~= nil) then
			SceneObject(pBanner):destroyObjectFromWorld()
		end
		return 0

end

function restussHelperNew:destroyWestBanner()

		local bannerID = readData("restuss:westBanner")

		local pBanner = getSceneObject(bannerID)

		if (pBanner ~= nil) then
			SceneObject(pBanner):destroyObjectFromWorld()
		end
		return 0

end

function restussHelperNew:spawnMob(num, controllingFaction, difficulty, npcTable)
	local mobsTable = npcTable

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if controllingFaction == FACTIONIMPERIAL then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif controllingFaction == FACTIONREBEL then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	if difficulty > 1 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	if pNpc ~= nil then
		if npcMood ~= "" then
			self:setMoodString(pNpc, npcMood)
		end
		if mobTable[10] then
			local aiAgent = AiAgent(pNpc)
			aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		end
	end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onDespawn", pNpc)
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussHelperNew:onDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)

	createEvent(100000, "restussHelperNew", "respawn", nil, tostring(mobNumber))

	return 1
end

function restussHelperNew:respawn(pAiAgent, args)
	local mobNumber = tonumber(args)
	local controllingFaction = getControllingFaction("rori")
	local difficulty = getWinningFactionDifficultyScaling("rori")

	self:spawnMob(mobNumber, controllingFaction, difficulty)
end

--Saved but unused function

function restussHelperNew:respawnImperialWest(pMobile)
	--writeData("restussManager:imperialNorth",readData("restussManager:imperialNorth") + 1)
	--writeData("restussManager:rebelProgress", readData("restussManager:rebelProgress") + 1)

	--if(readData("restussManager:rebelProgress") == 20) then -- Should be 250
	--	restuss:flipRebel()
	--	writeData("restussManager:rebelProgress",0)
	--end

	if(readData("restussManager:imperialNorth") == 10 and readData("restussManager:imperialOwned") == 1) then
		createEvent(2 * 60 * 1000, "restussHelperNew", "spawnImperialNorth", pMobile)
		writeData("restussManager:imperialNorth",0)
	end
	return 0
end
