local ObjectManager = require("managers.object.object_manager")

restussCommSpawners = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "restussCommSpawners",

	imperialNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	imperialEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	imperialSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	imperialWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	rebelNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	rebelEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	rebelSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	rebelWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	northCommNPCs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5365, 80, 5760,  132, 0, "", "" },
		{ "novatrooper_squad_leader_restuss", "rebel_elite_heavy_trooper_restuss", 5357, 80, 5760,  173, 0, "", "" },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5357, 80, 5767,  -49, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5365, 80, 5767,  38, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5392, 80, 5721,  149, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5361, 80, 5718,  165, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5398, 80, 5783,  71, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5409, 80, 5766,  112, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5429, 80, 5805,  -92, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5435, 80, 5823,  -92, 0, "", ""  },

	},

	southCommNPCs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5429, 80, 5622,  -2, 0, "", "" },
		{ "novatrooper_squad_leader_restuss", "rebel_elite_heavy_trooper_restuss", 5438, 80, 5624,  -2, 0, "", "" },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5438, 80, 5614,  -179, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5432, 80, 5616,  -107, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5428, 80, 5590,  -127, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5410, 80, 5590,  -131, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5402, 80, 5615,  -39, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5401, 80, 5562,  -70, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5418, 80, 5555,  163, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5378, 80, 5591,  -148, 0, "", ""  },

	},

	eastCommNPCs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5258, 80, 5537,  1, 0, "", "" },
		{ "novatrooper_squad_leader_restuss", "rebel_elite_heavy_trooper_restuss", 5250, 80, 5537,  1, 0, "", "" },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5250, 80, 5530,  1, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5256, 80, 5539,  1, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5263, 80, 5544,  55, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5231, 80, 5545,  -121, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5231, 80, 5568,  1, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5287, 80, 5561,  75, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5199, 80, 5553,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5212, 80, 5564,  1, 0, "", ""  },

	},

	westCommNPCs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5165, 80, 5685,  -61, 0, "", ""  },
		{ "novatrooper_squad_leader_restuss", "rebel_elite_heavy_trooper_restuss", 5173, 80, 5685,  52, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5174, 80, 5678,  109, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5165, 80, 5678,  -62, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5144, 80, 5688,  -139, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5137, 80, 5706,  -133, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5135, 80, 5719,  -50, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5187, 80, 5702,  35, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5185, 80, 5717,  34, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5186, 80, 5734,  -63, 0, "", ""  },

	},


}
  
registerScreenPlay("restussCommSpawners", true)

function restussCommSpawners:spawnNorthMob(num, controllingFaction, difficulty)
	local mobsTable = self.northCommNPCs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if controllingFaction == FACTIONIMPERIAL then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif controllingFaction == FACTIONREBEL then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(100)
	if random >= 90 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	if pNpc ~= nil then
		if npcMood ~= "" then
			self:setMoodString(pNpc, npcMood)
		end
		if mobTable[10] then
			local aiAgent = AiAgent(pNpc)
			aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		end
	end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onNorthDespawn", pNpc)
		createEvent(1800 * 1000, "restussCommSpawners", "despawnCommNPCs", pNpc, "")
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussCommSpawners:onNorthDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)

	createEvent(100000, "restussCommSpawners", "respawnNorth", nil, tostring(mobNumber))

	return 1
end

function restussCommSpawners:respawnNorth(pAiAgent, args)
	local mobNumber = tonumber(args)
	local controllingFaction
	local difficulty = 0 --getWinningFactionDifficultyScaling("rori")

	if (readData("restussManager:imperialOwned") == 1)then
		controllingFaction = FACTIONIMPERIAL
	elseif (readData("restussManager:rebelOwned") == 1) then
		controllingFaction = FACTIONREBEL
	end

	self:spawnNorthMob(mobNumber, controllingFaction, difficulty)
end

---------------------------------------------------------------------------

function restussCommSpawners:spawnEastMob(num, controllingFaction, difficulty)
	local mobsTable = self.eastCommNPCs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if controllingFaction == FACTIONIMPERIAL then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif controllingFaction == FACTIONREBEL then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(100)
	if random >= 90 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	if pNpc ~= nil then
		if npcMood ~= "" then
			self:setMoodString(pNpc, npcMood)
		end
		if mobTable[10] then
			local aiAgent = AiAgent(pNpc)
			aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		end
	end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onEastDespawn", pNpc)
		createEvent(1800 * 1000, "restussCommSpawners", "despawnCommNPCs", pNpc, "")
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussCommSpawners:onEastDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)

	createEvent(100000, "restussCommSpawners", "respawnEast", nil, tostring(mobNumber))

	return 1
end

function restussCommSpawners:respawnEast(pAiAgent, args)
	local mobNumber = tonumber(args)
	local controllingFaction
	local difficulty = 0 --getWinningFactionDifficultyScaling("rori")

	if (readData("restussManager:imperialOwned") == 1)then
		controllingFaction = FACTIONIMPERIAL
	elseif (readData("restussManager:rebelOwned") == 1) then
		controllingFaction = FACTIONREBEL
	end

	self:spawnEastMob(mobNumber, controllingFaction, difficulty)
end

---------------------------------------------------------------------------

function restussCommSpawners:spawnSouthMob(num, controllingFaction, difficulty)
	local mobsTable = self.southCommNPCs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if controllingFaction == FACTIONIMPERIAL then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif controllingFaction == FACTIONREBEL then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(90)
	if random >= 50 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	if pNpc ~= nil then
		if npcMood ~= "" then
			self:setMoodString(pNpc, npcMood)
		end
		if mobTable[10] then
			local aiAgent = AiAgent(pNpc)
			aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		end
	end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onSouthDespawn", pNpc)
		createEvent(1800 * 1000, "restussCommSpawners", "despawnCommNPCs", pNpc, "")
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussCommSpawners:onSouthDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)

	createEvent(100000, "restussCommSpawners", "respawnSouth", nil, tostring(mobNumber))

	return 1
end

function restussCommSpawners:respawnSouth(pAiAgent, args)
	local mobNumber = tonumber(args)
	local controllingFaction
	local difficulty = 0 --getWinningFactionDifficultyScaling("rori")

	if (readData("restussManager:imperialOwned") == 1)then
		controllingFaction = FACTIONIMPERIAL
	elseif (readData("restussManager:rebelOwned") == 1) then
		controllingFaction = FACTIONREBEL
	end

	self:spawnSouthMob(mobNumber, controllingFaction, difficulty)
end

---------------------------------------------------------------------------

function restussCommSpawners:spawnWestMob(num, controllingFaction, difficulty)
	local mobsTable = self.westCommNPCs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if controllingFaction == FACTIONIMPERIAL then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif controllingFaction == FACTIONREBEL then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	local random = math.random(100)
	if random >= 90 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	if pNpc ~= nil then
		if npcMood ~= "" then
			self:setMoodString(pNpc, npcMood)
		end
		if mobTable[10] then
			local aiAgent = AiAgent(pNpc)
			aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		end
	end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onWestDespawn", pNpc)
		createEvent(1800 * 1000, "restussCommSpawners", "despawnCommNPCs", pNpc, "")
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussCommSpawners:onWestDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)

	createEvent(100000, "restussCommSpawners", "respawnWest", nil, tostring(mobNumber))

	return 1
end

function restussCommSpawners:respawnWest(pAiAgent, args)
	local mobNumber = tonumber(args)
	local controllingFaction
	local difficulty = 0 --getWinningFactionDifficultyScaling("rori")

	if (readData("restussManager:imperialOwned") == 1)then
		controllingFaction = FACTIONIMPERIAL
	elseif (readData("restussManager:rebelOwned") == 1) then
		controllingFaction = FACTIONREBEL
	end

	self:spawnWestMob(mobNumber, controllingFaction, difficulty)
end

function restussCommSpawners:despawnCommNPCs(pMobile)
	if (pMobile == nil) then
		return
	end

	if pMobile ~= nil then
		SceneObject(pMobile):destroyObjectFromWorld()
	end

	return 0
end
