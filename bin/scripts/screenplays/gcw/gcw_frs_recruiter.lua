local ObjectManager = require("managers.object.object_manager")

gcwFrsRecruiter = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "gcwFrsRecruiter",

}
registerScreenPlay("gcwFrsRecruiter", true)

function gcwFrsRecruiter:start()
	if (isZoneEnabled("naboo")) then
		--self:spawnMobiles()
		--self:spawnSceneObjects()
		--self:spawnActiveAreas()
	end
end


--Mobile Spawning
function gcwFrsRecruiter:spawnMobiles()
	local pNpc = spawnMobile("naboo", "grey_recruiter", 1, -4900, 6, 4110,  35, 0)

end

gcw_frs_recruiter_convo_handler = Object:new {
	
 }

function gcw_frs_recruiter_convo_handler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

			local hasJedi = CreatureObject(conversingPlayer):hasSkill("force_title_jedi_rank_01")
			local hasImp = CreatureObject(conversingPlayer):hasSkill("imp_rank_novice")
			local hasReb = CreatureObject(conversingPlayer):hasSkill("reb_rank_novice")
			local hasDwb = CreatureObject(conversingPlayer):hasSkill("death_watch_novice")
			local hasBsb = CreatureObject(conversingPlayer):hasSkill("black_sun_novice")
			local hasMandalorian = CreatureObject(conversingPlayer):hasSkill("mandalorian_novice")
			local getFaction = CreatureObject(conversingPlayer):getFaction()
			local hasFrsImp = CreatureObject(conversingPlayer):hasSkill("imp_rank_novice")
			local hasFrsReb = CreatureObject(conversingPlayer):hasSkill("reb_rank_novice")

				if (hasJedi == true or hasImp == true or hasReb or hasDwb or hasBsb or hasMandalorian) then
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				elseif (getFaction == hasFrsImp or hasFrsReb) then
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				elseif (getFaction == 3679112276) then
					nextConversationScreen = conversation:getScreen("imperial_intro_screen")
				elseif (getFaction == 370444368) then
					nextConversationScreen = conversation:getScreen("rebel_intro_screen")
				else
					nextConversationScreen = conversation:getScreen("no_quest_screen")
				end

		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function gcw_frs_recruiter_convo_handler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	
	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()	
	local player = LuaCreatureObject(conversingPlayer)
	local pGhost = CreatureObject(conversingPlayer):getPlayerObject()
	local questNPC = LuaCreatureObject(conversingNPC)	
	local pInventory = player:getSlottedObject("inventory")

	if ( screenID == "imperial_complete_screen" ) then

		PlayerObject(pGhost):setFrsCouncil(8)
		PlayerObject(pGhost):setFrsRank(0)
		awardSkill(conversingPlayer, "imp_rank_novice")
		CreatureObject(conversingPlayer):setFactionStatus(2)

	end

	if ( screenID == "rebel_complete_screen" ) then

		PlayerObject(pGhost):setFrsCouncil(9)
		PlayerObject(pGhost):setFrsRank(0)
		awardSkill(conversingPlayer, "reb_rank_novice")
		CreatureObject(conversingPlayer):setFactionStatus(2)

	end


	return conversationScreen
end
