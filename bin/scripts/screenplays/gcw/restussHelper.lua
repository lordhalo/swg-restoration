local ObjectManager = require("managers.object.object_manager")

restussHelper = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "restussHelper",

	imperialNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	imperialEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	imperialSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	imperialWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_imperial.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	rebelNorthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5361, 80, 5761, 0, 1, 0, 0, 0 }, --West
	},

	rebelEastBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5435, 80, 5618, 0, 1, 0, 0, 0 }, --East
	},

	rebelSouthBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5255, 80, 5535, 0, 1, 0, 0, 0 }, --South
	},

	rebelWestBanner = {
		{ "object/tangible/gcw/flip_banner_onpole_rebel.iff", 5170, 80, 5683, 0, 1, 0, 0, 0 } --North
	},

	cityControlMobs = {
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5286, 80, 5772,  -61, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5286, 80, 5774,  52, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5286, 80, 5776,  109, 0, "", ""  },

		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5235, 80, 5830,  109, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5235, 80, 5826,  109, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5232, 80, 5830,  109, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5232, 80, 5826,  109, 0, "", ""  },

		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5170, 80, 5800,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5167, 80, 5800,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5164, 80, 5800,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5161, 80, 5800,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5158, 80, 5800,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5155, 80, 5800,  -90, 0, "", ""  },

		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5140, 80, 5750,  90, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5143, 80, 5750,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5146, 80, 5750,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5149, 80, 5750,  90, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5140, 80, 5745,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5143, 80, 5745,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5146, 80, 5745,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5149, 80, 5745,  90, 0, "", ""  },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5140, 80, 5695,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5143, 80, 5695,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5146, 80, 5695,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5149, 80, 5695,  90, 0, "", ""  },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5140, 80, 5690,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5143, 80, 5690,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5146, 80, 5690,  90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5149, 80, 5690,  90, 0, "", ""  },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5227, 80, 5673,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5227, 80, 5675,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5218, 80, 5667,  -174, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5215, 80, 5682,  -174, 0, "", ""  },

		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5248, 80, 5600,  0, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5248, 80, 5596,  0, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5248, 80, 5592,  0, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5248, 80, 5588,  0, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5248, 80, 5584,  0, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5252, 80, 5600,  0, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5252, 80, 5596,  0, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5252, 80, 5592,  0, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5252, 80, 5588,  0, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5252, 80, 5584,  0, 0, "", ""  },

		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5412, 80, 5586,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5412, 80, 5582,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5412, 80, 5578,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5412, 80, 5574,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5408, 80, 5586,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5408, 80, 5582,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5408, 80, 5578,  -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5408, 80, 5574,  -90, 0, "", ""  },

		{ "dark_trooper_restuss", "rebel_commando_restuss", 5446, 80, 5682, -90, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5442, 80, 5682, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5438, 80, 5682, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5434, 80, 5682, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5430, 80, 5682, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5426, 80, 5682, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5422, 80, 5682, -90, 0, "", ""  },
		{ "dark_trooper_restuss", "rebel_commando_restuss", 5446, 80, 5678, -90, 0, "", ""  },
		{ "assault_trooper_restuss", "rebel_assault_trooper_restuss", 5442, 80, 5678, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5438, 80, 5678, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5434, 80, 5678, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5430, 80, 5678, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5426, 80, 5678, -90, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5422, 80, 5678, -90, 0, "", ""  },

		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5468, 80, 5772,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5468, 80, 5776,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5468, 80, 5780,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5468, 80, 5784,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5472, 80, 5772,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5472, 80, 5776,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5472, 80, 5780,  190, 0, "", ""  },
		{ "stormtrooper_restuss", "rebel_trooper_restuss", 5472, 80, 5784,  190, 0, "", ""  },
	
	},

	rebelAntenna = { 
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5193, 80, 5763, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5185, 80, 5543, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5297, 80, 5592, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_rebel_antennae.iff", 5248, 80, 5848, 0, 1, 0, 0, 0 }
	},

	imperialAntenna = { 
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5476, 80, 5778, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5462, 80, 5638, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5355, 80, 5555, 0, 1, 0, 0, 0 },
		{ "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff", 5384, 80, 5854, 0, 1, 0, 0, 0 }
	},

	northCommArray = {
		{ "object/tangible/terminal/terminal_newbie_instrument.iff", 5361, 80, 5763, 0, 1, 0, 0, 0 }
	},

	eastCommArray = {
		{ "object/tangible/terminal/terminal_newbie_instrument.iff", 5255, 80, 5541, 0, 1, 0, 0, 0 }
	},

	southCommArray = {
		{ "object/tangible/terminal/terminal_newbie_instrument.iff", 5435, 80, 5620, 0, 1, 0, 0, 0 }
	},

	westCommArray = {
		{ "object/tangible/terminal/terminal_newbie_instrument.iff", 5169, 80, 5681, 0, 1, 0, 0, 0 }
	}

}
  
registerScreenPlay("restussHelper", true)

function restussHelper:spawnNorthComm()

		local sceneObject = self.northCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelper", "northCommArrayHacked", pComm)
		end

end

function restussHelper:spawnEastComm()

		local sceneObject = self.eastCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelper", "eastCommArrayHacked", pComm)
		end

end

function restussHelper:spawnSouthComm()

		local sceneObject = self.southCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelper", "southCommArrayHacked", pComm)
		end

end

function restussHelper:spawnWestComm()

		local sceneObject = self.westCommArray
		for i = 1, #sceneObject, 1 do
			local pComm = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTRADIALUSED, "restussHelper", "westCommArrayHacked", pComm)
		end

end

function restussHelper:spawnImperialAntenna()

	local sceneObject = self.imperialAntenna
		for i = 1, #sceneObject, 1 do
			local pAntenna = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTDESTRUCTION, "restussHelper", "imperialAntennaDestroyed", pAntenna)
		end

end

function restussHelper:spawnRebelAntenna()

		local sceneObject = self.rebelAntenna
		for i = 1, #sceneObject, 1 do
			local pAntenna = spawnSceneObject("rori", sceneObject[i][1], sceneObject[i][2], sceneObject[i][3], sceneObject[i][4], sceneObject[i][5], sceneObject[i][6], sceneObject[i][7], sceneObject[i][8], sceneObject[i][9])
			createObserver(OBJECTDESTRUCTION, "restussHelper", "rebelAntennaDestroyed", pAntenna)
		end

end


function restussHelper:spawnNorthMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.northCommNPCs
		
		for i = 1, #restussCommSpawners.northCommNPCs do
			restussCommSpawners:spawnNorthMob(i, controllingFaction, difficulty)
		end

end

function restussHelper:spawnEastMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.eastCommNPCs
		
		for i = 1, #restussCommSpawners.eastCommNPCs do
			restussCommSpawners:spawnEastMob(i, controllingFaction, difficulty)
		end

end

function restussHelper:spawnSouthMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.southCommNPCs
		
		for i = 1, #restussCommSpawners.southCommNPCs do
			restussCommSpawners:spawnSouthMob(i, controllingFaction, difficulty)
		end

end

function restussHelper:spawnWestMobs(controllingFaction)
		local difficulty = 1
		local npcTable = restussCommSpawners.westCommNPCs
		
		for i = 1, #restussCommSpawners.westCommNPCs do
			restussCommSpawners:spawnWestMob(i, controllingFaction, difficulty)
		end

end


function restussHelper:rebelAntennaDestroyed(pTower, playerObject)
	local towerObject = LuaSceneObject(pTower)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "antennaDestructionImperial")


	towerObject:playEffect("clienteffect/combat_explosion_lair_large.cef", "")

	towerObject:destroyObjectFromWorld()

	player:clearCombatState(1)

	writeData("restussManager:rebAntDest", readData("restussManager:rebAntDest") + 1)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "antennaDestructionImperial")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, antennaDestructionImperial.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "antennaDestructionImperial")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, antennaDestructionImperial.questString)
					end
			end
		end)
	return 0
end

function restussHelper:imperialAntennaDestroyed(pTower, playerObject)
	local towerObject = LuaSceneObject(pTower)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "antennaDestructionRebel")

	towerObject:playEffect("clienteffect/combat_explosion_lair_large.cef", "")

	towerObject:destroyObjectFromWorld()

	player:clearCombatState(1)

	writeData("restussManager:impAntDest", readData("restussManager:impAntDest") + 1)

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "antennaDestructionRebel")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 42)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, antennaDestructionRebel.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "antennaDestructionRebel")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, antennaDestructionRebel.questString)
					end
			end
		end)
	return 0
end

function restussHelper:northCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if CreatureObject(playerObject):isInCombat() or CreatureObject(playerObject):isIncapacitated() or CreatureObject(playerObject):isDead() then
		player:sendSystemMessage("You must exit Combat.")
		return 0
	end

	if (faction == FACTIONIMPERIAL) then
		if (readData("restuss:nothHackable") == 0) then
			restussHelper:spawnNorthMobs(faction)
			restussHelper:commArrayCompleteImperial(playerObject)
			writeData("restuss:nothHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 2, "") -- 1 == West, 2 == North, 3 == East, 4 == South
			if (readData("restussManager:impAntDest") == 4) then
				restuss:respawnImpAntenna()
			end
		elseif (readData("restuss:nothHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	elseif (faction == FACTIONREBEL) then
		if (readData("restuss:nothHackable") == 0) then
			restussHelper:spawnNorthMobs(faction)
			restussHelper:commArrayCompleteRebel(playerObject)
			writeData("restuss:nothHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 2, "")
			if (readData("restussManager:rebAntDest") == 4) then
				restuss:respawnRebAntenna()
			end
		elseif (readData("restuss:nothHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	end

	return 0
end

function restussHelper:eastCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if CreatureObject(playerObject):isInCombat() or CreatureObject(playerObject):isIncapacitated() or CreatureObject(playerObject):isDead() then
		player:sendSystemMessage("You must exit Combat.")
		return 0
	end

	if (faction == FACTIONIMPERIAL) then
		if (readData("restuss:eastHackable") == 0) then
			restussHelper:spawnEastMobs(faction)
			restussHelper:commArrayCompleteImperial(playerObject)
			writeData("restuss:eastHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 3, "") -- 1 == West, 2 == North, 3 == East, 4 == South
			if (readData("restussManager:impAntDest") == 4) then
				restuss:respawnImpAntenna()
			end
		elseif (readData("restuss:eastHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	elseif (faction == FACTIONREBEL) then
		if (readData("restuss:eastHackable") == 0) then
			restussHelper:spawnEastMobs(faction)
			restussHelper:commArrayCompleteRebel(playerObject)
			writeData("restuss:eastHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 3, "")
			if (readData("restussManager:rebAntDest") == 4) then
				restuss:respawnRebAntenna()
			end
		elseif (readData("restuss:eastHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	end

	return 0
end

function restussHelper:southCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if CreatureObject(playerObject):isInCombat() or CreatureObject(playerObject):isIncapacitated() or CreatureObject(playerObject):isDead() then
		player:sendSystemMessage("You must exit Combat.")
		return 0
	end

	if (faction == FACTIONIMPERIAL) then
		if (readData("restuss:southHackable") == 0) then
			restussHelper:spawnSouthMobs(faction)
			restussHelper:commArrayCompleteImperial(playerObject)
			writeData("restuss:southHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 4, "") -- 1 == West, 2 == North, 3 == East, 4 == South
			if (readData("restussManager:impAntDest") == 4) then
				restuss:respawnImpAntenna()
			end
		elseif (readData("restuss:southHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	elseif (faction == FACTIONREBEL) then
		if (readData("restuss:southHackable") == 0) then
			restussHelper:spawnSouthMobs(faction)
			restussHelper:commArrayCompleteRebel(playerObject)
			writeData("restuss:southHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 4, "")
			if (readData("restussManager:rebAntDest") == 4) then
				restuss:respawnRebAntenna()
			end
		elseif (readData("restuss:southHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	end

	return 0
end

function restussHelper:westCommArrayHacked(pArray, playerObject)
	local player = LuaCreatureObject(playerObject)
	local faction = CreatureObject(playerObject):getFaction()

	if CreatureObject(playerObject):isInCombat() or CreatureObject(playerObject):isIncapacitated() or CreatureObject(playerObject):isDead() then
		player:sendSystemMessage("You must exit Combat.")
		return 0
	end

	if (faction == FACTIONIMPERIAL) then
		if (readData("restuss:westHackable") == 0) then
			restussHelper:spawnWestMobs(faction)
			restussHelper:commArrayCompleteImperial(playerObject)
			writeData("restuss:westHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 1, "") -- 1 == West, 2 == North, 3 == East, 4 == South
			if (readData("restussManager:impAntDest") == 4) then
				restuss:respawnImpAntenna()
			end
		elseif (readData("restuss:westHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	elseif (faction == FACTIONREBEL) then
		if (readData("restuss:westHackable") == 0) then
			restussHelper:spawnWestMobs(faction)
			restussHelper:commArrayCompleteRebel(playerObject)
			writeData("restuss:westHackable", 1)
			createEvent(1800 * 1000, "restussHelper", "commHackableFlip", 1, "")
			if (readData("restussManager:rebAntDest") == 4) then
				restuss:respawnRebAntenna()
			end
		elseif (readData("restuss:westHackable") == 1) then
			player:sendSystemMessage("This Object has been hacked recently.")
		end
	end

	return 0
end

function restussHelper:spawnCityControlMobs(num, difficulty)
	local mobsTable = self.cityControlMobs

	if num <= 0 or num > #mobsTable then
		return
	end

	local mobTable = mobsTable[num]
	local pNpc = nil
	local npcTemplate = ""
	local npcMood = ""

	if (readData("restussManager:imperialOwned") == 1)then
		npcTemplate = mobTable[1]
		npcMood = mobTable[8]
	elseif (readData("restussManager:rebelOwned") == 1) then
		npcTemplate = mobTable[2]
		npcMood = mobTable[9]
	end

	local scaling = ""
	if difficulty > 1 and creatureTemplateExists(npcTemplate .. "_hard") then
		scaling = "_hard"
	end

	pNpc = spawnMobile("rori", npcTemplate .. scaling, 0, mobTable[3], mobTable[4], mobTable[5], mobTable[6], mobTable[7])

	--if pNpc ~= nil then
	--	if npcMood ~= "" then
	--		self:setMoodString(pNpc, npcMood)
	--	end
		--if mobTable[10] then
		--	local aiAgent = AiAgent(pNpc)
		--	aiAgent:setCreatureBit(SCANNING_FOR_CONTRABAND)
		--end
	--end

	if pNpc ~= nil then
		createObserver(CREATUREDESPAWNED, self.screenplayName, "onControlDespawn", pNpc)
		writeData(SceneObject(pNpc):getObjectID(), num)
	end
end

function restussHelper:onControlDespawn(pAiAgent)
	if pAiAgent == nil or not SceneObject(pAiAgent):isAiAgent() then
		printf("pAiAgent is nil or not an AiAgent")
		return
	end

	local npc = LuaCreatureObject(pAiAgent)

	writeData("restussManager:flipCity", readData("restussManager:flipCity") + 1)

	if (readData("restussManager:flipCity") == 200) then -- Should be 250
	    
		if (npc:isImperial() == true) then
			restuss:flipRebel()
			writeData("restussManager:flipCity", 0)
		else
			restuss:flipImperial()
			writeData("restussManager:flipCity", 0)
		end
	end

	local oid = SceneObject(pAiAgent):getObjectID()
	local mobNumber = readData(oid)
	deleteData(oid)


	createEvent(60000, "restussHelper", "respawnControlMobs", nil, tostring(mobNumber))

	return 1
end

function restussHelper:respawnControlMobs(pAiAgent, args)
	if (args == nil) then
		return
	end

	local mobNumber = tonumber(args)
	local difficulty = 0

	self:spawnCityControlMobs(mobNumber, difficulty)
end

function restussHelper:commHackableFlip(numb)
	if (numb == nil) then
		return
	end

	if (numb == 1) then
		writeData("restuss:westHackable", 0)
	elseif (numb == 2) then
		writeData("restuss:northHackable", 0)
	elseif (numb == 3) then
		writeData("restuss:eastHackable", 0)
	else
		writeData("restuss:southHackable", 0)
	end


	return 0
end

function restussHelper:commArrayCompleteRebel(playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "sensorArrayRebel")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "sensorArrayRebel")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 22)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, sensorArrayRebel.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "sensorArrayRebel")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, sensorArrayRebel.questString)
					end
			end
		end)
	return 0
end

function restussHelper:commArrayCompleteImperial(playerObject)
	local player = LuaCreatureObject(playerObject)
	local hasState = player:hasScreenPlayState(2, "sensorArrayImperial")

	ObjectManager.withCreatureObject(playerObject, function(creature)
		-- screenplaystates for login/logout
		if (creature:isGrouped()) then
			local groupSize = creature:getGroupSize()

			for i = 0, groupSize - 1, 1 do
				local pMember = creature:getGroupMember(i)
				if pMember ~= nil then
					
					local groupMember = LuaCreatureObject(pMember)
					local hasState = groupMember:hasScreenPlayState(2, "sensorArrayImperial")
					local wasInRange = false

						if (CreatureObject(playerObject):isInRangeWithObject(pMember, 22)) then
							wasInRange = true
						end

						if(hasState == true and wasInRange == true) then
							groupMember:sendSystemMessage("Mission Complete")
							groupMember:setScreenPlayState(4, sensorArrayImperial.questString)
						end
					end
				end
			else
				local hasState = player:hasScreenPlayState(2, "sensorArrayImperial")
					
					if(hasState == true) then 
						player:sendSystemMessage("Mission Complete")
						player:setScreenPlayState(4, sensorArrayImperial.questString)
					end
			end
		end)
	return 0
end
