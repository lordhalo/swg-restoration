local ObjectManager = require("managers.object.object_manager")

restussVendorConvoHandler = Object:new {
	
}

function restussVendorConvoHandler:getNextConversationScreen(conversationTemplate, conversingPlayer, selectedOption)
	local creature = LuaCreatureObject(conversingPlayer)
	local convosession = creature:getConversationSession()
	lastConversation = nil
	local conversation = LuaConversationTemplate(conversationTemplate)
	local nextConversationScreen 
	if ( conversation ~= nil ) then
		if ( convosession ~= nil ) then
			 local session = LuaConversationSession(convosession)
			 if ( session ~= nil ) then
			 	lastConversationScreen = session:getLastConversationScreen()
			 end
		end
		if ( lastConversationScreen == nil ) then

				--if (hasDeathWatch == true or hasBlackSun == true) then
					nextConversationScreen = conversation:getScreen("greet_friend")
				--else
				--	nextConversationScreen = conversation:getScreen("greet_hated")
				--end	
		else
			
			local luaLastConversationScreen = LuaConversationScreen(lastConversationScreen)
			local optionLink = luaLastConversationScreen:getOptionLink(selectedOption)		
			nextConversationScreen = conversation:getScreen(optionLink)			
		end			
	end			
	return nextConversationScreen	
end

function restussVendorConvoHandler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)	

	local screen = LuaConversationScreen(conversationScreen)	
	local screenID = screen:getScreenID()
	local player = LuaCreatureObject(conversingPlayer)
			
		if (screenID == "fp_furniture" or screenID == "fp_weapons_armor" or screenID == "fp_installations" or screenID == "fp_uniforms" or screenID == "fp_hirelings" or screenID == "fp_schematics") then
			restussVendorScreenplay:sendPurchaseSui(conversingNPC, conversingPlayer, screenID)
		end
	
	return conversationScreen
end
