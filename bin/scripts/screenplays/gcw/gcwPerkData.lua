factionRewardType = {
	armor = 1,
	weapon = 2,
	uniform = 3,
	furniture = 4,
	container = 5,
	terminal = 6,
	installation = 7,
	hireling = 8,
	deed = 9,
}

overtRebelRewardData = {
	weaponsArmorList = {
		--"armor_marine_backpack",
		"armor_rebel_snow_helmet", "armor_rebel_snow_chest", "armor_rebel_snow_legs", "armor_rebel_snow_boots", 		"armor_rebel_snow_bicep_l", "armor_rebel_snow_bicep_r", "armor_rebel_snow_bracer_l", "armor_rebel_snow_bracer_r", 			"armor_rebel_snow_boots", "armor_rebel_snow_gloves", "armor_rebel_snow_belt",

	},

	weaponsArmor = {
		--armor_marine_backpack = { index=0, type=factionRewardType.armor, display="@wearables_name:armor_marine_backpack", item="object/tangible/wearables/armor/marine/armor_marine_backpack.iff",cost=1500},
		armor_rebel_snow_helmet = { type=factionRewardType.armor, display="Rebel Snow Armor Helmet", item="object/tangible/loot/loot_schematic/rebel_snow_helmet_schematic.iff", cost=1500},
		armor_rebel_snow_chest = { type=factionRewardType.armor, display="Rebel Snow Armor Chestplate", item="object/tangible/loot/loot_schematic/rebel_snow_chest_plate_schematic.iff", cost=1500},
		armor_rebel_snow_legs = { type=factionRewardType.armor, display="Rebel Snow Armor Legs", item="object/tangible/loot/loot_schematic/rebel_snow_leggings_schematic.iff", cost=1500},
		armor_rebel_snow_boots = { type=factionRewardType.armor, display="Rebel Snow Armor Boots", item="object/tangible/loot/loot_schematic/rebel_snow_boots_schematic.iff", cost=1500},
		armor_rebel_snow_bicep_l = { type=factionRewardType.armor, display="Rebel Snow Armor Left Bicep", item="object/tangible/loot/loot_schematic/rebel_snow_bicep_l_schematic", cost=1500},
		armor_rebel_snow_bicep_r = { type=factionRewardType.armor, display="Rebel Snow Armor Right Bicep", item="object/tangible/loot/loot_schematic/rebel_snow_bicep_r_schematic", cost=1500},
		armor_rebel_snow_bracer_l = { type=factionRewardType.armor, display="Rebel Snow Armor Left Bracer", item="object/tangible/loot/loot_schematic/rebel_snow_bracer_l_schematic", cost=1500},
		armor_rebel_snow_bracer_r = { type=factionRewardType.armor, display="Rebel Snow Armor Right Bracer", item="object/tangible/loot/loot_schematic/rebel_snow_bracer_r_schematic", cost=1500},
		armor_rebel_snow_boots = { type=factionRewardType.armor, display="Rebel Snow Armor Boots", item="object/tangible/loot/loot_schematic/rebel_snow_boots_schematic", cost=1500},
		armor_rebel_snow_gloves = { type=factionRewardType.armor, display="Rebel Snow Armor Gloves", item="object/tangible/loot/loot_schematic/rebel_snow_gloves_schematic", cost=1500},
		armor_rebel_snow_belt = { type=factionRewardType.armor, display="Rebel Snow Armor Belt", item="object/tangible/loot/loot_schematic/rebel_snow_belt_schematic", cost=1500},

		
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	furnitureList = {
		"art_bas_relief", "art_alliance_propaganda", "art_trooper", "art_leia_statuette"
	},

	furniture = {
		art_bas_relief = { type=factionRewardType.furniture, display="Great Hyperspace War Bas-Relief", item="object/tangible/tcg/series1/decorative_bas_relief.iff", cost=500},
		art_alliance_propaganda = { type=factionRewardType.furniture, display="Alliance Propaganda Poster", item="object/tangible/tcg/series1/decorative_painting_alliance_propaganda.iff", cost=500},
		art_trooper = { type=factionRewardType.furniture, display="Clone Trooper Painting", item="object/tangible/tcg/series1/decorative_painting_trooper.iff", cost=500},
		art_leia_statuette = { type=factionRewardType.furniture, display="Princess Leia Statuette", item="object/tangible/tcg/series1/decorative_statuette_princess_leia.iff", cost=800},

	},

	hirelingList = {
		--"rebel_trooper",
	},

	hirelings = {
		--rebel_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:command_barc", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="rebel_trooper", cost=150},
	},

	deedListing = {
		"command_barc", "rebel_spire"
	},

	deedList = {
		command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/tangible/deed/vehicle_deed/barc_speeder_deed_reb.iff", cost=50000},	
		rebel_spire = { type=factionRewardType.deed, display="Rebel Spire", item="object/tangible/deed/player_house_deed/rebel_house_deed.iff", cost=75000}
	},
}

overtImperialRewardData = {
	weaponsArmorList = {
		--"armor_stormtrooper_helmet", 
		"armor_snowtrooper_helmet", "armor_snowtrooper_chest", "armor_snowtrooper_legs", "armor_snowtrooper_boots", 		"armor_snowtrooper_bicep_l", "armor_snowtrooper_bicep_r", "armor_snowtrooper_bracer_l", "armor_snowtrooper_bracer_r", 			"armor_snowtrooper_boots", "armor_snowtrooper_gloves", "armor_snowtrooper_belt",
	},

	weaponsArmor = {
		--armor_stormtrooper_bicep_l = { type=factionRewardType.armor, display="@wearables_name:armor_stormtrooper_bicep_l", item="object/tangible/loot/factional_schematic/imperial_stormtrooper_bicep_l_schematic.iff",cost=1400}
		armor_snowtrooper_helmet = { type=factionRewardType.armor, display="Snowtrooper Armor Helmet", item="object/tangible/loot/loot_schematic/snowtrooper_helmet_schematic.iff", cost=1500},
		armor_snowtrooper_chest = { type=factionRewardType.armor, display="Snowtrooper Armor Chestplate", item="object/tangible/loot/loot_schematic/snowtrooper_chest_plate_schematic.iff", cost=1500},
		armor_snowtrooper_legs = { type=factionRewardType.armor, display="Snowtrooper Armor Legs", item="object/tangible/loot/loot_schematic/snowtrooper_leggings_schematic.iff", cost=1500},
		armor_snowtrooper_boots = { type=factionRewardType.armor, display="Snowtrooper Armor Boots", item="object/tangible/loot/loot_schematic/snowtrooper_boots_schematic.iff", cost=1500},
		armor_snowtrooper_bicep_l = { type=factionRewardType.armor, display="Snowtrooper Armor Left Bicep", item="object/tangible/loot/loot_schematic/snowtrooper_bicep_l_schematic", cost=1500},
		armor_snowtrooper_bicep_r = { type=factionRewardType.armor, display="Snowtrooper Armor Right Bicep", item="object/tangible/loot/loot_schematic/snowtrooper_bicep_r_schematic", cost=1500},
		armor_snowtrooper_bracer_l = { type=factionRewardType.armor, display="Snowtrooper Armor Left Bracer", item="object/tangible/loot/loot_schematic/snowtrooper_bracer_l_schematic", cost=1500},
		armor_snowtrooper_bracer_r = { type=factionRewardType.armor, display="Snowtrooper Armor Right Bracer", item="object/tangible/loot/loot_schematic/snowtrooper_bracer_r_schematic", cost=1500},
		armor_snowtrooper_boots = { type=factionRewardType.armor, display="Snowtrooper Armor Boots", item="object/tangible/loot/loot_schematic/snowtrooper_boots_schematic", cost=1500},
		armor_snowtrooper_gloves = { type=factionRewardType.armor, display="Snowtrooper Armor Gloves", item="object/tangible/loot/loot_schematic/snowtrooper_gloves_schematic", cost=1500},
		armor_snowtrooper_belt = { type=factionRewardType.armor, display="Snowtrooper Armor Belt", item="object/tangible/loot/loot_schematic/snowtrooper_utility_belt_schematic", cost=1500},
	},

	uniformList = {
		--"boots_s14"
	},

	uniforms = {
		--boots_s14 = { type=factionRewardType.uniform, display="@wearables_name:boots_s14", item="object/tangible/wearables/boots/boots_s14.iff", cost=140 }
	},

	installationsList = {
		--"hq_s01_pvp_imperial"
	},

	installations = {
		--hq_s01_pvp_imperial = {type=factionRewardType.installation, display="@deed:hq_s01_pvp_imperial", item="object/tangible/deed/faction_perk/hq/hq_s01_pvp.iff", generatedObjectTemplate="object/building/faction_perk/hq/hq_s01_imp_pvp.iff", cost=14000, bonus={"hq_s01_imperial","hq_s01_imperial"} }
	},

	furnitureList = {
		"art_bas_relief", "art_imperial_propaganda", "art_trooper", "art_vader_statuette", "art_darth_vader"
	},

	furniture = {
		art_bas_relief = { type=factionRewardType.furniture, display="Great Hyperspace War Bas-Relief", item="object/tangible/tcg/series1/decorative_bas_relief.iff", cost=500},
		art_imperial_propaganda = { type=factionRewardType.furniture, display="Alliance Imperial Poster", item="object/tangible/tcg/series1/decorative_painting_imperial_propaganda.iff", cost=500},
		art_trooper = { type=factionRewardType.furniture, display="Clone Trooper Painting", item="object/tangible/tcg/series1/decorative_painting_trooper.iff", cost=500},
		art_vader_statuette = { type=factionRewardType.furniture, display="Darth Vader Statuette", item="object/tangible/tcg/series1/decorative_statuette_darth_vader.iff", cost=800},
		art_darth_vader = { type=factionRewardType.furniture, display="Darth Vader Painting", item="object/tangible/tcg/series1/decorative_painting_darth_vader.iff", cost=500},
	},

	hirelingList = {
		--"assault_trooper"
	},

	hirelings = {
		--assault_trooper = { type=factionRewardType.hireling, display="@mob/creature_names:assault_trooper", item="object/intangible/pet/pet_control.iff", controlledObjectTemplate="assault_trooper", cost=420}
	},

	deedListing = {
		"command_barc", "imperial_spire"
	},

	deedList = {
		command_barc = { type=factionRewardType.deed, display="Command Barc", item="object/tangible/deed/vehicle_deed/barc_speeder_deed_imp.iff", cost=50000},	
		imperial_spire = { type=factionRewardType.deed, display="Imperial Spire", item="object/tangible/deed/player_house_deed/emperors_house_deed.iff", cost=75000}
	},
}
