local ObjectManager = require("managers.object.object_manager")

GcwConvoHandler = Object:new {}

function GcwConvoHandler:getNextConversationScreen(pConversationTemplate, pPlayer, selectedOption, pConversingNpc)
	local pConversationSession = CreatureObject(pPlayer):getConversationSession()

	local pLastConversationScreen = nil

	if (pConversationSession ~= nil) then
		local conversationSession = LuaConversationSession(pConversationSession)
		pLastConversationScreen = conversationSession:getLastConversationScreen()
	end

	local conversationTemplate = LuaConversationTemplate(pConversationTemplate)

	if (pLastConversationScreen ~= nil) then
		local lastConversationScreen = LuaConversationScreen(pLastConversationScreen)
		local optionLink = lastConversationScreen:getOptionLink(selectedOption)

		return conversationTemplate:getScreen(optionLink)
	end

	return self:getInitialScreen(pPlayer, pConversingNpc, pConversationTemplate)
end

function GcwConvoHandler:runScreenHandlers(conversationTemplate, conversingPlayer, conversingNPC, selectedOption, conversationScreen)
	return ObjectManager.withCreatureAndPlayerObject(conversingPlayer, function(player, playerObject)
		local screen = LuaConversationScreen(conversationScreen)
		local screenID = screen:getScreenID()

		local conversationScreen = screen:cloneScreen()
		local clonedConversation = LuaConversationScreen(conversationScreen)

		if (screenID == "greet_friend") then
			if (recruiterScreenplay:getFactionFromHashCode(player:getFaction()) == "rebel") then
				clonedConversation:addOption("@conversation/faction_recruiter_rebel:s_480", "faction_purchase")
			else
				clonedConversation:addOption("@conversation/faction_recruiter_imperial:s_324", "faction_purchase")
			end
		end
			
		if (screenID == "fp_furniture" or screenID == "fp_weapons_armor" or screenID == "fp_installations" or screenID == "fp_uniforms" or screenID == "fp_hirelings" or screenID == "fp_schematics") then
			gcwScreenplay:sendPurchaseSui(conversingNPC, conversingPlayer, screenID)
		end

		return conversationScreen
	end)
end

function GcwConvoHandler:getInitialScreen(pPlayer, pNpc, conversationTemplate)
	local convoTemplate = LuaConversationTemplate(conversationTemplate)
	return ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		local faction = player:getFaction()
		local factionStanding = playerObject:getFactionStanding(gcwScreenplay:getRecruiterFaction(pNpc))
	
		if (faction == gcwScreenplay:getRecruiterEnemyFactionHashCode(pNpc)) then
			return convoTemplate:getScreen("greet_enemy")
		elseif factionStanding < -200 and playerObject:getFactionStanding(gcwScreenplay:getRecruiterEnemyFaction(pNpc)) > 0 then
			return convoTemplate:getScreen("greet_hated")
		elseif (faction == gcwScreenplay:getRecruiterFactionHashCode(pNpc)) then
			return convoTemplate:getScreen("greet_friend")
		else
			return convoTemplate:getScreen("greet_neutral_start")
		end
		return nil
	end)
end

function GcwConvoHandler:updateScreenWithPromotions(pPlayer, conversationTemplate, screen, faction)
	ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		local screenObject = LuaConversationScreen(screen)
		local rank = player:getFactionRank()

		if rank < 0 or isHighestRank(rank) == true then
			return
		end

		local requiredPoints = getRankCost(rank + 1)
		local currentPoints = playerObject:getFactionStanding(faction)

		if (currentPoints < requiredPoints + gcwScreenplay:getMinimumFactionStanding()) then
			return
		end

		self:addRankReviewOption(faction, screenObject)
	end)
end
