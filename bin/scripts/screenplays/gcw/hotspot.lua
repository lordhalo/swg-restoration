local ObjectManager = require("managers.object.object_manager")

hotspot = ScreenPlay:new {
	numberOfActs = 1,
 	 screenplayName = "hotspot",
	
}
  
registerScreenPlay("hotspot", true)
  
function hotspot:start()
    	self:spawnActiveAreas()
	self:spawnSceneObjects()

		local random = math.random(100)
			
		if random >= 50 then
			self:spawnImperials()
		elseif random < 50 then
			self:spawnRebels()
		else
			self:spawnImperials()
		end

	if (isZoneEnabled("talus")) then
		spawnMobile("talus", "gcw_vendor", 0, -2382, 39, 3788,  1, 0)
		spawnMobile("talus", "gcw_vendor_imp", 0, -2386, 39, 3788,  1, 0)
		spawnMobile("talus", "gcw_rank_recruiter", 0, -2205, 20, 2316,  1, 0)
		spawnMobile("talus", "imp_rank_trainer", 0, -2205, 20, 2318,  1, 0)

	end
	if (isZoneEnabled("rori")) then
		spawnMobile("rori", "gcw_rank_recruiter_reb", 0, 3688, 96, -6440,  1, 0)
		spawnMobile("rori", "reb_rank_trainer", 0, 3690, 96, -6440,  1, 0)
	end
end
  
function hotspot:spawnActiveAreas()

	local pActiveArea = spawnActiveArea("talus", "object/active_area.iff", -2445, 38, 3696, 384, 0)
    
	if (pActiveArea ~= nil) then
	        createObserver(ENTEREDAREA, "hotspot", "notifySpawnArea", pActiveArea)
	        createObserver(EXITEDAREA, "hotspot", "notifySpawnAreaLeave", pActiveArea)
	    end
end

function hotspot:spawnImperials()

	writeData("hotspot:imperialOwned",1)
	local pSludgepanther = spawnMobile("talus", "captain_sludgepanther", 0, -2396, 38, 3802,  1, 0)
	createObserver(OBJECTDESTRUCTION, "hotspot", "imperialDefeated", pSludgepanther)
	hotspotHelper:spawnImperialGroupOne()
	hotspotHelper:spawnImperialGroupTwo()
	hotspotHelper:spawnPatrol()
	hotspotHelper:spawnImperialFarm()

end

function hotspot:spawnRebels()

	writeData("hotspot:rebelOwned",1)
	local pCommander = spawnMobile("talus", "commander_trov", 0, -2396, 38, 3802,  1, 0)
	createObserver(OBJECTDESTRUCTION, "hotspot", "rebelDefeated", pCommander)
	hotspotHelper:spawnRebelGroupOne()
	hotspotHelper:spawnRebelGroupTwo()
	hotspotHelper:spawnRebelPatrol()
	hotspotHelper:spawnRebelFarm()

end

function hotspot:imperialDefeated()
	writeData("hotspot:imperialOwned",0)
	self:spawnRebels()
	return 0
end

function hotspot:rebelDefeated()
	writeData("hotspot:rebelOwned",0)
	self:spawnImperials()
	return 0
end

 
--checks if player enters the zone, and what to do with them.
function hotspot:notifySpawnArea(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			createEvent(1, "hotspot", "handlehotspotZone", pMovingObject)
			player:sendSystemMessage("You have entered a battlefield!")
		else
			player:sendSystemMessage("You must be Rebel or Imperial to enter the battlefield!")
			player:teleport(-1137, 13, -4529, 0)
		end

		createObserver(OBJECTDESTRUCTION, "hotspot", "killedPlayer", pMovingObject)
		return 0
	end)
end

function hotspot:killedPlayer(pTarget, pKiller)
	local player = LuaCreatureObject(pKiller)

	if (player:isAiAgent()) then
		return 0
	end

	local hasImpSkill = player:hasSkill("imp_rank_novice")
	local hasRebSkill = player:hasSkill("reb_rank_novice")
	

	if (player:isRebel() == true) then
		ObjectManager.withCreatureAndPlayerObject(pKiller, function(player, playerObject)
		playerObject:increaseFactionStanding("rebel_overt", 15)
		end)
		if(hasRebSkill == true) then
			player:awardExperience("gcw_xp", 100, true)
		end
	end

	if (player:isImperial() == true) then
		ObjectManager.withCreatureAndPlayerObject(pKiller, function(player, playerObject)
		playerObject:increaseFactionStanding("imperial_overt", 15)
		end)
		if(hasImpSkill == true) then
			player:awardExperience("gcw_xp", 100, true)
		end
	end

	return 0
end

--Handles the setting of factional status
function hotspot:handlehotspotZone(pPlayer)
	ObjectManager.withCreatureAndPlayerObject(pPlayer, function(player, playerObject)
		deleteData(player:getObjectID() .. ":changingFactionStatus")
		if (playerObject:isCovert() or playerObject:isOnLeave()) then
			playerObject:setFactionStatus(2)
		end
	end)

end

--Simply sends a system message
function hotspot:notifySpawnAreaLeave(pActiveArea, pMovingObject)
	
	if (not SceneObject(pMovingObject):isCreatureObject()) then
		return 0
	end
	
	return ObjectManager.withCreatureObject(pMovingObject, function(player)
		if (player:isAiAgent()) then
			return 0
		end
		
		if (player:isImperial() or player:isRebel()) then
			player:sendSystemMessage("You have left the battlefield!")
		end
		return 0
	end)
end

function hotspot:spawnSceneObjects()

	local pTurret = spawnSceneObject("tatooine", "object/static/destructible/destructible_cave_wall_damprock.iff", -757, 5, -4461, 0, 0, 0, 0, 0)	

	--local pDebris = spawnSceneObject("talus", "object/battlefield_marker/battlefield_marker_384m.iff", -2445, 38, 3696, 0, 0, 0, 0, 0)			
	--if (pDebris ~= nil) then
	--	createObserver(OBJECTDESTRUCTION, "hotspot", "notifyDebrisDestroyed", pDebris)
	--end

end

function hotspot:notifyDebrisDestroyed(pDebris, pPlayer)
	if (pPlayer == nil or pDebris == nil) then
		return 0
	end

	SceneObject(pDebris):destroyObjectFromWorld()

	CreatureObject(pPlayer):clearCombatState(1)
	return 0
end

