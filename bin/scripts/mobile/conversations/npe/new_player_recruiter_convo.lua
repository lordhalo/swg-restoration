newPlayerRecruiterConvo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "new_player_recruiter_quest_convo_handler",
	screens = {}

}
--Intro Frst
greet_screen = ConvoScreen:new {
	id = "greet_screen",
	leftDialog = "",
	customDialogText = "Hello Citizan! Are you interested in enlisting for the Nabooian Secuirty Forces? ", -- Welcome to Star wars
	stopConversation = "false",
	options = {
		{"Yes", "second_screen"}
	}
}
newPlayerRecruiterConvo:addScreen(greet_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Excellent, Would you like to learn the very basic skills in everything before we get started? (This option awards all Novice Basic professions, recommended for new players)", --Are you interested in learning all skills or sticking with one
	stopConversation = "false",
	options = {
		{"yes", "award_skills"},
		{"no", "third_screen"},
	}
}
newPlayerRecruiterConvo:addScreen(second_screen);

award_skills = ConvoScreen:new {
	id = "award_skills",
	leftDialog = "",
	customDialogText = "Perfect, this will allow you to try everything before making your next decision.",	-- Skills awarded
	stopConversation = "false",
	options = {
		{"whats next?", "third_screen"}
	}
}
newPlayerRecruiterConvo:addScreen(award_skills);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "History should have tought you about the Trade Federations ocupation of Naboo, But what it won't tell you is small groups of thier droid armies never got discovered, Mostly groups deactivated in the swamps seeking Gungan settlements. The trouble is some maniac is finding and activating these old droids. While not very sucessful due to thier age, they have caused some chaos in remote parts of Naboo.", -- Back story on Trade Federation uprising
	stopConversation = "false",
	options = {
		{"How can I help?", "combat_training"}, -- I want to help
		{"Im not much of a fighter.", "noncombat_training"}, -- Im not into Combat
		{"I don't have interest in any of this.", "leave_quest_screen"}, -- Im not interested in any of this
	}
}
newPlayerRecruiterConvo:addScreen(third_screen);

leave_quest_screen = ConvoScreen:new {
	id = "leave_quest_screen",
	leftDialog = "",
	customDialogText = "Understood, If you change your mind come see me again.", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(leave_quest_screen);

combat_training = ConvoScreen:new {
	id = "combat_training",
	leftDialog = "",
	customDialogText = "The Royal Guards always need more brawlers, While Secuirty could use more Marksman.",	-- Skills awarded
	stopConversation = "false",
	options = {
		{"I am interested in the Royal Guard.", "melee_screen"},
		{"Secuirty sounds interesting.", "range_screen"},
	}
}
newPlayerRecruiterConvo:addScreen(combat_training);

noncombat_training = ConvoScreen:new {
	id = "noncombat_training",
	leftDialog = "",
	customDialogText = "You don't need to be a warrior to help, Theres always a demand for Medics, Equipment, Scouts and Entertainment.",
	stopConversation = "false",
	options = {
		{"I have always wanted to help people, Could I become a Doctor?", "medic_screen"},
		{"Learning to craft has always been a dream of mine.", "crafting_screen"},
		{"Exploring would be fun.", "scout_screen"},
		{"I like Entertaining people.", "entertainer_screen"},
	}
}
newPlayerRecruiterConvo:addScreen(noncombat_training);

melee_screen = ConvoScreen:new {
	id = "melee_screen",
	leftDialog = "",
	customDialogText = "I think it would suit you, Visit Officer Miller he will sign you up.", -- 
	stopConversation = "false",
	options = {
		{"Thank you, Sir.", "melee_screen_final"},
		{"Actually id rather join Secuirty.", "range_screen"},
	}
}
newPlayerRecruiterConvo:addScreen(melee_screen);

melee_screen_final = ConvoScreen:new {
	id = "melee_screen_final",
	leftDialog = "",
	customDialogText = "Good luck, Take this waypoint to Miller.", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(melee_screen_final);

range_screen = ConvoScreen:new {
	id = "range_screen",
	leftDialog = "",
	customDialogText = "Quality Marksman are always needed, Secuirty it is. Report to Officer Joll", -- 
	stopConversation = "false",
	options = {
		{"I will do my best.", "range_screen_final"},
		{"Actually id rather join the Guard", "melee_screen"},
	}
}
newPlayerRecruiterConvo:addScreen(range_screen);

range_screen_final = ConvoScreen:new {
	id = "range_screen_final",
	leftDialog = "",
	customDialogText = "Good luck, Take this waypoint to Joll.", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(range_screen_final);

medic_screen = ConvoScreen:new {
	id = "medic_screen",
	leftDialog = "",
	customDialogText = "Medics are always high in demand, Thank you for your future service.", -- 
	stopConversation = "false",
	options = {
		{"I won't let you down.", "medic_screen_final"},
		{"Iv changed my mind.", "noncombat_training"},
	}
}
newPlayerRecruiterConvo:addScreen(medic_screen);

medic_screen_final = ConvoScreen:new {
	id = "medic_screen_final",
	leftDialog = "",
	customDialogText = "Im counting on it, Report to Doctor Hue. Hes not very friendly but hes the best. These are his Coordinates.", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(medic_screen_final);

crafting_screen = ConvoScreen:new {
	id = "crafting_screen",
	leftDialog = "",
	customDialogText = "Crafting is the backbone of this Planet, we have some of the best Tradesmith in the Galaxy.", -- 
	stopConversation = "false",
	options = {
		{"Im looking forward to learning more.", "crafting_screen_final"},
		{"Iv changed my mind.", "noncombat_training"},
	}
}
newPlayerRecruiterConvo:addScreen(crafting_screen);

crafting_screen_final = ConvoScreen:new {
	id = "crafting_screen_final",
	leftDialog = "",
	customDialogText = "Seek out Engineer Race, He's retired now and helps us with newcomers. Uploading his Coordinates", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(crafting_screen_final);

quest_complete_screen = ConvoScreen:new {
	id = "quest_complete_screen",
	leftDialog = "",
	customDialogText = "Move along friend, I have others to task. Head to the waypoint I gave you.", -- 
	stopConversation = "true",
	options = {}
}
newPlayerRecruiterConvo:addScreen(quest_complete_screen);

addConversationTemplate("newPlayerRecruiterConvo", newPlayerRecruiterConvo);
