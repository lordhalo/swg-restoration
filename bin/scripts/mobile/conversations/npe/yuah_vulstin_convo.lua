yuahVulstinConvo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "yuah_vulstin_convo_handler",
	screens = {}

}
--Intro Frst
greet_screen = ConvoScreen:new {
	id = "greet_screen",
	leftDialog = "",
	customDialogText = "Oh LOOK! a rookie 'Royal' Guard. Get out of my way, Im having a discussion with these fine people.", 
	stopConversation = "false",
	options = {
		{"Hey, Calm down sir. Im just here to talk.", "light_first_screen"},
		{"I don't like your tone..", "dark_first_screen"},
	}
}
yuahVulstinConvo:addScreen(greet_screen);

light_first_screen = ConvoScreen:new {
	id = "light_first_screen",
	leftDialog = "",
	customDialogText = "Talk about WHAT?! How you tyrants want to silence us for speaking freely!", 
	stopConversation = "false",
	options = {
		{"No sir, the Guard is simply concerned about the safety of the senator ", "light_second_screen"}
	}
}
yuahVulstinConvo:addScreen(light_first_screen);

light_second_screen = ConvoScreen:new {
	id = "light_second_screen",
	leftDialog = "",
	customDialogText = "Safety for the RICH! What about us normal people HUH? The Senator wants to allow OPEN BLASTER CARRY and you are worried about HIM! What about US! ", 
	stopConversation = "false",
	options = {
		{"I no Politician, Im here because you need to make your statements less threatening.", "light_second_screenb"},
		{"I don't like your tone..", "dark_first_screen"},
	}
}
yuahVulstinConvo:addScreen(light_second_screen);

light_second_screenb = ConvoScreen:new {
	id = "light_second_screenb",
	leftDialog = "",
	customDialogText = "Threatening HOW?!", 
	stopConversation = "false",
	options = {
		{"You can't demand the Senator be thrown to the Hutt gang with only a Blaster..You need to be less Violant.", "light_third_screen"},
	}
}
yuahVulstinConvo:addScreen(light_second_screenb);

light_third_screen = ConvoScreen:new {
	id = "light_third_screen",
	leftDialog = "",
	customDialogText = "I'll show you LESS VIOLANT!",
	stopConversation = "true",
	options = {}
}
yuahVulstinConvo:addScreen(light_third_screen);

dark_first_screen = ConvoScreen:new {
	id = "dark_first_screen",
	leftDialog = "",
	customDialogText = "Yeah!? What are you gona do about it PUNK! Pull a Blaster on ME?!", 
	stopConversation = "false",
	options = {
		{"Blasters make to much noise.. Vibro Knifes don't make a sound.", "dark_second_screen"},
		{"Hey, Calm down sir. Im just here to talk.", "light_first_screen"},
	}
}
yuahVulstinConvo:addScreen(dark_first_screen);

dark_second_screen = ConvoScreen:new {
	id = "dark_second_screen",
	leftDialog = "",
	customDialogText = "You won't dare threaten ME!!", 
	stopConversation = "true",
	options = {}
}
yuahVulstinConvo:addScreen(dark_second_screen);

no_quest_screen = ConvoScreen:new {
	id = "no_quest_screen",
	leftDialog = "",
	customDialogText = "Back up, Pal!", 
	stopConversation = "true",
	options = {}
}
yuahVulstinConvo:addScreen(no_quest_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Just go away..", 
	stopConversation = "true",
	options = {}
}
yuahVulstinConvo:addScreen(complete_screen);



addConversationTemplate("yuahVulstinConvo", yuahVulstinConvo);
