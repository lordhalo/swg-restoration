nabooCuratorConvo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "naboo_curator_convo_handler",
	screens = {}

}
--Intro Frst
greet_screen = ConvoScreen:new {
	id = "greet_screen",
	leftDialog = "",
	customDialogText = "Hello, In need of some guidance? ", -- Welcome to Star wars
	stopConversation = "false",
	options = {
		{"Yes", "second_screen"}
	}
}
nabooCuratorConvo:addScreen(greet_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "How can I help you?", --Are you interested in learning all skills or sticking with one
	stopConversation = "false",
	options = {
		{"I need a Trainer", "trainer_screen"}, --Trainers
		{"What is there to do around here?", "task_screen"} -- Missions, POIs, Quest, Dungeons.. ect
	}
}
nabooCuratorConvo:addScreen(second_screen);

trainer_screen = ConvoScreen:new {
	id = "trainer_screen",
	leftDialog = "",
	customDialogText = "What Trainer are you looking for?",
	stopConversation = "false",
	options = {
		{"Im intrested in Melee Combat", "melee_class"},
		{"Im intrested in Ranged Combat", "range_class"},
		{"Im intrested in Crafting", "crafting_class"},
		{"Im intrested in Healing", "healing_class"},
		{"Im intrested in Unique Classes", "unique_class"},
		{"Im intrested in Social Classes", "social_class"},
	}
}
nabooCuratorConvo:addScreen(trainer_screen);

melee_class = ConvoScreen:new {
	id = "melee_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Swordsman, Fencer, Pikeman, Teras Kasi Artist and Brawler.", 
	stopConversation = "false",
	options = {
		{"Swordsman.", "swordsman_training"},
		{"Fencer.", "fencer_training"}, 
		{"Pikeman.", "pikeman_training"},
		{"Teras Kasi.", "teraskasi_training"},
		{"Brawler.", "brawler_training"},
	}
}
nabooCuratorConvo:addScreen(melee_class);

range_class = ConvoScreen:new {
	id = "range_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Rifleman, Carbineer, Pistoleer, Commando and Marksman.", 
	stopConversation = "false",
	options = {
		{"Rifleman.", "rifleman_training"},
		{"Carbineer.", "carbineer_training"}, 
		{"Pistoleer.", "pistoleer_trainer"},
		{"Commando.", "commando_training"},
		{"Marksman.", "marksman_training"},
	}
}
nabooCuratorConvo:addScreen(range_class);

crafting_class = ConvoScreen:new {
	id = "crafting_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Armorsmith, Weaponsmith, Droid Engineer, Tailor, Mechanic, Chef, Bio-Engineerr, Architect and Artisan.", 
	stopConversation = "false",
	options = {
		{"Armorsmith.", "armorsmith_training"},
		{"Weaponsmith.", "weaponsmith_training"}, 
		{"Droid Engineer.", "droid_engineer_trainer"},
		{"Tailor.", "tailor_training"},
		{"Mechanic.", "mechanic_training"},
		{"Chef.", "chef_training"},
		{"Bio-Engineer.", "bio_engineer_training"},
		{"Architect.", "architect_training"},
		{"Artisan.", "artisan_training"},
	}
}
nabooCuratorConvo:addScreen(crafting_class);

healing_class = ConvoScreen:new {
	id = "healing_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Doctor, Combat Medic, and Medic.", 
	stopConversation = "false",
	options = {
		{"Doctor.", "doctor_training"},
		{"Combat Medic.", "combat_medic_training"}, 
		{"Medic.", "medic_trainer"}
	}
}
nabooCuratorConvo:addScreen(healing_class);

unique_class = ConvoScreen:new {
	id = "unique_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Creature Handler, Smuggler, Squadleader, and Bounty Hunter.", 
	stopConversation = "false",
	options = {
		{"Creature Handler.", "creature_handler_training"},
		{"Smuggler.", "smuggler_training"}, 
		{"Squad Leader.", "squad_leader_training"}, 
		{"Bounty Hunter.", "bounty_hunter_training"}
	}
}
nabooCuratorConvo:addScreen(unique_class);

social_class = ConvoScreen:new {
	id = "social_class",
	leftDialog = "",
	customDialogText = "Excellient, Please Choose from the following: Dancer, Musician, Merchant, and Entertainer.", 
	stopConversation = "false",
	options = {
		{"Dancer.", "dancer_training"},
		{"Musician.", "musician_training"}, 
		{"Merchant.", "merchant_training"}, 
		{"Image Designer.", "image_designer_training"}, 
		{"Entertainer.", "entertainer_trainer"}
	}
}
nabooCuratorConvo:addScreen(social_class);

swordsman_training = ConvoScreen:new {
	id = "swordsman_training",
	leftDialog = "",
	customDialogText = "You can find a Master Swordsman at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(swordsman_training);

fencer_training = ConvoScreen:new {
	id = "fencer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Fencer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(fencer_training);

pikeman_training = ConvoScreen:new {
	id = "pikeman_training",
	leftDialog = "",
	customDialogText = "You can find a Master Pikeman at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(pikeman_training);

teraskasi_training = ConvoScreen:new {
	id = "teraskasi_training",
	leftDialog = "",
	customDialogText = "You can find a Master Teras Kasi Artist at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(teraskasi_training);

brawler_training = ConvoScreen:new {
	id = "brawler_training",
	leftDialog = "",
	customDialogText = "You can find a Master Brawler at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(brawler_training);

rifleman_training = ConvoScreen:new {
	id = "rifleman_training",
	leftDialog = "",
	customDialogText = "You can find a Master Rifleman at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(rifleman_training);

carbineer_training = ConvoScreen:new {
	id = "carbineer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Carbineer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(carbineer_training);

pistoleer_training = ConvoScreen:new {
	id = "pistoleer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Pistoleer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(pistoleer_training);

commando_training = ConvoScreen:new {
	id = "commando_training",
	leftDialog = "",
	customDialogText = "You can find a Master Commando at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(commando_training);

marksman_training = ConvoScreen:new {
	id = "marksman_training",
	leftDialog = "",
	customDialogText = "You can find a Master Marksman at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(marksman_training);

doctor_training = ConvoScreen:new {
	id = "doctor_training",
	leftDialog = "",
	customDialogText = "You can find a Master Doctor at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(doctor_training);

combat_medic_training = ConvoScreen:new {
	id = "combat_medic_training",
	leftDialog = "",
	customDialogText = "You can find a Master Doctor at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(combat_medic_training);

medic_training = ConvoScreen:new {
	id = "medic_training",
	leftDialog = "",
	customDialogText = "You can find a Master Doctor at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(medic_training);

creature_handler_training = ConvoScreen:new {
	id = "creature_handler_training",
	leftDialog = "",
	customDialogText = "You can find a Master Creature Handler at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(creature_handler_training);

smuggler_training = ConvoScreen:new {
	id = "smuggler_training",
	leftDialog = "",
	customDialogText = "You can find a Master Smuggler at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(smuggler_training);

squad_leader_training = ConvoScreen:new {
	id = "squad_leader_training",
	leftDialog = "",
	customDialogText = "You can find a Master Squad Leader at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(squad_leader_training);

bounty_hunter_training = ConvoScreen:new {
	id = "bounty_hunter_training",
	leftDialog = "",
	customDialogText = "You can find a Master Bounty Hunter at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(bounty_hunter_training);

dancer_training = ConvoScreen:new {
	id = "dancer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Dancer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(dancer_training);

musician_training = ConvoScreen:new {
	id = "musician_training",
	leftDialog = "",
	customDialogText = "You can find a Master Musician at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(musician_training);

merchant_training = ConvoScreen:new {
	id = "merchant_training",
	leftDialog = "",
	customDialogText = "You can find a Master Merchant at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(merchant_training);

entertainer_training = ConvoScreen:new {
	id = "entertainer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Entertainer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(entertainer_training);

armorsmith_training = ConvoScreen:new {
	id = "armorsmith_training",
	leftDialog = "",
	customDialogText = "You can find a Master Armorsmith at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(armorsmith_training);

weaponsmith_training = ConvoScreen:new {
	id = "weaponsmith_training",
	leftDialog = "",
	customDialogText = "You can find a Master Weaponsmith at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(weaponsmith_training);

droid_engineer_training = ConvoScreen:new {
	id = "droid_engineer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Droid Engineer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(droid_engineer_training);

tailor_training = ConvoScreen:new {
	id = "tailor_training",
	leftDialog = "",
	customDialogText = "You can find a Master Tailor at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(tailor_training);

mechanic_training = ConvoScreen:new {
	id = "mechanic_training",
	leftDialog = "",
	customDialogText = "You can find a Master Mechanic at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(mechanic_training);

chef_training = ConvoScreen:new {
	id = "chef_training",
	leftDialog = "",
	customDialogText = "You can find a Master Chef at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(chef_training);

bio_engineer_training = ConvoScreen:new {
	id = "bio_engineer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Bio Engineer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(bio_engineer_training);

architect_training = ConvoScreen:new {
	id = "architect_training",
	leftDialog = "",
	customDialogText = "You can find a Master Architect at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(architect_training);

artisan_training = ConvoScreen:new {
	id = "artisan_training",
	leftDialog = "",
	customDialogText = "You can find a Master Artisan at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(artisan_training);

image_designer_training = ConvoScreen:new {
	id = "image_designer_training",
	leftDialog = "",
	customDialogText = "You can find a Master Image Designer at this location, Ill upload the waypoint to your Datapad.", 
	stopConversation = "true",
	options = {}
}
nabooCuratorConvo:addScreen(image_designer_training);

task_screen = ConvoScreen:new {
	id = "task_screen",
	leftDialog = "",
	customDialogText = "This Planet features many unique areas.", 
	stopConversation = "false",
	options = {
		{"Tell me about Points of Interest.", "poi_screen"},
		{"I just want want to gain Experience.", "grind_screen"}, 
		{"I need a Challenge", "dungeon_screen"}, 
		{"I want to be a Jedi!", "jedi_screen"}
	}
}
nabooCuratorConvo:addScreen(task_screen);

poi_screen = ConvoScreen:new {
	id = "poi_screen",
	leftDialog = "",
	--customDialogText = "Of course, Points of Interest (POI's) can be found on your Map, How ever this planet contains 5 Hidden POI's. ", 
	customDialogText = "Please return later, My Databank is incomplete.", 
	stopConversation = "true",
	options = {
		{"Tell me about Points of Interest.", "poi_screen"},
		{"I just want want to gain Experience.", "grind_screen"}, 
		{"I need a Challenge.", "dungeon_screen"}, 
		{"I want to be a Jedi!", "jedi_screen"}
	}
}
nabooCuratorConvo:addScreen(poi_screen);

poi_screen = ConvoScreen:new {
	id = "poi_screen",
	leftDialog = "",
	--customDialogText = "Mission Terminals offer a combination of Experience and Money and can be done alone, Hunting NPCs in a Group offers increased Experience and are the most effective way to earn Experience. POI's offer a combination of Experience and Loot, some can be done alone and others require a Group. ", 
	customDialogText = "Please return later, My Databank is incomplete.", 
	stopConversation = "true",
	options = {

	}
}
nabooCuratorConvo:addScreen(poi_screen);

dungeon_screen = ConvoScreen:new {
	id = "dungeon_screen",
	leftDialog = "",
	--customDialogText = "Mission Terminals offer a combination of Experience and Money and can be done alone, Hunting NPCs in a Group offers increased Experience and are the most effective way to earn Experience. POI's offer a combination of Experience and Loot, some can be done alone and others require a Group. ", 
	customDialogText = "Please return later, My Databank is incomplete.", 
	stopConversation = "true",
	options = {

	}
}
nabooCuratorConvo:addScreen(dungeon_screen);

jedi_screen = ConvoScreen:new {
	id = "jedi_screen",
	leftDialog = "",
	customDialogText = "Hokey religions and ancient weapons are no match for a good blaster at your side, kid.", 
	stopConversation = "true",
	options = {

	}
}
nabooCuratorConvo:addScreen(jedi_screen);



addConversationTemplate("nabooCuratorConvo", nabooCuratorConvo);
