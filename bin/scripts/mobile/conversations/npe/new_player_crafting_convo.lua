newPlayerCraftingConvo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "new_player_crafting_convo_handler",
	screens = {}

}
--Intro Frst
greet_screen = ConvoScreen:new {
	id = "greet_screen",
	leftDialog = "",
	customDialogText = "Are you the Rookiee Larrick sent over? Of course you are. Lets get started, I have fine Mandalorian Wine waiting for me. ", -- Welcome to Star wars
	stopConversation = "false",
	options = {
		{"Yes sir", "second_screen"}
	}
}
newPlayerCraftingConvo:addScreen(greet_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Above all you have to learn, you must always rememeber how important Resources are. I suggest your Survey daily for new Resource spawns, You never know if something more valueable has been uncovered. Take these Survey tools, I make extras just for Beings like you.", --Are you interested in learning all skills or sticking with one
	stopConversation = "false",
	options = {
		{"Thank you", "first_ark_complete"},
	}
}
newPlayerCraftingConvo:addScreen(second_screen);

first_ark_complete = ConvoScreen:new {
	id = "first_ark_complete",
	leftDialog = "",
	customDialogText = "Try these out if you like, Then we will continue.",
	stopConversation = "true",
	options = {}
}
newPlayerCraftingConvo:addScreen(first_ark_complete);

no_quest_screen = ConvoScreen:new {
	id = "no_quest_screen",
	leftDialog = "",
	customDialogText = "Ohhh, What do you want? Why don't you go beat up some Frogs or Dragons.",
	stopConversation = "true",
	options = {}
}
newPlayerCraftingConvo:addScreen(no_quest_screen);

phase_two_screen = ConvoScreen:new {
	id = "phase_two_screen",
	leftDialog = "",
	customDialogText = "Alright, Lets get you ready to build some items. You will need to select what you want to Focus on later on, Take this Generic Crafting tool of mine until you figure out what path you want to follow.",
	stopConversation = "false",
	options = {
		{"What are my options?", "phase_two_screen_s2"},
	}
}
newPlayerCraftingConvo:addScreen(phase_two_screen);

phase_two_screen_s2 = ConvoScreen:new {
	id = "phase_two_screen_s2",
	leftDialog = "",
	customDialogText = "Oh goodness, The Galaxy has many demands. I think its best you explore the Galaxy and find what fits you.",
	stopConversation = "false",
	options = {
		{"Okay", "phase_two_screen_final"},
	}
}
newPlayerCraftingConvo:addScreen(phase_two_screen_s2);

phase_two_screen_final = ConvoScreen:new {
	id = "phase_two_screen_final",
	leftDialog = "",
	customDialogText = "Here are some Schematics I learned at an early age, Browse them and see what you like.",
	stopConversation = "true",
	options = {}
}
newPlayerCraftingConvo:addScreen(phase_two_screen_final);

phase_three_screen = ConvoScreen:new {
	id = "phase_three_screen",
	leftDialog = "",
	customDialogText = "Okay Young one I have writers block",
	stopConversation = "true",
	options = {
		{"What are my options?", "phase_two_screen_s2"},
	}
}
newPlayerCraftingConvo:addScreen(phase_three_screen);

addConversationTemplate("newPlayerCraftingConvo", newPlayerCraftingConvo);
