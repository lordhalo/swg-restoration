newPlayerMeleeConvo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "new_player_melee_quest_convo_handler",
	screens = {}

}
--Intro Frst
greet_screen = ConvoScreen:new {
	id = "greet_screen",
	leftDialog = "",
	customDialogText = "What do you want? Are you one of the new recruits?", 
	stopConversation = "false",
	options = {
		{"I am.", "second_screen"}
	}
}
newPlayerMeleeConvo:addScreen(greet_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Good, I am Miller. Brawling expert and Trainer of new recruits for the Royal Guard. Are you ready to get started?", 
	stopConversation = "false",
	options = {
		{"I am ready.", "third_screen"}
	}
}
newPlayerMeleeConvo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "Great, First you need to decide what Combat Path most interest you, Your options are Teras Kasi Artist, Swordsmanship, Fencing, Polearms. What would you like to know more about?",
	stopConversation = "false",
	options = {
		{"Teras Kasi.", "tk_screen"},
		{"Swordsman.", "swordsman_screen"},
		{"Fencer.", "fencer_screen"},
		{"Pikeman.", "pikeman_screen"},
	}
}
newPlayerMeleeConvo:addScreen(third_screen);

options_screen = ConvoScreen:new {
	id = "options_screen",
	leftDialog = "",
	customDialogText = "Your options are Teras Kasi, Swordsman, Fencing or Polearms", -- 
	stopConversation = "false",
	options = {
		{"Teras Kasi.", "tk_screen"},
		{"Swordsman.", "swordsman_screen"},
		{"Fencer.", "fencer_screen"},
		{"Pikeman.", "pikeman_screen"}
	}
}
newPlayerMeleeConvo:addScreen(options_screen);

tk_screen = ConvoScreen:new {
	id = "tk_screen",
	leftDialog = "",
	customDialogText = "Teras Kasi are a Master of Defense and have skills for every combat situation.", 
	stopConversation = "false",
	options = {
		{"That sounds like me.", "tk2_screen"},
		{"Im not sure, What are my options again?", "options_screen"}
	}
}
newPlayerMeleeConvo:addScreen(tk_screen);

tk2_screen = ConvoScreen:new {
	id = "tk2_screen",
	leftDialog = "",
	customDialogText = "Excellent, Lets get started. Im going to train you Center of Being, Powerful for Tanking.", 
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "tk_train_one_screen"}
	}
}
newPlayerMeleeConvo:addScreen(tk2_screen);

unarmed_train_one_screen = ConvoScreen:new {
	id = "unarmed_train_one_screen",
	leftDialog = "",
	customDialogText = "Go and Practice on those Trainer Droids if you want, Or just Speak to me again and we can continue.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(unarmed_train_one_screen);

swordsman_screen = ConvoScreen:new {
	id = "swordsman_screen",
	leftDialog = "",
	customDialogText = "Swordsmen offer Raw, Brutal single target Damage.", 
	stopConversation = "false",
	options = {
		{"That sounds like me.", "swordsman2_screen"},
		{"Im not sure, What are my options again?", "options_screen"}
	}
}
newPlayerMeleeConvo:addScreen(swordsman_screen);

swordsman2_screen = ConvoScreen:new {
	id = "swordsman2_screen",
	leftDialog = "",
	customDialogText = "Excellent, Im going to teach you Melee Strike, An Improved Damage Ability", 
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "swordsman_train_one_screen"}
	}
}
newPlayerMeleeConvo:addScreen(swordsman2_screen);

swordsman_train_one_screen = ConvoScreen:new {
	id = "swordsman_train_one_screen",
	leftDialog = "",
	customDialogText = "Go and Practice on those Trainer Droids if you want, Or just Speak to me again and we can continue.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(swordsman_train_one_screen);

fencer_screen = ConvoScreen:new {
	id = "fencer_screen",
	leftDialog = "",
	customDialogText = "The Art of Fencing does well to keep targets riddled with debuffs while also providing quick single target Damage.", 
	stopConversation = "false",
	options = {
		{"That sounds like me.", "fencer2_screen"},
		{"Im not sure, What are my options again?", "options_screen"}
	}
}
newPlayerMeleeConvo:addScreen(fencer_screen);

fencer2_screen = ConvoScreen:new {
	id = "fencer2_screen",
	leftDialog = "",
	customDialogText = "Excellent, Im going to teach you Lunge, An extended Range Ability", 
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "fencer_train_one_screen"}
	}
}
newPlayerMeleeConvo:addScreen(fencer2_screen);

fencer_train_one_screen = ConvoScreen:new {
	id = "fencer_train_one_screen",
	leftDialog = "",
	customDialogText = "Go and Practice on those Trainer Droids if you want, Or just Speak to me again and we can continue.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(fencer_train_one_screen);


pikeman_screen = ConvoScreen:new {
	id = "pikeman_screen",
	leftDialog = "",
	customDialogText = "Those who wield a Polearm are masters of area damage and crowd control.", 
	stopConversation = "false",
	options = {
		{"That sounds like me.", "pikeman2_screen"},
		{"Im not sure, What are my options again?", "options_screen"}
	}
}
newPlayerMeleeConvo:addScreen(pikeman_screen);

pikeman2_screen = ConvoScreen:new {
	id = "pikeman2_screen",
	leftDialog = "",
	customDialogText = "Excellent, Lets get started. Im going to train you Center of Being, Powerful for Tanking.", 
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "pikeman_train_one_screen"}
	}
}
newPlayerMeleeConvo:addScreen(pikeman2_screen);

pikeman_train_one_screen = ConvoScreen:new {
	id = "pikeman_train_one_screen",
	leftDialog = "",
	customDialogText = "Go and Practice on those Trainer Droids if you want, Or just Speak to me again and we can continue.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(pikeman_train_one_screen);

box_one_complete_screen = ConvoScreen:new {
	id = "box_one_complete_screen",
	leftDialog = "",
	customDialogText = "Alright, Now that you have learned a bit from me, Im going to give you a Guard Recruit Badge and send you on a task that needs immidiate attention. While you arn't Combat Ready, This Job is simply to negotiate with local. Think you can handle that?", 
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "negotiation_quest_one"}
	}
}
newPlayerMeleeConvo:addScreen(box_one_complete_screen);

negotiation_quest_one = ConvoScreen:new {
	id = "negotiation_quest_one",
	leftDialog = "",
	customDialogText = "Good! A local named Yuah Vulstin has been making threats to one of our senators in the Cantina, Normally it gets passed off as Freedom of speach, However hes starting to get a following at the local Cantina, I want you to talk him down. Local Guard radioed hes in the Cantina now and stirring trouble. Handle it!", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(negotiation_quest_one);

negotiation_quest_complete = ConvoScreen:new {
	id = "negotiation_quest_complete",
	leftDialog = "",
	customDialogText = "Well, Look who it is.. I heard about what happened in the Cantina, I didn't expect things to get Violant but you handled yourself and the situation. ", 
	stopConversation = "false",
	options = {
		{"Thank you, Sir.", "negotiation_quest_reward"}
	}
}
newPlayerMeleeConvo:addScreen(negotiation_quest_complete);

negotiation_quest_reward = ConvoScreen:new {
	id = "negotiation_quest_reward",
	leftDialog = "",
	customDialogText = "As a reward im going to accelerate your training, Then with your new skills I have another Job for you. ", 
	stopConversation = "false",
	options = {
		{"Roger", "negotiation_quest_reward_complete"}
	}
}
newPlayerMeleeConvo:addScreen(negotiation_quest_reward);

negotiation_quest_reward_complete = ConvoScreen:new {
	id = "negotiation_quest_reward_complete",
	leftDialog = "",
	customDialogText = "Alright you are well on your way to being a Royal Guard member, As a result its time for a debriefing.", 
	stopConversation = "false",
	options = {
		{"Debriefing?", "scout_droid_slayer_quest_intro"}
	}
}
newPlayerMeleeConvo:addScreen(negotiation_quest_reward_complete);

scout_droid_slayer_quest_intro = ConvoScreen:new {
	id = "scout_droid_slayer_quest_intro",
	leftDialog = "",
	customDialogText = "You may have heard about the old Trade Federation Droids someone is activating. What you don't know is the growing fear of the culprit being from the Nabooian Goverment.", 
	stopConversation = "false",
	options = {
		{"Who and Why?", "scout_droid_slayer_quest_intro_s2"}
	}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_intro);

scout_droid_slayer_quest_intro_s2 = ConvoScreen:new {
	id = "scout_droid_slayer_quest_intro_s2",
	leftDialog = "",
	customDialogText = "We don't know who, However they must have large amounts of Credits, and Resources in order to track down these Droids and get them online. We believe its a very small team, Lead by someone in the Govorment trying to force a change. With the Sith running around the Galaxy again, I fear the worst.", 
	stopConversation = "false",
	options = {
		{"What change? Sith? What is going on?", "scout_droid_slayer_quest_intro_s3"}
	}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_intro_s2);

scout_droid_slayer_quest_intro_s3 = ConvoScreen:new {
	id = "scout_droid_slayer_quest_intro_s3",
	leftDialog = "",
	customDialogText = "We have done well to prevent total Control by the Imperial Elites dispite the Star Destroyers over head watching us. Mostly we continue to run things peacefully. However we fear someone wants complete Imperial Ocupation! I hear the Dark Lord Vader has history on Naboo and as a result our Goverment was able to remain intact. The Sith our an Ancient ideology that seek to rule the galaxy, Years back the Sith had an unfortunate return to our Galaxy that distabilized the entire Republic.", 
	stopConversation = "false",
	options = {
		{"This is a lot to process.. What should I do?", "scout_droid_slayer_quest_intro_s4"}
	}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_intro_s3);

scout_droid_slayer_quest_intro_s4 = ConvoScreen:new {
	id = "scout_droid_slayer_quest_intro_s4",
	leftDialog = "",
	customDialogText = "What you signed up for! Keep your Head strong and help us put an end to this Droid Threat once and for all.", 
	stopConversation = "false",
	options = {
		{"Yes Sir! where do I start?", "scout_droid_slayer_quest_intro_s5"}
	}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_intro_s4);

scout_droid_slayer_quest_intro_s5 = ConvoScreen:new {
	id = "scout_droid_slayer_quest_intro_s5",
	leftDialog = "",
	customDialogText = "Our Scouts reported some highly disfunctional Droids outside of the City, While disfunctional like most of these groups are.. We still don't need them reaching the City and causing a Panic. Ill upload the coordinates the Scouts sent over the network for you. Go there and eliminate them.", 
	stopConversation = "false",
	options = {
		{"Roger", "scout_droid_slayer_quest_start"}
	}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_intro_s5);

scout_quest_complete = ConvoScreen:new {
	id = "scout_quest_complete",
	leftDialog = "",
	customDialogText = "Welcome Back, Scouts reported the Droids have been eliminated. Good work.", 
	stopConversation = "false",
	options = {
		{"I Found a Data Spike on one of the Droids, Sir.", "scout_quest_complete_s2"}
	}
	
}
newPlayerMeleeConvo:addScreen(scout_quest_complete);

scout_quest_complete_s2 = ConvoScreen:new {
	id = "scout_quest_complete_s2",
	leftDialog = "",
	customDialogText = "Data Spike? Let me see that... Hrmm. I know this Marking, This was sold by an Ithorian named Danlun. He's a Scavanger who makes a living off Junk left behind. Iv seen him leave this Mark on a Power Converter I bought from him.", 
	stopConversation = "false",
	options = {
		{"You don't think hes responsible for the Droids do you?", "scout_quest_complete_s3"}
	}
}
newPlayerMeleeConvo:addScreen(scout_quest_complete_s2);

scout_quest_complete_s3 = ConvoScreen:new {
	id = "scout_quest_complete_s3",
	leftDialog = "",
	customDialogText = "Oh, No. But we need to ask who bought this Spike from him! Im sending you to investigate.", 
	stopConversation = "false",
	options = {
		{"Where can I find Danlun?", "scout_quest_reward"}
	}
}
newPlayerMeleeConvo:addScreen(scout_quest_complete_s3);

scout_quest_reward = ConvoScreen:new {
	id = "scout_quest_reward",
	leftDialog = "",
	customDialogText = "We are giving you a Standard Guard Patrol Vehicle to help get you around town during your investigation. Danlun is located at these Coordinates. Report back to me with your findings.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(scout_quest_reward);

spike_vendor_wait_screen = ConvoScreen:new {
	id = "spike_vendor_wait_screen",
	leftDialog = "",
	customDialogText = "Return to me after you question Danlun.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(spike_vendor_wait_screen);

scout_droid_slayer_quest_start = ConvoScreen:new {
	id = "scout_droid_slayer_quest_start",
	leftDialog = "",
	customDialogText = "Proceed with caution, But I feel you won't have a problem with this group.", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(scout_droid_slayer_quest_start);

scout_quest_wait_screen = ConvoScreen:new {
	id = "scout_quest_wait_screen",
	leftDialog = "",
	customDialogText = "Eliminate those Droids!", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(scout_quest_wait_screen);

no_quest_screen = ConvoScreen:new {
	id = "no_quest_screen",
	leftDialog = "",
	customDialogText = "You arn't on my list", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(no_quest_screen);

negotiation_wait_screen = ConvoScreen:new {
	id = "negotiation_wait_screen",
	leftDialog = "",
	customDialogText = "Go deal with the situation in the Cantina!", 
	stopConversation = "true",
	options = {}
}
newPlayerMeleeConvo:addScreen(negotiation_wait_screen);



addConversationTemplate("newPlayerMeleeConvo", newPlayerMeleeConvo);
