ig88DailyEpic = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "ig88_daily_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Greeting: Internal Contract for you!",
	stopConversation = "false",
	options = {
		{"Internal?", "second_screen"},
	}
}
ig88DailyEpic:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Target Location: Yavin4. Warning: use Caution",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Yes: Target Experimented on Clan Memeber. Clan Memeber did not survive. Clan request: Kill Target. ",
	stopConversation = "false",
	options = {
		{"Roger", "accept_screen"},
	}
}
ig88DailyEpic:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await youre return.",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Target: Eliminated?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
ig88DailyEpic:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Contract: Impressive! Respect of all Clan increased. Reward: Valuable Liquid mined by Clan. ",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Currently I have no Contracts.",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
ig88DailyEpic:addScreen(restart_quest);

addConversationTemplate("ig88DailyEpic", ig88DailyEpic);

