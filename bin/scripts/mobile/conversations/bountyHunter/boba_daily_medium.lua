bobaDailyMedium = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "boba_daily_medium_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hey, Rookiee. I got a job and I don't want to deal with it, You want to take over?",
	stopConversation = "false",
	options = {
		{"It would be an honor, Sir.", "second_screen"},
	}
}
bobaDailyMedium:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "I'll wait your return from Dantooine.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Don't thank me yet, Theres a reason I don't want to deal with this.",
	stopConversation = "false",
	options = {
		{"Whats the job?", "third_screen"},
	}
}
bobaDailyMedium:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "On Dantooine theres a Imperial research base thats out of control, The Warren they call it. One of the Droids in there holds valuable infomation, A Contract has been placed to destroy the Droid to prevent its knowledge base from leaking into the Public.",
	stopConversation = "false",
	options = {
		{"I'll Handle it.", "accept_screen"},
	}
}
bobaDailyMedium:addScreen(third_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Good, The pay will make it worth your time. Good luck",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "I got reports the Target was eliminated, Nice work.",
	stopConversation = "false",
	options = {
		{"Thank you", "complete_screen_final"},
	}
}
bobaDailyMedium:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Here is compensation, You are gaining quiet the reputation around here.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "I don't have any other gigs right now.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bobaDailyMedium:addScreen(restart_quest);

addConversationTemplate("bobaDailyMedium", bobaDailyMedium);

