bosskDailyMedium = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "bossk_daily_medium_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hey! Look. I hate dealing with Droids, and some guy drops a contract on me for a Mad Droid Scientist.",
	stopConversation = "false",
	options = {
		{"You want me to handle it?", "second_screen"},
	}
}
bosskDailyMedium:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Droids on Talus when you are ready.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Yeah, Why not. Some Droid got his wires crossed and is running some Science freak show on Talus.",
	stopConversation = "false",
	options = {
		{"I'll take care of it.", "accept_screen"},
	}
}
bosskDailyMedium:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Perfect.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Is that Droid Scrap now Friend?",
	stopConversation = "false",
	options = {
		{"Should be heading to Raxus Prime now", "complete_screen_final"},
	}
}
bosskDailyMedium:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "HaHa! Great. Look im gona throw something extra in for you, Keep up the solid work.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "I don't have any contracts right now friend.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bosskDailyMedium:addScreen(restart_quest);

addConversationTemplate("bosskDailyMedium", bosskDailyMedium);

