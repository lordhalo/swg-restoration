lomDailyEasy = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "lom_daily_easy_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "*Beep* Greetings fellow Hunter. *Click* I 4-L0M have a mission for you.",
	stopConversation = "false",
	options = {
		{"Whats the job?", "second_screen"},
	}
}
lomDailyEasy:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Destroy Protocol Droid! *Click*",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "*Extensive Static fading out* A Protocol Droid with extensive information on my Logical *Click* Programing has surfaced. *Beep* I want you to *Click* Destroy it. 4LOM Would be to transparent *Click* ",
	stopConversation = "false",
	options = {
		{"Consider it done", "accept_screen"},
	}
}
lomDailyEasy:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "*Static*",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "*Click, Click, Click* Complete?",
	stopConversation = "false",
	options = {
		{"Target has been eliminated", "complete_screen_final"},
	}
}
lomDailyEasy:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "*Beep* *Beep*",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "*Static*",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
lomDailyEasy:addScreen(restart_quest);

addConversationTemplate("lomDailyEasy", lomDailyEasy);

