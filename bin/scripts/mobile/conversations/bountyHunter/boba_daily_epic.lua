bobaDailyEpic = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "boba_daily_epic_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "This is the Easy quest",
	stopConversation = "false",
	options = {
		{"I used to bullseye wromprats, so I think so", "second_screen"},
	}
}
bobaDailyEpic:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Kill those rats already",
	stopConversation = "false",
	options = {
		{"Iv lost track of things and I need to start over.", "restart_quest"},
	}
}
bobaDailyEpic:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Well ok then, clean up 10 rats for us.",
	stopConversation = "false",
	options = {
		{"sure", "accept_screen"},
	}
}
bobaDailyEpic:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Thanks",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "You done?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
bobaDailyEpic:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Great, now we can get back to work. Here are some Minerals we extracted yesterday.",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Come see us tomorrow when we have more minerals.",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bobaDailyEpic:addScreen(restart_quest);

addConversationTemplate("bobaDailyEpic", bobaDailyEpic);

