commonDailyTwo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "common_daily_two_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Are you for hire?",
	stopConversation = "false",
	options = {
		{"Whats the gig?", "second_screen"},
	}
}
commonDailyTwo:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Whats up?",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Its a DC-15 Rifle left over from the clone wars.",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I visited Theed recently and this Nabooian Officer confiscated my rifle!",
	stopConversation = "false",
	options = {
		{"rifle?", "third_screen"},
	}
}
commonDailyTwo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "Yes my RIFLE! It was a gift from my friends on Kashyyyk. This Jerk tells me its illegal because of the scope! Had he not had his friends support i'd have used it on him!",
	stopConversation = "false",
	options = {
		{"You want the rifle back?", "fourth_screen"},
	}
}
commonDailyTwo:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "Yeah, This power abusing slime hangs around the Theed Palace. The locals arnt very found of this guy, so you would be doing everyone a favor.",
	stopConversation = "false",
	options = {
		{"Consider him terminated.", "accept_screen"},
	}
}
commonDailyTwo:addScreen(fourth_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "And don't forget my Rifle!",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "My Rifle! I was worried I wouldn't see this again.",
	stopConversation = "false",
	options = {
		{"Here ya go", "complete_screen_final"},
	}
}
commonDailyTwo:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Thank you, I hope these are credits are enough for your skill.",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Im gona be polish my rifle for the rest of the day.",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
commonDailyTwo:addScreen(restart_quest);

addConversationTemplate("commonDailyTwo", commonDailyTwo);

