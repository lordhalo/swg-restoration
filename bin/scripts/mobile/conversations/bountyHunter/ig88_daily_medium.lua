ig88DailyMedium = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "ig88_daily_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Greeting: You need Contract?",
	stopConversation = "false",
	options = {
		{"Yes, Im looking for work.", "second_screen"},
	}
}
ig88DailyMedium:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Target Location: Corellia",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Good: Simple Contract. Target Stole from Client. Client request: Kill Target. ",
	stopConversation = "false",
	options = {
		{"Roger", "accept_screen"},
	}
}
ig88DailyMedium:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await youre return.",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Target: Eliminated?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
ig88DailyMedium:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Contract paid in full to you. Bonus issued, Good day!",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Currently I have no Contracts.",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
ig88DailyMedium:addScreen(restart_quest);

addConversationTemplate("ig88DailyMedium", ig88DailyMedium);

