--Black Sun Daily Quest (Common #3B)
--Given by random contractor NPC in the Black Sun Bunker
blackSunMHADailyConvo = ConvoTemplate:new {
	 initialScreen = "greet_friend",
	 templateType = "Lua",
	 luaClassHandler = "black_sun_mha_daily_convo_handler",
	 screens = {}
}

greet_friend = ConvoScreen:new { --this is the quest intro screen
	 id = "greet_friend",
	leftDialog = "",
	customDialogText = "Death Watch forces have established a foothold on Lok.  I have it on good authority they have deployed units to a spike camp not far from here.  It's only a matter of time before they challenge our supremacy.",
		stopConversation = "false",
	options = {
		 {"What do you want me to do?","screen2"},
		 {"I don't care.","decline_screen"}
	 }
 }
blackSunMHADailyConvo:addScreen(greet_friend);

screen2 = ConvoScreen:new { --this screen the player can accept or decline the quest
	 id = "screen2",
	leftDialog = "",
	customDialogText = "We can't let them gain any more traction in the Outer Rim.  Infiltrate their camp and eliminate their presence on this planet.",
	stopConversation = "false",
	options = {
		 {"Yes sir.","accept_screen"},
		 {"Not now, I'm busy.","decline_screen"}
	 }
 }
blackSunMHADailyConvo:addScreen(screen2);

accept_screen = ConvoScreen:new { --this initiates the quest
	 id = "accept_screen",
	leftDialog = "",
	customDialogText = "They must know that Black Sun rules the underworld, and we will not be usurped.",
	stopConversation = "true",
	options = {
	 }
 }
blackSunMHADailyConvo:addScreen(accept_screen);

decline_screen = ConvoScreen:new { --this cancels the quest
	 id = "decline_screen",
	leftDialog = "",
	customDialogText = "I'll find someone else then.",
	stopConversation = "true",
	options = {
	 }
 }
blackSunMHADailyConvo:addScreen(decline_screen);

--Objective: Kill 10 Death Watch Ghosts.  Reward is Mandalorian Hardening Agent.

complete_screen = ConvoScreen:new { --this is the turn-in
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Is it done?",
	stopConversation = "false",
	options = {
		{"I've weakened our competition.", "complete_screen_final"},
	}
}
blackSunMHADailyConvo:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new { --this completes the quest. 
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Good.  It will take them some time to recover from your attack, but stay vigilant.  We may need your assistance again soon.  In the meantime, here is something to help you prepare.",
	stopConversation = "true",
	options = {}
}
blackSunMHADailyConvo:addScreen(complete_screen_final);

waiting_screen = ConvoScreen:new { --the response from the giver after the quest is complete
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Hury up, We need to deal with this Death Watch infestation",
	stopConversation = "true",
	options = {}
}
blackSunMHADailyConvo:addScreen(waiting_screen);

quest_done = ConvoScreen:new { --the response from the giver after the quest is complete
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Come back later, I'm busy.",
	stopConversation = "true",
	options = {}
}
blackSunMHADailyConvo:addScreen(quest_done);

go_away = ConvoScreen:new { --the response from the giver after the quest is complete
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost",
	stopConversation = "true",
	options = {}
}
blackSunMHADailyConvo:addScreen(go_away);

done_screen = ConvoScreen:new { --the response from the giver after the quest is complete
	id = "done_screen",
	leftDialog = "",
	customDialogText = "Come back tomorrow.",
	stopConversation = "true",
	options = {}
}
blackSunMHADailyConvo:addScreen(done_screen);

addConversationTemplate("blackSunMHADailyConvo", blackSunMHADailyConvo);
