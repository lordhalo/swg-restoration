bosskDailyHard = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "bossk_daily_hard_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Look who it is! Iv got a Test for you.",
	stopConversation = "false",
	options = {
		{"a Test?", "second_screen"},
	}
}
bosskDailyHard:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Target is on Dathomir. Use Caution!",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Yeah, a Test. Its a job, but its not an easy one. Someone slaps this Contract on the table for me but I want you to have it.",
	stopConversation = "false",
	options = {
		{"why do you want me to have it?", "third_screen"},
	}
}
bosskDailyHard:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "The Target is Force Sensitive, It will push your limits. This is no easy task but should you complete the task, You will be well respected around here.",
	stopConversation = "false",
	options = {
		{"Consider it done", "accept_screen"},
	}
}
bosskDailyHard:addScreen(third_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Careful, Those Force users are crafty.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Youre alive! I was worried I sent you to your death!",
	stopConversation = "false",
	options = {
		{"Hardly, Target eliminated.", "complete_screen_final"},
	}
}
bosskDailyHard:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Impressive! Im sure everyone will be talking about your latest kill.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Whats up Hotshot? I don't have anymore work right now. Go relax! you earned it.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bosskDailyHard:addScreen(restart_quest);

addConversationTemplate("bosskDailyHard", bosskDailyHard);

