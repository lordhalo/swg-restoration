commonDailyOne = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "common_daily_one_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "You..YOU! *Hic*... You uh, you looking for..Wo...Work?",
	stopConversation = "false",
	options = {
		{"Whats your deal old man?", "second_screen"},
	}
}
commonDailyOne:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Go slaughter that Warlord already",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "OH! More then...just a hi..hir..hired gun?",
	stopConversation = "false",
	options = {
		{"sure", "third_screen"},
	}
}
commonDailyOne:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "So im on Dantooine..Tatoo..TATOOINE, Trading with the small folk.. Jawa? Yeah. Out from nowhere comes these Sandpeople!",
	stopConversation = "false",
	options = {
		{"Tuskens?", "fourth_screen"},
	}
}
commonDailyOne:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "Yep! thats the one..So I make a go for my Speeder and one...One spikes me in the Knee with his Stick! I havn't been able to...toooo...Walk! Good since! I made good Credits off them small ones, Ill pay you to return there and gut the bastard who did this to me.",
	stopConversation = "false",
	options = {
		{"Whats the location?", "accept_screen"},
	}
}
commonDailyOne:addScreen(fourth_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "JAWA! Thats it, JAWA! traders outpost on Nabo...Dantoo...TATOOINE! *Hic*",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Oi Friend! Did you kill him?!",
	stopConversation = "false",
	options = {
		{"Its done", "complete_screen_final"},
	}
}
commonDailyOne:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Oh thank you! I hope he suffered like I have..Next round is on me lad.",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Im tapped out for the Day mate.",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
commonDailyOne:addScreen(restart_quest);

addConversationTemplate("commonDailyOne", commonDailyOne);

