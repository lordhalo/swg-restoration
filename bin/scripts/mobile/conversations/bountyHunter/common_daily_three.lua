commonDailyThree = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "common_daily_three_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hey, Can you help me out with something?",
	stopConversation = "false",
	options = {
		{"Help? Not really my thing.", "second_screen"},
	}
}
commonDailyThree:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Whats up?",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Well? What are you waiting for?",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Oh wait! wait! I know I don't have much for credits, but I do have something you may find valuable.",
	stopConversation = "false",
	options = {
		{"unlikely.", "third_screen"},
	}
}
commonDailyThree:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "Look Im a Moister Farmer from Tatooine, I procuted some resources I trade with the clan here.. I could share some with you if you can help me.",
	stopConversation = "false",
	options = {
		{"Mysterious resources.. not interested.", "fourth_screen"},
	}
}
commonDailyThree:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "Oh no! no! Trust me, the clan supplies me with speeders, cargo containers and what ever else I need in trade for one of these, Im sure you will find it valueable.",
	stopConversation = "false",
	options = {
		{"What ARE the resources?", "fifth_screen"},
	}
}
commonDailyThree:addScreen(fourth_screen);

fifth_screen = ConvoScreen:new {
	id = "fifth_screen",
	leftDialog = "",
	customDialogText = "If you can eliminate my competition on Tatooine, Ill give you one. But I insist you do the job before I show you the prize, I don't want any trouble over this.. Just to return to work.",
	stopConversation = "false",
	options = {
		{"fine! Where am I going, you better not waste my time old man.", "accept_screen"},
	}
}
commonDailyThree:addScreen(fifth_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Tatooine, Outside Mos Eisley.. Trust me, you will be pleased.",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Hello Friend! shall I be heading back home pleased?",
	stopConversation = "false",
	options = {
		{"Target has been eliminated", "complete_screen_final"},
	}
}
commonDailyThree:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Oh! Thank you! Please..Take this and enjoy.",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Im all out of those liquids today Friend.",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
commonDailyThree:addScreen(restart_quest);

addConversationTemplate("commonDailyThree", commonDailyThree);

