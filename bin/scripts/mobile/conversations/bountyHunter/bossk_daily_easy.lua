bosskDailyEasy = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "bossk_daily_easy_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "You, Do you want a Job? A Client wants me to eliminate some Engineer but they pay is a little light for me.",
	stopConversation = "false",
	options = {
		{"Im interested", "second_screen"},
	}
}
bosskDailyEasy:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Head to Lok and wipe out that Engineer",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Good..Good, The target is an Engineer working for the Canyon Corsair's on Lok. My Client just wants him dead, Says the Engineer stole something from him or another.. I don't care. Just take care of it and ill Pay you a little extra.",
	stopConversation = "false",
	options = {
		{"Roger", "accept_screen"},
	}
}
bosskDailyEasy:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Happy Hunting",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Look who's back, How did it go?",
	stopConversation = "false",
	options = {
		{"Target eliminated", "complete_screen_final"},
	}
}
bosskDailyEasy:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Good, You have potental, I don't even see a scratch on your armor! Ill keep my eye out for you next time a petty job comes up.",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "I don't have anything else going on right now.",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bosskDailyEasy:addScreen(restart_quest);

addConversationTemplate("bosskDailyEasy", bosskDailyEasy);

