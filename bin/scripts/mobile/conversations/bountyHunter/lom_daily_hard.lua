lomDailyHard = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "lom_daily_hard_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "*Beep* *Beep* Target: *Beep* *Beep* Tusken Raider *Beep* *Beep* Location: Tatooine",
	stopConversation = "false",
	options = {
		{"I understand", "second_screen"},
	}
}
lomDailyHard:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "*Beep* Location: Tatooine *Static* ??????",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "*Static* Use Cation *Static*",
	stopConversation = "false",
	options = {
		{"Roger", "accept_screen"},
	}
}
lomDailyHard:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "*Static*",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "*Beep* Complete?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
lomDailyHard:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "*Boop* Reward",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "*Static* ?????? *Static*",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
lomDailyHard:addScreen(restart_quest);

addConversationTemplate("lomDailyHard", lomDailyHard);

