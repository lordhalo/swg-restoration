lomDailyMedium = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "lom_daily_medium_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "*Click* *Click* *Click* 4-L0M request you  to destroy *Beep* Jedi Crystal Smuggler.",
	stopConversation = "false",
	options = {
		{"Jedi Crystal Smuggler?", "second_screen"},
	}
}
lomDailyMedium:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "*Click Dantooine, *Click* Force Crystal Cave",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "*Boop* Yes, Location *Click* Dantooine, *Click* Force Crystal Cave.",
	stopConversation = "false",
	options = {
		{"Roger", "accept_screen"},
	}
}
lomDailyMedium:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "*Static*",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Complete? *Beep*",
	stopConversation = "false",
	options = {
		{"Target eliminated", "complete_screen_final"},
	}
}
lomDailyMedium:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Good *Click* Work",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "???????",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
lomDailyMedium:addScreen(restart_quest);

addConversationTemplate("lomDailyMedium", lomDailyMedium);

