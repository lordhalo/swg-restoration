bobaDailyEasy = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "boba_daily_easy_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Are you looking for work?",
	stopConversation = "false",
	options = {
		{"Whats the gig?", "second_screen"},
	}
}
bobaDailyEasy:addScreen(greet_friend);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Mos Eisley..What else do you need?",
	stopConversation = "true",
	options = {}
}
bobaDailyEasy:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I need you to keep this short and quiet. My cought my wife Rikln seeing other men behind my back",
	stopConversation = "false",
	options = {
		{"Who you want taken care of, her or them?", "third_screen"},
	}
}
bobaDailyEasy:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "Her, You will be compensated well.. just get it done and speak no more of it. She hangs around the Mos Eisley Cantina",
	stopConversation = "false",
	options = {
		{"Roger that.", "accept_screen"},
	}
}
bobaDailyEasy:addScreen(third_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Right...",
	stopConversation = "true",
	options = {}
}
bobaDailyEasy:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Is it done?",
	stopConversation = "false",
	options = {
		{"Yes", "complete_screen_final"},
	}
}
bobaDailyEasy:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Here's the payment.. Leave me.",
	stopConversation = "true",
	options = {}
}
bobaDailyEasy:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Its really over?",
	stopConversation = "true",
	options = {}
}
bobaDailyEasy:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bobaDailyEasy:addScreen(restart_quest);

addConversationTemplate("bobaDailyEasy", bobaDailyEasy);

