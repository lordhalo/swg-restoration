bobaDailyHard = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "boba_daily_hard_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Are you ready for a real challenge?",
	stopConversation = "false",
	options = {
		{"Always, Sir", "second_screen"},
	}
}
bobaDailyHard:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Target is on Dantooine.",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I have a Contract on a Jedi Knight. Intel says hes meeting companion on Dantooine, You need to get there before his companion shows up and eliminate him.",
	stopConversation = "false",
	options = {
		{"I can handle it", "accept_screen"},
	}
}
bobaDailyHard:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Don't let him intimidate you, One good shot from your blaster and he falls like any other man.",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Word on the street is you ran into some trouble",
	stopConversation = "false",
	options = {
		{"I had to deal with two Jedi.", "complete_screen_final"},
	}
}
bobaDailyHard:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "And yet here you are, Good work, Now go relax..Enjoy a Mandalorian Wine on me",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Cheers",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(tomorrow_screen);

restart_quest = ConvoScreen:new {
	id = "restart_quest",
	leftDialog = "",
	customDialogText = "Failure, Such a biological trait.",
	stopConversation = "true",
	options = {}
}
bobaDailyHard:addScreen(restart_quest);

addConversationTemplate("bobaDailyHard", bobaDailyHard);

