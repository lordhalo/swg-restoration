marauderVendorConvTemplate = ConvoTemplate:new {
	initialScreen = "",
	templateType = "Lua",
	luaClassHandler = "marauderVendor_handler",
	screens = {}
}

greet_hated = ConvoScreen:new {
	id = "greet_hated",
	leftDialog = "@conversation/faction_recruiter_imperial:s_306", -- I do not talk to terrorists and insurgents.
	stopConversation = "true",
	options = {
	}
}

marauderVendorConvTemplate:addScreen(greet_hated);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "What can I help you with?",
	stopConversation = "false",
	options = {
		--{ "@conversation/faction_recruiter_imperial:s_328", "fp_installations" }, -- I need a base of operations.
		--{ "@conversation/faction_recruiter_imperial:s_332", "fp_uniforms"}, -- I need a new uniform.
		{ "@conversation/faction_recruiter_imperial:s_336", "fp_weapons_armor" }, -- I need personal protection and armament.
		--{ "@conversation/faction_recruiter_imperial:s_340", "fp_schematics" }, -- I want to see what schematics you have.
		--{ "@conversation/faction_recruiter_imperial:s_344", "fp_furniture"}, -- Furniture. I am improving my quality of my life.
		{ "@conversation/faction_recruiter_imperial:s_348", "fp_hirelings" }, -- I would like to requisition additional troops.
		--{ "I am in needs of some Special Materials.", "mando_materials" }, -- I would like to requisition additional troops.
	}
}

marauderVendorConvTemplate:addScreen(greet_friend);

faction_purchase = ConvoScreen:new {
	id = "faction_purchase",
	leftDialog = "@conversation/faction_recruiter_imperial:s_326",  -- What are you looking for?
	stopConversation = "false",
	options = {
		{ "@conversation/faction_recruiter_imperial:s_328", "fp_installations" }, -- I need a base of operations.
		{ "@conversation/faction_recruiter_imperial:s_332", "fp_uniforms"}, -- I need a new uniform.
		{ "@conversation/faction_recruiter_imperial:s_336", "fp_weapons_armor" }, -- I need personal protection and armament.
		--{ "@conversation/faction_recruiter_imperial:s_340", "fp_schematics" }, -- I want to see what schematics you have.
		{ "@conversation/faction_recruiter_imperial:s_344", "fp_furniture"}, -- Furniture. I am improving my quality of my life.
		{ "@conversation/faction_recruiter_imperial:s_348", "fp_hirelings" }, -- I would like to requisition additional troops.
	},
}

marauderVendorConvTemplate:addScreen(faction_purchase);

mando_materials = ConvoScreen:new {
	id = "mando_materials",
	leftDialog = "", -- I will show you our selection. Wear the uniform proudly.
	customDialogText = "Ok, Binary Liquid or Protective Liquid coating? They cost 30k Faction Each.",
	stopConversation = "false",
	options = {	
		{ "Binary Liquid.", "binary_liquid" },
		{ "Protective Liquid Coating.", "protective_liquid" },
		{ "I don't have that kind of Faction.", "broke_fool" },
	},
}
marauderVendorConvTemplate:addScreen(mando_materials);

broke_fool = ConvoScreen:new {
	id = "broke_fool",
	customDialogText = "Then stop wasting my time!",
	leftDialog = "", -- I will show you our selection. Wear the uniform proudly.
	stopConversation = "true",
	options = {	},
}
marauderVendorConvTemplate:addScreen(broke_fool);

binary_liquid = ConvoScreen:new {
	id = "binary_liquid",
	customDialogText = "I will process the request.",
	leftDialog = "", -- I will show you our selection. Wear the uniform proudly.
	stopConversation = "true",
	options = {	},
}
marauderVendorConvTemplate:addScreen(binary_liquid);

protective_liquid = ConvoScreen:new {
	id = "protective_liquid",
	customDialogText = "I will process the request.",
	leftDialog = "", -- I will show you our selection. Wear the uniform proudly.
	stopConversation = "true",
	options = {	},
}
marauderVendorConvTemplate:addScreen(protective_liquid);

fp_uniforms = ConvoScreen:new {
	id = "fp_uniforms",
	leftDialog = "@conversation/faction_recruiter_imperial:s_334", -- I will show you our selection. Wear the uniform proudly.
	stopConversation = "true",
	options = {	},
}
marauderVendorConvTemplate:addScreen(fp_uniforms);

fp_furniture = ConvoScreen:new {
	id = "fp_furniture",
	leftDialog = "@conversation/faction_recruiter_imperial:s_346", -- Submit your purchase order and I will get right on it.
	stopConversation = "true",
	options = {	},
}
marauderVendorConvTemplate:addScreen(fp_furniture);

fp_weapons_armor = ConvoScreen:new {
	id = "fp_weapons_armor",
	leftDialog = "@conversation/faction_recruiter_imperial:s_338", -- Always a good choice. I have a new shipment to look through.
	stopConversation = "true",
	options = {},
}
marauderVendorConvTemplate:addScreen(fp_weapons_armor);

fp_installations = ConvoScreen:new {
	id = "fp_installations",
	leftDialog = "@conversation/faction_recruiter_imperial:s_330", -- Very well. I will show you what is available.
	stopConversation = "true",
	options = {},
}
marauderVendorConvTemplate:addScreen(fp_installations);

fp_schematics = ConvoScreen:new {
	id = "fp_schematics",
	leftDialog = "@conversation/faction_recruiter_imperial:s_342", -- Review this list. Quite a few are available.
	stopConversation = "true",
	options = {},
}
marauderVendorConvTemplate:addScreen(fp_schematics);

fp_hirelings = ConvoScreen:new {
	id = "fp_hirelings",
	leftDialog = "@conversation/faction_recruiter_imperial:s_350", -- I believe that can be arranged. We have some spare units in maneuvers. Let me show you what you can requisition.
	stopConversation = "true",
	options = {},

}
marauderVendorConvTemplate:addScreen(fp_hirelings);

addConversationTemplate("marauderVendorConvTemplate", marauderVendorConvTemplate);
