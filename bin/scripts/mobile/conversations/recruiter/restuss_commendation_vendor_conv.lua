restussVendorConvoTemplate = ConvoTemplate:new {
	initialScreen = "",
	templateType = "Lua",
	luaClassHandler = "restussVendorConvoHandler",
	screens = {}
}

greet_hated = ConvoScreen:new {
	id = "greet_hated",
	leftDialog = "@conversation/faction_recruiter_imperial:s_306", -- I do not talk to terrorists and insurgents.
	stopConversation = "true",
	options = {
	}
}

restussVendorConvoTemplate:addScreen(greet_hated);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "What are you looking for?",
	stopConversation = "false",
	options = {
		--{ "@conversation/faction_recruiter_imperial:s_328", "fp_installations" }, -- I need a base of operations.
		{ "@conversation/faction_recruiter_imperial:s_332", "fp_uniforms"}, -- I need a new uniform.
		{ "@conversation/faction_recruiter_imperial:s_336", "fp_weapons_armor" }, -- I need personal protection and armament.
		{ "@conversation/faction_recruiter_imperial:s_340", "fp_schematics" }, -- I want to see what schematics you have.
		{ "@conversation/faction_recruiter_imperial:s_344", "fp_furniture"}, -- Furniture. I am improving my quality of my life.
		--{ "@conversation/faction_recruiter_imperial:s_348", "fp_hirelings" }, -- I would like to requisition additional troops.
	}
}

restussVendorConvoTemplate:addScreen(greet_friend);

faction_purchase = ConvoScreen:new {
	id = "faction_purchase",
	leftDialog = "@conversation/faction_recruiter_imperial:s_326",  -- What are you looking for?
	stopConversation = "false",
	options = {
		{ "@conversation/faction_recruiter_imperial:s_328", "fp_installations" }, -- I need a base of operations.
		{ "@conversation/faction_recruiter_imperial:s_332", "fp_uniforms"}, -- I need a new uniform.
		{ "@conversation/faction_recruiter_imperial:s_336", "fp_weapons_armor" }, -- I need personal protection and armament.
		--{ "@conversation/faction_recruiter_imperial:s_340", "fp_schematics" }, -- I want to see what schematics you have.
		{ "@conversation/faction_recruiter_imperial:s_344", "fp_furniture"}, -- Furniture. I am improving my quality of my life.
		{ "@conversation/faction_recruiter_imperial:s_348", "fp_hirelings" }, -- I would like to requisition additional troops.
	},
}

restussVendorConvoTemplate:addScreen(faction_purchase);

fp_uniforms = ConvoScreen:new {
	id = "fp_uniforms",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},
}
restussVendorConvoTemplate:addScreen(fp_uniforms);

fp_furniture = ConvoScreen:new {
	id = "fp_furniture",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},
}
restussVendorConvoTemplate:addScreen(fp_furniture);

fp_weapons_armor = ConvoScreen:new {
	id = "fp_weapons_armor",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},
}
restussVendorConvoTemplate:addScreen(fp_weapons_armor);

fp_installations = ConvoScreen:new {
	id = "fp_installations",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},
}
restussVendorConvoTemplate:addScreen(fp_installations);

fp_schematics = ConvoScreen:new {
	id = "fp_schematics",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},
}
restussVendorConvoTemplate:addScreen(fp_schematics);

fp_hirelings = ConvoScreen:new {
	id = "fp_hirelings",
	leftDialog = "",
	customDialogText = "!",
	stopConversation = "true",
	options = {	},

}
restussVendorConvoTemplate:addScreen(fp_hirelings);

addConversationTemplate("restussVendorConvoTemplate", restussVendorConvoTemplate);
