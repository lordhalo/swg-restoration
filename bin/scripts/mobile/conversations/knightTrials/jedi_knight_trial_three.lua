jedi_knight_trials_three = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_knight_trials_three_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hello Padawan, For this Trial you will be doing some exploration.",
	stopConversation = "false",
	options = {
		{"Exploration?", "second_screen"},
	}
}
jedi_knight_trials_three:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "The Stintaril have been causing problems around these parts, I want you to hunt 8 Stintaril, this pack has been rumored to hide in an ancient Volcano.",
	stopConversation = "false",
	options = {
		{"Volcano?", "accept_screen"},
	}
}
jedi_knight_trials_three:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Yes, Explore the Volcanos of Yavin until you find the pack of Stintaril. Good luck.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_three:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Welcome back, did you find the Stintaril?",
	stopConversation = "false",
	options = {
		{"Yes I did.", "complete_screen_final"},
	}
}
jedi_knight_trials_three:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Good work Padawan, find Yooln for your next Trial.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_three:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "I will be waiting here.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_three:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Yooln waits for you.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_three:addScreen(quest_done);

addConversationTemplate("jedi_knight_trials_three", jedi_knight_trials_three);

