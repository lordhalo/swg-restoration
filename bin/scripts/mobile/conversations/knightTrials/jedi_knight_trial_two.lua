jedi_knight_trials_two = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_knight_trials_two_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "I have been expecting you, for your next trial we will be testing your Strength.",
	stopConversation = "false",
	options = {
		{"My Strength?", "second_screen"},
	}
}
jedi_knight_trials_two:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Yes a Jedi must prove his ability to overcome the most challenging of obsticles, rather by Lightsaber or the Force you must proove yourself strong enought to overcome fear of a beast much larger then you.",
	stopConversation = "false",
	options = {
		{"Beast?", "accept_screen"},
	}
}
jedi_knight_trials_two:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Yes, Head to Dathomir.. Slay 8 Ancient Bull Rancors roaming the great Tar Pits. Feel, Don't think.. The Force will be with you.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_two:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Welcome home, I trust you completed your trial?",
	stopConversation = "false",
	options = {
		{"Only by your guidance was I able to overcome those Beast", "complete_screen_final"},
	}
}
jedi_knight_trials_two:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "I am pleased of your success, Seek out Aullni for your next Trial.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_two:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_two:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Aullni awaits you.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_two:addScreen(quest_done);

addConversationTemplate("jedi_knight_trials_two", jedi_knight_trials_two);

