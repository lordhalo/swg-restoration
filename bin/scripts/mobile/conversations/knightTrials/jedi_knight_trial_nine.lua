jedi_knight_trials_nine = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_knight_trials_nine_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "I have heard of your Conquest on Talus, If you are up for returning to the Battlefront.. More work needs to be done.",
	stopConversation = "false",
	options = {
		{"How can I help?", "second_screen"},
	}
}
jedi_knight_trials_nine:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "The Imperials have sent in reinforcements to Talus, trying to rebuild from thier losses. Many Generals are now overseeing operations, If you deal with them the Alliance will be one step closer to controlling Talus. Try and draw these Generals out and eliminate them.",
	stopConversation = "false",
	options = {
		{"It will be done, Master.", "accept_screen"},
	}
}
jedi_knight_trials_nine:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Let no Hate or Anger enpower you young Padawan, Do only want must be done.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_nine:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "You have done well, The Empire has suffered greatly from the loss of so many Generals. ",
	stopConversation = "false",
	options = {
		{"I hope this puts us one step closer to Peace in the Galaxy.", "complete_screen_final"},
	}
}
jedi_knight_trials_nine:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "A",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_nine:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Return to me when your Mission is complete.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_nine:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Greetings",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_nine:addScreen(quest_done);

not_ready = ConvoScreen:new {
	id = "not_ready",
	leftDialog = "",
	customDialogText = "You need to Complete Padawan Trial Eight.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_nine:addScreen(not_ready);

addConversationTemplate("jedi_knight_trials_nine", jedi_knight_trials_nine);

