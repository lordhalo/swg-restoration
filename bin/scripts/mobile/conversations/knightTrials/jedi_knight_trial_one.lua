jedi_knight_trials_one = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_knight_trials_one_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hello Padawan, You have proven yourself strong with the Force, The Council has instructed me to give you a new Task.",
	stopConversation = "false",
	options = {
		{"I am ready, what is needed of me?", "second_screen"},
	}
}
jedi_knight_trials_one:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "As Defenders of peace we ensure safety of the innocent, In the Outter Rim an inveasion of the city of Mos Espa has begun. You are tasked with liberating the city from its Tusken onslaught.",
	stopConversation = "false",
	options = {
		{"It shall be done.", "accept_screen"},
	}
}
jedi_knight_trials_one:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Once you complete this task, you will begin your Jedi Knight Trials. Good luck",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_one:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "We recieved communication saying you eliminated the Tuskens and restored balance to the City, The Natives thank you.",
	stopConversation = "false",
	options = {
		{"What is my next task?", "complete_screen_final"},
	}
}
jedi_knight_trials_one:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Seek out Euunl, he will guide you further.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_one:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Please hurry, this task can not wait.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_one:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "You have already completed my mission.",
	stopConversation = "true",
	options = {}
}
jedi_knight_trials_one:addScreen(quest_done);

addConversationTemplate("jedi_knight_trials_one", jedi_knight_trials_one);

