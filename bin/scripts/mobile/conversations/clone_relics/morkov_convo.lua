morkovConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "morkov_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = " How may I assist?",
	stopConversation = "false",
	options = {
		{"I'm here on behalf of my master, Jabba the Hutt.", "screen_two"},
	}
}
morkovConvo:addScreen(greetings);

screen_two = ConvoScreen:new {
	id = "screen_two",
	leftDialog = "",
	customDialogText = "Another one? Can you please tell your 'master' to stick to the same person doing his business? It's getting a little tiresome doing background checks on all of you.",
	stopConversation = "false",
	options = {
		{"A lot of people in my masters service have a short life span.", "screen_three"},
		{"Background check? No one told me about that...", "s_3"},
	}
}
morkovConvo:addScreen(screen_two);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Well they should have. Now what's your name?",
	stopConversation = "false",
	options = {
		{"My name? Well it's...  Xfawqo Qreop", "s_31"},
	}
}
morkovConvo:addScreen(s_3);

s_31 = ConvoScreen:new {
	id = "s_31",
	leftDialog = "",
	customDialogText = "How the heck do you spell that? You know what, never mind. What does the hutt want this time?",
	stopConversation = "false",
	options = {
		{"What do you do here?", "s_4"},
		{"I've heard you're the man to go to for if I'm looking for a weapon.", "s_32"},
		{"My master needs 5 new E-11's.", "screen_four"},
		{"My master require one standard TIE and 5 crates of E-11's.", "s_1"},
		{"My master wants a walker model AT-AT.", "s_2"},
	}
}
morkovConvo:addScreen(s_31);

s_32 = ConvoScreen:new {
	id = "s_32",
	leftDialog = "",
	customDialogText = "I'm afraid you heard wrong. There's a gun store up the street though I believe. Just be careful with those things, they are dangerous.",
	stopConversation = "True",
	options = {}
}
morkovConvo:addScreen(s_32);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "I live here, how about you?",
	stopConversation = "false",
	options = {
		{"Umm... I'm just passing through, what about the droids?", "s_41"},
	}
}
morkovConvo:addScreen(s_4);

s_41 = ConvoScreen:new {
	id = "s_41",
	leftDialog = "",
	customDialogText = "What about them?",
	stopConversation = "false",
	options = {
		{"Are they yours?", "s_42"},
	}
}
morkovConvo:addScreen(s_41);

s_42 = ConvoScreen:new {
	id = "s_42",
	leftDialog = "",
	customDialogText = "Yes. Now I'm a busy man I'm afraid. Do you need to know anything else?",
	stopConversation = "false",
	options = {
		{"No I suppose not...", "s_43"},
	}
}
morkovConvo:addScreen(s_42);

s_43 = ConvoScreen:new {
	id = "s_43",
	leftDialog = "",
	customDialogText = "Very well then. Good day to you.",
	stopConversation = "True",
	options = {}
}
morkovConvo:addScreen(s_43);

screen_three = ConvoScreen:new {
	id = "screen_three",
	leftDialog = "",
	customDialogText = "(Morkov chuckles) Yeah I suppose that would be true. Never mind on the background check, what does the hutt want this time?.",
	stopConversation = "false",
	options = {
		{"I've heard you're the man to go to for if I'm looking for a weapon.", "s_32"},
		{"My master needs 5 new E-11's.", "screen_four"},
		{"My master require one standard TIE and 5 crates of E-11's.", "s_1"},
		{"My master wants a walker model AT-AT.", "s_2"},
	}
}
morkovConvo:addScreen(screen_three);

s_1 = ConvoScreen:new {
	id = "s_1",
	leftDialog = "",
	customDialogText = "Hmm, the TIE will take a little work, going to need a week for that one. The crates I can have ready for you when you arrive at the usual location.",
	stopConversation = "false",
	options = {
		{"Usual location...?", "screen_six"},
	}
}
morkovConvo:addScreen(s_1);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Has he gone mad?! I can't get that...",
	stopConversation = "false",
	options = {
		{"My master was under the impression you can get anything.", "s_21"},
	}
}
morkovConvo:addScreen(s_2);

s_21 = ConvoScreen:new {
	id = "s_21",
	leftDialog = "",
	customDialogText = "Well he better think again. An AT-ST shouldn't be too hard but an AT-AT would just not be possible.",
	stopConversation = "false",
	options = {
		{"My master will be most disappointed.", "s_22"},
	}
}
morkovConvo:addScreen(s_21);

s_22 = ConvoScreen:new {
	id = "s_22",
	leftDialog = "",
	customDialogText = "I don't care if he starts to cry, it's just not possible.",
	stopConversation = "false",
	options = {
		{"Very well, he also requires 5 crates of E-11's.", "s_23"},
	}
}
morkovConvo:addScreen(s_22);

s_23 = ConvoScreen:new {
	id = "s_23",
	leftDialog = "",
	customDialogText = "Now that's not a problem. I'll have that ready for you when you arrive at usual location.",
	stopConversation = "false",
	options = {
		{"Usual location...?", "screen_six"},
	}
}
morkovConvo:addScreen(s_23);

screen_four = ConvoScreen:new {
	id = "screen_four",
	leftDialog = "",
	customDialogText = "What?! He can send his thugs out to beat up 5 troopers and get that. I don't deal in quantities like that, is this a joke?",
	stopConversation = "false",
	options = {
		{"My apologies, I of course meant 5 crates", "screen_five"},
	}
}
morkovConvo:addScreen(screen_four);

screen_five = ConvoScreen:new {
	id = "screen_five",
	leftDialog = "",
	customDialogText = "Oh, why didn't you say so? Yeah that's not a problem, I'll have that ready for you when you arrive at the usual location.",
	stopConversation = "false",
	options = {
		{"Usual location...?", "screen_six"},
	}
}
morkovConvo:addScreen(screen_five);

screen_six = ConvoScreen:new {
	id = "screen_six",
	leftDialog = "",
	customDialogText = "Did they not tell you anything?",
	stopConversation = "false",
	options = {
		{"My information was very sparse...", "screen_seven"},
	}
}
morkovConvo:addScreen(screen_six);

screen_seven = ConvoScreen:new {
	id = "screen_seven",
	leftDialog = "",
	customDialogText = "I'd say...very well, there's an empty building located across from the hotel in Dee'Ja peak. My courier will meet you upstairs in that building.",
	stopConversation = "true",
	options = {}
}
morkovConvo:addScreen(screen_seven);

accept_screen_2 = ConvoScreen:new {
	id = "accept_screen_2",
	leftDialog = "",
	customDialogText = "You got it. Now if you'll excuse me.",
	stopConversation = "true",
	options = {}
}
morkovConvo:addScreen(accept_screen_2);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Hurry up. My couriers standing by with the shipment",
	stopConversation = "false",
	options = {
		{"Dee'Ja Peak, opposite hotel, upstairs, right?", "accept_screen_2"},	
	}
}
morkovConvo:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Yes?",
	stopConversation = "true",
	options = {}
}
morkovConvo:addScreen(quest_done);

kill_quest = ConvoScreen:new {
	id = "kill_quest",
	leftDialog = "",
	customDialogText = "I don't know who you are or what your deal is but you will pay with your life for what you did!",
	stopConversation = "true",
	options = {}
}
morkovConvo:addScreen(kill_quest);

addConversationTemplate("morkovConvo", morkovConvo);

