waldaConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "walda_convo_handler",
	screens = {}
}
--Intro First
walda_first_screen = ConvoScreen:new {
	id = "walda_first_screen",
	leftDialog = "",
	customDialogText = " Yeah?",
	stopConversation = "false",
	options = {
		{"Jawl sent me here, told me you could help me out.", "s_2"},
	}
}
waldaConvo:addScreen(walda_first_screen);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Oh so you're the messanger",
	stopConversation = "false",
	options = {
		{"Sure am.", "s_3"},
	}
}
waldaConvo:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Yeah I can help you out. Anything to make that bucket head look bad.",
	stopConversation = "false",
	options = {
		{"Make who look bad?", "s_4"},
	}
}
waldaConvo:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "G-5P0 of course! That devious little droid stole my job! I was well respected through out the galaxy for never erring in translating even the most peculiar of languages. How was I supposed to talk to a Ithorian that didn't know basic? I don't have two mouths!",
	stopConversation = "false",
	options = {
		{"Okay okay, I understand.", "s_5"},
	}
}
waldaConvo:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "Sorry about that. Alright, I've compiled a disk with the basics for understanding and making yourself understood in Huttese. It should be enough for what you want to accomplish. It will be yours for a mere 20,000 credits!",
	stopConversation = "false",
	options = {
		{"20,000 credits?! Are you insane?!", "walda_final_screen"},
	}
}
waldaConvo:addScreen(s_5);

walda_final_screen = ConvoScreen:new {
	id = "walda_final_screen",
	leftDialog = "",
	customDialogText = "< Walda laughs > That was a good one, you should have seen the look on your face! Here you go. < Walda hands you the disk > Be careful what you say to Jabba, he's got quite the temper.",
	stopConversation = "true",
	options = {}
}
waldaConvo:addScreen(walda_final_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "I hope that disk worked out for you. That metal head probably had circuits fry when he wasn't needed. < Walda laughs >",
	stopConversation = "true",
	options = {}
}
waldaConvo:addScreen(complete_screen);


greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "You're not one of those metal heads are you? Well, what do you want?",
	stopConversation = "true",
	options = {}
}
waldaConvo:addScreen(greet_friend);

addConversationTemplate("waldaConvo", waldaConvo);

