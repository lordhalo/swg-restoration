ep3RaevConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "clone_relics_raev_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = "What is it soldier?",
	stopConversation = "false",
	options = {
		{"This datapad contain important information regarding a traitor.", "s_2"},
	}
}
ep3RaevConvo:addScreen(greetings);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Like we didn't have enough of those...",
	stopConversation = "false",
	options = {
		{"I have come across logs that show of a traitor.", "s_3"},
	}
}
ep3RaevConvo:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Really? Very well, I'll send it to intel for analyzing. Wait a moment and I'll have their reply for you.",
	stopConversation = "true",
	options = {}
}
ep3RaevConvo:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "What the heck did you have me send them? You need to go downstairs immediately, there's a holo call waiting for you.",
	stopConversation = "false",
	options = {
		{"From who?", "s_5"},
	}
}
ep3RaevConvo:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "Just go down there soldier!",
	stopConversation = "true",
	options = {}
}
ep3RaevConvo:addScreen(s_5);

s_6 = ConvoScreen:new {
	id = "s_6",
	leftDialog = "",
	customDialogText = "Good. Any more information you recover from him I'd be interested in as well, it will help us track down what he's sold to us. Good luck soldier and thank you, many men and women owe you their life already.",
	stopConversation = "True",
	options = {}
}
ep3RaevConvo:addScreen(s_6);

holo_wait = ConvoScreen:new {
	id = "holo_wait",
	leftDialog = "",
	customDialogText = "You need to go downstairs immediately, there's a holo call waiting for you.",
	stopConversation = "true",
	options = {}
}
ep3RaevConvo:addScreen(holo_wait);

quest_wait_screen = ConvoScreen:new {
	id = "quest_wait_screen",
	leftDialog = "",
	customDialogText = "You have your orders soldier, you better carry them out quickly.",
	stopConversation = "True",
	options = {}
}
ep3RaevConvo:addScreen(quest_wait_screen);

quest_turnin_screen = ConvoScreen:new {
	id = "quest_turnin_screen",
	leftDialog = "",
	customDialogText = "Welcome back soldier. I trust that you followed orders?",
	stopConversation = "false",
	options = {
		{"Yes Sir, Morkov will not bother anyone again.", "quest_complete_screen"},
	}
}
ep3RaevConvo:addScreen(quest_turnin_screen);

quest_complete_screen = ConvoScreen:new {
	id = "quest_complete_screen",
	leftDialog = "",
	customDialogText = "Excellent, and the logs from his operation?",
	stopConversation = "false",
	options = {
		{"Should all be on here Sir. < hand Raev the datapad >", "quest_complete_screen_final"},
	}
}
ep3RaevConvo:addScreen(quest_complete_screen);

quest_complete_screen_final = ConvoScreen:new {
	id = "quest_complete_screen_final",
	leftDialog = "",
	customDialogText = "Very good soldier, I will send this to intel right away. The Emperor will be most pleased.",
	stopConversation = "true",
	options = {}
}
ep3RaevConvo:addScreen(quest_complete_screen_final);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "I don't have anything else I need help with here at the moment soldier. You should return to your duties.",
	stopConversation = "True",
	options = {}
}
ep3RaevConvo:addScreen(quest_done);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "I'm sorry citizen but I have some pressing matters to deal with. Fill out form OAQ-49P and leave it in the drop off box if you have a matter that needs attention.",
	stopConversation = "True",
	options = {}
}
ep3RaevConvo:addScreen(greet_friend);

addConversationTemplate("ep3RaevConvo", ep3RaevConvo);

