courierConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "courier_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = " Finally. I don't like waiting. Where's the creds?",
	stopConversation = "false",
	options = {
		{"Creds?", "s_2"},
		{"Oh I didn't bring any.", "s_4"},
	}
}
courierConvo:addScreen(greetings);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Yeah, credits, money!",
	stopConversation = "false",
	options = {
		{"Ahh, oh I don't have any.", "s_4"},
	}
}
courierConvo:addScreen(s_2);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "I knew something was wrong when Morkov told me about you. Which is why I set up a little surprise. Get him!",
	stopConversation = "true",
	options = {}
}
courierConvo:addScreen(s_4);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "You better leave before you get hurt.",
	stopConversation = "true",
	options = {}
}
courierConvo:addScreen(quest_done);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "What?",
	stopConversation = "true",
	options = {}
}
courierConvo:addScreen(greet_friend);

addConversationTemplate("courierConvo", courierConvo);

