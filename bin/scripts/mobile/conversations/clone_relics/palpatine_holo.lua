morkov_courier = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "clone_relics_palpatine_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = "And so we meet. I just wish it hadn't been under such... distracting circumstances.",
	stopConversation = "false",
	options = {
		{"Y..yes my Lord.", "s_2"},
	}
}
morkov:addScreen(greetings);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Come closer, let me see you. Yes... I understand that you don't quite grasp what it is you have come across so let me explain.",
	stopConversation = "false",
	options = {
		{"Thank you my Lord.", "s_3"},
	}
}
morkov:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Although this Morkov is indeed the spider in the web, the treachery reaches deep in to our glorious Empire. The information you sent us is allowing my loyal subjects to find and... remove these disturbances quickly and efficiently.",
	stopConversation = "false",
	options = {
		{"I'm glad I could be of service my Lord.", "s_4"},
	}
}
morkov:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "So you have been, so you have been. There is one more favor to the Empire I'd like to ask of you though.",
	stopConversation = "false",
	options = {
		{"Of course my Lord.", "s_5"},
	}
}
morkov:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "Morkov, I want all of the logs from his 'business' and then I never want to hear of him again. Am I making myself clear?.",
	stopConversation = "false",
	options = {
		{"Yes my Lord, I understand.", "s_6"},
		{"I'm sorry my Lord, I'm not a killer.", "s_7"},
	}
}
morkov:addScreen(s_5);

s_6 = ConvoScreen:new {
	id = "s_6",
	leftDialog = "",
	customDialogText = "Good, I am most pleased. Until we meet again.",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(s_6);

s_7 = ConvoScreen:new {
	id = "s_7",
	leftDialog = "",
	customDialogText = "Oh but I think you are....",
	stopConversation = "false",
	options = {
		{"Yes my Lord, I will do as you say.", "s_6"},
		{"I'm afraid you are wrong my Lord.", "s_8"},
	}
}
morkov:addScreen(s_7);

s_8 = ConvoScreen:new {
	id = "s_8",
	leftDialog = "",
	customDialogText = "You are starting to disappoint me. In these times, I don't like being disappointed.",
	stopConversation = "false",
	options = {
		{"I understand my Lord, it will be done then.", "s_6"},
		{"I'm sorry, you will have to be in this case my Lord.", "s_9"},
	}
}
morkov:addScreen(s_8);

s_9 = ConvoScreen:new {
	id = "s_9",
	leftDialog = "",
	customDialogText = "No, I don't think so...",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(s_9);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "You know what I want. Now leave.",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(waiting_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "You should have known better...",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(complete_screen_final);

addConversationTemplate("morkov_courier", morkov_courier);

