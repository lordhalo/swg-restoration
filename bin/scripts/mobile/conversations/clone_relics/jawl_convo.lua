jawlConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "jawl_convo_handler",
	screens = {}
}
--Intro First
jawl_first_screen = ConvoScreen:new {
	id = "jawl_first_screen",
	leftDialog = "",
	customDialogText = " Yes, what's up?",
	stopConversation = "false",
	options = {
		{"I'm not sure you are the right person to talk to...", "s_2"},
	}
}
jawlConvo:addScreen(jawl_first_screen);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Well if it has anything to do with inventory, acquisition or deliveries, then I am. If not, then I'm not.",
	stopConversation = "false",
	options = {
		{"I suppose you could say it does. Do you know of Morkov?", "s_3"},
	}
}
jawlConvo:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Maybe... why?",
	stopConversation = "false",
	options = {
		{"I've come across some important information about his 'business'.", "s_4"},
	}
}
jawlConvo:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "Alright, I'm interested, what do you know?",
	stopConversation = "false",
	options = {
		{"Take a look at this. < hand Jawl the couriers logs >", "s_5"},
	}
}
jawlConvo:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "< Jawl spends a few minutes looking over the information in the logs > I knew that sod couldn't be trusted! This is some dynamite information, Jabba is not going to be happy, which is never a good thing. You're going to have to deliver this information to Jabba personally...",
	stopConversation = "false",
	options = {
		{"Umm... why don't you do it?", "s_6"},
	}
}
jawlConvo:addScreen(s_5);

s_6 = ConvoScreen:new {
	id = "s_6",
	leftDialog = "",
	customDialogText = "Are you cr... he would want to know who acquired the information and how. You need to tell him. Do you speak Huttese?",
	stopConversation = "false",
	options = {
		{"In fact, I do.", "s_7"},
	}
}
jawlConvo:addScreen(s_6);

s_7 = ConvoScreen:new {
	id = "s_7",
	leftDialog = "",
	customDialogText = "Really? Well I want to know that for sure before I send you up there. Go talk to Walda in the Wayfar cantina and she'll make sure you know as much as you think you do.",
	stopConversation = "false",
	options = {
		{"Who's she?", "s_8"},
	}
}
jawlConvo:addScreen(s_7);

s_8 = ConvoScreen:new {
	id = "s_8",
	leftDialog = "",
	customDialogText = "She used to be a translator in the palace and is fluent in Huttese. She will be able to make sure you know enough to make yourself understood and more importantly, understand Jabba.",
	stopConversation = "false",
	options = {
		{"No but doesn't he have a translator?", "s_9"},
	}
}
jawlConvo:addScreen(s_8);

s_9 = ConvoScreen:new {
	id = "s_9",
	leftDialog = "",
	customDialogText = "Yes but he wouldn't want all the guests in the throne room to know about this, especially with some of the names that are on here.",
	stopConversation = "false",
	options = {
		{"So why don't I meet him somewhere else?", "s_10"},
	}
}
jawlConvo:addScreen(s_9);

s_10 = ConvoScreen:new {
	id = "s_10",
	leftDialog = "",
	customDialogText = "I can't set up a private meeting with Jabba. I wish I had that sort of influence with him but... no we need to figure out a solution to this. Hmm, I think Walda still lives in Wayfar...",
	stopConversation = "false",
	options = {
		{"Who's that?", "s_11"},
	}
}
jawlConvo:addScreen(s_10);

s_11 = ConvoScreen:new {
	id = "s_11",
	leftDialog = "",
	customDialogText = "Walda used to be a translator in the palace, she's fluent in Huttese. You should be able to find her in the Wayfar cantina, have her teach you basic Huttese so you can privately convey this matter to Jabba.",
	stopConversation = "false",
	options = {
		{"Fine, I'll go find her.", "s_12"},
	}
}
jawlConvo:addScreen(s_11);

s_12 = ConvoScreen:new {
	id = "s_12",
	leftDialog = "",
	customDialogText = "Good, come back to me when you're ready and I'll set it up.",
	stopConversation = "true",
	options = {}
}
jawlConvo:addScreen(s_12);

walda_complete_screen = ConvoScreen:new {
	id = "walda_complete_screen",
	leftDialog = "",
	customDialogText = "Did you see Walda yet?",
	stopConversation = "false",
	options = {
		{"Sure did.", "s_13"},
	}
}
jawlConvo:addScreen(walda_complete_screen);

s_13 = ConvoScreen:new {
	id = "s_13",
	leftDialog = "",
	customDialogText = "Did she get you up to speed on Huttese?",
	stopConversation = "false",
	options = {
		{"Yeah I think I can make myself understood.", "s_14"},
	}
}
jawlConvo:addScreen(s_13);

s_14 = ConvoScreen:new {
	id = "s_14",
	leftDialog = "",
	customDialogText = "I sure hope so. Alright, you can go up to the throne room. I will let Fortuna know that you are coming up.",
	stopConversation = "true",
	options = {}
}
jawlConvo:addScreen(s_14);

jawl_waiting_screen = ConvoScreen:new {
	id = "jawl_waiting_screen",
	leftDialog = "",
	customDialogText = "We both have a lot to do. Come back to me when you are ready.",
	stopConversation = "true",
	options = {}
}
jawlConvo:addScreen(jawl_waiting_screen);

jawl_complete_screen = ConvoScreen:new {
	id = "jawl_complete_screen",
	leftDialog = "",
	customDialogText = "You're on your own from here, good luck...",
	stopConversation = "true",
	options = {}
}
jawlConvo:addScreen(jawl_complete_screen);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "I have too much stuff on my hands right now so please leave me alone.",
	stopConversation = "true",
	options = {}
}
jawlConvo:addScreen(greet_friend);

addConversationTemplate("jawlConvo", jawlConvo);

