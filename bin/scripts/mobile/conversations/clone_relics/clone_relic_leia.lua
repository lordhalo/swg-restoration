ep3LeiaConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "clone_relics_leia_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = "Yes, how may I help you soldier?",
	stopConversation = "false",
	options = {
		{"I bring grave news your highness.", "s_2"},
	}
}
ep3LeiaConvo:addScreen(greetings);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Like we didn't have enough of those...",
	stopConversation = "false",
	options = {
		{"I have come across logs that show of a traitor.", "s_3"},
	}
}
ep3LeiaConvo:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "Who might that be?",
	stopConversation = "false",
	options = {
		{"The arms dealer Morkov. He's been selling you faulty weapons.", "s_4"},
	}
}
ep3LeiaConvo:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "Let me see that please. < Leia takes the datapad and looks through the information on it > I've seen many men and women die from accidents and malfunctions. What we thought to be mistreatment on the soldiers part now turns out to be the intention of this monster!",
	stopConversation = "false",
	options = {
		{"What would you have me do your highness?", "s_5"},
	}
}
ep3LeiaConvo:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "I don't care what it takes but he has to be punished for these inhumane crimes. You know where to find him, can you do it soldier?",
	stopConversation = "false",
	options = {
		{"Of course your highness, he won't get away with this.", "s_6"},
	}
}
ep3LeiaConvo:addScreen(s_5);

s_6 = ConvoScreen:new {
	id = "s_6",
	leftDialog = "",
	customDialogText = "Good. Any more information you recover from him I'd be interested in as well, it will help us track down what he's sold to us. Good luck soldier and thank you, many men and women owe you their life already.",
	stopConversation = "True",
	options = {}
}
ep3LeiaConvo:addScreen(s_6);

s_7 = ConvoScreen:new {
	id = "s_7",
	leftDialog = "",
	customDialogText = "Good to see you again soldier. How did it go?",
	stopConversation = "false",
	options = {
		{"I got a hold of the scum and his logs.", "s_8"},
	}
}
ep3LeiaConvo:addScreen(s_7);

s_8 = ConvoScreen:new {
	id = "s_8",
	leftDialog = "",
	customDialogText = "And Morkov?",
	stopConversation = "false",
	options = {
		{"Let's just say that his business is closed, permanently.", "complete_screen_final"},
	}
}
ep3LeiaConvo:addScreen(s_8);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "I understand... We will use these logs to track down any of the equipment we purchased from him. Thank you again soldier, you have done the Alliance a great service and saved many lives.",
	stopConversation = "True",
	options = {}
}
ep3LeiaConvo:addScreen(complete_screen_final);

quest_wait_screen = ConvoScreen:new {
	id = "quest_wait_screen",
	leftDialog = "",
	customDialogText = "Return to me after you handle Morkov",
	stopConversation = "True",
	options = {}
}
ep3LeiaConvo:addScreen(quest_wait_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Come see me later soldier, I have to go over this with Ackbar.",
	stopConversation = "True",
	options = {}
}
ep3LeiaConvo:addScreen(quest_done);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hello",
	stopConversation = "True",
	options = {}
}
ep3LeiaConvo:addScreen(greet_friend);

addConversationTemplate("ep3LeiaConvo", ep3LeiaConvo);

