jabbaConvo = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "jabba_convo_handler",
	screens = {}
}
--Intro First
jabba_first_screen = ConvoScreen:new {
	id = "jabba_first_screen",
	leftDialog = "",
	customDialogText = "...",
	stopConversation = "false",
	options = {
		{"I bring grave news that I think might interest you Lord.", "s_2"},
	}
}
jabbaConvo:addScreen(jabba_first_screen);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "< Jabba laughs > You speak my language. I will give you a minute of my precious time then.",
	stopConversation = "false",
	options = {
		{"I believe you know of the arms dealer Morkov lord?", "s_3"},
	}
}
jabbaConvo:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "I know of that scum yes. Does this lead anywhere?",
	stopConversation = "false",
	options = {
		{"I have here logs that show him selling weapons to Valarian.", "s_4"},
	}
}
jabbaConvo:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "That filthy old hag! Fortuna, confirm those logs. < Bib Fortuna takes the datapad from you > Is it true? < Jabba roars > You know where Morkov is, kill him for me and bring me all information on his smuggling!",
	stopConversation = "false",
	options = {
		{"What is in it for me lord?", "s_5"},
	}
}
jabbaConvo:addScreen(s_4);

s_5 = ConvoScreen:new {
	id = "s_5",
	leftDialog = "",
	customDialogText = "You will walk out of this palace alive.",
	stopConversation = "false",
	options = {
		{"Yes lord Jabba, immediately.", "s_6"},
	}
}
jabbaConvo:addScreen(s_5);

s_6 = ConvoScreen:new {
	id = "s_6",
	leftDialog = "",
	customDialogText = "Good. < Jabba laughs >",
	stopConversation = "false",
	options = {
		{"Of course Lord Jabba, it will be done.", "s_7"},
	}
}
jabbaConvo:addScreen(s_6);

s_7 = ConvoScreen:new {
	id = "s_7",
	leftDialog = "",
	customDialogText = "I thought so. < Jabba laughs >",
	stopConversation = "true",
	options = {}
}
jabbaConvo:addScreen(s_7);


morkov_dead_screen = ConvoScreen:new {
	id = "morkov_dead_screen",
	leftDialog = "",
	customDialogText = "Fortuna, who is this? Oh it's you. Is Morkov dead?",
	stopConversation = "false",
	options = {
		{"Yes lord Jabba and I have his records.", "s_13"},
	}
}
jabbaConvo:addScreen(morkov_dead_screen);

s_13 = ConvoScreen:new {
	id = "s_13",
	leftDialog = "",
	customDialogText = "Lucky for you. Fortuna, take those. < Bib Fortuna takes the logs from you > Good, now move out of my way.",
	stopConversation = "true",
	options = {}
}
jabbaConvo:addScreen(s_13);


jabba_waiting_screen = ConvoScreen:new {
	id = "jabba_waiting_screen",
	leftDialog = "",
	customDialogText = "< Jabba chuckles >",
	stopConversation = "true",
	options = {}
}
jabbaConvo:addScreen(jabba_waiting_screen);

jabba_complete_screen = ConvoScreen:new {
	id = "jabba_complete_screen",
	leftDialog = "",
	customDialogText = "< Jabba chuckles >",
	stopConversation = "true",
	options = {}
}
jabbaConvo:addScreen(jabba_complete_screen);

greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Fortuna, who is this imbecile?",
	stopConversation = "true",
	options = {}
}
jabbaConvo:addScreen(greet_friend);

addConversationTemplate("jabbaConvo", jabbaConvo);

