morkov_courier = ConvoTemplate:new {
	initialScreen = "greetings",
	templateType = "Lua",
	luaClassHandler = "clone_relics_raev_convo_handler",
	screens = {}
}
--Intro First
greetings = ConvoScreen:new {
	id = "greetings",
	leftDialog = "",
	customDialogText = "What is it soldier?",
	stopConversation = "false",
	options = {
		{"This datapad contain important information regarding a traitor.", "s_2"},
	}
}
morkov:addScreen(greetings);

s_2 = ConvoScreen:new {
	id = "s_2",
	leftDialog = "",
	customDialogText = "Really? Very well, I'll send it to intel for analyzing. Wait a moment and I'll have their reply for you.",
	stopConversation = "false",
	options = {
		{"...", "s_3"},
	}
}
morkov:addScreen(s_2);

s_3 = ConvoScreen:new {
	id = "s_3",
	leftDialog = "",
	customDialogText = "What the heck did you have me send them? You need to go downstairs immediately, there's a holo call waiting for you.",
	stopConversation = "false",
	options = {
		{"From who?", "s_4"},
	}
}
morkov:addScreen(s_3);

s_4 = ConvoScreen:new {
	id = "s_4",
	leftDialog = "",
	customDialogText = "Just go down there soldier!",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(s_4);

s_7 = ConvoScreen:new {
	id = "s_7",
	leftDialog = "",
	customDialogText = "Welcome back soldier. I trust that you followed orders?",
	stopConversation = "false",
	options = {
		{"Yes Sir, Morkov will not bother anyone again.", "s_8"},
	}
}
morkov:addScreen(s_7);

s_8 = ConvoScreen:new {
	id = "s_8",
	leftDialog = "",
	customDialogText = "Excellent, and the logs from his operation?",
	stopConversation = "false",
	options = {
		{"Should all be on here Sir. < hand Raev the datapad >", "complete_screen_final"},
	}
}
morkov:addScreen(s_8);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Very good soldier, I will send this to intel right away. The Emperor will be most pleased.",
	stopConversation = "True",
	options = {}
}
morkov:addScreen(complete_screen_final);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "I don't have anything else I need help with here at the moment soldier. You should return to your duties.",
	stopConversation = "True",
	options = {}
morkov:addScreen(quest_done);

addConversationTemplate("morkov_courier", morkov_courier);

