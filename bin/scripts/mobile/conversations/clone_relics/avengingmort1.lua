AvengingMortConvoOne = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "clone_relics_1_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = " I'm a bit busy here okay? This glass is the only friend I need.",
	stopConversation = "false",
	options = {
		{"I was going to buy you another 'friend' but I guess not...", "second_screen"},
		{"Oh sorry.", "g_1"},
		{"Sure...", "g_1"},
	}
}
AvengingMortConvoOne:addScreen(greet_friend);

g_1 = ConvoScreen:new {
	id = "g_1",
	leftDialog = "",
	customDialogText = "See ya",
	stopConversation = "True",
	options = {}
}
AvengingMortConvoOne:addScreen(g_1);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Well, one can never have too many friends I guess. Bartender, another one of these! So, I assume this friend comes with a price?",
	stopConversation = "false",
	options = {
		{"I'm just curious, you look familiar...", "third_screen"},
	}
}
AvengingMortConvoOne:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "(Mort chuckles) Yeah I'm sure I do, considering I used to have more brothers than there's people on this planet.?",
	stopConversation = "false",
	options = {
		{"You're a clone?", "fourth_screen"},
	}
}
AvengingMortConvoOne:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "Guilty...",
	stopConversation = "false",
	options = {
		{"Never met a clone before. Do you work for the Empire?", "fifth_screen"},
	}
}
AvengingMortConvoOne:addScreen(fourth_screen);

fifth_screen = ConvoScreen:new {
	id = "fifth_screen",
	leftDialog = "",
	customDialogText = "I was part of 'liberating' the galaxy and bringing our 'glorious' Empire to power. Nowadays, I just drink...",
	stopConversation = "false",
	options = {
		{"That's not possible... I thought you all died young?", "sixth_screen"},
	}
}
AvengingMortConvoOne:addScreen(fifth_screen);

sixth_screen = ConvoScreen:new {
	id = "sixth_screen",
	leftDialog = "",
	customDialogText = "See this arm? Well it's not the only thing that's not flesh in my body. I would have been better off dying with the rest of them though...",
	stopConversation = "false",
	options = {
		{"Why do you say that?", "seventh_screen"},
	}
}
AvengingMortConvoOne:addScreen(sixth_screen);

seventh_screen = ConvoScreen:new {
	id = "seventh_screen",
	leftDialog = "",
	customDialogText = "What can I do? I'm a washed up soldier that knows nothing else, except drinking. I'm really getting good at that. My life has no purpose...",
	stopConversation = "false",
	options = {
		{"That can't be true. What happened to you?", "eigth_screen"},
	}
}
AvengingMortConvoOne:addScreen(seventh_screen);

eigth_screen = ConvoScreen:new {
	id = "eigth_screen",
	leftDialog = "",
	customDialogText = "I was bred to be part of one of the Clone Trooper commando squadrons, elite four man teams causing disruption behind enemy lines. During the battles on Kashyyyk my squadron and I had one such mission, taking out a command bunker deep inside enemy territory. You really interested in this?",
	stopConversation = "false",
	options = {
		{"Yes, please go on.", "ninth_screen"},
	}
}
AvengingMortConvoOne:addScreen(eigth_screen);

ninth_screen = ConvoScreen:new {
	id = "ninth_screen",
	leftDialog = "",
	customDialogText = "The mission was going well, it wasn't the first time we've done something like this. We blew up the bunker and was on our way to be extracted when the enemy found us. We notified command that we were under attack but would still be on time at extraction. We worked our way toward it as quickly as we could, dispatching pursuers be dozens...",
	stopConversation = "false",
	options = {
		{"Yes?", "tenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(ninth_screen);

tenth_screen = ConvoScreen:new {
	id = "tenth_screen",
	leftDialog = "",
	customDialogText = "As we approached the extraction point, we saw our LAAT take off. There was barely any enemy left and none at the extraction point. But our commander saw us as expendable clones and would rather lose us than risk a carrier.",
	stopConversation = "false",
	options = {
		{"What did you do?", "eleventh_screen"},
	}
}
AvengingMortConvoOne:addScreen(tenth_screen);

eleventh_screen = ConvoScreen:new {
	id = "eleventh_screen",
	leftDialog = "",
	customDialogText = "We tried to make it back towards our own ranks but we were deep inside the Kashyyyk forests, and let me tell you, those forests are deep. Over the next few weeks, I experienced the deaths of my three brothers to wildlife, enemies and nature itself...",
	stopConversation = "false",
	options = {
		{"I thought clones didn't have feelings?", "twelfth_screen"},
	}
}
AvengingMortConvoOne:addScreen(eleventh_screen);

twelfth_screen = ConvoScreen:new {
	id = "twelfth_screen",
	leftDialog = "",
	customDialogText = "So did our commanders but let me tell you kid, I felt pain so strong that it could have been my own as my brothers fell.",
	stopConversation = "false",
	options = {
		{"So how did you eventually end up here?", "thirtheenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(twelfth_screen);

thirtheenth_screen = ConvoScreen:new {
	id = "thirtheenth_screen",
	leftDialog = "",
	customDialogText = "For months, maybe even years, I was living in the forests of Kashyyyk. During a fight over food with one of the monstrous creatures on the planet, before I could put it down, I lost my arm. I just laid down on my back and thought, well that was it...",
	stopConversation = "false",
	options = {
		{"But it wasn't...", "fourteenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(thirtheenth_screen);

fourteenth_screen = ConvoScreen:new {
	id = "fourteenth_screen",
	leftDialog = "",
	customDialogText = "No, as I was laying there, bleeding to death, a research expedition happened upon me. They saved my life but had ulterior motives. Dr. Ortoz, the leader of the expedition, saw me as the perfect lab animal and started experimenting on me.",
	stopConversation = "false",
	options = {
		{"And I thought my life was tough...", "fifthteenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(fourteenth_screen);

fifthteenth_screen = ConvoScreen:new {
	id = "fifthteenth_screen",
	leftDialog = "",
	customDialogText = "Eventually I regained my strength though and put an end to the experiments...permanently. I managed to sneak on to a freighter and make my way to here...and that about wraps up my life story.",
	stopConversation = "false",
	options = {
		{"Amazing...and you've been here ever since?", "sixteenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(fifthteenth_screen);

sixteenth_screen = ConvoScreen:new {
	id = "sixteenth_screen",
	leftDialog = "",
	customDialogText = "Well I did try to track down my old commander for some good old revenge a few years ago. By that time he was a high ranking officer in the Empire and I had no chance to get him, so I gave up.",
	stopConversation = "false",
	options = {
		{"Is he still alive?", "seventeenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(sixteenth_screen);

seventeenth_screen = ConvoScreen:new {
	id = "seventeenth_screen",
	leftDialog = "",
	customDialogText = "Yes, he's retired now but not much easier to get to, especially in my state. From what I've managed to find out, he's running a racket smuggling Imperial weapons he can get ahold of with old contacts, selling them to anyone willing to pay.",
	stopConversation = "false",
	options = {
		{"Why don't you turn him in?", "eighteenth_screen"},
	}
}
AvengingMortConvoOne:addScreen(seventeenth_screen);

eighteenth_screen = ConvoScreen:new {
	id = "eighteenth_screen",
	leftDialog = "",
	customDialogText = "If the Empire found out I was alive, well let's just say I wouldn't stay that way. Besides, I don't have any evidence...",
	stopConversation = "false",
	options = {
		{"Say I had some time to look in to this, where would I start?", "nineteenth_screen"},
		{"Well, that's too bad, I'll be going then", "end_conversation_screen"}
	}
}
AvengingMortConvoOne:addScreen(eighteenth_screen);

end_conversation_screen = ConvoScreen:new {
	id = "end_conversation_screen",
	leftDialog = "",
	customDialogText = "Farewell my friend",
	stopConversation = "true",
}
AvengingMortConvoOne:addScreen(end_conversation_screen);

nineteenth_screen = ConvoScreen:new {
	id = "nineteenth_screen",
	leftDialog = "",
	customDialogText = "You are something else friend..hehe. If nothing else, you've brought a smile on my face for the first time in...well maybe for the first time. If you're serious though, he's running his operation out of the hotel in Keren on Naboo. His name is Morkov.",
	stopConversation = "false",
	options = {
		{"I will look in to it my friend. You want another?", "twentieth_screen"},
	}
}
AvengingMortConvoOne:addScreen(nineteenth_screen);

twentieth_screen = ConvoScreen:new {
	id = "twentieth_screen",
	leftDialog = "",
	customDialogText = "Actually...I think I'm fine. I'm not getting my hopes up but if you're actually going to check in to it, I wish you good luck and thank you.",
	stopConversation = "false",
	options = {
		{"I will be back and let you know what happened.", "accept_screen"},
	}
}
AvengingMortConvoOne:addScreen(twentieth_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "You are something else friend...",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(accept_screen);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "You're back so soon?",
	stopConversation = "false",
	options = {
		{"Just wanted to let you know I'm heading to Naboo now.", "waiting_screen_2"},
		--{"Yeah I'm afraid something came up, I can't do this right now.", "restart_screen"}
	}
}
AvengingMortConvoOne:addScreen(waiting_screen);

restart_screen = ConvoScreen:new {
	id = "restart_screen",
	leftDialog = "",
	customDialogText = "That's too bad",
	stopConversation = "True",
	options = {}
}
AvengingMortConvoOne:addScreen(restart_screen);

waiting_screen_2 = ConvoScreen:new {
	id = "waiting_screen_2",
	leftDialog = "",
	customDialogText = "Let me know if you find anything",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(waiting_screen_2);


complete_uwo_screen = ConvoScreen:new {
	id = "complete_uwo_screen",
	leftDialog = "",
	customDialogText = "There you are. How are things going??",
	stopConversation = "false",
	options = {
		{"Quite well. I've recovered these logs of Morkov's 'business'", "complete_screen_2"},
	}
}
AvengingMortConvoOne:addScreen(complete_uwo_screen);

c_3 = ConvoScreen:new {
	id = "c_3",
	leftDialog = "",
	customDialogText = "< Mort looks through the data on the log > This is really good. I don't pretend to understand everything that is on here but I bet that the Rebel Alliance would be very interested in some of these transactions. Especially the ones that show him selling low quality and even faulty equipment to them. He must be responsible for hundreds of deaths on their part.",
	stopConversation = "false",
	options = {
		{"I bet you are right.", "c_s"},
	}
}
AvengingMortConvoOne:addScreen(c_3);

complete_screen_2 = ConvoScreen:new {
	id = "complete_screen_2",
	leftDialog = "",
	customDialogText = "What are you going to do?",
	stopConversation = "false",
	options = {
		{"I'm going to find the right person in the Alliance to show this to.", "c_3"},
		{"I'm going to show this to someone with Imperial authority.", "c_4"},
		{"I'm going to show this to someone at Jabba's palace.", "c_5"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_2);

c_3 = ConvoScreen:new {
	id = "c_3",
	leftDialog = "",
	customDialogText = "< Mort looks through the data on the log > This is really good. I don't pretend to understand everything that is on here but I bet that the Rebel Alliance would be very interested in some of these transactions. Especially the ones that show him selling low quality and even faulty equipment to them. He must be responsible for hundreds of deaths on their part.",
	stopConversation = "false",
	options = {
		{"I bet you are right.", "c_s"},
	}
}
AvengingMortConvoOne:addScreen(c_3);

c_4 = ConvoScreen:new {
	id = "c_4",
	leftDialog = "",
	customDialogText = "< Mort looks through the data on the log > This is really good. I don't pretend to understand everything that is on here but I bet the Empire would be very interested in this. He's even sold equipment to the Rebel Alliance.",
	stopConversation = "false",
	options = {
		{"I bet you are right.", "c_s_1"},
	}
}
AvengingMortConvoOne:addScreen(c_4);

c_5 = ConvoScreen:new {
	id = "c_5",
	leftDialog = "",
	customDialogText = "< Mort looks through the data on the log > This is really good. I don't pretend to understand everything that is on here but I bet that Jabba the Hutt would be very interested in some of these transactions, especially the ones to Lady Valarian. They are not particularly fond of each other from what I hear.",
	stopConversation = "false",
	options = {
		{"I bet you are right.", "c_s_2"},
	}
}
AvengingMortConvoOne:addScreen(c_5);

c_s = ConvoScreen:new {
	id = "c_s",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s);

c_s_1 = ConvoScreen:new {
	id = "c_s_1",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine, and what a fitting end for old Morkov too. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s_1);

c_s_2 = ConvoScreen:new {
	id = "c_s_2",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine. I hear Jabba is quite the genius when it comes to plotting some ones demise. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s_2);

c_s_wait = ConvoScreen:new {
	id = "c_s_wait",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s_wait);

c_s_1_wait = ConvoScreen:new {
	id = "c_s_1_wait",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine, and what a fitting end for old Morkov too. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s_1_wait);

c_s_2_wait = ConvoScreen:new {
	id = "c_s_2_wait",
	leftDialog = "",
	customDialogText = "< Mort chuckles > Revenge will finally be mine. I hear Jabba is quite the genius when it comes to plotting some ones demise. Come back and let me know what happened, I can't wait.",
	stopConversation = "true",
	options = {}
}
AvengingMortConvoOne:addScreen(c_s_2_wait);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Good to see you my friend! How are you doing?",
	stopConversation = "false",
	options = {
		{"Everything is going well Mort, how are you?", "q_1"},
	}
}
AvengingMortConvoOne:addScreen(quest_done);

q_1 = ConvoScreen:new {
	id = "q_1",
	leftDialog = "",
	customDialogText = "Things are not looking too bad. I've got myself a job working security at the space station. Not glamorous but it's something to do.",
	stopConversation = "false",
	options = {
		{"Still enjoying a drink though?", "q_2"},
	}
}
AvengingMortConvoOne:addScreen(q_1);

q_2 = ConvoScreen:new {
	id = "q_2",
	leftDialog = "",
	customDialogText = "< Mort laughs > Yeah, but only on my days off! Which reminds me, I should probably go and get some rest. It was great to see you again, take care of yourself.",
	stopConversation = "True",
	options = {}
}
AvengingMortConvoOne:addScreen(q_2);

complete_screen_reb = ConvoScreen:new {
	id = "complete_screen_reb",
	leftDialog = "",
	customDialogText = "Good to see you again. What's new?",
	stopConversation = "false",
	options = {
		{"Morkov finally got what he deserved.", "complete_screen_31"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_reb);

complete_screen_31 = ConvoScreen:new {
	id = "complete_screen_31",
	leftDialog = "",
	customDialogText = "Are you sure? What happened?",
	stopConversation = "false",
	options = {
		{"I went to the Alliance. They got a bit upset.", "complete_screen_32"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_31);

complete_screen_32 = ConvoScreen:new {
	id = "complete_screen_32",
	leftDialog = "",
	customDialogText = "They had him killed?",
	stopConversation = "false",
	options = {
		{"Sort of, they asked me to do it.", "complete_screen_33"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_32);

complete_screen_33 = ConvoScreen:new {
	id = "complete_screen_33",
	leftDialog = "",
	customDialogText = "After all these years... I can't believe it... When you went on your way to the Alliance I didn't truly expect to actually get my revenge, but you proved me wrong my friend. I may sound blood thirsty but after what that scum did to me and probably thousands of others, he deserved it, trust me.",
	stopConversation = "false",
	options = {
		{"I agree, or I wouldn't have gone through with this.", "complete_screen_34"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_33);

complete_screen_34 = ConvoScreen:new {
	id = "complete_screen_34",
	leftDialog = "",
	customDialogText = "I couldn't possibly repay what I owe you but I did save something that you may find useful. If you're interested, I still have my old combat armor. I don't need it anymore.",
	stopConversation = "false",
	options = {
		{"I'd be honored to put it to use. So what will you do now?", "complete_screen_35"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_34);

complete_screen_35 = ConvoScreen:new {
	id = "complete_screen_35",
	leftDialog = "",
	customDialogText = "I'll just finish living my life. Maybe cut back on the drinking some, try to straighten myself out.",
	stopConversation = "false",
	options = {
		{"Sounds good my friend. I wish you the best of luck.", "complete_screen_final"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_35);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Thank you. Stop by some day, let me know how you are doing.",
	stopConversation = "True",
	options = {}
}
AvengingMortConvoOne:addScreen(complete_screen_final);

complete_screen_imp = ConvoScreen:new {
	id = "complete_screen_imp",
	leftDialog = "",
	customDialogText = "Good to see you again. What's new?",
	stopConversation = "false",
	options = {
		{"Morkov finally got what he deserved.", "complete_screen_41"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_imp);

complete_screen_41 = ConvoScreen:new {
	id = "complete_screen_41",
	leftDialog = "",
	customDialogText = "Are you sure? What happened?",
	stopConversation = "false",
	options = {
		{"After I turned in the evidence, the Emperor ordered his termination.", "complete_screen_42"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_41);

complete_screen_42 = ConvoScreen:new {
	id = "complete_screen_42",
	leftDialog = "",
	customDialogText = "And you are sure they went through with it?",
	stopConversation = "false",
	options = {
		{"Yes, I was the one that had to carry out the sentence...", "complete_screen_33"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_42);

complete_screen_jabba = ConvoScreen:new {
	id = "complete_screen_jabba",
	leftDialog = "",
	customDialogText = "Good to see you again. What's new?",
	stopConversation = "false",
	options = {
		{"Morkov finally got what he deserved.", "complete_screen_51"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_jabba);

complete_screen_51 = ConvoScreen:new {
	id = "complete_screen_51",
	leftDialog = "",
	customDialogText = "Are you sure? What happened?",
	stopConversation = "false",
	options = {
		{"I went to Jabba. Needless to say he wasn't too happy.", "complete_screen_52"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_51);

complete_screen_52 = ConvoScreen:new {
	id = "complete_screen_52",
	leftDialog = "",
	customDialogText = "And you are sure they killed him?",
	stopConversation = "false",
	options = {
		{"Couldn't get more sure. I had to do it.", "complete_screen_33"},
	}
}
AvengingMortConvoOne:addScreen(complete_screen_52);

addConversationTemplate("AvengingMortConvoOne", AvengingMortConvoOne);

