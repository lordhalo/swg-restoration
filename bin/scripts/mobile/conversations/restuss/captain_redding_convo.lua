captain_redding_convo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "captain_redding_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Greetings Solder, We have some Targets in Restuss that need to be eliminated, What interest you first? ",
	stopConversation = "false",
	options = {
		{"I will help push back Stormtroopers", "normal_screen"},
		{"I will help push back DarkTroopers", "elite_screen"},
	}
}
captain_redding_convo:addScreen(greet_friend);

normal_screen = ConvoScreen:new {
	id = "normal_screen",
	leftDialog = "",
	customDialogText = "The Alliance needs to secure any remains of the Imperial research in Restuss. You've been tasked with aiding in securing the city by defeating 20 Imperial Stormtroopers.",
	stopConversation = "false",
	options = {
		{"Yes Sir", "accept_screen"},
	}
}
captain_redding_convo:addScreen(normal_screen);

elite_screen = ConvoScreen:new {
	id = "elite_screen",
	leftDialog = "",
	customDialogText = "The Alliance needs to secure any remains of the Imperial research in Restuss. The Imperial Army has brought in some of their most skilled soldiers, Urban Assault Dark Troopers. Your task is to quell this high threat be eliminating 5 of them.",
	stopConversation = "false",
	options = {
		{"Yes Sir", "accept_screen_elite"},
	}
}
captain_redding_convo:addScreen(elite_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(accept_screen);

accept_screen_elite = ConvoScreen:new {
	id = "accept_screen_elite",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(accept_screen_elite);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Good work!",
	stopConversation = "false",
	options = {
		{"Thank you, Sir", "complete_screen_final"},
	}
}
captain_redding_convo:addScreen(complete_screen);

complete_elite_screen = ConvoScreen:new {
	id = "complete_elite_screen",
	leftDialog = "",
	customDialogText = "Good work eliminating the Dark Troopers.",
	stopConversation = "false",
	options = {
		{"Thank you, Sir", "complete_elite_screen_final"},
	}
}
captain_redding_convo:addScreen(complete_elite_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "At ease.",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(complete_screen_final);

complete_elite_screen_final = ConvoScreen:new {
	id = "complete_elite_screen_final",
	leftDialog = "",
	customDialogText = "At ease.",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(complete_elite_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "You've been ordered to eliminate Imperials in Restuss.",
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "waiting_screen_two"},
		{"I would like to start over, sir", "restart_screen"},
	}
}
captain_redding_convo:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "QUEST IS DONE",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(quest_done);

restart_screen = ConvoScreen:new {
	id = "restart_screen",
	leftDialog = "",
	customDialogText = "I'll scrub the records, get back to work!",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(restart_screen);

waiting_screen_two = ConvoScreen:new {
	id = "waiting_screen_two",
	leftDialog = "",
	customDialogText = "Good luck Solder",
	stopConversation = "true",
	options = {}
}
captain_redding_convo:addScreen(waiting_screen_two);

addConversationTemplate("captain_redding_convo", captain_redding_convo);

