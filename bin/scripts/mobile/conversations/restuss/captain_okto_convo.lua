captain_okto_convo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "captain_okto_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "The Rebel scum have placed antennas around Restuss, to spy on Imperial communications and coordinate their troops. Your orders are to destroy one of these antennas.",
	stopConversation = "false",
	options = {
		{"Yes Sir", "accept_screen"},
	}
}
captain_okto_convo:addScreen(greet_friend);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
captain_okto_convo:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Good work!",
	stopConversation = "false",
	options = {
		{"Thank you, Sir", "complete_screen_final"},
	}
}
captain_okto_convo:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "At ease.",
	stopConversation = "true",
	options = {}
}
captain_okto_convo:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "You've been ordered to destroy one of the Rebel antennas in Restuss.",
	stopConversation = "true",
	options = {}
}
captain_okto_convo:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "QUEST IS DONE",
	stopConversation = "true",
	options = {}
}
captain_okto_convo:addScreen(quest_done);

addConversationTemplate("captain_okto_convo", captain_okto_convo);

