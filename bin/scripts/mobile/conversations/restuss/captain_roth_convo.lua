captain_roth_convo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "captain_roth_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Greetings Solder, We have some Targets in Restuss that need to be eliminated, What interest you first? ",
	stopConversation = "false",
	options = {
		{"I will help push back Rebel Troopers", "normal_screen"},
		{"I will help push back Rebel Commandos", "elite_screen"},
	}
}
captain_roth_convo:addScreen(greet_friend);

normal_screen = ConvoScreen:new {
	id = "normal_screen",
	leftDialog = "",
	customDialogText = "The Empire needs to make sure that the Rebels don't get their hands on any leftovers from the failed experiment in Restuss. Your orders are to defeat 20 Rebel soldiers in Restuss, to help restore control of the city.",
	stopConversation = "false",
	options = {
		{"Yes Sir", "accept_screen"},
	}
}
captain_roth_convo:addScreen(normal_screen);

elite_screen = ConvoScreen:new {
	id = "elite_screen",
	leftDialog = "",
	customDialogText = "The Empire needs to make sure that the Rebels don't get their hands on any left overs from the failed experiment in Restuss. The Rebel scum has enlisted some of their 'finest' soldiers for their dirty business. Your orders are to eliminate 5 of these Rebel 'Elite' Commandos.",
	stopConversation = "false",
	options = {
		{"Yes Sir", "accept_screen_elite"},
	}
}
captain_roth_convo:addScreen(elite_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(accept_screen);

accept_screen_elite = ConvoScreen:new {
	id = "accept_screen_elite",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(accept_screen_elite);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Good work!",
	stopConversation = "false",
	options = {
		{"Thank you, Sir", "complete_screen_final"},
	}
}
captain_roth_convo:addScreen(complete_screen);

complete_elite_screen = ConvoScreen:new {
	id = "complete_elite_screen",
	leftDialog = "",
	customDialogText = "Good work eliminating the Rebel Commandos.",
	stopConversation = "false",
	options = {
		{"Thank you, Sir", "complete_elite_screen_final"},
	}
}
captain_roth_convo:addScreen(complete_elite_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "At ease.",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(complete_screen_final);

complete_elite_screen_final = ConvoScreen:new {
	id = "complete_elite_screen_final",
	leftDialog = "",
	customDialogText = "At ease.",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(complete_elite_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "You've been ordered to eliminate Rebels in Restuss.",
	stopConversation = "false",
	options = {
		{"Yes! Sir!", "waiting_screen_two"},
		{"I would like to start over, sir", "restart_screen"},
	}
}
captain_roth_convo:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "QUEST IS DONE",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(quest_done);

restart_screen = ConvoScreen:new {
	id = "restart_screen",
	leftDialog = "",
	customDialogText = "I'll scrub the records, get back to work!",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(restart_screen);

waiting_screen_two = ConvoScreen:new {
	id = "waiting_screen_two",
	leftDialog = "",
	customDialogText = "Good luck Solder",
	stopConversation = "true",
	options = {}
}
captain_roth_convo:addScreen(waiting_screen_two);

addConversationTemplate("captain_roth_convo", captain_roth_convo);

