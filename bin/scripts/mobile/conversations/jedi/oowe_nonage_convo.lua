oowe_nonage_convo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "oowe_nonage_convo_handler",
	screens = {}

}
--Intro Frst
greet_player = ConvoScreen:new {
	id = "greet_player",
	leftDialog = "",
	customDialogText = "Leave me, I don't care if you are Jedi or Sith. I only seek to gain power on my own.", 
	stopConversation = "false",
	options = {
		{"Oowe? Hey lets take it easy.", "screen_two"}
	}
}
oowe_nonage_convo:addScreen(greet_player);

screen_two = ConvoScreen:new {
	id = "screen_two",
	leftDialog = "",
	customDialogText = "*Beams* Im not Oowe!!..No..No..Im..I am lost.", 
	stopConversation = "false",
	options = {
		{"Listen, I know you took an Object from the Sif family.", "screen_three"}
	}
}
oowe_nonage_convo:addScreen(screen_two);

screen_three = ConvoScreen:new {
	id = "screen_three",
	leftDialog = "",
	customDialogText = "I EARNED THAT OBJECT!!! Gah! No, All those lives lost.. How could I do such a thing..", 
	stopConversation = "false",
	options = {
		{"So.. It was you who destroyed the city of Durbin", "screen_four"}
	}
}
oowe_nonage_convo:addScreen(screen_three);

screen_four = ConvoScreen:new {
	id = "screen_four",
	leftDialog = "",
	customDialogText = "Yes..My mind is slipping, Ever since I found the Sith Holocron my Master sent me to find.", 
	stopConversation = "false",
	options = {
		{"Its going to be ok, we can work this out.", "screen_five_light"},
		{"Good.", "screen_five_dark"},
	}
}
oowe_nonage_convo:addScreen(screen_four);

screen_five_light = ConvoScreen:new {
	id = "screen_five_light",
	leftDialog = "",
	customDialogText = "Its..Destroying me, Iv come to this Dark place in search of a Sith Lord who died ages ago. Please, Just end me!", 
	stopConversation = "false",
	options = {
		{"There has to be another way! The Council will know how to help!", "screen_six_light"},
	}
}
oowe_nonage_convo:addScreen(screen_five_light);

screen_six_light = ConvoScreen:new {
	id = "screen_six_light",
	leftDialog = "",
	customDialogText = "Its..Destroying me, Iv come to this Dark place in search of a Sith Lord who died ages ago. Please, Just end me!", 
	stopConversation = "false",
	options = {
		{"There has to be another way! The Council will know how to help!", "screen_seven_light"},
	}
}
oowe_nonage_convo:addScreen(screen_six_light);

screen_seven_light = ConvoScreen:new {
	id = "screen_seven_light",
	leftDialog = "",
	customDialogText = "Yes..The Council..can..can..BURN!", 
	stopConversation = "true",
	options = {}
}
oowe_nonage_convo:addScreen(screen_seven_light);

screen_five_dark = ConvoScreen:new {
	id = "screen_five_dark",
	leftDialog = "",
	customDialogText = "I was hoping it would come to this.", 
	stopConversation = "true",
	options = {}
}
oowe_nonage_convo:addScreen(screen_five_dark);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Hello there.", 
	stopConversation = "true",
	options = {}
}
oowe_nonage_convo:addScreen(go_away);



addConversationTemplate("oowe_nonage_convo", oowe_nonage_convo);
