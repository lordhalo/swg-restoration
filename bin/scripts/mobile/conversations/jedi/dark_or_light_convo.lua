dark_or_light_convo = ConvoTemplate:new {
	initialScreen = "greet_screen",
	templateType = "Lua",
	luaClassHandler = "dark_or_light_convo_handler",
	screens = {}

}
--Intro Frst
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Iv been expecting you, The Force has guided you here to help me young Padawan. ", 
	stopConversation = "false",
	options = {
		{"What has happened here?", "screen_two"}
	}
}
dark_or_light_convo:addScreen(greet_friend);

screen_two = ConvoScreen:new {
	id = "screen_two",
	leftDialog = "",
	customDialogText = "I am Master Erym Kafialp, I had sent my Padawan on a quest to retreive an artifact of great power on this Planet.. Unfortunately, hes gone missing.", 
	stopConversation = "false",
	options = {
		{"Do you think he destroyed this village?", "screen_three"}
	}
}
dark_or_light_convo:addScreen(screen_two);

screen_three = ConvoScreen:new {
	id = "screen_three",
	leftDialog = "",
	customDialogText = "Its unclear, Rumor has it a Sith attacked this village. I believe the artifact, or perhaps a clue to its location was here in this village. I found my Padawan, Oowe Nonage's Lightsaber here split in half.", 
	stopConversation = "false",
	options = {
		{"Split in half?", "screen_four"}
	}
}
dark_or_light_convo:addScreen(screen_three);

screen_four = ConvoScreen:new {
	id = "screen_four",
	leftDialog = "",
	customDialogText = "Yes.. the Dark Side of the force is clouding my vision of what has transpired here. Meditate on this I have, and the Force has sent me you. Perhaps you should look around for a clue leading to my Padawan or this 'Sith' who attacked the village.", 
	stopConversation = "false",
	options = {
		{"I will take a look around, Master.", "screen_five"}
	}
}
dark_or_light_convo:addScreen(screen_four);

screen_five = ConvoScreen:new {
	id = "screen_five",
	leftDialog = "",
	customDialogText = "May the Force be with you.", 
	stopConversation = "true",
	options = {}
}
dark_or_light_convo:addScreen(screen_five);


tracking_screen = ConvoScreen:new {
	id = "tracking_screen",
	leftDialog = "",
	customDialogText = "Have you found any clues?", 
	stopConversation = "false",
	options = {
		{"I found a T21 Rifle, Possibly made at the Weapons Depot. Im going to investigate.", "clue_close"},
		{"I found Kahmurra DNA, I think it came from a Research station. Im going to investigate.", "clue_close"},
		{"I found a Detainment center Jumpsuit, Im going to investgate.", "clue_close"},
		{"I want to visit the Sif family and ask them some questions", "clue_close"},
		{"None of this feels right, Im think the Binyare Pirates are responsible", "clue_close"}
	}
}
dark_or_light_convo:addScreen(tracking_screen);

clue_close = ConvoScreen:new {
	id = "clue_close",
	leftDialog = "",
	customDialogText = "You must continue this investigation alone Padawan, I will be waiting for you. I must prepair myself for what you find. Find me at the Land bleached in fiery ashes on Lok.", 
	stopConversation = "true",
	options = {}
}
dark_or_light_convo:addScreen(clue_close);


go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Hello there.", 
	stopConversation = "true",
	options = {}
}
dark_or_light_convo:addScreen(go_away);

phasetwo_screen_one = ConvoScreen:new {
	id = "phasetwo_screen_one",
	leftDialog = "",
	customDialogText = "Greetings Padawan.. I see you have come alone, I sense my Padawan is no more..", 
	stopConversation = "false",
	options = {
		{"I had no choice Master, His mind was damaged by a Sith Holocron", "light_two"},
		{"And you're next! the Artifact is mine!", "sith_close"},
	}
}
dark_or_light_convo:addScreen(phasetwo_screen_one);

sith_close = ConvoScreen:new {
	id = "sith_close",
	leftDialog = "",
	customDialogText = "I shall do as I must..", 
	stopConversation = "true",
	options = {}
}
dark_or_light_convo:addScreen(sith_close);

light_two = ConvoScreen:new {
	id = "light_two",
	leftDialog = "",
	customDialogText = "Unfortinate he could not pass the Trial of Insight, Somehow allowing himself to be captivated by a Sith Holocron.", 
	stopConversation = "false",
	options = {
		{"I retrieved the Artifact he held.", "light_three"}
	}
}
dark_or_light_convo:addScreen(light_two);

light_three = ConvoScreen:new {
	id = "light_three",
	leftDialog = "",
	customDialogText = "Ahh, A Sith Holocron of Knowledge and Krayt Dragon Pearl. It would seem the Sith used the Dark side of the Force to Polish these Krayt pearls into near Flawless condition to improve their Lightsabers..", 
	stopConversation = "false",
	options = {
		{"Interesting, What will we do with them?", "light_four"}
	}
}
dark_or_light_convo:addScreen(light_three);

light_four = ConvoScreen:new {
	id = "light_four",
	leftDialog = "",
	customDialogText = "The Holocron has a strong connection with the Dark Side, It must be destroyed.", 
	stopConversation = "false",
	options = {
		{"Is that why you are at this Volcano?", "light_five"}
	}
}
dark_or_light_convo:addScreen(light_four);

light_five = ConvoScreen:new {
	id = "light_five",
	leftDialog = "",
	customDialogText = "Indeed Padawan. *Cast the Objects into the fire* You have done well. Even with such knowledge in your hands you resisted the temptations of the Dark Side. I want you to have this, Its a Crystal I was keeping for Oowe when he completed his Trials.. Nevertheless, You will make a great addition to the New Jedi Order.", 
	stopConversation = "true",
	options = {}
}
dark_or_light_convo:addScreen(light_five);



addConversationTemplate("dark_or_light_convo", dark_or_light_convo);
