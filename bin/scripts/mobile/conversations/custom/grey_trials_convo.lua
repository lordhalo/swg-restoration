grey_trials_convo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "grey_trials_convo_handler",
	screens = {}
}
--Intro First
grey_intro_screen = ConvoScreen:new {
	id = "grey_intro_screen",
	leftDialog = "",
	customDialogText = "Sah dude, join up",
	stopConversation = "false",
	options = {
		{"Okay", "grey_trial_done"},
	}
}
grey_trials_convo:addScreen(grey_intro_screen);

grey_trial_done = ConvoScreen:new {
	id = "grey_trial_done",
	leftDialog = "",
	customDialogText = "Sick bruh",
	stopConversation = "true",
	options = {}
}
grey_trials_convo:addScreen(grey_trial_done);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "QUEST IS DONE",
	stopConversation = "true",
	options = {}
}
grey_trials_convo:addScreen(quest_done);

addConversationTemplate("grey_trials_convo", grey_trials_convo);

