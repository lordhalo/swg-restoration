initiateQuestDark = ConvoTemplate:new {

	initialScreen = "initiate_first_screen",

	templateType = "Lua",

	luaClassHandler = "initiateQuestDark_handler",

	screens = {}

}





--Intro First

first_screen = ConvoScreen:new {

	id = "initiate_first_screen",

	leftDialog = "",

	customDialogText = "I am Vlixu, I have watched you grow in the force ever since you entered this temple young Acolyte. It is time for your next phase of training. Once complete you will become a Sith Adept, bringing one step closer to full mastery of the Darkside. Are you ready to begin?",

	stopConversation = "false",

	options = {

		{"My destiny this is, I am ready.", "initiate_second_screen"},

		{"I have much still to learn my lord, I will return when I am ready.", "fork_fs"}

	

	}

}



initiateQuestDark:addScreen(first_screen);

second_screen = ConvoScreen:new {

	id = "initiate_second_screen",

	leftDialog = "",

	customDialogText = "First, resite the Sith code with me. Peace is a lie...",

	stopConversation = "false",

	options = {

		{"It is only a dream.", "wrong_screen"},

		{"There is only passion.", "initiate_third_screen"}, 
		
		{"It is for the weak.", "wrong_screen"}, 

	

	}

}



initiateQuestDark:addScreen(second_screen);

third_screen = ConvoScreen:new {

	id = "initiate_third_screen",

	leftDialog = "",

	customDialogText = "Through passion...",

	stopConversation = "false",

	options = {

		{"I gain respect.", "wrong_screen"},

		{"I am stronger.", "wrong_screen"}, 
		
		{"I gain stregnth.", "initiate_forth_screen"}, 

	

	}

}



initiateQuestDark:addScreen(third_screen);

forth_screen = ConvoScreen:new {

	id = "initiate_forth_screen",

	leftDialog = "",

	customDialogText = "Through strength...",

	stopConversation = "false",

	options = {
	
		{"I gain power.", "initiate_fifth_screen"}, 

		{"Destruction I bring.", "wrong_screen"},
		
		{"I devour the weak.", "wrong_screen"}, 

	

	}

}



initiateQuestDark:addScreen(forth_screen);

fifth_screen = ConvoScreen:new {

	id = "initiate_fifth_screen",

	leftDialog = "",

	customDialogText = "Through Power...",

	stopConversation = "false",

	options = {
	
		{"I gain victory.", "initiate_sixth_screen"}, 

		{"I control all.", "wrong_screen"},
		
		{"I light the way.", "wrong_screen"}, 

	

	}

}



initiateQuestDark:addScreen(fifth_screen);

sixth_screen = ConvoScreen:new {

	id = "initiate_sixth_screen",

	leftDialog = "",

	customDialogText = "Through victory my chains are broken.",

	stopConversation = "false",

	options = {
	
		{"The force shall free me.", "initiate_complete_screen"}, 

	}

}



initiateQuestDark:addScreen(sixth_screen);

complete_screen = ConvoScreen:new {

	id = "initiate_complete_screen",

	leftDialog = "",

	customDialogText = "The Sith code is the primal doctorine that binds us all Adept. Remember it always.",

	stopConversation = "false",

	options = {
	
		{"Yes my lord. Is the anything further you ask of me?", "next_npc_screen"}, 

	}

}



initiateQuestDark:addScreen(complete_screen);

next_npc_screen = ConvoScreen:new {

	id = "next_npc_screen",

	leftDialog = "",

	customDialogText = "Seek out Blade Master Kas'im for your next Trial",

	stopConversation = "true",

	options = {
	
		{"As you commnad my lord.", ""}, 

	}

}



initiateQuestDark:addScreen(next_npc_screen);

wrong_screen = ConvoScreen:new {

	id = "wrong_screen",

	leftDialog = "",

	customDialogText = "<A bolt of force lightning knocks you to the ground> Failure is punished severely Acolyte, shall we try again?",

	stopConversation = "false",

	options = {

		{"<You rise to your feet slowly, your body smoking from the attack> I will not..ugh.. fail you again my lord.", "initiate_first_screen"},

		{"My lord, I cannot continue.", "no_screen"}


	

	}

}



initiateQuestDark:addScreen(wrong_screen);

initiate_not_ready = ConvoScreen:new {

	id = "initiate_not_ready",

	leftDialog = "",

	customDialogText = "Leave my sight at once Acolyte, or be punished! I will summon you when I am ready.",

	stopConversation = "true",

	options = {

	}

}



initiateQuestDark:addScreen(initiate_not_ready);

no_screen = ConvoScreen:new {

	id = "no_screen",

	leftDialog = "",

	customDialogText = "Get out! I have no time for games Acolyte.",

	stopConversation = "yes",

	options = {

	--	{"Yes", "initiate_first_screen"},

	--	{"Not right now", "no_screen"}


	

	}

}



initiateQuestDark:addScreen(no_screen);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "You wish to speak?",

	stopConversation = "yes",

	options = {

	}

}

initiateQuestDark:addScreen(hello_screen);

lightsaber_screen = ConvoScreen:new {

	id = "lightsaber_screen",

	leftDialog = "",

	customDialogText = "You lack the proper skills, come back when you have completed Lightsaber and Force nowledge IV training.",

	stopConversation = "yes",

	options = {
	}

}

initiateQuestDark:addScreen(lightsaber_screen);





addConversationTemplate("initiateQuestDark", initiateQuestDark);



initiateQuestDarkTwo = ConvoTemplate:new {

	initialScreen = "first_screen", 

	templateType = "Lua", 

	luaClassHandler = "initiateQuestDarkTwo_handler",

	screens = {}

}
--Intro First

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "I am Lord Kas'im, are you ready to begin your next trial Acolyte?.",

	stopConversation = "false",

	options = {

		{"Lets begin.", "second_screen"},

	}

}



initiateQuestDarkTwo:addScreen(first_screen);

second_screen = ConvoScreen:new {

	id = "second_screen",

	leftDialog = "",

	customDialogText = "This exercise is to prepare you for crafting a Lightsaber. First, can you tell me what all Lightsabers require to activate?",

	stopConversation = "false",

	options = {

		{"a Pearl.", "third_screena"},
		{"a Power Crystal.", "third_screenb"},
		{"a Color Crystal.", "third_screenc"}

	}

}



initiateQuestDarkTwo:addScreen(second_screen);

third_screena = ConvoScreen:new {

	id = "third_screena",

	leftDialog = "",

	customDialogText = "<you balance falters as you are asailed with a Force Mind Blast and you hold your head screeming> Your failure to answer my questions correctly will cause you great pain Acolyte, try again. ",

	stopConversation = "false",

	options = {

		{"a Power Crystal.", "third_screenb"},
		{"a Color Crystal.", "third_screenc"}

	}

}

initiateQuestDarkTwo:addScreen(third_screena);

third_screenb = ConvoScreen:new {

	id = "third_screenb",

	leftDialog = "",

	customDialogText = "<You are lifted off your feet and flung into the nearst wall where you fall to the ground in a crumpled heap> You toy with me Acolyte, try again.",

	stopConversation = "false",

	options = {
		{"a Pearl.", "third_screena"},
		{"a Color Crystal.", "third_screenc"}

	}

}

initiateQuestDarkTwo:addScreen(third_screenb);

third_screenc = ConvoScreen:new {

	id = "third_screenc",

	leftDialog = "",

	customDialogText = "Good. Now, what are the risks of equiping a Lightsaber in public?",

	stopConversation = "false",

	options = {

		{"I will put myself at risk for being attacked by anyone.", "forth_screena"}

	}

}

initiateQuestDarkTwo:addScreen(third_screenc);

forth_screena = ConvoScreen:new {

	id = "forth_screena",

	leftDialog = "",

	customDialogText = "Correct, The Sith are hunted relentlessly. In addition to being attackable, others who spot you with your saber equiped will report you to the Bounty Hunter's Guild putting you at risk of being bounty target. Now, Lightsabers can be enhanced with the useage of Power Crystals and Pearls, but removing these items for your Lightsaber does what?",

	stopConversation = "false",

	options = {

		{"Damages the Lightsaber", "fifth_screena"},
		{"Breaks the Crystal", "fifth_screenb"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestDarkTwo:addScreen(forth_screena);

fifth_screena = ConvoScreen:new {

	id = "fifth_screena",

	leftDialog = "",

	customDialogText = "<Your ability to breathe is slowy taken from you as an invisible hand cruches your neck> Try again!",
	stopConversation = "false",

	options = {

		{"Breaks the Crystal", "fifth_screenb"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestDarkTwo:addScreen(fifth_screena);

fifth_screenb = ConvoScreen:new {

	id = "fifth_screenb",

	leftDialog = "",

	customDialogText = "<you receive a backhand across the face> Once more Acolyte, try to answer correctly this time.",
	stopConversation = "false",

	options = {

		{"Damages the Lightsaber", "fifth_screena"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestDarkTwo:addScreen(fifth_screenb);

fifth_screenc = ConvoScreen:new {

	id = "fifth_screenc",

	leftDialog = "",

	customDialogText = "your knowledge is impressive Acolyte, the removal process damages the finish on the Crystal or Pearl, causing a slight decrease in condition and overall effectiveness.",
	stopConversation = "false",

	options = {

		{"I understand", "sixth_screen"}

	}

}

initiateQuestDarkTwo:addScreen(fifth_screenc);

sixth_screen = ConvoScreen:new {

	id = "sixth_screen",

	leftDialog = "",

	customDialogText = "Lightsabers vary in speed and damage, with the fastest being a one hand saber and the slowest, a polearm. This puts a two hand saber sitting in the middle. You may master the art of all three, but it is wise to select one style that fits you and focus on it first.",
	stopConversation = "false",

	options = {

		{"Yes master.", "seventh_screen"}

	}

}

initiateQuestDarkTwo:addScreen(sixth_screen);

seventh_screen = ConvoScreen:new {

	id = "seventh_screen",

	leftDialog = "",

	customDialogText = "I am going to give you a Color Crystal you may use to construct your first Lightsaber, Ensure that you have two Refined Crystal packs and the required materials to construct your Lightsaber after your next trial is complete.",
	stopConversation = "false",

	options = {

		{"What is my next task?", "final_screen"}

	}

}

initiateQuestDarkTwo:addScreen(seventh_screen);


final_screen = ConvoScreen:new {

	id = "final_screen",

	leftDialog = "",

	customDialogText = "Speak with Lord Mellichae when you are ready, he will instruct you next.",

	stopConversation = "true",

	options = {

	}

}



initiateQuestDarkTwo:addScreen(final_screen);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "What is it?.",

	stopConversation = "true",

	options = {

	}

}



initiateQuestDarkTwo:addScreen(hello_screen);


addConversationTemplate("initiateQuestDarkTwo", initiateQuestDarkTwo);

initiateQuestDarkThree = ConvoTemplate:new {

	initialScreen = "first_screen", 

	templateType = "Lua", 

	luaClassHandler = "initiateQuestDarkThree_handler",

	screens = {}

}

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "I am Lord Mellichae, Keeper of Ancient Sith Knowledge and Artifacts.  The Sith Council has kept a close eye on you, and you have surpassed our expectations.",

	stopConversation = "false",

	options = {
		{"I did not realize the Council was interested in the training of Acolytes.", "screen_1a"},
		{"Lord Zash... I have heard of you.", "screen_1b"},
	}

}
initiateQuestDarkThree:addScreen(first_screen);

screen_1a = ConvoScreen:new {

	id = "screen_1a",

	leftDialog = "",

	customDialogText = "Only those who possess great power and cunning such as yourself young Acolyte. Now, lets get to the task at hand shall we?  Your training is almost complete and your final task is to retreive an holocron belonging to the ancient Sith Lord, Tulak Hord.",

	stopConversation = "false",

	options = {
		{"Where do I find this Holocron?", "screen_3a"}
	}

}
initiateQuestDarkThree:addScreen(screen_1a);

screen_1b = ConvoScreen:new {

	id = "screen_1b",

	leftDialog = "",

	customDialogText = "Yes well, I have lived many centuries over. My studies in ancient Sith lore have stregnthend our order, but at a great cost to me. As strong as I am in the Darkside, my body does not share that affinity. So, I must perform an Ancient Sith Ritual that allows me to possess another Force Sensitives body completely. But do not fret, young Acolyte your body shall remain yours.",

	stopConversation = "false",

	options = {
		{"I'm honored.", "screen_2a"},
	}

}
initiateQuestDarkThree:addScreen(screen_1b);

screen_2a = ConvoScreen:new {

	id = "screen_2a",

	leftDialog = "",

	customDialogText = "You should be, The Council thinks greatly of your abilities .",
	stopConversation = "false",

	options = {
		{"The council, watching me?", "screen_1a"},
	}

}
initiateQuestDarkThree:addScreen(screen_2a);

screen_3a = ConvoScreen:new {

	id = "screen_3a",

	leftDialog = "",

	customDialogText = "West of this Temple you will find what you are looking for..The rest is up too you.",

	stopConversation = "true",

	options = {
	}

}
initiateQuestDarkThree:addScreen(screen_3a);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "May the force serve you well.",

	stopConversation = "true",

	options = {

	}

}
initiateQuestDarkThree:addScreen(hello_screen);

initiate_screen = ConvoScreen:new {

	id = "initiate_screen",

	leftDialog = "",

	customDialogText = "Well done, you have helped to preserve Sith Knowledge by helping me complete my ritual. The rank of Sith Acolyte I now bestow upon you. Allow me to also train you how to Regain Consciousness. This ability can save you from Permadeath. You should see in your experiance window your lives remaining. Using Regain Consciousness costs one of these lives and when you run out, you will be forced to clone into a new body. Proceed with caution, start refining your skills in preparation for your Adept Trials from Darth Vader, and may the Force serve you well.",

	stopConversation = "true",

	options = {

	}

}
initiateQuestDarkThree:addScreen(initiate_screen);

addConversationTemplate("initiateQuestDarkThree", initiateQuestDarkThree);
