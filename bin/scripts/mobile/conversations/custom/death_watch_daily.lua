deathWatchDaily = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "death_watch_daily_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "These mining rats are getting in our way of our operation, can you help?",
	stopConversation = "false",
	options = {
		{"I used to bullseye wromprats, so I think so", "second_screen"},
	}
}
deathWatchDaily:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Kill those rats already",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Well ok then, clean up 10 rats for us.",
	stopConversation = "false",
	options = {
		{"sure", "accept_screen"},
	}
}
deathWatchDaily:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Thanks",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "You done?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
deathWatchDaily:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Great, now we can get back to work. Here are some Minerals we extracted yesterday.",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Come see us tomorrow when we have more minerals.",
	stopConversation = "true",
	options = {}
}
deathWatchDaily:addScreen(tomorrow_screen);

addConversationTemplate("deathWatchDaily", deathWatchDaily);

