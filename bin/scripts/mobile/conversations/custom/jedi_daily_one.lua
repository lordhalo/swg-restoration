jedi_daily_one = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_daily_one_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Thank the Force, you are here!",
	stopConversation = "false",
	options = {
		{"How may I be of service Sentinel Ryashk. ", "second_screen"},
	}
}
jedi_daily_one:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "The council has just reciecved word that one of out newer knights has been seduced by the darkside and they have asked for your assistance in the matter.",
	stopConversation = "false",
	options = {
		{"Tell the council I will not fail.", "accept_screen"},
	}
}
jedi_daily_one:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Remeber, your a Jedi, only use force if necessary, lest you be tempted by the darkside as well.",
	stopConversation = "true",
	options = {}
}
jedi_daily_one:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "You've returned! The decsion to eliminate one of our own must have been hard, but in the end you were given no choice.",
	stopConversation = "false",
	options = {
		{"Taking another's life is never easy, even if the choice to do so is not given.", "complete_screen_final"},
	}
}
jedi_daily_one:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "You are wise beyond your years young one, the council has asked me to give this to you.",
	stopConversation = "true",
	options = {}
}
jedi_daily_one:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Please hurry, this task can not wait.",
	stopConversation = "true",
	options = {}
}
jedi_daily_one:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "QUEST IS DONE",
	stopConversation = "true",
	options = {}
}
jedi_daily_one:addScreen(quest_done);

addConversationTemplate("jedi_daily_one", jedi_daily_one);

