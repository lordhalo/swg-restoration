gcw_frs_recruiter_convo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "gcw_frs_recruiter_convo_handler",
	screens = {}
}
--Intro First
imperial_intro_screen = ConvoScreen:new {
	id = "imperial_intro_screen",
	leftDialog = "",
	customDialogText = "Greetings, Trooper! Your file indicates that you qualify for our elite Stormtrooper regimen.",
	stopConversation = "false",
	options = {
		{"Interesting. Tell me more", "imperial_screen_two"},
		{"I'm quite happy serving the Emperor in my current capacity.", "exit_screen"}
	}
}
gcw_frs_recruiter_convo:addScreen(imperial_intro_screen);

imperial_screen_two = ConvoScreen:new {
	id = "imperial_screen_two",
	leftDialog = "",
	customDialogText = "Sign the datapad here, and you will be signed up to join into the Stormtrooper ranks. You'll learn to be far more formidable in battle, and have access to exclusive gear. But be warned, signing up means you will no longer be able to request a leave of absence from service, and the Rebel terrorists will see you as a far higher value target",
	stopConversation = "false",
	options = {
		{"I'm willing to take that risk. Sign me up, sir", "imperial_complete_screen"},
		{"I'm quite happy serving the Emperor in my current capacity.", "exit_screen"}
	}
}
gcw_frs_recruiter_convo:addScreen(imperial_intro_screen);

imperial_complete_screen = ConvoScreen:new {
	id = "imperial_complete_screen",
	leftDialog = "",
	customDialogText = "Excellent. For the Emperor!",
	stopConversation = "true",
	options = {}
}
gcw_frs_recruiter_convo:addScreen(imperial_complete_screen);

rebel_intro_screen = ConvoScreen:new {
	id = "rebel_intro_screen",
	leftDialog = "",
	customDialogText = "Greetings, Soldier! Your file indicates that you qualify for our SpecForce training program.",
	stopConversation = "false",
	options = {
		{"Interesting. Tell me more", "rebel_screen_two"},
		{"I'm quite happy fighting the Empire in my current capacity.", "exit_screen"}
	}
}
gcw_frs_recruiter_convo:addScreen(rebel_intro_screen);

rebel_screen_two = ConvoScreen:new {
	id = "rebel_screen_two",
	leftDialog = "",
	customDialogText = "Sign the datapad here, and you will accepted into the SpecForce ranks. You'll learn new skills and tactics to help the Rebellion, and have access to the best weapons and armor we have to offer. But be warned, signing up means you will no longer be able to request a leave of absence from service, and the Empire will likely send their best to kill you",
	stopConversation = "false",
	options = {
		{"I'm willing to take that risk. Sign me up, sir", "rebel_complete_screen"},
		{"I'm quite happy fighting the Empire in my current capacity.", "exit_screen"}
	}
}
gcw_frs_recruiter_convo:addScreen(rebel_screen_two);

rebel_complete_screen = ConvoScreen:new {
	id = "rebel_complete_screen",
	leftDialog = "",
	customDialogText = "I'm glad to hear that, Soldier. And may the Force be with you",
	stopConversation = "true",
	options = {}
}
gcw_frs_recruiter_convo:addScreen(rebel_complete_screen);

no_quest_screen = ConvoScreen:new {
	id = "no_quest_screen",
	leftDialog = "",
	customDialogText = "Carry on Soldier",
	stopConversation = "true",
	options = {}
}
gcw_frs_recruiter_convo:addScreen(no_quest_screen);

exit_screen = ConvoScreen:new {
	id = "exit_screen",
	leftDialog = "",
	customDialogText = "Move Along then!",
	stopConversation = "true",
	options = {}
}
gcw_frs_recruiter_convo:addScreen(exit_screen);

addConversationTemplate("gcw_frs_recruiter_convo", gcw_frs_recruiter_convo);

