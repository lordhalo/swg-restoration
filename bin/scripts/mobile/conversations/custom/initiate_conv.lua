initiateQuestLight = ConvoTemplate:new {

	initialScreen = "initiate_first_screen",

	templateType = "Lua",

	luaClassHandler = "initiateQuestLight_handler",

	screens = {}

}





--Intro First

first_screen = ConvoScreen:new {

	id = "initiate_first_screen",

	leftDialog = "",

	customDialogText = "Welcome young one, The time has come for you to began your Initiate Trials",

	stopConversation = "false",

	options = {

		{"I am ready", "initiate_second_screen"},

		{"I don't know that I want to become a Jedi", "fork_fs"}

	

	}

}



initiateQuestLight:addScreen(first_screen);

second_screen = ConvoScreen:new {

	id = "initiate_second_screen",

	leftDialog = "",

	customDialogText = "First, resite the Jedi code with me. There is no emotion.",

	stopConversation = "false",

	options = {

		{"There is calm", "wrong_screen"},

		{"There is peace", "initiate_third_screen"}, 
		
		{"There is quiet", "wrong_screen"}, 

	

	}

}



initiateQuestLight:addScreen(second_screen);

third_screen = ConvoScreen:new {

	id = "initiate_third_screen",

	leftDialog = "",

	customDialogText = "There is no ignorance...",

	stopConversation = "false",

	options = {

		{"There is willpower", "wrong_screen"},

		{"There is serenity", "wrong_screen"}, 
		
		{"There is knowledge", "initiate_forth_screen"}, 

	

	}

}



initiateQuestLight:addScreen(third_screen);

forth_screen = ConvoScreen:new {

	id = "initiate_forth_screen",

	leftDialog = "",

	customDialogText = "There is no passion...",

	stopConversation = "false",

	options = {
	
		{"There is serenity", "initiate_fifth_screen"}, 

		{"There is compassion", "wrong_screen"},
		
		{"There is love", "wrong_screen"}, 

	

	}

}



initiateQuestLight:addScreen(forth_screen);

fifth_screen = ConvoScreen:new {

	id = "initiate_fifth_screen",

	leftDialog = "",

	customDialogText = "There is no chaos...",

	stopConversation = "false",

	options = {
	
		{"There is harmony", "initiate_sixth_screen"}, 

		{"There is order", "wrong_screen"},
		
		{"There is balance", "wrong_screen"}, 

	

	}

}



initiateQuestLight:addScreen(fifth_screen);

sixth_screen = ConvoScreen:new {

	id = "initiate_sixth_screen",

	leftDialog = "",

	customDialogText = "There is no death...",

	stopConversation = "false",

	options = {
	
		{"There is the Force", "initiate_complete_screen"}, 

	}

}



initiateQuestLight:addScreen(sixth_screen);

complete_screen = ConvoScreen:new {

	id = "initiate_complete_screen",

	leftDialog = "",

	customDialogText = "Very good, You have completed your first trial.",

	stopConversation = "false",

	options = {
	
		{"What is my next task?", "next_npc_screen"}, 

	}

}



initiateQuestLight:addScreen(complete_screen);

next_npc_screen = ConvoScreen:new {

	id = "next_npc_screen",

	leftDialog = "",

	customDialogText = "Seek out Xexus for your next Trial",

	stopConversation = "true",

	options = {
	
		{"Thank you Master", ""}, 

	}

}



initiateQuestLight:addScreen(next_npc_screen);

wrong_screen = ConvoScreen:new {

	id = "wrong_screen",

	leftDialog = "",

	customDialogText = "Im sorry, but that is not correct. Would you like to try again?",

	stopConversation = "false",

	options = {

		{"Yes", "initiate_first_screen"},

		{"Not right now", "no_screen"}


	

	}

}



initiateQuestLight:addScreen(wrong_screen);

initiate_not_ready = ConvoScreen:new {

	id = "initiate_not_ready",

	leftDialog = "",

	customDialogText = "You are not ready for the Initiate Trials",

	stopConversation = "true",

	options = {

	}

}



initiateQuestLight:addScreen(initiate_not_ready);

no_screen = ConvoScreen:new {

	id = "no_screen",

	leftDialog = "",

	customDialogText = "Return too me when you are ready.",

	stopConversation = "yes",

	options = {

	--	{"Yes", "initiate_first_screen"},

	--	{"Not right now", "no_screen"}


	

	}

}



initiateQuestLight:addScreen(no_screen);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "Hello.",

	stopConversation = "yes",

	options = {

	}

}

initiateQuestLight:addScreen(hello_screen);

lightsaber_screen = ConvoScreen:new {

	id = "lightsaber_screen",

	leftDialog = "",

	customDialogText = "You must train Lightsaber Knowledge IV and Force Knowledge IV before starting this trial.",

	stopConversation = "yes",

	options = {
	}

}

initiateQuestLight:addScreen(lightsaber_screen);





addConversationTemplate("initiateQuestLight", initiateQuestLight);



initiateQuestLightTwo = ConvoTemplate:new {

	initialScreen = "first_screen", 

	templateType = "Lua", 

	luaClassHandler = "initiateQuestLightTwo_handler",

	screens = {}

}
--Intro First

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "I have been expecting you, this is the second of three trials you have been and will be tasked with.",

	stopConversation = "false",

	options = {

		{"I am ready to begin.", "second_screen"},

	}

}



initiateQuestLightTwo:addScreen(first_screen);

second_screen = ConvoScreen:new {

	id = "second_screen",

	leftDialog = "",

	customDialogText = "This exercise is to prepare you for crafting and respecting a Lightsaber. First, all Lightsabers require what to activate?",

	stopConversation = "false",

	options = {

		{"a Pearl.", "third_screena"},
		{"a Power Crystal.", "third_screenb"},
		{"a Color Crystal.", "third_screenc"}

	}

}



initiateQuestLightTwo:addScreen(second_screen);

third_screena = ConvoScreen:new {

	id = "third_screena",

	leftDialog = "",

	customDialogText = "That is not correct",

	stopConversation = "false",

	options = {

		{"a Power Crystal.", "third_screenb"},
		{"a Color Crystal.", "third_screenc"}

	}

}

initiateQuestLightTwo:addScreen(third_screena);

third_screenb = ConvoScreen:new {

	id = "third_screenb",

	leftDialog = "",

	customDialogText = "That is not correct",

	stopConversation = "false",

	options = {
		{"a Pearl.", "third_screena"},
		{"a Color Crystal.", "third_screenc"}

	}

}

initiateQuestLightTwo:addScreen(third_screenb);

third_screenc = ConvoScreen:new {

	id = "third_screenc",

	leftDialog = "",

	customDialogText = "Correct, What are the risk of equiping a Lightsaber in public?",

	stopConversation = "false",

	options = {

		{"I may be attacked by anyone for any reason?", "forth_screena"}

	}

}

initiateQuestLightTwo:addScreen(third_screenc);

forth_screena = ConvoScreen:new {

	id = "forth_screena",

	leftDialog = "",

	customDialogText = "Correct, The Jedi are hunted relentlessly, In addition to being attackable, Others who spot you with it will report you to the Bounty Hunters putting you at risk of being hunted. Now, Lightsabers can be enhanced with the useage of Power Crystals and Pearls, But removing these items for your Lightsaber does what?",

	stopConversation = "false",

	options = {

		{"Damages the Lightsaber", "fifth_screena"},
		{"Breaks the Crystal", "fifth_screenb"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestLightTwo:addScreen(forth_screena);

fifth_screena = ConvoScreen:new {

	id = "fifth_screena",

	leftDialog = "",

	customDialogText = "That is not correct",
	stopConversation = "false",

	options = {

		{"Breaks the Crystal", "fifth_screenb"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestLightTwo:addScreen(fifth_screena);

fifth_screenb = ConvoScreen:new {

	id = "fifth_screenb",

	leftDialog = "",

	customDialogText = "That is not correct",
	stopConversation = "false",

	options = {

		{"Damages the Lightsaber", "fifth_screena"},
		{"Damages the Crystal", "fifth_screenc"}

	}

}

initiateQuestLightTwo:addScreen(fifth_screenb);

fifth_screenc = ConvoScreen:new {

	id = "fifth_screenc",

	leftDialog = "",

	customDialogText = "Correct, the removal process damages the finish on the Crystal or Pearl, causing a slight decrease in condition and overall effectiveness.",
	stopConversation = "false",

	options = {

		{"I understand", "sixth_screen"}

	}

}

initiateQuestLightTwo:addScreen(fifth_screenc);

sixth_screen = ConvoScreen:new {

	id = "sixth_screen",

	leftDialog = "",

	customDialogText = "Correct, Lightsabers vary in speed and damage, With the fastest being a One hand the slowest being a Polearm, with Two hand sitting in the middle, You may Master the arts of all three but it is wise to select one style that fits you and focus on it first.",
	stopConversation = "false",

	options = {

		{"Yes, Master", "seventh_screen"}

	}

}

initiateQuestLightTwo:addScreen(sixth_screen);

seventh_screen = ConvoScreen:new {

	id = "seventh_screen",

	leftDialog = "",

	customDialogText = "I am going to give you a Color Crystal you may use to construct your first Lightsaber, Ensure that you have two Refined Crystal packs and the required materials to construct your Lightsaber after your next trial is complete.",
	stopConversation = "false",

	options = {

		{"What is my next task?", "final_screen"}

	}

}

initiateQuestLightTwo:addScreen(seventh_screen);


final_screen = ConvoScreen:new {

	id = "final_screen",

	leftDialog = "",

	customDialogText = "Speak with Luke Skywalker when you are ready, he will be waiting for you.",

	stopConversation = "true",

	options = {

	}

}



initiateQuestLightTwo:addScreen(final_screen);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "Hello.",

	stopConversation = "true",

	options = {

	}

}



initiateQuestLightTwo:addScreen(hello_screen);


addConversationTemplate("initiateQuestLightTwo", initiateQuestLightTwo);

initiateQuestLightThree = ConvoTemplate:new {

	initialScreen = "first_screen", 

	templateType = "Lua", 

	luaClassHandler = "initiateQuestLightThree_handler",

	screens = {}

}

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "Hello, Im Luke Skywalker. The Jedi Order has kept a close eye on you, you have surpassed our expectations and have great hopes for your future.",

	stopConversation = "false",

	options = {
		{"Thank you, I hope I meet all your expectations", "screen_1a"},
		{"You're THE Luke Skywalker?!", "screen_1b"},
	}

}
initiateQuestLightThree:addScreen(first_screen);

screen_1a = ConvoScreen:new {

	id = "screen_1a",

	leftDialog = "",

	customDialogText = "Likewise. You have completed all the task required of you and are ready to become a Jedi, Your sole remaining task is to acquire a Holocron of Knowledge.",

	stopConversation = "false",

	options = {
		{"Where do I find this Holocron?", "screen_3a"}
	}

}
initiateQuestLightThree:addScreen(screen_1a);

screen_1b = ConvoScreen:new {

	id = "screen_1b",

	leftDialog = "",

	customDialogText = "Yes I am, I helped Destroy the Deathstar. Son of Anakin Skywalker, Currently known as Darth Vadar. Blinded by the Dark side and the Emperors mind games, Fearing for my Mothers life he betrayed the Jedi Order setting him down a dark path, but I still have hope.",

	stopConversation = "false",

	options = {
		{"How did you get here?", "screen_2a"},
	}

}
initiateQuestLightThree:addScreen(screen_1b);

screen_2a = ConvoScreen:new {

	id = "screen_2a",

	leftDialog = "",

	customDialogText = "When my Mother died, Obi-wan watched over me and began to teach me about the Force. After the Destruction of the Death star, I was sent here to complete my training and became a Jedi Knight.",
	stopConversation = "false",

	options = {
		{"I look forward to fighting by your side.", "screen_1a"},
	}

}
initiateQuestLightThree:addScreen(screen_2a);

screen_3a = ConvoScreen:new {

	id = "screen_3a",

	leftDialog = "",

	customDialogText = "West of this Enclave you will find what you are looking for..The rest is up too you.",

	stopConversation = "true",

	options = {
	}

}
initiateQuestLightThree:addScreen(screen_3a);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "Hello.",

	stopConversation = "true",

	options = {

	}

}
initiateQuestLightThree:addScreen(hello_screen);

initiate_screen = ConvoScreen:new {

	id = "initiate_screen",

	leftDialog = "",

	customDialogText = "You have done well, I am proud to see you become a Jedi, With it you have learned Regain Consciousness, The only ability that can save you from Permadeath. You should see in your experiance window your lifes remaining, Using Regain Consciousness cost one of these lifes and when you run out, you will be forced to Clone into a new body. Proceed with caution, Start refining your skills in preparation for the Padawan Trials from Obi-wan, and may the Force be with you.",

	stopConversation = "true",

	options = {

	}

}
initiateQuestLightThree:addScreen(initiate_screen);

addConversationTemplate("initiateQuestLightThree", initiateQuestLightThree);
