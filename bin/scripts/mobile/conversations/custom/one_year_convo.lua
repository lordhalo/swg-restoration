oneYearConvo = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "one_year_convo_handler",
	screens = {}

}
--Intro Frst
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hello! September 19th we celibrate 1 Year of Restoration, I want to personally thank everyone who has been here to support the server. I look forward to more years of Restoration, as a token of my graditude. Please take this gift. ",
	stopConversation = "false",
	options = {
		{"Thanks!", "second_screen"}
	}
}
oneYearConvo:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "See you next year!",
	stopConversation = "true",
	options = {}
}
oneYearConvo:addScreen(second_screen);


complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Sorry, only one!",
	stopConversation = "true",
	options = {}
}
oneYearConvo:addScreen(complete_screen);

addConversationTemplate("oneYearConvo", oneYearConvo);
