scavenger_convo = ConvoTemplate:new {
	initialScreen = "first_screen",
	templateType = "Lua",
	luaClassHandler = "scavenger_convo_handler",
	screens = {}
}


--Intro First
first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "If you arnt here to help go away! Half of us went mad when we picked up where the imperials left off.",
	stopConversation = "true",
	options = {
		--{"ok sure.", "scavenger_accepted_screen"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

scavenger_convo:addScreen(first_screen);

addConversationTemplate("scavenger_convo", scavenger_convo);

