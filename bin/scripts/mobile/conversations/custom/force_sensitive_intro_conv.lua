--[[

Main characters:

Jedi Master Thiel:
Thiel is new to the Village, he mainly serves to give the starting players history about what's going on and gear you towards progressing as a Jedi and the training.

Jedi Master La'Guo:
La'Guo serves only the purpose to ensure they know everything about the new sabers and the crystals.

Paemos:
Paemos gives you more history about the Force and that all Jedi are connected. He will give you knowledge about Force Skills.

Captain Sarguillo:
Sarg. will give you more knowledge about the defender skill

Noldan:
Noldan will talk about general Jedi skills, covering anything not covered by prior trainers.

Rohak:
Rohak will be used to let the player know that they need to do, what their new purpose is.

Mellichae:
His plays the role of the main evil, he will attempt to stop the player.


Pre Plot:
You are a Force Sensitive brought to Dathomir to learn about the Jedi way, you are fresh and new to this way of life and they must show you the way.

Main plot:
You will go through all the training from the mentors at the Village, you will learn that an evil has presented itself to you and that you must do something about it.
You will find your purpose and learn that you must leave the village to save the village and the Jedi.

End Plot:
upon leaving the village you will be presented a chance to become dark, exile, or light. where-ever you go afterwards when you do decide to join a team you will begin your next story.


]]

force_sensitive_intro_conv = ConvoTemplate:new {
	initialScreen = "intro_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_convo_handler",
	screens = {}
}


--Intro First
intro_first_screen = ConvoScreen:new {
	id = "intro_first_screen",
	leftDialog = "",
	customDialogText = "Hello there! I see you're our newest arrival. Allow me to be the first to introduce myself and welcome you to our Enclave. I am Jedi Master Janakus... Are you ready to get started?",
	stopConversation = "false",
	options = {
		{"Yes, I have been waiting for this moment for a long time now.", "thiel2"},
		{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

force_sensitive_intro_conv:addScreen(intro_first_screen);

--deny
deny = ConvoScreen:new {
	id = "deny",
	leftDialog = "",
	customDialogText = "Just come back when you're ready.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(deny);

--thiel2
thiel2 = ConvoScreen:new {
	id = "thiel2",
	leftDialog = "",
	customDialogText = "Great! There is going to be a lot of information given to you during your time with the mentors. We are here to give you all the insight you need to become a sucessful Jedi, becoming a Jedi is not easy and it's not an easy life when you become one either. Are you sure you wish to continue this path?",
	stopConversation = "false",
	options = {
		{"Yes I am ready and willing to do this.","thiel3"},
		{"You're right, I am not ready for this.","deny"}
	}
}

force_sensitive_intro_conv:addScreen(thiel2);

--thiel3
thiel3 = ConvoScreen:new {
	id = "thiel3",
	leftDialog = "",
	customDialogText = "Now that you understand what your journey is about to be, I am going to explain what you will be doing here. We have gathered some of our best from the Jedi Order to come here and teach all of our new Force sensitives, when you go up to each mentor they will specificly bring up their area of expertise.",
	stopConversation = "false",
	options = {
		{"I understand completely, please continue.","thiel4"},
	}
}

force_sensitive_intro_conv:addScreen(thiel3);

--thiel4
thiel4 = ConvoScreen:new {
	id = "thiel4",
	leftDialog = "",
	customDialogText = "When you are here you will learn how to survive on your own, we will not cradle you and you will be at your own risk if you leave the enclave... we will not stop you.",
	stopConversation = "false",
	options = {
		{"Is there anything else I need to know?","thiel5"},
	}
}

force_sensitive_intro_conv:addScreen(thiel4);

--thiel5
thiel5 = ConvoScreen:new {
	id = "thiel5",
	leftDialog = "",
	customDialogText = "I will provide you with the basic tools for your success here, and a 'Mentor's Crystal' it will give you guidence if you feel lost, and it will be needed to complete your training. ",
	stopConversation = "false",
	options = {
		{"Alright so I use the crystal after completing my training from each mentor, or if I feel lost.","thiel6"},
	}
}

force_sensitive_intro_conv:addScreen(thiel5);

--thiel6
thiel6 = ConvoScreen:new {
	id = "thiel6",
	leftDialog = "",
	customDialogText = "Correct, now before I let you go I must warn you about what is going on in the galaxy. Us Jedi are under constant attack by the Dark side, Bounty Hunters and commoners who hate Jedi. Its very important that outside this Enclave you remain hidden, For if anyone sees you using Force Powers or holding a Lightsaber they might attack, Or report you to the Bounty Hunters.",
	stopConversation = "false",
	options = {
		{"I understand the dangers, I will remember this.","thielFinal"},
	}
}

force_sensitive_intro_conv:addScreen(thiel6);

--thielFinal
thielFinal = ConvoScreen:new {
	id = "thielFinal",
	leftDialog = "",
	customDialogText = "Good, here the tools you need. Now go speak with Captain Sarguillo. May the Force be with you.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(thielFinal);


--complete
complete = ConvoScreen:new {
	id = "complete",
	leftDialog = "",
	customDialogText = "You need not speak to me anymore, if you haven't spoken to Captain Sarguillo I'd suggest you do that.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(complete);


--not yet
not_yet = ConvoScreen:new {
	id = "not_yet",
	leftDialog = "",
	customDialogText = "You're just not ready.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(not_yet);

crystal_screen = ConvoScreen:new {
	id = "crystal_screen",
	leftDialog = "",
	customDialogText = "Did you really forget to use your Crystal?",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(crystal_screen);

return_screen = ConvoScreen:new {
	id = "return_screen",
	leftDialog = "",
	customDialogText = "Its been wonderful to watch your progress, you are almost ready to become a Jedi Initiate. There are a few things you need to do before the Initiate trials begin, are you ready?",
	stopConversation = "false",
	options = {
		{"I am ready.","return_screen2"},
		{"I was born for this.","return_screen2"},
	}
}

force_sensitive_intro_conv:addScreen(return_screen);

return_screen2 = ConvoScreen:new {
	id = "return_screen2",
	leftDialog = "",
	customDialogText = "Outside the Enclave we keep Practice droids for new recruits to train on, One task you have yet to learn is training a skill. You need to Practice on the droids until you have enough experience to learn Force Knowledge IV. Then Jedi Max Seran will expect you to have all the required components for a Lightsaber so I suggest you get Lightsaber Knowledge IV before speaking with him.",
	stopConversation = "false",
	options = {
		{"I will do my best, Master","fs_done"}
	}
}

force_sensitive_intro_conv:addScreen(return_screen2);

fs_done = ConvoScreen:new {
	id = "fs_done",
	leftDialog = "",
	customDialogText = "May the Force be with you, I suspect next time we speak you will be a Jedi.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_intro_conv:addScreen(fs_done);


addConversationTemplate("force_sensitive_intro_conv", force_sensitive_intro_conv);

--####################################################################		Captain Sarguillo	##########################################################################




force_sensitive_defense_conv = ConvoTemplate:new {
	initialScreen = "defense_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_defense_conv_handler",
	screens = {}
}


--Def First
defense_first_screen = ConvoScreen:new {
	id = "defense_first_screen",
	leftDialog = "",
	customDialogText = "Hello, I am Captain Sarguillo, Master of Defense and I will be teacing you how to defend yourself. Are you ready?",
	stopConversation = "false",
	options = {
		{"Yes I am ready.","def1"},
		{"What brought you here?.","info1"},
		{"At this time I am not ready.","defense_deny"}
	}
}
force_sensitive_defense_conv:addScreen(defense_first_screen);

--defense deny
defense_deny = ConvoScreen:new {
	id = "defense_deny",
	leftDialog = "",
	customDialogText = "I understand, not everyone is equiped to learn at such a fast rate. Come back when you're ready.",
	stopConversation = "true",
	options = {}
}
force_sensitive_defense_conv:addScreen(defense_deny);

info1 = ConvoScreen:new {
	id = "info1",
	leftDialog = "",
	customDialogText = "I was once in charge of the protection at the village of Aurilia, After Order 66 I assisted the Jedi during their relocation here. Unforiantly during my absence the village of Aurilia came under siege. Now I have Permanently found residence here, training new recruits and fortifying this Enclave.",
	stopConversation = "false",
	options = {
		{"Are you a Force user?","info2"}	
	}
}
force_sensitive_defense_conv:addScreen(info1);

info2 = ConvoScreen:new {
	id = "info2",
	leftDialog = "",
	customDialogText = "I am a Master of Defense, The Force is one of my primary shields. Are you ready to learn from me?",
	stopConversation = "false",
	options = {
		{"Yes I am.","def1"}	
	}
}
force_sensitive_defense_conv:addScreen(info2);


--def1
def1 = ConvoScreen:new {
	id = "def1",
	leftDialog = "",
	customDialogText = "Right now you are a Jedi without your Lightsaber, what other weapons do you have?",
	stopConversation = "false",
	options = {
	{"I have the Force!","def2"}
	}
}
force_sensitive_defense_conv:addScreen(def1);


--def2
def2 = ConvoScreen:new {
	id = "def2",
	leftDialog = "",
	customDialogText = "At least you understand that the Force can be a powerful ally. You will be wise to understand that the Force cannot protect you in all aspects, and to prepare your body for what the Force lacks.",
	stopConversation = "false",
	options = {
	{"I believe I understand, so... I must train my body as well as my knowledge with the Force?","def3"}
	}
}
force_sensitive_defense_conv:addScreen(def2);


--def3
def3 = ConvoScreen:new {
	id = "def3",
	leftDialog = "",
	customDialogText = "You understand correctly, but you must train yourself at close range combat and ranged combat! They are two completely different areas in defense, you will do yourself a favor by being an expert in those areas. If you're not prepared to defend yourself where the Force cannot then you will suffer greatly.",
	stopConversation = "false",
	options = {
	{"So I must learn to be without the Force, as well as being with it. I understand.","def4"}
	}
}
force_sensitive_defense_conv:addScreen(def3);

--def4
def4 = ConvoScreen:new {
	id = "def4",
	leftDialog = "",
	customDialogText = "If your enemies find you weakened and your not able to draw on the Force you very well could be destroyed. If you train yourself as we spoke... You will be able to overcome that.",
	stopConversation = "false",
	options = {
	{"So as a Jedi I have a greater responsibility to protect myself with and without the Force, as my enemies rely on me only drawing on the Force for protection.","def5"}
	}
}
force_sensitive_defense_conv:addScreen(def4);

--def5
def5 = ConvoScreen:new {
	id = "def5",
	leftDialog = "",
	customDialogText = "You catch on quick. Practice what we have spoken about, and I assure you it will save your life one day.",
	stopConversation = "false",
	options = {
	{"Thank Captain, what do I do now?","def6"}
	}
}
force_sensitive_defense_conv:addScreen(def5);

--def6
def6 = ConvoScreen:new {
	id = "def6",
	leftDialog = "",
	customDialogText = "Defeat this droid, As the droid teases you be mindful and eventually you will learn to deflect blasters and swings.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_defense_conv:addScreen(def6);

--defFinal
defFinalOne = ConvoScreen:new {
	id = "defFinalOne",
	leftDialog = "",
	customDialogText = "You did well, Could you feel the Force guide you?",
	stopConversation = "false",
	options = {
	{"I did feel something.","defFinal"}
	}
}
force_sensitive_defense_conv:addScreen(defFinalOne);

--defFinal
defFinal = ConvoScreen:new {
	id = "defFinal",
	leftDialog = "",
	customDialogText = "Good, In time you will learn to control those feelings. I have updated your Mentor's Crystal... Go seek out Jedi Master La'Guo.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_defense_conv:addScreen(defFinal);

--complete
complete = ConvoScreen:new {
	id = "complete",
	leftDialog = "",
	customDialogText = "You have already completed my training, I'd suggest seeking out Jedi Master La'Guo.",
	stopConversation = "true",
	options = {}
}
force_sensitive_defense_conv:addScreen(complete);

complete2 = ConvoScreen:new {
	id = "complete2",
	leftDialog = "",
	customDialogText = "You are growing strong with the Force.",
	stopConversation = "true",
	options = {}
}
force_sensitive_defense_conv:addScreen(complete2);

--notyet
not_yet = ConvoScreen:new {
	id = "not_yet",
	leftDialog = "",
	customDialogText = "I cannot be speaking to you at this time. You're not ready for my training.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_defense_conv:addScreen(not_yet);

def_part2 = ConvoScreen:new {
	id = "def_part2",
	leftDialog = "",
	customDialogText = "Welcome back, I see your training is going well.",
	stopConversation = "false",
	options = {
		{"Thank you Master im really getting the hang of things.","def_part3"},
		{"I am trying Master, I know there is much more to learn","def_part3"}
	}
}
force_sensitive_defense_conv:addScreen(def_part2);

def_part2_crystal = ConvoScreen:new {
	id = "def_part2_crystal",
	leftDialog = "",
	customDialogText = "You need to use your Mentor Crystal before we continue.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_defense_conv:addScreen(def_part2_crystal);

def_part3 = ConvoScreen:new {
	id = "def_part3",
	leftDialog = "",
	customDialogText = "You have learned enough to recieve your first intermediate task, Inside the Enclave you will meet Aulun who will practice combat with you before sending you outside on a mission. Before you go, I brief you Force Focus, as you keep Force Focus active you will recieve less damage from all sources, At first a little but later a considerable amount of damage will be mitigated that can make the difference between life in death. ",
	stopConversation = "false",
	options = {
		{"I understand, I will do my best to stay Focused","def_part4"}
	}
}
force_sensitive_defense_conv:addScreen(def_part3);

def_part4 = ConvoScreen:new {
	id = "def_part4",
	leftDialog = "",
	customDialogText = "Now use your Mentor Crystal, then meet with Aulun.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_defense_conv:addScreen(def_part4);

--[[
--
 = ConvoScreen:new {
	id = "",
	leftDialog = "",
	customDialogText = "",
	stopConversation = "",
	options = {}
}
force_sensitive_defense_conv:addScreen();
]]
addConversationTemplate("force_sensitive_defense_conv", force_sensitive_defense_conv);




--#############################################			laguo				#################################

force_sensitive_craft_conv = ConvoTemplate:new {
	initialScreen = "craft_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_craft_conv_handler",
	screens = {}
}


--First
craft_first_screen = ConvoScreen:new {
	id = "craft_first_screen",
	leftDialog = "",
	customDialogText = "I am Jedi Master La'Guo, I am going to teach you everything there is to know about Lightsabers. These are complex tools, and weapons... Are you ready for this portion in your training?",
	stopConversation = "false",
	options = {
		{"I am pleased to meet you master. I am ready to begin.","craft1"},
		{"This sounds intense, I feel I am not ready at this time master.","craft_deny"}	
	}
}

force_sensitive_craft_conv:addScreen(craft_first_screen);

--Craft deny
craft_deny = ConvoScreen:new {
	id = "craft_deny",
	leftDialog = "",
	customDialogText = "Don't keep me waiting.",
	stopConversation = "true",
	options = {}
}
force_sensitive_craft_conv:addScreen(craft_deny);

--craft1
craft1 = ConvoScreen:new {
	id = "craft1",
	leftDialog = "",
	customDialogText = "The Lightsaber is a very valuable weapon when you learn how to properly use it. Unfortuantly in the galaxy we live in today, using a Lightsaber puts you at risk to being attacked.",
	stopConversation = "false",
	options = {
	{"So I must be cautious about using my Lightsaber, and only use it around those I can trust.","craft2"}

	}
}
force_sensitive_craft_conv:addScreen(craft1);

--craft2
craft2 = ConvoScreen:new {
	id = "craft2",
	leftDialog = "",
	customDialogText = "Correct, now you must also learn about how a Lightsaber functions. It requires a tuned color crystal that you tuned yourself, and as you improve your Lightsabers you will be able to hold Krayt Dragon Pearls, and Power Crystals in them. Those also need to be tuned by you.",
	stopConversation = "false",
	options = {
	{"So I must obtain these items, and tune them for my Lightsaber only.","craft3"}

	}
}
force_sensitive_craft_conv:addScreen(craft2);

--craft3
craft3 = ConvoScreen:new {
	id = "craft3",
	leftDialog = "",
	customDialogText = "You must also note that pulling them out of your saber will cause them to take damage, and they will not be as they once were. You see the pearls and crystals tune themselves to the Lightsaber and when you remove them... they lose perfection.",
	stopConversation = "false",
	options = {
	{"The pearls and crystals are almost uniquely made for each lightsaber, and lose their value when I attempt to put them in another saber.","craftFinal"}

	}
}
force_sensitive_craft_conv:addScreen(craft3);

--craftFinal
craftFinal = ConvoScreen:new {
	id = "craftFinal",
	leftDialog = "",
	customDialogText = "Correct, Remember all I have taught you, and you will become a wise and powerful Jedi. Go seek out Ithah.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_craft_conv:addScreen(craftFinal);

--Complete
complete = ConvoScreen:new {
	id = "complete",
	leftDialog = "",
	customDialogText = "There is nothing more you can learn from me at this time. Go seek out Paemos.",
	stopConversation = "true",
	options = {}
}
force_sensitive_craft_conv:addScreen(complete);


addConversationTemplate("force_sensitive_craft_conv", force_sensitive_craft_conv);




--#######################################################				paemos						################################

force_sensitive_forced_conv = ConvoTemplate:new {
	initialScreen = "forced_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_forced_conv_handler",
	screens = {}
}


--First
forced_first_screen = ConvoScreen:new {
	id = "forced_first_screen",
	leftDialog = "",
	customDialogText = "I am Ithah, you must be the one Jedi Master Thiel was talking about. Lets begin, Being able to defend yourself from the Force is as valuable as wielding it. I am ready to teach you if you're ready to learn.",
	stopConversation = "false",
	options = {
		{"I'm ready to learn.","forced_continue"},
		{"I will be back when I am ready.","forced_deny"}	
	}
}
force_sensitive_forced_conv:addScreen(forced_first_screen);


--Cont
forced_continue = ConvoScreen:new {
	id = "forced_continue",
	leftDialog = "",
	customDialogText = "Once a great Jedi Master told me... Control is internal. It is the Jedi's ability to recognize the Force in himself and to use it to his benefit ...Sense involves the next step, in which the Jedi recognizes the force in the universe outside herself. Alter is the third and most difficult area to master, for it involves the student's ability to modify the Force and redistribute its energies.",
	stopConversation = "",
	options = {
		{"I will study these basic practices.","forced_test"},
	}
}
force_sensitive_forced_conv:addScreen(forced_continue);


--Test
forced_test = ConvoScreen:new {
	id = "forced_test",
	leftDialog = "",
	customDialogText = "A Jedi can train themselves to draw on the Force to heal themselves and the others around them. Healing is one of the basic requirements to keep yourself alive and in the fight!",
	stopConversation = "false",
	options = {
		{"So for my basic survivability I would need to be able to draw on the Force to heal myself.","forced1"}	
	}
}
force_sensitive_forced_conv:addScreen(forced_test);


--Deny
forced_deny = ConvoScreen:new {
	id = "forced_deny",
	leftDialog = "",
	customDialogText = "Don't keep me waiting.",
	stopConversation = "true",
	options = {}
}
force_sensitive_forced_conv:addScreen(forced_deny);

--forced1
forced1 = ConvoScreen:new {
	id = "forced1",
	leftDialog = "",
	customDialogText = "There are several ways to heal yourself, or others... But there are two main arts of healing, you have Internal healing and Supportive healing. Internal healing is used for yourself, and Supportive is for others. You will learn how to heal your mind, health, and action. You can focus the healing to heal the damage, or wounds. You could also unlock the secrets to being able to heal yourself over time for a duration.",
	stopConversation = "false",
	options = {
	{"This is a lot to remember, but I understand that healing is very important and I can use it for not only myself but for others.","forced_complete"}
	}
}
force_sensitive_forced_conv:addScreen(forced1);


--Comp
forced_complete = ConvoScreen:new {
	id = "forced_complete",
	leftDialog = "",
	customDialogText = "Great, you were paying attention. You will need to learn how to alter the Force first, then you will be able to manipulate the Force for yourself. Go and study with your Force crystal, and when you're ready for more training go talk to Joki he is awaiting your arrival.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_forced_conv:addScreen(forced_complete);


--N Y
forced_not_yet = ConvoScreen:new {
	id = "forced_not_yet",
	leftDialog = "",
	customDialogText = "You're not yet ready for my training youngling you shouldn't be wondering around these halls.",
	stopConversation = "true",
	options = {}
}
force_sensitive_forced_conv:addScreen(forced_not_yet);


--T Y
forced_thank_you = ConvoScreen:new {
	id = "forced_thank_you",
	leftDialog = "",
	customDialogText = "Go seek out Joki if you haven't already.",
	stopConversation = "true",
	options = {}
}
force_sensitive_forced_conv:addScreen(forced_thank_you);


addConversationTemplate("force_sensitive_forced_conv", force_sensitive_forced_conv);






--####################                                  NOLDAN				########

force_sensitive_force_conv = ConvoTemplate:new {
	initialScreen = "force_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_force_conv_handler",
	screens = {}
}


--First
force_first_screen = ConvoScreen:new {
	id = "force_first_screen",
	leftDialog = "",
	customDialogText = "Ah well it appears you finally arrive, I am Joki. I am here to teach you about the Force. Ithah should have told you the three basic practices of the Force, are you ready to learn more?",
	stopConversation = "false",
	options = {
		{"Yes I am ready to learn more.","force_continue"},
		{"I'll be back later I need to study more.","force_deny"}	
	}
}
force_sensitive_force_conv:addScreen(force_first_screen);


--Cont
force_continue = ConvoScreen:new {
	id = "force_continue",
	leftDialog = "",
	customDialogText = "Clear your mind, Absorb the Force from around you. Channel its energy into you, Focus.",
	stopConversation = "false",
	options = {
		{"I can feel it!.","force1"},
	}
}
force_sensitive_force_conv:addScreen(force_continue);


--force1
force1 = ConvoScreen:new {
	id = "force1",
	leftDialog = "",
	customDialogText = "Good, Good. Now once more allow the force to flow through you. Once you master this training, You will have to learn to manage and control the Force around you. How much Force you currently have control of is displayed on the Force Bar. Once you have exhausted this bar, You will no longer be able to use the Force until you have regenerated.",
	stopConversation = "false",
	options = {
		{"I understand. I am ready to manipulate the Force.","force2"}
	}
}
force_sensitive_force_conv:addScreen(force1);

--Deny
force_deny = ConvoScreen:new {
	id = "force_deny",
	leftDialog = "",
	customDialogText = "Don't keep me waiting.",
	stopConversation = "true",
	options = {}
}
force_sensitive_force_conv:addScreen(force_deny);


--Comp
force2 = ConvoScreen:new {
	id = "force2",
	leftDialog = "",
	customDialogText = "Your connection to the Force will progress with your growing knowledge of it. Take a moment and absorb the Force from your Crystal. Speak with me after.",
	stopConversation = "true",
	options = {}
}
force_sensitive_force_conv:addScreen(force2);

force_part2 = ConvoScreen:new {
	id = "force_part2",
	leftDialog = "",
	customDialogText = "You are now ready to control the Force, At this time you will exhaust yourself very quickly, But in time the Force will be your most powerful ally.",
	stopConversation = "false",
	options = {
		{"I feel I can attack with the Force now master.","force_part3"}
	}
}
force_sensitive_force_conv:addScreen(force_part2);

force_part3 = ConvoScreen:new {
	id = "force_part3",
	leftDialog = "",
	customDialogText = "Very good, Manipulating the Force to put down your enemies can be as powerful as wielding a light saber with enough practice.",
	stopConversation = "false",
	options = {
		{"I think I am more interested in Lightsaber training","force_part4a"},
		{"That sounds great, I don't care for hand to hand combat","force_part4b"}
	}
}
force_sensitive_force_conv:addScreen(force_part3);

force_part4a = ConvoScreen:new {
	id = "force_part4a",
	leftDialog = "",
	customDialogText = "That is understandable, After you have learned these basics you may choose to focus only on Lightsaber training but for now you must finish this task, Use your Crystal and you can be on your way back to Sarguillo.",
	stopConversation = "false",
	options = {
		{"Thank you Master.","force_part5"}
	}
}
force_sensitive_force_conv:addScreen(force_part4a);

force_part4b = ConvoScreen:new {
	id = "force_part4b",
	leftDialog = "",
	customDialogText = "You must still learn to defend yourself if the situation arises, Sarguillo will help with that, Now use your Crystal and you can be on your way.",
	stopConversation = "false",
	options = {
		{"I understand, I will speak with Capitain Sarguillo next.","force_part5"}
	}
}
force_sensitive_force_conv:addScreen(force_part4b);

force_part5 = ConvoScreen:new {
	id = "force_part5",
	leftDialog = "",
	customDialogText = "You will make a fine Jedi, I look forward to watching you grow.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_force_conv:addScreen(force_part5);



--N Y
force_not_yet = ConvoScreen:new {
	id = "force_not_yet",
	leftDialog = "",
	customDialogText = "I am busy right now youngling. Come back later.",
	stopConversation = "true",
	options = {}
}
force_sensitive_force_conv:addScreen(force_not_yet);


--T Y
force_thank_you = ConvoScreen:new {
	id = "force_thank_you",
	leftDialog = "",
	customDialogText = "Use your Mentors crystal then speak with me again.",
	stopConversation = "true",
	options = {}
}
force_sensitive_force_conv:addScreen(force_thank_you);



addConversationTemplate("force_sensitive_force_conv", force_sensitive_force_conv);



--################################################################################################################################################################################################################################
--######################################################################### ROHAK #######################################################################################################################



force_sensitive_rohak_conv = ConvoTemplate:new {
	initialScreen = "rohak_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_rohak_conv_handler",
	screens = {}
}


--First
rohak_first_screen = ConvoScreen:new {
	id = "rohak_first_screen",
	leftDialog = "",
	customDialogText = "I am the village elder Rohak, I am glad to finally meet you. I am here to tell you what you will see once you leave our village. There are three seperate Jedi factions vying for power over the other. The Light Jedi Order, the Dark Jedi Order, and these Jedi that call themselves the Exiles. You will do well to educate yourself about them all.",
	stopConversation = "false",
	options = {
		{"I will see what I can do.","rohak_complete"}	
	}
}
force_sensitive_rohak_conv:addScreen(rohak_first_screen);


--cont
rohak_continue = ConvoScreen:new {
	id = "rohak_continue",
	leftDialog = "",
	customDialogText = "Later I will discuss this with you, but we need you to become more in tuned with your Force training, come back when you're a Jedi Initiate. There are plenty of Sith Shadows out there that need to be taken care of, strike true and May the Force be with you.",
	stopConversation = "false",
	options = {
		{"Yes Rohak, May the Force be with you.","rohak_complete"},
	}
}
force_sensitive_rohak_conv:addScreen(rohak_continue);


--Deny
rohak_deny = ConvoScreen:new {
	id = "rohak_deny",
	leftDialog = "",
	customDialogText = "",
	stopConversation = "true",
	options = {}
}
force_sensitive_rohak_conv:addScreen(rohak_deny);


--Comp
rohak_complete = ConvoScreen:new {
	id = "rohak_complete",
	leftDialog = "",
	customDialogText = "May the Force be with you.",
	stopConversation = "true",
	options = {}
}
force_sensitive_rohak_conv:addScreen(rohak_complete);


--N Y
rohak_not_yet = ConvoScreen:new {
	id = "rohak_not_yet",
	leftDialog = "",
	customDialogText = "Right now I'm busy, Noldan will let me know when you're ready to talk.",
	stopConversation = "true",
	options = {}
}
force_sensitive_rohak_conv:addScreen(rohak_not_yet);


--T Y
rohak_thank_you = ConvoScreen:new {
	id = "rohak_thank_you",
	leftDialog = "",
	customDialogText = "May the Force be with you.",
	stopConversation = "true",
	options = {}
}
force_sensitive_rohak_conv:addScreen(rohak_thank_you);


-- Holocron
rohak_holocron = ConvoScreen:new {
	id = "rohak_holocron",
	leftDialog = "",
	customDialogText = "Ah, im glad you have returned I need your help with, The Sith are renlentless and wont stop untill this place is in ruin",
	stopConversation = "false",
	options = {
		{"What can I do to help?","rohak_mell"},
		{"Im getting out of here this is to much!", "fail_rohak"}
	}
}
force_sensitive_rohak_conv:addScreen(rohak_holocron);

--Mellichae
fail_rohak = ConvoScreen:new {
	id = "fail_rohak",
	leftDialog = "",
	customDialogText = "And give up all that you have learned? You would be a prime target for the empire and its bounty hunters just for having visited this place, are you sure thats what you want to do?",
	stopConversation = "false",
	options = {
		{"...No..im sorry master, Im not sure what came over me.","rohak_mell"},
	}
}
force_sensitive_rohak_conv:addScreen(fail_rohak);


--Mellichae
rohak_mell = ConvoScreen:new {
	id = "rohak_mell",
	leftDialog = "",
	customDialogText = "There has been sightings of Mellichae around the village, He has to be planning something to make a apperance outside of his lair.",
	stopConversation = "false",
	options = {
		{"Where do I come in?","take_holo"},--done
		{"Who is Mellichae?", "who_mell"}
	}
}
force_sensitive_rohak_conv:addScreen(rohak_mell);

who_mell = ConvoScreen:new {
	id = "who_mell",
	leftDialog = "",
	customDialogText = "Mellichae was once a a promissing memeber of our Order who fell to the dark side after encountering a unknown sith",
	stopConversation = "false",
	options = {
		{"unknown sith?","sith_who"}--done
	}
}
force_sensitive_rohak_conv:addScreen(who_mell);

sith_who = ConvoScreen:new {
	id = "sith_who",
	leftDialog = "",
	customDialogText = "Yes, Mellichae was sent on a mission to Dantooine in search of ancient Holocrons from the ruined Jedi Temple. During his search he found fragments believed to be of Sith origin, when he was confronted by a masked Sith who incapacitated his companions and drug Mellichae away, When his companions awoke and begain a search, Mellichae confronted them with lightsaber drawn and demanded they leave immediately.",
	stopConversation = "false",
	options = {
		{"What do you think happend?","went_dark"}--done
	}
}
force_sensitive_rohak_conv:addScreen(sith_who);

went_dark = ConvoScreen:new {
	id = "went_dark",
	leftDialog = "",
	customDialogText = "We do not know, but years later he was in command of the small army of Sith Shadows who begain attacking us recently. We can only assume the Sith turned Mellichae to the Dark Side, But now we must investigate his current plans for this place before something terrible happens",
	stopConversation = "false",
	options = {
		{"Im on it, where do I start?","take_holo"}--done
	}
}
force_sensitive_rohak_conv:addScreen(went_dark);


--Take Holo
take_holo = ConvoScreen:new {
	id = "take_holo",
	leftDialog = "",
	customDialogText = "Our scouts have reported commanders placed around the village, its possible you could isolate one and attempt to gain some intell",
	stopConversation = "false",
	options = {
		{"im on it","capture"}--done
	}
}
force_sensitive_rohak_conv:addScreen(take_holo);


--Capture
capture = ConvoScreen:new {
	id = "capture",
	leftDialog = "",
	customDialogText = "Tread lightly, with Mellichae close by we must take extra precautions during our investigation ",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_rohak_conv:addScreen(capture);


--Jedi
jedi = ConvoScreen:new {
	id = "jedi",
	leftDialog = "",
	customDialogText = "I see you have returned, have you brought back any information?",
	stopConversation = "false",
	options = {
		{"I ran into Mellichae, he and his guards attacked me.","jedi2"},
		{"",""},
		{"Greedo shot first!","stupid"}
	}
}
force_sensitive_rohak_conv:addScreen(jedi);

stupid = ConvoScreen:new {
	id = "stupid",
	leftDialog = "",
	customDialogText = "Sarcasum isn't getting us anywhere, care to try again?",
	stopConversation = "false",
	options = {
		{"Fine, as it turns out I ended up defeating Mellichae in a duel","jedi2"},
	}
}
force_sensitive_rohak_conv:addScreen(stupid);

jedi2 = ConvoScreen:new {
	id = "jedi2",
	leftDialog = "",
	customDialogText = "You..You are serious arnt you? Its amazing you made it back alive, what happend?",
	stopConversation = "false",
	options = {
		{"I believe he was in a weakend state, as I aproached the commander who was standing guard I could feel a void in the force.. A sinking feeling the closer I got to the tent.","jedi1"}
	}
}
force_sensitive_rohak_conv:addScreen(jedi2);


--Jedi1
jedi1 = ConvoScreen:new {
	id = "jedi1",
	leftDialog = "",
	customDialogText = "A void in the force you say.. This is very troubling indeed, I will report your findings to our fellow masters but with Mellichae fleeing you have stabilized the area and surpassed our expectations. Its time you join the new Jedi Order to advance your training and protect our galaxie.",
	stopConversation = "false",
	options = {
		{"Me, a Jedi? What a honor. Thank you very much, Master.", "completelight"},
		{"Mellichae.. He told me to join him.","completedark"},
		{"After all this, im not sure I want anything to do with the Jedi.","completegrey"},
		{"I dont want to join the Jedi or the Sith, what are my options?", "quit"}
	}
}
force_sensitive_rohak_conv:addScreen(jedi1);

completedark = ConvoScreen:new {
	id = "completedark",
	leftDialog = "",
	customDialogText = "And you considered it? Do not allow the Dark side to tempt you, what ever he offered will only load to pain and suffering.",
	stopConversation = "false",
	options = {
		{"Parhaphs.", "completedark2"},
		{"You are right.. I need to find peace again, What should I do?","completesublight"}
	}
}
force_sensitive_rohak_conv:addScreen(completedark);

quit = ConvoScreen:new {
	id = "quit",
	leftDialog = "",
	customDialogText = "Your options our limited, the empire will hunt you just because you are force sensitive, I suggest you learn to control your senses so you can defend yourself if the situation arises.",
	stopConversation = "false",
	options = {
		{"Ok.", "leavejedi"}
	}
}
force_sensitive_rohak_conv:addScreen(quit);

leavejedi = ConvoScreen:new {
	id = "leavejedi",
	leftDialog = "",
	customDialogText = "Take this, use it. blahblah",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_rohak_conv:addScreen(leavejedi);

completesublight = ConvoScreen:new {
	id = "completesublight",
	leftDialog = "",
	customDialogText = "Your encounter with Mellichae must have been difficult, but you are stronger then him.",
	stopConversation = "false",
	options = {
		{"Parhaphs.", "completelight"}
	}
}
force_sensitive_rohak_conv:addScreen(completesublight);

completelight = ConvoScreen:new {
	id = "completelight",
	leftDialog = "",
	customDialogText = "somethingsomething rebuilding jedi order on something planet.",
	stopConversation = "false",
	options = {
		{"thanks and stuff ill go try and find it.", "completelight"}
	}
}
force_sensitive_rohak_conv:addScreen(completelight);

completedark2 = ConvoScreen:new {
	id = "completedark2",
	leftDialog = "",
	customDialogText = "Parhaphs?..oh my friend, I fear you have started down a Dark path, Do not allow this Darkness to overtake you.",
	stopConversation = "false",
	options = {
		{"The Darkness empowers me, The Light holds me back.. I am going to seek out Mellichae.", "completedark3"},
		{"There is no Darkness, there is no Light..there is only the Force","completegrey"}
	}
}
force_sensitive_rohak_conv:addScreen(completedark2);

completedark3 = ConvoScreen:new {
	id = "completedark3",
	leftDialog = "",
	customDialogText = "Leave this place.",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_rohak_conv:addScreen(completedark3);


addConversationTemplate("force_sensitive_rohak_conv", force_sensitive_rohak_conv);


--[[

--
 = ConvoScreen:new {
	id = "",
	leftDialog = "",
	customDialogText = "",
	stopConversation = "false",
	options = {
		{"",""},
		{"",""}
	}
}
force_sensitive_rohak_conv:addScreen();

]]

--###############################################					Mellichae				##############################################--

--[[

--
 = ConvoScreen:new {
	id = "",
	leftDialog = "",
	customDialogText = "",
	stopConversation = "false",
	options = {
		{"",""},
		{"",""}
	}
}
force_sensitive_mel_conv:addScreen();

]]

force_sensitive_mel_conv = ConvoTemplate:new {
	initialScreen = "mel_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_mel_conv_handler",
	screens = {}
}

--Not yet
not_yet = ConvoScreen:new {
	id = "not_yet",
	leftDialog = "",
	customDialogText = "Step away from me or suffer my wraith.",
	stopConversation = "true",
	options = {

	}
}
force_sensitive_mel_conv:addScreen(not_yet);


--First Screen
mel_first_screen = ConvoScreen:new {
	id = "mel_first_screen",
	leftDialog = "",
	customDialogText = "I have been watching you, you're the newest hot shot around the village. I am excited we finally meet, I am sure you know who I am.",
	stopConversation = "false",
	options = {
		{"I sure do, I am here to capture you. Come peacefully and no one will get hurt.","light"},--done
		{"I am here to show you what real power looks like!","dark"},--done
		{"You think you're so smart, I am going to rid you of this place... After I am done with you, you can become 'One with the Force'","exile"}--done
	}
}
force_sensitive_mel_conv:addScreen(mel_first_screen);


--Light
light = ConvoScreen:new {
	id = "light",
	leftDialog = "",
	customDialogText = "HA! Capture me? You truely are a tool of the Jedi. I will not be captured by an untrained Force Sensitive! I hope you came ready for a fight youngling!",
	stopConversation = "false",
	options = {
		{"Wait wait, we can still talk this over can't we?","switch"},--done
		{"I will do what I must.","light1"}--done
	}
}
force_sensitive_mel_conv:addScreen(light);


--Light1
light1 = ConvoScreen:new {
	id = "light1",
	leftDialog = "",
	customDialogText = "Then face your doom!",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_mel_conv:addScreen(light1);


--switch
switch = ConvoScreen:new {
	id = "switch",
	leftDialog = "",
	customDialogText = "You come here to try to capture me, and now you just want to talk? We can talk about you dying or serving me those are your only options.",
	stopConversation = "false",
	options = {
		{"I don't wish to die, I will join you.","switch_setup"},--done
		{"Actually... How about I just kill you instead.","exile"},--done
		{"You'll have to kill me then, I will never join you!","light1"}--done
	}
}
force_sensitive_mel_conv:addScreen(switch);


--Switch Setup
switch_setup = ConvoScreen:new {
	id = "switch_setup",
	leftDialog = "",
	customDialogText = "I knew we could work something out, altho I won't be so trusting... You prove to me that you're loyal to me and only me I will train you and give you ultimate power.",
	stopConversation = "false",
	options = {
		{"What must I do to prove myself","alert"},--done
		{"What must I do to prove myself (lie)","alert"}--done
	}
}
force_sensitive_mel_conv:addScreen(switch_setup);


--alert
alert = ConvoScreen:new {
	id = "alert",
	leftDialog = "",
	customDialogText = "Bring me Sarguillo and when the time comes strike him down, now go!",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_mel_conv:addScreen(alert);


--Dark
dark = ConvoScreen:new {
	id = "dark",
	leftDialog = "",
	customDialogText = "True power? You know nothing of true power!",
	stopConversation = "false",
	options = {
		{"You think because you control the Sith Shadows that you are made up of 'Power'? You are only going to fail, only I can bring the village to it's knees!","dark1"},--done
		{"I know enough power to take you down.","exile"}--done
	}
}
force_sensitive_mel_conv:addScreen(dark);


--dark1
dark1 = ConvoScreen:new {
	id = "dark1",
	leftDialog = "",
	customDialogText = "Maybe we could work together, think of what we could do TOGETHER! Join me and we can conquer the galaxy, think about what we could accomplish. The Jedi council will fall to their knees before us.",
	stopConversation = "false",
	options = {
		{"I can see that we have the same goals, maybe we could work something out. What should we do first?","dark2"},--done
		{"I don't need you, you're weak!","exile"}--done
	}
}
force_sensitive_mel_conv:addScreen(dark1);


--dark2
dark2 = ConvoScreen:new {
	id = "dark2",
	leftDialog = "",
	customDialogText = "Well I had just been thinking about weakening them with my Sith Shadows, then luring Sarguillo and Paemos outside... Strike them in the heart.",
	stopConversation = "false",
	options = {
		{"I will convince Paemos, and Sarguillo to come outside the village then. They know not of my plan, they are fools.","dark3"}--done
	}
}
force_sensitive_mel_conv:addScreen(dark2);


--dark3
dark3 = ConvoScreen:new {
	id = "dark3",
	leftDialog = "",
	customDialogText = "Then go, I will put our scheme into motions.",
	stopConversation = "false",
	options = {
	}
}
force_sensitive_mel_conv:addScreen(dark3);


--Exile
exile = ConvoScreen:new {
	id = "exile",
	leftDialog = "",
	customDialogText = "Those are some bold words coming from a youngling, very well then prepare yourself!",
	stopConversation = "true",
	options = {
	}
}
force_sensitive_mel_conv:addScreen(exile);

addConversationTemplate("force_sensitive_mel_conv", force_sensitive_mel_conv);

--#############################################			aulun				#################################

force_sensitive_aulun_conv = ConvoTemplate:new {
	initialScreen = "aulun_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_aulun_conv_handler",
	screens = {}
}


--First
aulun_first_screen = ConvoScreen:new {
	id = "aulun_first_screen",
	leftDialog = "",
	customDialogText = "Hello, Im Aulun, Are you the one the Capitain sent over?",
	stopConversation = "false",
	options = {
		{"Yes I am.","aulun2"}
	}
}

force_sensitive_aulun_conv:addScreen(aulun_first_screen);

aulun2 = ConvoScreen:new {
	id = "aulun2",
	leftDialog = "",
	customDialogText = "Good, I will be teaching you some basic engagement techniques with a target.",
	stopConversation = "false",
	options = {
		{"I am ready.","aulun3"},
		{"I think I already know what to do.","aulun4"}	
	}
}

force_sensitive_aulun_conv:addScreen(aulun2);

aulun3 = ConvoScreen:new {
	id = "aulun3",
	leftDialog = "",
	customDialogText = "First, Focus yourself with Force Focus, Then Apply a stunned state on the target as you approach with Force Throw, Once in range begin your melee assault with OneHandSaberHit1.",
	stopConversation = "false",
	options = {
		{"I will give it my best","aulun4"}
	}
}

force_sensitive_aulun_conv:addScreen(aulun3);

aulun4 = ConvoScreen:new {
	id = "aulun4",
	leftDialog = "",
	customDialogText = "Then when you are ready, attack the Practice droid.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulun_conv:addScreen(aulun4);

aulun_parttwo = ConvoScreen:new {
	id = "aulun_parttwo",
	leftDialog = "",
	customDialogText = "You did great! I need you to help me outside of the enclave now, use your Mentors Crystal then meet me outside.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulun_conv:addScreen(aulun_parttwo);

crystal_screen = ConvoScreen:new {
	id = "crystal_screen",
	leftDialog = "",
	customDialogText = "Use your Mentor Crystal before we begin this exercise.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulun_conv:addScreen(crystal_screen);

hello_screen = ConvoScreen:new {
	id = "hello_screen",
	leftDialog = "",
	customDialogText = "Hey there.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulun_conv:addScreen(hello_screen);




addConversationTemplate("force_sensitive_aulun_conv", force_sensitive_aulun_conv);

--#############################################			aulun				#################################

force_sensitive_auluntwo_conv = ConvoTemplate:new {
	initialScreen = "auluntwo_first_screen",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_auluntwo_conv_handler",
	screens = {}
}


--First
auluntwo_first_screen = ConvoScreen:new {
	id = "auluntwo_first_screen",
	leftDialog = "",
	customDialogText = "Thanks for coming so quickly, What do you know about the Kliknik species?",
	stopConversation = "false",
	options = {
		{"I have not heard of them.","fork1"},
		{"I hear they are Force sensitive","fork2"},
	}
}

force_sensitive_auluntwo_conv:addScreen(auluntwo_first_screen);

crystal_screen = ConvoScreen:new {
	id = "crystal_screen",
	leftDialog = "",
	customDialogText = "Use your Mentor Crystal before we begin this exercise.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_auluntwo_conv:addScreen(crystal_screen);

fork1 = ConvoScreen:new {
	id = "fork1",
	leftDialog = "",
	customDialogText = "They are a mantis like creature, but whats unique about them is they seek out Force users. When encountered they become aggressive, in packs they should not be taken lightly.",
	stopConversation = "false",
	options = {
		{"Fascinating, Will we encounter some today?","fork3a"},
		{"So we get to hunt them down?","fork3b"},
	}
}

force_sensitive_auluntwo_conv:addScreen(fork1);

fork2 = ConvoScreen:new {
	id = "fork2",
	leftDialog = "",
	customDialogText = "It would seem they are drawn to the Force, but we are not certain.",
	stopConversation = "false",
	options = {
		{"Interesting, what do they have to do with me?","fork3a"}
	}
}

force_sensitive_auluntwo_conv:addScreen(fork2);

fork3a = ConvoScreen:new {
	id = "fork3a",
	leftDialog = "",
	customDialogText = "Since the move here to Yavin4 large numbers of Kliknik have moved on the enclave and attacked us. A group was spotted near by and would be a perfect test of your new abilities. But you must be mindful, try not to take them all on at once. ",
	stopConversation = "false",
	options = {
		{"I wont fail you.","fork4"}
	}
}

force_sensitive_auluntwo_conv:addScreen(fork3a);

fork3b = ConvoScreen:new {
	id = "fork3b",
	leftDialog = "",
	customDialogText = "Careful youngone, that sounds aggressive. We do only as we must, The Jedi was is to promote peace with all living creatures in the galaxy.",
	stopConversation = "false",
	options = {
		{"Yes of course, im sorry. ","fork3a"}
	}
}

force_sensitive_auluntwo_conv:addScreen(fork3b);

fork4 = ConvoScreen:new {
	id = "fork4",
	leftDialog = "",
	customDialogText = "I trust your abilities, Here is the location. Return to me after you have defeated them.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_auluntwo_conv:addScreen(fork4);

turnin = ConvoScreen:new {
	id = "turnin",
	leftDialog = "",
	customDialogText = "Good work, You made quick work of them but another matter has arised, A Jedi Initiate captured a Kliknik that was behaving oddly just North of here. I have been asked to investigate and would like you to join me. Use your Mentors Crystal then meet me here.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_auluntwo_conv:addScreen(turnin);





addConversationTemplate("force_sensitive_auluntwo_conv", force_sensitive_auluntwo_conv);

--#############################################			aulunthree			#################################

force_sensitive_aulunthree_conv = ConvoTemplate:new {
	initialScreen = "aulun_kliknik",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_aulunthree_conv_handler",
	screens = {}
}


--First
aulun_kliknik = ConvoScreen:new {
	id = "aulun_kliknik",
	leftDialog = "",
	customDialogText = "Glad you made it, Do not fear this Kliknik seems completely passive.",
	stopConversation = "false",
	options = {
		{"What are we going to do with it?","aulun_kliknik2"}
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_kliknik);

crystal_screen = ConvoScreen:new {
	id = "crystal_screen",
	leftDialog = "",
	customDialogText = "Use your Mentor Crystal before we begin this exercise.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulunthree_conv:addScreen(crystal_screen);

aulun_kliknik2 = ConvoScreen:new {
	id = "aulun_kliknik2",
	leftDialog = "",
	customDialogText = "You will be discovering the truth behind this Klikniks actions, it was non aggressive and peacefully approached the Trez. Since it was no threat, the Trez kept it company and called for assistance. I believe this creature is trying to communicate with us. I want you to find out why.",
	stopConversation = "false",
	options = {
		{"But it cannot speak? How do I try and communicate?","aulun_kliknik3"}
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_kliknik2);

aulun_kliknik3 = ConvoScreen:new {
	id = "aulun_kliknik3",
	leftDialog = "",
	customDialogText = "Use the Force, keep your mind clear, Follow your instinct.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_kliknik3);

aulun_waiting = ConvoScreen:new {
	id = "aulun_waiting",
	leftDialog = "",
	customDialogText = "When ever you are ready.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_waiting);

aulun_returned = ConvoScreen:new {
	id = "aulun_returned",
	leftDialog = "",
	customDialogText = "That was interesting, What do you think?",
	stopConversation = "false",
	options = {
		{"These creatures seem to be Force Sensitive, I fear they are being antagonized making them aggressive.","aulun_returned1a"},
		{"They are spawns of the Darkside and should be wiped out.","aulun_returned1b"},
		{"It seems like a mindless creature to me, We should just release it.","aulun_returned1c"}
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned);

aulun_returned1a = ConvoScreen:new {
	id = "aulun_returned1a",
	leftDialog = "",
	customDialogText = "Very good, I agree. I think a member of the Dark side is provoking these attacks by exploiting these creatures extreamly unique abilitiy to feel the Force.",
	stopConversation = "false",
	options = {
		{"Thank you, What will happen to it now?","aulun_returned2a"},

	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned1a);



aulun_returned1b = ConvoScreen:new {
	id = "aulun_returned1b",
	leftDialog = "",
	customDialogText = "This is disappointing news, Your senses are not as well tuned as I had hoped. I observed your interaction witht he Kliknik and feel the creature is being provoked, not a spawn of evil.",
	stopConversation = "false",
	options = {
		{"Im sorry to disappoint.. I will try harder","aulun_returned2b"}
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned1b);

aulun_returned1c = ConvoScreen:new {
	id = "aulun_returned1c",
	leftDialog = "",
	customDialogText = "This is disappointing news, Your senses are not as well tuned as I had hoped. I observed your interaction witht he Kliknik and feel the creature is very unique in its abilitiy to sense the Force.",
	stopConversation = "false",
	options = {
		{"I need to be more intune with the Force","aulun_returned2c"}
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned1c);

aulun_returned2a = ConvoScreen:new {
	id = "aulun_returned2a",
	leftDialog = "",
	customDialogText = "This Kilknik will be protected, I will report to my Master what has transpired here who will launch a investigation to find the culprit who is tormenting these creatures.",
	stopConversation = "false",
	options = {
		{"Will I be helping with the investigation?","aulun_returned3a"},
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned2a);

aulun_returned2b = ConvoScreen:new {
	id = "aulun_returned2b",
	leftDialog = "",
	customDialogText = "You must be careful, I feel you act with emotion and anger, remember the Jedi Code. There is no emotion, there is peace.",
	stopConversation = "false",
	options = {
		{"There is no emotion, There is peace. ","aulun_returned2e"},
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned2b);

aulun_returned2e = ConvoScreen:new {
	id = "aulun_returned2e",
	leftDialog = "",
	customDialogText = "Good, Meditate on this and you will get back on track.",
	stopConversation = "false",
	options = {
		{"Yes Master, What will happen to the Kliknik now? ","aulun_returned2a"},
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned2e);

aulun_returned2c = ConvoScreen:new {
	id = "aulun_returned2c",
	leftDialog = "",
	customDialogText = "In time you will learn, remember to be mindful of the Force. Feel, Don't think.",
	stopConversation = "false",
	options = {
		{"I am trying Master, What will happen to it now?","aulun_returned2a"},
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned2c);

aulun_returned3a = ConvoScreen:new {
	id = "aulun_returned3a",
	leftDialog = "",
	customDialogText = "Not at this time, You have done very well but you have much to learn before we send you searching for a memeber of the Dark side. Our work here is complete. You should now use your Mentors Crystal then meet Janakus back inside the enclave who will help complete your training.",
	stopConversation = "false",
	options = {
		{"I understand, Thank you for allowing me to assist you.","aulun_returned4a"},
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned3a);

aulun_returned4a = ConvoScreen:new {
	id = "aulun_returned4a",
	leftDialog = "",
	customDialogText = "You are going to make a fine Jedi, I look forward to seeing you again.",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_aulunthree_conv:addScreen(aulun_returned4a);



addConversationTemplate("force_sensitive_aulunthree_conv", force_sensitive_aulunthree_conv);

--#############################################			kliknik			#################################

force_sensitive_kliknik_conv = ConvoTemplate:new {
	initialScreen = "kliknik_one",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_kliknik_conv_handler",
	screens = {}
}


--First
kliknik_one = ConvoScreen:new {
	id = "kliknik_one",
	leftDialog = "",
	customDialogText = "<The Kliknik peers at you>",
	stopConversation = "false",
	options = {
		{"Uhmm, Hi? <Waves at Kliknik>","kliknik_1a"},
		{"Do you understand me?","kliknik_1b"},
		{"<Introduce yourself>","kliknik_1c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_one);

kliknik_1a = ConvoScreen:new {
	id = "kliknik_1a",
	leftDialog = "",
	customDialogText = "<The Kliknik looks confused>",
	stopConversation = "false",
	options = {
		{"Do you understand me?","kliknik_1b"},
		{"<Introduce yourself>","kliknik_1c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_1a);

kliknik_1b = ConvoScreen:new {
	id = "kliknik_1b",
	leftDialog = "",
	customDialogText = "<The Kliknik lets out a loud screech>",
	stopConversation = "false",
	options = {
		{"<Draw your way> Stay back!","kliknik_2a"},
		{"<Introduce yourself>","kliknik_1c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_1b);

kliknik_1c = ConvoScreen:new {
	id = "kliknik_1c",
	leftDialog = "",
	customDialogText = "<The Kliknik Jabs both arms into the ground and lowers its head>",
	stopConversation = "false",
	options = {
		{"I think its being aggressive now!","kliknik_2a"},
		{"<It seems to understand>","kliknik_2b"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_1c);

kliknik_2a = ConvoScreen:new {
	id = "kliknik_2a",
	customDialogText = "<The Kliknik steps back, almost like its trying to defuse the situation>",
	stopConversation = "false",
	options = {
		{"<Make a calming jesture> Sorry, Sorry.","kliknik_2b"},
		{"You do understand me?","kliknik_2b"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_2a);

kliknik_2b = ConvoScreen:new {
	id = "kliknik_2b",
	customDialogText = "<The Kliknik blinks at you>",
	stopConversation = "false",
	options = {
		{"<Channel the Lightside>","kliknik_3a"},
		{"<Channel the Darkside>","kliknik_3b"},
		{"Why are you here?","kliknik_3c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_2b);

kliknik_3a = ConvoScreen:new {
	id = "kliknik_3a",
	customDialogText = "<The Kliknik reaches out at you slowly>",
	stopConversation = "false",
	options = {
		{"<Channel the Lightside on the Kliknik>","kliknik_4a"},
		{"<Grab the Klikniks claw>","kliknik_4b"},
		{"<Draw your weapon>","kliknik_3a"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_3a);

kliknik_3b = ConvoScreen:new {
	id = "kliknik_3b",
	customDialogText = "<The Kliknik jabs at you!>",
	stopConversation = "false",
	options = {
		{"<Channel the Lightside>","kliknik_3a"},
		{"<I think this Creature has been provoked by the Dark Side>","kliknik_4c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_3b);

kliknik_3c = ConvoScreen:new {
	id = "kliknik_3c",
	customDialogText = "<The Kliknik looks confused and scared>",
	stopConversation = "false",
	options = {
		{"<Channel the Lightside>","kliknik_3a"},
		{"<Channel the Darkside>","kliknik_3b"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_3c);

kliknik_4a = ConvoScreen:new {
	id = "kliknik_4a",
	customDialogText = "<The Kliknik is zen",
	stopConversation = "false",
	options = {
		{"They are Force Sensitive","kliknik_5a"},
		{"Impossible, can this creature really sense the Force?","kliknik_5c"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_4a);

kliknik_4b = ConvoScreen:new {
	id = "kliknik_4b",
	customDialogText = "<The Kliknik pulls away>",
	stopConversation = "false",
	options = {
		{"<Channel the Lightside on the Kliknik>","kliknik_4a"},
		{"This creature is only behaving this way because something is wrong with it.","kliknik_5b"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_4b);

kliknik_4c = ConvoScreen:new {
	id = "kliknik_4c",
	customDialogText = "<The Kliknik shivers>",
	stopConversation = "false",
	options = {
		{"They are Force Sensitive","kliknik_5a"},
		{"This creature is a product of the Dark side.","kliknik_5b"}
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_4c);

kliknik_5a = ConvoScreen:new {
	id = "kliknik_5a",
	customDialogText = "<I should speak with Aulun>",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_5a);

kliknik_5b = ConvoScreen:new {
	id = "kliknik_5b",
	customDialogText = "<I should speak with Aulun and suggest klling it>",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_5b);

kliknik_5c = ConvoScreen:new {
	id = "kliknik_5c",
	customDialogText = "<I should speak with Aulun and suggest releasing it>",
	stopConversation = "true",
	options = {
	}
}

force_sensitive_kliknik_conv:addScreen(kliknik_5c);








addConversationTemplate("force_sensitive_kliknik_conv", force_sensitive_kliknik_conv);
