sith_daily_two = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "sith_daily_two_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Finaly a true Sith.",
	stopConversation = "false",
	options = {
		{"You have a task for me?.", "second_screen"},
	}
}
sith_daily_two:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I have lured a Jedi Knight into a trap near by, the full thinks I wish to Leave the Sith. Destroy him!",
	stopConversation = "false",
	options = {
		{"It will be my pleasure.", "accept_screen"},
	}
}
sith_daily_two:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Even though the darkside is strong, do not underestimate your opponent.",
	stopConversation = "true",
	options = {}
}
sith_daily_two:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "My plan worked. If only all Jedi were that stupid.",
	stopConversation = "false",
	options = {
		{"Stupid as he may have been, strong in the force he was.", "complete_screen_final"},
	}
}
sith_daily_two:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "There are those in the Jedi Order who are quite strong, Anyways, here take this.",
	stopConversation = "true",
	options = {}
}
sith_daily_two:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Afriad are you? Maybe I should kill you.",
	stopConversation = "true",
	options = {}
}
sith_daily_two:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Ive nothing for you, come back later.",
	stopConversation = "true",
	options = {}
}
sith_daily_two:addScreen(quest_done);

addConversationTemplate("sith_daily_two", sith_daily_two);

