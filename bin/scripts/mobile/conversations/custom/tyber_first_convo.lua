tyberZannFirstConvo = ConvoTemplate:new {

	initialScreen = "first_screen",

	templateType = "Lua",

	luaClassHandler = "zannFirstConvo_handler",

	screens = {}



}

--Intro First

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "What?",

	stopConversation = "false",

	options = {

		{"I'm looking for Tyber Zann, you know of him?", "second_screen"},

	}

}

tyberZannFirstConvo:addScreen(first_screen);





second_screen = ConvoScreen:new {

	id = "second_screen",

	leftDialog = "",

	customDialogText = "Uhm... Who's asking?",

	stopConversation = "false",

	options = {

		{"That's classified information, until I know if it's you or not.", "third_screen"},

	}

}

tyberZannFirstConvo:addScreen(second_screen);



third_screen = ConvoScreen:new {

	id = "third_screen",

	leftDialog = "",

	customDialogText = "You must be the contact I'm supposed to meet for this meeting?",

	stopConversation = "false",

	options = {
	
		{"That's a big negative; However, you just gave yourself away. *smirk*", "tyberZann_explaina"}

	}

}

tyberZannFirstConvo:addScreen(third_screen);



tyberZann_explaina = ConvoScreen:new {

	id = "tyberZann_explaina",

	leftDialog = "",

	customDialogText = "What do you mean?! Who are you?!",

	stopConversation = "false",

	options = {
	
		{"Doesn't matter who I'm with, it matters who I'm here on the behalf of.", "tyberZann_explain"}

	}

}

tyberZannFirstConvo:addScreen(tyberZann_explaina);



tyberZann_explain = ConvoScreen:new {

	id = "tyberZann_explain",

	leftDialog = "",

	customDialogText = "The Black Sun... What do you want? I'll give you anything, just don't hurt me!",

	stopConversation = "false",

	options = {
	
		{"What was this meeting about? I want the information now!", "final_screen"}

	}

}

tyberZannFirstConvo:addScreen(tyberZann_explain);



final_screen = ConvoScreen:new {

	id = "final_screen",

	leftDialog = "",

	customDialogText = "He was supposed to come here for this datapad, take it! Just please don't hurt me, I have a family... I didn't want any of this.",

	stopConversation = "true",

	options = {

	}

}

tyberZannFirstConvo:addScreen(final_screen);



goaway_screen = ConvoScreen:new {

	id = "goaway_screen",

	leftDialog = "",

	customDialogText = "Yes?",

	stopConversation = "true",

	options = {

	}

}



tyberZannFirstConvo:addScreen(goaway_screen);



complete_screen = ConvoScreen:new {

	id = "complete_screen",

	leftDialog = "",

	customDialogText = "You got what you needed, don't hurt me...",

	stopConversation = "true",

	options = {

	}

}



tyberZannFirstConvo:addScreen(complete_screen);



addConversationTemplate("tyberZannFirstConvo", tyberZannFirstConvo);




