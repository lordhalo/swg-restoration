select_dark_conv = ConvoTemplate:new {
	initialScreen = "dark_first_screen",
	templateType = "Lua",
	luaClassHandler = "select_dark_convo_handler",
	screens = {}
}


--Intro First
dark_first_screen = ConvoScreen:new {
	id = "dark_first_screen",
	leftDialog = "",
	customDialogText = "blahblahblah",
	stopConversation = "true",
	options = {
		--{"Yes, I have been waiting for this moment for a long time now.", "thiel2"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

select_dark_conv:addScreen(dark_first_screen);



addConversationTemplate("select_dark_conv", select_dark_conv);

select_dark_injured_conv = ConvoTemplate:new {
	initialScreen = "dark_injured_screen",
	templateType = "Lua",
	luaClassHandler = "select_dark_injured_convo_handler",
	screens = {}
}


--Intro First
dark_injured_screen = ConvoScreen:new {
	id = "dark_injured_screen",
	leftDialog = "",
	customDialogText = "You there, I know who you are. Mellichae will be expecting you. We have a camp North of here, find it.",
	stopConversation = "true",
	options = {
		--{"Yes, I have been waiting for this moment for a long time now.", ""},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

select_dark_injured_conv:addScreen(dark_injured_screen);

dark_injured_leave = ConvoScreen:new {
	id = "dark_injured_leave",
	leftDialog = "",
	customDialogText = "fuckoff",
	stopConversation = "true",
	options = {
		--{"Yes, I have been waiting for this moment for a long time now.", "thiel2"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

select_dark_injured_conv:addScreen(dark_injured_leave);



addConversationTemplate("select_dark_injured_conv", select_dark_injured_conv);

select_dark_path_conv = ConvoTemplate:new {
	initialScreen = "dark_path_screen",
	templateType = "Lua",
	luaClassHandler = "select_dark_path_convo_handler",
	screens = {}
}


--Intro First
dark_path_screen = ConvoScreen:new {
	id = "dark_path_screen",
	leftDialog = "",
	customDialogText = "Welcome, I have been expecting you. I am Sertul and I have some questions for you.",
	stopConversation = "false",
	options = {
		{"Not exactly the welcome I was expecting, but pleasure to meet you. How can I help?", "screen2"},
		{"Ill be the one asking questions, Where is Mellichae?","subscreen2"},
		{"Save your breath if you want to keep breathing, You know why I am here.","angerscreen2"}
	
	}
}

select_dark_path_conv:addScreen(dark_path_screen);

subscreen2 = ConvoScreen:new {
	id = "subscreen2",
	leftDialog = "",
	customDialogText = "If you wish to speak with Mellichae, you must first convince me you are worth his time.",
	stopConversation = "false",
	options = {
		{"Fine, what is your question?", "screen2"},
		{"Do not test my patience, I will end you.","angerscreen2"}
	
	}
}

select_dark_path_conv:addScreen(subscreen2);

angerscreen2 = ConvoScreen:new {
	id = "angerscreen2",
	leftDialog = "",
	customDialogText = "Such anger, Good, Good. Once you learn to channel that rage you will be a valuable asset.",
	stopConversation = "false",
	options = {
		{"I am ready to learn, Send me on my way.", "angerscreen3"},
		{"Im about to channel it on you! Where is Mellichae!","angerscreen3"}
	
	}
}

select_dark_path_conv:addScreen(angerscreen2);

angerscreen3 = ConvoScreen:new {
	id = "angerscreen3",
	leftDialog = "",
	customDialogText = "Very well, Mellichae is waiting for you at these coordinates. x y z",
	stopConversation = "true",
	options = {
		--{"I am ready to learn, Send me on my way.", "angerscreen3"},
		--{"Im about to channel it on you! Where is Mellichae!","angerscreen3"}
	
	}
}

select_dark_path_conv:addScreen(angerscreen3);

screen2 = ConvoScreen:new {
	id = "screen2",
	leftDialog = "",
	customDialogText = "What made you decide to seek Mellichae out?",
	stopConversation = "false",
	options = {
		{"I want to join him", "screen3"},
		{"His offer intrigued me","subscreen3"}
	
	}
}

select_dark_path_conv:addScreen(screen2);

screen3 = ConvoScreen:new {
	id = "screen3",
	leftDialog = "",
	customDialogText = "",
	stopConversation = "false",
	options = {
		{"I can sence the power he possesses", "screen4"},
		{"His offer intrigued me","subscreen3"}
	
	}
}

select_dark_path_conv:addScreen(screen3);


addConversationTemplate("select_dark_path_conv", select_dark_path_conv);

