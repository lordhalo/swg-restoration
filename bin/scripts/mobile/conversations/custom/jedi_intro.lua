jedi_intro_convo = ConvoTemplate:new {
	initialScreen = "jedi_intro_screen",
	templateType = "Lua",
	luaClassHandler = "jedi_intro_convo_handler",
	screens = {}
}


--Intro First
jedi_intro_screen = ConvoScreen:new {
	id = "jedi_intro_screen",
	leftDialog = "",
	customDialogText = "Who are you? Why are you here? Keep your distance!",
	stopConversation = "false",
	options = {
		{"Sorry, I will be on my way.", "leave_screen"},
		{"I am searching for the village of Aurilia. ","second_screen"}
	
	}
}

jedi_intro_convo:addScreen(jedi_intro_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Why? No one has sought out this place in years.",
	stopConversation = "false",
	options = {
		{"What is going on here?", "explain_screen"},
		{"I heard a rumor those in touch with the force come here for training and I want to join.","third_screen"}
	
	}
}

jedi_intro_convo:addScreen(second_screen);

explain_screen = ConvoScreen:new {
	id = "explain_screen",
	leftDialog = "",
	customDialogText = "a time ago this place was home to new Force users, until the cult of Sith followers nearly wiped us out.",
	stopConversation = "false",
	options = {
		{"Sith followers?", "explain_screen_two"}
	
	}
}

jedi_intro_convo:addScreen(explain_screen);

explain_screen_two = ConvoScreen:new {
	id = "explain_screen_two",
	leftDialog = "",
	customDialogText = "When the Emperor executed Order 66 it caused a inbalance in the Force, Driving some Jedi mad and to the Dark side, However the Emperor underestimated the Jedis resilience and lost a large number of clones who failed to complete their mission. With Order 66 having failed, The Emperor had no choice but to end the ancient rule of two doctrine in attempt to regain control of the galaxy by taking new apprentices. Mellichae, the leader of the Sith Shadow clan was one of them, who then launched a assault on our village and attempt to limit any new force users from joining the new Jedi Order.",
	stopConversation = "false",
	options = {
		{"New Jedi Order?", "explain_screen_three"},
		{"The Jedi are a bane on this galaxy and I wish to help crush them!","dark_screen"}
	
	}
}

jedi_intro_convo:addScreen(explain_screen_two);

explain_screen_three = ConvoScreen:new {
	id = "explain_screen_three",
	leftDialog = "",
	customDialogText = "Yes the Jedi order nearly collapsed the attack, but relocated on a remote Yavinian moon and rallies to fight the Emperor and his followers.",
	stopConversation = "false",
	options = {
		{"I want to help!", "third_screen"}
	
	}
}

jedi_intro_convo:addScreen(explain_screen_three);

dark_screen = ConvoScreen:new {
	id = "dark_screen",
	leftDialog = "",
	customDialogText = "So that is why you came..Leave this place",
	stopConversation = "true",
	options = {
	
	}
}

jedi_intro_convo:addScreen(dark_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "But you have no glowing connection to the Force, Why do you want to help?",
	stopConversation = "false",
	options = {
		{"I have no strong connection, but I know the Force exist and I want to learn more.", "fourth_screen"}
	
	}
}

jedi_intro_convo:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "It is no easy task, if you truely wish to learn more, go to these coordinates and meet Utao.. he can help further.",
	stopConversation = "true",
	options = {
	
	}
}

jedi_intro_convo:addScreen(fourth_screen);

done_screen = ConvoScreen:new {
	id = "done_screen",
	leftDialog = "",
	customDialogText = "I have done all I can to guide you.",
	stopConversation = "true",
	options = {
	
	}
}

jedi_intro_convo:addScreen(done_screen);

leave_screen = ConvoScreen:new {
	id = "leave_screen",
	leftDialog = "",
	customDialogText = "This is no place for commoners! Flee with caution.",
	stopConversation = "true",
	options = {
		--{"Yes, I have been waiting for this moment for a long time now.", "thiel2"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

jedi_intro_convo:addScreen(leave_screen);



addConversationTemplate("jedi_intro_convo", jedi_intro_convo);

utato_convo = ConvoTemplate:new {
	initialScreen = "first_screen", --select_job_path_convo_handler
	templateType = "Lua", 
	luaClassHandler = "utato_convo_handler",
	screens = {}
}


--Intro First
first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "Greetings, If you have saught me out there can be only one thing on your mind, The Force.",
	stopConversation = "false",
	options = {
		{"Yes, I want to learn about the Force.", "second_screen"}
	
	}
}

utato_convo:addScreen(first_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "To be a Force user in this galaxy comes at great risk. All Force users are attackable on sight if any Force powers, Tittles or Lightsabers are displayed. Never can a Force user clone, doing so would sever your connection with the force, causing you to unlearn any knowledge you aquire.",
	stopConversation = "false",
	options = {
		{"Very well, I am ready.", "third_screen"}
	
	}
}

utato_convo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "So be it, Meditate at this shrine.. May the Force be with you.",
	stopConversation = "true",
	options = {
	
	}
}

utato_convo:addScreen(third_screen);

leave_screen = ConvoScreen:new {
	id = "leave_screen",
	leftDialog = "",
	customDialogText = "You have no buisness with me.",
	stopConversation = "true",
	options = {
	
	}
}

utato_convo:addScreen(leave_screen);



addConversationTemplate("utato_convo", utato_convo);

mellichae_convo = ConvoTemplate:new {
	initialScreen = "first_screen", --select_job_path_convo_handler
	templateType = "Lua", 
	luaClassHandler = "mellichae_convo_handler",
	screens = {}
}


--Intro First
first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "Speak quicky before I decide to kill you.",
	stopConversation = "false",
	options = {
		{"I encountered an old man at a force user village and I wish to crush them.", "second_screen"}
	
	}
}

mellichae_convo:addScreen(first_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Ha! The new Dark Order could use someone like you.",
	stopConversation = "false",
	options = {
		{"Excelliant", "third_screen"}
	
	}
}

mellichae_convo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "First a warning, To be a Force user in this galaxy comes at great risk. All Force users are attackable on sight if any Force powers, Tittles or Lightsabers are displayed. Never can a Force user clone, doing so would sever your connection with the force, causing you to unlearn any knowledge you aquire.",
	stopConversation = "false",
	options = {
		{"I fear notthing", "fourth_screen"}
	
	}
}

mellichae_convo:addScreen(third_screen);

fourth_screen = ConvoScreen:new {
	id = "fourth_screen",
	leftDialog = "",
	customDialogText = "Then we shall meet again, Meditate at this shrine and be gone.",
	stopConversation = "true",
	options = {
	
	}
}

mellichae_convo:addScreen(fourth_screen);

leave_screen = ConvoScreen:new {
	id = "leave_screen",
	leftDialog = "",
	customDialogText = "You have only a moment to leave before I destroy you.",
	stopConversation = "true",
	options = {
	
	}
}

mellichae_convo:addScreen(leave_screen);



addConversationTemplate("mellichae_convo", mellichae_convo);

