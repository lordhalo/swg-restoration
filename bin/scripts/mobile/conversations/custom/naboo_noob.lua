naboo_noob_convo = ConvoTemplate:new {
	initialScreen = "naboo_noob_screen",
	templateType = "Lua",
	luaClassHandler = "naboo_noob_convo_handler",
	screens = {}
}


--Intro First
naboo_noob_screen = ConvoScreen:new {
	id = "naboo_noob_screen",
	leftDialog = "",
	customDialogText = "Welcome to the event, Up the road from here you will find everything you need, The CampFire will supply buffs that would normally be found in Ranger camps. Jedi when using the Crystal you will be given a random skill set for testing, Thanks for your support!",
	stopConversation = "true",
	options = {
		--{"ok sure.", "naboo_noob_accepted_screen"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

naboo_noob_convo:addScreen(naboo_noob_screen);

naboo_noob_accepted_screen = ConvoScreen:new {
	id = "naboo_noob_accepted_screen",
	leftDialog = "",
	customDialogText = "What are you waiting for? go talk to him, I think his name is Binyre",
	stopConversation = "true",
	options = {
		--{"Yes, I have been waiting for this moment for a long time now.", "thiel2"},
		--{"I'd prefer to take my time, I'll come back when I am ready.","deny"}
	
	}
}

naboo_noob_convo:addScreen(naboo_noob_accepted_screen);



addConversationTemplate("naboo_noob_convo", naboo_noob_convo);

select_job_path_convo = ConvoTemplate:new {
	initialScreen = "job_screen", --select_job_path_convo_handler
	templateType = "Lua", 
	luaClassHandler = "select_job_path_convo_handler",
	screens = {}
}


--Intro First
job_screen = ConvoScreen:new {
	id = "job_screen",
	leftDialog = "",
	customDialogText = "Are you looking to join a Security detail? We really need some help around here.",
	stopConversation = "false",
	options = {
		{"Yes, What do you need me to do?", "join"},
		{"I dont know, im not much of a fighter.","craft"}
	
	}
}

select_job_path_convo:addScreen(job_screen);

job_screen_join = ConvoScreen:new {
	id = "join",
	leftDialog = "",
	customDialogText = "Are you looking to join a Security detail? We really need some help around here.",
	stopConversation = "false",
	options = {
		{"Yes, What do you need me to do?", "join"},
		{"I dont know, im not much of a fighter.","craft"}
	
	}
}

select_job_path_convo:addScreen(job_screen_join);


addConversationTemplate("select_job_path_convo", select_job_path_convo);

