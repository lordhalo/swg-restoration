gcwJoinFaction = ConvoTemplate:new {
	initialScreen = "first_screen",
	templateType = "Lua",
	luaClassHandler = "gcwJoinFaction_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "How can I help you Solder?",
	stopConversation = "false",
	options = {
		{"I want join the Special Forces", "second_screen"},
	}
}
gcwJoinFaction:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "If you enlist yourself as Special Forces, you will be permanetly Overt and attackable, are you sure you are ready for that?",
	stopConversation = "false",
	options = {
		{"I am ready!", "join_rank"},
	}
}
gcwJoinFaction:addScreen(second_screen);

join_rank = ConvoScreen:new {
	id = "join_rank",
	leftDialog = "",
	customDialogText = "I will file the paperwork now, Good luck!",
	stopConversation = "true",
	options = {}
}
gcwJoinFaction:addScreen(join_rank);
------------------------------------Imperial
greet_imperial = ConvoScreen:new {
	id = "greet_imperial",
	leftDialog = "",
	customDialogText = "How can I help you Solder?",
	stopConversation = "false",
	options = {
		{"I want to be a Stormtrooper", "second_imperial"},
	}
}
gcwJoinFaction:addScreen(greet_imperial);

second_imperial = ConvoScreen:new {
	id = "second_imperial",
	leftDialog = "",
	customDialogText = "If you enlist yourself as a Stormtrooper, you will be permanetly Overt and attackable, are you sure you are ready for that?",
	stopConversation = "false",
	options = {
		{"Yes, Im ready", "join_imperial"},
	}
}
gcwJoinFaction:addScreen(second_imperial);

join_imperial = ConvoScreen:new {
	id = "join_imperial",
	leftDialog = "",
	customDialogText = "I will file the paperwork now, Good luck!",
	stopConversation = "true",
	options = {}
}
gcwJoinFaction:addScreen(join_imperial);
---------------------Goaway
go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "You have no buisness with me, find a Ranking Officer when you are ready for a promotion",
	stopConversation = "true",
	options = {}
}
gcwJoinFaction:addScreen(go_away);

addConversationTemplate("gcwJoinFaction", gcwJoinFaction);


