arukTheGreatIntro = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "aruk_thegreat_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Are you the guy Rell sent over?",
	stopConversation = "false",
	options = {
		{"I am", "second_screen"},
	}
}
arukTheGreatIntro:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
arukTheGreatIntro:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
arukTheGreatIntro:addScreen(go_away);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Good, I am pleased with your work on Naboo. You will rise fast in our clan.",
	stopConversation = "false",
	options = {
		{"Thank you", "final_screen"},
	}
}
arukTheGreatIntro:addScreen(second_screen);

final_screen = ConvoScreen:new {
	id = "final_screen",
	leftDialog = "",
	customDialogText = "Welcome to the Black Sun, Ill have some work for you later. See if anyone else needs a hand.",
	stopConversation = "true",
	options = {}
}
arukTheGreatIntro:addScreen(final_screen);

addConversationTemplate("arukTheGreatIntro", arukTheGreatIntro);

