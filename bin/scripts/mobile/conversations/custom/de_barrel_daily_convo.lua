debarrelDaily = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "de_barrel_daily_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "We had a trade agreement with Kobola clan, trading Minerials for Spice. Now they are demanding more Spice, No one backs out on a trade with us.",
	stopConversation = "false",
	options = {
		{"What do you want me to do?", "second_screen"},
	}
}
debarrelDaily:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Get on with it.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I want you to send a Message, head to Rori and kill 30 Kobola clan memeber.",
	stopConversation = "false",
	options = {
		{"Yes, Sir", "accept_screen"},
	}
}
debarrelDaily:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Perhaps this will make them rethink our agreement.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Is it done?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
debarrelDaily:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Good work, The Materials we get in trade are used in production of DE-10 Pistol barrels. Heres a spare I have from the last batch we made.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Come see me tomorrow, I will have another mission.",
	stopConversation = "true",
	options = {}
}
debarrelDaily:addScreen(tomorrow_screen);

addConversationTemplate("debarrelDaily", debarrelDaily);

