jedi_daily_two = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "jedi_daily_two_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Hello young Master, may I bother you for a moment?",
	stopConversation = "false",
	options = {
		{"Of course, I live to serve. ", "second_screen"},
	}
}
jedi_daily_two:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "I have received a report from one of our scout patrols, that a Sith has been spotted Northwest of here, near an old farm. I ask that you handle this matter personally.",
	stopConversation = "false",
	options = {
		{"Then I shall go.", "accept_screen"},
	}
}
jedi_daily_two:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "Thank you, may the Force guide you.",
	stopConversation = "true",
	options = {}
}
jedi_daily_two:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "So the report was right, there was a Sith.",
	stopConversation = "false",
	options = {
		{"I've never felt so much anger in one person, it gave him strength beyond limit. But, the Force helped me prevail.", "complete_screen_final"},
	}
}
jedi_daily_two:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "As I knew you would. Thank you.",
	stopConversation = "true",
	options = {}
}
jedi_daily_two:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Have you anything to report?",
	stopConversation = "true",
	options = {}
}
jedi_daily_two:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Thank you again.",
	stopConversation = "true",
	options = {}
}
jedi_daily_two:addScreen(quest_done);

addConversationTemplate("jedi_daily_two", jedi_daily_two);

