contracterDaily = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "contracter_daily_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "Jabba the Hutt placed a Bounty on the Valarian clan, Are you interest?",
	stopConversation = "false",
	options = {
		{"Yes", "second_screen"},
	}
}
contracterDaily:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some task for you.",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(go_away);

waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "I would suggest not making Jabba wait.",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(waiting_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "The targets are located in the Dune Sea of Tatooine, Kill 5 clan memebers then return to me.",
	stopConversation = "false",
	options = {
		{"Yes, Sir", "accept_screen"},
	}
}
contracterDaily:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I will upload the waypoint.",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Is it done?",
	stopConversation = "false",
	options = {
		{"yes", "complete_screen_final"},
	}
}
contracterDaily:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Excellent, Ill have Jabba wire you the funds",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(complete_screen_final);

tomorrow_screen = ConvoScreen:new {
	id = "tomorrow_screen",
	leftDialog = "",
	customDialogText = "Come see me tomorrow, I should have another contract",
	stopConversation = "true",
	options = {}
}
contracterDaily:addScreen(tomorrow_screen);

addConversationTemplate("contracterDaily", contracterDaily);

