blackSunIntroConvo = ConvoTemplate:new {
	initialScreen = "first_screen",
	templateType = "Lua",
	luaClassHandler = "blackSunIntroConvo_handler",
	screens = {}
}

--Intro First



first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "Can I help you?",
	stopConversation = "false",
	options = {
		{"I'm looking for someone who can tell me about the Black Sun.", "second_screen"},
		--{"Watch your tone.","deny"}
	}
}
blackSunIntroConvo:addScreen(first_screen);



accepted_screen = ConvoScreen:new {
	id = "accepted_screen",
	leftDialog = "",
	customDialogText = "Are you going or not?",
	stopConversation = "true",
	options = {
	}
}
blackSunIntroConvo:addScreen(accepted_screen);



second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Are you? Why would that be?",
	stopConversation = "false",
	options = {
		{"I'm looking for work, and not an 'honest living' either.", "third_screen"}
	}
}
blackSunIntroConvo:addScreen(second_screen);



third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "Ha! Well when you put it like that, maybe I can help you.",
	stopConversation = "false",
	options = {
		{"You?", "forth_screena"},	
	}
}
blackSunIntroConvo:addScreen(third_screen);



forth_screena = ConvoScreen:new {
	id = "forth_screena",
	leftDialog = "",
	customDialogText = "Don't let the looks deceive you. The name's Rell, I've been a smuggler for the Black Sun for quite some time now. First you should know what the Black Sun is all about...",
	stopConversation = "false",
	options = {
		{"What do you mean?", "forth_screenb"}
	}
}
blackSunIntroConvo:addScreen(forth_screena);



forth_screenb = ConvoScreen:new {
	id = "forth_screenb",
	leftDialog = "",
	customDialogText = "We aren't people looking for some quick cash. To make your way with us, you have to be able to do what it takes to gain power. Something tells me that doesn't scare you though.",
	stopConversation = "false",
	options = {
		{"I'm always ready to do what it takes.", "sub_fifth_screen"}
	}
}
blackSunIntroConvo:addScreen(forth_screenb);



sub_fifth_screen = ConvoScreen:new {
	id = "sub_fifth_screen",
	leftDialog = "",
	customDialogText = "Alright, first I must warn you: You WILL be attackable by the Rebellion and Death Watch at all times. If you still agree, my little friend here will mark this as our first contract.",
	stopConversation = "false",
	options = {
		{"I agree.", "fifth_screen"}
	}
}
blackSunIntroConvo:addScreen(sub_fifth_screen);



fifth_screen = ConvoScreen:new {
	id = "fifth_screen",
	leftDialog = "",
	customDialogText = "Good. I would congratulate you, but you haven't done anything yet. Remember, I personally guarantee all my contracts to my boss.*Grips his blaster* If you step out of line, I'll deal with you myself.",
	stopConversation = "false",
	options = {
		{"I'll keep that in mind and I won't disappoint.", "sixth_screen"}
	}
}
blackSunIntroConvo:addScreen(fifth_screen);



sixth_screen = ConvoScreen:new {
	id = "sixth_screen",
	leftDialog = "",
	customDialogText = "Then on that note, we are ready to move on. I'm not on Naboo by chance... The Empire may have a hold on the planet, but the Death Watch are quickly encroaching.",
	stopConversation = "false",
	options = {
		{"*listen attentively*", "seventh_screen"}
	}
}
blackSunIntroConvo:addScreen(sixth_screen);



seventh_screen = ConvoScreen:new {
	id = "seventh_screen",
	leftDialog = "",
	customDialogText = "Last week, three of our largest shipments of Tibanna Gas were stolen directly from the safe house. Luckily someone had put a tracker on one of the shipments.",
	stopConversation = "false",
	options = {
		{"Are we going to take action?", "seventh_screenb"}
	}
}
blackSunIntroConvo:addScreen(seventh_screen);



seventh_screenb = ConvoScreen:new {
	id = "seventh_screenb",
	leftDialog = "",
	customDialogText = "Well... YOU are going to take action, not we. I want you to ride out and reclaim what is ours. ",
	stopConversation = "false",
	options = {
		{"I'll get started right away.", "first_quest_accept"}
	}
}
blackSunIntroConvo:addScreen(seventh_screenb);



first_quest_begin = ConvoScreen:new {
	id = "first_quest_begin",
	leftDialog = "",
	customDialogText = "Alright, I'm uploading the coordinates to your datapad. Teach them a lesson: When you poke the bear, the bear bites back.",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(first_quest_begin);



first_quest_complete = ConvoScreen:new {
	id = "first_quest_complete",
	leftDialog = "",
	customDialogText = "So then, did you take care of the thieves?",
	stopConversation = "false",
	options = {
		{"They're dealt with. The Tibanna gas is back in our hands.", "second_quest_intro"}
	}
}
blackSunIntroConvo:addScreen(first_quest_complete);



first_quest_waiting = ConvoScreen:new {
	id = "first_quest_waiting",
	leftDialog = "",
	customDialogText = "Why are you still here? You have work to do.",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(first_quest_complete_screen);



second_quest_intro = ConvoScreen:new {
	id = "second_quest_intro",
	leftDialog = "",
	customDialogText = "It's refreshing to see a new recruit carry out a mission with such little difficulty. Take this as a reward and let's proceed to the next task.",
	stopConversation = "false",
	options = {
		{"I told you I won't dissappoint. I also managed to find this datapad.", "second_quest_continued"}
	}
}
blackSunIntroConvo:addScreen(second_quest_intro);



second_quest_continued = ConvoScreen:new {
	id = "second_quest_continued",
	leftDialog = "",
	customDialogText = "Very impressive. Let me have a look at it... As I thought, the Death Watch are making a move into this system.",
	stopConversation = "false",
	options = {
		{"Is there anything we can do to prevent that?", "second_quest_explain"}	
	}
}
blackSunIntroConvo:addScreen(second_quest_continued);



second_quest_explain = ConvoScreen:new {
	id = "second_quest_explain",
	leftDialog = "",
	customDialogText = "Maybe not us alone, but there is a name that came up that I'm not familiar with: Tyber Zann. It looks like a meeting was supposed to take place between him and the poor sap you just killed.",
	stopConversation = "false",
	options = {
		{"So I guess that meeting isn't going to happened anymore...", "second_quest_explaina"}	
	}
}
blackSunIntroConvo:addScreen(second_quest_explain);


second_quest_explaina = ConvoScreen:new {
	id = "second_quest_explaina",
	leftDialog = "",
	customDialogText = "Actually, it is crucial that this meeting does take place. You will be attending said meeting, and finding any information you can on the Death Watch and their movements.",
	stopConversation = "false",
	options = {
		{"I see, should be easy enough!", "second_quest_begin"}	
	}
}
blackSunIntroConvo:addScreen(second_quest_explaina);



second_quest_begin = ConvoScreen:new {
	id = "second_quest_begin",
	leftDialog = "",
	customDialogText = "Then why are you still here and not heading there now? Get there fast! I'm not paying you for nothing.",
	stopConversation = "true",
	options = {
	}
}
blackSunIntroConvo:addScreen(second_quest_begin);



second_quest_waiting = ConvoScreen:new {
	id = "second_quest_waiting",
	leftDialog = "",
	customDialogText = "Are you going or not?",
	stopConversation = "true",
	options = {
	}
}
blackSunIntroConvo:addScreen(second_quest_waiting);



second_quest_complete_screen = ConvoScreen:new {
	id = "second_quest_complete_screen",
	leftDialog = "",
	customDialogText = "Back so soon? What did you manage to find?",
	stopConversation = "false",
	options = {
		{"The informant was easily persuaded to say the least. He turned over this datapad.", "third_quest_intro"}
	}
}
blackSunIntroConvo:addScreen(second_quest_complete_screen);



third_quest_intro = ConvoScreen:new {
	id = "third_quest_intro",
	leftDialog = "",
	customDialogText = "Hmm... There's some interesting information on this datapad. The Death Watch are as stupid as I presumed, trusting such an informant with this information.",
	stopConversation = "false",
	options = {
		{"What kind of information?", "third_quest_explain"}
	}
}
blackSunIntroConvo:addScreen(third_quest_intro);



third_quest_explain = ConvoScreen:new {
	id = "third_quest_explain",
	leftDialog = "",	
	customDialogText = "It contains the information of one of their bases located here on Naboo. I'm still checking through it though, maybe we can find something else.",	
	stopConversation = "false",	
	options = {	
		{"Any idea what this base would contain?", "third_quest_explainb"}	
	}	
}
blackSunIntroConvo:addScreen(third_quest_explain);



third_quest_explainb = ConvoScreen:new {
	id = "third_quest_explainb",	
	leftDialog = "",	
	customDialogText = "Not yet, but I did find that the security on this base is quite high indeed...",	
	stopConversation = "false",	
	options = {	
		{"Any way you know of to bypass it?", "third_quest_explainc"}	
	}	
}
blackSunIntroConvo:addScreen(third_quest_explainb);



third_quest_explainc = ConvoScreen:new {
	id = "third_quest_explainc",	
	leftDialog = "",	
	customDialogText = "Well, it's using a bioscanner. Luckily, from past intel, I know where to find a commander of the Death Watch!",	
	stopConversation = "false",	
	options = {	
		{"I'm guessing you want me to go and find him.", "third_quest_explaind"}	
	}	
}
blackSunIntroConvo:addScreen(third_quest_explainc);



third_quest_explaind = ConvoScreen:new {
	id = "third_quest_explaind",	
	leftDialog = "",	
	customDialogText = "What else would I be paying you for?",	
	stopConversation = "false",	
	options = {	
		{"You make a good point. What must I do?", "third_quest_begina"}	
	}	
}
blackSunIntroConvo:addScreen(third_quest_explaind);



third_quest_begina = ConvoScreen:new {
	id = "third_quest_begina",
	leftDialog = "",
	customDialogText = "We need to find a way to get his biological signature. In easier terms, kill him and bring me his blood.",
	stopConversation = "false",
	options = {	
		{"Easy enough. I'm on it right away.", "third_quest_begin"}
	}
}
blackSunIntroConvo:addScreen(third_quest_begina);



third_quest_begin = ConvoScreen:new {
	id = "third_quest_begin",
	leftDialog = "",
	customDialogText = "Go now, before word gets out we're after him.",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(third_quest_begin);



third_quest_waiting = ConvoScreen:new {
	id = "third_quest_waiting",
	leftDialog = "",
	customDialogText = "Why haven't you gone yet?",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(third_quest_waiting);



third_quest_complete_screen = ConvoScreen:new {
	id = "third_quest_complete_screen",
	leftDialog = "",
	customDialogText = "Did you manage to get the commander's biosignature?",
	stopConversation = "false",
	options = {
		{"*Hold out vial of blood*", "fourth_quest_intro"}
	}
}
blackSunIntroConvo:addScreen(third_quest_complete_screen);



fourth_quest_intro = ConvoScreen:new {
	id = "fourth_quest_intro",
	leftDialog = "",
	customDialogText = "Great! I don't want it though, you need to keep it for your next task *chuckles*",
	stopConversation = "false",
	options = {
		{"You always have something for me to do don't you?", "fourth_quest_explain"}
	}
}
blackSunIntroConvo:addScreen(fourth_quest_intro);



fourth_quest_explain = ConvoScreen:new {
	id = "fourth_quest_explain",
	leftDialog = "",
	customDialogText = "Well I definitely don't want to be doing it, and you're the one looking to make your way in the Black Sun.",
	stopConversation = "false",
	options = {	
		{"Good point... So then what's next?", "fourth_quest_explainb"}	
	}
}
blackSunIntroConvo:addScreen(fourth_quest_explain);



fourth_quest_explainb = ConvoScreen:new {
	id = "fourth_quest_explainb",
	leftDialog = "",
	customDialogText = "I need you to take the biosignature, get into the base we discussed earlier and set their power generator to self destruct. Oh... One more thing, they might have a Jedi's help.",
	stopConversation = "false",
	options = {
		{"A JEDI?!", "fourth_quest_begin"}
	}
}
blackSunIntroConvo:addScreen(fourth_quest_explainb);



fourth_quest_begin = ConvoScreen:new {
	id = "fourth_quest_begin",
	leftDialog = "",
	customDialogText = "Get used to it bud, you'll be fighting lots of them in the Black Sun. You've impressed me so far, here are the coordinates. Get to it!",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(fourth_quest_begin);



fourth_quest_waiting = ConvoScreen:new {
	id = "fourth_quest_waiting",
	leftDialog = "",
	customDialogText = "I already told you what you need to do!",
	stopConversation = "true",
	options = {}
}
blackSunIntroConvo:addScreen(fourth_quest_waiting);



fourth_quest_complete = ConvoScreen:new {
	id = "fourth_quest_complete",
	leftDialog = "",
	customDialogText = "Is it done? Is the base disabled?",
	stopConversation = "false",
	options = {
		{"Have I disappointed you thus far? Of course it's done.", "fifth_quest_intro"}
	}
}
blackSunIntroConvo:addScreen(fourth_quest_complete);



fifth_quest_intro = ConvoScreen:new {

	id = "fifth_quest_intro",
	leftDialog = "",
	customDialogText = "Great. Up until now you've performed most wonderfully. We've delivered a huge blow to the Death Watch here on Naboo. I'm going to pass the word on to my superiors, but first there's a loose end to tie up.",
	stopConversation = "false",
	options = {
		{"Loose ends? What do you mean?", "fifth_quest_explain"}
	}

}

blackSunIntroConvo:addScreen(fifth_quest_intro);



fifth_quest_explain = ConvoScreen:new {

	id = "fifth_quest_explain",

	leftDialog = "",

	customDialogText = "Apparently Tyber Zann wasn't just an informant, his ties go much deeper. He played you, played you bad. This was supposed to be a trap for you, but they under estimated your strength.",

	stopConversation = "false",

	options = {
	
		{"I feel like I know what's coming next.", "fifth_quest_begin"}
	
	}

}

blackSunIntroConvo:addScreen(fifth_quest_explain);




fifth_quest_begin = ConvoScreen:new {

	id = "fifth_quest_begin",

	leftDialog = "",

	customDialogText = "If you felt like you're going to chase him down, then you're right! Find him, and make him disappear and don't let me down.",

	stopConversation = "true",

	options = {
	}

}

blackSunIntroConvo:addScreen(fifth_quest_begin);





fifth_quest_waiting = ConvoScreen:new {

	id = "fifth_quest_waiting",

	leftDialog = "",

	customDialogText = "Tyber Zann played you once, don't let him get away again!",

	stopConversation = "true",

	options = {
	}

}

blackSunIntroConvo:addScreen(fifth_quest_waiting);





fifth_quest_complete_screen = ConvoScreen:new {

	id = "fifth_quest_complete_screen",

	leftDialog = "",

	customDialogText = "Did you finish him off?",

	stopConversation = "false",

	options = {
	
		{"No... He's hurt bad, but he managed to escape.", "fifth_quest_outro"}
	
	}

}

blackSunIntroConvo:addScreen(fifth_quest_complete_screen);





fifth_quest_outro = ConvoScreen:new {

	id = "fifth_quest_outro",

	leftDialog = "",

	customDialogText = "It's a disappointment to hear, but sometimes they get away. How do I know? Well let's just say I got away more than once. You did well! My boss, Aruk the Great, wants to meet you. It should be a great honor! Our base is on Lok, ill upload the waypoint. Go see him when you can.",

	stopConversation = "true",

	options = {
	}

}

blackSunIntroConvo:addScreen(fifth_quest_outro);

chain_complete_screen = ConvoScreen:new {

	id = "chain_complete_screen",

	leftDialog = "",

	customDialogText = "Head to Lok.",

	stopConversation = "true",

	options = {
	}

}

deathWatchIntroConvo:addScreen(chain_complete_screen);



addConversationTemplate("blackSunIntroConvo", blackSunIntroConvo);




