padawanQuestDark = ConvoTemplate:new {

	initialScreen = "padawan_start_screen",

	templateType = "Lua",

	luaClassHandler = "padawanQuestDark_handler",

	screens = {}

}





--Intro First

padawan_start_screen = ConvoScreen:new {

	id = "padawan_start_screen",

	leftDialog = "",

	customDialogText = "Your instructors tell me you are ready for your Adept trials young Acolyte. My time here is short, so I will be brief. If you chose to continue your training, you will join the ranks of the Sith and be gain access to the Dark Council as a low ranking member. This gives you a say in how the council is run and voting privliages for its Leadership. Leadership of the council changes frequently and one day you could find yourself....well we shall see.",

	stopConversation = "false",

	options = {

		{"To become a high ranking member of the Dark Council would be most pleasing.", "padawan_second_screen"}
	
	}

}



padawanQuestDark:addScreen(padawan_start_screen);

padawan_second_screen = ConvoScreen:new {

	id = "padawan_second_screen",

	leftDialog = "",

	customDialogText = "For now, the Council has tasked you with restoring a ancient Sith Holocron of Knowledge. This Holocron which has rested near the Temple of Exar K'un for centuries is now at risk of being discovered by the Jedi. It is our command that you find the lost fragments of this holocron to restore it, allowing us to use the techniques inside to destroy the Jedi once and for all.",

	stopConversation = "false",

	options = {

		{"At once Lord Vader, where shall I begin my search for the lost fragments?", "padawan_third_screen"},
		{"Who is Exar K'un my lord?", "padawan_exar_screen"}
	
	}

}



padawanQuestDark:addScreen(padawan_second_screen);

padawan_exar_screen = ConvoScreen:new {

	id = "padawan_exar_screen",

	leftDialog = "",

	customDialogText = "Exar K'un was a Dark Lord of the Sith from time past.",

	stopConversation = "false",

	options = {

		{"These lost fragments, where must I search?", "padawan_third_screen"}
	
	}

}



padawanQuestDark:addScreen(padawan_exar_screen);

padawan_third_screen = ConvoScreen:new {

	id = "padawan_third_screen",

	leftDialog = "",

	customDialogText = "The Holocron originated from the Blueleaf Temple. The temple may still contain information about the Holocron. The Holocron was damaged before making its way to Exar K'un, which has kept it from being used so far.",

	stopConversation = "false",

	options = {

		{" By your leave Lord Vader, I will begin my search now. ", "padawan_fourth_screen"}
	
	}

}



padawanQuestDark:addScreen(padawan_third_screen);

padawan_fourth_screen = ConvoScreen:new {

	id = "padawan_fourth_screen",

	leftDialog = "",

	customDialogText = "Do not fail me in this task Acolyte. Now go, I have pressing matters to attend.",

	stopConversation = "true",

	options = {
	
	}

}



padawanQuestDark:addScreen(padawan_fourth_screen);


padawan_not_ready = ConvoScreen:new {

	id = "padawan_not_ready",

	leftDialog = "",

	customDialogText = "You lack the skills to continue, Train Lightsaber Techniques IV, Force Healing IV, Force Defense and Lesser Force Wielding IV before you return to me.",

	stopConversation = "true",

	options = {

	}

}



padawanQuestDark:addScreen(padawan_not_ready);

no_screen = ConvoScreen:new {

	id = "no_screen",

	leftDialog = "",

	customDialogText = "I find your lack of faith distubing, return when you are ready.",

	stopConversation = "yes",

	options = {

	}

}



padawanQuestDark:addScreen(no_screen);

hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "you try my patience, failure will cost you your life.",

	stopConversation = "yes",

	options = {

	}

}



padawanQuestDark:addScreen(hello_screen);

padawan_accepted_screen = ConvoScreen:new {

	id = "padawan_accepted_screen",

	leftDialog = "",

	customDialogText = "May the Force serve you well.",

	stopConversation = "yes",

	options = {

	}

}



padawanQuestDark:addScreen(padawan_accepted_screen)

padawan_return_screen = ConvoScreen:new {

	id = "padawan_return_screen",

	leftDialog = "",

	customDialogText = "Have you completed your task, has the Holocron be restored?",

	stopConversation = "no",

	options = {
		{"Yes my lord, it is done.", "padawan_return_two"}
	}

}



padawanQuestDark:addScreen(padawan_return_screen)

padawan_return_two = ConvoScreen:new {

	id = "padawan_return_two",

	leftDialog = "",

	customDialogText = "What secrets have you discovered from it, and do not lie, the Force allows me to see through them.",

	stopConversation = "no",

	options = {
		{"Lord Vader, the Holocron contains infomation on mind manipulation.", "padawan_return_three"}
	}

}



padawanQuestDark:addScreen(padawan_return_two)

padawan_return_three = ConvoScreen:new {

	id = "padawan_return_three",

	leftDialog = "",

	customDialogText = "Very interesting, what more can you tell me?",

	stopConversation = "no",

	options = {
		{"I encountered a Nightsister who fell mad during her possession of a Shard, Scavengers at Exar K'uns temple who fell ill searching a dig sight for relics and begain chanting 'for the Master' ", "padawan_return_four"}
	}

}



padawanQuestDark:addScreen(padawan_return_three)

padawan_return_four = ConvoScreen:new {

	id = "padawan_return_four",

	leftDialog = "",

	customDialogText = "Very good, According to legend, Exar K'un had the ability to mind control Force users to do his bidding, and with this holocron, we now control that power. However, It would seem Exar K'un's spirit has risen from the grave, We will need to act quickly to defeat this enemy before he gains to much power.",

	stopConversation = "no",

	options = {
		{"What would you have me do my lord?", "padawan_return_five"}
	}

}



padawanQuestDark:addScreen(padawan_return_four)

padawan_return_five = ConvoScreen:new {

	id = "padawan_return_five",

	leftDialog = "",

	customDialogText = "When the time is right, you will strike with us. For now you must keep that Holocron hidden and safe.",

	stopConversation = "no",

	options = {
		{"Certainly", "padawan_return_six"}
	}

}



padawanQuestDark:addScreen(padawan_return_five)

padawan_return_six = ConvoScreen:new {

	id = "padawan_return_six",

	leftDialog = "",

	customDialogText = "You have done well, I grant you the rank of Sith Adept. You may now enter the councils chamber where new trainers await you.",

	stopConversation = "no",

	options = {
		{"Thank you, Lord Vader.", "padawan_return_seven"}
	}

}



padawanQuestDark:addScreen(padawan_return_six)

padawan_return_seven = ConvoScreen:new {

	id = "padawan_return_seven",

	leftDialog = "",

	customDialogText = "In addition, Im giving you an Adept robe construction kit and aligning you Empire. It is with the coperation between the Sith and the Empire that we will crush the rebels and defeat the Jedi. May the Force serve you well Adept.",

	stopConversation = "yes",

}



padawanQuestDark:addScreen(padawan_return_seven)

padawan_complete_screen = ConvoScreen:new {

	id = "padawan_complete_screen",

	leftDialog = "",

	customDialogText = "Speak to me when you have mastered two Sith professions, then we shall begin your next trials.",

	stopConversation = "yes",

}



padawanQuestDark:addScreen(padawan_complete_screen)

padawan_failure_screen = ConvoScreen:new {

	id = "padawan_failure_screen",
	leftDialog = "",
	customDialogText = "Don't fail me again.",
	stopConversation = "no",
	options = {
		{"Yes my Lord", "padawan_failure_two"}
	}
}
padawanQuestDark:addScreen(padawan_failure_screen)

padawan_failure_two = ConvoScreen:new {

	id = "padawan_failure_two",
	leftDialog = "",
	customDialogText = "Now go.",
	stopConversation = "yes",
}
padawanQuestDark:addScreen(padawan_failure_two)





addConversationTemplate("padawanQuestDark", padawanQuestDark);


