sith_daily_one = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "sith_daily_one_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "You, Come here!",
	stopConversation = "false",
	options = {
		{"Yes my Lord?", "second_screen"},
	}
}
sith_daily_one:addScreen(greet_friend);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "A Jedi Scout has been seen near here, go kill him.",
	stopConversation = "false",
	options = {
		{"As you command my Lord.", "accept_screen"},
	}
}
sith_daily_one:addScreen(second_screen);

accept_screen = ConvoScreen:new {
	id = "accept_screen",
	leftDialog = "",
	customDialogText = "I await your return.",
	stopConversation = "true",
	options = {}
}
sith_daily_one:addScreen(accept_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "Were you successful?",
	stopConversation = "false",
	options = {
		{"He will not bother us anymore my Lord.", "complete_screen_final"},
	}
}
sith_daily_one:addScreen(complete_screen);

complete_screen_final = ConvoScreen:new {
	id = "complete_screen_final",
	leftDialog = "",
	customDialogText = "Excellent! Here take this as a token of my gratitude.",
	stopConversation = "true",
	options = {}
}
sith_daily_one:addScreen(complete_screen_final);


waiting_screen = ConvoScreen:new {
	id = "waiting_screen",
	leftDialog = "",
	customDialogText = "Go now, deal with this intruder.",
	stopConversation = "true",
	options = {}
}
sith_daily_one:addScreen(waiting_screen);

quest_done = ConvoScreen:new {
	id = "quest_done",
	leftDialog = "",
	customDialogText = "Looking for more scraps are we, come back later.",
	stopConversation = "true",
	options = {}
}
sith_daily_one:addScreen(quest_done);

addConversationTemplate("sith_daily_one", sith_daily_one);

