deathWatchIntroConvo = ConvoTemplate:new {
	initialScreen = "first_screen",
	templateType = "Lua",
	luaClassHandler = "deathWatchIntroConvo_handler",
	screens = {}
}
--Intro Fir
first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "What?",
	stopConversation = "false",
	options = {
		{"I hear you are a recruiter for the Death Watch", "second_screen"},
	}
}
deathWatchIntroConvo:addScreen(first_screen);

jedi_screen = ConvoScreen:new {
	id = "jedi_screen",
	leftDialog = "",
	customDialogText = "Move along, Friend.",
	stopConversation = "true",
	options = {}
}
deathWatchIntroConvo:addScreen(jedi_screen);

accepted_screen = ConvoScreen:new {

	id = "accepted_screen",

	leftDialog = "",

	customDialogText = "Are you going or not?",

	stopConversation = "true",

	options = {

	}

}

deathWatchIntroConvo:addScreen(accepted_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Death Watch..hrmmm *Jekk sizes you up*",
	stopConversation = "false",
	options = {
		{"...", "third_screen"}
	}
}
deathWatchIntroConvo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "You think you are cut out to be one of the finest warriors in the galaxy?",
	stopConversation = "false",
	options = {
		{"I am ready to learn from the best", "forth_screena"},
		{"I am one of the finest warriors in the galaxy", "forth_screenb"}
	}
}
deathWatchIntroConvo:addScreen(third_screen);

forth_screena = ConvoScreen:new {
	id = "forth_screena",
	leftDialog = "",
	customDialogText = "Hrm.. Best indeed, but you must earn your worth",
	stopConversation = "false",
	options = {
		{"What ever it takes.", "fifth_screen"}
	}
}
deathWatchIntroConvo:addScreen(forth_screena);



forth_screenb = ConvoScreen:new {
	id = "forth_screenb",
	leftDialog = "",
	customDialogText = "Ha..haha, Don't figure you noticed my blaster pistol pointing at you?",
	stopConversation = "false",
	options = {
		{"Do..Don't shoot, I want to join you not fight you", "sub_fifth_screen"}
	}
}
deathWatchIntroConvo:addScreen(forth_screenb);



sub_fifth_screen = ConvoScreen:new {
	id = "sub_fifth_screen",
	leftDialog = "",
	customDialogText = "You have some spirit, but in the Death Watch you must earn your worth",
	stopConversation = "false",
	options = {
		{"Of course, my resources are yours.", "fifth_screen"}
	}
}
deathWatchIntroConvo:addScreen(sub_fifth_screen);

fifth_screen = ConvoScreen:new {
	id = "fifth_screen",
	leftDialog = "",
	customDialogText = "Outside Mos Eisley a Swoop gang has been intercepting our weapon shipments. I was about to pay them a visit, but how about you go instead?",
	stopConversation = "false",
	options = {
		{"How would you like me to deal with them?", "sixth_screen"}

	}
}
deathWatchIntroConvo:addScreen(fifth_screen);

sixth_screen = ConvoScreen:new {
	id = "sixth_screen",
	leftDialog = "",
	customDialogText = "Send them a message: No one interrupts Death Watch business.",
	stopConversation = "false",
	options = {
		{"Understood.", "seventh_screen"}
	}
}
deathWatchIntroConvo:addScreen(sixth_screen);

seventh_screen = ConvoScreen:new {
	id = "seventh_screen",
	leftDialog = "",
	customDialogText = "Here are the coordinates, I'll be waiting.",
	stopConversation = "true",
	options = {}
}
deathWatchIntroConvo:addScreen(seventh_screen);

first_quest_complete_screen = ConvoScreen:new {
	id = "first_quest_complete_screen",
	leftDialog = "",
	customDialogText = "Hrmm, I hear that swoop gang disapeared overnight. You got my attention now.",
	stopConversation = "false",
	options = {
		{"Easier than I expected.", "second_quest_intro"}
	}
}

deathWatchIntroConvo:addScreen(first_quest_complete_screen);

second_quest_intro = ConvoScreen:new {

	id = "second_quest_intro",

	leftDialog = "",

	customDialogText = "Well this second task wont be so easy... The Black Sun are trying to get a foothold on Tatooine.",

	stopConversation = "false",

	options = {

		{"Just tell me what you need me to do.", "second_quest_continued"},

		{"The Black Sun?", "second_quest_explain"}
	}

}

deathWatchIntroConvo:addScreen(second_quest_intro);



second_quest_explain = ConvoScreen:new {

	id = "second_quest_explain",
	
	leftDialog = "",
	
	customDialogText = "The Black Sun have been around for quite a long time, and are one of the most notorious crime syndicates in the galaxy.",
	
	stopConversation = "false",
	
	options = {
	
		{"How are they affecting you?", "second_quest_explainb"}
		
	}
	
}

deathWatchIntroConvo:addScreen(second_quest_explain);



second_quest_explainb = ConvoScreen:new {

	id = "second_quest_explainb",
	
	leftDialog = "",
	
	customDialogText = "They've secretly started working with the Empire on Tatooine and using their new ally to try to prevent other factions from rising to power.",
	
	stopConversation = "false",
	
	options = {
	
		{"Understood, How can I help?", "second_quest_continued"}
		
	}
	
}

deathWatchIntroConvo:addScreen(second_quest_explainb);



second_quest_continued = ConvoScreen:new {

	id = "second_quest_continued",
	
	leftDialog = "",
	
	customDialogText = "We have a contact in Bestine, goes by the name of Zeek. He wants out of the Black Sun and is seeking asylum with us.",
	
	stopConversation = "false",
	
	options = {
	
		{"Where do I come in?", "second_quest_begin"}
		
	}
	
}

deathWatchIntroConvo:addScreen(second_quest_continued);



second_quest_begin = ConvoScreen:new {

	id = "second_quest_begin",

	leftDialog = "",

	customDialogText = "I need you to see what information you can obtain from Zeek. Return anything you can get out of him to me.",

	stopConversation = "true",

	options = {

	}

}

deathWatchIntroConvo:addScreen(second_quest_begin);



second_quest_waiting = ConvoScreen:new {

	id = "second_quest_waiting",

	leftDialog = "",

	customDialogText = "Are you going or not?",

	stopConversation = "true",

	options = {

	}

}

deathWatchIntroConvo:addScreen(second_quest_waiting);



second_quest_complete_screen = ConvoScreen:new {

	id = "second_quest_complete_screen",

	leftDialog = "",

	customDialogText = "You got the datapad with the information we need, good job.",

	stopConversation = "false",

	options = {

		{"Once again, too easy.", "third_quest_intro"}

	}

}

deathWatchIntroConvo:addScreen(second_quest_complete_screen);



third_quest_intro = ConvoScreen:new {

	id = "third_quest_intro",

	leftDialog = "",

	customDialogText = "Easy? Something doesn't seem right.",

	stopConversation = "false",

	options = {

		{"What do you mean by that?", "third_quest_explain"}

	}

}

deathWatchIntroConvo:addScreen(third_quest_intro);



third_quest_explain = ConvoScreen:new {

	id = "third_quest_explain",
	
	leftDialog = "",
	
	customDialogText = "If the Black Sun knew that Zeek surrendered this information, they'd have his head...",
	
	stopConversation = "false",
	
	options = {
	
		{"So?", "third_quest_explainb"}
		
	}
	
}

deathWatchIntroConvo:addScreen(third_quest_explain);



third_quest_explainb = ConvoScreen:new {

	id = "third_quest_explainb",
	
	leftDialog = "",
	
	customDialogText = "So... I don't think his intentions were to actually help you. He may be leading you into a trap. Let me analyze this data first.",
	
	stopConversation = "false",
	
	options = {
	
		{"Take your time.", "third_quest_explainc"}
		
	}
	
}

deathWatchIntroConvo:addScreen(third_quest_explainb);



third_quest_explainc = ConvoScreen:new {

	id = "third_quest_explainc",
	
	leftDialog = "",
	
	customDialogText = "As I thought, they're trying to lead you to your death. We need to act one step ahead of them. Luckily, I managed to find another location embedded deep in the data.",
	
	stopConversation = "false",
	
	options = {
	
		{"Out with it, what do you need me to do?", "third_quest_explaind"}
		
	}
	
}

deathWatchIntroConvo:addScreen(third_quest_explainc);



third_quest_explaind = ConvoScreen:new {

	id = "third_quest_explaind",
	
	leftDialog = "",
	
	customDialogText = "This location I found matches the location we had of a suspected Black Sun base in the Dune Sea. You are going to infiltrate this base and show them who they're up against.",
	
	stopConversation = "false",
	
	options = {
	
		{"Any tips on how I can get inside?", "third_quest_begina"},
		
		{"I don't know if I want to put myself in that position...", "third_quest_explaine"}
		
	}
	
}

deathWatchIntroConvo:addScreen(third_quest_explaind);



third_quest_explaine = ConvoScreen:new {

	id = "third_quest_explaine",
	
	leftDialog = "",
	
	customDialogText = "It's too late now, you know too much. Back out now and you will be hunted by both the Death Watch and the Black Sun.",
	
	stopConversation = "false",
	
	options = {
	
		{"Okay okay... Tell me where to go, and what to do.", "third_quest_begina"}
		
	}
	
}

deathWatchIntroConvo:addScreen(third_quest_explaine);



third_quest_begina = ConvoScreen:new {

	id = "third_quest_begina",

	leftDialog = "",

	customDialogText = "There's a Black Sun commander that's been harrassing me ever since he landed on this desolate planet. His biosignature should pass the scanners, and luckily I know where he is. Find him, kill him, and bring me his blood.",

	stopConversation = "false",

	options = {
	
		{"Understood, I'll make quick work of this.", "third_quest_begin"}

	}

}

deathWatchIntroConvo:addScreen(third_quest_begina);



third_quest_begin = ConvoScreen:new {

	id = "third_quest_begin",

	leftDialog = "",

	customDialogText = "His location has been uploaded to your datapad. Don't come back to me unless you succeed.",

	stopConversation = "true",

	options = {

	}

}

deathWatchIntroConvo:addScreen(third_quest_begin);



third_quest_waiting = ConvoScreen:new {

	id = "third_quest_waiting",

	leftDialog = "",

	customDialogText = "Is he dead? No? Then why are you here?",

	stopConversation = "true",

	options = {

	}

}

deathWatchIntroConvo:addScreen(third_quest_waiting);



third_quest_complete_screen = ConvoScreen:new {

	id = "third_quest_complete_screen",

	leftDialog = "",

	customDialogText = "I expected you would succeed, you've shown nothing but promise so far.",

	stopConversation = "false",

	options = {

		{"I only wish to prove my worth.", "fourth_quest_intro"}

	}

}

deathWatchIntroConvo:addScreen(third_quest_complete_screen);



fourth_quest_intro = ConvoScreen:new {

	id = "fourth_quest_intro",

	leftDialog = "",

	customDialogText = "You have indeed been worthy so far... This next task will prove whether you're fit for the Death Watch or not. It's time for you to head to the Black Sun base.",

	stopConversation = "false",

	options = {

		{"Something in your tone tells me this won't be as easy...", "fourth_quest_explain"}

	}

}

deathWatchIntroConvo:addScreen(fourth_quest_intro);



fourth_quest_explain = ConvoScreen:new {

	id = "fourth_quest_explain",

	leftDialog = "",

	customDialogText = "Well... We have intel suggesting that the Black Sun have a Dark Force wielder among them, along with the already well trained guards. Make your own deductions.",

	stopConversation = "false",

	options = {
	
		{"Sounds like a good time...", "fourth_quest_explainb"}
	
	}

}

deathWatchIntroConvo:addScreen(fourth_quest_explain);



fourth_quest_explainb = ConvoScreen:new {

	id = "fourth_quest_explainb",

	leftDialog = "",

	customDialogText = "*chuckles* A sense of humour, I like it. Your job is going to be to fight your way in, and find a way to destroy the power generator.",

	stopConversation = "false",

	options = {
	
	{"It will be done.", "fourth_quest_begin"}
	
	}

}

deathWatchIntroConvo:addScreen(fourth_quest_explainb);



fourth_quest_begin = ConvoScreen:new {

	id = "fourth_quest_begin",

	leftDialog = "",

	customDialogText = "I'm starting to believe you have what it takes. I've uploaded the waypoint to your datapad, good luck.",

	stopConversation = "true",

	options = {}

}

deathWatchIntroConvo:addScreen(fourth_quest_begin);



fourth_quest_waiting = ConvoScreen:new {

	id = "fourth_quest_waiting",

	leftDialog = "",

	customDialogText = "Come speak to me once you've completed your task.",

	stopConversation = "true",

	options = {}

}

deathWatchIntroConvo:addScreen(fourth_quest_waiting);



fourth_quest_complete_screen = ConvoScreen:new {

	id = "fourth_quest_complete_screen",

	leftDialog = "",

	customDialogText = "I see you're still alive, did you take care of the base?",

	stopConversation = "false",

	options = {
	
		{"I did. It wasn't easy, but it's done.", "fifth_quest_intro"}
	
	}

}

deathWatchIntroConvo:addScreen(fourth_quest_complete_screen);



fifth_quest_intro = ConvoScreen:new {

	id = "fifth_quest_intro",

	leftDialog = "",

	customDialogText = "Great. Up until now you've performed most admirably. We've delivered a huge blow to the Black Sun here on Tatooine. I'm going to pass the word on to my superiors, but first you must complete this final task.",

	stopConversation = "false",

	options = {
	
		{"What does this final task consist of?", "fifth_quest_explain"}
	
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_intro);



fifth_quest_explain = ConvoScreen:new {

	id = "fifth_quest_explain",

	leftDialog = "",

	customDialogText = "Our friend Zeek is running scared after he saw what we've done to the Black Sun. Lucky for us we have connections... We have him grounded in Eisley, he's not going anywhere. ",

	stopConversation = "false",

	options = {
	
		{"I feel like I know what's coming next.", "fifth_quest_begin"}
	
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_explain);




fifth_quest_begin = ConvoScreen:new {

	id = "fifth_quest_begin",

	leftDialog = "",

	customDialogText = "If you felt like you're going to chase him down, then you're right! Find him, and make him disappear and don't let me down.",

	stopConversation = "true",

	options = {
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_begin);





fifth_quest_waiting = ConvoScreen:new {

	id = "fifth_quest_waiting",

	leftDialog = "",

	customDialogText = "You do know Zeek isn't going to kill himself? I wish he would...",

	stopConversation = "true",

	options = {
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_waiting);





fifth_quest_complete = ConvoScreen:new {

	id = "fifth_quest_complete_screen",

	leftDialog = "",

	customDialogText = "Did you finish him off?",

	stopConversation = "false",

	options = {
	
		{"No... He's hurt bad, but he managed to escape.", "fifth_quest_outro"}
	
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_complete);





fifth_quest_outro = ConvoScreen:new {

	id = "fifth_quest_outro",

	leftDialog = "",

	customDialogText = "As much as I want him dead, you performed much better than I had expected. You will make a fine addition to the Death Watch, I'm sure of it. Seek out my boss, Tungo Li, on Endor. He will accept you into the Death Watch.",

	stopConversation = "true",

	options = {
	}

}

deathWatchIntroConvo:addScreen(fifth_quest_outro);

chain_complete_screen = ConvoScreen:new {

	id = "chain_complete_screen",

	leftDialog = "",

	customDialogText = "Head to Endor.",

	stopConversation = "true",

	options = {
	}

}

deathWatchIntroConvo:addScreen(chain_complete_screen);



addConversationTemplate("deathWatchIntroConvo", deathWatchIntroConvo);




