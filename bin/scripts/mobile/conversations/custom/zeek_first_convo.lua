zeekFirstConvo = ConvoTemplate:new {
	initialScreen = "first_screen",
	templateType = "Lua",
	luaClassHandler = "zeekFirstConvo_handler",
	screens = {}

}
--Intro Frst
first_screen = ConvoScreen:new {
	id = "first_screen",
	leftDialog = "",
	customDialogText = "What?",
	stopConversation = "false",
	options = {
		{"Zeek?", "second_screen"}
	}
}
zeekFirstConvo:addScreen(first_screen);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "Uhm... Who's asking?",
	stopConversation = "false",
	options = {
		{"I'm here on the behalf of the Death Watch. I heard you want out of the Black Sun?", "third_screen"},
	}
}
zeekFirstConvo:addScreen(second_screen);

third_screen = ConvoScreen:new {
	id = "third_screen",
	leftDialog = "",
	customDialogText = "I've been hoping one of you would come by. You have to get me out of this! I really don't agree with their involvement with the Empire...",
	stopConversation = "false",
	options = {
		{"What are you going to do for us to prove your allegiance?", "zeek_explain"},
		{"Why don't you agree with the Empire being involved?", "zeek_explaina"}
	}
}
zeekFirstConvo:addScreen(third_screen);

zeek_explaina = ConvoScreen:new {
	id = "zeek_explaina",
	leftDialog = "",
	customDialogText = "The Empire are getting a strong foothold on Tatooine with the Black Sun. To make matters worse, the Hutts are starting to use the Black Sun crime lords to their advantage.",
	stopConversation = "false",
	options = {
		{"So then what can you do for us to prove your allegiance?", "zeek_explain"}
	}
}
zeekFirstConvo:addScreen(zeek_explaina);

zeek_explain = ConvoScreen:new {
	id = "zeek_explain",
	leftDialog = "",
	customDialogText = "Well luckily, I managed to obtain a datapad containing information about their movements and bases on planet. I'll give it to you if you agree to give me asylum with the Death Watch.",
	stopConversation = "false",
	options = {
		{"A fair trade. It's a deal.", "final_screen"}
	}
}
zeekFirstConvo:addScreen(zeek_explain);

final_screen = ConvoScreen:new {
	id = "final_screen",
	leftDialog = "",
	customDialogText = "Okay here it is. I'll uh... wait here until you manage to come back to me.",
	stopConversation = "true",
	options = {}
}
zeekFirstConvo:addScreen(final_screen);

goaway_screen = ConvoScreen:new {
	id = "goaway_screen",
	leftDialog = "",
	customDialogText = "Leave me be for now...",
	stopConversation = "true",
	options = {}
}
zeekFirstConvo:addScreen(goaway_screen);

complete_screen = ConvoScreen:new {
	id = "complete_screen",
	leftDialog = "",
	customDialogText = "I gave you what you wanted already!",
	stopConversation = "true",
	options = {}
}
zeekFirstConvo:addScreen(complete_screen);

addConversationTemplate("zeekFirstConvo", zeekFirstConvo);
