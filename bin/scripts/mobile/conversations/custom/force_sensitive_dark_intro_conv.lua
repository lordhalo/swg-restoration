--[[







Main characters:







Lord Tikqot:



Thiel is new to the Village, he mainly serves to give the starting players history about what's going on and gear you towards progressing as a Jedi and the training.







Lord Yanon:



La'Guo serves only the purpose to ensure they know everything about the new sabers and the crystals.







Lord Juwu:



Paemos gives you more history about the Force and that all Jedi are connected. He will give you knowledge about Force Skills.







Defense Lord Whikul:



Sarg. will give you more knowledge about the defender skill







Lord Shisil:



Noldan will talk about general Jedi skills, covering anything not covered by prior trainers.







Rohak:



Rohak will be used to let the player know that they need to do, what their new purpose is.







Mellichae:



His plays the role of the main evil, he will attempt to stop the player.











Pre Plot:



You are a Force Sensitive brought to Dathomir to learn about the Jedi way, you are fresh and new to this way of life and they must show you the way.







Main plot:



You will go through all the training from the mentors at the Village, you will learn that an evil has presented itself to you and that you must do something about it.



You will find your purpose and learn that you must leave the village to save the village and the Jedi.







End Plot:



upon leaving the village you will be presented a chance to become dark, exile, or light. where-ever you go afterwards when you do decide to join a team you will begin your next story.











]]







force_sensitive_dark_intro_conv = ConvoTemplate:new {

	initialScreen = "intro_first_screen",

	templateType = "Lua",

	luaClassHandler = "force_sensitive_dark_convo_handler",

	screens = {}



}

--Intro First



intro_first_screen = ConvoScreen:new {

	id = "intro_first_screen",

	leftDialog = "",

	customDialogText = "Another pathetic lifeform dares to enter this sacred temple! Speak your next words carefully or forfeit your life.",

	stopConversation = "false",

	options = {

		{"My words and my life are yours my lord. I'm here to discover my true potential and become a sith.", "tikqot2"},

		{"My apologies Lord Tikqot, I am unworthy to enter this sacred hall.","deny"}

	}

}

force_sensitive_dark_intro_conv:addScreen(intro_first_screen);

--deny



deny = ConvoScreen:new {

	id = "deny",

	leftDialog = "",

	customDialogText = "Away with you! If you waste my time again, you die where you stand! Now go!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(deny);

--tikqot2



tikqot2 = ConvoScreen:new {

	id = "tikqot2",

	leftDialog = "",

	customDialogText =  "We shall see. The dark side is full of secrets, some known to us and yet more to be discovered. If you choose to follow this path, learn quick, for others of our kind as well as the vile jedi will strike you down if you are weak. What say you now, join us and command limitless power or cower in fear?",

	stopConversation = "false",

	options = {

		{"Weakness and fear are useless to me, power is what i crave! When do i begin?","tikqot3"},

		{"My lord, my timing is premature, I will return when i am ready to continue.","deny"}

	}

}

force_sensitive_dark_intro_conv:addScreen(tikqot2);

--thiel3



tikqot3 = ConvoScreen:new {

	id = "tikqot3",

	leftDialog = "",

	customDialogText = "Never under estimate weakness or fear, they are powerful tools that a sith can use to draw upon the darkside. This temple, and those who dwell within, will teach you the ways of our order. Listen to your instructors for failure is sevrely punished and success is well rewarded.",

	stopConversation = "false",

	options = {

		{"Yes my lord,","tikqot4"}

	}

}

force_sensitive_dark_intro_conv:addScreen(tikqot3);

--thiel4



tikqot4 = ConvoScreen:new {

	id = "tikqot4",

	leftDialog = "",

	customDialogText = "We will teach all you need to know to become a sith. But as I have said before, you will not be coddled. If you choose to leave this temple without being fully trained, you will do so with no hinderance from us.",

	stopConversation = "false",

	options = {

		{"I understand, those who venture from these halls to soon deserve to die for their stupidity. The sith will be stronger without them!","tikqot5"}

	}

}

force_sensitive_dark_intro_conv:addScreen(tikqot4);

--thiel5



tikqot5 = ConvoScreen:new {

	id = "tikqot5",

	leftDialog = "",

	customDialogText = "Good, I sense great potential within you. Here is your 'Mentor's Crystal' you will need this during your training. It will serve as a guide for you during your stay here, consult it if you lose your way or allow weakness to take hold.",

	stopConversation = "false",

	options = {

		{"I'm sure weaker beings will find this usefull.","tikqot6"}

	}

}

force_sensitive_dark_intro_conv:addScreen(tikqot5);

--thiel6



tikqot6 = ConvoScreen:new {

	id = "tikqot6",

	leftDialog = "",

	customDialogText = "HA ha ha, you amuse me! Now, before I let you go I must warn you about what is going on in the galaxy. We Sith are under constant attack by the Jedi, Bounty Hunters and commoners who despise us. Its very important that outside this temple you identity remains hidden. For if anyone sees you using your powers or holding a lightsaber they might attack, or report you to the Bounty Hunter's Guild.",

	stopConversation = "false",

	options = {

		{"I will defeat all who stand in my way, but I will heade your warning master.","tikqotFinal"},

	}

}

force_sensitive_dark_intro_conv:addScreen(tikqot6);

--thielFinal



tikqotFinal = ConvoScreen:new {

	id = "tikqotFinal",

	leftDialog = "",

	customDialogText = "I've wasted enough time with you, now go! Lord Whikul is waiting for you with your first lesson.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(tikqotFinal);

--complete



complete = ConvoScreen:new {

	id = "complete",

	leftDialog = "",

	customDialogText = "You try my patience, go seek out Lord Whikul.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(complete);

--not yet



not_yet = ConvoScreen:new {

	id = "not_yet",

	leftDialog = "",

	customDialogText = "Your weakness sickens me!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(not_yet);



crystal_screen = ConvoScreen:new {

	id = "crystal_screen",

	leftDialog = "",

	customDialogText = "Even a gungan can remember to use their crystal, how about you?",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(crystal_screen);



return_screen = ConvoScreen:new {

	id = "return_screen",

	leftDialog = "",

	customDialogText = "I must say, your progress is quite impressive, you are almost ready to become a Sith Acolyte. There are a few things you need to do before your Acolyte trials begin, are you ready?",

	stopConversation = "false",

	options = {

		{"I am my lord.","return_screen2"},

		{"Your wasting my time, lets begin.","return_screen2"},

	}

}

force_sensitive_dark_intro_conv:addScreen(return_screen);



return_screen2 = ConvoScreen:new {

	id = "return_screen2",

	leftDialog = "",

	customDialogText = "Outside this temple we keep practice drones for new recruits to train on, One task you have yet to learn is training a skill. You need to practice on the droids until you have enough experience to learn Force Knowledge IV. Then Saber Master, Vlixu will expect you to have all the required components for a Lightsaber so I suggest you get Lightsaber Knowledge IV before speaking with her.",

	stopConversation = "false",

	options = {

		{"Understood.","fs_done"}

	}

}

force_sensitive_dark_intro_conv:addScreen(return_screen2);



fs_done = ConvoScreen:new {

	id = "fs_done",

	leftDialog = "",

	customDialogText = "May the force serve you well, for next we meet, a full Sith you will be.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_intro_conv:addScreen(fs_done);



addConversationTemplate("force_sensitive_dark_intro_conv", force_sensitive_dark_intro_conv);







--####################################################################		Whikul 	##########################################################################



















force_sensitive_dark_defense_conv = ConvoTemplate:new {

	initialScreen = "defense_first_screen",

	templateType = "Lua",

	luaClassHandler = "force_sensitive_dark_defense_conv_handler",

	screens = {}



}

--Def First



defense_first_screen = ConvoScreen:new {

	id = "defense_first_screen",

	leftDialog = "",

	customDialogText = "Ah, a fresh meat. Lets see if you fare better than my last victim, I mean trainee ha ha ha! Shall we begin?",

	stopConversation = "false",

	options = {

		{"Enough words, Lets begin","def1"},

		{"Defense Master indeed, why not fight our enemies instead of hiding here?","info1"},

		{"I am unworthy of your training my lord, I shall return when i am ready.","defense_deny"}

	}

}

force_sensitive_dark_defense_conv:addScreen(defense_first_screen);

--defense deny



defense_deny = ConvoScreen:new {

	id = "defense_deny",

	leftDialog = "",

	customDialogText = "Pathetic, servile scum, I should crush you beneath my heal. Do not return unless you're prepared.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(defense_deny);



info1 = ConvoScreen:new {

	id = "info1",

	leftDialog = "",

	customDialogText = "Hold your tounge you insulent wretch, lest you find your body broken by my hands! I do not hide from anything! A defense master not only spreads death on the field of battle, it is his duty to pass on his knowledge to future Sith.",

	stopConversation = "false",

	options = {

		{"My apologies Lord, what use is the force to one such as you?","info2"}	

	}

}

force_sensitive_dark_defense_conv:addScreen(info1);



info2 = ConvoScreen:new {

	id = "info2",

	leftDialog = "",

	customDialogText = "As a master of defense, the force is one of my primary shields. What you learn from me will keep your head attached to your body. Are you ready?",

	stopConversation = "false",

	options = {

		{"Yes, my lord.","def1"}	

	}

}

force_sensitive_dark_defense_conv:addScreen(info2);

--def1



def1 = ConvoScreen:new {

	id = "def1",

	leftDialog = "",

	customDialogText = "Tell me, what good are you in a fight without a Lightsaber, what weapons do you posses to defeat me? ",

	stopConversation = "false",

	options = {

	{"All I need is the force to destroy you!","def2"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def1);

--def2



def2 = ConvoScreen:new {

	id = "def2",

	leftDialog = "",

	customDialogText = "Yes, the force can service you well in a fight, but it cannot protect you from every attack. You must condition yourself in martial skills to stay alive as well.",

	stopConversation = "false",

	options = {

	{"My body must be a temple which the force feeds, I understand.","def3"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def2);

--def3



def3 = ConvoScreen:new {

	id = "def3",

	leftDialog = "",

	customDialogText = "Excellent, you are more perceptive than most. You must train yourself in both long range and close range combat! They are two completely different aspects of defence, and you will become expert in both areas. If you're not prepared to defend yourself where the Force cannot, you will die.",

	stopConversation = "false",

	options = {

	{"So I must learn to fight without the force as well as with it?","def4"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def3);

--def4



def4 = ConvoScreen:new {

	id = "def4",

	leftDialog = "",

	customDialogText = "If your enemies find you weakened and your not able to draw on the Force you very well could be destroyed. If you train yourself as we spoke... You will be able to overcome that.",

	stopConversation = "false",

	options = {

	{"So as a Sith, I have a greater responsibility to protect myself with and without the Force, as my enemies rely on me only drawing on the Force for protection.","def5"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def4);

--def5



def5 = ConvoScreen:new {

	id = "def5",

	leftDialog = "",

	customDialogText = "You catch on quick. Practice what we have spoken about, and I assure you it will save your life one day.",

	stopConversation = "false",

	options = {

	{"I now see why you are so revered as a defence master, your lessons will not be taken lightly.","def6"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def5);

--def6



def6 = ConvoScreen:new {

	id = "def6",

	leftDialog = "",

	customDialogText = "A test is in order! Kill this Slave, but be mindful of your technique and soon you will learn to deflect and redirect attacks back at your opponents.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(def6);

--defFinal



defFinalOne = ConvoScreen:new {

	id = "defFinalOne",

	leftDialog = "",

	customDialogText = "Most impressive, you aren't as pathetic as I thought. Tell me, did you feel anything?",

	stopConversation = "false",

	options = {

	{"I felt, I felt hatred, fear, power.","defFinal"}

	}

}

force_sensitive_dark_defense_conv:addScreen(defFinalOne);

--defFinal



defFinal = ConvoScreen:new {

	id = "defFinal",

	leftDialog = "",

	customDialogText = "Good, In time you will learn to control those feelings. Unlike the Jedi, a Sith can feed upon those feelings to become stronger. I have updated your Mentor's Crystal... Go seek out Lord Yanon, he awaits your arrival as we speak.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(defFinal);

--complete



complete = ConvoScreen:new {

	id = "complete",

	leftDialog = "",

	customDialogText = "I tire of of the sound of your flesh upon my fists, go speak with Lord Yanon.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(complete);



complete2 = ConvoScreen:new {

	id = "complete2",

	leftDialog = "",

	customDialogText = "I see your skills have increased, a great Sith you will become.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(complete2);



not_yet = ConvoScreen:new {

	id = "not_yet",

	leftDialog = "",

	customDialogText = "Leave me, those greater than you seek my wisdom!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(not_yet);



def_part2 = ConvoScreen:new {

	id = "def_part2",

	leftDialog = "",

	customDialogText = "Your alive, I see my lessons serve you well.",

	stopConversation = "false",

	options = {

		{"My enemies fall at my feet master.","def_part3"},

		{"I've much to learn my lord, but your lessons are helping.","def_part3"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def_part2);



def_part2_crystal = ConvoScreen:new {

	id = "def_part2_crystal",

	leftDialog = "",

	customDialogText = "Consult your crystal before wasting my time!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(def_part2_crystal);



def_part3 = ConvoScreen:new {

	id = "def_part3",

	leftDialog = "",

	customDialogText = "Your time with me is near completion, your have learned enough to receive your first task, Inside the Enclave you will meet Lulzi who will spar with you before sending you outside on a mission. But before you go, I will teach you how to use Force Focus. With this new ability, you will sustain less damage when hit which could be the difference between living and dying.",

	stopConversation = "false",

	options = {

		{"Thank you my lord.","def_part4"}

	}

}

force_sensitive_dark_defense_conv:addScreen(def_part3);



def_part4 = ConvoScreen:new {

	id = "def_part4",

	leftDialog = "",

	customDialogText = "Now go, use your Mentor Crystal, then meet with Lulzi.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_defense_conv:addScreen(def_part4);





addConversationTemplate("force_sensitive_dark_defense_conv", force_sensitive_dark_defense_conv);



--#############################################			laguo				#################################





force_sensitive_dark_craft_conv = ConvoTemplate:new {

	initialScreen = "craft_first_screen",

	templateType = "Lua",

	luaClassHandler = "force_sensitive_dark_craft_conv_handler",

	screens = {}

}

--First



craft_first_screen = ConvoScreen:new {

	id = "craft_first_screen",

	leftDialog = "",

	customDialogText = "I am Lord Yanon, Sith Blade Master. I am going to teach you everything there is to know about Lightsabers and how to use them. Ah, the smell of burnt flesh as you cleave your adversary in two, there is nothing more intoxicating. What say you, are you ready to begin?",

	stopConversation = "false",

	options = {

		{"Finally some real training! Yes my lord lets begin.","craft1"},

		{"No my lord, I am not.","craft_deny"}	

	}

}



force_sensitive_dark_craft_conv:addScreen(craft_first_screen);

--Craft deny



craft_deny = ConvoScreen:new {

	id = "craft_deny",

	leftDialog = "",

	customDialogText = "Hmmmm a pity indeed, return when you find your courage!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_craft_conv:addScreen(craft_deny);

--craft1



craft1 = ConvoScreen:new {

	id = "craft1",

	leftDialog = "",

	customDialogText = "The Lightsaber is a very valuable weapon when you learn how to properly use it. Unfortunately in the galaxy we live in today, using a Lightsaber puts you at risk to being attacked.",

	stopConversation = "false",

	options = {

		{"So I must be cautious when I choose to show my true self?","craft2"}

	}

}

force_sensitive_dark_craft_conv:addScreen(craft1);

--craft2



craft2 = ConvoScreen:new {

	id = "craft2",

	leftDialog = "",

	customDialogText = "Correct, now you must also learn how a Lightsaber is constructed. It requires a tuned color crystal that you tune yourself, and as you improve your skills with a blade, you will also improve your ability to create better Sabers. Each new Saber you create will allow you to enhance its potential further by adding certain pearls and crystals found throughout the galaxy.",

	stopConversation = "false",

	options = {

		{"So I must obtain these items, and tune them for my Lightsaber only.","craft3"}

	}

}

force_sensitive_dark_craft_conv:addScreen(craft2);

--craft3



craft3 = ConvoScreen:new {

	id = "craft3",

	leftDialog = "",

	customDialogText = "You must also note that pulling them out of your saber will cause them to take damage, and they will not be as they once were. You see, the pearls and crystals tune themselves to the Lightsaber they are put in and when you remove them... they lose there lustier causing them to be less effective.",

	stopConversation = "false",

	options = {

		{"My blade is an extension of the power I wield, and its craftsmanship is a test of my ability to harness the force.","craftFinal"}

	}

}

force_sensitive_dark_craft_conv:addScreen(craft3);

--craftFinal



craftFinal = ConvoScreen:new {

	id = "craftFinal",

	leftDialog = "",

	customDialogText = "Lord Whikul was right, you are strong in the force. Remember all I have taught you, and you will become powerful indeed. Lord Shisil is waiting with your next lesson.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_craft_conv:addScreen(craftFinal);

--Complete



complete = ConvoScreen:new {

	id = "complete",

	leftDialog = "",

	customDialogText = "Tales of your enemies heads rolling at your feet have reached my ears! there is nothing more I can teach you.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_craft_conv:addScreen(complete);



addConversationTemplate("force_sensitive_dark_craft_conv", force_sensitive_dark_craft_conv);



--#######################################################				Shisil						################################



force_sensitive_dark_forced_conv = ConvoTemplate:new {

	initialScreen = "forced_first_screen",

	templateType = "Lua",

	luaClassHandler = "force_sensitive_dark_forced_conv_handler",

	screens = {}

}

--First



forced_first_screen = ConvoScreen:new {

	id = "forced_first_screen",

	leftDialog = "",

	customDialogText = "I am Lord Shisil, you must be the one Yanon speaks highly of. Lets not waste any time, being able to defend yourself from the Force is as valuable as wielding it. I am ready to teach you if you're ready to learn.",

	stopConversation = "false",

	options = {

		{"By your lead my lord.","forced_continue"},

		{"My connection to the force is not strong enough, I will return.","forced_deny"}	

	}

}

force_sensitive_dark_forced_conv:addScreen(forced_first_screen);

--Cont



forced_continue = ConvoScreen:new {

	id = "forced_continue",

	leftDialog = "",

	customDialogText = "Once a powerful Sith Lord told me... the force bends to our will. It is the Sith's ability to recognize the force in himself and to use it to his benefit ...Fear, hatered, and, anger fuel our connection to the force and a Sith must recognize these traits within and the universe outside themselves. Altering the force is the third and most difficult area to master, for it involves the wielder's ability to modify the force and redistribute its energies.",

	stopConversation = "",

	options = {

		{"The force is mine to command!","forced_test"},

	}

}

force_sensitive_dark_forced_conv:addScreen(forced_continue);

--Test



forced_test = ConvoScreen:new {

	id = "forced_test",

	leftDialog = "",

	customDialogText = "A Sith can train themselves to draw on the force to heal themselves and the others around them as well. Healing is one of the basic requirements to keep yourself alive and in the fight!",

	stopConversation = "false",

	options = {

		{"Healing other is weakness, if they are to weak to fight, let them die!","forced1"}	

	}

}

force_sensitive_dark_forced_conv:addScreen(forced_test);

--Deny



forced_deny = ConvoScreen:new {

	id = "forced_deny",

	leftDialog = "",

	customDialogText = "Perhaps Lord Yanon places to much stock in you. Return when you are ready.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_forced_conv:addScreen(forced_deny);

--forced1



forced1 = ConvoScreen:new {

	id = "forced1",

	leftDialog = "",

	customDialogText = "There are several ways to heal yourself, or others... But there are two main arts of healing, you have Internal healing and Supportive healing. Internal healing is used for yourself, and Supportive is for others. You will learn how to heal your mind, health, and action. You can focus the healing to heal the damage, or wounds. You could also unlock the secrets to being able to heal yourself over time for a duration.",

	stopConversation = "false",

	options = {

		{"I understand that healing is very important and I can use it for not only myself but for others that i choose worthy.","forced_complete"}

	}

}

force_sensitive_dark_forced_conv:addScreen(forced1);

--Comp



forced_complete = ConvoScreen:new {

	id = "forced_complete",

	leftDialog = "",

	customDialogText = "You will need to learn how to alter the Force first, then you will be able to manipulate the Force for yourself. Go and study with your mentor crystal, and when you're ready Lord Juwu is awaiting your arrival.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_forced_conv:addScreen(forced_complete);

--N Y



forced_not_yet = ConvoScreen:new {

	id = "forced_not_yet",

	leftDialog = "",

	customDialogText = "Leave me to my studies then, I've secrets to unlock.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_forced_conv:addScreen(forced_not_yet);

--T Y



forced_thank_you = ConvoScreen:new {

	id = "forced_thank_you",

	leftDialog = "",

	customDialogText = "Go seek out Lord Juwu if you haven't already.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_forced_conv:addScreen(forced_thank_you);



addConversationTemplate("force_sensitive_dark_forced_conv", force_sensitive_dark_forced_conv);



--####################                                  NOLDAN				########



force_sensitive_dark_force_conv = ConvoTemplate:new {

	initialScreen = "force_first_screen",

	templateType = "Lua",

	luaClassHandler = "force_sensitive_dark_force_conv_handler",

	screens = {}

	

}

--First



force_first_screen = ConvoScreen:new {

	id = "force_first_screen",

	leftDialog = "",

	customDialogText = "Ah well it appears you finally arrive, I am Lord Juwu. I am here to teach you about the Force. Lord Shisil should have told you the three basic practices of the Force, are you ready to learn more?",

	stopConversation = "false",

	options = {

		{"Yes my lord.","force_continue"},

		{"I must return to the records hall to study more.","force_deny"}	

	}

}

force_sensitive_dark_force_conv:addScreen(force_first_screen);

--Cont



force_continue = ConvoScreen:new {

	id = "force_continue",

	leftDialog = "",

	customDialogText = "Clear your mind, Absorb the force from around you. Feed off the fear and anger of others. Channel that energy into you, Focus.",

	stopConversation = "false",

	options = {

		{"Yes, I can feel it coursing through me!.","force1"},

	}

}

force_sensitive_dark_force_conv:addScreen(force_continue);

--force1



force1 = ConvoScreen:new {

	id = "force1",

	leftDialog = "",

	customDialogText = "Good, Good. Now once more, allow the force to flow through you. Once you master this training, You will have to learn to manage and control the force around you. How much Force you currently have control of is displayed on the Force Bar. Once you have exhausted this bar, You will no longer be able to use the Force until you have regenerated.",

	stopConversation = "false",

	options = {

		{"The stronger I become the more control I have over the force and the ability to manipulate it.","force2"}

	}

}

force_sensitive_dark_force_conv:addScreen(force1);

--Deny



force_deny = ConvoScreen:new {

	id = "force_deny",

	leftDialog = "",

	customDialogText = "A waste of my time you are.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_force_conv:addScreen(force_deny);

--Comp



force2 = ConvoScreen:new {

	id = "force2",

	leftDialog = "",

	customDialogText = "Your connection to the Force will progress with your growing knowledge of it. Take a moment and absorb the force from your mentor's crystal. Speak with me after.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_force_conv:addScreen(force2);



force_part2 = ConvoScreen:new {

	id = "force_part2",

	leftDialog = "",

	customDialogText = "Your connection to the Force grows. At this time however, you will exhaust yourself very quickly, but in time the Force will be your most powerful ally.",

	stopConversation = "false",

	options = {

		{"My control grows, I feel it power, I can attack with the Force.","force_part3"}

	}

}

force_sensitive_dark_force_conv:addScreen(force_part2);



force_part3 = ConvoScreen:new {

	id = "force_part3",

	leftDialog = "",

	customDialogText = "Very good, Manipulating the Force to put down your enemies can be as powerful as wielding a Lightsaber with enough practice.",

	stopConversation = "false",

	options = {

		{"A Lightsaber is my preferred method for dispatching my enemies","force_part4a"},

		{"A Lightsaber is no match for the raw power of the force","force_part4b"}

	}

}

force_sensitive_dark_force_conv:addScreen(force_part3);



force_part4a = ConvoScreen:new {

	id = "force_part4a",

	leftDialog = "",

	customDialogText = "That is understandable, After you have learned these basics you may choose to focus only on Lightsaber training but for now you must finish this task, Use your crystal and you can be on your way back to Lord Whikul.",

	stopConversation = "false",

	options = {

		{"I shall practice your teachings.","force_part5"}

	}

}

force_sensitive_dark_force_conv:addScreen(force_part4a);



force_part4b = ConvoScreen:new {

	id = "force_part4b",

	leftDialog = "",

	customDialogText = "You must still learn to defend yourself if the situation arises, Lord Whikul will help with that, Now use your crystal and you can be on your way.",

	stopConversation = "false",

	options = {

		{"Yes my Lord","force_part5"}

	}

}

force_sensitive_dark_force_conv:addScreen(force_part4b);



force_part5 = ConvoScreen:new {

	id = "force_part5",

	leftDialog = "",

	customDialogText = "The Sith are stronger with you in its ranks!",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_force_conv:addScreen(force_part5);

--N Y



force_not_yet = ConvoScreen:new {

	id = "force_not_yet",

	leftDialog = "",

	customDialogText = "You bore me, come back later.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_force_conv:addScreen(force_not_yet);

--T Y



force_thank_you = ConvoScreen:new {

	id = "force_thank_you",

	leftDialog = "",

	customDialogText = "Play with your little crystal, I will summon you when I'm ready.",

	stopConversation = "true",

	options = {}

}

force_sensitive_dark_force_conv:addScreen(force_thank_you);



addConversationTemplate("force_sensitive_dark_force_conv", force_sensitive_dark_force_conv);







--################################################################################################################################################################################################################################



--######################################################################### ROHAK #######################################################################################################################















force_sensitive_dark_rohak_conv = ConvoTemplate:new {



	initialScreen = "rohak_first_screen",



	templateType = "Lua",



	luaClassHandler = "force_sensitive_dark_rohak_conv_handler",



	screens = {}



}











--First



rohak_first_screen = ConvoScreen:new {



	id = "rohak_first_screen",



	leftDialog = "",



	customDialogText = "I am the village elder Rohak, I am glad to finally meet you. I am here to tell you what you will see once you leave our village. There are three seperate Jedi factions vying for power over the other. The Light Jedi Order, the Dark Jedi Order, and these Jedi that call themselves the Exiles. You will do well to educate yourself about them all.",



	stopConversation = "false",



	options = {



		{"I will see what I can do.","rohak_complete"}	



	}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_first_screen);











--cont



rohak_continue = ConvoScreen:new {



	id = "rohak_continue",



	leftDialog = "",



	customDialogText = "Later I will discuss this with you, but we need you to become more in tuned with your Force training, come back when you're a Jedi Initiate. There are plenty of Sith Shadows out there that need to be taken care of, strike true and May the Force be with you.",



	stopConversation = "false",



	options = {



		{"Yes Rohak, May the Force be with you.","rohak_complete"},



	}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_continue);











--Deny



rohak_deny = ConvoScreen:new {



	id = "rohak_deny",



	leftDialog = "",



	customDialogText = "",



	stopConversation = "true",



	options = {}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_deny);











--Comp



rohak_complete = ConvoScreen:new {



	id = "rohak_complete",



	leftDialog = "",



	customDialogText = "May the Force be with you.",



	stopConversation = "true",



	options = {}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_complete);











--N Y



rohak_not_yet = ConvoScreen:new {



	id = "rohak_not_yet",



	leftDialog = "",



	customDialogText = "Right now I'm busy, Noldan will let me know when you're ready to talk.",



	stopConversation = "true",



	options = {}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_not_yet);











--T Y



rohak_thank_you = ConvoScreen:new {



	id = "rohak_thank_you",



	leftDialog = "",



	customDialogText = "May the Force be with you.",



	stopConversation = "true",



	options = {}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_thank_you);











-- Holocron



rohak_holocron = ConvoScreen:new {



	id = "rohak_holocron",



	leftDialog = "",



	customDialogText = "Ah, im glad you have returned I need your help with, The Sith are renlentless and wont stop untill this place is in ruin",



	stopConversation = "false",



	options = {



		{"What can I do to help?","rohak_mell"},



		{"Im getting out of here this is to much!", "fail_rohak"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_holocron);







--Mellichae



fail_rohak = ConvoScreen:new {



	id = "fail_rohak",



	leftDialog = "",



	customDialogText = "And give up all that you have learned? You would be a prime target for the empire and its bounty hunters just for having visited this place, are you sure thats what you want to do?",



	stopConversation = "false",



	options = {



		{"...No..im sorry master, Im not sure what came over me.","rohak_mell"},



	}



}



force_sensitive_dark_rohak_conv:addScreen(fail_rohak);











--Mellichae



rohak_mell = ConvoScreen:new {



	id = "rohak_mell",



	leftDialog = "",



	customDialogText = "There has been sightings of Mellichae around the village, He has to be planning something to make a apperance outside of his lair.",



	stopConversation = "false",



	options = {



		{"Where do I come in?","take_holo"},--done



		{"Who is Mellichae?", "who_mell"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(rohak_mell);







who_mell = ConvoScreen:new {



	id = "who_mell",



	leftDialog = "",



	customDialogText = "Mellichae was once a a promissing memeber of our Order who fell to the dark side after encountering a unknown sith",



	stopConversation = "false",



	options = {



		{"unknown sith?","sith_who"}--done



	}



}



force_sensitive_dark_rohak_conv:addScreen(who_mell);







sith_who = ConvoScreen:new {



	id = "sith_who",



	leftDialog = "",



	customDialogText = "Yes, Mellichae was sent on a mission to Dantooine in search of ancient Holocrons from the ruined Jedi Temple. During his search he found fragments believed to be of Sith origin, when he was confronted by a masked Sith who incapacitated his companions and drug Mellichae away, When his companions awoke and begain a search, Mellichae confronted them with lightsaber drawn and demanded they leave immediately.",



	stopConversation = "false",



	options = {



		{"What do you think happend?","went_dark"}--done



	}



}



force_sensitive_dark_rohak_conv:addScreen(sith_who);







went_dark = ConvoScreen:new {



	id = "went_dark",



	leftDialog = "",



	customDialogText = "We do not know, but years later he was in command of the small army of Sith Shadows who begain attacking us recently. We can only assume the Sith turned Mellichae to the Dark Side, But now we must investigate his current plans for this place before something terrible happens",



	stopConversation = "false",



	options = {



		{"Im on it, where do I start?","take_holo"}--done



	}



}



force_sensitive_dark_rohak_conv:addScreen(went_dark);











--Take Holo



take_holo = ConvoScreen:new {



	id = "take_holo",



	leftDialog = "",



	customDialogText = "Our scouts have reported commanders placed around the village, its possible you could isolate one and attempt to gain some intell",



	stopConversation = "false",



	options = {



		{"im on it","capture"}--done



	}



}



force_sensitive_dark_rohak_conv:addScreen(take_holo);











--Capture



capture = ConvoScreen:new {



	id = "capture",



	leftDialog = "",



	customDialogText = "Tread lightly, with Mellichae close by we must take extra precautions during our investigation ",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_rohak_conv:addScreen(capture);











--Jedi



jedi = ConvoScreen:new {



	id = "jedi",



	leftDialog = "",



	customDialogText = "I see you have returned, have you brought back any information?",



	stopConversation = "false",



	options = {



		{"I ran into Mellichae, he and his guards attacked me.","jedi2"},



		{"",""},



		{"Greedo shot first!","stupid"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(jedi);







stupid = ConvoScreen:new {



	id = "stupid",



	leftDialog = "",



	customDialogText = "Sarcasum isn't getting us anywhere, care to try again?",



	stopConversation = "false",



	options = {



		{"Fine, as it turns out I ended up defeating Mellichae in a duel","jedi2"},



	}



}



force_sensitive_dark_rohak_conv:addScreen(stupid);







jedi2 = ConvoScreen:new {



	id = "jedi2",



	leftDialog = "",



	customDialogText = "You..You are serious arnt you? Its amazing you made it back alive, what happend?",



	stopConversation = "false",



	options = {



		{"I believe he was in a weakend state, as I aproached the commander who was standing guard I could feel a void in the force.. A sinking feeling the closer I got to the tent.","jedi1"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(jedi2);











--Jedi1



jedi1 = ConvoScreen:new {



	id = "jedi1",



	leftDialog = "",



	customDialogText = "A void in the force you say.. This is very troubling indeed, I will report your findings to our fellow masters but with Mellichae fleeing you have stabilized the area and surpassed our expectations. Its time you join the new Jedi Order to advance your training and protect our galaxie.",



	stopConversation = "false",



	options = {



		{"Me, a Jedi? What a honor. Thank you very much, Master.", "completelight"},



		{"Mellichae.. He told me to join him.","completedark"},



		{"After all this, im not sure I want anything to do with the Jedi.","completegrey"},



		{"I dont want to join the Jedi or the Sith, what are my options?", "quit"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(jedi1);







completedark = ConvoScreen:new {



	id = "completedark",



	leftDialog = "",



	customDialogText = "And you considered it? Do not allow the Dark side to tempt you, what ever he offered will only load to pain and suffering.",



	stopConversation = "false",



	options = {



		{"Parhaphs.", "completedark2"},



		{"You are right.. I need to find peace again, What should I do?","completesublight"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(completedark);







quit = ConvoScreen:new {



	id = "quit",



	leftDialog = "",



	customDialogText = "Your options our limited, the empire will hunt you just because you are force sensitive, I suggest you learn to control your senses so you can defend yourself if the situation arises.",



	stopConversation = "false",



	options = {



		{"Ok.", "leavejedi"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(quit);







leavejedi = ConvoScreen:new {



	id = "leavejedi",



	leftDialog = "",



	customDialogText = "Take this, use it. blahblah",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_rohak_conv:addScreen(leavejedi);







completesublight = ConvoScreen:new {



	id = "completesublight",



	leftDialog = "",



	customDialogText = "Your encounter with Mellichae must have been difficult, but you are stronger then him.",



	stopConversation = "false",



	options = {



		{"Parhaphs.", "completelight"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(completesublight);







completelight = ConvoScreen:new {



	id = "completelight",



	leftDialog = "",



	customDialogText = "somethingsomething rebuilding jedi order on something planet.",



	stopConversation = "false",



	options = {



		{"thanks and stuff ill go try and find it.", "completelight"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(completelight);







completedark2 = ConvoScreen:new {



	id = "completedark2",



	leftDialog = "",



	customDialogText = "Parhaphs?..oh my friend, I fear you have started down a Dark path, Do not allow this Darkness to overtake you.",



	stopConversation = "false",



	options = {



		{"The Darkness empowers me, The Light holds me back.. I am going to seek out Mellichae.", "completedark3"},



		{"There is no Darkness, there is no Light..there is only the Force","completegrey"}



	}



}



force_sensitive_dark_rohak_conv:addScreen(completedark2);







completedark3 = ConvoScreen:new {



	id = "completedark3",



	leftDialog = "",



	customDialogText = "Leave this place.",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_rohak_conv:addScreen(completedark3);











addConversationTemplate("force_sensitive_dark_rohak_conv", force_sensitive_dark_rohak_conv);











--[[







--



 = ConvoScreen:new {



	id = "",



	leftDialog = "",



	customDialogText = "",



	stopConversation = "false",



	options = {



		{"",""},



		{"",""}



	}



}



force_sensitive_dark_rohak_conv:addScreen();







]]







--###############################################					Mellichae				##############################################--







--[[







--



 = ConvoScreen:new {



	id = "",



	leftDialog = "",



	customDialogText = "",



	stopConversation = "false",



	options = {



		{"",""},



		{"",""}



	}



}



force_sensitive_dark_mel_conv:addScreen();







]]







force_sensitive_dark_mel_conv = ConvoTemplate:new {



	initialScreen = "mel_first_screen",



	templateType = "Lua",



	luaClassHandler = "force_sensitive_dark_mel_conv_handler",



	screens = {}



}







--Not yet



not_yet = ConvoScreen:new {



	id = "not_yet",



	leftDialog = "",



	customDialogText = "Step away from me or suffer my wraith.",



	stopConversation = "true",



	options = {







	}



}



force_sensitive_dark_mel_conv:addScreen(not_yet);











--First Screen



mel_first_screen = ConvoScreen:new {



	id = "mel_first_screen",



	leftDialog = "",



	customDialogText = "I have been watching you, you're the newest hot shot around the village. I am excited we finally meet, I am sure you know who I am.",



	stopConversation = "false",



	options = {



		{"I sure do, I am here to capture you. Come peacefully and no one will get hurt.","light"},--done



		{"I am here to show you what real power looks like!","dark"},--done



		{"You think you're so smart, I am going to rid you of this place... After I am done with you, you can become 'One with the Force'","exile"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(mel_first_screen);











--Light



light = ConvoScreen:new {



	id = "light",



	leftDialog = "",



	customDialogText = "HA! Capture me? You truely are a tool of the Jedi. I will not be captured by an untrained Force Sensitive! I hope you came ready for a fight youngling!",



	stopConversation = "false",



	options = {



		{"Wait wait, we can still talk this over can't we?","switch"},--done



		{"I will do what I must.","light1"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(light);











--Light1



light1 = ConvoScreen:new {



	id = "light1",



	leftDialog = "",



	customDialogText = "Then face your doom!",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_mel_conv:addScreen(light1);











--switch



switch = ConvoScreen:new {



	id = "switch",



	leftDialog = "",



	customDialogText = "You come here to try to capture me, and now you just want to talk? We can talk about you dying or serving me those are your only options.",



	stopConversation = "false",



	options = {



		{"I don't wish to die, I will join you.","switch_setup"},--done



		{"Actually... How about I just kill you instead.","exile"},--done



		{"You'll have to kill me then, I will never join you!","light1"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(switch);











--Switch Setup



switch_setup = ConvoScreen:new {



	id = "switch_setup",



	leftDialog = "",



	customDialogText = "I knew we could work something out, altho I won't be so trusting... You prove to me that you're loyal to me and only me I will train you and give you ultimate power.",



	stopConversation = "false",



	options = {



		{"What must I do to prove myself","alert"},--done



		{"What must I do to prove myself (lie)","alert"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(switch_setup);











--alert



alert = ConvoScreen:new {



	id = "alert",



	leftDialog = "",



	customDialogText = "Bring me Sarguillo and when the time comes strike him down, now go!",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_mel_conv:addScreen(alert);











--Dark



dark = ConvoScreen:new {



	id = "dark",



	leftDialog = "",



	customDialogText = "True power? You know nothing of true power!",



	stopConversation = "false",



	options = {



		{"You think because you control the Sith Shadows that you are made up of 'Power'? You are only going to fail, only I can bring the village to it's knees!","dark1"},--done



		{"I know enough power to take you down.","exile"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(dark);











--dark1



dark1 = ConvoScreen:new {



	id = "dark1",



	leftDialog = "",



	customDialogText = "Maybe we could work together, think of what we could do TOGETHER! Join me and we can conquer the galaxy, think about what we could accomplish. The Jedi council will fall to their knees before us.",



	stopConversation = "false",



	options = {



		{"I can see that we have the same goals, maybe we could work something out. What should we do first?","dark2"},--done



		{"I don't need you, you're weak!","exile"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(dark1);











--dark2



dark2 = ConvoScreen:new {



	id = "dark2",



	leftDialog = "",



	customDialogText = "Well I had just been thinking about weakening them with my Sith Shadows, then luring Sarguillo and Paemos outside... Strike them in the heart.",



	stopConversation = "false",



	options = {



		{"I will convince Paemos, and Sarguillo to come outside the village then. They know not of my plan, they are fools.","dark3"}--done



	}



}



force_sensitive_dark_mel_conv:addScreen(dark2);











--dark3



dark3 = ConvoScreen:new {



	id = "dark3",



	leftDialog = "",



	customDialogText = "Then go, I will put our scheme into motions.",



	stopConversation = "false",



	options = {



	}



}



force_sensitive_dark_mel_conv:addScreen(dark3);











--Exile



exile = ConvoScreen:new {



	id = "exile",



	leftDialog = "",



	customDialogText = "Those are some bold words coming from a youngling, very well then prepare yourself!",



	stopConversation = "true",



	options = {



	}



}



force_sensitive_dark_mel_conv:addScreen(exile);







addConversationTemplate("force_sensitive_dark_mel_conv", force_sensitive_dark_mel_conv);







--#############################################			lulzi				#################################







force_sensitive_dark_lulzi_conv = ConvoTemplate:new {



	initialScreen = "lulzi_first_screen",



	templateType = "Lua",



	luaClassHandler = "force_sensitive_dark_lulzi_conv_handler",



	screens = {}



}











--First



lulzi_first_screen = ConvoScreen:new {



	id = "lulzi_first_screen",



	leftDialog = "",



	customDialogText = "Im Lulzi, Are you the one the Lord Whikul sent?",



	stopConversation = "false",



	options = {



		{"I am, lets get on with it already.","lulzi2"}



	}



}







force_sensitive_dark_lulzi_conv:addScreen(lulzi_first_screen);







lulzi2 = ConvoScreen:new {



	id = "lulzi2",



	leftDialog = "",



	customDialogText = "I hope your fighting is as strong as your arrogance. I will be instructing you on basic engagement techniques with a target.",



	stopConversation = "false",



	options = {



		{"If you must.","lulzi3"},



		{"You're wasting my time, I already know this.","lulzi4"}	



	}



}







force_sensitive_dark_lulzi_conv:addScreen(lulzi2);







lulzi3 = ConvoScreen:new {



	id = "lulzi3",



	leftDialog = "",



	customDialogText = "First, use force focus, then i want you to stun your target as you approach with force throw. Once in range begin your melee assault with OneHandSaberHit1.",



	stopConversation = "false",



	options = {



		{"You could of atleast presented me with more of a challenge, but if I must.","lulzi4"}



	}



}







force_sensitive_dark_lulzi_conv:addScreen(lulzi3);







lulzi4 = ConvoScreen:new {



	id = "lulzi4",



	leftDialog = "",



	customDialogText = "Get on with it then!",



	stopConversation = "true",



	options = {



	}



}







force_sensitive_dark_lulzi_conv:addScreen(lulzi4);







lulzi_parttwo = ConvoScreen:new {



	id = "lulzi_parttwo",



	leftDialog = "",



	customDialogText = "Hmm, your beter than I expected, I will have to keep a closer eye on you. Use your mentors crystal then meet me outside the temple.",



	stopConversation = "true",



	options = {



	}



}







force_sensitive_dark_lulzi_conv:addScreen(lulzi_parttwo);







crystal_screen = ConvoScreen:new {



	id = "crystal_screen",



	leftDialog = "",



	customDialogText = "Use your Mentor Crystal before we begin this test.",



	stopConversation = "true",



	options = {



	}



}







force_sensitive_dark_lulzi_conv:addScreen(crystal_screen);







hello_screen = ConvoScreen:new {



	id = "hello_screen",



	leftDialog = "",



	customDialogText = "You, come here!.",



	stopConversation = "true",



	options = {



	}



}







force_sensitive_dark_lulzi_conv:addScreen(hello_screen);



















addConversationTemplate("force_sensitive_dark_lulzi_conv", force_sensitive_dark_lulzi_conv);







--#############################################			lulzi				#################################







force_sensitive_dark_lulzitwo_conv = ConvoTemplate:new {



	initialScreen = "lulzitwo_first_screen",



	templateType = "Lua",



	luaClassHandler = "force_sensitive_dark_lulzitwo_conv_handler",



	screens = {}



}











--First



lulzitwo_first_screen = ConvoScreen:new {



	id = "lulzitwo_first_screen",



	leftDialog = "",



	customDialogText = "What do you know about the Jedi Order?",



	stopConversation = "false",



	options = {



		{"Jedi, who are they?","fork1"},



		{"Our sworn enemy, the Jedi are!","fork2"},



	}



}







force_sensitive_dark_lulzitwo_conv:addScreen(lulzitwo_first_screen);







crystal_screen = ConvoScreen:new {



	id = "crystal_screen",



	leftDialog = "",



	customDialogText = "Use your Mentor Crystal before we begin this test.",



	stopConversation = "true",



	options = {



	}



}







force_sensitive_dark_lulzitwo_conv:addScreen(crystal_screen);







fork1 = ConvoScreen:new {

	id = "fork1",

	leftDialog = "",

	customDialogText = "They are our sworn enemies, vile scum, who corrupt potential force users with there docile approach to force use. When encountered they will show you no mercy and none shall be given to them.",

	stopConversation = "false",

	options = {

		{"These Jedi you speak of sound weak and unworthy of the force.","fork3a"},

		{"Jedi here, why are we taking? We should hunt them down!","fork3b"},

	}

}

force_sensitive_dark_lulzitwo_conv:addScreen(fork1);



fork2 = ConvoScreen:new {

	id = "fork2",

	leftDialog = "",

	customDialogText = "Indeed they are.",

	stopConversation = "false",

	options = {

		{"Interesting, what do they have to do with me?","fork3a"}



	}



}







force_sensitive_dark_lulzitwo_conv:addScreen(fork2);







fork3a = ConvoScreen:new {



	id = "fork3a",



	leftDialog = "",



	customDialogText = "Since the move here to Yavin4 large numbers of Jedi have moved against us. A group of their initiates has been spotted near by and this would be a perfect test of your new found abilities. Be mindful however, as strong as you have proven to be in the force, taking them on all at once would be a mistake.",



	stopConversation = "false",



	options = {



		{"These pathetic weaklings are no match for me.","fork4"}



	}



}







force_sensitive_dark_lulzitwo_conv:addScreen(fork3a);







fork3b = ConvoScreen:new {



	id = "fork3b",



	leftDialog = "",



	customDialogText = "Such agression, but caution is needed here. We do not yet know the full stregnth of the Jedi here on Yavin and revealing our full numbers to soon could be costly.",



	stopConversation = "false",



	options = {



		{"I see now.","fork3a"}



	}



}







force_sensitive_dark_lulzitwo_conv:addScreen(fork3b);

fork4 = ConvoScreen:new {
	id = "fork4",
	leftDialog = "",
	customDialogText = "We shall see, Here is the location. Return to me at once if you survive this test.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzitwo_conv:addScreen(fork4);

turnin = ConvoScreen:new {
	id = "turnin",
	leftDialog = "",
	customDialogText = "Impressive, You made quick work of them but another matter has arised, A Sith Adept captured a Jedi Initiate that was lurking around just North of here. I have been asked to investigate and would like you to assist me. Use your Mentors Crystal then meet me here.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzitwo_conv:addScreen(turnin);

addConversationTemplate("force_sensitive_dark_lulzitwo_conv", force_sensitive_dark_lulzitwo_conv);


--#############################################			lulzithree			#################################

force_sensitive_dark_lulzithree_conv = ConvoTemplate:new {
	initialScreen = "lulzi_capturedjedi",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_dark_lulzithree_conv_handler",
	screens = {}
}
--First
lulzi_capturedjedi = ConvoScreen:new {
	id = "lulzi_capturedjedi",
	leftDialog = "",
	customDialogText = "Ah you've arrived! Behold our Jedi prisoner!",
	stopConversation = "false",
	options = {
		{"What are we going to do with...... it?","lulzi_capturedjedi2"}
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_capturedjedi);

crystal_screen = ConvoScreen:new {
	id = "crystal_screen",
	leftDialog = "",
	customDialogText = "Use your Mentor Crystal before we begin this test.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzithree_conv:addScreen(crystal_screen);

lulzi_capturedjedi2 = ConvoScreen:new {
	id = "lulzi_capturedjedi2",
	leftDialog = "",
	customDialogText = "You will discover the truth behind this Jedi's actions, he was easily caught by Adept Trez, as if he wanted to be caught. I believe he intends to join the Sith. I want you to find out why.",
	stopConversation = "false",
	options = {
		{"What methods shall i employ in this task?","lulzi_capturedjedi3"}
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_capturedjedi2);

lulzi_capturedjedi3 = ConvoScreen:new {
	id = "lulzi_capturedjedi3",
	leftDialog = "",
	customDialogText = "Use the Force, keep your mind clear, follow your instincts.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_capturedjedi3);

lulzi_waiting = ConvoScreen:new {
	id = "lulzi_waiting",
	leftDialog = "",
	customDialogText = "Lets begin shall we.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_waiting);

lulzi_returned = ConvoScreen:new {
	id = "lulzi_returned",
	leftDialog = "",
	customDialogText = "That was interesting, What do you think?",
	stopConversation = "false",
	options = {
		{"I sense no deceit in his intentions of becoming one of us. He has potential.","lulzi_returned1a"},
		{"Kill him, he is not strong enough to be a Sith!.","lulzi_returned1b"},
		{"He is no use to us. Return him to his kind and let them deal with his treason.","lulzi_returned1c"}
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned);

lulzi_returned1a = ConvoScreen:new {
	id = "lulzi_returned1a",
	leftDialog = "",
	customDialogText = "Very good. I agree. If he becomes one of us we will be stronger for it.",
	stopConversation = "false",
	options = {
		{"What happens now?","lulzi_returned2a"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned1a);

lulzi_returned1b = ConvoScreen:new {
	id = "lulzi_returned1b",
	leftDialog = "",
	customDialogText = "Your senses are not as well tuned as I had hoped. I observed your interaction with the Jedi and feel he will become a great asset.",
	stopConversation = "false",
	options = {
		{"You had better be right about this.","lulzi_returned2b"}
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned1b);

lulzi_returned1c = ConvoScreen:new {
	id = "lulzi_returned1c",
	leftDialog = "",
	customDialogText = "This is disappointing news, Your senses are not as well tuned as I had hoped. I observed your interaction with the Jedi and feel he will become a great asset.",
	stopConversation = "false",
	options = {
		{"You had better be right about this.","lulzi_returned2c"}
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned1c);

lulzi_returned2a = ConvoScreen:new {
	id = "lulzi_returned2a",
	leftDialog = "",
	customDialogText = "He will be trained as you were, I will report to the Masters as to what has transpired here.",
	stopConversation = "false",
	options = {
		{"Shall I go in search of more who wish to join us?","lulzi_returned3a"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned2a);

lulzi_returned2b = ConvoScreen:new {
	id = "lulzi_returned2b",
	leftDialog = "",
	customDialogText = "You doubt me, I trust in the force. Remember the Sith code. Peace is a lie, there is only passion.",
	stopConversation = "false",
	options = {
		{"Through passion, I gain strength.","lulzi_returned2e"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned2b);

lulzi_returned2e = ConvoScreen:new {
	id = "lulzi_returned2e",
	leftDialog = "",
	customDialogText = "Good, Meditate on this and you will get back on track.",
	stopConversation = "false",
	options = {
		{"I will. What will happen to the Jedi now? ","lulzi_returned2a"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned2e);

lulzi_returned2c = ConvoScreen:new {
	id = "lulzi_returned2c",
	leftDialog = "",
	customDialogText = "In time you will learn, remember to be mindful of the Force. Peace is a lie, there is only passion.",
	stopConversation = "false",
	options = {
		{"Through passion, I gain strength. What happens now?","lulzi_returned2a"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned2c);

lulzi_returned3a = ConvoScreen:new {
	id = "lulzi_returned3a",
	leftDialog = "",
	customDialogText = "Not at this time, You have done very well but you have much to learn before we send you out in the galaxy. Our work here is complete. You should now use your Mentors Crystal then meet Lord Tikqot back inside the enclave who will help complete your training.",
	stopConversation = "false",
	options = {
		{"I leave you to your duties then.","lulzi_returned4a"},
	}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned3a);

lulzi_returned4a = ConvoScreen:new {
	id = "lulzi_returned4a",
	leftDialog = "",
	customDialogText = "Powerful you are in the dark side, A great Sith you will be. Until next we meet.",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_lulzithree_conv:addScreen(lulzi_returned4a);

addConversationTemplate("force_sensitive_dark_lulzithree_conv", force_sensitive_dark_lulzithree_conv);

--#############################################			capturedjedi			#################################

force_sensitive_dark_capturedjedi_conv = ConvoTemplate:new {
	initialScreen = "capturedjedi_one",
	templateType = "Lua",
	luaClassHandler = "force_sensitive_dark_capturedjedi_conv_handler",
	screens = {}
}
--First
capturedjedi_one = ConvoScreen:new {
	id = "capturedjedi_one",
	leftDialog = "",
	customDialogText = "<The Jedi stares at you>",
	stopConversation = "false",
	options = {
		{"Why have you come here Jedi?","capturedjedi_1a"},
		{"I should kill you where you stand, Jedi!","capturedjedi_1b"},
		--{"<Introduce yourself>","capturedjedi_1c"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_one);

capturedjedi_1a = ConvoScreen:new {
	id = "capturedjedi_1a",
	leftDialog = "",
	customDialogText = "I've come to seek a new path to the force. One that is not riddled with half truths.",
	stopConversation = "false",
	options = {
		{"And why should I trust you Jedi? You are afterall, my enemy.","capturedjedi_1b"},
		--{"<Introduce yourself>","capturedjedi_1c"}
		}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_1a);

capturedjedi_1b = ConvoScreen:new {
	id = "capturedjedi_1b",
	leftDialog = "",
	customDialogText = "Try and kill me, it won't be easy! <The Jedi readies for a fight>",
	stopConversation = "false",
	options = {
		{"I sense anger in you, not at all like a Jedi.","capturedjedi_2a"},
		{"<Use a force attack to bring him to his knees>","capturedjedi_1c"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_1b);

capturedjedi_1c = ConvoScreen:new {
	id = "capturedjedi_1c",
	leftDialog = "",
	customDialogText = "<The Jedi falls to the ground screaming in agony>",
	stopConversation = "false",
	options = {
		{"Hah, you think a Jedi is any match for me!","capturedjedi_2a"},
		--{"<He does understand>","capturedjedi_2b"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_1c);

capturedjedi_2a = ConvoScreen:new {
	id = "capturedjedi_2a",
	customDialogText = "Like I said, the Jedi do not understand the true potential of the Force, That is why I have risked everything to come here.",
	stopConversation = "false",
	options = {
		{"To become Sith, you must unlearn all the Jedi have taught you about the Force. To show mercy to your enemy is not tolerated.","capturedjedi_2b"},
		{"I have my doubts about your intentions Jedi.","capturedjedi_2b"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_2a);

capturedjedi_2b = ConvoScreen:new {
	id = "capturedjedi_2b",
	customDialogText = "Test me then, I will do what ever it takes to become a Sith. Even if it costs me my life!",
	stopConversation = "false",
	options = {
		{"We shall see if your worth is greater than your life to the Sith. Lets begin.","capturedjedi_3a"},
		{"Your fate is not for me to decide.","capturedjedi_3b"},
		--{"Why are you here?","capturedjedi_3c"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_2b);

capturedjedi_3a = ConvoScreen:new {
	id = "capturedjedi_3a",
	customDialogText = "I am ready.",
	stopConversation = "false",
	options = {
		{"The Jedi teach, There is no passion only serenity, do you believe this? ","capturedjedi_4a"},
		{"A true Sith will stop at nothing to complete his objective, even if it means sacrificing the ones we love. Can you say the same Jedi?","capturedjedi_4b"},
		--{"<Draw your weapon>","capturedjedi_4c"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_3a);

capturedjedi_3b = ConvoScreen:new {
	id = "capturedjedi_3b",
	customDialogText = "<The Jedi spits at your feet> You waste my time, find me someone with more authority then!",
	stopConversation = "false",
	options = {
		{"Such anger, perhaps i will test you.","capturedjedi_3a"},
		{"<Backhand the Jedi across the face> Your lack of respect should cost you your life Jedi! But I sense anger, passion, fear with in you, you them, lash out!","capturedjedi_3c"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_3b);

capturedjedi_3c = ConvoScreen:new {
	id = "capturedjedi_3c",
	customDialogText = "<The Jedi lunges at you, but is slammed to the ground with a Force shove. He then moves slowly to kneel before you.> ",
	stopConversation = "false",
	options = {
		{"I have misjudged you Jedi, I will test you.","capturedjedi_3a"},
		--{"<Channel the Lightside>","capturedjedi_3b"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_3c);

capturedjedi_4a = ConvoScreen:new {
	id = "capturedjedi_4a",
	customDialogText = "Passion is essential to life, without our passion why fight, why live.",
	stopConversation = "false",
	options = {
		{"A Sith without passion is no Sith at all.","capturedjedi_5a"},
		{"You are weak Jedi, but I sense the truth in your answer and your passion in what you say.","capturedjedi_5a"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_4a);

capturedjedi_4b = ConvoScreen:new {
	id = "capturedjedi_4b",
	customDialogText = "I, I don't know.",
	stopConversation = "false",
	options = {
		{"As I thought, you will not sacrafice the things you prize the most. You are not worthy of the Sith title!","capturedjedi_5b"},
		--{"I sense a force connection in him.","capturedjedi_5b"}
	}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_4b);

--capturedjedi_4c = ConvoScreen:new {
--	id = "capturedjedi_4c",
--	customDialogText = "<The Jedi shivers>",
--	stopConversation = "false",
--	options = {
--		{"He is Force Sensitive","capturedjedi_5a"},
--		{"This being is a product of the Dark side.","capturedjedi_5b"}
--	}
--}
--force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_4c);

capturedjedi_5a = ConvoScreen:new {
	id = "capturedjedi_5a",
	customDialogText = "<I will discuss his fate with Lulzi>",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_5a);

capturedjedi_5b = ConvoScreen:new {
	id = "capturedjedi_5b",
	customDialogText = "<What a waste, if only he saw his true potential. I will discuss his fate with Lulzi>",
	stopConversation = "true",
	options = {}
}
force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_5b);

--capturedjedi_5c = ConvoScreen:new {
--	id = "capturedjedi_5c",
--	customDialogText = "<I should return to Lulzi>",
--	stopConversation = "true",
--	options = {}
--}
--force_sensitive_dark_capturedjedi_conv:addScreen(capturedjedi_5c);

addConversationTemplate("force_sensitive_dark_capturedjedi_conv", force_sensitive_dark_capturedjedi_conv);
