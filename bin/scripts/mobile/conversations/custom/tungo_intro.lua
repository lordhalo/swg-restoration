tungoIntro = ConvoTemplate:new {
	initialScreen = "greet_friend",
	templateType = "Lua",
	luaClassHandler = "tungo_li_convo_handler",
	screens = {}
}
--Intro First
greet_friend = ConvoScreen:new {
	id = "greet_friend",
	leftDialog = "",
	customDialogText = "You the guy Jekk sent over?",
	stopConversation = "false",
	options = {
		{"Yes", "second_screen"},
	}
}
tungoIntro:addScreen(greet_friend);

return_friend = ConvoScreen:new {
	id = "return_friend",
	leftDialog = "",
	customDialogText = "Come talk with me later, ill have some work for you.",
	stopConversation = "true",
	options = {}
}
tungoIntro:addScreen(return_friend);

go_away = ConvoScreen:new {
	id = "go_away",
	leftDialog = "",
	customDialogText = "Get lost.",
	stopConversation = "true",
	options = {}
}
tungoIntro:addScreen(go_away);

second_screen = ConvoScreen:new {
	id = "second_screen",
	leftDialog = "",
	customDialogText = "He spoke highly of you, welcome to Death Watch.",
	stopConversation = "false",
	options = {
		{"Thank you, Sir.", "final_screen"},
	}
}
tungoIntro:addScreen(second_screen);

final_screen = ConvoScreen:new {
	id = "final_screen",
	leftDialog = "",
	customDialogText = "Come talk with me later, I will have some more work for you. See if anyone else is in need of your services.",
	stopConversation = "true",
	options = {}
}
tungoIntro:addScreen(final_screen);

addConversationTemplate("tungoIntro", tungoIntro);

