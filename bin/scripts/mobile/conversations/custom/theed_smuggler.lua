theed_smuggler_convo = ConvoTemplate:new {

	initialScreen = "first_screen",

	templateType = "Lua",

	luaClassHandler = "theed_smuggler_convo_handler",

	screens = {}

}





--Intro First

first_screen = ConvoScreen:new {

	id = "first_screen",

	leftDialog = "",

	customDialogText = "What do you want?!",

	stopConversation = "false",

	options = {

		{"Im searching for something.", "second_screen"},

		--{"Watch your tone.","deny"}

	

	}

}



theed_smuggler_convo:addScreen(first_screen);

second_screen = ConvoScreen:new {

	id = "second_screen",

	leftDialog = "",

	customDialogText = "I have a few things for sale, what are you looking for?",

	stopConversation = "false",

	options = {

		{"Shards, from a holocron", "third_screen"}

	}

}



theed_smuggler_convo:addScreen(second_screen);

third_screen = ConvoScreen:new {

	id = "third_screen",

	leftDialog = "",

	customDialogText = "Holocr...guhh, nope sorry don't know what you are looking for.",

	stopConversation = "false",

	options = {

		{"I can since your fear, did you expect someone else?", "forth_screena"},
		{"Do not tempt me, one more lie and it may be the last decision you ever make.", "forth_screenb"},
		{"I think you do know exactly what im looking for, fear not I will not harm you.", "forth_screenc"},

	}

}



theed_smuggler_convo:addScreen(third_screen);

forth_screena = ConvoScreen:new {

	id = "forth_screena",

	leftDialog = "",

	customDialogText = "Fear? Ha! I could blast you away with the blink of an eye.",

	stopConversation = "false",

	options = {

		{"I think you are here to meet a Fallen Jedi", "fifth_screen_a"},
		{"My Lightsaber will make quick work of you, should you try anything.", "forth_screenb"}

	}

}



theed_smuggler_convo:addScreen(forth_screena);

forth_screenb = ConvoScreen:new {

	id = "forth_screenb",

	leftDialog = "",

	customDialogText = "Uhh OK sorry my Lord, I didn't know you are the one sent to retrieve the item.",

	stopConversation = "false",

	options = {

		{"Hand over the shards and I will leave you with your life.", "fifth_screen_b"},
		{"My Lord? I am no Sith.", "fifth_screen_c"},

	}

}



theed_smuggler_convo:addScreen(forth_screenb);

forth_screenc = ConvoScreen:new {

	id = "forth_screenc",

	leftDialog = "",

	customDialogText = "Sorry, can't help you.",

	stopConversation = "false",

	options = {

		{"Do not tempt me, one more lie and it may be the last decision you ever make.", "forth_screenb"},
		{"I think you are here to meet a Fallen Jedi", "fifth_screen_a"}

	}

}



theed_smuggler_convo:addScreen(forth_screenc);

fifth_screen_a = ConvoScreen:new {

	id = "fifth_screen_a",

	leftDialog = "",

	customDialogText = "Lets say I was, I would dare not cross a Jedi. Perhaps I can interest you in some other exotic items?",

	stopConversation = "false",

	options = {

		{"I am only in search of shards from a Holocron.", "sixth_sceen_a"},
		{"I believe I encountered your Jedi friend, He did not fair well against me, How do you think I found you?", "sixth_sceen_b"}

	}

}



theed_smuggler_convo:addScreen(fifth_screen_a);

fifth_screen_b = ConvoScreen:new {

	id = "fifth_screen_b",

	leftDialog = "",

	customDialogText = "But what about the payment?.",

	stopConversation = "false",

	options = {

		{"This is your last warning", "sixth_sceen_c"},
		{"What was the arrangement?", "sixth_sceen_d"}

	}

}



theed_smuggler_convo:addScreen(fifth_screen_b);

fifth_screen_c = ConvoScreen:new {

	id = "fifth_screen_c",

	leftDialog = "",

	customDialogText = "Well you fooled me",

	stopConversation = "false",

	options = {

		{"I think you are here to meet a Fallen Jedi", "fifth_screen_a"}

	}

}



theed_smuggler_convo:addScreen(fifth_screen_c);

sixth_sceen_a = ConvoScreen:new {

	id = "sixth_sceen_a",

	leftDialog = "",

	customDialogText = "Go dig them up yourself, I got these from a drunk Imperial miner who told me they are excavating some Sith Temple in the Yavin system.",

	stopConversation = "false",

	options = {

		{"+Try Force persuade+ You will give me the shards ", "sixth_sceen_b2"}

	}

}



theed_smuggler_convo:addScreen(sixth_sceen_a);

sixth_sceen_b = ConvoScreen:new {

	id = "sixth_sceen_b",

	leftDialog = "",

	customDialogText = "You killed him? Prove it!",

	stopConversation = "false",

	options = {

		{"Here is the datapad I took from him, the coordinates here lead me to you. ", "sixth_sceen_b2"}

	}

}



theed_smuggler_convo:addScreen(sixth_sceen_b);

sixth_sceen_b2 = ConvoScreen:new {

	id = "sixth_sceen_b2",

	leftDialog = "",

	customDialogText = "Hrmmm.. I don't want any trouble, You want these shards? You can have them for the same price.",

	stopConversation = "false",

	options = {

		{"Whats the price?", "sixth_sceen_d"}

	}

}



theed_smuggler_convo:addScreen(sixth_sceen_b2);

sixth_sceen_c = ConvoScreen:new {

	id = "sixth_sceen_c",

	leftDialog = "",

	customDialogText = "Ok! Ok! Here! ",

	stopConversation = "false",

	options = {

		{"Good, Before I go, Tell me how you found these?", "seventh_screen_a"}

	}

}



theed_smuggler_convo:addScreen(sixth_sceen_c);


sixth_sceen_d = ConvoScreen:new {

	id = "sixth_sceen_d",

	leftDialog = "",

	customDialogText = "2 Million Credits.",

	stopConversation = "false",

	options = {

		{"Is your life worth these shards?", "seventh_screen_b"},
		{"I don't have that kind of money", "seventh_screen_c"}

	}

}



theed_smuggler_convo:addScreen(sixth_sceen_d);

seventh_screen_a = ConvoScreen:new {

	id = "seventh_screen_a",

	leftDialog = "",

	customDialogText = "A Imperial miner sold me them, he found them in a Temple on Yavin4 they had been excavating, Next to some crystals and a Hologram of an Old Republic ship.. Thats all I know, now Please let me go.",

	stopConversation = "true",

	options = {

	}

}



theed_smuggler_convo:addScreen(seventh_screen_a);

seventh_screen_b = ConvoScreen:new {

	id = "seventh_screen_b",

	leftDialog = "",

	customDialogText = "No! Sorry, Maybe you can help me with some thugs, Then ill call it even!",

	stopConversation = "false",

	options = {

		{"I will deal with these thugs, Where are they?", "eighth_screen"}

	}

}

theed_smuggler_convo:addScreen(seventh_screen_b);

seventh_screen_c = ConvoScreen:new {

	id = "seventh_screen_c",

	leftDialog = "",

	customDialogText = "There's some thugs who have been bothering me, If you help me with them ill consider it even trade.",

	stopConversation = "false",

	options = {

		{"I will deal with these thugs, Where are they?", "eighth_screen"}

	}

}

theed_smuggler_convo:addScreen(seventh_screen_c);

eighth_screen = ConvoScreen:new {

	id = "eighth_screen",

	leftDialog = "",

	customDialogText = "They hang out behind the Cantina, The leaders name is Qron.",

	stopConversation = "true",

	options = {

	}

}

theed_smuggler_convo:addScreen(eighth_screen);

last_screen = ConvoScreen:new {

	id = "last_screen",

	leftDialog = "",

	customDialogText = "Did you take care of Qron?",

	stopConversation = "false",

	options = {
		{"He wont be bothering you anymore.", "last_screen2"}
	}

}



theed_smuggler_convo:addScreen(last_screen);

last_screen2 = ConvoScreen:new {

	id = "last_screen2",

	leftDialog = "",

	customDialogText = "A deal is a deal, here are the shards.",

	stopConversation = "true",

	options = {
	}

}
theed_smuggler_convo:addScreen(last_screen2);



hello_screen = ConvoScreen:new {

	id = "hello_screen",

	leftDialog = "",

	customDialogText = "Im not selling Deathsticks, get lost!",

	stopConversation = "true",

	options = {

	

	}

}
theed_smuggler_convo:addScreen(hello_screen);

fear_screen = ConvoScreen:new {

	id = "fear_screen",

	leftDialog = "",

	customDialogText = "Please dont hurt me!",

	stopConversation = "true",

	options = {

	}

}

theed_smuggler_convo:addScreen(fear_screen);

complete_screen = ConvoScreen:new {

	id = "complete_screen",

	leftDialog = "",

	customDialogText = "Our deal is done.",

	stopConversation = "true",

	options = {

	}

}

theed_smuggler_convo:addScreen(complete_screen);

help_screen = ConvoScreen:new {

	id = "help_screen",

	leftDialog = "",

	customDialogText = "Are you going to help with those thugs or not?",

	stopConversation = "true",

	options = {

	}

}

theed_smuggler_convo:addScreen(help_screen);





addConversationTemplate("theed_smuggler_convo", theed_smuggler_convo);


