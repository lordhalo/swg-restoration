imo_vledmo = Creature:new {
	objectName = "",
	customName = "Imo Vledmo",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_prisoner_01.iff",
				 "object/mobile/dressed_dathomir_prisoner_02.iff",
				 "object/mobile/dressed_dathomir_prisoner_03.iff",
				 "object/mobile/dressed_dathomir_prisoner_04.iff",
				 "object/mobile/dressed_dathomir_prisoner_05.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteVledmoConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(imo_vledmo, "imo_vledmo")
