planet_record_keeper_dantooine = Creature:new {
	objectName = "",
	customName = "Lt. Nilsson (Planetary Record Keeper)",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {"object/mobile/dressed_stranded_imperial_soldier.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "planet_record_keeper_dantooine_convConversationTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(planet_record_keeper_dantooine, "planet_record_keeper_dantooine")
