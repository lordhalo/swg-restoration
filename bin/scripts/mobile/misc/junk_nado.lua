junk_nado = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	customName = "Nado",
	planetMapCategory = "junkshop",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/junk_nado.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerNadoConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_nado, "junk_nado")
