junk_quich = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	customName = "Quich Marae",
	planetMapCategory = "junkshop",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/junk_quich.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerQuichConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_quich, "junk_quich")
