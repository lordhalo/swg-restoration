junk_lila = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	customName = "Lila Rawlkiss",
	planetMapCategory = "junkshop",
	socialGroup = "borvo",
	faction = "borvo",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/junk_lila.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerLilaConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_lila, "junk_lila")
