gabriel_lan = Creature:new {
	objectName = "",
	customName = "Gabriel Ian",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/human_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteIanConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(gabriel_lan, "gabriel_lan")
