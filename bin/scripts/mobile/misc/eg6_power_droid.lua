eg6_power_droid = Creature:new {
	objectName = "@mob/creature_names:eg6_power_droid",
	randomNameType = NAME_EG6,
	socialGroup = "",
	faction = "",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/eg6_power_droid.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(eg6_power_droid, "eg6_power_droid")
