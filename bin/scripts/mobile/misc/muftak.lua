muftak = Creature:new {
	objectName = "@mob/creature_names:muftak",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	diet = HERBIVORE,

	templates = {"object/mobile/muftak.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(muftak, "muftak")
