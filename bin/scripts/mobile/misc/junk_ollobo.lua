junk_ollobo = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	customName = "Ollobo",
	planetMapCategory = "junkshop",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/junk_ollobo.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerOlloboConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_ollobo, "junk_ollobo")
