junk_dealer = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	planetMapCategory = "junkshop",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
			"object/mobile/junk_nathan.iff",
			"object/mobile/junk_lila.iff",
			"object/mobile/junk_malik.iff",
			"object/mobile/junk_nado.iff",
			"object/mobile/junk_reggi.iff",
			"object/mobile/junk_sheani.iff",
			"object/mobile/junk_sneg.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerGenericConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_dealer, "junk_dealer")
