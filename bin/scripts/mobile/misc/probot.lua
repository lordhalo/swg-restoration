probot = Creature:new {
	objectName = "@droid_name:imperial_probot_base",
	socialGroup = "",
	faction = "",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/probot.iff"},
	lootGroups = {},
	conversationTemplate = "",
	weapons = {"droid_probot_ranged"},
	defaultAttack = "attack"
}

CreatureTemplates:addCreatureTemplate(probot, "probot")
