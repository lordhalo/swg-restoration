junk_reggi = Creature:new {
	objectName = "@mob/creature_names:junk_dealer",
	customName = "Reggi Tirver",
	planetMapCategory = "junkshop",
	socialGroup = "nym",
	faction = "nym",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/junk_reggi.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "junkDealerReggiConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(junk_reggi, "junk_reggi")
