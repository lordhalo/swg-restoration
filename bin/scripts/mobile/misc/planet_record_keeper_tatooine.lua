planet_record_keeper_tatooine = Creature:new {
	objectName = "",
	customName = "Lt. Ogo (Planetary Record Keeper)",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {"object/mobile/dressed_eisley_officer_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "planet_record_keeper_tatooine_convConversationTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(planet_record_keeper_tatooine, "planet_record_keeper_tatooine")
