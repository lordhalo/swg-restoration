mosespa_race_coordinator = Creature:new {
	objectName = "@event_perk:name_race_droid",
	socialGroup = "",
	faction = "",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "mosespa_conversationtemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mosespa_race_coordinator, "mosespa_race_coordinator")
