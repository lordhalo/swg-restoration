naboo_kidnapped_noble_01 = Creature:new {
	objectName = "",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	customName = "a Kidnapped Noble",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_noble_naboo_twilek_female_01.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kidnappedNobleConvoTemplate",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(naboo_kidnapped_noble_01, "naboo_kidnapped_noble_01")
