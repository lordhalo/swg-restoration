piket = Creature:new {
	objectName = "@mob/creature_names:piket",
	socialGroup = "piket",
	faction = "",
	npcStats = {34, 86, 4070, 66, 298, 86, 0, 2403},
	meatType = "meat_herbivore",
	meatAmount = 450,
	hideType = "hide_scaley",
	hideAmount = 300,
	boneType = "bone_mammal",
	boneAmount = 210,
	milkType = "milk_wild",
	milk = 225,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/piket.iff"},
	controlDeviceTemplate = "object/intangible/pet/piket_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(piket, "piket")
