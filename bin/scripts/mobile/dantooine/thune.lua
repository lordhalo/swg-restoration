thune = Creature:new {
	objectName = "@mob/creature_names:thune",
	socialGroup = "thune",
	faction = "",
	npcStats = {41, 94, 4643, 73, 318, 94, 0, 2916},
	meatType = "meat_herbivore",
	meatAmount = 115,
	hideType = "hide_wooly",
	hideAmount = 125,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/thune.iff"},
	controlDeviceTemplate = "object/intangible/pet/thune_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(thune, "thune")
