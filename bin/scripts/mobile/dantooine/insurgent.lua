insurgent = Creature:new {
	objectName = "@npc_spawner_n:insurgent",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {33, 85, 3988, 65, 295, 85, 0, 2302},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/space_imperial_tier4_corellia_nin_gursawe.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_heavy"},
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(insurgent, "insurgent")
