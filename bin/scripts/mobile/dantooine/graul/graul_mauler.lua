graul_mauler = Creature:new {
	objectName = "@mob/creature_names:graul_mauler",
	socialGroup = "graul",
	faction = "",
	npcStats = {51, 112, 5462, 82, 345, 112, 0, 3546},
	meatType = "meat_carnivore",
	meatAmount = 950,
	hideType = "hide_leathery",
	hideAmount = 875,
	boneType = "bone_mammal",
	boneAmount = 775,
	milk = 0,
	tamingChance = 0.05,
	ferocity = 10,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/graul_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/graul_hue.iff",
	scale = 1.05,
	lootGroups = {
		{
			groups = {
				{group = "graul_common", chance = 10000000},
			},
			lootChance = 1640000
		}

	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(graul_mauler, "graul_mauler")
