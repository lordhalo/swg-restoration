graul = Creature:new {
	objectName = "@mob/creature_names:graul",
	socialGroup = "graul",
	faction = "",
	npcStats = {50, 107, 5380, 81, 342, 107, 0, 3496},
	meatType = "meat_carnivore",
	meatAmount = 950,
	hideType = "hide_leathery",
	hideAmount = 875,
	boneType = "bone_mammal",
	boneAmount = 775,
	milk = 0,
	tamingChance = 0.07,
	ferocity = 10,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/graul.iff"},
	controlDeviceTemplate = "object/intangible/pet/graul_hue.iff",
	lootGroups = {
		{
			groups = {
				{group = "graul_common", chance = 10000000},
			},
			lootChance = 1640000
		}

	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(graul, "graul")
