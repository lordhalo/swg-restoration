infant_graul = Creature:new {
	objectName = "@mob/creature_names:infant_graul",
	socialGroup = "graul",
	faction = "",
	npcStats = {49, 105, 5298, 80, 339, 105, 0, 3428},
	meatType = "meat_carnivore",
	meatAmount = 700,
	hideType = "hide_leathery",
	hideAmount = 750,
	boneType = "bone_mammal",
	boneAmount = 650,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/infant_graul.iff"},
	scale = 0.75,
	lootGroups = {
		{
			groups = {
				{group = "graul_common", chance = 5500000},
				{group = "armor_attachments", chance = 1500000},
				{group = "clothing_attachments", chance = 1500000},
				{group = "wearables_all", chance = 1500000},
			},
			lootChance = 3440000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(infant_graul, "infant_graul")
