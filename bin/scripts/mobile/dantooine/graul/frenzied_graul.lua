frenzied_graul = Creature:new {
	objectName = "@mob/creature_names:frenzied_graul",
	socialGroup = "graul",
	faction = "",
	npcStats = {51, 112, 5462, 82, 345, 112, 0, 3546},
	meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_leathery",
	hideAmount = 950,
	boneType = "bone_mammal",
	boneAmount = 855,
	milk = 0,
	tamingChance = 0.02,
	ferocity = 15,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/frenzied_graul.iff"},
	controlDeviceTemplate = "object/intangible/pet/graul_hue.iff",
	scale = 1.2,
	lootGroups = {
		{
			groups = {
				{group = "graul_common", chance = 10000000},
			},
			lootChance = 1640000
		}

	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"creatureareableeding",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(frenzied_graul, "frenzied_graul")
