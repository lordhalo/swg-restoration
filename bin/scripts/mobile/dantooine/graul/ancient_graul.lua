ancient_graul = Creature:new {
	objectName = "@mob/creature_names:ancient_graul",
	socialGroup = "graul",
	faction = "",
	npcStats = {52, 118, 5544, 83, 347, 118, 0, 3597},
	meatType = "meat_carnivore",
	meatAmount = 950,
	hideType = "hide_leathery",
	hideAmount = 875,
	boneType = "bone_mammal",
	boneAmount = 775,
	milk = 0,
	tamingChance = 0,
	ferocity = 10,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/graul_hue.iff"},
	scale = 1.15,
	lootGroups = {
	 {
	        groups = {
				{group = "graul_common", chance = 10000000}
			},
			lootChance = 10000000
		},

		{
	        groups = {
				{group = "graul_uncommon", chance = 2000000},
				{group = "armor_attachments", chance = 4000000},
				{group = "clothing_attachments", chance = 4000000}
			},
			lootChance = 4500000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(ancient_graul, "ancient_graul")
