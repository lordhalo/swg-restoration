droid_factory_battle_droid = Creature:new {
	objectName = "@mob/creature_names:mand_bunker_battle_droid",
	socialGroup = "",
	faction = "",
	level = 125,
	chanceHit = 20.0,
	damageMin = 515,
	damageMax = 625,
	baseXp = 12612,
	baseHAM = 36000,
	baseHAMmax = 38000,
	armor = 2,
	resists = {50,50,50,50,50,50,50,50,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {
		"object/mobile/death_watch_battle_droid.iff",
		"object/mobile/death_watch_battle_droid_02.iff",
		"object/mobile/death_watch_battle_droid_03.iff"},
	lootGroups = {},
	weapons = {"trade_federation_droid_weapons"},
	conversationTemplate = "",
	attacks = merge(carbineermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(droid_factory_battle_droid, "droid_factory_battle_droid")
