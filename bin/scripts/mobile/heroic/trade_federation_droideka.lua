trade_federation_droideka = Creature:new {
	objectName = "@mob/creature_names:droideka",
	socialGroup = "trade_federation",
	faction = "trade_federation",
	level = 165,
	chanceHit = 50.0,
	damageMin = 665,
	damageMax = 725,
	baseXp = 3824,
	baseHAM = 56000,
	baseHAMmax = 56000,
	armor = 0,
	resists = {70,70,70,25,70,25,70,25,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/droideka.iff"},
	lootGroups = {
		{
			groups = {
				{group = "fed_heavy_carbonite_schematic", chance = 2000000},
				{group = "s06_token", chance = 2000000},
				{group = "fed_carbine_e5_schematic", chance = 2000000},
				{group = "fed_ranged_weapon_enhancement", chance = 2000000},
				{group = "fed_melee_weapon_enhancement", chance = 2000000},
			},
			lootChance = 1600000
		},

		{
			groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000},
			},
			lootChance = 2600000
		},
	},
	conversationTemplate = "",
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff",
	defaultAttack = "creaturerangedattack",
}

CreatureTemplates:addCreatureTemplate(trade_federation_droideka, "trade_federation_droideka")
