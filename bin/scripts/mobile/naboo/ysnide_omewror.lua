ysnide_omewror = Creature:new {
	objectName = "@npc_name:twilek_base_female",
	customName = "Ysnide Omewror",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_fat_twilek_female_01.iff"},
	lootGroups = {},
	conversationTemplate = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(ysnide_omewror, "ysnide_omewror")
