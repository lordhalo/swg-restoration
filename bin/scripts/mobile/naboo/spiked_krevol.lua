spiked_krevol = Creature:new {
	objectName = "@mob/creature_names:spiked_krevol",
	socialGroup = "krevol",
	faction = "",
	npcStats = {3, 15, 288, 22, 153, 15, 0, 300},
	meatType = "meat_reptilian",
	meatAmount = 25,
	hideType = "hide_scaley",
	hideAmount = 15,
	boneType = "bone_mammal",
	boneAmount = 7,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/horned_krevol_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/horned_krevol_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(spiked_krevol, "spiked_krevol")
