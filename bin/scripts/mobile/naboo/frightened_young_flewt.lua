frightened_young_flewt = Creature:new {
	objectName = "@mob/creature_names:frightened_young_flewt",
	socialGroup = "flewt",
	faction = "",
	npcStats = {3, 15, 288, 22, 153, 15, 0, 300},
	meatType = "meat_avian",
	meatAmount = 1,
	hideType = "hide_leathery",
	hideAmount = 1,
	boneType = "bone_avian",
	boneAmount = 1,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/flewt_hue.iff"},
	scale = 0.85,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(frightened_young_flewt, "frightened_young_flewt")
