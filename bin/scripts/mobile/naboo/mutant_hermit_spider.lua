mutant_hermit_spider = Creature:new {
	objectName = "",
	customName = "a mutant hermit spider",
	socialGroup = "spider",
	faction = "",
	npcStats = {4, 18, 428, 25, 165, 18, 0, 375},
	meatType = "meat_insect",
	meatAmount = 70,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/hermit_spider_hue.iff"},
	scale = 1.5,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"mildpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(mutant_hermit_spider, "mutant_hermit_spider")
