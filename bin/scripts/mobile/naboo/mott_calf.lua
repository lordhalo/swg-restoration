mott_calf = Creature:new {
	objectName = "@mob/creature_names:mott_calf",
	socialGroup = "mott",
	faction = "",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "meat_herbivore",
	meatAmount = 45,
	hideType = "hide_leathery",
	hideAmount = 30,
	boneType = "bone_mammal",
	boneAmount = 15,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/mott_calf.iff"},
	scale = 0.75,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mott_calf, "mott_calf")
