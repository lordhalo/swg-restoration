rogue_falumpaset = Creature:new {
	objectName = "@mob/creature_names:rogue_falumpaset",
	socialGroup = "self",
	faction = "",
	npcStats = {14, 42, 2432, 47, 244, 42, 0, 972},
	meatType = "meat_herbivore",
	meatAmount = 190,
	hideType = "hide_leathery",
	hideAmount = 110,
	boneType = "bone_mammal",
	boneAmount = 80,
	milkType = "milk_wild",
	milk = 100,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/falumpaset_hue.iff"},
	scale = 1.1,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(rogue_falumpaset, "rogue_falumpaset")
