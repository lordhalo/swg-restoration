shaupaut = Creature:new {
	objectName = "@mob/creature_names:shaupaut",
	socialGroup = "shaupaut",
	faction = "",
	npcStats = {13, 41, 2350, 47, 243, 41, 0, 919},
	meatType = "meat_carnivore",
	meatAmount = 6,
	hideType = "hide_bristley",
	hideAmount = 4,
	boneType = "bone_mammal",
	boneAmount = 4,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 1,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/shaupaut.iff"},
	controlDeviceTemplate = "object/intangible/pet/shaupaut_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(shaupaut, "shaupaut")
