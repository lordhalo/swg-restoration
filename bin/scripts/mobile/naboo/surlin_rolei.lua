surlin_rolei = Creature:new {
	objectName = "",
	customName = "Surlin Rolei",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {16, 50, 2596, 48, 246, 50, 0, 1086},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_surlin_rolei.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(surlin_rolei, "surlin_rolei")
