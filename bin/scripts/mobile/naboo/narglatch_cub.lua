narglatch_cub = Creature:new {
	objectName = "@mob/creature_names:narglatch_cub",
	socialGroup = "narglatch",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "meat_carnivore",
	meatAmount = 30,
	hideType = "hide_bristley",
	hideAmount = 25,
	boneType = "bone_mammal",
	boneAmount = 20,
	milk = 0,
	tamingChance = 0,
	ferocity = 1,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/narglatch_cub.iff"},
	scale = 0.65,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(narglatch_cub, "narglatch_cub")
