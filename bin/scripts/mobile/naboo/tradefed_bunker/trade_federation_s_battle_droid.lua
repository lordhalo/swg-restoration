trade_federation_s_battle_droid = Creature:new {
	objectName = "@mob/creature_names:mand_bunker_super_battle_droid",
	socialGroup = "trade_federation",
	faction = "trade_federation",
	npcStats = {50, 107, 5380, 81, 342, 107, 0, 3496},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = NONE,
	scale = 1.3,

	templates = {
		"object/mobile/death_watch_s_battle_droid.iff",
		"object/mobile/death_watch_s_battle_droid_02.iff",
		"object/mobile/death_watch_s_battle_droid_03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "fed_heavy_carbonite_schematic", chance = 2000000},
				{group = "s06_token", chance = 2000000},
				{group = "fed_carbine_e5_schematic", chance = 2000000},
				{group = "fed_ranged_weapon_enhancement", chance = 2000000},
				{group = "fed_melee_weapon_enhancement", chance = 2000000},
			},
			lootChance = 1600000
		},

		{
			groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000},
			},
			lootChance = 2600000
		},
	},
	conversationTemplate = "",
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff",
	defaultAttack = "creaturerangedattack"
}

CreatureTemplates:addCreatureTemplate(trade_federation_s_battle_droid, "trade_federation_s_battle_droid")
