trade_federation_vr_bunker = Creature:new {
	objectName = "",
	customName = "Viceroy Culsus",
	socialGroup = "trade_federation",
	faction = "trade_federation",
	npcStats = {85, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/som/neimoidian.iff"},
	lootGroups = {
		{
			groups = {
				{group = "fed_heavy_carbonite_schematic", chance = 2000000},
				{group = "s06_token", chance = 2000000},
				{group = "fed_carbine_e5_schematic", chance = 2000000},
				{group = "fed_ranged_weapon_enhancement", chance = 2000000},
				{group = "fed_melee_weapon_enhancement", chance = 2000000},
			},
			lootChance = 5000000
		},

		{
			groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000},
			},
			lootChance = 10000000
		},

		{
			groups = {
				{group = "power_cube_enhancement", chance = 5000000},
				{group = "exotic_weapon", chance = 5000000},
			},
			lootChance = 5000000
		},

		{
			groups = {
				{group = "vr_decorative_loot", chance = 10000000},
			},
			lootChance = 3500000
		}
	},
	weapons = {},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(trade_federation_vr_bunker, "trade_federation_vr_bunker")
