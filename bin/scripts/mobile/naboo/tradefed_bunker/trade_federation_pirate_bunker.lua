trade_federation_pirate_bunker = Creature:new {
	objectName = "@mob/creature_names:trade_federation_pirate",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "trade_federation",
	faction = "trade_federation",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_trade_federation_zealot.iff"},
	lootGroups = {
		{
			groups = {
				{group = "s06_goggles", chance = 2000000},
				{group = "s06_token", chance = 2000000},
				{group = "fed_carbine_e5", chance = 2000000},
				{group = "fed_ranged_weapon_enhancement", chance = 2000000},
				{group = "fed_melee_weapon_enhancement", chance = 2000000},
			},
			lootChance = 1600000
		},

		{
			groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000},
			},
			lootChance = 2100000
		},
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(trade_federation_pirate_bunker, "trade_federation_pirate_bunker")
