trade_federation_droideka = Creature:new {
	objectName = "@mob/creature_names:droideka",
	socialGroup = "trade_federation",
	faction = "trade_federation",
	npcStats = {50, 107, 13245, 243, 684, 173, 2957, 3496},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/droideka.iff"},
	lootGroups = {
		{
			groups = {
				{group = "fed_heavy_carbonite_schematic", chance = 2000000},
				{group = "s06_token", chance = 2000000},
				{group = "fed_carbine_e5_schematic", chance = 2000000},
				{group = "fed_ranged_weapon_enhancement", chance = 2000000},
				{group = "fed_melee_weapon_enhancement", chance = 2000000},
			},
			lootChance = 1600000
		},

		{
			groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000},
			},
			lootChance = 2600000
		},
	},
	conversationTemplate = "",
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff",
	defaultAttack = "creaturerangedattack",
}

CreatureTemplates:addCreatureTemplate(trade_federation_droideka, "trade_federation_droideka")
