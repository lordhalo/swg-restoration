gerrio_coronis = Creature:new {
	objectName = "",
	customName = "Gerrio Coronis",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {45, 100, 7457, 96, 411, 163, 2522, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_gerrio_coronis.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(gerrio_coronis, "gerrio_coronis")
