graf_zapalo = Creature:new {
	objectName = "",
	customName = "Graf Zapalo",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {25, 76, 3333, 56, 268, 76, 0, 1602},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_graf_zapalo.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(graf_zapalo, "graf_zapalo")
