male_plains_tusk_cat = Creature:new {
	objectName = "@mob/creature_names:male_great_grass_plains_tusk_cat",
	socialGroup = "tusk_cat",
	faction = "",
	npcStats = {16, 50, 2596, 48, 246, 50, 0, 1086},
	meatType = "meat_carnivore",
	meatAmount = 75,
	hideType = "hide_bristley",
	hideAmount = 45,
	boneType = "bone_mammal",
	boneAmount = 40,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 9,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/tusk_cat_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/tusk_cat_hue.iff",
	scale = 1.05,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(male_plains_tusk_cat, "male_plains_tusk_cat")
