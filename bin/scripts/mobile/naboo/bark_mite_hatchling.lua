bark_mite_hatchling = Creature:new {
	objectName = "@mob/creature_names:bark_mite_hatchling",
	socialGroup = "mite",
	faction = "",
	npcStats = {7, 35, 990, 37, 218, 35, 0, 568},
	meatType = "meat_insect",
	meatAmount = 5,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/bark_mite_hatchling.iff"},
	scale = 0.85,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(bark_mite_hatchling, "bark_mite_hatchling")
