giant_horned_krevol = Creature:new {
	objectName = "@mob/creature_names:giant_horned_krevol",
	socialGroup = "krevol",
	faction = "",
	npcStats = {6, 25, 783, 32, 198, 25, 0, 508},
	meatType = "meat_insect",
	meatAmount = 10,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/giant_horned_krevol.iff"},
	controlDeviceTemplate = "object/intangible/pet/horned_krevol_hue.iff",
	scale = 1.25,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(giant_horned_krevol, "giant_horned_krevol")
