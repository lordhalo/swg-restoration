domestic_tusk_cat = Creature:new {
	objectName = "@mob/creature_names:domestic_tusk_cat",
	socialGroup = "naboo",
	faction = "",
	npcStats = {18, 63, 2759, 49, 248, 63, 0, 1202},
	meatType = "meat_carnivore",
	meatAmount = 75,
	hideType = "hide_bristley",
	hideAmount = 45,
	boneType = "bone_mammal",
	boneAmount = 40,
	milk = 0,
	tamingChance = 0,
	ferocity = 8,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/tusk_cat_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(domestic_tusk_cat, "domestic_tusk_cat")
