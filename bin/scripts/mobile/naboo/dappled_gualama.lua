dappled_gualama = Creature:new {
	objectName = "@mob/creature_names:dappled_gualama",
	socialGroup = "gualama",
	faction = "",
	npcStats = {12, 40, 2268, 46, 242, 40, 0, 868},
	meatType = "meat_herbivore",
	meatAmount = 200,
	hideType = "hide_wooly",
	hideAmount = 150,
	boneType = "bone_mammal",
	boneAmount = 90,
	milkType = "milk_wild",
	milk = 100,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dappled_gualama.iff"},
	controlDeviceTemplate = "object/intangible/pet/gualama_hue.iff",
	scale = 1.05,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(dappled_gualama, "dappled_gualama")
