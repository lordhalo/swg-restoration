capper_spineflap = Creature:new {
	objectName = "@mob/creature_names:capper_spineflap",
	socialGroup = "spineflap",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "meat_insect",
	meatAmount = 3,
	hideType = "hide_scaley",
	hideAmount = 5,
	boneType = "bone_avian",
	boneAmount = 3,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/capper_spineflap.iff"},
	controlDeviceTemplate = "object/intangible/pet/capper_spineflap_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(capper_spineflap, "capper_spineflap")
