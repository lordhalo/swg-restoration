j1_po = Creature:new {
	objectName = "@npc_spawner_n:j1_po",
	socialGroup = "",
	faction = "",
	npcStats = {25, 76, 3333, 56, 268, 76, 0, 1602},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid_silver.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "",
	optionsBitmask = AIENABLED
}

CreatureTemplates:addCreatureTemplate(j1_po, "j1_po")
