spineflap_queen = Creature:new {
	objectName = "@mob/creature_names:spineflap_queen",
	socialGroup = "spineflap",
	faction = "",
	npcStats = {12, 40, 2268, 46, 242, 40, 0, 868},
	meatType = "meat_insect",
	meatAmount = 20,
	hideType = "hide_scaley",
	hideAmount = 5,
	boneType = "bone_avian",
	boneAmount = 3,
	milk = 0,
	tamingChance = 0,
	ferocity = 7,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/spineflap_queen.iff"},
	scale = 1.25,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"knockdownattack",""},
		{"mediumpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(spineflap_queen, "spineflap_queen")
