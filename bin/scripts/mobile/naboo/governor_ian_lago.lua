governor_ian_lago = Creature:new {
	objectName = "",
	customName = "Governor Ian Lago",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {45, 100, 7457, 96, 411, 163, 2522, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_govenor_ian_lago.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(governor_ian_lago, "governor_ian_lago")
