etheli_drenel = Creature:new {
	objectName = "@npc_name:bothan_base_male",
	customName = "Etheli Dre'nel",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_bothan_male_01.iff"},
	lootGroups = { },
	conversationTemplate = "",
	attacks = {	}
}

CreatureTemplates:addCreatureTemplate(etheli_drenel, "etheli_drenel")
