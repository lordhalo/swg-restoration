gungan_kaadu = Creature:new {
	objectName = "@mob/creature_names:gungan_kaadu",
	socialGroup = "gungan",
	faction = "gungan",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "meat_domesticated",
	meatAmount = 120,
	hideType = "hide_leathery",
	hideAmount = 85,
	boneType = "bone_avian",
	boneAmount = 70,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/kaadu_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(gungan_kaadu, "gungan_kaadu")
