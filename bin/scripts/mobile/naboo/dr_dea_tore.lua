dr_dea_tore = Creature:new {
	objectName = "@mob/creature_names:dr_dea_tore",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 6842, 90, 394, 153, 2087, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dr_dea_tore.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(dr_dea_tore, "dr_dea_tore")
