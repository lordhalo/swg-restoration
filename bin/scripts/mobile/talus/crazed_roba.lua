crazed_roba = Creature:new {
	objectName = "@mob/creature_names:crazed_roba",
	socialGroup = "self",
	faction = "",
	npcStats = {11, 39, 1985, 46, 241, 39, 0, 801},
	meatType = "meat_carnivore",
	meatAmount = 12,
	hideType = "hide_leathery",
	hideAmount = 18,
	boneType = "bone_mammal",
	boneAmount = 12,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 2,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/roba_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/roba_hue.iff",
	scale = 0.85,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(crazed_roba, "crazed_roba")
