glutted_fynock_queen = Creature:new {
	objectName = "@mob/creature_names:glutted_fynock_queen",
	socialGroup = "fynock",
	faction = "",
	npcStats = {14, 42, 2432, 47, 244, 42, 0, 972},
	meatType = "meat_avian",
	meatAmount = 30,
	hideType = "hide_leathery",
	hideAmount = 25,
	boneType = "bone_avian",
	boneAmount = 20,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/glutted_fynock_queen.iff"},
	scale = 1.25,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(glutted_fynock_queen, "glutted_fynock_queen")
