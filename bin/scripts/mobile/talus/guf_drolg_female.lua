guf_drolg_female = Creature:new {
	objectName = "@mob/creature_names:guf_drolg_female",
	socialGroup = "guf_drolg",
	faction = "",
	npcStats = {13, 41, 2350, 47, 243, 41, 0, 919},
	meatType = "meat_reptilian",
	meatAmount = 325,
	hideType = "hide_leathery",
	hideAmount = 265,
	boneType = "bone_mammal",
	boneAmount = 190,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/guf_drolg_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/guf_drolg_hue.iff",
	scale = 1.05,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(guf_drolg_female, "guf_drolg_female")
