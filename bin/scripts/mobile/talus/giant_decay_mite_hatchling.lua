giant_decay_mite_hatchling = Creature:new {
        objectName = "@mob/creature_names:giant_decay_mite_hatchling",
        socialGroup = "mite",
        faction = "",
        npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
        meatType = "meat_insect",
        meatAmount = 5,
        hideType = "hide_scaley",
        hideAmount = 4,
        boneType = "",
        boneAmount = 0,
        milk = 0,
        tamingChance = 0,
        ferocity = 0,
        pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
        creatureBitmask = PACK,
        optionsBitmask = AIENABLED,
        diet = CARNIVORE,

        templates = {"object/mobile/decay_mite.iff"},
        lootGroups = {},
        weapons = {},
        conversationTemplate = "",
        attacks = {
		{"",""},
                {"milddisease",""}
        }
}

CreatureTemplates:addCreatureTemplate(giant_decay_mite_hatchling, "giant_decay_mite_hatchling")
