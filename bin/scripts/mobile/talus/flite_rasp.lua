flite_rasp = Creature:new {
	objectName = "@mob/creature_names:flite_rasp",
	socialGroup = "rasp",
	faction = "",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "meat_avian",
	meatAmount = 19,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 9,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/flite_rasp.iff"},
	scale = 0.9,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(flite_rasp, "flite_rasp")
