feared_fynock_youth = Creature:new {
	objectName = "@mob/creature_names:feared_fynock_youth",
	socialGroup = "fynock",
	faction = "",
	npcStats = {3, 15, 288, 22, 153, 15, 0, 300},
	meatType = "meat_avian",
	meatAmount = 3,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 3,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/fearful_fynock_youth.iff"},
	scale = 0.8,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(feared_fynock_youth, "feared_fynock_youth")
