fynock = Creature:new {
	objectName = "@mob/creature_names:fynock",
	socialGroup = "fynock",
	faction = "",
	npcStats = {11, 39, 1985, 46, 241, 39, 0, 801},
	meatType = "meat_avian",
	meatAmount = 22,
	hideType = "hide_leathery",
	hideAmount = 33,
	boneType = "bone_avian",
	boneAmount = 10,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/fynock.iff"},
	controlDeviceTemplate = "object/intangible/pet/fynock_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(fynock, "fynock")
