genetically_enchanced_kahmurra = Creature:new {
	objectName = "@mob/creature_names:genetically_enhanced_kahmurra",
	socialGroup = "kahmurra",
	faction = "",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "meat_herbivore",
	meatAmount = 50,
	hideType = "hide_bristley",
	hideAmount = 30,
	boneType = "bone_mammal",
	boneAmount = 35,
	milkType = "milk_wild",
	milk = 25,
	tamingChance = 0.25,
	ferocity = 6,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/kahmurra.iff"},
	controlDeviceTemplate = "object/intangible/pet/murra_hue.iff",
	scale = 1.4,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(genetically_enchanced_kahmurra, "genetically_enchanced_kahmurra")
