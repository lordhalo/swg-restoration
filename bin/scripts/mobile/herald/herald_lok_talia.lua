herald_lok_talia = Creature:new {
	objectName = "@npc_name:lok_herald_01",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_lok_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldLok1ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_lok_talia, "herald_lok_talia")
