herald_naboo_vaikanna = Creature:new {
	objectName = "@npc_name:naboo_herald_01",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_naboo_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldNaboo1ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_naboo_vaikanna, "herald_naboo_vaikanna")
