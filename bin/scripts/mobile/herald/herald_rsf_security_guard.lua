herald_rsf_security_guard = Creature:new {
	objectName = "@mob/creature_names:rsf_security_guard",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {19, 65, 2841, 50, 249, 65, 0, 1254},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rsf_security_guard.iff"},
	lootGroups = {},
	weapons = {"rebel_weapons_light"},
	conversationTemplate = "heraldConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_rsf_security_guard, "herald_rsf_security_guard")
