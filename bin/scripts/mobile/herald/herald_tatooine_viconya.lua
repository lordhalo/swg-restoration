herald_tatooine_viconya = Creature:new {
	objectName = "@npc_name:tatooine_herald_02",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {19, 65, 2841, 50, 249, 65, 0, 1254},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_tatooine_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldTatooine1ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_tatooine_viconya, "herald_tatooine_viconya")
