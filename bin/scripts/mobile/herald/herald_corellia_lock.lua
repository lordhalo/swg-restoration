herald_corellia_lock = Creature:new {
	objectName = "@npc_name:corellia_herald_01",
	customName = "Lock Vlash",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK + HEALER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_corellia_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldCorellia1ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_corellia_lock, "herald_corellia_lock")
