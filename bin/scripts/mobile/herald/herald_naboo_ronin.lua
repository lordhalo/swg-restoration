herald_naboo_ronin = Creature:new {
	objectName = "@npc_name:naboo_herald_02",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_naboo_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldNaboo2ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_naboo_ronin, "herald_naboo_ronin")
