herald_corellia_karin = Creature:new {
	objectName = "@npc_name:corellia_herald_02",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_corellia_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heraldCorellia2ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_corellia_karin, "herald_corellia_karin")
