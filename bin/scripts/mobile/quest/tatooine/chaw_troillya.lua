chaw_troillya = Creature:new {
	objectName = "@mob/creature_names:bothan_information_broker",
	socialGroup = "",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_thug_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "stella_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(chaw_troillya, "chaw_troillya")
