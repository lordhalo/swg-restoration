huff_darklighter = Creature:new {
	objectName = "",
	customName = "Huff Darklighter",
	socialGroup = "darklighter",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/huff_darklighter.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "krayt_dragon_skull_mission_giver_convotemplate",
	outfit = "huff_darklighter_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(huff_darklighter, "huff_darklighter")
