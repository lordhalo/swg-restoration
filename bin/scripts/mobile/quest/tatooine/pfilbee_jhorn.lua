pfilbee_jhorn = Creature:new {
	objectName = "",
	customName = "Pfilbee Jhorn",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_pfilbee_jhorn.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "pfilbee_jhorn_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(pfilbee_jhorn, "pfilbee_jhorn")
