om_aynat = Creature:new {
	objectName = "",
	customName = "Om Aynat",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_om_aynat.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "om_aynat_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(om_aynat, "om_aynat")
