vardias_tyne = Creature:new {
	objectName = "",
	customName = "Vardias Tyne",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_vardias_tyne.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "vardias_tyne_mission_giver_convotemplate",
	outfit = "vardias_tyne_outfit",
	attacks = {
	}

}

CreatureTemplates:addCreatureTemplate(vardias_tyne, "vardias_tyne")
