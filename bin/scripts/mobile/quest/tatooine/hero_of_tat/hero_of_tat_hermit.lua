hero_of_tat_hermit = Creature:new {
	objectName = "@mob/creature_names:quest_hero_of_tatooine_hermit",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_hermit_of_tatooine.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heroOfTatHermitConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(hero_of_tat_hermit, "hero_of_tat_hermit")
