wild_bladeback_boar = Creature:new {
	objectName = "@mob/creature_names:quest_hero_of_tatooine_ferocious_beast",
	socialGroup = "boar",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "meat_herbivore",
	meatAmount = 120,
	hideType = "hide_leathery",
	hideAmount = 80,
	boneType = "bone_mammal",
	boneAmount = 50,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/zucca_boar_hue.iff"},
	scale = 1.5,
	lootGroups = {
		{
			groups = {
				{group = "hero_of_tat_mark_of_courage", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"knockdownattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(wild_bladeback_boar, "wild_bladeback_boar")
