hero_of_tat_smuggler = Creature:new {
	objectName = "@mob/creature_names:smuggler",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_criminal_slicer_human_male_01.iff",
		"object/mobile/dressed_criminal_slicer_human_female_01.iff",
		"object/mobile/dressed_smuggler_trainer_01.iff",
		"object/mobile/dressed_smuggler_trainer_02.iff",
		"object/mobile/dressed_smuggler_trainer_03.iff",
	},
	lootGroups = {},
	weapons = {"pirate_weapons_medium"},
	conversationTemplate = "heroOfTatIntellectLiarConvoTemplate",
	attacks = merge(brawlermid,marksmanmid)
}

CreatureTemplates:addCreatureTemplate(hero_of_tat_smuggler, "hero_of_tat_smuggler")
