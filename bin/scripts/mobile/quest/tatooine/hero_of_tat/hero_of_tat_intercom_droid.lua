hero_of_tat_intercom_droid = Creature:new {
	objectName = "",
	customName = "Intercom Droid",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heroOfTatIntercomConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(hero_of_tat_intercom_droid, "hero_of_tat_intercom_droid")
