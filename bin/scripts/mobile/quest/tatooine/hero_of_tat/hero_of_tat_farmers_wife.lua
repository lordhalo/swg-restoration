hero_of_tat_farmers_wife = Creature:new {
	objectName = "@mob/creature_names:quest_hero_of_tatooine_farmer_wife",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_quest_farmer_wife.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "heroOfTatMotherConvoTemplate",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(hero_of_tat_farmers_wife, "hero_of_tat_farmers_wife")
