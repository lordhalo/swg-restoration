lt_harburik = Creature:new {
	objectName = "@npc_name:it_harburik",
	customName = "Lt. Harburik",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_lt_harburik.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lt_harburik_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(lt_harburik, "lt_harburik")
