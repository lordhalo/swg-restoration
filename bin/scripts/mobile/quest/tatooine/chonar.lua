chonar = Creature:new {
	objectName = "",
	customName = "Brother Chonar",
	socialGroup = "dim_u",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_dim_u_monk.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "vardias_tyne_mission_target_convotemplate",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(chonar, "chonar")
