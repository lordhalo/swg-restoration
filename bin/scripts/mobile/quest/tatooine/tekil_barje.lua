tekil_barje = Creature:new {
	objectName = "",
	customName = "Tekil Barje",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_tekil_barje.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "tekil_barje_mission_giver_convotemplate",
	outfit = "tekil_barje_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(tekil_barje, "tekil_barje")
