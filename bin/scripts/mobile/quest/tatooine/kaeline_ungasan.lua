kaeline_ungasan = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Kaeline Ungasan",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_kaeline_ungasan.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kaeline_ungasan_mission_giver_convotemplate",
	outfit = "kaeline_ungasan_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kaeline_ungasan, "kaeline_ungasan")
