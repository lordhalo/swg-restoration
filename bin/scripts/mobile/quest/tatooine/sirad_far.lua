sirad_far = Creature:new {
	objectName = "@mob/creature_names:sirad_far",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_jabba_thug.iff"},
	lootGroups = {},
	conversationTemplate = "theme_park_jabba_mission_target_convotemplate",
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(sirad_far, "sirad_far")
