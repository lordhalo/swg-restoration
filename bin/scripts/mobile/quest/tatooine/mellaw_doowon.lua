mellaw_doowon = Creature:new {
	objectName = "",
	customName = "Mellaw Doowon",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/ithorian_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "blerx_tango_mission_target_convotemplate",
	attacks = brawlernovice
}
CreatureTemplates:addCreatureTemplate(mellaw_doowon, "mellaw_doowon")
