prefect_talmont = Creature:new {
	objectName = "@mob/creature_names:prefect_talmont",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/prefect_talmont.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "prefect_talmont_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(prefect_talmont, "prefect_talmont")
