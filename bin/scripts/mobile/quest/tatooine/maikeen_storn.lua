maikeen_storn = Creature:new {
	objectName = "@mob/creature_names:farmer",
	customName = "Maikeen Storn",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_naboo_zabrak_female_02.iff",
	},

	lootGroups = {},
	weapons = {},
	conversationTemplate = "melios_purl_mission_target_convotemplate",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(maikeen_storn, "maikeen_storn")
