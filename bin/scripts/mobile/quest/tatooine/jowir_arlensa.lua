jowir_arlensa = Creature:new {
	objectName = "",
	customName = "Jowir Ar'Lensa",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_jowir_arlensa.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "zicx_bug_bomb_mission_giver_convotemplate",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(jowir_arlensa, "jowir_arlensa")
