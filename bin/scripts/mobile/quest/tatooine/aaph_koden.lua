aaph_koden = Creature:new {
	objectName = "",
	customName = "Aaph Koden",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_aaph_koden.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "aaph_koden_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(aaph_koden, "aaph_koden")
