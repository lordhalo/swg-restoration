wilhalm_skrim = Creature:new {
	objectName = "@theme_park_name:willhalm_skrim",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_wilhalm_skrim.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "prefect_talmont_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(wilhalm_skrim, "wilhalm_skrim")
