seans_historian = Creature:new {
	objectName = "",
	randomNameType = NAME_GENERIC,
	randomNameTag = false,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_seans_historian.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "seansHistorianConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(seans_historian, "seans_historian")
