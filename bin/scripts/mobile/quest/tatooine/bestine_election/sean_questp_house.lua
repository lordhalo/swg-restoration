sean_questp_house = Creature:new {
	objectName = "",
	customName = "Edaekomeu Ossilei",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_sean_questp_house.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "seanQuestpHouseConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(sean_questp_house, "sean_questp_house")
