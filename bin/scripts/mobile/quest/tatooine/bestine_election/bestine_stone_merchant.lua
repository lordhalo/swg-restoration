bestine_stone_merchant = Creature:new {
	objectName = "@bestine:merchant_title",
	customName = "Stone Merchant",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_artisan_trainer_03.iff",
		"object/mobile/dressed_artisan_trainer_02.iff",
		"object/mobile/dressed_artisan_trainer_01.iff",
		"object/mobile/dressed_commoner_artisan_trandoshan_male_01.iff",
		"object/mobile/dressed_commoner_artisan_sullustan_male_01.iff",
		"object/mobile/dressed_commoner_artisan_bith_male_01.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "StoneMerchantConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(bestine_stone_merchant, "bestine_stone_merchant")
