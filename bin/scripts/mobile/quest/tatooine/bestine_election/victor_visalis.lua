victor_visalis = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Victor Visalis",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_victor_visalis.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "victorVisalisConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(victor_visalis, "victor_visalis")
