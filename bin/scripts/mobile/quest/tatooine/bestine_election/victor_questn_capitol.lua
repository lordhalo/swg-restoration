victor_questn_capitol = Creature:new {
	objectName = "",
	customName = "TL-017",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_victor_questn_capitol.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "victorQuestnCapitolConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(victor_questn_capitol, "victor_questn_capitol")
