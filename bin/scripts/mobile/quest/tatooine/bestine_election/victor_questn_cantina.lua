victor_questn_cantina = Creature:new {
	objectName = "",
	randomNameType = NAME_GENERIC,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 3,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_victor_questn_cantina.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "victorQuestnCantinaConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(victor_questn_cantina, "victor_questn_cantina")
