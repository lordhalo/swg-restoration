bestine_rumor03 = Creature:new {
	objectName = "@mob/creature_names:imperial_private",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {"object/mobile/dressed_bestine_rumor03.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bestineRumor03ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(bestine_rumor03, "bestine_rumor03")
