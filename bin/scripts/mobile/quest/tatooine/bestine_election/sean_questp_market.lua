sean_questp_market = Creature:new {
	objectName = "",
	customName = "Orqiti Aneofo",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_sean_questp_market.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "seanQuestpMarketConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(sean_questp_market, "sean_questp_market")
