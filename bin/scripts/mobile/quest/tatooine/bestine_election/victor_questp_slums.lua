victor_questp_slums = Creature:new {
	objectName = "",
	customName = "Foofice Lightingrunner",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_victor_questp_slums.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "victorQuestpSlumsConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(victor_questp_slums, "victor_questp_slums")
