sean_questn_university = Creature:new {
	objectName = "",
	customName = "Ocket Abaot",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_sean_questn_university.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "seanQuestnUniversityConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(sean_questn_university, "sean_questn_university")
