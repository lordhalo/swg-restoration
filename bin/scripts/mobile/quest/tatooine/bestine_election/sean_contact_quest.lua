sean_contact_quest = Creature:new {
	objectName = "",
	randomNameType = NAME_GENERIC,
	randomNameTag = false,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_sean_contact_quest.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "seanContactConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(sean_contact_quest, "sean_contact_quest")
