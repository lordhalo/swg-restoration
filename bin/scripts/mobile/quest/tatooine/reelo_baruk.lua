reelo_baruk = Creature:new {
	objectName = "@theme_park_name:reelo_baruk",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/reelo_baruk.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_jabba_mission_giver_convotemplate",
	outfit = "reelo_baruk_outfit",
	attacks = {},
	optionsBitmask = INVULNERABLE + CONVERSABLE
}

CreatureTemplates:addCreatureTemplate(reelo_baruk, "reelo_baruk")
