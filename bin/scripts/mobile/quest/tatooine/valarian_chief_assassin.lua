valarian_chief_assassin = Creature:new {
	objectName = "",
	socialGroup = "valarian",
	faction = "valarian",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_valarian_assassin.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_medium"},
	conversationTemplate = "theme_park_valarian_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(valarian_chief_assassin, "valarian_chief_assassin")
