stella = Creature:new {
	objectName = "@mob/creature_names:mos_taike_cantina_owner",
	customName = "Stella",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_cantina_owner.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "stella_mission_giver_convotemplate",
	outfit = "stella_outfit",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(stella, "stella")
