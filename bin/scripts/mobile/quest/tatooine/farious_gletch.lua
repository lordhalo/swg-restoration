farious_gletch = Creature:new {
	objectName = "",
	customName = "Farious Gletch",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_farious_gletch.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "farious_gletch_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(farious_gletch, "farious_gletch")
