jilljoo_jab = Creature:new {
	objectName = "@npc_name:twilek_base_female",
	customName = "Jilljoo Jab",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_jilljoo_jab.iff"},
	lootGroups = {},
	weapons = {},
	outfit = "jilljoo_jab_outfit",
	conversationTemplate = "jilljoo_jab_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(jilljoo_jab, "jilljoo_jab")
