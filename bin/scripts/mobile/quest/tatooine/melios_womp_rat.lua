melios_womp_rat = Creature:new {
	objectName = "@mob/creature_names:mutant_womprat",
	socialGroup = "rat",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "meat_wild",
	meatAmount = 6,
	hideType = "hide_leathery",
	hideAmount = 5,
	boneType = "bone_mammal",
	boneAmount = 4,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 3,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/womp_rat.iff"},
	controlDeviceTemplate = "object/intangible/pet/womp_rat_hue.iff",
	scale = 2.15,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	lootGroups = {
		{
			groups = {
				{ group = "task_loot_melios_purl_q2", chance = 10000000},			},
 			lootChance = 10000000
		},
	},
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(melios_womp_rat, "melios_womp_rat")
