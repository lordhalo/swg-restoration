bren_kingal = Creature:new {
	objectName = "@mob/creature_names:mos_taike_guard_young",
	customName = "Bren Kingal",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bren_kingal_mission_giver_convotemplate",
	outfit = "mos_taike_guard_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(bren_kingal, "bren_kingal")
