blerx_tango = Creature:new {
	objectName = "@npc_name:bith_base_male",
	customName = "Blerx Tango",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_blerx_tango.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "blerx_tango_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(blerx_tango, "blerx_tango")
