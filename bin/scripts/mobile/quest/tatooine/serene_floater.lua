serene_floater = Creature:new {
	objectName = "",
	customName = "Serene Floater",
	socialGroup = "bomarr",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/bomarr_spider_monk_droid.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "hedon_istee_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(serene_floater, "serene_floater")
