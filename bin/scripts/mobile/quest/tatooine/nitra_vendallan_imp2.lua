nitra_vendallan_imp2 = Creature:new {
	objectName = "",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_weaponsmith_trainer_01.iff",
		"object/mobile/dressed_weaponsmith_trainer_02.iff",
		"object/mobile/dressed_weaponsmith_trainer_03.iff"},
	lootGroups = {},
	weapons = {},
	attacks = brawlermaster,
	conversationTemplate = "nitra_vendallan_mission_target_convotemplate",
}

CreatureTemplates:addCreatureTemplate(nitra_vendallan_imp2, "nitra_vendallan_imp2")
