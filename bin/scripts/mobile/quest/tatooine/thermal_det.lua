thermal_det = Creature:new {
	objectName = "",
	customName = "Thermal Det",
	socialGroup = "",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/kitonak_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kitster_banai_mission_target_convotemplate",
	attacks = brawlermid
}

CreatureTemplates:addCreatureTemplate(thermal_det, "thermal_det")
