lorne_prestar = Creature:new {
	objectName = "@mob/creature_names:mos_taike_guard_old",
	customName = "Lorne Prestar",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_old.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lorne_prestar_mission_giver_convotemplate",
	outfit = "mos_taike_guard_old_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(lorne_prestar, "lorne_prestar")
