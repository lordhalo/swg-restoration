lilas_dinhint = Creature:new {
	objectName = "",
	customName = "Lilas Dinhint (museum curator)",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_lilas_dinhint.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "museumCuratorConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(lilas_dinhint, "lilas_dinhint")
