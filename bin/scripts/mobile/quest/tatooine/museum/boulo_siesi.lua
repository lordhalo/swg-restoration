boulo_siesi = Creature:new {
	objectName = "@mob/creature_names:bestine_artist06",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_bestine_artist06.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bestineArtist06ConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(boulo_siesi, "boulo_siesi")
