kolka_zteht = Creature:new {
	objectName = "@mob/creature_names:bestine_artist02",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_bestine_artist02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bestineArtist02ConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kolka_zteht, "kolka_zteht")
