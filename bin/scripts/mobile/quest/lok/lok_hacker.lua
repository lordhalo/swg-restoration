lok_hacker = Creature:new {
       objectName = "@npc_name:human_base_female",
       customName = "Hacker",
       socialGroup = "nym",
       faction = "nym",
       npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
       meatType = "",
       meatAmount = 0,
       hideType = "",
       hideAmount = 0,
       boneType = "",
       boneAmount = 0,
       milk = 0,
       tamingChance = 0.000000,
       ferocity = 0,
       pvpBitmask = NONE,
       creatureBitmask = NONE,
       optionsBitmask = AIENABLED + CONVERSABLE,
       diet = HERBIVORE,

       templates = { "object/mobile/dressed_criminal_assassin_human_female_01.iff" },
       conversationTemplate = "lokHackerConvoTemplate",
       lootGroups = {},
       weapons = {},
       attacks = {}
}

CreatureTemplates:addCreatureTemplate(lok_hacker, "lok_hacker")
