nym = Creature:new {
	objectName = "@mob/creature_names:nym",
	socialGroup = "nym",
	faction = "nym",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/nym.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "nymConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(nym, "nym")
