choster = Creature:new {
	objectName = "@mob/creature_names:hermit",
	customName = "Choster (Retired Engineer)",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_commoner_naboo_twilek_male_02.iff" },
	lootGroups = {},
	weapons = {},
	conversationTemplate = "chosterConvoTemplate",
	attacks = { }

}

CreatureTemplates:addCreatureTemplate(choster, "choster")
