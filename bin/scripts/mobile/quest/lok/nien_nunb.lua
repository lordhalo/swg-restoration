nien_nunb = Creature:new {
	objectName = "@mob/creature_names:nien_nunb",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_patron_sullustan_01.iff"},
	lootGroups = {},
	conversationTemplate = "theme_park_rebel_mission_giver_convotemplate",
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(nien_nunb, "nien_nunb")
