sergeant_moore = Creature:new {
	objectName = "@mob/creature_names:commoner",
	customName = "Sergeant Moore (Retired)",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_commoner_old_human_male_02.iff" },

	lootGroups = {},
	weapons = {},
	conversationTemplate = "sergeantMooreConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(sergeant_moore, "sergeant_moore")
