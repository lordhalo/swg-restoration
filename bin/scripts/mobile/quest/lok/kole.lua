kole = Creature:new {
	objectName = "@mob/creature_names:kole",
	socialGroup = "nym",
	faction = "nym",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/kole.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "koleConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kole, "kole")
