jinkins = Creature:new {
	objectName = "@mob/creature_names:jinkins",
	socialGroup = "nym",
	faction = "nym",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/jinkins.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jinkinsConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(jinkins, "jinkins")
