coa2_imperial_commander = Creature:new {
	objectName = "@mob/creature_names:coa2_imperial_commander",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_commander_m.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "imperialCommanderConvoTemplate",
	attacks = merge(riflemanmaster,carbineermaster,brawlermaster)

}

CreatureTemplates:addCreatureTemplate(coa2_imperial_commander, "coa2_imperial_commander")
