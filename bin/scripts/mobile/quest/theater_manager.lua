theater_manager = Creature:new {
	objectName = "@mob/creature_names:quest_crowd_pleaser_theater_manager",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_entertainer_trainer_twk_female_01.iff",
		"object/mobile/dressed_entertainer_trainer_twk_male_01.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theaterManagerConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(theater_manager, "theater_manager")
