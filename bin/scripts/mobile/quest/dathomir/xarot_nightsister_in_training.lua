xarot_nightsister_in_training = Creature:new {
	objectName = "",
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {75, 171, 6998, 104, 410, 171, 0, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_initiate.iff"},
	lootGroups = {},
	weapons = {"mixed_force_weapons"},
	conversationTemplate = "xarot_korlin_mission_target_convotemplate",
	attacks = merge(brawlermaster,pikemanmaster)
}

CreatureTemplates:addCreatureTemplate(xarot_nightsister_in_training, "xarot_nightsister_in_training")
