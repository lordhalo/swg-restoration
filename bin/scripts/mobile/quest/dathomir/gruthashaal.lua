gruthashaal = Creature:new {
	objectName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_commoner_naboo_human_female_08.iff"
		},

	lootGroups = {
		{
			groups = {
				{group = "task_loot_ancient_lightsaber", chance = 10000000},

			},
			lootChance = 10000000
		},
	},
	weapons = {},
	conversationTemplate = "",
	attacks = merge(tkamaster, brawlermaster)
}

CreatureTemplates:addCreatureTemplate(gruthashaal, "gruthashaal")
