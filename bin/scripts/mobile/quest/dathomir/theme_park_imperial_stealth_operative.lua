theme_park_imperial_stealth_operative = Creature:new {
  objectName = "@mob/creature_names:assassin_mission_recruiter_imperial",
  randomNameType = NAME_GENERIC,
  randomNameTag = true,
  socialGroup = "imperial",
  faction = "imperial",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_imperial_soldier_m.iff"},
  lootGroups = {
    {}
  },
  weapons = {},
  conversationTemplate = "theme_park_nightsister_mission_target_convotemplate",
  attacks = {
  }
}

CreatureTemplates:addCreatureTemplate(theme_park_imperial_stealth_operative, "theme_park_imperial_stealth_operative")
