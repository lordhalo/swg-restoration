old_man = Creature:new {
	objectName = "@mob/creature_names:fs_intro_oldman",
	socialGroup = "fs_villager",
	faction = "fs_villager",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fs_village_oldman.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "oldManIntroConvoTemplate",
	attacks = brawlermid
}

CreatureTemplates:addCreatureTemplate(old_man, "old_man")
