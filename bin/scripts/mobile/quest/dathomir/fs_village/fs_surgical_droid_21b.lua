fs_surgical_droid_21b = Creature:new {
	objectName = "@mob/creature_names:fs_surgical_droid_21b",
	socialGroup = "fs_villager",
	faction = "fs_villager",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/21b_surgical_droid.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(fs_surgical_droid_21b, "fs_surgical_droid_21b")
