fs_cll8_binary_load_lifter = Creature:new {
	objectName = "",
	socialGroup = "townsperson",
	faction = "townsperson",
	customName = "QT-QC",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/cll8_binary_load_lifter.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(fs_cll8_binary_load_lifter, "fs_cll8_binary_load_lifter")
