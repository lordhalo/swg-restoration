fs_mouse_droid = Creature:new {
	objectName = "@mob/creature_names:mouse_droid",
	socialGroup = "sith_shadow",
	faction = "",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/mouse_droid.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(fs_mouse_droid, "fs_mouse_droid")
