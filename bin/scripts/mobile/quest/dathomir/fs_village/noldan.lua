noldan = Creature:new {
	objectName = "@mob/creature_names:trainer_fs",
	socialGroup = "fs_villager",
	faction = "fs_villager",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fs_trainer.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "fsTrainerConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(noldan, "noldan")
