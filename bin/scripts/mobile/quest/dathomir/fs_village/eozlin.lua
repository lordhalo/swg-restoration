eozlin = Creature:new {
	objectName = "@mob/creature_names:fs_villager_surveyor",
	socialGroup = "fs_villager",
	faction = "fs_villager",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fs_village_surveyor.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "villageSurveyorConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(eozlin, "eozlin")
