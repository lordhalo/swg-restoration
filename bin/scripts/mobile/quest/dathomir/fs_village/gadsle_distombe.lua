gadsle_distombe = Creature:new {
	objectName = "@mob/creature_names:gadsle_distombe",
	socialGroup = "sith_shadow",
	faction = "sith_shadow",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fs_village_enemy_gadsle.iff"},
	lootGroups = {},
	weapons = {"mixed_force_weapons"},
	conversationTemplate = "",
	attacks = merge(brawlermaster,pikemanmaster)
}

CreatureTemplates:addCreatureTemplate(gadsle_distombe, "gadsle_distombe")
