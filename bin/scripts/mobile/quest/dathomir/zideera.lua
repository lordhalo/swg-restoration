zideera = Creature:new {
	objectName = "",
	customName = "Zideera",
	socialGroup = "mtn_clan",
	faction = "mtn_clan",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_zideera.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_smc_zideera_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(zideera, "zideera")
