malkloc_quest = Creature:new {
	objectName = "@mob/creature_names:malkloc",
	socialGroup = "malkloc",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "meat_herbivore",
	meatAmount = 1000,
	hideType = "hide_leathery",
	hideAmount = 1000,
	boneType = "bone_mammal",
	boneAmount = 1000,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/malkloc.iff"},
	controlDeviceTemplate = "object/intangible/pet/malkloc_hue.iff",
	lootGroups = {
        		{
			groups = {
				{group = "theme_park_loot_malkloc_heart_ns_kais", chance = 10000000}
			},
		lootChance = 10000000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(malkloc_quest, "malkloc_quest")
