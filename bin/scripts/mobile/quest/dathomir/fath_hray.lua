fath_hray = Creature:new {
	objectName = "@mob/creature_names:fath_hray",
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fath_hray.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_nightsister_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(fath_hray, "fath_hray")
