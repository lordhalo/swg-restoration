trujhazii = Creature:new {
	objectName = "",
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_stalker.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "singular_nak_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(trujhazii, "trujhazii")
