wallaw_loowobbli = Creature:new {
	objectName = "",
	customName = "Wallaw Loowobbli",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_wallaw_loowobbli.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "wallaw_loowobbli_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(wallaw_loowobbli, "wallaw_loowobbli")
