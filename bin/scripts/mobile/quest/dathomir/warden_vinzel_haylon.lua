warden_vinzel_haylon = Creature:new {
	objectName = "",
	customName = "Warden Vinzel Haylon",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_vinzel_haylon.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "warden_vinzel_haylon_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(warden_vinzel_haylon, "warden_vinzel_haylon")
