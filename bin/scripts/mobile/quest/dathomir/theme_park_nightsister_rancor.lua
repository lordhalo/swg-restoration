theme_park_nightsister_rancor = Creature:new {
  objectName = "@mob/creature_names:nightsister_rancor",
  socialGroup = "nightsister",
  faction = "nightsister",
  npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
  meatType = "meat_carnivore",
  meatAmount = 950,
  hideType = "hide_leathery",
  hideAmount = 876,
  boneType = "bone_mammal",
  boneAmount = 776,
  milk = 0,
  tamingChance = 0,
  ferocity = 10,
  pvpBitmask = ATTACKABLE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = CARNIVORE,

  templates = {"object/mobile/rancor.iff"},
  scale = 1.05,
  lootGroups = {},
  weapons = {},
  conversationTemplate = "theme_park_nightsister_mission_target_convotemplate",
  attacks = {}
}

CreatureTemplates:addCreatureTemplate(theme_park_nightsister_rancor, "theme_park_nightsister_rancor")
