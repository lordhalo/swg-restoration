wallaw_bull_rancor = Creature:new {
  objectName = "@mob/creature_names:bull_rancor",
  socialGroup = "rancor",
  faction = "",
  npcStats = {79, 177, 18035, 321, 840, 283, 5478, 5063},
  meatType = "meat_carnivore",
  meatAmount = 1000,
  hideType = "hide_leathery",
  hideAmount = 900,
  boneType = "bone_mammal",
  boneAmount = 850,
  milk = 0,
  tamingChance = 0.01,
  ferocity = 11,
  pvpBitmask = ATTACKABLE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = CARNIVORE,

  templates = {"object/mobile/bull_rancor.iff"},
  controlDeviceTemplate = "object/intangible/pet/rancor_hue.iff",
  lootGroups = {},
  weapons = {},
  conversationTemplate = "wallaw_loowobbli_mission_target_convotemplate",
  attacks = {
    {"creatureareadisease",""},
    {"dizzyattack",""}
  }
}

CreatureTemplates:addCreatureTemplate(wallaw_bull_rancor, "wallaw_bull_rancor")
