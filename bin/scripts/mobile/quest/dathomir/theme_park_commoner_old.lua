theme_park_commoner_old = Creature:new {
  objectName = "@mob/creature_names:commoner",
  randomNameType = NAME_GENERIC,
  randomNameTag = true,
  socialGroup = "townsperson",
  faction = "townsperson",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = ATTACKABLE,
  creatureBitmask = HERD,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = { "object/mobile/dressed_criminal_pirate_human_male_01.iff",
      "object/mobile/dressed_criminal_thug_human_male_01.iff",
      "object/mobile/dressed_criminal_thug_zabrak_male_01.iff"
      },

  lootGroups = {},
  weapons = {"pirate_weapons_light"},
  conversationTemplate = "theme_park_smc_zideera_mission_target_convotemplate",
  attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(theme_park_commoner_old, "theme_park_commoner_old")
