shaki_hamachil = Creature:new {
	objectName = "",
	customName = "Shaki Hamachil",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_shaki_hamachil.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "shaki_hamachil_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(shaki_hamachil, "shaki_hamachil")
