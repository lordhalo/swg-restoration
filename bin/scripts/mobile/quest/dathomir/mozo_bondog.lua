mozo_bondog = Creature:new {
	objectName = "",
	customName = "Mozo Bondog",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mozo_bondog.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "dolac_legasi_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mozo_bondog, "mozo_bondog")
