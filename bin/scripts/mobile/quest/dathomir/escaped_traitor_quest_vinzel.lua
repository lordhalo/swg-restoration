escaped_traitor_quest_vinzel = Creature:new {
	objectName = "@mob/creature_names:abandoned_rebel_private",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rebel_trooper_bith_m_01.iff",
		"object/mobile/dressed_rebel_trooper_twk_male_01.iff",
		"object/mobile/dressed_rebel_trooper_twk_female_01.iff",
		"object/mobile/dressed_rebel_trooper_human_female_01.iff",
		"object/mobile/dressed_rebel_trooper_human_male_01.iff",
		"object/mobile/dressed_rebel_trooper_sullustan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "warden_vinzel_haylon_mission_target_convotemplate",
	reactionStf = "@npc_reaction/military",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(escaped_traitor_quest_vinzel, "escaped_traitor_quest_vinzel")
