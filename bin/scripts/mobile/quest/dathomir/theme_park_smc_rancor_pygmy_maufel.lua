theme_park_smc_rancor_pygmy_maufel = Creature:new {
  objectName = "",
  customName = "Mau'fel",
  socialGroup = "rancor",
  faction = "mtn_clan",
  npcStats = {75, 171, 6998, 104, 410, 171, 0, 4819},
  meatType = "meat_carnivore",
  meatAmount = 500,
  hideType = "hide_leathery",
  hideAmount = 553,
  boneType = "bone_mammal",
  boneAmount = 453,
  milk = 0,
  tamingChance = 0,
  ferocity = 10,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED,
  diet = CARNIVORE,

  templates = {"object/mobile/rancor_hue.iff"},
  scale = 0.2,
  lootGroups = {},
  weapons = {},
  conversationTemplate = "",
  attacks = {
    {"stunattack",""},
    {"intimidationattack",""}
  }
}

CreatureTemplates:addCreatureTemplate(theme_park_smc_rancor_pygmy_maufel, "theme_park_smc_rancor_pygmy_maufel")
