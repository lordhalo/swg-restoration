theme_park_singing_mountain_clan_rancor_tamer = Creature:new {
  objectName = "@mob/creature_names:singing_mtn_clan_rancor_tamer",
  randomNameType = NAME_GENERIC,
  randomNameTag = true,
  socialGroup = "mtn_clan",
  faction = "mtn_clan",
  npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = ATTACKABLE,
  creatureBitmask = PACK,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_dathomir_sing_mt_clan_rancor_tamer.iff"},
  lootGroups = {},
  weapons = {"mixed_force_weapons"},
  conversationTemplate = "theme_park_smc_vhaunda_izaryx_mission_target_convotemplate",
  attacks = merge(brawlermaster,pikemanmaster)
}

CreatureTemplates:addCreatureTemplate(theme_park_singing_mountain_clan_rancor_tamer, "theme_park_singing_mountain_clan_rancor_tamer")
