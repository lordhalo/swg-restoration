raglith = Creature:new {
	objectName = "",
	customName = "Raglith",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_raglith.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_marauder_raglith_jorak_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(raglith, "raglith")
