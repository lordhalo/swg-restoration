fightmaster_jorak = Creature:new {
	objectName = "@mob/creature_names:fightmaster_jorak",
	socialGroup = "endor_marauder",
	faction = "endor_marauder",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_fightmaster_jorak.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_marauder_raglith_jorak_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(fightmaster_jorak, "fightmaster_jorak")
