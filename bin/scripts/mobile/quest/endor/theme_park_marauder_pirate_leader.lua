theme_park_marauder_pirate_leader = Creature:new {
	objectName = "@mob/creature_names:marooned_pirate_captain",
	socialGroup = "pirate",
	faction = "pirate",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_scarab_pirate_general_twilek_male_01.iff"},
	lootGroups =
	{
		{
			groups = {
				{group = "theme_park_loot_marauder_pirate_rifle", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(theme_park_marauder_pirate_leader, "theme_park_marauder_pirate_leader")
