theme_park_marauder_lizardman = Creature:new {
	objectName = "@npc_name:trandoshan_base_male",
	customName = "Lizardman",
	socialGroup = "",
	faction = "",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/trandoshan_male.iff",
		"object/mobile/dressed_cobral_runner_trandoshan_male_01.iff",
		"object/mobile/dressed_commoner_tatooine_trandoshan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_marauder_charal_mission_target_convotemplate",
	attacks = brawlernovice
}

CreatureTemplates:addCreatureTemplate(theme_park_marauder_lizardman, "theme_park_marauder_lizardman")
