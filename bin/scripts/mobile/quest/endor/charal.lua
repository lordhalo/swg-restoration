charal = Creature:new {
	objectName = "@mob/creature_names:charal",
	socialGroup = "endor_marauder",
	faction = "endor_marauder",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/charal.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_marauder_charal_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(charal, "charal")
