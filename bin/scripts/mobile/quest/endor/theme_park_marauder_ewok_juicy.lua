theme_park_marauder_ewok_juicy = Creature:new {
	objectName = "@npc_name:ewok_base_male",
	customName = "Juicy Ewok",
	socialGroup = "gondula_tribe",
	faction = "gondula_tribe",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_ewok_m_04.iff",
		"object/mobile/dressed_ewok_m_05.iff",
		"object/mobile/ewok_male.iff"},
	lootGroups = {},
	weapons = {"ewok_weapons"},
	conversationTemplate = "theme_park_marauder_charal_mission_target_convotemplate",
	attacks = merge(riflemanmaster,brawlermaster)
}
CreatureTemplates:addCreatureTemplate(theme_park_marauder_ewok_juicy, "theme_park_marauder_ewok_juicy")
