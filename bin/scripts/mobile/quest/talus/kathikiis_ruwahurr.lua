kathikiis_ruwahurr = Creature:new {
	objectName = "@mob/creature_names:kathikiis_ruwahurr",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/selonian_female.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kathikiis_ruwahurr_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(kathikiis_ruwahurr, "kathikiis_ruwahurr")
