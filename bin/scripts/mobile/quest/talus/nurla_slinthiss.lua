nurla_slinthiss = Creature:new {
	objectName = "@npc_name:twilek_base_female",
	customName = "Nurla Slinthiss",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_nurla_slinthiss.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "nurla_slinthiss_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(nurla_slinthiss, "nurla_slinthiss")
