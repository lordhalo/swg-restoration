clientrep_quest_green = Creature:new {
	objectName = "@mob/creature_names:assassin_mission_recruiter_neutral",
	socialGroup = "hutt",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_assassin_mission_giver_01.iff",
		"object/mobile/dressed_assassin_mission_giver_02.iff",
		"object/mobile/dressed_assassin_mission_giver_03.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "green_laser_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(clientrep_quest_green, "clientrep_quest_green")
