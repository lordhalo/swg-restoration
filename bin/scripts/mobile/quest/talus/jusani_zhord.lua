jusani_zhord = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Jusani Zhord",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_jusani_zhord.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jusani_zhord_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(jusani_zhord, "jusani_zhord")
