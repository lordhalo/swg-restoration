slooni_slavemaster = Creature:new {
	objectName = "@mob/creature_names:slavemaster",
	socialGroup = "slaver",
	faction = "thug",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_slicer_human_male_01.iff"},
	lootGroups = { },
	weapons = {},
	conversationTemplate = "slooni_jong_mission_target_convotemplate",
	attacks = brawlermid
}

CreatureTemplates:addCreatureTemplate(slooni_slavemaster, "slooni_slavemaster")
