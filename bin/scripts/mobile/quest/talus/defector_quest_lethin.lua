defector_quest_lethin = Creature:new {
	objectName = "@mob/creature_names:imperial_medic",
	socialGroup = "imperial",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_medic1_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lethin_bludder_mission_target_convotemplate",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster, teraskasinovice)
}

CreatureTemplates:addCreatureTemplate(defector_quest_lethin, "defector_quest_lethin")
