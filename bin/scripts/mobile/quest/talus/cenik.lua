cenik = Creature:new {
	objectName = "@mob/creature_names:spice_collective_courier",
	socialGroup = "spice_collective",
	faction = "spice_collective",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_spice_collective_courier_rodian_female_01.iff"},
	lootGroups = { },
	weapons = { },
	conversationTemplate = "sigrix_slix_mission_target_convotemplate",
	attacks = { }
}

CreatureTemplates:addCreatureTemplate(cenik, "cenik")
