nurla_corsec_captain = Creature:new {
	objectName = "@mob/creature_names:corsec_captain",
	socialGroup = "corsec",
	faction = "corsec",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_corsec_captain_human_female_01.iff",
		"object/mobile/dressed_corsec_captain_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "nurla_slinthiss_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(nurla_corsec_captain, "nurla_corsec_captain")
