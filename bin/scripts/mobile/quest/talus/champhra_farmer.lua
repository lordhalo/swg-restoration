champhra_farmer = Creature:new {
	objectName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_quest_farmer.iff" },
	lootGroups = {},
	weapons = {},
	conversationTemplate = "champhra_biahin_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(champhra_farmer, "champhra_farmer")
