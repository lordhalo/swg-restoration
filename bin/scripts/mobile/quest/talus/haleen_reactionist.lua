haleen_reactionist = Creature:new {
	objectName = "@mob/creature_names:liberation_reactionist",
	socialGroup = "liberation_party",
	faction = "liberation_party",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_talus_sif_mercenary_trand_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "haleen_snowline_hagrin_zeed_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(haleen_reactionist, "haleen_reactionist")
