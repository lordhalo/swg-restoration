gravin_attal = Creature:new {
	objectName = "",
	customName = "Gravin Attal",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_gravin_attal.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "gravin_attal_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(gravin_attal, "gravin_attal")
