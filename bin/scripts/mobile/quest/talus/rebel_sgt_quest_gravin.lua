rebel_sgt_quest_gravin = Creature:new {
	objectName = "@mob/creature_names:rebel_staff_sergeant",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rebel_staff_sergeant_sullustan_male_01.iff"},
	lootGroups = {
		{
			groups = {
					{group = "task_loot_hyperdrive_part_quest_gravin", chance = 10000000}
				},
			lootChance = 10000000
		}
	},
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	personalityStf = "@hireling/hireling_military",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(rebel_sgt_quest_gravin, "rebel_sgt_quest_gravin")
