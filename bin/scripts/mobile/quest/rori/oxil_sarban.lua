oxil_sarban = Creature:new {
  objectName = "",
  customName = "Oxil Sarban",
  socialGroup = "townsperson",
  faction = "townsperson",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_oxil_sarban.iff"},
  lootGroups = {},
  weapons = {},
  conversationTemplate = "oxil_sarban_mission_giver_convotemplate",
  attacks = {
  }
}

CreatureTemplates:addCreatureTemplate(oxil_sarban, "oxil_sarban")
