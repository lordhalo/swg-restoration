zeelius_kraymunder = Creature:new {
	objectName = "",
	customName = "Zeelius Kraymunder",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_zeelius_kraymunder.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "zeelius_kraymunder_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(zeelius_kraymunder, "zeelius_kraymunder")
