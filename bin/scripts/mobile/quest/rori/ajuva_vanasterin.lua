ajuva_vanasterin = Creature:new {
	objectName = "",
	customName = "Ajuva Vanasterin",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_ajuva_vanasterin.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ajuva_vanasterin_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(ajuva_vanasterin, "ajuva_vanasterin")
