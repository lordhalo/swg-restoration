jinderliss_prason = Creature:new {
	objectName = "",
	customName = "Jinderliss Prason",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_old_twk_male_01.iff",
			"object/mobile/dressed_noble_old_twk_male_02.iff"},
	lootGroups = {
	},
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "ajuva_vanasterin_mission_target_convotemplate",
	attacks = merge(brawlernovice,marksmannovice)
}

CreatureTemplates:addCreatureTemplate(jinderliss_prason, "jinderliss_prason")
