odoof_grunda = Creature:new {
  objectName = "",
  customName = "Odoof Grunda",
  socialGroup = "townsperson",
  faction = "townsperson",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/gungan_male.iff"},
  lootGroups = {},
  weapons = {},
  conversationTemplate = "jaleela_bindoo_mission_target_convotemplate",
  attacks = {
  }
}

CreatureTemplates:addCreatureTemplate(odoof_grunda, "odoof_grunda")
