indintra_imbru_yerevan = Creature:new {
	objectName = "",
	customName = "Indintra Imbru Yerevan",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_indintra_imbru_yerevan.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "indintra_imbru_yerevan_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(indintra_imbru_yerevan, "indintra_imbru_yerevan")
