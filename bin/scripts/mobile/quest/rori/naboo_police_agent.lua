naboo_police_agent = Creature:new {
	objectName = "@mob/creature_names:naboo_police",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_police.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biribas_tarun_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(naboo_police_agent, "naboo_police_agent")
