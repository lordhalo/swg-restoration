runaway_gualama = Creature:new {
	objectName = "@mob/creature_names:gualama",
	socialGroup = "gualama",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "meat_wild",
	meatAmount = 200,
	hideType = "hide_wooly",
	hideAmount = 150,
	boneType = "bone_mammal",
	boneAmount = 90,
	milkType = "milk_wild",
	milk = 100,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/gualama.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "hefsen_zindalai_mission_target_convotemplate",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(runaway_gualama, "runaway_gualama")
