draya_korbinari_smuggler = Creature:new {
	objectName = "@mob/creature_names:smuggler",
	customName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_kobola_smuggler_trandoshan_male_01.iff"},
	weapons = {"pirate_weapons_medium"},
	conversationTemplate = "",
	lootGroups = {
		{
			groups = {
				{ group = "task_loot_draya_korbinari_art_holo", chance = 10000000},			},
 			lootChance = 10000000
		},
	},
	outfit = "",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(draya_korbinari_smuggler, "draya_korbinari_smuggler")
