rohd_gostervek = Creature:new {
	objectName = "@mob/creature_names:imperial_army_captain",
	customName = "Captain Rohd Gostervek",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_captain_m.iff"},
	lootGroups = {
		{
			groups = {
				{group = "task_loot_booto_lubble_thermal_detonator", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"imperial_weapons_medium"},
	attacks = merge(riflemanmaster,carbineermaster,brawlermaster),
	conversationTemplate = "",
	optionsBitmask = AIENABLED
}

CreatureTemplates:addCreatureTemplate(rohd_gostervek, "rohd_gostervek")
