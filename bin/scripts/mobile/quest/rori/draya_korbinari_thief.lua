draya_korbinari_thief = Creature:new {
	objectName = "@mob/creature_names:thief",
	customName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_kobola_thief_rodian_male_01.iff"},
	weapons = {"pirate_weapons_medium"},
	conversationTemplate = "draya_korbinari_mission_target_convotemplate",
	lootGroups = {},
	outfit = "",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(draya_korbinari_thief, "draya_korbinari_thief")
