gadget_specialist = Creature:new {
	objectName = "@mob/creature_names:quest_gadget_specialist",
	socialGroup = "townsperson",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_combatmedic_trainer_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "villageGadgetSpecialistPhase4ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(gadget_specialist, "gadget_specialist")
