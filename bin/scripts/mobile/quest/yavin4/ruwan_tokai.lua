ruwan_tokai = Creature:new {
  objectName = "@mob/creature_names:sergeant_ruwan_tokai",
  socialGroup = "imperial",
  faction = "imperial",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0.000000,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_ruwan_tokai.iff"},
  lootGroups = {},
  weapons = {},
  conversationTemplate = "ruwan_tokai_mission_giver_convotemplate",
  attacks = {}
}

CreatureTemplates:addCreatureTemplate(ruwan_tokai, "ruwan_tokai")
