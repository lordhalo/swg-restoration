megan_drlar = Creature:new {
	objectName = "@mob/creature_names:megan_drlar",
	socialGroup = "thug",
	faction = "thug",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/megan_drlar.iff"},
	lootGroups = {},
	weapons = {},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "megan_drlar_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(megan_drlar, "megan_drlar")
