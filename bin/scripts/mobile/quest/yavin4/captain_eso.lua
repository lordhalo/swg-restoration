captain_eso = Creature:new {
	objectName = "@mob/creature_names:captain_eso",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_captain_eso.iff"},
	lootGroups = {},
	weapons = {"imperial_weapons_heavy"},
	conversationTemplate = "captain_eso_mission_giver_convotemplate",
	reactionStf = "@npc_reaction/military",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(captain_eso, "captain_eso")
