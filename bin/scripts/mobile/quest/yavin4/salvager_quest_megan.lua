salvager_quest_megan = Creature:new {
	objectName = "@mob/creature_names:info_broker",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "poacher",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_smuggler_human_female_01.iff",
				 "object/mobile/dressed_criminal_smuggler_human_male_01.iff"
				 },
	lootGroups = {},
	weapons = {},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "megan_drlar_mission_target_convotemplate",
	attacks = merge(brawlermaster, teraskasinovice)
}

CreatureTemplates:addCreatureTemplate(salvager_quest_megan, "salvager_quest_megan")
