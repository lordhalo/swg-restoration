surveyor_quest_jazeen = Creature:new {
	objectName = "@mob/creature_names:crafting_contractor",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_artisan_trainer_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jazeen_thurmm_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(surveyor_quest_jazeen, "surveyor_quest_jazeen")
