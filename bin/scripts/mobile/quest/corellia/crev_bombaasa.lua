crev_bombaasa = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Crev Bombaasa",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_crev_bombaasa.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "crev_bombaasa_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(crev_bombaasa, "crev_bombaasa")
