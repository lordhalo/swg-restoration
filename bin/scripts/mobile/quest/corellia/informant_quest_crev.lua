informant_quest_crev = Creature:new {
	objectName = "@mob/creature_names:spynet_operative",
	socialGroup = "spynet",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_eisley_officer_bothan_female_01.iff",
		"object/mobile/dressed_eisley_officer_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "crev_bombaasa_mission_target_convotemplate",
	reactionStf = "@npc_reaction/slang",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(informant_quest_crev, "informant_quest_crev")
