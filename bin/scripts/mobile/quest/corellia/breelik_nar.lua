breelik_nar = Creature:new {
	objectName = "",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_commoner_artisan_trandoshan_male_01.iff",
				"object/mobile/dressed_commoner_artisan_bith_male_01.iff",
				"object/mobile/dressed_commoner_artisan_sullustan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lady_hutt_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(breelik_nar, "breelik_nar")
