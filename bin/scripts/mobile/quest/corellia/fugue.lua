fugue = Creature:new {
	objectName = "",
	socialGroup = "thug",
	faction = "thug",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = STALKER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_pirate_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "noren_krast_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(fugue, "fugue")
