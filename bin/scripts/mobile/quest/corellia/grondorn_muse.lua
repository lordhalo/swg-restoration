grondorn_muse = Creature:new {
	objectName = "",
	customName = "Grondorn Muse",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_grondorn_muse.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "grondorn_muse_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(grondorn_muse, "grondorn_muse")
