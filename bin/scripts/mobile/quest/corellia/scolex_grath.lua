scolex_grath = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Scolex Grath",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_scolex_grath.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "scolex_grath_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(scolex_grath, "scolex_grath")
