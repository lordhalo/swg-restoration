bandleader_quest_grondorn = Creature:new {
	objectName = "@mob/creature_names:artisan",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_artisan_bith_male_01.iff"},
	lootGroups = {},
	weapons = {},
	outfit = "band_fanfar_outfit",
	conversationTemplate = "grondorn_muse_mission_target_convotemplate",
	reactionStf = "@npc_reaction/townsperson",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(bandleader_quest_grondorn, "bandleader_quest_grondorn")
