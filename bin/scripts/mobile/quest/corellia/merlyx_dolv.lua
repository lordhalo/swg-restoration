merlyx_dolv = Creature:new {
	objectName = "@mob/creature_names:commoner",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {"object/mobile/dressed_tatooine_valarian_assassin.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "diktatTargetConvo",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(merlyx_dolv, "merlyx_dolv")
