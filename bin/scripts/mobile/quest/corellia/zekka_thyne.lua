zekka_thyne = Creature:new {
	objectName = "@npc_name:bothan_base_male",
	customName = "Zekka Thyne",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "blk_sun_tasks_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(zekka_thyne, "zekka_thyne")
