shaedra_valouri = Creature:new {
	objectName = "@mob/creature_names:corsec_investigator",
	socialGroup = "corsec",
	faction = "corsec",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corsec_detective_human_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "denell_kel_vannon_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(shaedra_valouri, "shaedra_valouri")
