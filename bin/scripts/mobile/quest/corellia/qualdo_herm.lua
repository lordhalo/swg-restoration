qualdo_herm = Creature:new {
	objectName = "@mob/creature_names:qualdo_herm",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_qualdo_herm.iff"},
	lootGroups = {},
	conversationTemplate = "theme_park_rebel_mission_giver_convotemplate",
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(qualdo_herm, "qualdo_herm")
