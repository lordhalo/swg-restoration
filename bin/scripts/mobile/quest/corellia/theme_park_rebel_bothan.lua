theme_park_rebel_bothan = Creature:new {
	objectName = "",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_naboo_bothan_female_01.iff",
				 "object/mobile/dressed_commoner_naboo_bothan_female_02.iff",
				 "object/mobile/dressed_commoner_naboo_bothan_male_01.iff",
				 "object/mobile/dressed_commoner_naboo_bothan_male_02.iff",
				 "object/mobile/dressed_criminal_thug_bothan_female_01.iff",
				 "object/mobile/dressed_criminal_thug_bothan_male_01.iff",
				 "object/mobile/dressed_eisley_officer_bothan_female_01.iff",
				 "object/mobile/dressed_eisley_officer_bothan_male_01.iff",
				 "object/mobile/dressed_noble_bothan_female_01.iff",
				 "object/mobile/dressed_noble_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_rebel_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(theme_park_rebel_bothan, "theme_park_rebel_bothan")
