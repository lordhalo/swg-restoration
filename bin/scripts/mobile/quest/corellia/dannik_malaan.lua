dannik_malaan = Creature:new {
	objectName = "@npc_name:zabrak_base_male",
	customName = "Dannik Malaan",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_fat_zabrak_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "dannik_malaan_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(dannik_malaan, "dannik_malaan")
