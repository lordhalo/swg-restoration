midgoss_dlabaninaph = Creature:new {
	objectName = "@mob/creature_names:patron_devaronian_male",
	customName = "Midgoss D'labaninaph",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {"object/mobile/devaronian_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "diktatTargetConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(midgoss_dlabaninaph, "midgoss_dlabaninaph")
