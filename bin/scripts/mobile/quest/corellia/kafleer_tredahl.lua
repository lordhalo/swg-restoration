kafleer_tredahl = Creature:new {
	objectName = "",
	socialGroup = "townsperson",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_criminal_smuggler_human_male_01.iff" },
	lootGroups = {},
	weapons = {},
	conversationTemplate = "chertyl_ruluwoor_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kafleer_tredahl, "kafleer_tredahl")
