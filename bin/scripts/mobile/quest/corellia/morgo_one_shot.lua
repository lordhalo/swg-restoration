morgo_one_shot = Creature:new {
	objectName = "",
	customName = "Morgo One-Shot",
	socialGroup = "corsec",
	faction = "corsec",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,
	templates = {
		"object/mobile/dressed_corsec_pilot_human_male_01.iff"
	},
	lootGroups = { },
	weapons = {},
	conversationTemplate = "diktatTargetConvo",
	attacks = { }
}

CreatureTemplates:addCreatureTemplate(morgo_one_shot, "morgo_one_shot")
