missy = Creature:new {
	objectName = "@mob/creature_names:sand_panther_cub",
	socialGroup = "panther",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "meat_carnivore",
	meatAmount = 25,
	hideType = "hide_bristley",
	hideAmount = 14,
	boneType = "bone_mammal",
	boneAmount = 12,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/corellian_sand_panther_cub.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "diktatTargetConvo",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(missy, "missy")
