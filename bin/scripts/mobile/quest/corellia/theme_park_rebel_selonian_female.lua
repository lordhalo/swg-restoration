theme_park_rebel_selonian_female = Creature:new {
	objectName = "",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_selonian_f_01.iff",
				 "object/mobile/dressed_selonian_f_02.iff",
				 "object/mobile/dressed_selonian_f_03.iff",
				 "object/mobile/dressed_selonian_f_04.iff",
				 "object/mobile/dressed_selonian_f_05.iff",
				 "object/mobile/dressed_selonian_f_06.iff",
				 "object/mobile/dressed_selonian_f_07.iff",
				 "object/mobile/dressed_selonian_f_08.iff",
				 "object/mobile/dressed_selonian_f_09.iff",
				 "object/mobile/dressed_selonian_f_10.iff",
				 "object/mobile/dressed_selonian_f_11.iff",
				 "object/mobile/dressed_selonian_f_12.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_rebel_mission_target_convotemplate",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(theme_park_rebel_selonian_female, "theme_park_rebel_selonian_female")
