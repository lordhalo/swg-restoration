servant_quest_crev = Creature:new {
	objectName = "@mob/creature_names:slave",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/twilek_female.iff",
				 "object/mobile/twilek_male.iff",
				 "object/mobile/wookiee_male.iff",
				 "object/mobile/wookiee_female.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "crev_bombaasa_mission_target_convotemplate",
	reactionStf = "@npc_reaction/townperson",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(servant_quest_crev, "servant_quest_crev")
