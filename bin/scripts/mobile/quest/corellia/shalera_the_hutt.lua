shalera_the_hutt = Creature:new {
	objectName = "@npc_name:hutt_base_female",
	customName = "Shalera the Hutt",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/shalera.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lady_hutt_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(shalera_the_hutt, "shalera_the_hutt")
