ara_niorri = Creature:new {
	objectName = "",
	customName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_criminal_thug_aqualish_female_01.iff",
		"object/mobile/dressed_criminal_thug_aqualish_female_02.iff",
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "zakarisz_ghent_mission_target_convotemplate",
	attacks ={}
}

CreatureTemplates:addCreatureTemplate(ara_niorri, "ara_niorri")
