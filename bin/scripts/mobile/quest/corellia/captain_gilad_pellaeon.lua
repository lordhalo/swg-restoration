captain_gilad_pellaeon = Creature:new {
	objectName = "",
	customName = "Captain Gilad Pellaeon",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_captain_gilad_pellaeon.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "gilad_pellaeon_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(captain_gilad_pellaeon, "captain_gilad_pellaeon")
