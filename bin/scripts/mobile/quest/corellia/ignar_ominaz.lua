ignar_ominaz = Creature:new {
	objectName = "",
	customName = "Ignar Ominaz",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_ignar_ominaz.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ignar_ominaz_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(ignar_ominaz, "ignar_ominaz")
