theme_park_imperial_captain_sargon = Creature:new {
	objectName = "@mob/creature_names:crackdown_imperial_army_captain",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_captain_m.iff"},
	lootGroups = {
		{
			groups = {
				{group = "theme_park_loot_imperial_captain_sargon", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"imperial_weapons_medium"},
	attacks = merge(brawlermaster,marksmanmaster,riflemanmaster,carbineermaster),
	conversationTemplate = "",
	optionsBitmask = AIENABLED
}

CreatureTemplates:addCreatureTemplate(theme_park_imperial_captain_sargon, "theme_park_imperial_captain_sargon")
