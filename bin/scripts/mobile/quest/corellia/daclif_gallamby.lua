daclif_gallamby = Creature:new {
	objectName = "",
	customName = "Daclif Gallamby",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_daclif_gallamby.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "diktatGiverConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(daclif_gallamby, "daclif_gallamby")
