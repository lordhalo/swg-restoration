gleeb_tchotle = Creature:new {
	objectName = "",
	customName = "Gleeb Tchotle",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_patron_ishi_tib_01.iff"},
	lootGroups = {
		{
			groups = {
					{group = "task_loot_ledger_quest_serjix", chance = 10000000}
				},
			lootChance = 10000000
		}
	},
	weapons = {},
	attacks = merge(brawlermaster, teraskasinovice)
}

CreatureTemplates:addCreatureTemplate(gleeb_tchotle, "gleeb_tchotle")
