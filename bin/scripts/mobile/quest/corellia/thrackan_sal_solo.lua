thrackan_sal_solo = Creature:new {
	objectName = "",
	customName = "Thrackan Sal Solo",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_mayor.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "thrackan_sal_solo_mission_giver_convotemplate",
	outfit = "thracken_sal_solo_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(thrackan_sal_solo, "thrackan_sal_solo")
