rep_been = Creature:new {
	objectName = "",
	customName = "Rep Been",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/gungan_s03_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "rep_been_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(rep_been, "rep_been")
