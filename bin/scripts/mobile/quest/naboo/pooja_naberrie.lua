pooja_naberrie = Creature:new {
	objectName = "",
	customName = "Pooja Naberrie",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_pooja_naberrie.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "pooja_naberrie_mission_giver_convotemplate",
	attacks = {},
}

CreatureTemplates:addCreatureTemplate(pooja_naberrie, "pooja_naberrie")
