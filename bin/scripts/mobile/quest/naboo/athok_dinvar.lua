athok_dinvar = Creature:new {
	objectName = "",
	customName = "Athok Dinvar",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_athok_dinvar.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "athok_dinvar_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(athok_dinvar, "athok_dinvar")
