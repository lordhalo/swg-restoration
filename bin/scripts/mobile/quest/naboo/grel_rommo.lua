grel_rommo = Creature:new {
	objectName = "@mob/creature_names:rebel_pilot",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
	"object/mobile/dressed_rebel_pilot_human_male_01.iff",
	"object/mobile/dressed_rebel_pilot_human_male_02.iff",
	"object/mobile/dressed_rebel_pilot_rodian_male_01.iff"},
	lootGroups = {},
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "arven_wendik_mission_target_convotemplate",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(grel_rommo, "grel_rommo")
