theme_park_imperial_borvos_thug = Creature:new {
	objectName = "@mob/creature_names:borvos_thug",
	socialGroup = "borvo",
	faction = "borvo",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_borvos_thug.iff"},
	lootGroups = {
	    {
			groups = {
				{group = "theme_park_loot_shipping_manifest", chance = 10000000},
			},
			lootChance = 10000000
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(theme_park_imperial_borvos_thug, "theme_park_imperial_borvos_thug")
