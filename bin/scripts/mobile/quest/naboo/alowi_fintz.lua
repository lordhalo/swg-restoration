alowi_fintz = Creature:new {
  objectName = "",
  customName = "Alowi Fintz",
  socialGroup = "rebel",
  faction = "rebel",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_criminal_smuggler_human_female_01.iff"},
  lootGroups = {},
  weapons = {"pirate_weapons_heavy"},
  conversationTemplate = "arven_wendik_mission_target_convotemplate",
  attacks = merge(riflemanmaster,pistoleermaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(alowi_fintz, "alowi_fintz")
