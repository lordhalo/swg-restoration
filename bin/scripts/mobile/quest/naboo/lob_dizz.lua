lob_dizz = Creature:new {
	objectName = "",
	customName = "Lob Dizz",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/gungan_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lob_dizz_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(lob_dizz, "lob_dizz")
