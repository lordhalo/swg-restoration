majordomo_quest_bardo = Creature:new {
	objectName = "@mob/creature_names:noble",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_noble_old_twk_female_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bardo_klinj_mission_target_convotemplate",
	attacks = merge(brawlermaster,teraskasinovice)
}

CreatureTemplates:addCreatureTemplate(majordomo_quest_bardo, "majordomo_quest_bardo")
