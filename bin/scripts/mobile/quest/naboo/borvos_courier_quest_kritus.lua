borvos_courier_quest_kritus = Creature:new {
	objectName = "@mob/creature_names:borvos_mercenary",
	socialGroup = "borvo",
	faction = "borvo",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_borvos_mercenary.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kritus_morven_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(borvos_courier_quest_kritus, "borvos_courier_quest_kritus")
