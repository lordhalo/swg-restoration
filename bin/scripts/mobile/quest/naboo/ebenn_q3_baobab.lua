ebenn_q3_baobab = Creature:new {
	objectName = "",
	customName = "Ebenn Q3 Baobab",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_human_male_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ebenn_q3_baobab_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(ebenn_q3_baobab, "ebenn_q3_baobab")
