arrek_von_sarko = Creature:new {
	objectName = "",
	customName = "Arrek Von Sarko",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/human_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "arrek_von_sarko_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(arrek_von_sarko, "arrek_von_sarko")
