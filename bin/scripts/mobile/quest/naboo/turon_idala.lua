turon_idala = Creature:new {
	objectName = "@mob/creature_names:naboo_police",
	customName = "Turon Adala",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_police.iff"},
	conversationTemplate = "pooja_naberrie_mission_target_convotemplate",
  	weapons = {"rebel_weapons_medium"},
  	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(turon_idala, "turon_idala")
