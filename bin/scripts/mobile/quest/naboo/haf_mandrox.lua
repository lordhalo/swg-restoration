haf_mandrox = Creature:new {
	objectName = "",
	customName = "Haf Mandrox",
	socialGroup = "borvo",
	faction = "borvo",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_unarmed_trainer_01.iff" },
	lootGroups = {},
	weapons = {},
	conversationTemplate = "gavyn_sykes_mission_target_convotemplate",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(haf_mandrox, "haf_mandrox")
