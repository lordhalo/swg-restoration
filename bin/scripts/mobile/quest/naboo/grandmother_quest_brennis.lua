grandmother_quest_brennis = Creature:new {
	objectName = "@mob/creature_names:noble",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,


	templates = {
		"object/mobile/dressed_noble_old_human_female_01.iff",
		"object/mobile/dressed_noble_old_human_female_02.iff",
		"object/mobile/dressed_noble_old_zabrak_female_01.iff",
		"object/mobile/dressed_noble_old_zabrak_female_02.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "brennis_doore_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(grandmother_quest_brennis, "grandmother_quest_brennis")
