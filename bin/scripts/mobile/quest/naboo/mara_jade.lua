mara_jade = Creature:new {
	objectName = "@mob/creature_names:mara_jade",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/mara_jade.iff"},
	lootGroups = {},
	weapons = {},
	outfit = "mara_jade_outfit",
	conversationTemplate = "theme_park_imperial_mission_target_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mara_jade, "mara_jade")
