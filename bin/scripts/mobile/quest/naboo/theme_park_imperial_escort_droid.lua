theme_park_imperial_escort_droid = Creature:new {
	objectName = "",
	socialGroup = "",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/ra7_bug_droid.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "theme_park_imperial_mission_target_convotemplate",
	optionsBitmask = INVULNERABLE + CONVERSABLE
}

CreatureTemplates:addCreatureTemplate(theme_park_imperial_escort_droid, "theme_park_imperial_escort_droid")
