security_officer_quest_kritus = Creature:new {
	objectName = "@mob/creature_names:rsf_security_officer",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rsf_security_officer.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kritus_morven_mission_target_convotemplate",
	reactionStf = "@npc_reaction/townperson",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(security_officer_quest_kritus, "security_officer_quest_kritus")
