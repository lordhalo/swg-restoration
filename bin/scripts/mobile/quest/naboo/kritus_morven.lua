kritus_morven = Creature:new {
	objectName = "",
	customName = "Kritus Morven",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_kritus_morven.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "kritus_morven_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(kritus_morven, "kritus_morven")
