boss_nass = Creature:new {
	objectName = "@mob/creature_names:boss_nass",
	socialGroup = "gungan",
	faction = "gungan",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/boss_nass.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "boss_nass_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(boss_nass, "boss_nass")
