librarian = Creature:new {
	objectName = "@mob/creature_names:trivia_librarian",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	-- TODO: Find correct template
	templates = {"object/mobile/dressed_theed_palace_chamberlain.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "librarian_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(librarian, "librarian")
