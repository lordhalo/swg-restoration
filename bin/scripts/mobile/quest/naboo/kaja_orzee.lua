kaja_orzee = Creature:new {
	objectName = "@mob/creature_names:kaja_orzee",
	customName = "Kaja Orzee",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/kaja_orzee.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "theme_park_imperial_mission_giver_convotemplate",
	outfit = "kaja_orzee_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kaja_orzee, "kaja_orzee")
