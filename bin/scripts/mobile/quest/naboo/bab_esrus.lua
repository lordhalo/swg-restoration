bab_esrus = Creature:new {
	objectName = "",
	customName = "Bab Esrus",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_naboo_human_female_08.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bab_esrus_mission_giver_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(bab_esrus, "bab_esrus")
