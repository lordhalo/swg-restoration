theme_park_imperial_mellag_davin_lars = Creature:new {
	objectName = "",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_dissident.iff"},
	lootGroups = {
	    {
			groups = {
				{group = "theme_park_loot_information", chance = 10000000},
			},
			lootChance = 10000000
		}
	},
	weapons = {"rebel_weapons_light"},
	attacks = merge(brawlermid,marksmanmid)
}

CreatureTemplates:addCreatureTemplate(theme_park_imperial_mellag_davin_lars, "theme_park_imperial_mellag_davin_lars")
