imperial_courier_quest_bardo = Creature:new {
	objectName = "@mob/creature_names:assassin_mission_recruiter_imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_soldier_m.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bardo_klinj_mission_target_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(imperial_courier_quest_bardo, "imperial_courier_quest_bardo")
