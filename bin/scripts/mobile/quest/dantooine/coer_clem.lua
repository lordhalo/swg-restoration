coer_clem = Creature:new {
  objectName = "",
  socialGroup = "townsperson",
  faction = "townsperson",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = HERD,
  optionsBitmask = INVULNERABLE + CONVERSABLE,
  diet = HERBIVORE,

  templates =   {"object/mobile/dressed_criminal_thug_bothan_male_01.iff"},
  lootGroups = {},
  weapons = {},
  conversationTemplate = "stoos_olko_mission_target_convotemplate",
  attacks = {
  }
}

CreatureTemplates:addCreatureTemplate(coer_clem, "coer_clem")
