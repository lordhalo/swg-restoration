jatrian_lytus = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Cmdr. Jatrian Lytus",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_jatrian_lytus.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jatrian_lytus_mission_giver_convotemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(jatrian_lytus, "jatrian_lytus")
