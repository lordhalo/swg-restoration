orip_erchi = Creature:new {
	objectName = "",
	customName = "Orip Erchi",
	socialGroup = "pirate",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,


    templates = { "object/mobile/dressed_criminal_pirate_human_male_01.iff",
		"object/mobile/dressed_criminal_pirate_human_female_01.iff",
		"object/mobile/dressed_criminal_slicer_human_male_01.iff",
		"object/mobile/dressed_criminal_slicer_human_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "xaan_talmaron_mission_target_convotemplate",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(orip_erchi, "orip_erchi")
