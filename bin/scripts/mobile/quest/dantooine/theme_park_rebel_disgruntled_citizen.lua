theme_park_rebel_disgruntled_citizen = Creature:new {
	objectName = "@mob/creature_names:bounty_hunter",
	socialGroup = "mercenary",
	faction = "",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_bountyhunter_trainer_01.iff",
			"object/mobile/dressed_bountyhunter_trainer_02.iff",
			"object/mobile/dressed_bountyhunter_trainer_03.iff"},
	lootGroups =
	{
		{
			groups = {
				{group = "theme_park_loot_information", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"pirate_weapons_heavy"},
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(theme_park_rebel_disgruntled_citizen, "theme_park_rebel_disgruntled_citizen")
