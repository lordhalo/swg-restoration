stoos_stormtrooper_squad_leader = Creature:new {
  objectName = "@mob/creature_names:stormtrooper_squad_leader",
  randomNameType = NAME_STORMTROOPER,
	randomNameTag = true,
  socialGroup = "imperial",
  faction = "imperial",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {"object/mobile/dressed_stormtrooper_squad_leader_white_white.iff"},
  lootGroups = {},
  weapons = {"stormtrooper_weapons"},
  conversationTemplate = "stoos_olko_mission_target_convotemplate",
  attacks = merge(riflemanmaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(stoos_stormtrooper_squad_leader, "stoos_stormtrooper_squad_leader")
