drakka_judarrl = Creature:new {
	objectName = "",
	customName = "Drakka Judarrl",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_drakka_judarrl.iff"},
	lootGroups = {},
	weapons = {"light_jedi_weapons"},
	conversationTemplate = "drakka_judarrl_mission_giver_convotemplate",
	attacks = merge(lightsabermaster)
}

CreatureTemplates:addCreatureTemplate(drakka_judarrl, "drakka_judarrl")
