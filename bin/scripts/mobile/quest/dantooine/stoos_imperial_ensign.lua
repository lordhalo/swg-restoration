stoos_imperial_ensign = Creature:new {
  objectName = "",
  socialGroup = "imperial",
  faction = "imperial",
  npcStats = {40, 93, 4561, 72, 315, 93, 0, 2848},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0.,
  ferocity = 0,
  pvpBitmask = NONE,
  creatureBitmask = NONE,
  optionsBitmask = AIENABLED + CONVERSABLE,
  diet = HERBIVORE,

  templates = {
    "object/mobile/dressed_imperial_officer_f.iff",
    "object/mobile/dressed_imperial_officer_m.iff",
    "object/mobile/dressed_imperial_officer_m_2.iff",
    "object/mobile/dressed_imperial_officer_m_3.iff",
    "object/mobile/dressed_imperial_officer_m_4.iff",
    "object/mobile/dressed_imperial_officer_m_5.iff",
    "object/mobile/dressed_imperial_officer_m_6.iff"},
  lootGroups = {},
  weapons = {"imperial_weapons_medium"},
  conversationTemplate = "stoos_olko_mission_target_convotemplate",
  attacks = {}

}

CreatureTemplates:addCreatureTemplate(stoos_imperial_ensign, "stoos_imperial_ensign")
