pilot = Creature:new {
	objectName = "@mob/creature_names:pilot",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "mercenary",
	faction = "",
	npcStats = {7, 35, 990, 37, 218, 35, 0, 568},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mercenary_messanger_hum_f.iff",
		"object/mobile/dressed_mercenary_weak_hum_m.iff"},
	lootGroups = {},
	weapons = {"rebel_weapons_light"},
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(pilot, "pilot")
