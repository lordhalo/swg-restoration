pathfinder = Creature:new {
	objectName = "",
	customName = "Pathfinder",
	socialGroup = "wilder",
	faction = "",
	npcStats = {19, 65, 2841, 50, 249, 65, 0, 1254},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_thug_aqualish_male_01.iff",
			"object/mobile/dressed_criminal_thug_aqualish_male_02.iff",
			"object/mobile/dressed_criminal_thug_aqualish_female_01.iff",
			"object/mobile/dressed_criminal_thug_aqualish_female_02.iff",
			"object/mobile/dressed_criminal_thug_rodian_male_01.iff",
			"object/mobile/dressed_criminal_thug_rodian_female_01.iff", },
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4000000},
				{group = "wearables_common", chance = 3000000},
				{group = "loot_kit_parts", chance = 2000000},
				{group = "tailor_components", chance = 1000000},
			}
		}
	},
	weapons = {"rebel_weapons_heavy"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(pathfinder, "pathfinder")
