shadowy_figure = Creature:new {
	objectName = "@mob/creature_names:assassin_mission_recruiter_neutral",
	socialGroup = "hutt",
	faction = "",
	npcStats = {16, 50, 2596, 48, 246, 50, 0, 1086},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_assassin_mission_giver_01.iff",
		"object/mobile/dressed_assassin_mission_giver_02.iff",
		"object/mobile/dressed_assassin_mission_giver_03.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(shadowy_figure, "shadowy_figure")
