franz_outfit = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/bounty_hunter/armor_bounty_hunter_chest_plate.iff", customizationVariables = {} },
		--{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/pants/pants_s02.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/belt/belt_s02.iff", customizationVariables = {{"/private/index_color_1", 216}} },
		{objectTemplate = "object/tangible/wearables/bandolier/bandolier_s02.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("franz_outfit", franz_outfit)
