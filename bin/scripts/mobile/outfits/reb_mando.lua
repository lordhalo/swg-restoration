reb_mando = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_helmet.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_chest_plate.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_leggings.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_bracer_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_bracer_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_bicep_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_bicep_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_rebel/armor_mandalorian_rebel_belt.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("reb_mando", reb_mando)
