rebel_assault_trooper = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_helmet.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_chest_plate.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_leggings.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_bracer_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_bracer_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_bicep_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/rebel_assault/armor_rebel_assault_bicep_r.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("rebel_assault_trooper", rebel_assault_trooper)
