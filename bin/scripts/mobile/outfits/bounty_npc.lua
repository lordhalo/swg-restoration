bounty_npc = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/mandalorian/armor_mandalorian_helmet.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian/armor_mandalorian_chest_plate.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/bounty_hunter/armor_bounty_hunter_leggings.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_bicep_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_bicep_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_bracer_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/composite/armor_composite_bracer_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian/armor_mandalorian_belt.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("bounty_npc", bounty_npc)
