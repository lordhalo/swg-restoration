imp_mando = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_helmet.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_chest_plate.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_leggings.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_bracer_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_bracer_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_bicep_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_bicep_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/mandalorian_imperial/armor_mandalorian_imperial_belt.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("imp_mando", imp_mando)
