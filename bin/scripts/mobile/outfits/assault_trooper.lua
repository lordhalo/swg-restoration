assault_trooper = {
	{ 
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_helmet.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_chest_plate.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_leggings.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_boots.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_gloves.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_bracer_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_bracer_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_bicep_l.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_bicep_r.iff", customizationVariables = {} },
		{objectTemplate = "object/tangible/wearables/armor/assault_trooper/armor_assault_trooper_utility_belt.iff", customizationVariables = {} },
		
	}
	 
}

addOutfitGroup("assault_trooper", assault_trooper)
