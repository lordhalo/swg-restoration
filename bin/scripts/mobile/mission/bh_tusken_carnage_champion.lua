bh_tusken_carnage_champion = Creature:new {
	objectName = "@mob/creature_names:tusken_fort_tusken_champion",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	npcStats = {84, 209, 18890, 336, 868, 349, 5913, 5363},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/tusken_raider.iff"},
	lootGroups = {
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5320000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5320000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5320000
		},
	},
	weapons = {"tusken_weapons"},
	conversationTemplate = "",
	attacks = merge(marksmanmaster,brawlermaster,fencermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(bh_tusken_carnage_champion, "bh_tusken_carnage_champion")
