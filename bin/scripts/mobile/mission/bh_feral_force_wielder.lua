bh_feral_force_wielder = Creature:new {
	objectName = "@mob/creature_names:feral_force_wilder",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "force",
	faction = "",
	npcStats = {83, 199, 18673, 333, 862, 344, 5826, 5303},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_feral_force_wielder.iff"},
	lootGroups = {
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5300000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5300000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5300000
		},
	},
	weapons = {"mixed_force_weapons"},
	conversationTemplate = "",
	attacks = merge(fencermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(bh_feral_force_wielder, "bh_feral_force_wielder")
