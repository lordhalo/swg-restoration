function createInformantNPC(lvl)
	informant_npc = Creature:new {
		objectName = "@mob/creature_names:spynet_operative",
		socialGroup = "",
		faction = "",
		npcStats = {45, 100, 12223, 231, 658, 163, 2522, 3196},
		meatType = "",
		meatAmount = 0,
		hideType = "",
		hideAmount = 0,
		boneType = "",
		boneAmount = 0,
		milk = 0,
		tamingChance = 0,
		ferocity = 0,
		pvpBitmask = NONE,
		creatureBitmask = NONE,
		optionsBitmask = INVULNERABLE + CONVERSABLE,
		diet = HERBIVORE,

		templates = {"object/mobile/dressed_hutt_informant_quest.iff"},
		lootGroups = {},
		weapons = {},
		conversationTemplate = "informant_npc_lvl_" .. lvl,
		attacks = {
		}
	}

	CreatureTemplates:addCreatureTemplate(informant_npc, "informant_npc_lvl_" .. lvl)
end

createInformantNPC("1")
createInformantNPC("2")
createInformantNPC("3")
