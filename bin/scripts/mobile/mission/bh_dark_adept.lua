bh_dark_adept = Creature:new {
	objectName = "@mob/creature_names:dark_adept",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "dark_jedi",
	faction = "",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dark_jedi_human_male_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5400000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5400000
		},
		{
			groups = {
				{group = "jetpack_parts", chance = 500000},
				{group = "bounty_hunter_armor", chance = 500000},
				{group = "jetpack_base", chance = 200000},
				{group = "loot_kit_parts", chance = 800000},
				{group = "color_crystals", chance = 1000000},
				{group = "power_crystals", chance = 1000000},
				{group = "illegal_booster", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "armor_attachments", chance = 2000000}
			},
			lootChance = 5400000
		},
	},
	weapons = {"dark_jedi_weapons_gen2"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster,forcewielder)
}

CreatureTemplates:addCreatureTemplate(bh_dark_adept, "bh_dark_adept")
