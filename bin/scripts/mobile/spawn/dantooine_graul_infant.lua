dantooine_graul_infant = {

	lairSpawns = {
		{
			lairTemplateName = "dantooine_infant_graul_lair_neutral_medium",
			spawnLimit = 15,
			minDifficulty = 111,
			maxDifficulty = 131,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("dantooine_graul_infant", dantooine_graul_infant);
