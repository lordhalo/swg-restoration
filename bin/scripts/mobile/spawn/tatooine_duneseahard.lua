tatooine_duneseahard = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_cannibal_dewback_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 21,
			maxDifficulty = 25,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_cannibal_dewback_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 21,
			maxDifficulty = 25,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_desert_razorback_razorback_neutral_medium_boss_01",
			spawnLimit = -1,
			minDifficulty = 24,
			maxDifficulty = 28,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dragonet_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dragonet_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dragonet_dragonet_neutral_medium_boss_01",
			spawnLimit = -1,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_wild_dune_boar_neutral_none",
			spawnLimit = -1,
			minDifficulty = 39,
			maxDifficulty = 53,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dune_stalker_camp_neutral_medium_theater",
			spawnLimit = -1,
			minDifficulty = 35,
			maxDifficulty = 45,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_jabba_enforcer_camp_neutral_medium_theater",
			spawnLimit = -1,
			minDifficulty = 39,
			maxDifficulty = 52,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_jabba_assasin_camp_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 45,
			maxDifficulty = 62,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_canyon_krayt_dragon_pack_neutral_none",
			spawnLimit = 1,
			minDifficulty = 122,
			maxDifficulty = 276,
			numberToSpawn = 0,
			weighting = 4,
			size = 35
		},
		{
			lairTemplateName = "tatooine_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 1,
			minDifficulty = 122,
			maxDifficulty = 276,
			numberToSpawn = 0,
			weighting = 4,
			size = 35
		},
		{
			lairTemplateName = "tatooine_giant_canyon_krayt_dragon_pack_neutral_none",
			spawnLimit = 1,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 0,
			weighting = 2,
			size = 35
		},
		{
			lairTemplateName = "tatooine_giant_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 1,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 0,
			weighting = 2,
			size = 35
		},
	}
}

addSpawnGroup("tatooine_duneseahard", tatooine_duneseahard);
