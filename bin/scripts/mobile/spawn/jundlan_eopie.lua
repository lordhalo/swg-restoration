jundlan_eopie = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_jundland_eopie_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_jundland_eopie_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
	}
}

addSpawnGroup("jundlan_eopie", jundlan_eopie);
