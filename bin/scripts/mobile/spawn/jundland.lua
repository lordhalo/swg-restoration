jundland = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_jundland_eopie_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_jundland_eopie_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_desert_demon_patrol_neutral_none",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 18,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_desert_demon_outpost_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 18,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_mound_mite_swarm_neutral_none",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 13,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},
		{
			lairTemplateName = "tatooine_mound_mite_click_neutral_small",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 13,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},
		{
			lairTemplateName = "tatooine_dewback_lesser_neutral_none",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 13,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dewback_lesser_lair_neutral_small",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 13,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_valarian_assasin_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 18,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
	}
}

addSpawnGroup("jundland", jundland);
