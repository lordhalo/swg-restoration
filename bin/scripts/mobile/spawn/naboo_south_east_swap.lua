naboo_south_east_swap = {

	lairSpawns = {

		{
			lairTemplateName = "naboo_swamp_rat_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},

		{
			lairTemplateName = "naboo_swamp_rat_lair_neutral_small",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},

		{
			lairTemplateName = "naboo_swamp_trooper_outpost_imperial_large_theater",
			spawnLimit = -1,
			minDifficulty = 25,
			maxDifficulty = 29,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_narglatch_female_neutral_none",
			spawnLimit = -1,
			minDifficulty = 23,
			maxDifficulty = 27,
			numberToSpawn = 0,
			weighting = 20,
			size = 25
		},

		{
			lairTemplateName = "naboo_narglatch_pride_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 22,
			maxDifficulty = 26,
			numberToSpawn = 0,
			weighting = 20,
			size = 25
		},


		{
			lairTemplateName = "naboo_narglatch_male_neutral_none",
			spawnLimit = -1,
			minDifficulty = 18,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 20,
			size = 25
		},

		{
			lairTemplateName = "naboo_swamp_tusk_cat_hunt_neutral_none",
			spawnLimit = -1,
			minDifficulty = 16,
			maxDifficulty = 20,
			numberToSpawn = 0,
			weighting = 25,
			size = 25
		},

		{
			lairTemplateName = "naboo_swamp_tusk_cat_pride_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 16,
			maxDifficulty = 20,
			numberToSpawn = 0,
			weighting = 25,
			size = 25
		},

		{
			lairTemplateName = "naboo_gungan_military_base_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 13,
			maxDifficulty = 42,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_borvos_camp_neutral_small_theater",
			spawnLimit = -1,
			minDifficulty = 13,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},

	}
}

addSpawnGroup("naboo_south_east_swap", naboo_south_east_swap);
