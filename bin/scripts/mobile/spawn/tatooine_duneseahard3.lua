tatooine_duneseahard3 = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_cannibal_dewback_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 21,
			maxDifficulty = 25,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_cannibal_dewback_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 21,
			maxDifficulty = 25,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dragonet_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dragonet_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dragonet_dragonet_neutral_medium_boss_01",
			spawnLimit = 3,
			minDifficulty = 29,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_wild_dune_boar_neutral_none",
			spawnLimit = -1,
			minDifficulty = 39,
			maxDifficulty = 53,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_bocatt_bocatt_neutral_medium_boss_01",
			spawnLimit = 5,
			minDifficulty = 22,
			maxDifficulty = 26,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_ronto_male_neutral_medium_boss_01",
			spawnLimit = 5,
			minDifficulty = 23,
			maxDifficulty = 27,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dune_stalker_camp_neutral_medium_theater",
			spawnLimit = -1,
			minDifficulty = 35,
			maxDifficulty = 45,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_tusken_raider_patrol_neutral_none",
			spawnLimit = -1,
			minDifficulty = 19,
			maxDifficulty = 33,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_tusken_raider_kingdom_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 41,
			maxDifficulty = 100,
			numberToSpawn = 0,
			weighting = 10,
			size = 30
		},
		{
			lairTemplateName = "tatooine_tusken_raider_village_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 8,
			maxDifficulty = 27,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 1,
			minDifficulty = 122,
			maxDifficulty = 276,
			numberToSpawn = 0,
			weighting = 4,
			size = 35
		},
		{
			lairTemplateName = "tatooine_giant_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 1,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 0,
			weighting = 2,
			size = 35
		},
	}
}

addSpawnGroup("tatooine_duneseahard3", tatooine_duneseahard3);
