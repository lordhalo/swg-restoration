lok_black_sun_wild = {

	lairSpawns = {
		{
			lairTemplateName = "lok_black_sun",
			spawnLimit = -1,
			minDifficulty = 55,
			maxDifficulty = 59,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
	}
}

addSpawnGroup("lok_black_sun_wild", lok_black_sun_wild);
