tatooine_duneseajabba = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_dewback_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 19,
			maxDifficulty = 23,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dewback_dewback_neutral_medium_boss_01",
			spawnLimit = 2,
			minDifficulty = 19,
			maxDifficulty = 23,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
				{
			lairTemplateName = "tatooine_mountain_dewback_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 20,
			maxDifficulty = 24,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_mountain_dewback_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 20,
			maxDifficulty = 24,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
				{
			lairTemplateName = "tatooine_grizzled_dewback_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 27,
			maxDifficulty = 31,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_grizzled_dewback_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 27,
			maxDifficulty = 31,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dune_bantha_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 17,
			maxDifficulty = 21,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dune_bantha_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 17,
			maxDifficulty = 21,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_bantha_dune_bantha_neutral_medium_boss_01",
			spawnLimit = 3,
			minDifficulty = 17,
			maxDifficulty = 21,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dune_lizard_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 18,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dune_lizard_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 18,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
		{
			lairTemplateName = "tatooine_dune_lizard_dune_lizard_neutral_medium_boss_01",
			spawnLimit = 1,
			minDifficulty = 18,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 8,
			size = 25
		},
				{
			lairTemplateName = "tatooine_jabba_enforcer_camp_neutral_medium_theater",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 18,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "tatooine_jabba_assasin_camp_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 9,
			maxDifficulty = 18,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "tatooine_dune_stalker_camp_neutral_medium_theater",
			spawnLimit = -1,
			minDifficulty = 8,
			maxDifficulty = 27,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
	}
}

addSpawnGroup("tatooine_duneseajabba", tatooine_duneseajabba);
