tatooine_krayt_young = {

	lairSpawns = {
		{
			lairTemplateName = "tatooine_krayt_dragon_adolescent",
			spawnLimit = 15,
			minDifficulty = 121,
			maxDifficulty = 141,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "tatooine_krayt_dragon_juvenile",
			spawnLimit = 15,
			minDifficulty = 111,
			maxDifficulty = 131,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("tatooine_krayt_young", tatooine_krayt_young);
