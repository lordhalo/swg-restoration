dantooine_adepts = {

	lairSpawns = {
		{
			lairTemplateName = "dantooine_dark_adepts",
			spawnLimit = 10,
			minDifficulty = 121,
			maxDifficulty = 141,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("dantooine_adepts", dantooine_adepts);
