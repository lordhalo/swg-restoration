includeFile("spawn/lok_black_sun_wild.lua")
includeFile("spawn/lok_kimo.lua")

includeFile("spawn/endor_death_watch_wild.lua")
includeFile("spawn/endor_gorax.lua")

includeFile("spawn/yavin4_light_enclave.lua")
includeFile("spawn/yavin4_geocave.lua")
includeFile("spawn/yavin4_force_mobs.lua")

includeFile("spawn/naboo_mountain.lua")
includeFile("spawn/naboo_south_east_swap.lua")
includeFile("spawn/naboo_hard_fambaa.lua")
includeFile("spawn/naboo_plains.lua")

includeFile("spawn/dathomir_rancor.lua")

includeFile("spawn/kashyyyk.lua")


includeFile("spawn/jundland.lua")
includeFile("spawn/jundlan_eopie.lua")
includeFile("spawn/tatooine_krayt_young.lua")
includeFile("spawn/tatooine_duneseajabba.lua")
includeFile("spawn/tatooine_duneseahard.lua")
includeFile("spawn/tatooine_duneseahard2.lua")
includeFile("spawn/tatooine_duneseahard3.lua")
includeFile("spawn/tatooine_dunesea.lua")

includeFile("spawn/dantooine_grey_jedi.lua")
includeFile("spawn/dantooine_adepts.lua")
includeFile("spawn/dantooine_graul_infant.lua")
includeFile("spawn/dantooine_graul.lua")
includeFile("spawn/dantooine_hard_graul.lua")



-- spawn groups
includeFile("spawn/corellia_world.lua")
includeFile("spawn/dantooine_world.lua")
includeFile("spawn/dathomir_world.lua")
includeFile("spawn/endor_world.lua")
includeFile("spawn/lok_world.lua")
includeFile("spawn/naboo_world.lua")
includeFile("spawn/rori_world.lua")
includeFile("spawn/talus_world.lua")
includeFile("spawn/tatooine_world.lua")
includeFile("spawn/yavin4_world.lua")

includeFile("spawn/corellia_easy.lua")
includeFile("spawn/corellia_medium.lua")
includeFile("spawn/dantooine_hard_graul.lua")
includeFile("spawn/dantooine_world_npc.lua")
includeFile("spawn/dathomir_sarlacc_mutant.lua")
includeFile("spawn/global.lua")
includeFile("spawn/global_hard.lua")
includeFile("spawn/naboo_easy.lua")
includeFile("spawn/naboo_medium.lua")
includeFile("spawn/peko_albatross.lua")
includeFile("spawn/rori_easy.lua")
includeFile("spawn/rori_medium.lua")
includeFile("spawn/talus_easy.lua")
includeFile("spawn/talus_medium.lua")
includeFile("spawn/tatooine_easy.lua")
includeFile("spawn/tatooine_medium.lua")

--destroy_mission
includeFile("spawn/destroy_mission/corellia_destroy_missions.lua")
includeFile("spawn/destroy_mission/dantooine_destroy_missions.lua")
includeFile("spawn/destroy_mission/dathomir_destroy_missions.lua")
includeFile("spawn/destroy_mission/endor_destroy_missions.lua")
includeFile("spawn/destroy_mission/lok_destroy_missions.lua")
includeFile("spawn/destroy_mission/naboo_destroy_missions.lua")
includeFile("spawn/destroy_mission/rori_destroy_missions.lua")
includeFile("spawn/destroy_mission/talus_destroy_missions.lua")
includeFile("spawn/destroy_mission/tatooine_destroy_missions.lua")
includeFile("spawn/destroy_mission/yavin4_destroy_missions.lua")
includeFile("spawn/destroy_mission/factional_imperial_destroy_missions.lua")
includeFile("spawn/destroy_mission/factional_neutral_destroy_missions.lua")
includeFile("spawn/destroy_mission/factional_rebel_destroy_missions.lua")

