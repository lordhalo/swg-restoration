dantooine_destroy_missions = {
	minLevelCeiling = 45,

	lairSpawns = {
		{
			lairTemplateName = "dantooine_bol_pigmy_neutral_small",
			minDifficulty = 8,
			maxDifficulty = 12,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_voritor_lair_neutral_medium",
			minDifficulty = 20,
			maxDifficulty = 24,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_quenker_lair_neutral_medium",
			minDifficulty = 21,
			maxDifficulty = 25,
			size = 20,
		},
		{
			lairTemplateName = "dantooine_dantari_raider_camp_neutral_large_theater",
			minDifficulty = 23,
			maxDifficulty = 27,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_huurton_howler_lair_neutral_medium_boss_01",
			minDifficulty = 23,
			maxDifficulty = 27,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_huurton_matron_lair_neutral_medium",
			minDifficulty = 24,
			maxDifficulty = 28,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_thune_lair_neutral_medium",
			minDifficulty = 24,
			maxDifficulty = 28,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_bol_lair_neutral_medium",
			minDifficulty = 45,
			maxDifficulty = 51,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_quenker_savage_lair_neutral_medium_boss_01",
			minDifficulty = 26,
			maxDifficulty = 30,
			size = 35,
		},
		{
			lairTemplateName = "dantooine_dantari_battlelord_squad_neutral_none",
			minDifficulty = 29,
			maxDifficulty = 36,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_piket_longhorn_lair_neutral_medium",
			minDifficulty = 47,
			maxDifficulty = 54,
			size = 30,
		},
		{
			lairTemplateName = "dantooine_quenker_bile_drenched_lair_neutral_medium",
			minDifficulty = 32,
			maxDifficulty = 36,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_kunga_hunter_neutral_none",
			minDifficulty = 35,
			maxDifficulty = 39,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_quenker_ravager_lair_neutral_medium",
			minDifficulty = 36,
			maxDifficulty = 40,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_dantari_chief_squad_neutral_none",
			minDifficulty = 40,
			maxDifficulty = 44,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_mokk_hunter_neutral_none",
			minDifficulty = 40,
			maxDifficulty = 44,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_mokk_soothsayer_camp_neutral_medium_theater",
			minDifficulty = 41,
			maxDifficulty = 45,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_bol_seething_lair_neutral_medium",
			minDifficulty = 26,
			maxDifficulty = 30,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_quenker_relic_reaper_lair_neutral_medium",
			minDifficulty = 43,
			maxDifficulty = 47,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_voritor_tracker_lair_neutral_medium",
			minDifficulty = 43,
			maxDifficulty = 47,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_voritor_jungle_lair_neutral_medium_boss_01",
			minDifficulty = 51,
			maxDifficulty = 55,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_voritor_hunter_lair_neutral_medium",
			minDifficulty = 55,
			maxDifficulty = 59,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_thune_lair_neutral_medium",
			minDifficulty = 45,
			maxDifficulty = 75,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_bol_lair_neutral_medium_boss_01",
			minDifficulty = 50,
			maxDifficulty = 80,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_piket_longhorn_lair_neutral_medium",
			minDifficulty = 55,
			maxDifficulty = 83,
			size = 25,
		},
		{
			lairTemplateName = "dantooine_graul_lair_neutral_medium",
			minDifficulty = 60,
			maxDifficulty = 85,
			size = 25,
		},
	}
}

addDestroyMissionGroup("dantooine_destroy_missions", dantooine_destroy_missions);
