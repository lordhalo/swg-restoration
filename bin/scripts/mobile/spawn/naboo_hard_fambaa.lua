naboo_hard_fambaa = {

	lairSpawns = {
		{
			lairTemplateName = "naboo_peko_peko_giant_pair_neutral_none",
			spawnLimit = -1,
			minDifficulty = 23,
			maxDifficulty = 27,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_fambaa_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 34,
			maxDifficulty = 38,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "naboo_fambaa_lair_neutral_large_boss_01",
			spawnLimit = -1,
			minDifficulty = 34,
			maxDifficulty = 38,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "naboo_fambaa_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 34,
			maxDifficulty = 38,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "naboo_rogue_fambaa_neutral_none",
			spawnLimit = -1,
			minDifficulty = 37,
			maxDifficulty = 41,
			numberToSpawn = 0,
			weighting = 10,
			size = 30
		},
		{
			lairTemplateName = "naboo_peko_peko_albatross_neutral_none",
			spawnLimit = 2,
			minDifficulty = 155,
			maxDifficulty = 159,
			numberToSpawn = 0,
			weighting = 3,
			size = 35
		},
	}
}

addSpawnGroup("naboo_hard_fambaa", naboo_hard_fambaa);
