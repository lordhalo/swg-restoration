endor_gorax = {

	lairSpawns = {
		{
			lairTemplateName = "endor_marauder_wasteland_neutral_none",
			spawnLimit = -1,
			minDifficulty = 55,
			maxDifficulty = 59,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "endor_merek_lair_neutral_medium_04",
			spawnLimit = -1,
			minDifficulty = 55,
			maxDifficulty = 59,
			numberToSpawn = 0,
			weighting = 3,
			size = 25
		},
		{
			lairTemplateName = "endor_merek_lair_neutral_boss_04",
			spawnLimit = -1,
			minDifficulty = 55,
			maxDifficulty = 59,
			numberToSpawn = 0,
			weighting = 3,
			size = 25
		},
		{
			lairTemplateName = "endor_merek_blood_king_neutral_small",
			spawnLimit = -1,
			minDifficulty = 56,
			maxDifficulty = 60,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "endor_bark_mite_burrower_queen_lair_neutral_small",
			spawnLimit = -1,
			minDifficulty = 60,
			maxDifficulty = 64,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},
		{
			lairTemplateName = "endor_arachne_queen_lair_neutral_small",
			spawnLimit = -1,
			minDifficulty = 63,
			maxDifficulty = 67,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "endor_gorax_neutral_none",
			spawnLimit = 10,
			minDifficulty = 131,
			maxDifficulty = 300,
			numberToSpawn = 0,
			weighting = 5,
			size = 35
		},
	}
}

addSpawnGroup("endor_gorax", endor_gorax);
