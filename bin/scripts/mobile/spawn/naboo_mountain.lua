naboo_mountain = {

	lairSpawns = {

		{
			lairTemplateName = "naboo_philosopher_camp_mountain_neutral_large_theater",
			spawnLimit = -1,
			minDifficulty = 4,
			maxDifficulty = 8,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_mountain_krevol_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 15,
			maxDifficulty = 19,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},

		{
			lairTemplateName = "naboo_mountain_krevol_click_neutral_none",
			spawnLimit = -1,
			minDifficulty = 15,
			maxDifficulty = 19,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},

		{
			lairTemplateName = "naboo_capper_spineflap_hive_neutral_large",
			spawnLimit = -1,
			minDifficulty = 15,
			maxDifficulty = 19,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},

		{
			lairTemplateName = "naboo_mountain_outpost_rebel_large_theater",
			spawnLimit = -1,
			minDifficulty = 15,
			maxDifficulty = 24,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_mountain_ikopi_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 17,
			maxDifficulty = 21,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "naboo_mountain_ikopi_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 17,
			maxDifficulty = 21,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
	}
}

addSpawnGroup("naboo_mountain", naboo_mountain);
