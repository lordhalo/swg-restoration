dathomir_rancor = {

	lairSpawns = {
		{
			lairTemplateName = "dathomir_rancor_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 50,
			maxDifficulty = 75,
			numberToSpawn = 5,
			weighting = 30,
			size = 30
		},
		{
			lairTemplateName = "dathomir_rancor_enraged_bull_lair_neutral_large_boss_01",
			spawnLimit = -1,
			minDifficulty = 150,
			maxDifficulty = 154,
			numberToSpawn = 5,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "dathomir_rancor_ancient_bull_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 150,
			maxDifficulty = 154,
			numberToSpawn = 3,
			weighting = 10,
			size = 30
		}
	}
}

addSpawnGroup("dathomir_rancor", dathomir_rancor);
