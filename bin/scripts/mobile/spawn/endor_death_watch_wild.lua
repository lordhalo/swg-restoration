endor_death_watch_wild = {

	lairSpawns = {
		{
			lairTemplateName = "endor_death_watch",
			spawnLimit = -1,
			minDifficulty = 55,
			maxDifficulty = 59,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
	}
}

addSpawnGroup("endor_death_watch_wild", endor_death_watch_wild);
