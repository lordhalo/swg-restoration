dantooine_graul = {

	lairSpawns = {
		{
			lairTemplateName = "dantooine_graul_lair_neutral_medium",
			spawnLimit = 10,
			minDifficulty = 30,
			maxDifficulty = 40,
			numberToSpawn = 0,
			weighting = 35,
			size = 35
		},

		{
			lairTemplateName = "dantooine_graul_frenzied_lair_neutral_large",
			spawnLimit = 10,
			minDifficulty = 56,
			maxDifficulty = 70,
			numberToSpawn = 0,
			weighting = 32,
			size = 35
		},

		{
			lairTemplateName = "dantooine_graul_mauler_lair_neutral_large",
			spawnLimit = 10,
			minDifficulty = 35,
			maxDifficulty = 55,
			numberToSpawn = 0,
			weighting = 33,
			size = 35
		},
	}
}

addSpawnGroup("dantooine_graul", dantooine_graul);
