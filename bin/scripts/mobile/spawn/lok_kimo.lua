lok_kimo = {

	lairSpawns = {
		{
			lairTemplateName = "lok_kimogilla_pack3_neutral_none",
			spawnLimit = -1,
			minDifficulty = 95,
			maxDifficulty = 99,
			numberToSpawn = 0,
			weighting = 5,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_pack2_neutral_none",
			spawnLimit = -1,
			minDifficulty = 95,
			maxDifficulty = 139,
			numberToSpawn = 0,
			weighting = 3,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_pack_neutral_none",
			spawnLimit = -1,
			minDifficulty = 95,
			maxDifficulty = 139,
			numberToSpawn = 0,
			weighting = 3,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_lair2_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 95,
			maxDifficulty = 139,
			numberToSpawn = 0,
			weighting = 3,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 95,
			maxDifficulty = 139,
			numberToSpawn = 0,
			weighting = 3,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_dune_neutral_none",
			spawnLimit = -1,
			minDifficulty = 123,
			maxDifficulty = 127,
			numberToSpawn = 0,
			weighting = 4,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_dune_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 123,
			maxDifficulty = 127,
			numberToSpawn = 0,
			weighting = 4,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_enraged_dune_neutral_none",
			spawnLimit = -1,
			minDifficulty = 123,
			maxDifficulty = 152,
			numberToSpawn = 0,
			weighting = 3,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_giant_dune_neutral_none",
			spawnLimit = -1,
			minDifficulty = 123,
			maxDifficulty = 157,
			numberToSpawn = 0,
			weighting = 2,
			size = 30
		},
		{
			lairTemplateName = "lok_kimogilla_giant_dune_lair_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 123,
			maxDifficulty = 157,
			numberToSpawn = 0,
			weighting = 2,
			size = 30
		},
	}
}

addSpawnGroup("lok_kimo", lok_kimo);
