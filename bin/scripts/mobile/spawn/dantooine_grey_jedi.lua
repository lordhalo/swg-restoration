dantooine_grey_jedi = {

	lairSpawns = {
		{
			lairTemplateName = "dantooine_grey_jedi_theater",
			spawnLimit = -1,
			minDifficulty = 120,
			maxDifficulty = 160,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "dantooine_grey_jedi_theater",
			spawnLimit = 3,
			minDifficulty = 160,
			maxDifficulty = 200,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "dantooine_force_crystal_hunter_neutral_none",
			spawnLimit = -1,
			minDifficulty = 80,
			maxDifficulty = 120,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "dantooine_force_renegade_neutral_none",
			spawnLimit = -1,
			minDifficulty = 80,
			maxDifficulty = 120,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "dantooine_dark_jedi_theater",
			spawnLimit = 3,
			minDifficulty = 160,
			maxDifficulty = 200,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "dantooine_dark_jedi_theater",
			spawnLimit = -1,
			minDifficulty = 120,
			maxDifficulty = 160,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("dantooine_grey_jedi", dantooine_grey_jedi);
