yavin4_force_mobs = {

	lairSpawns = {
		{
			lairTemplateName = "yavin4_renegades_theater",
			spawnLimit = -1,
			minDifficulty = 90,
			maxDifficulty = 105,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "yavin4_droids_theater",
			spawnLimit = -1,
			minDifficulty = 70,
			maxDifficulty = 80,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("yavin4_force_mobs", yavin4_force_mobs);
