yavin4_geocave = {

	lairSpawns = {
		{
			lairTemplateName = "yavin4_mercenary",
			spawnLimit = -1,
			minDifficulty = 106,
			maxDifficulty = 152,
			numberToSpawn = 0,
			weighting = 25,
			size = 35
		},
		{
			lairTemplateName = "yavin4_geonosians",
			spawnLimit = -1,
			minDifficulty = 75,
			maxDifficulty = 122,
			numberToSpawn = 0,
			weighting = 15,
			size = 35
		},
		{
			lairTemplateName = "yavin4_firespider",
			spawnLimit = 3,
			minDifficulty = 175,
			maxDifficulty = 222,
			numberToSpawn = 3,
			weighting = 8,
			size = 35
		},
		{
			lairTemplateName = "yavin4_acklay",
			spawnLimit = 2,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 2,
			weighting = 4,
			size = 25
		},
	}
}

addSpawnGroup("yavin4_geocave", yavin4_geocave);
