naboo_plains = {

	lairSpawns = {

		{
			lairTemplateName = "naboo_plains_tusk_cat_pride_neutral_medium",
			spawnLimit = -1,
			minDifficulty = 16,
			maxDifficulty = 20,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},
		{
			lairTemplateName = "naboo_plains_tusk_cat_hunt_neutral_none",
			spawnLimit = -1,
			minDifficulty = 16,
			maxDifficulty = 20,
			numberToSpawn = 0,
			weighting = 15,
			size = 25
		},

		{
			lairTemplateName = "naboo_plains_tusk_cat_stalker_neutral_none",
			spawnLimit = 5,
			minDifficulty = 31,
			maxDifficulty = 35,
			numberToSpawn = 0,
			weighting = 3,
			size = 25
		},

		{
			lairTemplateName = "naboo_falumpaset_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 19,
			maxDifficulty = 23,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "naboo_falumpaset_herd_neutral_large",
			spawnLimit = -1,
			minDifficulty = 19,
			maxDifficulty = 23,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},

		{
			lairTemplateName = "naboo_rogue_falumpaset_neutral_none",
			spawnLimit = -1,
			minDifficulty = 18,
			maxDifficulty = 22,
			numberToSpawn = 0,
			weighting = 10,
			size = 25
		},

		{
			lairTemplateName = "naboo_fanned_rawl_hunter_neutral_none",
			spawnLimit = -1,
			minDifficulty = 19,
			maxDifficulty = 23,
			numberToSpawn = 0,
			weighting = 15,
			size = 20
		},

		{
			lairTemplateName = "naboo_bolle_bol_lair_neutral_large",
			spawnLimit = -1,
			minDifficulty = 27,
			maxDifficulty = 31,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "naboo_bolle_bol_lair_neutral_large_boss_01",
			spawnLimit = -1,
			minDifficulty = 27,
			maxDifficulty = 31,
			numberToSpawn = 0,
			weighting = 8,
			size = 30
		},
		{
			lairTemplateName = "naboo_bolle_bol_herd_neutral_none",
			spawnLimit = -1,
			minDifficulty = 27,
			maxDifficulty = 31,
			numberToSpawn = 0,
			weighting = 15,
			size = 30
		},
		{
			lairTemplateName = "naboo_fanned_rawl_lair_neutral_small_boss_01",
			spawnLimit = -1,
			minDifficulty = 24,
			maxDifficulty = 28,
			numberToSpawn = 0,
			weighting = 8,
			size = 20
		},
		{
			lairTemplateName = "naboo_ikopi_stag_neutral_none",
			spawnLimit = 10,
			minDifficulty = 30,
			maxDifficulty = 34,
			numberToSpawn = 0,
			weighting = 10,
			size = 25
		},

	}
}

addSpawnGroup("naboo_plains", naboo_plains);
