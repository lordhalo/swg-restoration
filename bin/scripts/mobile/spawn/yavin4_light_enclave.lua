yavin4_light_enclave = {

	lairSpawns = {
		{
			lairTemplateName = "yavin4_trainer_droids",
			spawnLimit = -1,
			minDifficulty = 10,
			maxDifficulty = 14,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},

		{
			lairTemplateName = "yavin4_trainer_droids",
			spawnLimit = -1,
			minDifficulty = 12,
			maxDifficulty = 16,
			numberToSpawn = 0,
			weighting = 50,
			size = 35
		},
	}
}

addSpawnGroup("yavin4_light_enclave", yavin4_light_enclave);
