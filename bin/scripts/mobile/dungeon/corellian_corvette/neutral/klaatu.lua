klaatu = Creature:new {
	objectName = "@mob/creature_names:klaatu",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/klaatu.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteKlaatuConvoTemplate",
	outfit = "klaatu_outfit",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(klaatu, "klaatu")
