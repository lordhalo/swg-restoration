corsec_battle_droid = Creature:new {
  objectName = "@mob/creature_names:rebel_battle_droid",
  socialGroup = "corsec",
  faction = "corsec",
  npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
  creatureBitmask = PACK + KILLER,
  optionsBitmask = AIENABLED,
  diet = HERBIVORE,
  scale = 1.15,

  templates = {
    "object/mobile/battle_droid.iff"
  },
  lootGroups = {
  },
  weapons = {"battle_droid_weapons"},
  attacks = merge(pistoleermaster,carbineermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(corsec_battle_droid, "corsec_battle_droid")
