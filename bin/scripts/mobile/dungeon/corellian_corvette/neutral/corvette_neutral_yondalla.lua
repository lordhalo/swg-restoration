corvette_neutral_yondalla = Creature:new {
	objectName = "",
	customName = "Yondalla",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corvette_neutral_yondalla.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteYondallaConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(corvette_neutral_yondalla, "corvette_neutral_yondalla")
