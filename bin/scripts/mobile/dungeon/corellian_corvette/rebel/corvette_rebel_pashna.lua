corvette_rebel_pashna = Creature:new {
	objectName = "@npc_name:moncal_base_male",
	customName = "Pashna Starkiller",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corvette_rebel_pashna.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvettePashnaConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(corvette_rebel_pashna, "corvette_rebel_pashna")
