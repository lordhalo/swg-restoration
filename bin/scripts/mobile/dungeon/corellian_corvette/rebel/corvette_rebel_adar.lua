corvette_rebel_adar = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Adar Tallon",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corvette_rebel_adar.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteTallonConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(corvette_rebel_adar, "corvette_rebel_adar")
