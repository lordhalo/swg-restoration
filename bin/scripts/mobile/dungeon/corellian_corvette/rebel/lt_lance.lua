lt_lance = Creature:new {
	objectName = "",
	customName = "Lt. Lance",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_lieutenant_lance.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteLtLanceConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(lt_lance, "lt_lance")
