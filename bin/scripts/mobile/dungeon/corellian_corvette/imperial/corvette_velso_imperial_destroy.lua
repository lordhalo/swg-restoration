corvette_velso_imperial_destroy = Creature:new {
	objectName = "",
	customName = "Lt. Velso",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corvette_imperial_velso.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteVelsoConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(corvette_velso_imperial_destroy, "corvette_velso_imperial_destroy")
