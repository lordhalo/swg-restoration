corvette_imperial_prisoner_02 = Creature:new {
	objectName = "@mob/creature_names:prisoner",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "prisoner",
	faction = "",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_dathomir_prisoner_05.iff",
		"object/mobile/dressed_dathomir_prisoner_04.iff",
		"object/mobile/dressed_dathomir_prisoner_03.iff",
		"object/mobile/dressed_dathomir_prisoner_02.iff",
		"object/mobile/dressed_dathomir_prisoner_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteImperialPrisonerConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(corvette_imperial_prisoner_02, "corvette_imperial_prisoner_02")
