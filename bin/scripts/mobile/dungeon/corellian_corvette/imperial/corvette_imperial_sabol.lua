corvette_imperial_sabol = Creature:new {
	objectName = "",
	customName = "Lt. Sabol",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING + INVULNERABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corvette_imperial_sabol.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "corvetteSabolConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(corvette_imperial_sabol, "corvette_imperial_sabol")
