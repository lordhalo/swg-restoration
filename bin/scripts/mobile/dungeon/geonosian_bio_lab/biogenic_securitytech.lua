biogenic_securitytech = Creature:new {
	objectName = "@mob/creature_names:geonosian_scientist",
	socialGroup = "self",
	faction = "",
	npcStats = {60, 147, 6025, 90, 369, 147, 0, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_geonosian_scientist_01.iff",
		"object/mobile/dressed_geonosian_scientist_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicSecurityTechConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(biogenic_securitytech, "biogenic_securitytech")
