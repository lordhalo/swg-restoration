biogenic_crazyguy = Creature:new {
	objectName = "",
	customName = "Ebotia Dived (a scientist)",
	socialGroup = "self",
	faction = "",
	npcStats = {64, 155, 6267, 94, 380, 155, 0, 4336},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_doctor_trainer_human_female_01.iff",
		"object/mobile/dressed_combatmedic_trainer_human_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicCrazyGuyConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(biogenic_crazyguy, "biogenic_crazyguy")
