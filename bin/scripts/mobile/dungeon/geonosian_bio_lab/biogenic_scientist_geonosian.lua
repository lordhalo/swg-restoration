biogenic_scientist_geonosian = Creature:new {
	objectName = "@mob/creature_names:geonosian_scientist",
	socialGroup = "self",
	faction = "",
	npcStats = {66, 160, 9591, 119, 481, 239, 4348, 4414},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_geonosian_scientist_01.iff",
		"object/mobile/dressed_geonosian_scientist_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicScientistGeonosianConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(biogenic_scientist_geonosian, "biogenic_scientist_geonosian")
