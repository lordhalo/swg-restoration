herald_biogenic_harmon = Creature:new {
	objectName = "",
	customName = "Lt. Harmon Vintollo",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_biogenic_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicHerald2ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_biogenic_harmon, "herald_biogenic_harmon")
