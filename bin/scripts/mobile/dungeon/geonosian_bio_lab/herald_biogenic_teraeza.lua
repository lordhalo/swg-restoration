herald_biogenic_teraeza = Creature:new {
	objectName = "",
	customName = "Teraeza Furloti",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_biogenic_03.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicHerald3ConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_biogenic_teraeza, "herald_biogenic_teraeza")
