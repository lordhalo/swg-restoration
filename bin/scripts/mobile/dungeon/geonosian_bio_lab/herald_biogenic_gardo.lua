herald_biogenic_gardo = Creature:new {
	objectName = "",
	customName = "Gardo Valdell",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_herald_biogenic_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicHeraldDealerConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(herald_biogenic_gardo, "herald_biogenic_gardo")
