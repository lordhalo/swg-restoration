biogenic_assistant = Creature:new {
	objectName = "@mob/creature_names:geonosian_scientist",
	socialGroup = "self",
	faction = "",
	npcStats = {64, 155, 6267, 94, 380, 155, 0, 4336},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_geonosian_scientist_01.iff","object/mobile/dressed_geonosian_scientist_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "biogenicAssistantConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(biogenic_assistant, "biogenic_assistant")
