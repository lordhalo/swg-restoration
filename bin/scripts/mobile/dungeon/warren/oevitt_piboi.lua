oevitt_piboi = Creature:new {
	objectName = "",
	customName = "O'Evitt Piboi (en escapee)",
	socialGroup = "warren_imperial_worker",
	faction = "",
	npcStats = {55, 131, 8630, 108, 445, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/warren_escapee.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "OevittPiboiConversationTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(oevitt_piboi, "oevitt_piboi")
