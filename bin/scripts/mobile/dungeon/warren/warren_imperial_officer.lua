warren_imperial_officer = Creature:new {
	objectName = "@mob/creature_names:warren_imperial_officer",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "warren_imperial",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + AGGRESSIVE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/warren_imperial_officer_s01.iff",
		"object/mobile/warren_imperial_officer_s02.iff",
		"object/mobile/warren_imperial_officer_s03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 1000000},
				{group = "junk", chance = 6000000},
				{group = "clothing_attachments", chance = 1000000},
				{group = "armor_attachments", chance = 1000000},
				{group = "imperial_officer_common", chance = 500000},
				{group = "finely_toolset", chance = 500000}
			}
		}
	},
	weapons = {"imperial_weapons_medium"},
	conversationTemplate = "",
	attacks = merge(brawlermaster,marksmanmaster,carbineermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(warren_imperial_officer, "warren_imperial_officer")
