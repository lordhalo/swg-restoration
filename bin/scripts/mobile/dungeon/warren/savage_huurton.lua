savage_huurton = Creature:new {
	objectName = "@mob/creature_names:warren_huurton_savage",
	socialGroup = "warren_huurton",
	faction = "",
	npcStats = {55, 131, 8630, 108, 445, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "hide_wooly",
	hideAmount = 35,
	boneType = "bone_mammal",
	boneAmount = 35,
	milkType = "milk_wild",
	milk = 35,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/huurton_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(savage_huurton, "savage_huurton")
