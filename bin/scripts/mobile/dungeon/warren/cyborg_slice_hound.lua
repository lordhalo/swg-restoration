cyborg_slice_hound = Creature:new {
	objectName = "@mob/creature_names:warren_cyborg_slicehound",
	socialGroup = "warren_cyborg",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "meat_wild",
	meatAmount = 70,
	hideType = "hide_leathery",
	hideAmount = 40,
	boneType = "bone_mammal",
	boneAmount = 35,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,
	scale = 2.0,

	templates = {"object/mobile/warren_cyborg_slicehound.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(cyborg_slice_hound, "cyborg_slice_hound")
