crazed_scientist = Creature:new {
	objectName = "@mob/creature_names:warren_irradiated_worker",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "warren_imperial_worker",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/warren_irradiated_worker_s01.iff",
		"object/mobile/warren_irradiated_worker_s02.iff",
		"object/mobile/warren_irradiated_worker_s03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4000000},
				{group = "wearables_common", chance = 3000000},
				{group = "loot_kit_parts", chance = 2000000},
				{group = "tailor_components", chance = 1000000},
			}
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = brawlernovice
}

CreatureTemplates:addCreatureTemplate(crazed_scientist, "crazed_scientist")
