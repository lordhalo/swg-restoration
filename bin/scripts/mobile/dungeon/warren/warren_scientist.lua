warren_scientist = Creature:new {
	objectName = "@mob/creature_names:warren_scientist",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "warren_scientist",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + AGGRESSIVE + ENEMY,
	creatureBitmask = PACK,
	diet = HERBIVORE,

	templates = {
		"object/mobile/warren_scientist_s01.iff",
		"object/mobile/warren_scientist_s02.iff",
		"object/mobile/warren_scientist_s03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 5500000},
				{group = "tailor_components", chance = 1000000},
				{group = "armor_attachments", chance = 500000},
				{group = "clothing_attachments", chance = 500000},
				{group = "finely_toolset", chance = 2500000}
			}
		}
	},
	weapons = {},
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(warren_scientist, "warren_scientist")
