manx_try = Creature:new {
	objectName = "@theme_park/warren/warren_system_messages:name_manx",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/warren_research_scientist.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ManxTryConversationTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(manx_try, "manx_try")
