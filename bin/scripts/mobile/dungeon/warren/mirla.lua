mirla = Creature:new {
	objectName = "@theme_park/warren/warren_system_messages:name_mirla",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 14248, 258, 712, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/warren_mirla.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "MirlaConversationTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mirla, "mirla")
