warren_agro_droid = Creature:new {
	objectName = "@mob/creature_names:warren_agro_droid_s01",
	socialGroup = "warren_imperial",
	faction = "",
	npcStats = {55, 131, 14248, 258, 712, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + AGGRESSIVE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {	"object/mobile/warren_agro_droid_s01.iff", --droideka
					"object/mobile/warren_agro_droid_s02.iff", --probot
					"object/mobile/warren_agro_droid_s03.iff", --ig assassin
					"object/mobile/warren_agro_droid_s04.iff"  --dz70 fugitive tracker droid
				},
	lootGroups = {},
	conversationTemplate = "",
	defaultAttack = "attack",
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff"
}

CreatureTemplates:addCreatureTemplate(warren_agro_droid, "warren_agro_droid")
