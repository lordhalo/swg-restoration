fierce_huurton = Creature:new {
	objectName = "@mob/creature_names:warren_huurton_fierce",
	socialGroup = "warren_huurton",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "hide_wooly",
	hideAmount = 65,
	boneType = "bone_mammal",
	boneAmount = 25,
	milkType = "milk_wild",
	milk = 25,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/huurton_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(fierce_huurton, "fierce_huurton")
