captain_heff = Creature:new {
	objectName = "@theme_park/warren/warren_system_messages:name_heff",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_captain_m.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "CaptainHeffConversationTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(captain_heff, "captain_heff")
