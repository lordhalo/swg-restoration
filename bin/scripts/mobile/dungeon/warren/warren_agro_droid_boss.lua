warren_agro_droid_boss = Creature:new {
	objectName = "@mob/creature_names:warren_agro_droid_boss",
	socialGroup = "warren_imperial",
	faction = "",
	npcStats = {55, 131, 14248, 258, 712, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + AGGRESSIVE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/warren_agro_droid_boss.iff"},
	lootGroups = {},
	conversationTemplate = "",
	defaultAttack = "attack",
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff"
}

CreatureTemplates:addCreatureTemplate(warren_agro_droid_boss, "warren_agro_droid_boss")
