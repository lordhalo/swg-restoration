rell_bunker = Creature:new {
	objectName = "",
	customName = "Rell",
	socialGroup = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_black_sun_thug.iff"},
	lootGroups = {},
	weapons = {"melee_weapons"},
	conversationTemplate = "",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(rell_bunker, "rell_bunker")
