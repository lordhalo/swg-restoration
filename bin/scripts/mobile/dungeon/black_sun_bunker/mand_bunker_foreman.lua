mand_bunker_foreman = Creature:new {
	objectName = "",
	customName = "Japer Witter (a mine Foreman)",
	socialGroup = "death_watch",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mand_bunker_foreman.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "deathWatchForemanConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mand_bunker_foreman, "mand_bunker_foreman")
