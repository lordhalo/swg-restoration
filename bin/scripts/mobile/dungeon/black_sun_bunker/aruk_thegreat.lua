aruk_thegreat = Creature:new {
	objectName = "",
	customName = "Aruk The Great",
	socialGroup = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_black_sun_assassin.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "arukTheGreatIntro",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(aruk_thegreat, "aruk_thegreat")
