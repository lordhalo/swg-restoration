mand_bunker_vent_droid = Creature:new {
	objectName = "@mob/creature_names:r2",
	socialGroup = "",
	faction = "",
	npcStats = {20, 67, 2923, 50, 250, 67, 0, 1310},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE ,
	creatureBitmask = NONE,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = { "object/mobile/r2.iff" },
	lootGroups = {},
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(mand_bunker_vent_droid, "mand_bunker_vent_droid")
