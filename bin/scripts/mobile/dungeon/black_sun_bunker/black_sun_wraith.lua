black_sun_wraith = Creature:new {
	objectName = "@mob/creature_names:mand_bunker_blksun_assassin",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "black_sun",
	faction = "black_sun",
	npcStats = {79, 177, 10929, 134, 525, 283, 5478, 5063},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {
		{
			groups = {
				{group = "death_watch_bunker_commoners",   chance = 6000000},
				{group = "death_watch_bunker_lieutenants", chance = 3500000},
				{group = "death_watch_bunker_ingredient_protective",  chance = 200000},
				{group = "death_watch_bunker_ingredient_binary",  chance = 200000},
				{group = "jetpack_parts",  chance = 100000}
			},
			lootChance = 1800000
		},

		{
	        groups = {
				{group = "clothing_attachments", chance = 2500000},
				{group = "armor_attachments", chance = 2500000},
				{group = "s01_goggles", chance = 2500000},
				{group = "s01_token", chance = 2500000}
			},
			lootChance = 2000000
		}
	},
	weapons = {"pirate_weapons_heavy"},
	outfit = "reb_mando",
	conversationTemplate = "",
	attacks = merge(bountyhuntermaster,marksmanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(black_sun_wraith, "black_sun_wraith")
