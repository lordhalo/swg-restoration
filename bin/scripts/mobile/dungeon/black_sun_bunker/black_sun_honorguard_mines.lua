black_sun_honorguard_mines = Creature:new {
	objectName = "",
	customName = "Duthral L'nau (a Black Sun Honor Guard)",
	socialGroup = "black_sun",
	faction = "imperial",
	npcStats = {82, 189, 18458, 330, 856, 338, 5739, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE ,
	creatureBitmask = KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_death_watch_gold.iff"},
	lootGroups = {
		{ -- 100 % Drop
			groups = {
				{group = "death_watch_bunker_overlord_shared", chance =  10000000}
			},
			lootChance = 10000000
		},

		{ -- Random Filler
	        	groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 7500000
		},

		{ -- Random Filler
	        	groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 7500000
		},

		{ -- Extra Rare Resources
			groups = {
				{group = "death_watch_bunker_ingredient_protective",  chance = 10000000},
			},
			lootChance = 700000
		},

		{  -- Extra Rare Resources
			groups = {
				{group = "death_watch_bunker_ingredient_binary",  chance = 10000000},
			},
			lootChance = 700000
		},

		{ -- Deco
			groups = {
				{group = "dw_anniversary_painting_02",  chance = 10000000},
			},
			lootChance = 3700000
		},

		{ -- Rare Bonus
			groups = {
				{group = "death_watch_bunker_jetpacks",  chance = 10000000},
			},
			lootChance = 1500000
		},
	},
	weapons = {"dark_trooper_weapons"},
	conversationTemplate = "",
	attacks = merge(riflemanmaster,fencermaster,marksmanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(black_sun_honorguard_mines, "black_sun_honorguard_mines")
