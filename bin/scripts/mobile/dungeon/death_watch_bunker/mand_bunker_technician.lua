mand_bunker_technician = Creature:new {
	objectName = "",
	customName = "Labsoll Renuffi (a technican)",
	socialGroup = "death_watch",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mand_bunker_technician.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "deathWatchTechnicianConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mand_bunker_technician, "mand_bunker_technician")
