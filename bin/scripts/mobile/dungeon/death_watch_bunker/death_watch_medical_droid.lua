death_watch_medical_droid = Creature:new {
	objectName = "",
	customName = "G12-4J (a medical droid)",
	socialGroup = "",
	faction = "",
	npcStats = {34, 86, 4070, 66, 298, 86, 0, 2403},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/21b_surgical_droid.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "deathWatchMedicalDroidConvoTemplate",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(death_watch_medical_droid, "death_watch_medical_droid")
