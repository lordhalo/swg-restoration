death_watch_ghost_new = Creature:new {
	objectName = "",
	customName = "Death Watch Ghost",
	socialGroup = "death_watch",
	faction = "rebel",
	pvpFaction = "rebel",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	weapons = {"nyaxs_weapons"},
	outfit = "reb_mando",
	conversationTemplate = "",
	attacks = merge(bountyhuntermaster,marksmanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(death_watch_ghost_new, "death_watch_ghost_new")
