tungo_li = Creature:new {
	objectName = "",
	customName = "Tungo Li",
	socialGroup = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_black_sun_henchman.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "tungoIntro",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(tungo_li, "tungo_li")
