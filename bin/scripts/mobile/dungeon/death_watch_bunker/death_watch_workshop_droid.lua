death_watch_workshop_droid = Creature:new {
	objectName = "@droid_name:wed_treadwell_base",
	customName = "WED15-I643 (a workshop droid)",
	socialGroup = "",
	faction = "",
	npcStats = {34, 86, 4070, 66, 298, 86, 0, 2403},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/wed_treadwell.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "deathWatchWorkshopDroidConvoTemplate",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(death_watch_workshop_droid, "death_watch_workshop_droid")
