death_watch_bloodguard = Creature:new {
	objectName = "@mob/creature_names:mand_bunker_dthwatch_red",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "death_watch",
	faction = "death_watch",
	npcStats = {78, 175, 10821, 133, 523, 279, 5391, 5000},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_death_watch_red.iff"},
	lootGroups = {
		{
			groups = {
				{group = "death_watch_bunker_commoners", chance = 6000000},
				{group = "death_watch_bunker_lieutenants", chance = 3800000},
				{group = "death_watch_bunker_ingredient_protective",  chance = 100000},
				{group = "jetpack_parts",  chance = 300000}
			},
			lootChance = 1000000
		},

		{
	        groups = {
				{group = "clothing_attachments", chance = 2500000},
				{group = "armor_attachments", chance = 2500000},
				{group = "s01_goggles", chance = 2500000},
				{group = "s01_token", chance = 2500000}
			},
			lootChance = 1500000
		}
	},
	weapons = {"death_watch_bloodguard_weapons"},
	conversationTemplate = "",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(death_watch_bloodguard, "death_watch_bloodguard")
