death_watch_herald_rebel = Creature:new {
	objectName = "",
	customName = "Lutin Nightstalker",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {76, 172, 10605, 130, 515, 271, 5217, 4878},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rebel_general_moncal_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lutinNightstalkerConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(death_watch_herald_rebel, "death_watch_herald_rebel")
