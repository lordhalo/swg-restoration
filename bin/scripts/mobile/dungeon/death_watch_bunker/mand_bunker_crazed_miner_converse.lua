mand_bunker_crazed_miner_converse = Creature:new {
	objectName = "",
	customName = "Haldo",
	socialGroup = "death_watch",
	faction = "",
	npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mand_bunker_crazed_miner.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "deathWatchDaily",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(mand_bunker_crazed_miner_converse, "mand_bunker_crazed_miner_converse")
