myyydril_herder = Creature:new {
	objectName = "@mob/creature_names:myyydril_herder",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "myyydril",
	faction = "myyydril",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_myyydril_herder_m_01.iff",
		"object/mobile/dressed_myyydril_herder_m_02.iff",
		"object/mobile/dressed_myyydril_herder_m_03.iff"
	},
	lootGroups = {},
	weapons = {"melee_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/fancy",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(myyydril_herder, "myyydril_herder")
