tutorial_room2_greeter = Creature:new {
	objectName = "@newbie_tutorial/system_messages:imp_name",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_imperial_officer_f.iff",
		"object/mobile/dressed_imperial_officer_m.iff",
		"object/mobile/dressed_imperial_officer_m_2.iff",
		"object/mobile/dressed_imperial_officer_m_3.iff",
		"object/mobile/dressed_imperial_officer_m_4.iff",
		"object/mobile/dressed_imperial_officer_m_5.iff",
		"object/mobile/dressed_imperial_officer_m_6.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "tutorialRoomTwoGreeterConvoTemplate",
	attacks = merge(marksmannovice,brawlernovice),
}

CreatureTemplates:addCreatureTemplate(tutorial_room2_greeter, "tutorial_room2_greeter")
