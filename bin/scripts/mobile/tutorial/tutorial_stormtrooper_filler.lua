tutorial_stormtrooper_filler = Creature:new {
	objectName = "@mob/creature_names:stormtrooper",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_stormtrooper_m.iff"},
	lootGroups = {},
	weapons = {"stormtrooper_weapons"},
	attacks = {},
	conversationTemplate = "stormtrooperFillerConvoTemplate",
}

CreatureTemplates:addCreatureTemplate(tutorial_stormtrooper_filler, "tutorial_stormtrooper_filler")
