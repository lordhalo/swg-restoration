tutorial_clone_droid = Creature:new {
	objectName = "@newbie_tutorial/system_messages:droid_name",
	socialGroup = "imperial",
	faction = "",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid_silver.iff"},
	lootGroups = {},
	weapons = {},
	attacks = merge(marksmannovice,brawlernovice),
	conversationTemplate = "tutorialCloneDroidConvoTemplate",
}

CreatureTemplates:addCreatureTemplate(tutorial_clone_droid, "tutorial_clone_droid")
