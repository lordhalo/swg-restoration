tutorial_training_room_officer = Creature:new {
	objectName = "@newbie_tutorial/system_messages:imp_name",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_major_m.iff"},
	lootGroups = {},
	weapons = {"imperial_weapons_medium"},
	attacks = merge(marksmannovice,brawlernovice),
	conversationTemplate = "tutorialTrainingRoomOfficerConvoTemplate",
}

CreatureTemplates:addCreatureTemplate(tutorial_training_room_officer, "tutorial_training_room_officer")
