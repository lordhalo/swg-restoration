tutorial_quartermaster = Creature:new {
	objectName = "@newbie_tutorial/system_messages:quarter_name",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_major_m.iff"},
	lootGroups = {},
	weapons = {},
	attacks = merge(marksmannovice,brawlernovice),
	conversationTemplate = "tutorialQuartermasterConvoTemplate",
}

CreatureTemplates:addCreatureTemplate(tutorial_quartermaster, "tutorial_quartermaster")
