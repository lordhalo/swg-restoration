initiate_quest = Creature:new {
	objectName = "",
	customName = "Ancient droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 45,
	chanceHit = 1,
	damageMin = 65,
	damageMax = 100,
	baseXp = 9429,
	baseHAM = 2000,
	baseHAMmax = 2000,
	armor = 0,
	resists = {0,0,0,0,0,0,0,0,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/som/hk77.iff"},
	lootGroups = {},

	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(initiate_quest, "initiate_quest")
