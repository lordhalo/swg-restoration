mutant_acklay = Creature:new {
	objectName = "",
	customName = "Mutant Acklay",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 2000,
	chanceHit = 0.27,
	damageMin = 70,
	damageMax = 75,
	baseXp = 0,
	baseHAM = 420000,
	baseHAMmax = 420000,
	armor = 0,
	resists = {70,70,10,10,60,90,70,80,80},
	meatType = "meat_insect",
	meatAmount = 15,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/mutant_acklay.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mutant_acklay, "mutant_acklay")
