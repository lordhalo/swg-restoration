ancient_spider_guard = Creature:new {
	customName = "Spider Consort",
	socialGroup = "spider",
	faction = "",
	level = 150,
	chanceHit = 25.0,
	damageMin = 330,
	damageMax = 340,
	baseXp = 430,
	baseHAM = 2500000,
	baseHAMmax = 2500000,
	armor = 0,
	resists = { 30, 30, 30, 30, 0, 40, 10, 10, 10 },
	meatType = "meat_insect",
	meatAmount = 7,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + STALKER,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/nightspider_aggressor.iff"},
	scale = 1.5,
	lootGroups = {},
	weapons = {"creature_spit_small_toxicgreen"},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""},
		{"mildpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(ancient_spider_guard, "ancient_spider_guard")
