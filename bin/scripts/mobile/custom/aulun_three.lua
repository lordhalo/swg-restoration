aulun_three = Creature:new {
	objectName = "",
	customName = "Jedi Knight Aulun",
	socialGroup = "townsperson",
	pvpFaction = "townsperson",
	faction = "townsperson",
	level = 100,
	chanceHit = 1,
	damageMin = 645,
	damageMax = 1000,
	baseXp = 9429,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 0,
	resists = {0,0,0,0,0,0,0,0,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	weapons = {"luke_skywalker_weapons"},
	outfit = "jedi_padawan_s02",
	conversationTemplate = "force_sensitive_aulunthree_conv",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(aulun_three, "aulun_three")
