dark_quest = Creature:new {
	objectName = "",
	customName = "a Foolish Jedi Initiate",
	socialGroup = "",
	faction = "",
	level = 15,
	chanceHit = 0.36,
	damageMin = 45,
	damageMax = 85,
	baseXp = 1,
	baseHAM = 600,
	baseHAMmax = 800,
	armor = 0,
	resists = {15,15,15,15,15,15,15,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {
	},
	outfit = "jedi_padawan_s02",
	weapons = {"melee_weapons"},
	conversationTemplate = "",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(dark_quest, "dark_quest")
