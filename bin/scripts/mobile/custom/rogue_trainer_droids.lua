rogue_trainer_droids = Creature:new {
	objectName = "",
	customName = "a Malfunctioning Practice Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 75,
	chanceHit = 10,
	damageMin = 485,
	damageMax = 780,
	baseXp = 7668,
	baseHAM = 16000,
	baseHAMmax = 18000,
	armor = 0,
	resists = {15,15,0,45,10,0,0,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = NONE,

	templates = {"object/mobile/som/8t88.iff"},
	lootGroups = {
		{
			groups = {
				{group = "p_robe_tokens", chance = 5000000},
				{group = "robe_padawan", chance = 1000000},
				{group = "junk", chance = 4000000},
			},
			lootChance = 2700000
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = marksmanrmaster
}

CreatureTemplates:addCreatureTemplate(rogue_trainer_droids, "rogue_trainer_droids")
