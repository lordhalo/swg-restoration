trainer_droids = Creature:new {
	objectName = "",
	customName = "a Practice Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 10,
	chanceHit = 3,
	damageMin = 45,
	damageMax = 125,
	baseXp = 1500,
	baseHAM = 1310,
	baseHAMmax = 1490,
	armor = 0,
	resists = {0,0,0,0,10,0,0,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = NONE,

	templates = {"object/mobile/som/8t88.iff"},
	lootGroups = {
		{
			groups = {
				{group = "p_robe_tokens", chance = 10000000},
			},
			lootChance = 1700000
		},

		{
			groups = {
				{group = "crystals_poor", chance = 5000000},
				{group = "power_crystals", chance = 5000000},
			},
			lootChance = 2700000
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = marksmanrmaster
}

CreatureTemplates:addCreatureTemplate(trainer_droids, "trainer_droids")
