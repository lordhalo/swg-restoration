clone_trooper = Creature:new {
	objectName = "",
	customName = "a Clone Trooper",
	socialGroup = "",
	faction = "",
	level = 70,
	chanceHit = 75,
	damageMin = 1000,
	damageMax = 2350,
	baseXp = 9429,
	baseHAM = 1000000,
	baseHAMmax = 1020000,
	armor = 0,
	resists = {50,50,50,50,50,50,50,50,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {

	},
	weapons = {"nyaxs_weapons"},
	outfit = "clone_trooper",
	conversationTemplate = "",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(clone_trooper, "clone_trooper")
