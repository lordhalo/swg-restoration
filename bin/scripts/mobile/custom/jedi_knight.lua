jedi_knight = Creature:new {
	objectName = "",
	customName = "a Jedi Knight",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "rebel",
	pvpFaction = "rebel",
	faction = "rebel",
	level = 100,
	chanceHit = 4.75,
	damageMin = 700,
	damageMax = 1000,
	baseXp = 13178,
	baseHAM = 50000,
	baseHAMmax = 61000,
	armor = 1,
	resists = {80,80,80,80,80,80,80,80,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {
		{
			groups = {
				{group = "power_crystals", chance = 400000},
				{group = "color_crystals", chance = 1800000},
				{group = "rifles", chance = 1300000},
				{group = "pistols", chance = 1300000},
				{group = "melee_weapons", chance = 1300000},
				{group = "armor_attachments", chance = 900000},
				{group = "clothing_attachments", chance = 900000},
				{group = "carbines", chance = 1300000},
				{group = "wearables_rare", chance = 800000}
			}
		}
	},
	weapons = {"light_jedi_weapons"},
	conversationTemplate = "",
	outfit = "jedi_knight",
	attacks = merge(lightsabermaster,forcewielder)
}

CreatureTemplates:addCreatureTemplate(jedi_knight, "jedi_knight")
