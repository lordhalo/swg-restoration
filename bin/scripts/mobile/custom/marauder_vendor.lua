marauder_vendor = Creature:new {
	objectName = "",
	customName = "Marauder Faction Vendor",
	socialGroup = "endor_marauder",
	faction = "endor_marauder",
	level = 100,
	chanceHit = 1,
	damageMin = 645,
	damageMax = 1000,
	baseXp = 9429,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 0,
	resists = {0,0,0,0,0,0,0,0,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_feral_marauder.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "marauderVendorConvTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(marauder_vendor, "marauder_vendor")
