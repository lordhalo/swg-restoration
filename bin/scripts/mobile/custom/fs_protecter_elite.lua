fs_protecter_elite = Creature:new {
	objectName = "",
	customName = "Village Protecter",
	socialGroup = "fs_villager",
	pvpFaction = "fs_villager",
	faction = "fs_villager",
	level = 4,
	chanceHit = 1.24,
	damageMin = 40,
	damageMax = 45,
	baseXp = 0,--This will make players not want to attack them, no xp.
	baseHAM = 2713, 
	baseHAMmax = 2838,
	armor = 0,
	resists = {25,25,25,25,25,25,25,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {	"object/mobile/dressed_force_trained_archaist.iff",
			"object/mobile/dressed_noble_old_zabrak_male_01.iff", 
			"object/mobile/dressed_noble_old_zabrak_male_02.iff"}, 
	lootGroups = {},
	weapons = {"luke_skywalker_weapons"},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(fs_protecter_elite, "fs_protecter_elite")
