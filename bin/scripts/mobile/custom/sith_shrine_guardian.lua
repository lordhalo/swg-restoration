sith_shrine_guardian = Creature:new {
	objectName = "",
	customName = "a Sith Shrine Guardian",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {80, 178, 18215, 324, 846, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	diet = HERBIVORE,
	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {
		{
			groups = {
				{group = "robe_light", chance = 2000000},
				{group = "l_robe_tokens", chance = 8000000},
			},
						lootChance = 10000000
		}
	},
	weapons = {"dark_jedi_weapons_gen3"},
	outfit = "sith_shrine_guardian_outfit",
	reactionStf = "",
	attacks = merge(lightsabermaster)
}

CreatureTemplates:addCreatureTemplate(sith_shrine_guardian, "sith_shrine_guardian")
