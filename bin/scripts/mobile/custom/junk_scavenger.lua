junk_scavenger = Creature:new {
	objectName = "",
	customName = "a Mad Scavenger",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 65,
	chanceHit = 3.36,
	damageMin = 145,
	damageMax = 185,
	baseXp = 1,
	baseHAM = 7600,
	baseHAMmax = 7800,
	armor = 0,
	resists = {0,0,0,0,10,0,0,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mercenary_weak_hum_m.iff",
			"object/mobile/dressed_plasma_bandit.iff"},
	lootGroups = {
		{
			groups = {
				{group = "padawan_quest_holocron_06", chance = 10000000}
			},
			lootChance = 3500000
		}
	},
	weapons = {"melee_weapons"},
	conversationTemplate = "",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(junk_scavenger, "junk_scavenger")
