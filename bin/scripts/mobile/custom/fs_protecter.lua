fs_protecter = Creature:new {
	objectName = "",
	customName = "Village Protecter",
	socialGroup = "fs_villager",
	pvpFaction = "fs_villager",
	faction = "fs_villager",
	level = 4,
	chanceHit = 1.24,
	damageMin = 40,
	damageMax = 45,
	baseXp = 0,--This will make players not want to attack them, no xp.
	baseHAM = 1713, 
	baseHAMmax = 1838,
	armor = 0,
	resists = {0,0,0,0,0,0,0,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {	"object/mobile/dressed_force_trained_archaist.iff",
			"object/mobile/dressed_noble_old_zabrak_male_01.iff", 
			"object/mobile/dressed_noble_old_zabrak_male_02.iff"}, 
	lootGroups = {},
	weapons = {"village_weapons"},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(fs_protecter, "fs_protecter")
--[[
great question, are you ready?
lets make sure they attack as is... may need to make one agressive... but highly doubt it.
]]
