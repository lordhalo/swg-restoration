bikerBoss = Creature:new {
	objectName = "",
	customName = "Bracks Jeller (a Biker Thug)",
	socialGroup = "thug",
	faction = "thug",
	npcStats = {100, 209, 191050, 800, 1200, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {
		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 10000000
		},

		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 10000000
		},

		{ --Vehicle/House deed rare
	        groups = {
				{group = "player_flare_deed", chance = 5000000},
				{group = "player_garage_deed", chance = 5000000}
			},
			lootChance = 2600000

		},

		{ --Weapon Schematics
	        groups = {
				{group = "cryo_lance_schematic", chance = 3500000},
				{group = "kyd21_pistol_schematic", chance = 3500000},
				{group = "power_cube_enhancement", chance = 3000000}
			},
			lootChance = 2600000

		},

		{ --Deco/Misc 100% drop
	        groups = {
				{group = "corellia_painting_boss", chance = 1000000},
				{group = "anniversary_boss", chance = 6000000},
				{group = "corltwo_painting_boss", chance = 3000000}
			},
			lootChance = 10000000

		},

		{ --Datapad for Engine location
	        groups = {
				{group = "player_flare_engine", chance = 10000000}
			},
			lootChance = 30000000

		}
	},
	weapons = {"nyaxs_weapons"},
	outfit = "bounty_hunter_armor",
	conversationTemplate = "",
	attacks = merge(marksmannovice, brawlernovice)
}

CreatureTemplates:addCreatureTemplate(bikerBoss, "bikerBoss")
