kliknik_questtwo = Creature:new {
	objectName = "",
	customName = "Captured Kliknik",
	socialGroup = "",
	faction = "",
	level = 15,
	chanceHit = 0.36,
	damageMin = 110,
	damageMax = 135,
	baseXp = 50,
	baseHAM = 600,
	baseHAMmax = 800,
	armor = 0,
	resists = {15,15,15,15,15,15,15,-1,-1},
	meatType = "meat_carnivore",
	meatAmount = 6,
	hideType = "hide_scaley",
	hideAmount = 4,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = CARNIVORE,

	templates = {"object/mobile/kliknik_hue.iff"},
	lootGroups = {
	},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "force_sensitive_kliknik_conv",
	attacks = {
		{"intimidationattack",""},
		{"mildpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(kliknik_questtwo, "kliknik_questtwo")
