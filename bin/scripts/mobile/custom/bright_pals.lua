bright_pals = Creature:new {
	objectName = "",
	customName = "Bright Pals",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	level = 10,
	chanceHit = 0.28,
	damageMin = 90,
	damageMax = 110,
	baseXp = 356,
	baseHAM = 810,
	baseHAMmax = 990,
	armor = 0,
	resists = {0,0,0,0,10,0,0,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 136,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_binayre_thief_twk_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "naboo_noob_convo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(bright_pals, "bright_pals")
