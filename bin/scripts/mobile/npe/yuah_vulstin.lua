yuah_vulstin = Creature:new {
	objectName = "",
	customName = "Yuah Vulstin",
	socialGroup = "",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_thug_bothan_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "yuahVulstinConvo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(yuah_vulstin, "yuah_vulstin")
