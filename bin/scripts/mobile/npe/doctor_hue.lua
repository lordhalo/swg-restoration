doctor_hue = Creature:new {
	objectName = "",
	customName = "Doctor Hue",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_doctor_trainer_moncal_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(doctor_hue, "doctor_hue")
