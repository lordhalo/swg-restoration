officer_miller = Creature:new {
	objectName = "",
	customName = "Officer Miller",
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_police_chief.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "newPlayerMeleeConvo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(officer_miller, "officer_miller")
