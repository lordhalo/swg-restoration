npe_trainer_droid = Creature:new {
	objectName = "",
	customName = "a Practice Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = NONE,

	templates = {"object/mobile/som/8t88.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	reactionStf = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(npe_trainer_droid, "npe_trainer_droid")
