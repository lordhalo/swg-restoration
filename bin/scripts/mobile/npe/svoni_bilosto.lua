svoni_bilosto = Creature:new {
	objectName = "",
	customName = "Svoni Bilosto",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_naboo_twilek_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "nabooHouseQuestConvo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(svoni_bilosto, "svoni_bilosto")
