npe_junk_battledroid = Creature:new {
	objectName = "",
	customName = "a Rusty Battle Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = NONE,

	templates = {"object/mobile/death_watch_battle_droid.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	reactionStf = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(npe_junk_battledroid, "npe_junk_battledroid")
