figrin_dan = Creature:new {
	objectName = "",
	customName = "Figrin D'an",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_figrin_dan.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	outfit = "band_kloo_horn_outfit",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(figrin_dan, "figrin_dan")
