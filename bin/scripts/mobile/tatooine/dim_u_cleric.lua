dim_u_cleric = Creature:new {
	objectName = "@mob/creature_names:dim_u_cleric",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "dim_u",
	faction = "",
	npcStats = {3, 15, 288, 22, 153, 15, 0, 300},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_dim_u_cleric.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4500000},
				{group = "wearables_common", chance = 2000000},
				{group = "tailor_components", chance = 1500000},
				{group = "loot_kit_parts", chance = 1500000},
				{group = "dim_u_common", chance = 500000}
			}
		}
	},
	weapons = {},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/fancy",
	attacks = brawlernovice
}

CreatureTemplates:addCreatureTemplate(dim_u_cleric, "dim_u_cleric")
