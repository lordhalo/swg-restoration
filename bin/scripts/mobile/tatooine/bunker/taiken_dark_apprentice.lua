taiken_dark_apprentice = Creature:new {
  customName = "Dark Apprentice",
  socialGroup = "imperial",
  level = 200,
  chanceHit = 0.95,
  damageMin = 645,
  damageMax = 780,
  baseXp = 0,
  baseHAM = 81000,
  baseHAMmax = 99000,
  armor = 1,
  resists = {60,60,60,60,60,60,60,60,-1},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = ENEMY + ATTACKABLE,
  creatureBitmask = PACK + KILLER + HEALER,
  optionsBitmask = 128,
  diet = HERBIVORE,

  templates = {
    "object/mobile/dressed_dark_jedi_human_male_01.iff",
    "object/mobile/dressed_dark_jedi_human_female_01.iff"},

  lootGroups = {
	{
	groups = {
			{group = "madness_common", chance = 10000000},
		},
		lootChance = 100000
	},
	{
	groups = {
			{group = "pearls_premium", chance = 10000000},
		},
		lootChance = 600000
	},
	{
	groups = {
			{group = "clothing_attachments", chance = 5000000},
			{group = "armor_attachments", chance = 5000000},
		},
		lootChance = 2500000
	},
  },
  weapons = {"dark_jedi_weapons_gen2"},
  conversationTemplate = "",
  attacks = merge(lightsabermaster)
}

CreatureTemplates:addCreatureTemplate(taiken_dark_apprentice, "taiken_dark_apprentice")
