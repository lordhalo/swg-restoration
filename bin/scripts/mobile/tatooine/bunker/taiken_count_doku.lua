taiken_count_doku = Creature:new {
  customName = "Darth Savage",
  socialGroup = "imperial",
  level = 265,
  chanceHit = 1.5,
  damageMin = 980,
  damageMax = 1200,
  baseXp = 0,
  baseHAM = 222000,
  baseHAMmax = 230000,
  armor = 3,
  resists = {90,90,90,90,90,90,90,90,90},
  meatType = "",
  meatAmount = 0,
  hideType = "",
  hideAmount = 0,
  boneType = "",
  boneAmount = 0,
  milk = 0,
  tamingChance = 0,
  ferocity = 0,
  pvpBitmask = ENEMY + ATTACKABLE,
  creatureBitmask = PACK + KILLER,
  optionsBitmask = 128,
  diet = HERBIVORE,

  templates = {
    "object/mobile/dressed_lord_nyax.iff"},

  lootGroups = {
	{
	groups = {
			{group = "madness_common", chance = 10000000},
		},
		lootChance = 2500000
	},
	{
	groups = {
			{group = "pearls_premium", chance = 9900000},
			{group = "pearls_flawless", chance = 100000},
		},
		lootChance = 3500000
	},
	{
	groups = {
			{group = "clothing_attachments", chance = 5000000},
			{group = "armor_attachments", chance = 5000000},
		},
		lootChance = 3500000
	},
  },
  weapons = {"dark_jedi_weapons_gen4"},
  conversationTemplate = "",
  reactionStf = "@npc_reaction/fancy",
  attacks = merge(lightsabermaster,forcewielder)
}

CreatureTemplates:addCreatureTemplate(taiken_count_doku, "taiken_count_doku")
