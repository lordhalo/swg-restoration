desert_swooper_leader = Creature:new {
	objectName = "@mob/creature_names:desert_swooper_leader",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "swoop",
	faction = "swoop",
	npcStats = {3, 15, 288, 22, 153, 15, 0, 300},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_desert_swooper_leader.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4000000},
				{group = "tailor_components", chance = 1500000},
				{group = "loot_kit_parts", chance = 1500000},
				{group = "printer_parts", chance = 1000000},
				{group = "desert_swooper_common", chance = 2000000}
			}
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlernovice,marksmannovice)
}

CreatureTemplates:addCreatureTemplate(desert_swooper_leader, "desert_swooper_leader")
