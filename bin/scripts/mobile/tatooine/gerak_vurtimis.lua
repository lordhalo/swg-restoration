gerak_vurtimis = Creature:new {
	objectName = "@mob/creature_names:commoner_human_male",
	customName = "Gerak Vurtimis",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_fat_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = brawlermaster
}

CreatureTemplates:addCreatureTemplate(gerak_vurtimis, "gerak_vurtimis")
