anim_kahn = Creature:new {
	objectName = "@npc_spawner_n:anim_kahn",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_valarian_swooper_leader.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(anim_kahn, "anim_kahn")
