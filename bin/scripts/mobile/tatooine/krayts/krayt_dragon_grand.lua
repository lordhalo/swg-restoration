krayt_dragon_grand = Creature:new {
	objectName = "@mob/creature_names:krayt_dragon_grand",
	socialGroup = "krayt",
	faction = "",
	npcStats = {81, 178, 18395, 327, 852, 325, 5652, 5183},
	meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_bristley",
	hideAmount = 950,
	boneType = "bone_mammal",
	boneAmount = 905,
	milk = 0,
	tamingChance = 0,
	ferocity = 30,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/krayt_dragon.iff"},
	scale = 1.4,

	lootGroups = {
		{
	        groups = {
				{group = "krayt_tissue_rare", chance = 4000000},
				{group = "krayt_pearls", chance = 6000000}
			},
			lootChance = 700000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 3500000},
				{group = "krayt_tissue", chance = 2500000},
				{group = "krayt_tissue_rare", chance = 1500000},
				{group = "krayt_pearls", chance = 2500000},
			},
			lootChance = 3500000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 2500000},
				{group = "krayt_tissue", chance = 2500000},
				{group = "krayt_tissue_rare", chance = 2500000},
				{group = "krayt_pearls", chance = 2500000},
			},
			lootChance = 3500000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"creatureareacombo","stateAccuracyBonus=50"},
		{"blindattack","stateAccuracyBonus=50"},
		{"intimidationattack","stateAccuracyBonus=50"},
		{"creatureareaknockdown","stateAccuracyBonus=50"},
		{"stunattack","stateAccuracyBonus=50"}
	}
}

CreatureTemplates:addCreatureTemplate(krayt_dragon_grand, "krayt_dragon_grand")
