giant_canyon_krayt_dragon = Creature:new {
	objectName = "@mob/creature_names:giant_canyon_krayt_dragon",
	socialGroup = "krayt",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_bristley",
	hideAmount = 870,
	boneType = "bone_mammal",
	boneAmount = 805,
	milk = 0,
	tamingChance = 0,
	ferocity = 20,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/canyon_krayt_dragon.iff"},
	scale = 1.2,

	lootGroups = {
		{
	        groups = {
				{group = "krayt_tissue", chance = 4000000},
				{group = "krayt_pearls", chance = 6000000},
			},
			lootChance = 4000000
		},

		{
	        groups = {
				{group = "krayt_tissue", chance = 4000000},
				{group = "krayt_dragon_common", chance = 6000000},
			},
			lootChance = 3500000
		},

		{
	        groups = {
				{group = "krayt_tissue_rare", chance = 4000000},
				{group = "krayt_pearls", chance = 6000000},
			},
			lootChance = 700000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 2500000},
				{group = "krayt_tissue", chance = 2500000},
				{group = "krayt_tissue_rare", chance = 1500000},
				{group = "krayt_pearls", chance = 3500000},
			},
			lootChance = 3500000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 2500000},
				{group = "krayt_tissue", chance = 3500000},
				{group = "krayt_tissue_rare", chance = 1500000},
				{group = "krayt_pearls", chance = 2500000},
			},
			lootChance = 3500000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"creatureareaattack","stateAccuracyBonus=50"},
		{"creatureareaknockdown","stateAccuracyBonus=50"},
		{"dizzyattack","stateAccuracyBonus=50"}
	}
}

CreatureTemplates:addCreatureTemplate(giant_canyon_krayt_dragon, "giant_canyon_krayt_dragon")
