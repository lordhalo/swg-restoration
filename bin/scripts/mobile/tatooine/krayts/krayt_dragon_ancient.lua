krayt_dragon_ancient = Creature:new {
	objectName = "@mob/creature_names:krayt_dragon_ancient",
	socialGroup = "krayt",
	faction = "",
	npcStats = {83, 199, 18673, 333, 862, 344, 5826, 5303},
	meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_bristley",
	hideAmount = 950,
	boneType = "bone_mammal",
	boneAmount = 905,
	milk = 0,
	tamingChance = 0,
	ferocity = 30,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/krayt_dragon.iff"},
	scale = 1.3,

	lootGroups = {
		{
	        groups = {
				{group = "krayt_tissue_rare", chance = 5000000},
				{group = "krayt_pearls", chance = 5000000}
			},
			lootChance = 900000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 2500000},
				{group = "krayt_tissue", chance = 2500000},
				{group = "krayt_tissue_rare", chance = 2500000},
				{group = "krayt_pearls", chance = 2500000},
			},
			lootChance = 3500000
		},

		{
	        groups = {
				{group = "krayt_dragon_common", chance = 2000000},
				{group = "krayt_tissue", chance = 4000000},
				{group = "krayt_pearls", chance = 4000000}
			},
			lootChance = 3500000

		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"creatureareacombo","stateAccuracyBonus=100"},
		{"creatureareaknockdown","stateAccuracyBonus=100"}
	}
}

CreatureTemplates:addCreatureTemplate(krayt_dragon_ancient, "krayt_dragon_ancient")
