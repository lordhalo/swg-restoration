eight_lom = Creature:new {
	objectName = "@theme_park_name:lom",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/theme_park_record_keeper_jabba.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(eight_lom, "eight_lom")
