jabba_the_hutt = Creature:new {
	objectName = "@mob/creature_names:jabba_the_hutt",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/jabba_the_hutt.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jabbaConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(jabba_the_hutt, "jabba_the_hutt")
