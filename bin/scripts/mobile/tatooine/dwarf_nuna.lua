dwarf_nuna = Creature:new {
	objectName = "@mob/creature_names:dwarf_nuna",
	socialGroup = "nuna",
	faction = "",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "meat_avian",
	meatAmount = 2,
	hideType = "hide_leathery",
	hideAmount = 3,
	boneType = "bone_avian",
	boneAmount = 1,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dwarf_nuna.iff"},
	controlDeviceTemplate = "object/intangible/pet/dwarf_nuna_hue.iff",
	scale = 0.65,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(dwarf_nuna, "dwarf_nuna")
