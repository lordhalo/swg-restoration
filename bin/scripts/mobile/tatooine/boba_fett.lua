boba_fett = Creature:new {
	objectName = "@mob/creature_names:boba_fett",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/boba_fett.iff"},
	lootGroups = {},
	weapons = {"boba_fett_weapons"},
	conversationTemplate = "bobaFettConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(boba_fett, "boba_fett")
