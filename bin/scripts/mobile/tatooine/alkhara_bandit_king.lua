alkhara_bandit_king = Creature:new {
	objectName = "@mob/creature_names:alkhara_bandit_king",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "alkhara",
	faction = "alkhara",
	npcStats = {22, 71, 3087, 52, 257, 71, 0, 1415},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_alkhara_king.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2550000},
				{group = "wearables_common", chance = 2000000},
				{group = "rifles", chance = 2000000},
				{group = "tailor_components", chance = 1500000},
				{group = "loot_kit_parts", chance = 1500000},
				{group = "alkhara_common", chance = 450000}
			}
		}
	},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(alkhara_bandit_king, "alkhara_bandit_king")
