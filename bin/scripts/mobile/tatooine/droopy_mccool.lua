droopy_mccool = Creature:new {
	objectName = "@mob/creature_names:droopy_mccool",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/droopy_mccool.iff"},
	lootGroups = {},
	weapons = {},
	outfit = "band_flute_droopy_outfit",
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(droopy_mccool, "droopy_mccool")
