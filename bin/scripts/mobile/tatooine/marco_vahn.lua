marco_vahn = Creature:new {
	objectName = "",
	customName = "Marco Vahn (a booking agent)",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "padawan_old_musician_02_convo_template",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(marco_vahn, "marco_vahn")
