rale_moonraiser = Creature:new {
	objectName = "",
	customName = "Rale Moonraiser",
	socialGroup = "darklighter",
	faction = "",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/darklight_guard.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = merge(riflemanmaster,pitoleermaster,carbineermaster)
}

CreatureTemplates:addCreatureTemplate(rale_moonraiser, "rale_moonraiser")
