drooling_nuna = Creature:new {
	objectName = "@mob/creature_names:dwarf_nuna_drooling",
	socialGroup = "nuna",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "meat_avian",
	meatAmount = 2,
	hideType = "hide_leathery",
	hideAmount = 3,
	boneType = "bone_avian",
	boneAmount = 1,
	milk = 0,
	tamingChance = 0.05,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dwarf_nuna.iff"},
	controlDeviceTemplate = "object/intangible/pet/dwarf_nuna_hue.iff",
	scale = 0.75,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(drooling_nuna, "drooling_nuna")
