protocol_droid_3po_red = Creature:new {
	objectName = "@mob/creature_names:protocol_droid_3po_red",
	socialGroup = "",
	faction = "",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid_red.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(protocol_droid_3po_red, "protocol_droid_3po_red")
