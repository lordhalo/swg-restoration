tusken_raid_leader_boss = Creature:new {
	customName = "Hurtsh Hururzht (an Elite Tusken Raid Leader)",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,
	scale = 1.3,

	templates = {"object/mobile/tusken_raider.iff"},
	lootGroups = {
		{
	        groups = { -- Exotic DOT weapon pack rare
				{group = "sand_crawler_house_deed", chance = 10000000}
			},
			lootChance = 3500000
		},

		{
	        groups = {
				{group = "delicate_assembly", chance = 5000000},
				{group = "illegal_core_booster", chance = 5000000}
			},
			lootChance = 3500000
		},

		{
		groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 5000000
		},

		{
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}
			},
			lootChance = 5000000
		},

	},
	weapons = {"tusken_weapons"},
	conversationTemplate = "",
	attacks = merge(marksmanmaster,brawlermaster,fencermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(tusken_raid_leader_boss, "tusken_raid_leader_boss")
