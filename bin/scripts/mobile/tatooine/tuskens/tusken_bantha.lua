tusken_bantha = Creature:new {
	objectName = "@mob/creature_names:tusken_bantha",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "meat_domesticated",
	meatAmount = 475,
	hideType = "hide_wooly",
	hideAmount = 350,
	boneType = "bone_mammal",
	boneAmount = 375,
	milkType = "milk_domesticated",
	milk = 235,
	tamingChance = 0,
	ferocity = 2,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/bantha_saddle_hue.iff",
			"object/mobile/bantha_saddle.iff"},
	scale = 1.25,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(tusken_bantha, "tusken_bantha")
