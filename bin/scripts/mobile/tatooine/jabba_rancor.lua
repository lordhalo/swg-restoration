jabba_rancor = Creature:new {
	objectName = "@mob/creature_names:jabbas_palace_rancor",
	socialGroup = "jabba ",
	faction = "",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = CARNIVORE,

	templates = {"object/mobile/rancor.iff"},
	lootGroups = {},
	weapons = {},
	attacks = {
		{"stunattack",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(jabba_rancor, "jabba_rancor")
