talon_karrde = Creature:new {
	objectName = "@mob/creature_names:talon_karrde",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_talon_karrde.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",--TODO: convert to a JTL 'Smuggler Alliance' PILOT TRAINER for 'Tier 2' missions
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(talon_karrde, "talon_karrde")
