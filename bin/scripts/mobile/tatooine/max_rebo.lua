max_rebo = Creature:new {
	objectName = "@mob/creature_names:max_rebo",
	socialGroup = "jabba",
	faction = "jabba",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	optionsBitmask = AIENABLED,
	creatureBitmask = PACK,
	diet = HERBIVORE,

	templates = {"object/mobile/max_rebo.iff"},
	lootGroups = {},
	weapons = {},
	outfit = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(max_rebo, "max_rebo")
