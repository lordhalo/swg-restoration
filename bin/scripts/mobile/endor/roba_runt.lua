roba_runt = Creature:new {
	objectName = "@mob/creature_names:roba_runt",
	socialGroup = "roba",
	faction = "",
	npcStats = {44, 98, 4889, 76, 326, 98, 0, 3125},
	meatType = "meat_carnivore",
	meatAmount = 8,
	hideType = "hide_leathery",
	hideAmount = 14,
	boneType = "bone_mammal",
	boneAmount = 8,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/roba_hue.iff"},
	scale = 0.75,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(roba_runt, "roba_runt")
