young_hanadak_rock_crusher = Creature:new {
	objectName = "@mob/creature_names:young_hanadak_rock_crusher",
	socialGroup = "hanadak",
	faction = "",
	npcStats = {37, 89, 4316, 69, 307, 89, 0, 2651},
	meatType = "meat_carnivore",
	meatAmount = 35,
	hideType = "hide_bristley",
	hideAmount = 35,
	boneType = "bone_mammal",
	boneAmount = 35,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/young_hanadak_rock_crusher.iff"},
	scale = 0.85,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(young_hanadak_rock_crusher, "young_hanadak_rock_crusher")
