gleaming_lantern_bird = Creature:new {
	objectName = "@mob/creature_names:gleaming_lantern_bird",
	socialGroup = "lantern",
	faction = "",
	npcStats = {32, 84, 3906, 64, 291, 84, 0, 2187},
	meatType = "meat_herbivore",
	meatAmount = 75,
	hideType = "hide_leathery",
	hideAmount = 41,
	boneType = "bone_avian",
	boneAmount = 46,
	milk = 0,
	tamingChance = 0.2,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/lantern_bird_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/lantern_bird_hue.iff",
	scale = 1.1,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(gleaming_lantern_bird, "gleaming_lantern_bird")
