highland_remmer = Creature:new {
	objectName = "@mob/creature_names:highland_remmer",
	socialGroup = "remmer",
	faction = "",
	npcStats = {58, 143, 5915, 88, 364, 143, 0, 3947},
	meatType = "meat_carnivore",
	meatAmount = 46,
	hideType = "hide_bristley",
	hideAmount = 35,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 5,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/remmer_hue.iff"},
	scale = 1.05,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(highland_remmer, "highland_remmer")
