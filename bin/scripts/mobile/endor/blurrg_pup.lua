blurrg_pup = Creature:new {
	objectName = "@mob/creature_names:blurrg_pup",
	socialGroup = "blurrg",
	faction = "",
	npcStats = {44, 98, 4889, 76, 326, 98, 0, 3125},
	meatType = "meat_carnivore",
	meatAmount = 30,
	hideType = "hide_leathery",
	hideAmount = 20,
	boneType = "bone_avian",
	boneAmount = 20,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/blurrg_pup.iff"},
	scale = 0.7,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(blurrg_pup, "blurrg_pup")
