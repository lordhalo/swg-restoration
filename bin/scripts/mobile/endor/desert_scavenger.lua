desert_scavenger = Creature:new {
	objectName = "@mob/creature_names:borgle_desert_runner",
	socialGroup = "borgle",
	faction = "",
	npcStats = {52, 118, 5544, 83, 347, 118, 0, 3597},
	meatType = "meat_carnivore",
	meatAmount = 20,
	hideType = "hide_leathery",
	hideAmount = 5,
	boneType = "bone_avian",
	boneAmount = 5,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/borgle_hue.iff"},
	scale = 1.25,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(desert_scavenger, "desert_scavenger")
