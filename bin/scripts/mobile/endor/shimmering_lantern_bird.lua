shimmering_lantern_bird = Creature:new {
	objectName = "@mob/creature_names:shimmering_lantern_bird",
	socialGroup = "lantern",
	faction = "",
	npcStats = {34, 86, 4070, 66, 298, 86, 0, 2403},
	meatType = "meat_herbivore",
	meatAmount = 75,
	hideType = "hide_leathery",
	hideAmount = 41,
	boneType = "bone_avian",
	boneAmount = 46,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/lantern_bird_hue.iff"},
	scale = 1.05,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(shimmering_lantern_bird, "shimmering_lantern_bird")
