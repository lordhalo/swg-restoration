bloodseeker_mite_drone = Creature:new {
	objectName = "@mob/creature_names:bloodseeker_mite_drone",
	socialGroup = "bloodseeker",
	faction = "",
	npcStats = {57, 138, 5861, 87, 361, 138, 0, 3894},
	meatType = "meat_insect",
	meatAmount = 15,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/bloodseeker_mite.iff"},
	scale = 0.9,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(bloodseeker_mite_drone, "bloodseeker_mite_drone")
