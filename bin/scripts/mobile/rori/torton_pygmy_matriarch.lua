torton_pygmy_matriarch = Creature:new {
	objectName = "@mob/creature_names:torton_pygmy_matriarch",
	socialGroup = "torton",
	faction = "",
	npcStats = {26, 77, 3415, 57, 271, 77, 0, 1666},
	meatType = "meat_carnivore",
	meatAmount = 700,
	hideType = "hide_wooly",
	hideAmount = 500,
	boneType = "bone_mammal",
	boneAmount = 700,
	milk = 0,
	tamingChance = 0,
	ferocity = 9,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/torton_hue.iff"},
	scale = .6,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(torton_pygmy_matriarch, "torton_pygmy_matriarch")
