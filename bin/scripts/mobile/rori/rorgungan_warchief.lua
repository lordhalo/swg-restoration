rorgungan_warchief = Creature:new {
	objectName = "@mob/creature_names:rorgungan_warchief",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rorgungan",
	faction = "rorgungan",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER + HERD,
	diet = HERBIVORE,

	templates = {"object/mobile/gungan_s02_male.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 5500000},
				{group = "gungan_common", chance = 2000000},
				{group = "tailor_components", chance = 500000},
				{group = "loot_kit_parts", chance = 1500000},
				{group = "color_crystals", chance = 250000},
				{group = "power_crystals", chance = 250000}
			}
		}
	},
	weapons = {"rebel_weapons_heavy"},
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(rorgungan_warchief, "rorgungan_warchief")
