vrobal = Creature:new {
	objectName = "@mob/creature_names:vrobal",
	socialGroup = "vrobal",
	faction = "",
	npcStats = {14, 42, 2432, 47, 244, 42, 0, 972},
	meatType = "meat_carnivore",
	meatAmount = 13,
	hideType = "hide_leathery",
	hideAmount = 18,
	boneType = "bone_mammal",
	boneAmount = 13,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/vrobal.iff"},
	controlDeviceTemplate = "object/intangible/pet/roba_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(vrobal, "vrobal")
