vir_vur = Creature:new {
	objectName = "@mob/creature_names:vir_vur",
	socialGroup = "vir_vur",
	faction = "",
	npcStats = {8, 36, 1215, 40, 230, 36, 0, 625},
	meatType = "meat_avian",
	meatAmount = 10,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_mammal",
	boneAmount = 4,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/vir_vur.iff"},
	controlDeviceTemplate = "object/intangible/pet/vir_vur_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(vir_vur, "vir_vur")
