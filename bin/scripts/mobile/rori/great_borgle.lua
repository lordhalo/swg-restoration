great_borgle = Creature:new {
	objectName = "@mob/creature_names:great_borgle",
	socialGroup = "borgle",
	faction = "",
	npcStats = {14, 42, 2432, 47, 244, 42, 0, 972},
	meatType = "meat_carnivore",
	meatAmount = 11,
	hideType = "hide_leathery",
	hideAmount = 7,
	boneType = "bone_avian",
	boneAmount = 11,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 2,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/great_borgle.iff"},
	controlDeviceTemplate = "object/intangible/pet/borgle_hue.iff",
	scale = 1.2,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(great_borgle, "great_borgle")
