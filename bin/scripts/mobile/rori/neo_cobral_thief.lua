neo_cobral_thief = Creature:new {
	objectName = "@mob/creature_names:cobral_thief",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "cobral",
	faction = "cobral",
	npcStats = {13, 41, 2350, 47, 243, 41, 0, 919},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_cobral_thief_human_female_01.iff",
		"object/mobile/dressed_cobral_thief_human_male_01.iff"},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2000000},
				{group = "wearables_common", chance = 2000000},
				{group = "pistols", chance = 2000000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermid,marksmanmid)
}

CreatureTemplates:addCreatureTemplate(neo_cobral_thief, "neo_cobral_thief")
