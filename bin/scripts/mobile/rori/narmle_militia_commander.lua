narmle_militia_commander = Creature:new {
	objectName = "@mob/creature_names:narmle_commander",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "narmle",
	faction = "narmle",
	npcStats = {22, 71, 3087, 52, 257, 71, 0, 1415},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_narmle_commander_rodian_female_01.iff",
		"object/mobile/dressed_narmle_commander_rodian_male_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 3500000},
				{group = "wearables_all", chance = 2000000},
				{group = "color_crystals", chance = 500000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"rebel_weapons_heavy"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(narmle_militia_commander, "narmle_militia_commander")
