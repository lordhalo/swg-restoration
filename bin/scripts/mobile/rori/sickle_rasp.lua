sickle_rasp = Creature:new {
	objectName = "@mob/creature_names:sickle_rasp",
	socialGroup = "rasp",
	faction = "",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "meat_avian",
	meatAmount = 15,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 5,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/crowned_rasp_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"mildpoison",""},
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(sickle_rasp, "sickle_rasp")
