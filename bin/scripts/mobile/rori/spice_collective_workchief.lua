spice_collective_workchief = Creature:new {
	objectName = "@mob/creature_names:spice_collective_workchief",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "spice_collective",
	faction = "spice_collective",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_spice_collective_workchief_twk_female_01.iff",
		"object/mobile/dressed_spice_collective_workchief_twk_male_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2000000},
				{group = "wearables_common", chance = 2000000},
				{group = "rifles", chance = 2000000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"ranged_weapons"},
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermid,marksmanmid)
}

CreatureTemplates:addCreatureTemplate(spice_collective_workchief, "spice_collective_workchief")
