neo_cobral_hitman = Creature:new {
	objectName = "@mob/creature_names:cobral_hitman",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "cobral",
	faction = "cobral",
	npcStats = {22, 71, 3087, 52, 257, 71, 0, 1415},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_cobral_hitman_human_female_01.iff",
		"object/mobile/dressed_cobral_hitman_human_male_01.iff"},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2000000},
				{group = "wearables_common", chance = 2000000},
				{group = "rifles", chance = 2000000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"ranged_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(neo_cobral_hitman, "neo_cobral_hitman")
