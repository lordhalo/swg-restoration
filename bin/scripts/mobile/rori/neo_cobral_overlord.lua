neo_cobral_overlord = Creature:new {
	objectName = "@mob/creature_names:cobral_overlord",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "cobral",
	faction = "cobral",
	npcStats = {32, 84, 3906, 64, 291, 84, 0, 2187},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_cobral_overlord_human_female_01.iff",
		"object/mobile/dressed_cobral_overlord_human_male_01.iff"},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2000000},
				{group = "wearables_common", chance = 2000000},
				{group = "heavy_weapons", chance = 2000000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(neo_cobral_overlord, "neo_cobral_overlord")
