pudgy_nudfuh = Creature:new {
	objectName = "@mob/creature_names:pudgy_nudfuh",
	socialGroup = "nudfuh",
	faction = "",
	npcStats = {7, 35, 990, 37, 218, 35, 0, 568},
	meatType = "meat_herbivore",
	meatAmount = 900,
	hideType = "hide_leathery",
	hideAmount = 1000,
	boneType = "bone_mammal",
	boneAmount = 900,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/nudfuh.iff"},
	scale = 0.85,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(pudgy_nudfuh, "pudgy_nudfuh")
