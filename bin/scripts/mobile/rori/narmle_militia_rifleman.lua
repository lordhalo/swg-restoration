narmle_militia_rifleman = Creature:new {
	objectName = "@mob/creature_names:narmle_rifleman",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "narmle",
	faction = "narmle",
	npcStats = {12, 40, 2268, 46, 242, 40, 0, 868},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_narmle_rifleman_rodian_male_01.iff",
		"object/mobile/dressed_narmle_rifleman_rodian_female_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2000000},
				{group = "wearables_common", chance = 2000000},
				{group = "rifles", chance = 2000000},
				{group = "tailor_components", chance = 2000000},
				{group = "loot_kit_parts", chance = 2000000}
			}
		}
	},
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = merge(brawlermid,marksmanmid)
}

CreatureTemplates:addCreatureTemplate(narmle_militia_rifleman, "narmle_militia_rifleman")
