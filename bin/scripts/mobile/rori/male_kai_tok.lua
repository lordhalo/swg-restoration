male_kai_tok = Creature:new {
	objectName = "@mob/creature_names:male_kai_tok",
	socialGroup = "kai_tok",
	faction = "",
	npcStats = {12, 40, 2268, 46, 242, 40, 0, 868},
	meatType = "meat_carnivore",
	meatAmount = 25,
	hideType = "hide_leathery",
	hideAmount = 41,
	boneType = "bone_avian",
	boneAmount = 46,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 2,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/kai_tok.iff"},
	controlDeviceTemplate = "object/intangible/pet/kai_tok_hue.iff",
	lootGroups = {},
	weapons = {"creature_spit_small_green"},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(male_kai_tok, "male_kai_tok")
