nym_domesticated_gurk = Creature:new {
	objectName = "@mob/creature_names:nym_domesticated_gurk",
	socialGroup = "gurk", "nym",
	faction = "nym",
	npcStats = {25, 76, 3333, 56, 268, 76, 0, 1602},
	meatType = "meat_herbivore",
	meatAmount = 300,
	hideType = "hide_leathery",
	hideAmount = 225,
	boneType = "bone_mammal",
	boneAmount = 250,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/gurk_hue.iff"},
	scale = 0.8,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(nym_domesticated_gurk, "nym_domesticated_gurk")
