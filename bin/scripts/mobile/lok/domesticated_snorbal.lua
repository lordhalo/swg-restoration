domesticated_snorbal = Creature:new {
	objectName = "@mob/creature_names:domesticated_snorbal",
	socialGroup = "self",
	faction = "",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "meat_herbivore",
	meatAmount = 545,
	hideType = "hide_leathery",
	hideAmount = 440,
	boneType = "bone_mammal",
	boneAmount = 400,
	milkType = "milk_domesticated",
	milk = 300,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/snorbal_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(domesticated_snorbal, "domesticated_snorbal")
