spined_snake = Creature:new {
	objectName = "@mob/creature_names:spined_snake",
	socialGroup = "spine_snake",
	faction = "",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "meat_reptilian",
	meatAmount = 11,
	hideType = "hide_leathery",
	hideAmount = 5,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/spined_snake.iff"},
	controlDeviceTemplate = "object/intangible/pet/spined_snake_hue.iff",
	scale = 1.1,
	lootGroups = {},
	weapons = {"creature_spit_small_toxicgreen"},
	conversationTemplate = "",
	attacks = {
		{"mediumpoison",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(spined_snake, "spined_snake")
