vixur_webb = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Vixur Webb (a musician)",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {23, 74, 3169, 54, 261, 74, 0, 1483},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_noble_old_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "padawan_old_musician_03_convo_template",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(vixur_webb, "vixur_webb")
