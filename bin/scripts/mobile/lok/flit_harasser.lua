flit_harasser = Creature:new {
	objectName = "@mob/creature_names:flit_harasser",
	socialGroup = "flit",
	faction = "",
	npcStats = {8, 36, 1215, 40, 230, 36, 0, 625},
	meatType = "meat_avian",
	meatAmount = 9,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 10,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/flit_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/flit_hue.iff",
	scale = 1.15,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(flit_harasser, "flit_harasser")
