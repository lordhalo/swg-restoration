mountain_vesp = Creature:new {
	objectName = "@mob/creature_names:mountain_vesp",
	socialGroup = "vesp",
	faction = "",
	npcStats = {24, 76, 3251, 55, 264, 76, 0, 1535},
	meatType = "meat_reptilian",
	meatAmount = 11,
	hideType = "hide_leathery",
	hideAmount = 5,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/vesp_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/vesp_hue.iff",
	scale = 0.95,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	attacks = {
		{"blindattack",""},
		{"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(mountain_vesp, "mountain_vesp")
