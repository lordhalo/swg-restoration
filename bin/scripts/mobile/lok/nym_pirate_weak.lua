nym_pirate_weak = Creature:new {
	objectName = "@mob/creature_names:nym_pirate_weak",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nym",
	faction = "nym",
	npcStats = {23, 74, 3169, 54, 261, 74, 0, 1483},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_nym_pirate_weak_hum_f.iff",
		"object/mobile/dressed_nym_pirate_weak_rod_m.iff",
		"object/mobile/dressed_nym_pirate_weak_nikto_m.iff",
		"object/mobile/dressed_nym_pirate_weak_hum_m.iff",
		"object/mobile/dressed_nym_pirate_weak_rod_f.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 6500000},
				{group = "nyms_common", chance = 1000000},
				{group = "pistols", chance = 1000000},
				{group = "carbines", chance = 1000000},
				{group = "tailor_components", chance = 500000}
			}
		}
	},
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(bountyhuntermaster,marksmanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(nym_pirate_weak, "nym_pirate_weak")
