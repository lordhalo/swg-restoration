lowland_salt_mynock = Creature:new {
	objectName = "@mob/creature_names:lowland_salt_mynock",
	socialGroup = "mynock",
	faction = "",
	npcStats = {23, 74, 3169, 54, 261, 74, 0, 1483},
	meatType = "meat_herbivore",
	meatAmount = 55,
	hideType = "hide_leathery",
	hideAmount = 73,
	boneType = "bone_mammal",
	boneAmount = 25,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/salt_mynock_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/salt_mynock_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"mediumdisease",""}

	}
}

CreatureTemplates:addCreatureTemplate(lowland_salt_mynock, "lowland_salt_mynock")
