canyon_corsair_cutthroat = Creature:new {
	objectName = "@mob/creature_names:canyon_corsair_cutthroat",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "canyon_corsair",
	faction = "canyon_corsair",
	npcStats = {34, 86, 4070, 66, 298, 86, 0, 2403},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_corsair_cutthroat_hum_f.iff",
		"object/mobile/dressed_corsair_cutthroat_hum_m.iff",
		"object/mobile/dressed_corsair_cutthroat_wee_m.iff",
		"object/mobile/dressed_corsair_cutthroat_zab_m.iff"
	},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 3650000},
				{group = "tailor_components", chance = 1000000},
				{group = "color_crystals", chance = 400000},
				{group = "power_crystals", chance = 400000},
				{group = "simple_toolkits", chance = 1800000},
				{group = "clothing_attachments", chance = 450000},
				{group = "armor_attachments", chance = 450000},
				{group = "canyon_corsair_common", chance = 1350000},
				{group = "wearables_uncommon", chance = 500000},
			}
		}
	},
	weapons = {"canyon_corsair_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(swordsmanmaster,carbineermaster,tkamaster,brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(canyon_corsair_cutthroat, "canyon_corsair_cutthroat")
