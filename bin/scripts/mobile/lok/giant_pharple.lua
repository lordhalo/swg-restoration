giant_pharple = Creature:new {
	objectName = "@mob/creature_names:giant_pharple",
	socialGroup = "pharple",
	faction = "",
	npcStats = {4, 18, 428, 25, 165, 18, 0, 375},
	meatType = "meat_avian",
	meatAmount = 21,
	hideType = "hide_bristley",
	hideAmount = 12,
	boneType = "bone_avian",
	boneAmount = 9,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/giant_pharple.iff"},
	scale = 1.5,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(giant_pharple, "giant_pharple")
