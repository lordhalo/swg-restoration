vesp_scavenger = Creature:new {
	objectName = "@mob/creature_names:vesp_scavanger",
	socialGroup = "vesp",
	faction = "",
	npcStats = {27, 78, 3497, 59, 275, 78, 0, 1741},
	meatType = "meat_reptilian",
	meatAmount = 9,
	hideType = "hide_leathery",
	hideAmount = 3,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/vesp.iff"},
	controlDeviceTemplate = "object/intangible/pet/vesp_hue.iff",
	scale = 1.15,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(vesp_scavenger, "vesp_scavenger")
