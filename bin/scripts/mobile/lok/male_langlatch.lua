male_langlatch = Creature:new {
	objectName = "@mob/creature_names:langlatch_male",
	socialGroup = "langlatch",
	faction = "",
	npcStats = {13, 41, 2350, 47, 243, 41, 0, 919},
	meatType = "meat_carnivore",
	meatAmount = 10,
	hideType = "hide_wooly",
	hideAmount = 9,
	boneType = "bone_mammal",
	boneAmount = 9,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/langlatch_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/langlatch_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(male_langlatch, "male_langlatch")
