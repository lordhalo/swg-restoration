elite_canyon_corsair = Creature:new {
	objectName = "@mob/creature_names:elite_canyon_corsair",
	socialGroup = "canyon_corsair",
	faction = "canyon_corsair",
	npcStats = {45, 100, 4971, 77, 329, 100, 0, 3196},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_corsair_pirate_elite_hum_f.iff",
		"object/mobile/dressed_corsair_pirate_elite_hum_m.iff",
		"object/mobile/dressed_corsair_pirate_elite_nikto_m.iff",
		"object/mobile/dressed_corsair_pirate_elite_rod_m.iff",
		"object/mobile/dressed_corsair_pirate_elite_wee_m.iff",
		"object/mobile/dressed_corsair_pirate_elite_zab_m.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4000000},
				{group = "tailor_components", chance = 1000000},
				{group = "color_crystals", chance = 400000},
				{group = "power_crystals", chance = 400000},
				{group = "melee_two_handed", chance = 600000},
				{group = "carbines", chance = 600000},
				{group = "pistols", chance = 600000},
				{group = "clothing_attachments", chance = 450000},
				{group = "armor_attachments", chance = 450000},
				{group = "canyon_corsair_common", chance = 1500000}
			}
		}
	},
	weapons = {"canyon_corsair_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(swordsmanmaster,carbineermaster,tkamaster,brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(elite_canyon_corsair, "elite_canyon_corsair")
