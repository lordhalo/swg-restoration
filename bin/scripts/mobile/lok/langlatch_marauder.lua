langlatch_marauder = Creature:new {
	objectName = "@mob/creature_names:langlatch_marauder",
	socialGroup = "langlatch",
	faction = "",
	npcStats = {16, 50, 2596, 48, 246, 50, 0, 1086},
	meatType = "meat_carnivore",
	meatAmount = 12,
	hideType = "hide_wooly",
	hideAmount = 11,
	boneType = "bone_mammal",
	boneAmount = 11,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/langlatch_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/langlatch_hue.iff",
	scale = 1.1,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"blindattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(langlatch_marauder, "langlatch_marauder")
