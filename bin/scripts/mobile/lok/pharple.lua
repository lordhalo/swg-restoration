pharple = Creature:new {
	objectName = "@mob/creature_names:pharple",
	socialGroup = "pharple",
	faction = "",
	npcStats = {1, 5, 108, 17, 128, 5, 0, 100},
	meatType = "meat_avian",
	meatAmount = 18,
	hideType = "hide_bristley",
	hideAmount = 10,
	boneType = "bone_avian",
	boneAmount = 7,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/pharple.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(pharple, "pharple")
