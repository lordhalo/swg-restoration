cas_vankoo = Creature:new {
	objectName = "@mob/creature_names:cas_vankoo",
	socialGroup = "kimogila",
	faction = "bandit",
	npcStats = {55, 131, 8630, 108, 445, 201, 3391, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_lok_cas_vankoo.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 5200000},
				{group = "color_crystals", chance = 600000},
				{group = "power_crystals", chance = 400000},
				{group = "melee_polearm", chance = 600000},
				{group = "pistols", chance = 600000},
				{group = "clothing_attachments", chance = 800000},
				{group = "armor_attachments", chance = 800000},
				{group = "wearables_all", chance = 1000000}
			}
		}
	},
	weapons = {"cas_vankoo_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/townperson",
	attacks = merge(pistoleermaster,pikemanmaster,marksmanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(cas_vankoo, "cas_vankoo")
