bapibac = Creature:new {
	objectName = "@npc_name:human_base_male",
	customName = "Bapibac One-Oesp",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {6, 25, 783, 32, 198, 25, 0, 508},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_rebel_trooper_human_male_01.iff"},
	lootGroups = {},
	weapons = {"rebel_weapons_light"},
	attacks = merge(riflemanmaster,pistoleermaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(bapibac, "bapibac")
