yavin4_mercenary = Lair:new {
	mobiles = {{"mercenary_sentry",1},{"mercenary_sentry",1}},
	spawnLimit = 12,
	buildingsVeryEasy = {"object/building/poi/tatooine_fugitive_camp_large1.iff","object/building/poi/tatooine_fugitive_camp_large2.iff","object/building/poi/tatooine_fugitive_camp_large3.iff"},
	buildingsEasy = {"object/building/poi/tatooine_fugitive_camp_large1.iff","object/building/poi/tatooine_fugitive_camp_large2.iff","object/building/poi/tatooine_fugitive_camp_large3.iff"},
	buildingsMedium = {"object/building/poi/tatooine_fugitive_camp_large1.iff","object/building/poi/tatooine_fugitive_camp_large2.iff","object/building/poi/tatooine_fugitive_camp_large3.iff"},
	buildingsHard = {"object/building/poi/tatooine_fugitive_camp_large1.iff","object/building/poi/tatooine_fugitive_camp_large2.iff","object/building/poi/tatooine_fugitive_camp_large3.iff"},
	buildingsVeryHard = {"object/building/poi/tatooine_fugitive_camp_large1.iff","object/building/poi/tatooine_fugitive_camp_large2.iff","object/building/poi/tatooine_fugitive_camp_large3.iff"},
	mobType = "npc",
	buildingType = "theater"
}

addLairTemplate("yavin4_mercenary", yavin4_mercenary)
