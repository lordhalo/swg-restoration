tatooine_krayt_dragon_adolescent = Lair:new {
	mobiles = {{"krayt_dragon_adolescent",1}},
	spawnLimit = 3,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	buildingType = "none",
}

addLairTemplate("tatooine_krayt_dragon_adolescent", tatooine_krayt_dragon_adolescent)
