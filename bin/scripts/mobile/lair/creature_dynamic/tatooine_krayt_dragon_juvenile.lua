tatooine_krayt_dragon_juvenile = Lair:new {
	mobiles = {{"juvenile_canyon_krayt_dragon",1}},
	spawnLimit = 3,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	buildingType = "none",
}

addLairTemplate("tatooine_krayt_dragon_juvenile", tatooine_krayt_dragon_juvenile)
