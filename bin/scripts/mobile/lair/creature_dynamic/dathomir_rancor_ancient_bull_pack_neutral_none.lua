dathomir_rancor_ancient_bull_pack_neutral_none = Lair:new {
	mobiles = {{"ancient_bull_rancor",2},{"bull_rancor",1}},
	spawnLimit = 3,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	buildingType = "none"
}

addLairTemplate("dathomir_rancor_ancient_bull_pack_neutral_none", dathomir_rancor_ancient_bull_pack_neutral_none)
