endor_death_watch = Lair:new {
	mobiles = {{"death_watch_ghost_lair",1}},
	bossMobiles = {{"death_watch_honorguard_world",1}},
	spawnLimit = 6,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	mobType = "npc",
	buildingType = "none"
}

addLairTemplate("endor_death_watch", endor_death_watch)
