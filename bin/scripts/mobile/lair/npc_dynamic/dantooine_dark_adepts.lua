dantooine_dark_adepts = Lair:new {
	mobiles = {{"dark_adept",1}},
	spawnLimit = 2,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	mobType = "npc",
	buildingType = "none"
}

addLairTemplate("dantooine_dark_adepts", dantooine_dark_adepts)
