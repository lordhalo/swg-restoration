yavin4_trainer_droids = Lair:new {
	mobiles = {{"trainer_droids",1}, {"trainer_droids",1}, {"trainer_droids",1}},
	spawnLimit = 6,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	mobType = "npc",
	buildingType = "none"
}

addLairTemplate("yavin4_trainer_droids", yavin4_trainer_droids)
