lok_black_sun = Lair:new {
	mobiles = {{"black_sun_henchman_lair",1}},
	bossMobiles = {{"black_sun_honorguard_world",1}},
	spawnLimit = 6,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	mobType = "npc",
	buildingType = "none"
}

addLairTemplate("lok_black_sun", lok_black_sun)
