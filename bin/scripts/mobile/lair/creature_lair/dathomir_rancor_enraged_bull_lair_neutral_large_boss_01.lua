dathomir_rancor_enraged_bull_lair_neutral_large_boss_01 = Lair:new {
	mobiles = {{"enraged_bull_rancor",3},{"bull_rancor",3},{"ancient_bull_rancor",2}},
	bossMobiles = {{"monstrous_brute",1}},
	spawnLimit = 8,
	buildingsVeryEasy = {"object/tangible/lair/base/poi_all_lair_bones_large_fog_red.iff"},
	buildingsEasy = {"object/tangible/lair/base/poi_all_lair_bones_large_fog_red.iff"},
	buildingsMedium = {"object/tangible/lair/base/poi_all_lair_bones_large_fog_red.iff"},
	buildingsHard = {"object/tangible/lair/base/poi_all_lair_bones_large_fog_red.iff"},
	buildingsVeryHard = {"object/tangible/lair/base/poi_all_lair_bones_large_fog_red.iff"},
}

addLairTemplate("dathomir_rancor_enraged_bull_lair_neutral_large_boss_01", dathomir_rancor_enraged_bull_lair_neutral_large_boss_01)
