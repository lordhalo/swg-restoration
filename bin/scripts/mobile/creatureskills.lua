--This is the Skill trees for NPC's
--to modify this for hybrids you can create a name for your hybrid tree and insert it at the bottom of the list
--in the Server Administrator NPC Skill section
-- command usage inside npc templates is attacks = merge(skilltreename1,skilltreename2,ect,ect)

--creature level 1 to 10
brawlernovice = { {"meleeStrike",""} }
marksmannovice = { {"placedShot",""} }

--creature level 11 to 15
brawlermid = { {"meleeStrike",""} }
marksmanmid = { {"meleeStrike",""} }

--creature level 16 to 20
marksmanmaster = { {"placedShot",""} }
brawlermaster = { {"meleeStrike",""} }

--creature level 21 to 25 use base profession master with these depending on weapons in thier weapons groups
bountyhunternovice = { {"placedShot",""} }
commandonovice = { {"placedShot",""} }
carbineernovice = { {"placedShot",""} }
pistoleernovice = { {"placedShot",""} }
riflemannovice = { {"placedShot",""} }
fencernovice = { {"meleeStrike",""} }
swordsmannovice = { {"meleeStrike",""} }
pikemannovice = { {"meleeStrike",""} }
tkanovice = { {"meleeStrike",""} }

--creature level 26 to 30 use base profession master with these depending on weapons in their weapons groups
bountyhuntermid = { {"placedShot",""} }
commandomid = { {"placedShot",""} }
carbineermid = { {"placedShot",""} }
pistoleermid = { {"placedShot",""} }
riflemanmid = { {"placedShot",""} }
fencermid = { {"meleeStrike",""} }
swordsmanmid = { {"meleeStrike",""} }
pikemanmid = { {"meleeStrike",""} }
tkamid = { {"meleeStrike",""} }

--creature level 31 and above use combinations of base profesion mastery and these
bountyhuntermaster = { {"placedShot",""} }
commandomaster = { {"placedShot",""} }
carbineermaster = { {"placedShot",""} }
pistoleermaster = { {"placedShot",""}, {"bodyShot2",""} }
riflemanmaster = { {"placedShot",""} }
fencermaster = { {"bleedattack3",""}, {"blindattack3",""}, {"bodyhit2",""} }
swordsmanmaster = { {"criticalstrike2",""}, {"headhit2",""}, {"powerattack2",""} }
pikemanmaster = { {"spinattack2",""}, {"sweepattack3",""}, {"impale2",""} }
tkamaster = { {"comboattack3",""}, {"dizzyattack3",""}, {"legsweep3",""} }

--npc jedi skills
lightsabermaster = { {"saberdervish",""}, {"saberblind2",""}, {"saberstunstrike2",""}, {"sabersweep2",""}, {"saberpowerattack2",""} }
forcepowermaster = { {"forceknockdown3",""}, {"forcechoke",""}, {"forcelightningarea2",""} }
-- npc force wielders use standard profession mastery with the addition of this command
lightsaberwielder = { {"saberdervish",""},{"sabersweep2",""} }
forcewielder = { {"forceintimidate2",""},{"forceknockdown3",""},{"forcelightning2",""} }
necrosis = { {"saberpowerattack2",""},{"sabersweep2",""},{"saberspinattack2",""} }
--Server Administrator NPC skill trees place below
