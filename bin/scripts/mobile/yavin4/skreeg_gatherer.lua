skreeg_gatherer = Creature:new {
	objectName = "@mob/creature_names:skreeg_gatherer",
	socialGroup = "skreeg",
	faction = "",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "meat_herbivore",
	meatAmount = 18,
	hideType = "hide_bristley",
	hideAmount = 9,
	boneType = "bone_mammal",
	boneAmount = 7,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/skreeg_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/skreeg_hue.iff",
	scale = 0.9,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(skreeg_gatherer, "skreeg_gatherer")
