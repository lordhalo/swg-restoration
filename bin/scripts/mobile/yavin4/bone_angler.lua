bone_angler = Creature:new {
	objectName = "@mob/creature_names:angler_bone",
	socialGroup = "angler",
	faction = "",
	npcStats = {27, 78, 3497, 59, 275, 78, 0, 1741},
	meatType = "meat_insect",
	meatAmount = 4,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.05,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/angler_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/angler_hue.iff",
	scale = 1.1,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"strongpoison",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(bone_angler, "bone_angler")
