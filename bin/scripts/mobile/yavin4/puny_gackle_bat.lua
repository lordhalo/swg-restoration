puny_gackle_bat = Creature:new {
	objectName = "@mob/creature_names:puny_gackle_bat",
	socialGroup = "gacklebat",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "meat_carnivore",
	meatAmount = 2,
	hideType = "hide_bristley",
	hideAmount = 1,
	boneType = "bone_mammal",
	boneAmount = 1,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/puny_gackle_bat.iff"},
	scale = 0.6,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(puny_gackle_bat, "puny_gackle_bat")
