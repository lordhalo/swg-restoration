female_mawgax = Creature:new {
	objectName = "@mob/creature_names:mawgax_female",
	socialGroup = "mawgax",
	faction = "",
	npcStats = {23, 74, 3169, 54, 261, 74, 0, 1483},
	meatType = "meat_domesticated",
	meatAmount = 115,
	hideType = "hide_leathery",
	hideAmount = 72,
	boneType = "bone_avian",
	boneAmount = 52,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/mawgax_hue.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(female_mawgax, "female_mawgax")
