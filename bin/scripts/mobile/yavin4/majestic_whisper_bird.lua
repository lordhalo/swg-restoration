majestic_whisper_bird = Creature:new {
	objectName = "@mob/creature_names:whisper_bird_majestic",
	socialGroup = "bird",
	faction = "",
	npcStats = {19, 65, 2841, 50, 249, 65, 0, 1254},
	meatType = "meat_avian",
	meatAmount = 65,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 40,
	milk = 0,
	tamingChance = 0.05,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/whisper_bird_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/lantern_bird_hue.iff",
	scale = 1.2,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(majestic_whisper_bird, "majestic_whisper_bird")
