giant_mawgax = Creature:new {
	objectName = "@mob/creature_names:giant_mawgax",
	socialGroup = "mawgax",
	faction = "",
	npcStats = {32, 84, 3906, 64, 291, 84, 0, 2187},
	meatType = "meat_domesticated",
	meatAmount = 130,
	hideType = "hide_leathery",
	hideAmount = 85,
	boneType = "bone_avian",
	boneAmount = 70,
	milk = 0,
	tamingChance = 0.2,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/giant_mawgax.iff"},
	controlDeviceTemplate = "object/intangible/pet/mawgax_hue.iff",
	scale = 1.4,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(giant_mawgax, "giant_mawgax")
