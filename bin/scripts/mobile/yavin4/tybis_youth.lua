tybis_youth = Creature:new {
	objectName = "@mob/creature_names:tybis_youth",
	socialGroup = "tybis",
	faction = "",
	npcStats = {18, 63, 2759, 49, 248, 63, 0, 1202},
	meatType = "meat_herbivore",
	meatAmount = 200,
	hideType = "hide_bristley",
	hideAmount = 150,
	boneType = "bone_mammal",
	boneAmount = 100,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/tybis_youth.iff"},
	scale = 0.75,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(tybis_youth, "tybis_youth")
