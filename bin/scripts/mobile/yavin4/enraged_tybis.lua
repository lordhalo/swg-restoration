enraged_tybis = Creature:new {
	objectName = "@mob/creature_names:enraged_tybis",
	socialGroup = "tybis",
	faction = "",
	npcStats = {26, 77, 3415, 57, 271, 77, 0, 1666},
	meatType = "meat_herbivore",
	meatAmount = 220,
	hideType = "hide_bristley",
	hideAmount = 160,
	boneType = "bone_mammal",
	boneAmount = 105,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	diet = HERBIVORE,

	templates = {"object/mobile/tybis_hue.iff"},
	scale = 1.15,
	lootGroups = {},
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(enraged_tybis, "enraged_tybis")
