angler = Creature:new {
	objectName = "@mob/creature_names:angler",
	socialGroup = "angler",
	faction = "",
	npcStats = {22, 71, 3087, 52, 257, 71, 0, 1415},
	meatType = "meat_insect",
	meatAmount = 4,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/angler.iff"},
	controlDeviceTemplate = "object/intangible/pet/angler_hue.iff",
	lootGroups = {},
	weapons = {"creature_spit_small_green","creature_spit_small_green"},
	conversationTemplate = "",
	attacks = {
		{"strongpoison",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(angler, "angler")
