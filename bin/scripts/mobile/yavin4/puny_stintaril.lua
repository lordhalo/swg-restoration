puny_stintaril = Creature:new {
	objectName = "@mob/creature_names:puny_stintaril",
	socialGroup = "stinaril",
	faction = "",
	npcStats = {7, 35, 990, 37, 218, 35, 0, 568},
	meatType = "meat_carnivore",
	meatAmount = 2,
	hideType = "hide_bristley",
	hideAmount = 1,
	boneType = "bone_mammal",
	boneAmount = 1,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/puny_stintaril.iff"},
	scale = 0.6,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(puny_stintaril, "puny_stintaril")
