commoner_fat = Creature:new {
	objectName = "@mob/creature_names:commoner",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {	"object/mobile/dressed_commoner_fat_human_female_01.iff",
					"object/mobile/dressed_commoner_fat_human_female_02.iff",
					"object/mobile/dressed_commoner_fat_human_male_01.iff",
					"object/mobile/dressed_commoner_fat_human_male_02.iff",
					"object/mobile/dressed_commoner_fat_twilek_female_01.iff",
					"object/mobile/dressed_commoner_fat_twilek_female_02.iff",
					"object/mobile/dressed_commoner_fat_twilek_male_01.iff",
					"object/mobile/dressed_commoner_fat_twilek_male_02.iff",
					"object/mobile/dressed_commoner_fat_zabrak_female_01.iff",
					"object/mobile/dressed_commoner_fat_zabrak_female_02.iff",
					"object/mobile/dressed_commoner_fat_zabrak_male_01.iff",
					"object/mobile/dressed_commoner_fat_zabrak_male_02.iff"
					},

	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(commoner_fat, "commoner_fat")
