contractor = Creature:new {
	objectName = "@mob/creature_names:crafting_contractor",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED + CONVERSABLE + INTERESTING,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_artisan_trainer_02.iff"},

	lootGroups = {},
	weapons = {},
	conversationTemplate = "craftingContractorConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(contractor, "contractor")
