medic = Creature:new {
	objectName = "@mob/creature_names:medic",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = HERD,
	diet = HERBIVORE,

	templates = {
				"object/mobile/dressed_doctor_trainer_human_female_01.iff",
				"object/mobile/dressed_doctor_trainer_moncal_male_01.iff",
				"object/mobile/dressed_mercenary_elite_medic_human_male_01.iff",
	},
	lootGroups = {},
	weapons = {},
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(medic, "medic")
