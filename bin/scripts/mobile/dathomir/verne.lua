verne = Creature:new {
	objectName = "@mob/creature_names:verne",
	socialGroup = "verne",
	faction = "",
	npcStats = {16, 50, 2596, 48, 246, 50, 0, 1086},
	meatType = "meat_herbivore",
	meatAmount = 125,
	hideType = "hide_leathery ",
	hideAmount = 90,
	boneType = "bone_mammal",
	boneAmount = 80,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/verne.iff"},
	controlDeviceTemplate = "object/intangible/pet/verne_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(verne, "verne")
