young_malkloc_plainswalker = Creature:new {
	objectName = "@mob/creature_names:malkloc_plainswalker_youth",
	socialGroup = "malkloc",
	faction = "",
	npcStats = {53, 123, 5626, 84, 350, 123, 0, 3649},
	meatType = "meat_herbivore",
	meatAmount = 1500,
	hideType = "hide_leathery",
	hideAmount = 1350,
	boneType = "bone_mammal",
	boneAmount = 1100,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/young_malkloc.iff"},
	scale = 0.9,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(young_malkloc_plainswalker, "young_malkloc_plainswalker")
