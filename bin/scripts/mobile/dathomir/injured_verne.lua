injured_verne = Creature:new {
	objectName = "@mob/creature_names:injured_verne",
	socialGroup = "verne",
	faction = "",
	npcStats = {2, 10, 180, 20, 140, 10, 0, 200},
	meatType = "meat_wild",
	meatAmount = 30,
	hideType = "hide_leathery",
	hideAmount = 25,
	boneType = "bone_mammal",
	boneAmount = 22,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/verne.iff"},
	scale = 0.9,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(injured_verne, "injured_verne")
