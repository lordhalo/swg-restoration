jaleela_bindoo = Creature:new {
	objectName = "",
	customName = "Jaleela Bindoo",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {8, 36, 1215, 40, 230, 36, 0, 625},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_jaleela_bindoo.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(jaleela_bindoo, "jaleela_bindoo")
