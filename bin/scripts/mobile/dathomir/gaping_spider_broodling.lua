gaping_spider_broodling = Creature:new {
	objectName = "@mob/creature_names:gaping_spider_broodling",
	socialGroup = "spider",
	faction = "",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "meat_insect",
	meatAmount = 5,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 5,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/gaping_spider_broodling.iff"},
	scale = 0.75,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"mildpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(gaping_spider_broodling, "gaping_spider_broodling")
