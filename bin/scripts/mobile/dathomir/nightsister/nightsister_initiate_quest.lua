nightsister_initiate_quest = Creature:new {
	objectName = "",
	customName = "Litau (a Crazed Nightsister initiate)",
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {60, 147, 9038, 113, 461, 222, 3826, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_initiate.iff"},
	lootGroups = {
		{
			groups = {
				{group = "padawan_quest_holocron_04", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"mixed_force_weapons"},
	conversationTemplate = "",
	attacks = merge(pikemanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(nightsister_initiate_quest, "nightsister_initiate_quest")
