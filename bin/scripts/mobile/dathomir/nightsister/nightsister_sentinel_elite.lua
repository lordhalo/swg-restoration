nightsister_sentinel_elite = Creature:new {
	objectName = "@mob/creature_names:nightsister_sentinal",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {85, 209, 11592, 141, 546, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_sentinal.iff"},
	lootGroups = {
		{
			groups = {
				{group = "nightsister_brutal", chance = 500000},
				{group = "crystals_premium", chance = 1000000},
				{group = "nightsister_common", chance = 850000},
				{group = "armor_attachments", chance = 4000000},
				{group = "clothing_attachments", chance = 4000000},
			},
			lootChance = 3800000
		},
		{
			groups = {
				{group = "nightsister_rare", chance = 40000},
				{group = "crystals_premium", chance = 6500000},
				{group = "nightsister_brutal", chance = 3460000},

			},
			lootChance = 1000000
		}
	},
	weapons = {"nightsister_sentinel_elite"},
	conversationTemplate = "",
	attacks = merge(fencermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(nightsister_sentinel_elite, "nightsister_sentinel_elite")
