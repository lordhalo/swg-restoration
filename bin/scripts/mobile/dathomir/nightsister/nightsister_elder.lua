nightsister_elder = Creature:new {
	objectName = "@mob/creature_names:nightsister_elder",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_elder.iff"},
	lootGroups = {
		{
			groups = {
				{group = "nightsister_rare", chance = 40000},
				{group = "power_crystals", chance = 1000000},
				{group = "nightsister_common", chance = 960000},
				{group = "armor_attachments", chance = 4000000},
				{group = "clothing_attachments", chance = 4000000},
			},
			lootChance = 3800000
		},

		{
			groups = {
				{group = "power_crystals", chance = 1000000},
				{group = "nightsister_common", chance = 1000000},
				{group = "armor_attachments", chance = 4000000},
				{group = "clothing_attachments", chance = 4000000},
			},
			lootChance = 3500000
		},

		{
			groups = {
				{group = "delicate_assembly", chance = 10000000}
			},
			lootChance = 3900000
		},

		{
			groups = {
				{group = "nightsister_rare", chance = 40000},
				{group = "power_crystals", chance = 6500000},
				{group = "nightsister_uncommon", chance = 3460000},

			},
			lootChance = 3500000
		}
	},
	weapons = {"polearm_weapons"},
	conversationTemplate = "",
	attacks = merge(pikemanmaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(nightsister_elder, "nightsister_elder")
