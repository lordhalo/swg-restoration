nightsister_force_ranger = Creature:new {
	objectName = "@mob/creature_names:nightsister_ranger",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {77, 174, 7142, 105, 415, 174, 0, 4938},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_ranger.iff"},
	lootGroups = {
		{
			groups = {
				{group = "power_crystals", chance = 1000000},
				{group = "color_crystals", chance = 500000},
				{group = "nightsister_common", chance = 2500000},
				{group = "armor_attachments", chance = 2500000},
				{group = "clothing_attachments", chance = 2500000},
				{group = "nightsister_force_ranger", chance = 1000000}
			},
			lootChance = 2100000
		},

		{
			groups = {
				{group = "power_crystals", chance = 1000000},
				{group = "color_crystals", chance = 500000},
				{group = "nightsister_common", chance = 2000000},
				{group = "armor_attachments", chance = 2500000},
				{group = "clothing_attachments", chance = 2500000},
				{group = "nightsister_force_ranger", chance = 1500000}
			},
			lootChance = 2100000
		}
	},
	weapons = {"nightsister_force_ranger"},
	conversationTemplate = "",
	attacks = merge(pikemanmaster)
}

CreatureTemplates:addCreatureTemplate(nightsister_force_ranger, "nightsister_force_ranger")
