naboo_ancient_spider = Creature:new {
	objectName = "",
	customName = "an Ancient Spider",
	socialGroup = "spider",
	faction = "",
	level = 225,
	chanceHit = 25,
	damageMin = 1115,
	damageMax = 1175,
	baseXp = 0,
	baseHAM = 2500000,
	baseHAMmax = 2500000,
	armor = 0,
	resists = { 75, 75, 75, 75, 25, 85, 35, 15, 15 },
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,
	scale = 2.0,

	templates = {"object/mobile/naboo_spiderboss.iff"},
	lootGroups = {

		{
 		groups = { 
 				{group = "ancient_spider", chance = 10000000}
 			},
 			lootChance = 10000000 
 		},

		{
 		groups = { 
 				{group = "ancient_spider", chance = 10000000}
 			},
 			lootChance = 5000000 
 		},

		{
 		groups = { 
 				{group = "massassi_knuckler", chance = 10000000}
 			},
 			lootChance = 5000000 
 		},


		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}					
			},
			lootChance = 10000000
		},

		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}				
			},
			lootChance = 10000000
		},
		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}				
			},
			lootChance = 10000000
		},
		{ -- default attachments 100% drop
	        groups = {
				{group = "clothing_attachments", chance = 5000000},
				{group = "armor_attachments", chance = 5000000}				
			},
			lootChance = 10000000
		}
 	},
	weapons = {"unarmed_weapons"},
	conversationTemplate = "",
	reactionStf = "",
	attacks = {
		{"creatureareableeding",""},
		{"creatureareacombo",""}
	}
}

CreatureTemplates:addCreatureTemplate(naboo_ancient_spider, "naboo_ancient_spider")
