mandalorian_renegade = Creature:new {
	customName = "Myyek",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {100, 209, 221050, 800, 1200, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = { "object/mobile/dressed_death_watch_gold.iff" },
	lootGroups = {
		{
			groups = {
				{group = "power_crystals", chance = 2000000},
				{group = "armor_attachments", chance = 2500000},
				{group = "clothing_attachments", chance = 2500000},
				{group = "holocron_dark", chance = 500000},
				{group = "power_cube_enhancement", chance = 2500000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"dark_trooper_weapons"},
	conversationTemplate = "",
	attacks = merge(marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(mandalorian_renegade, "mandalorian_renegade")
