ancient_spider_guard = Creature:new {
	customName = "Spider Consort",
	socialGroup = "spider",
	faction = "",
	level = 110,
	chanceHit = 10.29,
	damageMin = 845,
	damageMax = 875,
	baseXp = 430,
	baseHAM = 500000,
	baseHAMmax = 500000,
	armor = 0,
	resists = { 75, 75, 75, 75, 25, 85, 35, 15, 15 },
	meatType = "meat_insect",
	meatAmount = 7,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + STALKER,
	optionsBitmask = 128,
	diet = CARNIVORE,

	templates = {"object/mobile/nightspider_aggressor.iff"},
	scale = 1.5,
	lootGroups = {},
	weapons = {"creature_spit_small_toxicgreen"},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""},
		{"mildpoison",""}
	}
}

CreatureTemplates:addCreatureTemplate(ancient_spider_guard, "ancient_spider_guard")
