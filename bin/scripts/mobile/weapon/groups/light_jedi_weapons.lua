light_jedi_weapons = {
	"object/weapon/melee/sword/crafted_saber/npc_saber_1h_blue.iff",
	"object/weapon/melee/sword/crafted_saber/npc_saber_1h_green.iff",
	"object/weapon/melee/2h_sword/crafted_saber/npc_saber_2h_blue.iff",
	"object/weapon/melee/2h_sword/crafted_saber/npc_saber_2h_green.iff",
	"object/weapon/melee/polearm/crafted_saber/npc_saber_pole_blue.iff",
	"object/weapon/melee/polearm/crafted_saber/npc_saber_pole_green.iff"
}

addWeapon("light_jedi_weapons", light_jedi_weapons)
--To facilitate the correctness to Weapons groups
--please include comments under here that point to your source
--such as links to screenshots from wiki's that show pre-cu creature holding specific weapons
--or links to cannon wiki pages showing a weapon belonging to a specific group in the Star Wars Universe
--Source:
