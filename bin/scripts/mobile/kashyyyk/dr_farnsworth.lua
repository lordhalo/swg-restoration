dr_farnsworth = Creature:new {
	objectName = "",
	customName = "Dr Farnsworth",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/ep3/ep3_dr_farnsworth.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "farnsworthConvo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(dr_farnsworth, "dr_farnsworth")
