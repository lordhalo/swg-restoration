sentinel_ryashk = Creature:new {
	--objectName = "",
	customName = "Sentinel Ryashk",
	socialGroup = "self",
	pvpFaction = "",
	faction = "",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_jedi_trainer_old_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jedi_daily_one",
	attacks = merge(forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(sentinel_ryashk, "sentinel_ryashk")
