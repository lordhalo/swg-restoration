jedi_vendor = Creature:new {
	objectName = "",
	customName = "Jedi Relics",
	socialGroup = "",
	faction = "rebel",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_jedi_trainer_old_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jediVendorConvTemplate",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(jedi_vendor, "jedi_vendor")
