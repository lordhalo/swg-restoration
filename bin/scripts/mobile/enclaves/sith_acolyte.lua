sith_acolyte = Creature:new {
	objectName = "",
	customName = "Sith Acolyte",
	socialGroup = "self",
	pvpFaction = "",
	faction = "",
	npcStats = {80, 178, 18215, 324, 846, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_dark_jedi_human_male_01.iff",
		"object/mobile/dressed_dark_jedi_human_female_01.iff"},
	lootGroups = {},
	weapons = {"dark_jedi_weapons_gen3"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster, forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(sith_acolyte, "sith_acolyte")
