guardian_lyshnal = Creature:new {
	objectName = "",
	customName = "Guardian Lyshnal",
	socialGroup = "self",
	pvpFaction = "",
	faction = "",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dark_jedi_human_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jedi_daily_two",
	attacks = merge(forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(guardian_lyshnal, "guardian_lyshnal")
