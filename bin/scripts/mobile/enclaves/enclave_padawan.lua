enclave_padawan = Creature:new {
	objectName = "",
	customName = "a Jedi Padawan",
	socialGroup = "rebel",
	pvpFaction = "rebel",
	faction = "rebel",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	weapons = {"luke_skywalker_weapons"},
	outfit = "jedi_padawan_s01",
	conversationTemplate = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(enclave_padawan, "enclave_padawan")
