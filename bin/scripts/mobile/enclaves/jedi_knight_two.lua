jedi_knight_two = Creature:new {
	objectName = "",
	customName = "Jedi Guardian Euunl (Knight Trials Two Quest Giver)",
	socialGroup = "self",
	pvpFaction = "",
	faction = "",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dark_jedi_human_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jedi_knight_trials_two",
	attacks = merge(forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(jedi_knight_two, "jedi_knight_two")
