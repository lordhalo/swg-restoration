assault_trooper_restuss = Creature:new {
	objectName = "@mob/creature_names:assault_trooper_restuss",
	randomNameType = NAME_STORMTROOPER,
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,
	scale = 1.05,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	outfit = "assault_trooper",
	weapons = {"st_assault_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/stormtrooper",
	personalityStf = "@hireling/hireling_stormtrooper",
	attacks = merge(marksmanmaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(assault_trooper_restuss, "assault_trooper_restuss")
