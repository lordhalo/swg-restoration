rebel_vendor = Creature:new {
	objectName = "",
	customName = "Restuss Commendation Vendor",
	socialGroup = "",
	faction = "rebel",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,
	scale = 1.2,

	templates = {"object/mobile/dressed_criminal_slicer_human_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "restussVendorConvoTemplate",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(rebel_vendor, "rebel_vendor")
