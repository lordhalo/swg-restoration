lieutenant_gregor = Creature:new {
	customName = "Lieutenant Gregor",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_imperial_officer_m_2.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "",
}

CreatureTemplates:addCreatureTemplate(lieutenant_gregor, "lieutenant_gregor")
