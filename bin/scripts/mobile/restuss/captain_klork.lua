captain_klork = Creature:new {
	customName = "Captain Klork",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_rebel_army_captain_human_male_01.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "captain_klork_convo",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(captain_klork, "captain_klork")
