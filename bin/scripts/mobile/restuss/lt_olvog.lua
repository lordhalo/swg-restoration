lt_olvog = Creature:new {
	customName = "Lt Olvog",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_rebel_second_lieutenant_twk_male_01.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "",
}

CreatureTemplates:addCreatureTemplate(lt_olvog, "lt_olvog")
