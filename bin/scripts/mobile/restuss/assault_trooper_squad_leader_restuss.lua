assault_trooper_squad_leader_restuss = Creature:new {
	objectName = "@mob/creature_names:fbase_stormtrooper_squad_leader_restuss",
	randomNameType = NAME_STORMTROOPER_TAG,
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {83, 199, 11334, 139, 539, 344, 5826, 5303},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_stormtrooper_squad_leader_white_white.iff"},
	lootGroups = {},
	weapons = {"st_assault_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/stormtrooper",
	personalityStf = "@hireling/hireling_stormtrooper",
	attacks = merge(marksmanmaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(assault_trooper_squad_leader_restuss, "assault_trooper_squad_leader_restuss")
