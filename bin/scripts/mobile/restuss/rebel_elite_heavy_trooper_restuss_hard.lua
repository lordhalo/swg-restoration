rebel_elite_heavy_trooper_restuss_hard = Creature:new {
	objectName = "@mob/creature_names:rebel_jedi_knight_restuss",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {85, 209, 11592, 141, 546, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	outfit = "jedi_knight_s02",
	lootGroups = {},
	weapons = {"light_jedi_weapons"},
	conversationTemplate = "",
	attacks = merge(lightsaberwielder)
}

CreatureTemplates:addCreatureTemplate(rebel_elite_heavy_trooper_restuss_hard, "rebel_elite_heavy_trooper_restuss_hard")
