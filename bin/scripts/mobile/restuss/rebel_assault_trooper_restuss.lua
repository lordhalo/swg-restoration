rebel_assault_trooper_restuss = Creature:new {
	objectName = "@mob/creature_names:rebel_assault_trooper_restuss",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,
	scale = 1.05,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	outfit = "rebel_battle_armor",
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	personalityStf = "@hireling/hireling_military",
	attacks = merge(marksmanmaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(rebel_assault_trooper_restuss, "rebel_assault_trooper_restuss")
