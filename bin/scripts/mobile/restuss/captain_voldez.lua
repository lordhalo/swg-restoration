captain_voldez = Creature:new {
	customName = "Captain Voldez",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_rebel_first_lieutenant_human_female_01.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "captain_voldez_convo",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(captain_voldez, "captain_voldez")
