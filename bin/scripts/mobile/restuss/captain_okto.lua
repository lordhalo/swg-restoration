captain_okto = Creature:new {
	customName = "Captain Okto",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_imperial_officer_m_2.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "captain_okto_convo",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(captain_okto, "captain_okto")
