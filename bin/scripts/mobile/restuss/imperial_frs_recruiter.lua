imperial_frs_recruiter = Creature:new {
	customName = "a Stormtrooper Recruiter",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {82, 189, 7469, 110, 428, 189, 0, 5243},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,


	templates = {"object/mobile/dressed_stormtrooper_squad_leader_black_gold.iff"},

	lootGroups = {},
	weapons = {},
	attacks = {},
	conversationTemplate = "gcw_frs_recruiter_convo",
	optionsBitmask = 264
}

CreatureTemplates:addCreatureTemplate(imperial_frs_recruiter, "imperial_frs_recruiter")
