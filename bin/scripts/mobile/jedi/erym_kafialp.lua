erym_kafialp = Creature:new {
	customName = "Master Erym Kafialp",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	--templates = {"object/mobile/dressed_dark_jedi_master_male_sul_01.iff"},
	templates = {"object/mobile/dressed_jedi_trainer_old_human_male_01.iff"},

	lootGroups = {},
	weapons = {"light_jedi_weapons"},
	conversationTemplate = "dark_or_light_convo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(erym_kafialp, "erym_kafialp")
