oowe_nonage = Creature:new {
	customName = "Oowe Nonage",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {80, 178, 7358, 108, 423, 178, 0, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dark_jedi_human_male_01.iff",},
	lootGroups = {},
	weapons = {"light_jedi_weapons"},
	conversationTemplate = "oowe_nonage_convo",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(oowe_nonage, "oowe_nonage")
