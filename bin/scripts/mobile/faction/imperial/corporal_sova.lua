corporal_sova = Creature:new {
	objectName = "@mob/creature_names:corporal_sova",
	socialGroup = "imperial",
	faction = "imperial",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_imperial_major_m.iff"},
	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 200000},
				{group = "junk", chance = 5900000},
				{group = "rifles", chance = 800000},
				{group = "pistols", chance = 800000},
				{group = "carbines", chance = 800000},
				{group = "melee_weapons", chance = 1200000},
				{group = "clothing_attachments", chance = 150000},
				{group = "armor_attachments", chance = 150000}
			}
		}
	},
	weapons = {"stormtrooper_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	personalityStf = "@hireling/hireling_military",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(corporal_sova, "corporal_sova")
