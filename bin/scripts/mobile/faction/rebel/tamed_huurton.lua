tamed_huurton = Creature:new {
	objectName = "@mob/creature_names:rebel_tamed_huurton",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "meat_wild",
	meatAmount = 12,
	hideType = "hide_wooly",
	hideAmount = 12,
	boneType = "bone_mammal",
	boneAmount = 12,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/huurton.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(tamed_huurton, "tamed_huurton")
