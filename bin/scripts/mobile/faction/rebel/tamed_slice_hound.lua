tamed_slice_hound = Creature:new {
	objectName = "@mob/creature_names:rebel_tamed_slice_hound",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "meat_carnivore",
	meatAmount = 65,
	hideType = "hide_bristley",
	hideAmount = 35,
	boneType = "bone_mammal",
	boneAmount = 30,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/corellian_slice_hound.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(tamed_slice_hound, "tamed_slice_hound")
