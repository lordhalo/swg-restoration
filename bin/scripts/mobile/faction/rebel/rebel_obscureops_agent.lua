rebel_obscureops_agent = Creature:new {
	objectName = "@mob/creature_names:assassin_mission_recruiter_rebel",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {11, 39, 1985, 46, 241, 39, 0, 801},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_assassin_mission_giver_reb_01.iff",
		"object/mobile/dressed_assassin_mission_giver_reb_02.iff",
		"object/mobile/dressed_assassin_mission_giver_reb_03.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = merge(riflemanmaster,pistoleermaster,carbineermaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(rebel_obscureops_agent, "rebel_obscureops_agent")
