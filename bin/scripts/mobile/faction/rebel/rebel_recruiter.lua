rebel_recruiter = Creature:new {
	objectName = "@mob/creature_names:rebel_recruiter",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_rebel_recruiter_human_female_01.iff",
		"object/mobile/dressed_rebel_recruiter_human_female_02.iff",
		"object/mobile/dressed_rebel_recruiter_moncal_male_01.iff",
		"object/mobile/dressed_rebel_recruiter_twilek_female_01.iff"},
	lootGroups = {},
	weapons = {},
	attacks = merge(riflemanmaster,pistoleermaster,carbineermaster,brawlermaster),
	conversationTemplate = "rebelRecruiterConvoTemplate",
	containerComponentTemplate = "FactionRecruiterContainerComponent",
	optionsBitmask = INVULNERABLE + CONVERSABLE

}

CreatureTemplates:addCreatureTemplate(rebel_recruiter, "rebel_recruiter")
