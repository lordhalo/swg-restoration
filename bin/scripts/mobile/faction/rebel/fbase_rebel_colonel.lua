fbase_rebel_colonel = Creature:new {
	objectName = "@mob/creature_names:fbase_rebel_colonel",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {75, 171, 6998, 104, 410, 171, 0, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_rebel_colonel_bothan_male.iff",
		"object/mobile/dressed_rebel_colonel_fat_zabrak_male.iff",
		"object/mobile/dressed_rebel_colonel_moncal_female.iff",
		"object/mobile/dressed_rebel_colonel_old_human_female.iff",
		"object/mobile/dressed_rebel_colonel_sullustan_male_01.iff",
		"object/mobile/dressed_rebel_colonel_twk_male_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 100000},
				{group = "junk", chance = 6000000},
				{group = "weapons_all", chance = 1100000},
				{group = "armor_all", chance = 1100000},
				{group = "clothing_attachments", chance = 150000},
				{group = "armor_attachments", chance = 150000},
				{group = "rebel_officer_common", chance = 400000},
				{group = "wearables_all", chance = 1000000}
			}
		}
	},
	weapons = {"rebel_weapons_medium"},
	conversationTemplate = "rebelRecruiterConvoTemplate",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster,marksmanmaster,riflemannovice,carbineernovice)
}

CreatureTemplates:addCreatureTemplate(fbase_rebel_colonel, "fbase_rebel_colonel")
