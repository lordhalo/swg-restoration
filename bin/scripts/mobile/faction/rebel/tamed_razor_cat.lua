tamed_razor_cat = Creature:new {
	objectName = "@mob/creature_names:rebel_tamed_razor_cat",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "meat_carnivore",
	meatAmount = 65,
	hideType = "hide_bristley",
	hideAmount = 35,
	boneType = "bone_mammal",
	boneAmount = 30,
	milk = 0,
	tamingChance = 0,
	ferocity = 2,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/tusk_cat.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(tamed_razor_cat, "tamed_razor_cat")
