trainer_chef = Creature:new {
	objectName = "@mob/creature_names:trainer_chef",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_chef_trainer_devaronian_male_01.iff",
		"object/mobile/dressed_chef_trainer_human_female_01.iff",
		"object/mobile/dressed_chef_trainer_human_male_01.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "chefTrainerConvoTemplate",
	attacks = {}
}
CreatureTemplates:addCreatureTemplate(trainer_chef,"trainer_chef")
