trainer_shipwright = Creature:new {
	objectName = "@npc_spawner_n:shipwright_trainer",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE + JTLINTERESTING,
	diet = HERBIVORE,

	templates = {
		"object/mobile/space_shipwright_trainer_01.iff",
		"object/mobile/space_shipwright_trainer_02.iff",
		"object/mobile/space_shipwright_trainer_03.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "mechanicTrainerConvoTemplate",
	attacks = {}
}
CreatureTemplates:addCreatureTemplate(trainer_shipwright,"trainer_shipwright")
