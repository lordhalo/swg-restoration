trainer_mechanic = Creature:new {
	objectName = "@mob/creature_names:trainer_mechanic",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_armorsmith_trainer_01.iff",
		"object/mobile/dressed_armorsmith_trainer_02.iff",
		"object/mobile/dressed_armorsmith_trainer_03.iff"
	},
	conversationTemplate = "mechanicTrainerConvoTemplate",
	optionsBitmask = INVULNERABLE + CONVERSABLE
}
CreatureTemplates:addCreatureTemplate(trainer_mechanic,"trainer_mechanic")
