trainer_doctor = Creature:new {
	objectName = "@mob/creature_names:trainer_doctor",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_doctor_trainer_human_female_01.iff",
		"object/mobile/dressed_doctor_trainer_moncal_female_01.iff",
		"object/mobile/dressed_doctor_trainer_moncal_male_01.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "doctorTrainerConvoTemplate",
	attacks = {}
}
CreatureTemplates:addCreatureTemplate(trainer_doctor,"trainer_doctor")
