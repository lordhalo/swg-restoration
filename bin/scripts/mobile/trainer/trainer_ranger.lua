trainer_ranger = Creature:new {
	objectName = "@mob/creature_names:trainer_ranger",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0.000000,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_ranger_trainer_01.iff",
		"object/mobile/dressed_ranger_trainer_02.iff",
		"object/mobile/dressed_ranger_trainer_03.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "rangerTrainerConvoTemplate",
	attacks = {}
}
CreatureTemplates:addCreatureTemplate(trainer_ranger,"trainer_ranger")
