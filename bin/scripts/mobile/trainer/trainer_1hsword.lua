trainer_1hsword = Creature:new {
	objectName = "@mob/creature_names:trainer_1hsword",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "",
	faction = "",
	npcStats = {10, 38, 1715, 45, 240, 38, 0, 744},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_1handsword_trainer_01.iff",
		"object/mobile/dressed_1handsword_trainer_02.iff",
		"object/mobile/dressed_1handsword_trainer_03.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "1hswordTrainerConvoTemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(trainer_1hsword, "trainer_1hsword")
