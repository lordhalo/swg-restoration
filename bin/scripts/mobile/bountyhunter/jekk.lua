jekk = Creature:new {
	objectName = "",
	customName = "Jekk",
	socialGroup = "",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_death_watch_silver.iff"},
	lootGroups = {},
	weapons = {"melee_weapons"},
	conversationTemplate = "deathWatchIntroConvo",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(jekk, "jekk")
