contracterOne = Creature:new {
	objectName = "@mob/creature_names:crafting_contractor",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_death_watch_silver.iff"},
	lootGroups = {},
	weapons = {"melee_weapons"},
	conversationTemplate = "contracterDaily",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(contracterOne, "contracterOne")
