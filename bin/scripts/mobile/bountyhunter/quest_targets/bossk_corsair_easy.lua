bossk_corsair_easy = Creature:new {
	objectName = "",
	customName = "a Canyon Corsair Engineer",
	socialGroup = "canyon_corsair",
	faction = "canyon_corsair",
	npcStats = {75, 171, 6998, 104, 410, 171, 0, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_armorsmith_trainer_01.iff"},
	lootGroups = {},
	weapons = {"canyon_corsair_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(swordsmanmaster,carbineermaster,tkamaster,brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(bossk_corsair_easy, "bossk_corsair_easy")
