bossk_kahmurra_medium = Creature:new {
	objectName = "",
	customName = "a Droid Bio-Genetics Master",
	socialGroup = "kahmurra",
	faction = "",
	npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/4lom.iff"},
	lootGroups = {},
	weapons = {"carbine_weapons"},
	conversationTemplate = "",
	--reactionStf = "@npc_reaction/slang",
	attacks = merge(carbineermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(bossk_kahmurra_medium, "bossk_kahmurra_medium")
