boba_jedi_guard = Creature:new {
	objectName = "",
	customName = "Y'rall (a Grey Jedi Padawan)",
	socialGroup = "",
	faction = "",
	npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	outfit = "jedi_knight",
	weapons = {"dark_jedi_weapons_gen2"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster)
}

CreatureTemplates:addCreatureTemplate(boba_jedi_guard, "boba_jedi_guard")
