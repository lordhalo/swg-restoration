boba_wife_easy = Creature:new {
	objectName = "",
	customName = "Rikln Dannz",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {70, 164, 9972, 124, 495, 250, 4696, 4568},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_patron_chiss_f_01.iff"},
	lootGroups = {},
	weapons = {},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(tkamaster)
}

CreatureTemplates:addCreatureTemplate(boba_wife_easy, "boba_wife_easy")
