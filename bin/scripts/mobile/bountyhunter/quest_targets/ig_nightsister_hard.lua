ig_nightsister_hard = Creature:new {
	objectName = "",
	customName = "Rik'la Nullo (a Nightsister Force Master)",
	socialGroup = "nightsister",
	faction = "nightsister",
	npcStats = {85, 209, 19105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_dathomir_nightsister_elder.iff"},
	lootGroups = {},
	weapons = {"mixed_force_weapons"},
	--reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(tkamaster,swordsmanmaster,fencermaster,pikemanmaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(ig_nightsister_hard, "ig_nightsister_hard")
