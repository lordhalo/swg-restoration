ig_corsec_medium = Creature:new {
	objectName = "",
	customName = "CorSec Smuggler",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_corsec_officer_human_male_01.iff"},
	lootGroups = {},
	weapons = {"han_solo_weapons"},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(pistoleermaster)
}

CreatureTemplates:addCreatureTemplate(ig_corsec_medium, "ig_corsec_medium")
