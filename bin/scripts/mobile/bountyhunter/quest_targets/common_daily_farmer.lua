common_daily_farmer = Creature:new {
	objectName = "@mob/creature_names:farmer",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "",
	faction = "",
	npcStats = {15, 43, 2514, 48, 245, 43, 0, 1029},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_tatooine_nikto_male_01.iff"},
	lootGroups = {},
	weapons = {},
	--reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(common_daily_farmer, "common_daily_farmer")
