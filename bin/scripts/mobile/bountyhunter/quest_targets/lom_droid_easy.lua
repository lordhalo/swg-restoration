lom_droid_easy = Creature:new {
	objectName = "",
	customName = "a Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {70, 164, 9972, 124, 495, 250, 4696, 4568},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/21b_surgical_droid.iff"},
	lootGroups = {},
	weapons = {},
	--reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(tkamaster)
}

CreatureTemplates:addCreatureTemplate(lom_droid_easy, "lom_droid_easy")
