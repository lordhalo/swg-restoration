boba_warren_medium = Creature:new {
	objectName = "",
	customName = "Rogue Droid",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {75, 171, 10497, 130, 513, 267, 5130, 4819},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/le_repair_droid.iff"},
	lootGroups = {},
	weapons = {"imperial_weapons_light"},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(pistoleermaster)
}

CreatureTemplates:addCreatureTemplate(boba_warren_medium, "boba_warren_medium")
