ig_geonosian_epic = Creature:new {
	objectName = "",
	customName = "a Mad Geonosian Scientist",
	socialGroup = "geonosian",
	faction = "",
	npcStats = {85, 209, 50105, 339, 874, 349, 6000, 5423},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_geonosian_commoner_01.iff"},
	lootGroups = {},
	weapons = {"geonosian_weapons"},
	--reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(pistoleermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(ig_geonosian_epic, "ig_geonosian_epic")
