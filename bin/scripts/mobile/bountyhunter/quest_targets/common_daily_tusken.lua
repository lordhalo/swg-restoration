common_daily_tusken = Creature:new {
	objectName = "",
	customName = "a Tusken Warlord",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	npcStats = {70, 164, 9972, 124, 495, 250, 4696, 4568},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/tusken_raider.iff"},
	lootGroups = {},
	weapons = {"tusken_weapons"},
	--reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(marksmanmaster,brawlermaster,fencermaster,riflemanmaster)
}

CreatureTemplates:addCreatureTemplate(common_daily_tusken, "common_daily_tusken")
