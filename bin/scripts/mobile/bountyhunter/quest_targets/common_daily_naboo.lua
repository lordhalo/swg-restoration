common_daily_naboo = Creature:new {
	objectName = "@mob/creature_names:naboo_police",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "naboo_security_force",
	faction = "naboo_security_force",
	npcStats = {70, 164, 9972, 124, 495, 250, 4696, 4568},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_naboo_police.iff"},
	lootGroups = {},
	weapons = {},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(brawlermaster,fencermaster)
}

CreatureTemplates:addCreatureTemplate(common_daily_naboo, "common_daily_naboo")
