death_watch_mha_quest = Creature:new {
	objectName = "",
	customName = "Nulren Raul",
	socialGroup = "",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_bountyhunter_trainer_02.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "deathWatchMHADailyConvo",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(death_watch_mha_quest, "death_watch_mha_quest")
