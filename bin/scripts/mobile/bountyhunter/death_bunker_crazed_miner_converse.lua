death_bunker_crazed_miner_converse = Creature:new {
	objectName = "",
	customName = "Raldo",
	socialGroup = "black_sun",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mand_bunker_crazed_miner.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "blackSunDaily",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(death_bunker_crazed_miner_converse, "death_bunker_crazed_miner_converse")
