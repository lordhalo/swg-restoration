tyber_combat = Creature:new {
	objectName = "",
	customName = "Tyber Zann",
	socialGroup = "death_watch",
	pvpFaction = "",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_death_watch_red.iff"},
	lootGroups = {},
	weapons = {"boba_fett_weapons"},
	conversationTemplate = "",
	attacks = merge(bountyhuntermaster)
}

CreatureTemplates:addCreatureTemplate(tyber_combat, "tyber_combat")
