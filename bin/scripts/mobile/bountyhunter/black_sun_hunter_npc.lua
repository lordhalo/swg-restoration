black_sun_hunter_npc = Creature:new {
	objectName = "",
	customName = "a Rogue Black Sun Assassin",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/mos_taike_guard_young.iff"},
	lootGroups = {},
	outfit = "bounty_npc",
	weapons = {"pirate_weapons_heavy"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster,forcewielderquest)
}

CreatureTemplates:addCreatureTemplate(black_sun_hunter_npc, "black_sun_hunter_npc")
