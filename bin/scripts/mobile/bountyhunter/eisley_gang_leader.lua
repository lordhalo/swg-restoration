eisley_gang_leader = Creature:new {
	objectName = "@mob/creature_names:thug",
	customName = "a Swoop Thug Ringleader",
	socialGroup = "thug",
	faction = "",
	npcStats = {80, 178, 11037, 135, 529, 288, 5565, 5123},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_organized_twilek_male_01.iff"},
	lootGroups = { },
	weapons = {"st_assault_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(carbineermaster)
}

CreatureTemplates:addCreatureTemplate(eisley_gang_leader, "eisley_gang_leader")
