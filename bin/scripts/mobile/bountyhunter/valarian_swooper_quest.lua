valarian_swooper_quest = Creature:new {
	objectName = "@mob/creature_names:valarian_swooper",
	randomNameType = NAME_GENERIC_TAG,
	socialGroup = "valarian",
	faction = "valarian",
	npcStats = {60, 147, 14923, 270, 738, 222, 3826, 4083},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = 128,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_tatooine_valarian_swooper.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlernovice,marksmannovice)
}

CreatureTemplates:addCreatureTemplate(valarian_swooper_quest, "valarian_swooper_quest")
