bossk_daily_easy = Creature:new {
	objectName = "",
	customName = "Bossk",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	scale = 1.2,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/bossk.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bosskDailyEasy",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(bossk_daily_easy, "bossk_daily_easy")
