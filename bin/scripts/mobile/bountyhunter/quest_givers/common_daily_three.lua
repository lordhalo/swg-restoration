common_daily_three = Creature:new {
	objectName = "@mob/creature_names:farmer",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	scale = 1.1,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_quest_farmer.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "commonDailyThree",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(common_daily_three, "common_daily_three")
