boba_daily_easy = Creature:new {
	objectName = "",
	customName = "Kulran Dannz",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	scale = 1.0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/dressed_criminal_thug_aqualish_male_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "bobaDailyEasy",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(boba_daily_easy, "boba_daily_easy")
