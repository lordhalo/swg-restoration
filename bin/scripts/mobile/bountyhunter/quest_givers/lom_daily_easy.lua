lom_daily_easy = Creature:new {
	objectName = "",
	customName = "4-L0M",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	scale = 1.0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/4lom.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "lomDailyEasy",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(lom_daily_easy, "lom_daily_easy")
