ig88_daily_medium = Creature:new {
	objectName = "",
	customName = "IG-88",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/ig_88.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ig88DailyMedium",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(ig88_daily_medium, "ig88_daily_medium")
