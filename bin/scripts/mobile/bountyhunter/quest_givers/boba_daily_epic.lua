boba_daily_epic = Creature:new {
	objectName = "@mob/creature_names:boba_fett",
	socialGroup = "",
	faction = "",
	npcStats = {55, 131, 5753, 86, 356, 131, 0, 3751},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	scale = 1.1,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/boba_fett.iff"},
	lootGroups = {},
	weapons = {"boba_fett_weapons"},
	conversationTemplate = "bobaDailyEpic",
	attacks = merge(brawlermaster)
}

CreatureTemplates:addCreatureTemplate(boba_daily_epic, "boba_daily_epic")
