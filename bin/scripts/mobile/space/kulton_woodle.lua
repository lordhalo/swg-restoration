kulton_woodle = Creature:new {
	objectName = "@npc_spawner_n:kultin_woodle",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/space_greeter_theed_freighter_captain.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(kulton_woodle, "kulton_woodle")
