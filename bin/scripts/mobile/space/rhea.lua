rhea = Creature:new {
	objectName = "@npc_spawner_n:rhea",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/space_privateer_tier1_corellia.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(rhea, "rhea")
