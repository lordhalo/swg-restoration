chassis_dealer = Creature:new {
	objectName = "@chassis_npc:npc_name",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = INVULNERABLE + CONVERSABLE + JTLINTERESTING,
	diet = HERBIVORE,

	templates = {
		"object/mobile/space_chassis_broker_01.iff",
		"object/mobile/space_chassis_broker_02.iff",
		"object/mobile/space_chassis_broker_03.iff",
		"object/mobile/space_chassis_broker_04.iff",
		"object/mobile/space_chassis_broker_05.iff"
	},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "chassis_dealer_convotemplate",
	attacks = {}
}

CreatureTemplates:addCreatureTemplate(chassis_dealer, "chassis_dealer")
