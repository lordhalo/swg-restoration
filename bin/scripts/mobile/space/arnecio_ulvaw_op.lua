arnecio_ulvaw_op = Creature:new {
	objectName = "@npc_spawner_n:arnecio_ulvaw_op",
	socialGroup = "rebel",
	faction = "rebel",
	npcStats = {30, 81, 3742, 61, 284, 81, 0, 1991},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/space_rebel_tier3_arnecio.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(arnecio_ulvaw_op, "arnecio_ulvaw_op")
