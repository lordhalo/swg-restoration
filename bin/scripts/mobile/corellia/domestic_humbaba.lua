domestic_humbaba = Creature:new {
	objectName = "@mob/creature_names:domestic_humbaba",
	socialGroup = "self",
	faction = "",
	npcStats = {4, 18, 428, 25, 165, 18, 0, 375},
	meatType = "meat_domesticated",
	meatAmount = 215,
	hideType = "hide_leathery",
	hideAmount = 115,
	boneType = "bone_mammal",
	boneAmount = 65,
	milkType = "milk_domesticated",
	milk = 105,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/savage_humbaba.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(domestic_humbaba, "domestic_humbaba")
