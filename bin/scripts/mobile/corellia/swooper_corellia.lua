swooper_corellia = Creature:new {
	objectName = "@mob/creature_names:swooper",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "cor_swoop",
	faction = "cor_swoop",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_swooper_human_female_01.iff",
		"object/mobile/dressed_criminal_swooper_human_male_01.iff",
		"object/mobile/dressed_criminal_swooper_zabrak_female_01.iff",
		"object/mobile/dressed_criminal_swooper_zabrak_male_01.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 3500000},
				{group = "loot_kit_parts", chance = 2000000},
				{group = "tailor_components", chance = 1500000},
				{group = "printer_parts", chance = 1000000},
				{group = "swooper_common", chance = 2000000}
			}
		}
	},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(marksmannovice,brawlernovice)
}

CreatureTemplates:addCreatureTemplate(swooper_corellia, "swooper_corellia")
