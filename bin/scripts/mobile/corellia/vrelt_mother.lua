vrelt_mother = Creature:new {
	objectName = "@mob/creature_names:startled_vrelt_mother",
	socialGroup = "vrelt",
	faction = "",
	npcStats = {5, 21, 594, 30, 185, 21, 0, 443},
	meatType = "meat_carnivore",
	meatAmount = 13,
	hideType = "hide_bristley",
	hideAmount = 13,
	boneType = "bone_mammal",
	boneAmount = 13,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/startled_vrelt_mother.iff"},
	controlDeviceTemplate = "object/intangible/pet/stintaril_hue.iff",
	scale = 1.2,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"knockdownattack",""},
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(vrelt_mother, "vrelt_mother")
