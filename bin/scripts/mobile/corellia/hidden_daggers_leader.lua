hidden_daggers_leader = Creature:new {
	objectName = "@mob/creature_names:hidden_daggers_leader",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "hidden_daggers",
	faction = "hidden_daggers",
	npcStats = {19, 65, 2841, 50, 249, 65, 0, 1254},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/dressed_mugger.iff",
		"object/mobile/dressed_criminal_thug_human_female_01.iff",
		"object/mobile/dressed_criminal_thug_human_female_02.iff",
		"object/mobile/dressed_criminal_thug_human_male_01.iff",
		"object/mobile/dressed_criminal_thug_human_male_02.iff"},
	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 3300000},
				{group = "loot_kit_parts", chance = 2000000},
				{group = "tailor_components", chance = 1000000},
				{group = "hidden_dagger_common", chance = 2700000},
				{group = "wearables_all", chance = 1000000},

			}
		}
	},
	weapons = {"ranged_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(hidden_daggers_leader, "hidden_daggers_leader")
