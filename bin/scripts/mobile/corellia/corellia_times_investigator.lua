corellia_times_investigator = Creature:new {
	objectName = "@mob/creature_names:corellia_times_investigator",
	socialGroup = "corellia_times",
	faction = "",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_fed_dub_investigator_twk_female_01.iff"},
	lootGroups = {},
	weapons = {"pirate_weapons_light"},
	conversationTemplate = "",
	attacks = merge(brawlernovice,marksmannovice)
}

CreatureTemplates:addCreatureTemplate(corellia_times_investigator, "corellia_times_investigator")
