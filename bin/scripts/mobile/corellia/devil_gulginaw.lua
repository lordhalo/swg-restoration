devil_gulginaw = Creature:new {
	objectName = "@mob/creature_names:devil_gulginaw",
	socialGroup = "gulginaw",
	faction = "",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "meat_avian",
	meatAmount = 70,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 35,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/devil_gulginaw.iff"},
	controlDeviceTemplate = "object/intangible/pet/gulginaw_hue.iff",
	scale = 1.15,
	lootGroups = {},
	weapons = {"creature_spit_small_red"},
	conversationTemplate = "",
	attacks = {
		{"",""},
		{"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(devil_gulginaw, "devil_gulginaw")
