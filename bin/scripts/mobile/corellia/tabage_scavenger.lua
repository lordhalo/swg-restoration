tabage_scavenger = Creature:new {
	objectName = "@mob/creature_names:tabage_scavenger",
	socialGroup = "tabage",
	faction = "",
	npcStats = {11, 39, 1985, 46, 241, 39, 0, 801},
	meatType = "meat_carnivore",
	meatAmount = 18,
	hideType = "hide_leathery",
	hideAmount = 24,
	boneType = "bone_mammal",
	boneAmount = 7,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/tabage.iff"},
	controlDeviceTemplate = "object/intangible/pet/langlatch_hue.iff",
	scale = 1.1,
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(tabage_scavenger, "tabage_scavenger")
