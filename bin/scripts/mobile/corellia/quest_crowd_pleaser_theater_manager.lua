quest_crowd_pleaser_theater_manager = Creature:new {
	objectName = "@mob/creature_names:quest_crowd_pleaser_theater_manager",
	socialGroup = "townsperson",
	faction = "townsperson",
	npcStats = {17, 61, 2678, 49, 247, 61, 0, 1146},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_entertainer_trainer_twk_female_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(quest_crowd_pleaser_theater_manager, "quest_crowd_pleaser_theater_manager")
