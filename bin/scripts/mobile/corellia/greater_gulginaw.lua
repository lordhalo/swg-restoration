greater_gulginaw = Creature:new {
	objectName = "@mob/creature_names:greater_gulginaw",
	socialGroup = "gulginaw",
	faction = "",
	npcStats = {22, 71, 3087, 52, 257, 71, 0, 1415},
	meatType = "meat_avian",
	meatAmount = 97,
	hideType = "",
	hideAmount = 0,
	boneType = "bone_avian",
	boneAmount = 48,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 6,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/greater_gulginaw.iff"},
	controlDeviceTemplate = "object/intangible/pet/gulginaw_hue.iff",
	scale = 1.25,
	lootGroups = {},
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"dizzyattack",""},
		{"stunattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(greater_gulginaw, "greater_gulginaw")
