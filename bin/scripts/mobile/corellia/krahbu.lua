krahbu = Creature:new {
	objectName = "@mob/creature_names:krahbu",
	socialGroup = "krahbu",
	faction = "",
	npcStats = {13, 41, 2350, 47, 243, 41, 0, 919},
	meatType = "meat_herbivore",
	meatAmount = 210,
	hideType = "hide_bristley",
	hideAmount = 135,
	boneType = "bone_mammal",
	boneAmount = 120,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = HERD,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/krahbu.iff"},
	controlDeviceTemplate = "object/intangible/pet/krahbu_hue.iff",
	lootGroups = {},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"intimidationattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(krahbu, "krahbu")
