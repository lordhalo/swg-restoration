palpatine_holo = Creature:new {
	objectName = "",
	customName = "Emperor Palpatine Hologram",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/ep3/palpatine_hologram.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "ep3PalpatineConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(palpatine_holo, "palpatine_holo")
