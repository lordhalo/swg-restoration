uwo_guard = Creature:new {
	objectName = "",
	customName = "Uwo Poal's Bodyguard",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {35, 87, 6228, 85, 378, 145, 1652, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_criminal_assassin_human_male_01.iff"},
	lootGroups = {},
	weapons = {"imperial_weapons_light"},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(pistoleermaster)
}

CreatureTemplates:addCreatureTemplate(uwo_guard, "uwo_guard")
