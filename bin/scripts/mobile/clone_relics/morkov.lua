morkov = Creature:new {
	objectName = "",
	customName = "Morkov",
	socialGroup = "morkov",
	pvpFaction = "",
	faction = "",
	npcStats = {51, 112, 13450, 246, 690, 178, 3043, 3546},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/ep3/ep3_clone_relics_morkov.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "morkovConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(morkov, "morkov")
