uwo = Creature:new {
	objectName = "",
	customName = "Uwo Poal",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED + CONVERSABLE,
	diet = CARNIVORE,

	templates = {"object/mobile/ep3/ep3_clone_relics_armsdealer_courier.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "courierConvo",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(uwo, "uwo")
