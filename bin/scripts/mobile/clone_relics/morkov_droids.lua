morkov_droids = Creature:new {
	objectName = "",
	customName = "Super Battle Droid",
	socialGroup = "morkov",
	pvpFaction = "",
	faction = "",
	npcStats = {51, 112, 8193, 103, 431, 178, 3043, 3546},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/ep3/ep3_clone_relics_super_battle_droid_01.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = {},
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(morkov_droids, "morkov_droids")
