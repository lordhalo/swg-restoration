uwo_combat = Creature:new {
	objectName = "",
	customName = "Uwo Poal",
	socialGroup = "",
	pvpFaction = "",
	faction = "",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/ep3/ep3_clone_relics_armsdealer_courier.iff"},
	lootGroups = {},
	weapons = {"imperial_weapons_light"},
	reactionStf = "@npc_reaction/slang",
	conversationTemplate = "",
	attacks = merge(pistoleermaster)
}

CreatureTemplates:addCreatureTemplate(uwo_combat, "uwo_combat")
