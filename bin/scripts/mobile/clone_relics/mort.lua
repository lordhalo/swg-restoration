mort = Creature:new {
	objectName = "",
	customName = "Mort",
	socialGroup = "townsperson",
	pvpFaction = "townsperson",
	faction = "townsperson",
	npcStats = {35, 87, 4152, 68, 302, 87, 0, 2500},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = PACK,
	optionsBitmask = 264,
	diet = HERBIVORE,

	templates = {"object/mobile/ep3/ep3_clone_relics_mort.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "AvengingMortConvoOne",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(mort, "mort")
