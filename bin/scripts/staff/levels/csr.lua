csr = {
	level = 9,
	name = "csr",
	tag = "SWGRestoration-CSR",
	skills = {
		"admin_base",
		"admin_debug_01",
		"admin_general_admin_01",
		"admin_general_admin_02",
		"admin_player_management_01",
		"admin_server_admin_01"
	}
}

addLevel(csr)
