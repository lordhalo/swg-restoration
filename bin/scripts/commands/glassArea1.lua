GlassArea1Command = {
        name = "glassarea1",

	damageMultiplier = 2.0,
	speedMultiplier = 0.5,
	healthCostMultiplier = 0,
	actionCostMultiplier = 2.25,
	mindCostMultiplier = 0,

	coneAngle = 45,
	coneAction = true,


	poolsToDamage = RANDOM_ATTRIBUTE,

	animationCRC = hashCode("fire_area_medium"),

	combatSpam = "sapblast",

	weaponType = SPECIALHEAVYWEAPON,


	range = -1
}

AddCommand(GlassArea1Command)

