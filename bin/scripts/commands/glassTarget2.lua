GlassTarget2Command = {
        name = "glasstarget2",

	damageMultiplier = 3.0,
	speedMultiplier = 0.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 2.25,
	mindCostMultiplier = 0,


	poolsToDamage = RANDOM_ATTRIBUTE,

	animationCRC = hashCode("fire_3_special_single_light_face"),

	combatSpam = "sapblast",

	weaponType = SPECIALHEAVYWEAPON,


	range = -1
}

AddCommand(GlassTarget2Command)

