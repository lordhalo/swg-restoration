GlassArea2Command = {
        name = "glassarea2",

	damageMultiplier = 3.0,
	speedMultiplier = 0.5,
	healthCostMultiplier = 0,
	actionCostMultiplier = 2.75,
	mindCostMultiplier = 0,

	coneAngle = 45,
	coneAction = true,


	poolsToDamage = RANDOM_ATTRIBUTE,

	animationCRC = hashCode("fire_area_medium"),

	combatSpam = "sapblast",

	weaponType = SPECIALHEAVYWEAPON,


	range = -1
}

AddCommand(GlassArea2Command)

