DisintegrateTargetCommand = {
        name = "disintegratetarget",

	damageMultiplier = 3.0,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 2.25,
	mindCostMultiplier = 2.25,


	poolsToDamage = HEALTH_ATTRIBUTE + ACTION_ATTRIBUTE + MIND_ATTRIBUTE,

	animationCRC = hashCode("fire_3_special_single_light_face"),

	combatSpam = "disintegratetarget",

	stateEffects = {
	  StateEffect( 
		STUN_EFFECT, 
		{}, 
		{ "stun_defense", "resistance_states" }, 
		{ "jedi_state_defense" }, 
		70, 
		100, 
		30 
	  )
	},

	weaponType = SPECIALHEAVYWEAPON,


	range = -1
}

AddCommand(DisintegrateTargetCommand)

