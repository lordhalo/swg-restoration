
object_tangible_tcg_series3_deed_tcg_merr_sonn_jt12_jetpack = object_tangible_tcg_series3_shared_deed_tcg_merr_sonn_jt12_jetpack:new {
    
	templateType = VEHICLEDEED,

	controlDeviceObjectTemplate = "object/intangible/vehicle/tcg_merr_sonn_jt12_jetpack_pcd.iff",
	generatedObjectTemplate = "object/mobile/vehicle/tcg_merr_sonn_jt12_jetpack.iff",

	numberExperimentalProperties = {1, 1, 1},
	experimentalProperties = {"XX", "XX", "SR"},
	experimentalWeights = {1, 1, 1},
	experimentalGroupTitles = {"null", "null", "exp_durability"},
	experimentalSubGroupTitles = {"null", "null", "hit_points"},
	experimentalMin = {0, 0, 1500},
	experimentalMax = {0, 0, 3000},
	experimentalPrecision = {0, 0, 0},
	experimentalCombineType = {0, 0, 1},

}
ObjectTemplates:addTemplate(object_tangible_tcg_series3_deed_tcg_merr_sonn_jt12_jetpack, "object/tangible/tcg/series3/deed_tcg_merr_sonn_jt12_jetpack.iff")
