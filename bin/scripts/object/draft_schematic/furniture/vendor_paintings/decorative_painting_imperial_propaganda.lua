
object_draft_schematic_furniture_vendor_paintings_decorative_painting_imperial_propaganda = object_draft_schematic_furniture_vendor_paintings_shared_decorative_painting_imperial_propaganda:new {
	templateType = DRAFTSCHEMATIC,

	customObjectName = "Imperial Propaganda",

	craftingToolTab = 512, -- (See DraftSchemticImplementation.h)
	complexity = 15,
	size = 2,

	xpType = "crafting_structure_general",
	xp = 80,

	assemblySkill = "structure_assembly",
	experimentingSkill = "structure_experimentation",
	customizationSkill = "structure_customization",

	customizationOptions = {},
	customizationStringNames = {},
	customizationDefaults = {},

	ingredientTemplateNames = {"craft_furniture_ingredients_n", "craft_furniture_ingredients_n", "craft_furniture_ingredients_n"},
	ingredientTitleNames = {"frame", "canvas", "paints"},
	ingredientSlotType = {0, 0, 0},
	resourceTypes = {"metal", "hide", "petrochem_inert_polymer"},
	resourceQuantities = {50, 50, 40},
	contribution = {100, 100, 100},


	targetTemplate = "object/tangible/tcg/series1/decorative_painting_imperial_propaganda.iff",

	additionalTemplates = {
	}
}

ObjectTemplates:addTemplate(object_draft_schematic_vendor_paintings_bestine_decorative_painting_imperial_propaganda, "object/draft_schematic/furniture/vendor_paintings/decorative_painting_imperial_propaganda.iff")
