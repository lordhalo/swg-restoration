
object_draft_schematic_armor_cybernetic_appearance_s01_legs = object_draft_schematic_armor_shared_cybernetic_appearance_s01_legs :new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "Cybernetic Legs",

   craftingToolTab = 2, -- (See DraftSchemticImplementation.h)
   complexity = 16, 
   size = 3, 

   xpType = "mechanic_general", 
   xp = 760, 

   assemblySkill = "cyber_assembly", 
   experimentingSkill = "cyber_experimentation", 
   customizationSkill = "armor_customization", 

   customizationOptions = {2, 1},
   customizationStringNames = {"/private/index_color_1", "/private/index_color_2"},
   customizationDefaults = {7, 0},

   ingredientTemplateNames = {"craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n"},
   ingredientTitleNames = {"auxilary_coverage", "body", "liner", "hardware_and_attachments", "binding_and_reinforcement", "armor", "load_bearing_harness", "reinforcement"},
   ingredientSlotType = {0, 0, 0, 0, 0, 0, 1, 1, 1},
   resourceTypes = {"copper_mythra", "steel_carbonite", "fiberplast_corellia", "aluminum_linksteel", "petrochem_inert_polymer", "object/tangible/component/armor/shared_armor_core_recon.iff", "object/tangible/component/clothing/shared_synthetic_cloth.iff", "object/tangible/component/clothing/shared_reinforced_fiber_panels.iff"},
   resourceQuantities = {100, 100, 50, 60, 40, 2, 1, 1},
   contribution = {100, 100, 100, 100, 100, 100, 100, 100},


   targetTemplate = "object/tangible/wearables/cybernetic/cybernetic_crafted_s01_legs.iff",

   additionalTemplates = {},

	skillMods = {	    
		{"slope_move", 35}
	}

}
ObjectTemplates:addTemplate(object_draft_schematic_armor_cybernetic_appearance_s01_legs, "object/draft_schematic/armor/cybernetic_appearance_s01_legs.iff")
