object_draft_schematic_chemistry_instant_stimpack_stimpack_a = object_draft_schematic_chemistry_instant_stimpack_shared_stimpack_a:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "Instant Stimpack - A",

   craftingToolTab = 64, -- (See DraftSchemticImplementation.h)
   complexity = 18, 
   size = 3, 

   xpType = "crafting_medicine_general", 
   xp = 30, 

   assemblySkill = "medicine_assembly", 
   experimentingSkill = "medicine_experimentation", 
   customizationSkill = "medicine_customization", 

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_chemical_ingredients_n", "craft_chemical_ingredients_n"},
   ingredientTitleNames = {"organic_element", "inorganic_element"},
   ingredientSlotType = {0, 0},
   resourceTypes = {"organic", "inorganic"},
   resourceQuantities = {8, 8},
   contribution = {100, 100},


   targetTemplate = "object/tangible/medicine/instant_stimpack/stimpack_a.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_chemistry_instant_stimpack_stimpack_a, "object/draft_schematic/chemistry/instant_stimpack/stimpack_a.iff")
