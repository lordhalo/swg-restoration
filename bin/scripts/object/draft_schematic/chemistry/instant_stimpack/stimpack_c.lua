object_draft_schematic_chemistry_instant_stimpack_stimpack_c = object_draft_schematic_chemistry_instant_stimpack_shared_stimpack_c:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "Instant Stimpack - C",

   craftingToolTab = 64, -- (See DraftSchemticImplementation.h)
   complexity = 18, 
   size = 3, 

   xpType = "crafting_medicine_general", 
   xp = 50, 

   assemblySkill = "medicine_assembly", 
   experimentingSkill = "medicine_experimentation", 
   customizationSkill = "medicine_customization", 

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_chemical_ingredients_n", "craft_chemical_ingredients_n", "craft_chemical_ingredients_n", "craft_chemical_ingredients_n", "craft_chemical_ingredients_n"},
   ingredientTitleNames = {"organic_element", "inorganic_element", "delivery_medium", "drug_duration_compound", "drug_strength_compound"},
   ingredientSlotType = {0, 0, 1, 1, 1},
   resourceTypes = {"organic", "inorganic", "object/tangible/component/chemistry/shared_liquid_delivery_suspension.iff", "object/tangible/component/chemistry/shared_release_mechanism_duration.iff", "object/tangible/component/chemistry/shared_biologic_effect_controller.iff"},
   resourceQuantities = {14, 14, 1, 1, 1},
   contribution = {100, 100, 100, 100, 100},


   targetTemplate = "object/tangible/medicine/instant_stimpack/stimpack_c.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_chemistry_instant_stimpack_stimpack_c, "object/draft_schematic/chemistry/instant_stimpack/stimpack_c.iff")
