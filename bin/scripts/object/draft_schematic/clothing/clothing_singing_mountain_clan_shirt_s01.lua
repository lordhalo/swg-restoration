
object_draft_schematic_clothing_clothing_singing_mountain_clan_shirt_s01 = object_draft_schematic_clothing_shared_clothing_singing_mountain_clan_shirt_s01:new {
   
   templateType = DRAFTSCHEMATIC,

   customObjectName = "Singing Mountain Clan Striker Shroud",
   craftingToolTab = 2, -- (See DraftSchemticImplementation.h)
   complexity = 45, 
   size = 4, 

   xpType = "crafting_clothing", 
   xp = 550, 

   assemblySkill = "clothing_assembly", 
   experimentingSkill = "clothing_experimentation", 
   customizationSkill = "clothing_customization", 

   customizationOptions = {2, 1},
   customizationStringNames = {"/private/index_color_1", "/private/index_color_2"},
   customizationDefaults = {0, 0},

   ingredientTemplateNames = {"craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n", "craft_clothing_ingredients_n"},
   ingredientTitleNames = {"skirt_material", "trim", "extra_trim", "hardware_and_attachments"},
   ingredientSlotType = {1, 1, 1, 1},
   resourceTypes = {"object/tangible/component/clothing/shared_synthetic_cloth.iff", "object/tangible/component/clothing/trim.iff", "object/tangible/component/clothing/trim.iff", "copper_beyrllius"},
   resourceQuantities = {2, 1, 1, 35},
   contribution = {100, 100, 100, 100},


   targetTemplate = "object/tangible/wearables/dress/singing_mountain_clan_shirt_s01.iff",

   additionalTemplates = {
             }
}

ObjectTemplates:addTemplate(object_draft_schematic_clothing_clothing_singing_mountain_clan_shirt_s01, "object/draft_schematic/clothing/clothing_singing_mountain_clan_shirt_s01.iff")
