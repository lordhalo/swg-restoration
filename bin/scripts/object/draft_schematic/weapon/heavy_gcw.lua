object_draft_schematic_weapon_heavy_gcw = object_draft_schematic_weapon_shared_heavy_gcw:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "GCW Launcher",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 50, 
   size = 4, 

   xpType = "crafting_weapons_general", 
   xp = 770, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization", 
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_munition_ingredients_n", "craft_munition_ingredients_n", "craft_munition_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"frame_assembly", "receiver_assembly", "grip_assembly", "ignition_pulse_generator", "launch_tubes", "target_tracker", "barrel", "stock","power_supply"},
   ingredientSlotType = {0, 0, 0, 1, 0, 1, 1, 1, 3},
   resourceTypes = {"iron", "metal", "metal", "object/tangible/component/weapon/shared_chemical_dispersion_mechanism.iff", "steel", "object/tangible/component/weapon/shared_scope_weapon.iff", "object/tangible/component/weapon/shared_blaster_rifle_barrel.iff", "object/tangible/component/weapon/shared_stock.iff", "object/tangible/component/weapon/shared_geonosian_power_cube.iff"},
   resourceQuantities = {150, 40, 25, 1, 170, 1, 1, 1, 1},
   contribution = {100, 100, 100, 100, 100, 100, 100, 100, 100},


   targetTemplate = "object/weapon/ranged/heavy/launcher_gcw.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_heavy_gcw, "object/draft_schematic/weapon/heavy_gcw.iff")
