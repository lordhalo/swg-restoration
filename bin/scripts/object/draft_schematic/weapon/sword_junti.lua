object_draft_schematic_weapon_sword_junti = object_draft_schematic_weapon_shared_sword_junti:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "Junti Mace",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 20, 
   size = 3, 

   xpType = "crafting_weapons_general", 
   xp = 110, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization", 
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"grip_unit", "conductive_strike_face", "power_cell_brackets", "charge_accumulator", "reinforcement_core", "power_supply"},
   ingredientSlotType = {0, 0, 0, 0, 1, 1},
   resourceTypes = {"copper_polysteel", "steel", "metal", "crystalline_byrothsis", "object/tangible/component/weapon/shared_sword_core.iff", "object/tangible/loot/dungeon/death_watch_bunker/shared_mandalorian_hardening_agent.iff"},
   resourceQuantities = {18, 18, 10, 6, 1, 1},
   contribution = {100, 100, 100, 100, 100, 100},


   targetTemplate = "object/weapon/melee/sword/sword_mace_junti.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_sword_junti, "object/draft_schematic/weapon/sword_junti.iff")
