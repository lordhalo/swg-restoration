object_draft_schematic_weapon_2h_sword_gcw = object_draft_schematic_weapon_shared_2h_sword_gcw:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "GCW 2H Sword",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 30, 
   size = 4, 

   xpType = "crafting_weapons_general", 
   xp = 280, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization", 
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"grip_unit", "reactive_striking_surface", "power_cell_brackets", "reinforcement_core", "power_supply"},
   ingredientSlotType = {0, 0, 0, 1, 3},
   resourceTypes = {"iron_kammris", "metal", "copper", "object/tangible/component/weapon/shared_reinforcement_core.iff", "object/tangible/component/weapon/shared_geonosian_power_cube.iff"},
   resourceQuantities = {75, 40, 24, 1, 1},
   contribution = {100, 100, 100, 100, 100},


   targetTemplate = "object/weapon/melee/2h_sword/2h_sword_gcw.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_2h_sword_gcw, "object/draft_schematic/weapon/2h_sword_gcw.iff")
