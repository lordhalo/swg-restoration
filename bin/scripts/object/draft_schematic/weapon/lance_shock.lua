
object_draft_schematic_weapon_lance_shock = object_draft_schematic_weapon_shared_lance_shock:new {


   templateType = DRAFTSCHEMATIC,

   customObjectName = "Shock Lance",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 30, 
   size = 4, 

   xpType = "crafting_weapons_general", 
   xp = 220, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization",
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"lance_shaft", "vibro_unit_and_power_cell_brackets", "grip", "vibration_generator", "axe_head", "power_supply"},
   ingredientSlotType = {0, 0, 0, 1, 0, 3},
   resourceTypes = {"steel_ditanium", "copper", "metal_nonferrous", "object/tangible/component/weapon/shared_vibro_unit.iff", "steel_duralloy", "object/tangible/loot/dungeon/death_watch_bunker/shared_mandalorian_hardening_agent.iff"},
   resourceQuantities = {55, 25, 15, 1, 15, 1},
   contribution = {100, 100, 100, 100, 100, 100},


   targetTemplate = "object/weapon/melee/polearm/lance_shock.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_lance_shock, "object/draft_schematic/weapon/lance_shock.iff")
