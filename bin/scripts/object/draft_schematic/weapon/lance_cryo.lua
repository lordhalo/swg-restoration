object_draft_schematic_weapon_lance_cryo = object_draft_schematic_weapon_shared_lance_cryo:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "Cryo Lance",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 17, 
   size = 4, 

   xpType = "crafting_weapons_general", 
   xp = 130, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization", 
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"lance_shaft", "vibro_unit_and_power_cell_brackets", "grip", "vibration_generator", "power_supply"},
   ingredientSlotType = {0, 0, 0, 1, 3},
   resourceTypes = {"steel_ditanium", "copper_polysteel", "metal", "object/tangible/component/weapon/shared_vibro_unit.iff", "object/tangible/component/weapon/shared_geonosian_power_cube.iff"},
   resourceQuantities = {38, 17, 6, 1, 1},
   contribution = {100, 100, 100, 100, 1},



   targetTemplate = "object/weapon/melee/polearm/lance_cryo.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_lance_cryo, "object/draft_schematic/weapon/lance_cryo.iff")
