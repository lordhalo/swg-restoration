object_draft_schematic_weapon_unarmed_gcw = object_draft_schematic_weapon_shared_unarmed_gcw:new {

   templateType = DRAFTSCHEMATIC,

   customObjectName = "GCW Knuckler",

   craftingToolTab = 1, -- (See DraftSchemticImplementation.h)
   complexity = 25, 
   size = 2, 

   xpType = "crafting_weapons_general", 
   xp = 65, 

   assemblySkill = "weapon_assembly", 
   experimentingSkill = "weapon_experimentation", 
   customizationSkill = "weapon_customization", 
   disableFactoryRun = true,  

   customizationOptions = {},
   customizationStringNames = {},
   customizationDefaults = {},

   ingredientTemplateNames = {"craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n", "craft_weapon_ingredients_n"},
   ingredientTitleNames = {"grip_unit", "strike_face", "vibro_unit_and_power_cell_brackets", "power_cell_socket", "feather_core", "power_supply"},
   ingredientSlotType = {0, 0, 0, 0, 1, 3},
   resourceTypes = {"metal_ferrous", "steel", "metal", "copper", "object/tangible/component/weapon/shared_feather_core.iff", "object/tangible/component/weapon/shared_geonosian_power_cube.iff"},
   resourceQuantities = {12, 8, 8, 4, 1, 1},
   contribution = {100, 100, 100, 100, 100, 100},


   targetTemplate = "object/weapon/melee/special/unarmed_gcw.iff",

   additionalTemplates = {
             }

}
ObjectTemplates:addTemplate(object_draft_schematic_weapon_unarmed_gcw, "object/draft_schematic/weapon/unarmed_gcw.iff")
