object_tangible_medicine_instant_stimpack_stimpack_a = object_tangible_medicine_instant_stimpack_shared_stimpack_a:new {
	--gameObjectType = 8241,
	--templateType = INSTANTSTIMPACK,
	--useCount = 11,
	--effectiveness = 100,
	--medicineClass = STIM_D,

	gameObjectType = 8241,
	templateType = INSTANTSTIMPACK,
	useCount = 15,
	effectiveness = 100,

	numberExperimentalProperties = {1, 1, 2, 2, 1},
	experimentalProperties = {"XX", "XX", "OQ", "PE", "OQ", "UT", "XX"},
	experimentalWeights = {1, 1, 2, 1, 2, 1, 1},
	experimentalGroupTitles = {"null", "null", "exp_effectiveness", "null", "null"},
	experimentalSubGroupTitles = {"null", "null", "power", "charges", "hitpoints"},
	experimentalMin = {0, 0, 80, 10, 1000},
	experimentalMax = {0, 0, 120, 20, 1000},
	experimentalPrecision = {0, 0, 0, 0, 0},
	experimentalCombineType = {0, 0, 1, 1, 4},
}

ObjectTemplates:addTemplate(object_tangible_medicine_instant_stimpack_stimpack_a, "object/tangible/medicine/instant_stimpack/stimpack_a.iff")
