object_tangible_medicine_instant_stimpack_stimpack_d = object_tangible_medicine_instant_stimpack_shared_stimpack_d:new {
	--gameObjectType = 8241,
	--templateType = INSTANTSTIMPACK,
	--useCount = 11,
	--effectiveness = 100,
	--medicineClass = STIM_D,

	gameObjectType = 8241,
	templateType = INSTANTSTIMPACK,
	useCount = 15,
	effectiveness = 1000,

	numberExperimentalProperties = {1, 1, 2, 2, 1},
	experimentalProperties = {"XX", "XX", "OQ", "PE", "OQ", "UT", "XX"},
	experimentalWeights = {1, 1, 2, 1, 2, 1, 1},
	experimentalGroupTitles = {"null", "null", "exp_effectiveness", "null", "null"},
	experimentalSubGroupTitles = {"null", "null", "power", "charges", "hitpoints"},
	experimentalMin = {0, 0, 780, 40, 1000},
	experimentalMax = {0, 0, 820, 50, 1000},
	experimentalPrecision = {0, 0, 0, 0, 0},
	experimentalCombineType = {0, 0, 1, 1, 4},
}

ObjectTemplates:addTemplate(object_tangible_medicine_instant_stimpack_stimpack_d, "object/tangible/medicine/instant_stimpack/stimpack_d.iff")
