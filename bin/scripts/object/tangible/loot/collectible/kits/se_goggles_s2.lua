object_tangible_loot_collectible_kits_se_goggles_s2 = object_tangible_loot_collectible_kits_shared_se_goggles_s2 :new {
		
	customName = "Special Edition Goggles Style 2 Kit",

	templateType = LOOTKIT,
	
	gameObjectType = 8233,
	
	--These are used to determine which components are necessary in the loot kit to finish the item
	collectibleComponents = {"object/tangible/loot/collectible/collectible_parts/orange_rug_adhesive.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_dye.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_patches.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_01.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_02.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_03.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_04.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_05.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_06.iff", "object/tangible/loot/collectible/collectible_parts/orange_rug_thread_07.iff"},
	
	collectibleReward =  {"object/tangible/loot/collectible/collectible_rewards/orange_rug.iff"},
	
	deleteComponents = 1,
	
	--These are used to display to the player which components he already added. Same order as above is used
	attributes = {"orange_rug_adhesive","orange_rug_dye","orange_rug_cloth_patches","orange_rug_thread_01","orange_rug_thread_02","orange_rug_thread_03","orange_rug_thread_04","orange_rug_thread_05","orange_rug_thread_06","orange_rug_thread_07"}
}

ObjectTemplates:addTemplate(object_tangible_loot_collectible_kits_se_goggles_s2 , "object/tangible/loot/collectible/kits/se_goggles_s2.iff")
