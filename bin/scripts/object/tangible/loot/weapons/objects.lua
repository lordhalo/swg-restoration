object_tangible_loot_weapons_shared_polearm_cryo_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_polearm_cryo_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_polearm_cryo_schematic, "object/tangible/loot/weapons/shared_polearm_cryo_schematic.iff")

object_tangible_loot_weapons_shared_pistol_kyd21_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_pistol_kyd21_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_pistol_kyd21_schematic, "object/tangible/loot/weapons/shared_pistol_kyd21_schematic.iff")

object_tangible_loot_weapons_shared_massassi_knuckler_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_massassi_knuckler_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_massassi_knuckler_schematic, "object/tangible/loot/weapons/shared_massassi_knuckler_schematic.iff")

object_tangible_loot_weapons_shared_carbine_e5_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_carbine_e5_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_carbine_e5_schematic, "object/tangible/loot/weapons/shared_carbine_e5_schematic.iff")

object_tangible_loot_weapons_shared_heavy_carbonite_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_heavy_carbonite_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_heavy_carbonite_schematic, "object/tangible/loot/weapons/shared_heavy_carbonite_schematic.iff")


object_tangible_loot_weapons_shared_rifle_dl19_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_rifle_dl19_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_rifle_dl19_schematic, "object/tangible/loot/weapons/shared_rifle_dl19_schematic.iff")

object_tangible_loot_weapons_shared_carbine_9118_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_carbine_9118_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_carbine_9118_schematic, "object/tangible/loot/weapons/shared_carbine_9118_schematic.iff")

object_tangible_loot_weapons_shared_heavy_cmfrag_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_heavy_cmfrag_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_heavy_cmfrag_schematic, "object/tangible/loot/weapons/shared_heavy_cmfrag_schematic.iff")

object_tangible_loot_weapons_shared_sword_junti_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_sword_junti_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_sword_junti_schematic, "object/tangible/loot/weapons/shared_sword_junti_schematic.iff")

object_tangible_loot_weapons_shared_lance_shock_schematic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/weapons/shared_lance_shock_schematic.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_weapons_shared_lance_shock_schematic, "object/tangible/loot/weapons/shared_lance_shock_schematic.iff")
