object_tangible_loot_weapons_rifle_dl19_schematic = object_tangible_loot_weapons_shared_rifle_dl19_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/rifle_dl19.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_weapons_rifle_dl19_schematic, "object/tangible/loot/weapons/rifle_dl19_schematic.iff")
