object_tangible_loot_weapons_carbine_9118_schematic = object_tangible_loot_weapons_shared_carbine_9118_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/carbine_9118.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_weapons_carbine_9118_schematic, "object/tangible/loot/weapons/carbine_9118_schematic.iff")
