object_tangible_loot_weapons_pistol_kyd21_schematic = object_tangible_loot_weapons_shared_pistol_kyd21_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/pistol_kyd21.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_weapons_pistol_kyd21_schematic, "object/tangible/loot/weapons/pistol_kyd21_schematic.iff")
