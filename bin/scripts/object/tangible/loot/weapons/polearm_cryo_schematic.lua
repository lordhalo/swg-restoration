object_tangible_loot_weapons_polearm_cryo_schematic = object_tangible_loot_weapons_shared_polearm_cryo_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/polearm_cryo.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_weapons_polearm_cryo_schematic, "object/tangible/loot/weapons/polearm_cryo_schematic.iff")
