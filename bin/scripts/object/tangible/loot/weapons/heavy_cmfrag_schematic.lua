object_tangible_loot_weapons_heavy_cmfrag_schematic = object_tangible_loot_weapons_shared_heavy_cmfrag_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/heavy_cmfrag.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_weapons_heavy_cmfrag_schematic, "object/tangible/loot/weapons/heavy_cmfrag_schematic.iff")
