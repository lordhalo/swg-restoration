object_tangible_loot_factional_schematic_imperial_stormtrooper_helmet_schematic = object_tangible_loot_factional_schematic_shared_imperial_stormtrooper_helmet_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_armorsmith_master",
	targetDraftSchematic = "object/draft_schematic/armor/armor_appearance_stormtrooper_helmet.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_imperial_stormtrooper_helmet_schematic, "object/tangible/loot/factional_schematic/imperial_stormtrooper_helmet_schematic.iff")
