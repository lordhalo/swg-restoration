object_tangible_loot_factional_schematic_carbine_gcw = object_tangible_loot_factional_schematic_shared_carbine_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/carbine_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_carbine_gcw, "object/tangible/loot/factional_schematic/carbine_gcw.iff")
