object_tangible_loot_factional_schematic_rebel_marine_gloves_schematic = object_tangible_loot_factional_schematic_shared_rebel_marine_gloves_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_armorsmith_master",
	targetDraftSchematic = "object/draft_schematic/armor/armor_appearance_rebel_marine_gloves.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_rebel_marine_gloves_schematic, "object/tangible/loot/factional_schematic/rebel_marine_gloves_schematic.iff")
