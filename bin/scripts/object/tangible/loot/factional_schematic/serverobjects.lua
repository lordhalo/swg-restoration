--Children folder includes

-- Server Objects

-- Weapons
includeFile("tangible/loot/factional_schematic/unarmed_gcw.lua")
includeFile("tangible/loot/factional_schematic/sword_gcw.lua")
includeFile("tangible/loot/factional_schematic/2h_sword_gcw.lua")
includeFile("tangible/loot/factional_schematic/polearm_gcw.lua")
includeFile("tangible/loot/factional_schematic/rifle_gcw.lua")
includeFile("tangible/loot/factional_schematic/pistol_gcw.lua")
includeFile("tangible/loot/factional_schematic/carbine_gcw.lua")
includeFile("tangible/loot/factional_schematic/heavy_gcw.lua")
-- Rebel Assault

includeFile("tangible/loot/factional_schematic/rebel_assault_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_chest_plate_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_assault_bracer_r_schematic.lua")

-- Rebel Marine

includeFile("tangible/loot/factional_schematic/rebel_marine_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_chest_plate_rebel_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_marine_bracer_r_schematic.lua")

-- Rebel Battle

includeFile("tangible/loot/factional_schematic/rebel_battle_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_chest_plate_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/rebel_battle_bracer_r_schematic.lua")

-- Assault Trooper

includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_utility_belt_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_chest_plate_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_assault_trooper_bracer_r_schematic.lua")

-- Scout Trooper

includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_utility_belt_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_chest_plate_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_scout_trooper_bracer_r_schematic.lua")

-- Storm Trooper

includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_utility_belt_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_helmet_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_chest_plate_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_leggings_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_boots_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_gloves_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_bicep_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_bicep_r_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_bracer_l_schematic.lua")
includeFile("tangible/loot/factional_schematic/imperial_stormtrooper_bracer_r_schematic.lua")

