object_tangible_loot_factional_schematic_2h_sword_gcw = object_tangible_loot_factional_schematic_shared_2h_sword_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent =  "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/2h_sword_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_2h_sword_gcw, "object/tangible/loot/factional_schematic/2h_sword_gcw.iff")
