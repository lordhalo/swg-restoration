object_tangible_loot_factional_schematic_sword_gcw = object_tangible_loot_factional_schematic_shared_sword_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/sword_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_sword_gcw, "object/tangible/loot/factional_schematic/sword_gcw.iff")
