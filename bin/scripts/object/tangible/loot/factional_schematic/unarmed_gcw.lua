object_tangible_loot_factional_schematic_unarmed_gcw = object_tangible_loot_factional_schematic_shared_unarmed_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/unarmed_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_unarmed_gcw, "object/tangible/loot/factional_schematic/unarmed_gcw.iff")
