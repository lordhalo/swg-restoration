object_tangible_loot_factional_schematic_heavy_gcw = object_tangible_loot_factional_schematic_shared_heavy_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/heavy_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_heavy_gcw, "object/tangible/loot/factional_schematic/heavy_gcw.iff")
