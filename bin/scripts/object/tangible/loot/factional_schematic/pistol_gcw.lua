object_tangible_loot_factional_schematic_pistol_gcw = object_tangible_loot_factional_schematic_shared_pistol_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/pistol_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_pistol_gcw, "object/tangible/loot/factional_schematic/pistol_gcw.iff")
