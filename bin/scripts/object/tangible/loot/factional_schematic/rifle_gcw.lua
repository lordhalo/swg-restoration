object_tangible_loot_factional_schematic_rifle_gcw = object_tangible_loot_factional_schematic_shared_rifle_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/rifle_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_rifle_gcw, "object/tangible/loot/factional_schematic/rifle_gcw.iff")
