object_tangible_loot_factional_schematic_polearm_gcw = object_tangible_loot_factional_schematic_shared_polearm_gcw:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/polearm_gcw.iff",
	targetUseCount = 1,
}

ObjectTemplates:addTemplate(object_tangible_loot_factional_schematic_polearm_gcw, "object/tangible/loot/factional_schematic/polearm_gcw.iff")
