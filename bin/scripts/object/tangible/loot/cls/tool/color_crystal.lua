object_tangible_loot_cls_tool_color_crystal = object_tangible_loot_cls_tool_shared_color_crystal:new {
	templateType = LOOTKIT,

	

	gameObjectType = 8233,

	

	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/cc_token_1of5.iff","object/tangible/loot/cls/token/cc_token_2of5.iff","object/tangible/loot/cls/token/cc_token_3of5.iff","object/tangible/loot/cls/token/cc_token_4of5.iff","object/tangible/loot/cls/token/cc_token_5of5.iff"},

	

	collectibleReward =  {"object/tangible/loot/cls/reward/color_token.iff"},

	

	deleteComponents = 1,

	

	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"cc_1","cc_2","cc_3","cc_4","cc_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_color_crystal, "object/tangible/loot/cls/tool/color_crystal.iff")
