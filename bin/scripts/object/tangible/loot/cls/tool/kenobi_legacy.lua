object_tangible_loot_cls_tool_kenobi_legacy = object_tangible_loot_cls_tool_shared_kenobi_legacy:new {
	templateType = LOOTKIT,

	

	gameObjectType = 8233,

	

	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/kenobi_token_one.iff","object/tangible/loot/cls/token/kenobi_token_one.iff","object/tangible/loot/cls/token/kenobi_token_one.iff","object/tangible/loot/cls/token/kenobi_token_one.iff","object/tangible/loot/cls/token/kenobi_token_one.iff"},

	

	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_dark_s01.iff"},

	

	deleteComponents = 1,

	

	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"kenobi_one","kenobi_two","kenobi_three","kenobi_four","kenobi_five"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_kenobi_legacy, "object/tangible/loot/cls/tool/kenobi_legacy.iff")
