object_tangible_loot_cls_tool_goggles_s4 = object_tangible_loot_cls_tool_shared_goggles_s4:new {
	templateType = LOOTKIT,

	

	gameObjectType = 8233,

	

	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/g4_token_1of5.iff","object/tangible/loot/cls/token/g4_token_2of5.iff","object/tangible/loot/cls/token/g4_token_3of5.iff","object/tangible/loot/cls/token/g4_token_4of5.iff","object/tangible/loot/cls/token/g4_token_5of5.iff"},

	

	collectibleReward =  {"object/tangible/wearables/goggles/goggles_s04.iff"},

	

	deleteComponents = 1,

	

	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"g_s4_1","g_s4_2","g_s4_3","g_s4_4","g_s4_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_goggles_s4, "object/tangible/loot/cls/tool/goggles_s4.iff")