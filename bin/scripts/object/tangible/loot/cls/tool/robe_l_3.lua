object_tangible_loot_cls_tool_robe_l_3 = object_tangible_loot_cls_tool_shared_robe_l_3:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rl3_token_1of5.iff","object/tangible/loot/cls/token/rl3_token_2of5.iff","object/tangible/loot/cls/token/rl3_token_3of5.iff","object/tangible/loot/cls/token/rl3_token_4of5.iff","object/tangible/loot/cls/token/rl3_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_light2_s03.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same orler as above is used

	attributes = {"rl3_1","rl3_2","rl3_3","rl3_4","rl3_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_l_3, "object/tangible/loot/cls/tool/robe_l_3.iff")
