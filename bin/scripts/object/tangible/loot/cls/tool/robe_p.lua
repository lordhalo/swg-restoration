object_tangible_loot_cls_tool_robe_p = object_tangible_loot_cls_tool_shared_robe_p:new {
	templateType = LOOTKIT,

	

	gameObjectType = 8233,

	

	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rp_token_1of5.iff","object/tangible/loot/cls/token/rp_token_2of5.iff","object/tangible/loot/cls/token/rp_token_3of5.iff","object/tangible/loot/cls/token/rp_token_4of5.iff","object/tangible/loot/cls/token/rp_token_5of5.iff"},

	

	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_padawan.iff"},

	

	deleteComponents = 1,

	

	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"rp_1","rp_2","rp_3","rp_4","rp_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_p, "object/tangible/loot/cls/tool/robe_p.iff")