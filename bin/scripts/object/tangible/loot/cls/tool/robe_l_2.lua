object_tangible_loot_cls_tool_robe_l_2 = object_tangible_loot_cls_tool_shared_robe_l_2:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rl2_token_1of5.iff","object/tangible/loot/cls/token/rl2_token_2of5.iff","object/tangible/loot/cls/token/rl2_token_3of5.iff","object/tangible/loot/cls/token/rl2_token_4of5.iff","object/tangible/loot/cls/token/rl2_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_light2_s02.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same orler as above is used

	attributes = {"rl2_1","rl2_2","rl2_3","rl2_4","rl2_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_l_2, "object/tangible/loot/cls/tool/robe_l_2.iff")
