object_tangible_loot_cls_tool_imperial_research_ranged = object_tangible_loot_cls_tool_shared_imperial_research_ranged:new {
	templateType = LOOTKIT,

	

	gameObjectType = 8233,

	

	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/imperial_research_ranged_01.iff","object/tangible/loot/cls/token/imperial_research_ranged_02.iff","object/tangible/loot/cls/token/imperial_research_ranged_03.iff","object/tangible/loot/cls/token/imperial_research_ranged_04iff","object/tangible/loot/cls/token/imperial_research_ranged_05.iff"},

	

	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_padawan.iff"},

	

	deleteComponents = 1,

	

	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"imperial_research_ranged_01","imperial_research_ranged_01","imperial_research_ranged_01","imperial_research_ranged_01","imperial_research_ranged_01"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_imperial_research_ranged, "object/tangible/loot/cls/tool/imperial_research_ranged.iff")
