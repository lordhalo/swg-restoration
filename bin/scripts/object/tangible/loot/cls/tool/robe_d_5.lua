object_tangible_loot_cls_tool_robe_d_5 = object_tangible_loot_cls_tool_shared_robe_d_5:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rd5_token_1of5.iff","object/tangible/loot/cls/token/rd5_token_2of5.iff","object/tangible/loot/cls/token/rd5_token_3of5.iff","object/tangible/loot/cls/token/rd5_token_4of5.iff","object/tangible/loot/cls/token/rd5_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_sith2_s05.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"rd5_1","rd5_2","rd5_3","rd5_4","rd5_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_d_5, "object/tangible/loot/cls/tool/robe_d_5.iff")
