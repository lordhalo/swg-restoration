object_tangible_loot_cls_tool_robe_d_3 = object_tangible_loot_cls_tool_shared_robe_d_3:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rd3_token_1of5.iff","object/tangible/loot/cls/token/rd3_token_2of5.iff","object/tangible/loot/cls/token/rd3_token_3of5.iff","object/tangible/loot/cls/token/rd3_token_4of5.iff","object/tangible/loot/cls/token/rd3_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_sith2_s03.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"rd3_1","rd3_2","rd3_3","rd3_4","rd3_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_d_3, "object/tangible/loot/cls/tool/robe_d_3.iff")
