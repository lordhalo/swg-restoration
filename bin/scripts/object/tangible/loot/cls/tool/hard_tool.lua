object_tangible_loot_cls_tool_hard_tool = object_tangible_loot_cls_tool_shared_hard_tool:new {
	templateType = LOOTKIT,

	gameObjectType = 8233,

	--These are used to determine which components are necessary in the loot kit to finish the item
	collectibleComponents = {"object/tangible/loot/cls/token/h_token_1of5.iff","object/tangible/loot/cls/token/h_token_2of5.iff","object/tangible/loot/cls/token/h_token_3of5.iff","object/tangible/loot/cls/token/h_token_4of5.iff","object/tangible/loot/cls/token/h_token_5of5.iff"},
	
	collectibleReward =  {"object/tangible/loot/cls/reward/hard_box.iff"},
	
	deleteComponents = 1,
	
	--These are used to display to the player which components he already added. Same order as above is used
	
	attributes = {"h_1","h_2","h_3","h_4","h_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_hard_tool, "object/tangible/loot/cls/tool/hard_tool.iff")
