object_tangible_loot_cls_tool_robe_l_4 = object_tangible_loot_cls_tool_shared_robe_l_4:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rl4_token_1of5.iff","object/tangible/loot/cls/token/rl4_token_2of5.iff","object/tangible/loot/cls/token/rl4_token_3of5.iff","object/tangible/loot/cls/token/rl4_token_4of5.iff","object/tangible/loot/cls/token/rl4_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_light2_s04.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same orler as above is used

	attributes = {"rl4_1","rl4_2","rl4_3","rl4_4","rl4_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_l_4, "object/tangible/loot/cls/tool/robe_l_4.iff")
