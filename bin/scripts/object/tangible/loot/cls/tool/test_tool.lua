object_tangible_loot_cls_tool_test_tool = object_tangible_loot_cls_tool_shared_test_tool:new {
	templateType = LOOTKIT,
	
	gameObjectType = 8233,
	
	--These are used to determine which components are necessary in the loot kit to finish the item
	collectibleComponents = {"object/tangible/loot/cls/token/test_token.iff","object/tangible/loot/cls/token/test_token_1.iff"},
	
	collectibleReward =  {"object/tangible/wearables/goggles/goggles_s01.iff"},
	
	deleteComponents = 1,
	
	--These are used to display to the player which components he already added. Same order as above is used
	attributes = {"test_part_1","test_part_2"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_test_tool, "object/tangible/loot/cls/tool/test_tool.iff")
