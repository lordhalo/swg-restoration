object_tangible_loot_cls_tool_shared_kenobi_legacy = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_kenobi_legacy.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_kenobi_legacy, "object/tangible/loot/cls/tool/shared_kenobi_legacy.iff")

object_tangible_loot_cls_tool_shared_hard_tool = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_hard_tool.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_hard_tool, "object/tangible/loot/cls/tool/shared_hard_tool.iff")

object_tangible_loot_cls_tool_shared_goggles_s1 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s1.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s1, "object/tangible/loot/cls/tool/shared_goggles_s1.iff")

object_tangible_loot_cls_tool_shared_goggles_s2 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s2.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s2, "object/tangible/loot/cls/tool/shared_goggles_s2.iff")

object_tangible_loot_cls_tool_shared_goggles_s3 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s3.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s3, "object/tangible/loot/cls/tool/shared_goggles_s3.iff")

object_tangible_loot_cls_tool_shared_goggles_s4 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s4.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s4, "object/tangible/loot/cls/tool/shared_goggles_s4.iff")

object_tangible_loot_cls_tool_shared_goggles_s5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s5, "object/tangible/loot/cls/tool/shared_goggles_s5.iff")

object_tangible_loot_cls_tool_shared_goggles_s6 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_goggles_s6.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_goggles_s6, "object/tangible/loot/cls/tool/shared_goggles_s6.iff")

object_tangible_loot_cls_tool_shared_robe_p = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_p.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_p, "object/tangible/loot/cls/tool/shared_robe_p.iff")

object_tangible_loot_cls_tool_shared_robe_d_1 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_d_1.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_d_1, "object/tangible/loot/cls/tool/shared_robe_d_1.iff")

object_tangible_loot_cls_tool_shared_robe_d_2 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_d_2.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_d_2, "object/tangible/loot/cls/tool/shared_robe_d_2.iff")

object_tangible_loot_cls_tool_shared_robe_d_3 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_d_3.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_d_3, "object/tangible/loot/cls/tool/shared_robe_d_3.iff")

object_tangible_loot_cls_tool_shared_robe_d_4 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_d_4.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_d_4, "object/tangible/loot/cls/tool/shared_robe_d_4.iff")

object_tangible_loot_cls_tool_shared_robe_d_5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_d_5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_d_5, "object/tangible/loot/cls/tool/shared_robe_d_5.iff")


object_tangible_loot_cls_tool_shared_robe_l_1 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_l_1.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_l_1, "object/tangible/loot/cls/tool/shared_robe_l_1.iff")

object_tangible_loot_cls_tool_shared_robe_l_2 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_l_2.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_l_2, "object/tangible/loot/cls/tool/shared_robe_l_2.iff")

object_tangible_loot_cls_tool_shared_robe_l_3 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_l_3.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_l_3, "object/tangible/loot/cls/tool/shared_robe_l_3.iff")

object_tangible_loot_cls_tool_shared_robe_l_4 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_l_4.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_l_4, "object/tangible/loot/cls/tool/shared_robe_l_4.iff")

object_tangible_loot_cls_tool_shared_robe_l_5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_robe_l_5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_robe_l_5, "object/tangible/loot/cls/tool/shared_robe_l_5.iff")

object_tangible_loot_cls_tool_shared_color_crystal = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/tool/shared_color_crystal.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_tool_shared_color_crystal, "object/tangible/loot/cls/tool/shared_color_crystal.iff")
