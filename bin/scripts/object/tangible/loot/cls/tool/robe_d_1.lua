object_tangible_loot_cls_tool_robe_d_1 = object_tangible_loot_cls_tool_shared_robe_d_1:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rd1_token_1of5.iff","object/tangible/loot/cls/token/rd1_token_2of5.iff","object/tangible/loot/cls/token/rd1_token_3of5.iff","object/tangible/loot/cls/token/rd1_token_4of5.iff","object/tangible/loot/cls/token/rd1_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_sith2_s01.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"rd1_1","rd1_2","rd1_3","rd1_4","rd1_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_d_1, "object/tangible/loot/cls/tool/robe_d_1.iff")
