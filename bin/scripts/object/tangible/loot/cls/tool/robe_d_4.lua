object_tangible_loot_cls_tool_robe_d_4 = object_tangible_loot_cls_tool_shared_robe_d_4:new {
	templateType = LOOTKIT,



	gameObjectType = 8233,



	--These are used to determine which components are necessary in the loot kit to finish the item

	collectibleComponents = {"object/tangible/loot/cls/token/rd4_token_1of5.iff","object/tangible/loot/cls/token/rd4_token_2of5.iff","object/tangible/loot/cls/token/rd4_token_3of5.iff","object/tangible/loot/cls/token/rd4_token_4of5.iff","object/tangible/loot/cls/token/rd4_token_5of5.iff"},



	collectibleReward =  {"object/tangible/wearables/robe/robe_jedi_sith2_s04.iff"},



	deleteComponents = 1,



	--These are used to display to the player which components he already added. Same order as above is used

	attributes = {"rd4_1","rd4_2","rd4_3","rd4_4","rd4_5"}
}

ObjectTemplates:addTemplate(object_tangible_loot_cls_tool_robe_d_4, "object/tangible/loot/cls/tool/robe_d_4.iff")
