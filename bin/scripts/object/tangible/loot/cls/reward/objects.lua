object_tangible_loot_cls_reward_shared_hard_box = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/cls/reward/shared_hard_box.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_reward_shared_hard_box, "object/tangible/loot/cls/reward/shared_hard_box.iff")

object_tangible_loot_cls_reward_shared_rebel_ranged_research = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/loot/cls/reward/shared_rebel_ranged_research.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_reward_shared_rebel_ranged_research, "object/tangible/loot/cls/reward/shared_rebel_ranged_research.iff")
