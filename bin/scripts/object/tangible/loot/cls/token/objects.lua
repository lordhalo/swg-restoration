
object_tangible_loot_cls_token_shared_h_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_h_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_h_token_5of5, "object/tangible/loot/cls/token/shared_h_token_5of5.iff")

object_tangible_loot_cls_token_shared_h_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_h_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_h_token_4of5, "object/tangible/loot/cls/token/shared_h_token_4of5.iff")

object_tangible_loot_cls_token_shared_h_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_h_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_h_token_3of5, "object/tangible/loot/cls/token/shared_h_token_3of5.iff")

object_tangible_loot_cls_token_shared_h_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_h_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_h_token_2of5, "object/tangible/loot/cls/token/shared_h_token_2of5.iff")

object_tangible_loot_cls_token_shared_h_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_h_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_h_token_1of5, "object/tangible/loot/cls/token/shared_h_token_1of5.iff")



object_tangible_loot_cls_token_shared_g2_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g2_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g2_token_5of5, "object/tangible/loot/cls/token/shared_g2_token_5of5.iff")

object_tangible_loot_cls_token_shared_g2_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g2_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g2_token_4of5, "object/tangible/loot/cls/token/shared_g2_token_4of5.iff")

object_tangible_loot_cls_token_shared_g2_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g2_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g2_token_3of5, "object/tangible/loot/cls/token/shared_g2_token_3of5.iff")

object_tangible_loot_cls_token_shared_g2_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g2_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g2_token_2of5, "object/tangible/loot/cls/token/shared_g2_token_2of5.iff")

object_tangible_loot_cls_token_shared_g2_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g2_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g2_token_1of5, "object/tangible/loot/cls/token/shared_g2_token_1of5.iff")

object_tangible_loot_cls_token_shared_g1_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g1_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g1_token_5of5, "object/tangible/loot/cls/token/shared_g1_token_5of5.iff")

object_tangible_loot_cls_token_shared_g1_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g1_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g1_token_4of5, "object/tangible/loot/cls/token/shared_g1_token_4of5.iff")

object_tangible_loot_cls_token_shared_g1_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g1_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g1_token_3of5, "object/tangible/loot/cls/token/shared_g1_token_3of5.iff")

object_tangible_loot_cls_token_shared_g1_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g1_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g1_token_2of5, "object/tangible/loot/cls/token/shared_g1_token_2of5.iff")

object_tangible_loot_cls_token_shared_g1_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g1_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g1_token_1of5, "object/tangible/loot/cls/token/shared_g1_token_1of5.iff")

object_tangible_loot_cls_token_shared_g3_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g3_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g3_token_5of5, "object/tangible/loot/cls/token/shared_g3_token_5of5.iff")

object_tangible_loot_cls_token_shared_g3_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g3_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g3_token_4of5, "object/tangible/loot/cls/token/shared_g3_token_4of5.iff")

object_tangible_loot_cls_token_shared_g3_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g3_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g3_token_3of5, "object/tangible/loot/cls/token/shared_g3_token_3of5.iff")

object_tangible_loot_cls_token_shared_g3_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g3_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g3_token_2of5, "object/tangible/loot/cls/token/shared_g3_token_2of5.iff")

object_tangible_loot_cls_token_shared_g3_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g3_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g3_token_1of5, "object/tangible/loot/cls/token/shared_g3_token_1of5.iff")

object_tangible_loot_cls_token_shared_g4_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g4_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g4_token_5of5, "object/tangible/loot/cls/token/shared_g4_token_5of5.iff")

object_tangible_loot_cls_token_shared_g4_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g4_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g4_token_4of5, "object/tangible/loot/cls/token/shared_g4_token_4of5.iff")

object_tangible_loot_cls_token_shared_g4_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g4_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g4_token_3of5, "object/tangible/loot/cls/token/shared_g4_token_3of5.iff")

object_tangible_loot_cls_token_shared_g4_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g4_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g4_token_2of5, "object/tangible/loot/cls/token/shared_g4_token_2of5.iff")

object_tangible_loot_cls_token_shared_g4_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g4_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g4_token_1of5, "object/tangible/loot/cls/token/shared_g4_token_1of5.iff")

object_tangible_loot_cls_token_shared_g5_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g5_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g5_token_5of5, "object/tangible/loot/cls/token/shared_g5_token_5of5.iff")

object_tangible_loot_cls_token_shared_g5_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g5_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g5_token_4of5, "object/tangible/loot/cls/token/shared_g5_token_4of5.iff")

object_tangible_loot_cls_token_shared_g5_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g5_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g5_token_3of5, "object/tangible/loot/cls/token/shared_g5_token_3of5.iff")

object_tangible_loot_cls_token_shared_g5_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g5_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g5_token_2of5, "object/tangible/loot/cls/token/shared_g5_token_2of5.iff")

object_tangible_loot_cls_token_shared_g5_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g5_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g5_token_1of5, "object/tangible/loot/cls/token/shared_g5_token_1of5.iff")

object_tangible_loot_cls_token_shared_g6_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g6_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g6_token_5of5, "object/tangible/loot/cls/token/shared_g6_token_5of5.iff")

object_tangible_loot_cls_token_shared_g6_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g6_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g6_token_4of5, "object/tangible/loot/cls/token/shared_g6_token_4of5.iff")

object_tangible_loot_cls_token_shared_g6_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g6_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g6_token_3of5, "object/tangible/loot/cls/token/shared_g6_token_3of5.iff")

object_tangible_loot_cls_token_shared_g6_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g6_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g6_token_2of5, "object/tangible/loot/cls/token/shared_g6_token_2of5.iff")

object_tangible_loot_cls_token_shared_g6_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_g6_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_g6_token_1of5, "object/tangible/loot/cls/token/shared_g6_token_1of5.iff")

object_tangible_loot_cls_token_shared_rp_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rp_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rp_token_5of5, "object/tangible/loot/cls/token/shared_rp_token_5of5.iff")

object_tangible_loot_cls_token_shared_rp_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rp_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rp_token_4of5, "object/tangible/loot/cls/token/shared_rp_token_4of5.iff")

object_tangible_loot_cls_token_shared_rp_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rp_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rp_token_3of5, "object/tangible/loot/cls/token/shared_rp_token_3of5.iff")

object_tangible_loot_cls_token_shared_rp_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rp_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rp_token_2of5, "object/tangible/loot/cls/token/shared_rp_token_2of5.iff")

object_tangible_loot_cls_token_shared_rp_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rp_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rp_token_1of5, "object/tangible/loot/cls/token/shared_rp_token_1of5.iff")

object_tangible_loot_cls_token_shared_rd1_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd1_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd1_token_5of5, "object/tangible/loot/cls/token/shared_rd1_token_5of5.iff")

object_tangible_loot_cls_token_shared_rd1_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd1_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd1_token_4of5, "object/tangible/loot/cls/token/shared_rd1_token_4of5.iff")

object_tangible_loot_cls_token_shared_rd1_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd1_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd1_token_3of5, "object/tangible/loot/cls/token/shared_rd1_token_3of5.iff")

object_tangible_loot_cls_token_shared_rd1_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd1_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd1_token_2of5, "object/tangible/loot/cls/token/shared_rd1_token_2of5.iff")

object_tangible_loot_cls_token_shared_rd1_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd1_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd1_token_1of5, "object/tangible/loot/cls/token/shared_rd1_token_1of5.iff")

object_tangible_loot_cls_token_shared_rd2_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd2_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd2_token_5of5, "object/tangible/loot/cls/token/shared_rd2_token_5of5.iff")

object_tangible_loot_cls_token_shared_rd2_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd2_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd2_token_4of5, "object/tangible/loot/cls/token/shared_rd2_token_4of5.iff")

object_tangible_loot_cls_token_shared_rd2_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd2_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd2_token_3of5, "object/tangible/loot/cls/token/shared_rd2_token_3of5.iff")

object_tangible_loot_cls_token_shared_rd2_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd2_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd2_token_2of5, "object/tangible/loot/cls/token/shared_rd2_token_2of5.iff")

object_tangible_loot_cls_token_shared_rd2_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd2_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd2_token_1of5, "object/tangible/loot/cls/token/shared_rd2_token_1of5.iff")

object_tangible_loot_cls_token_shared_rd3_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd3_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd3_token_5of5, "object/tangible/loot/cls/token/shared_rd3_token_5of5.iff")

object_tangible_loot_cls_token_shared_rd3_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd3_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd3_token_4of5, "object/tangible/loot/cls/token/shared_rd3_token_4of5.iff")

object_tangible_loot_cls_token_shared_rd3_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd3_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd3_token_3of5, "object/tangible/loot/cls/token/shared_rd3_token_3of5.iff")

object_tangible_loot_cls_token_shared_rd3_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd3_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd3_token_2of5, "object/tangible/loot/cls/token/shared_rd3_token_2of5.iff")

object_tangible_loot_cls_token_shared_rd3_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd3_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd3_token_1of5, "object/tangible/loot/cls/token/shared_rd3_token_1of5.iff")

object_tangible_loot_cls_token_shared_rd4_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd4_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd4_token_5of5, "object/tangible/loot/cls/token/shared_rd4_token_5of5.iff")

object_tangible_loot_cls_token_shared_rd4_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd4_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd4_token_4of5, "object/tangible/loot/cls/token/shared_rd4_token_4of5.iff")

object_tangible_loot_cls_token_shared_rd4_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd4_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd4_token_3of5, "object/tangible/loot/cls/token/shared_rd4_token_3of5.iff")

object_tangible_loot_cls_token_shared_rd4_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd4_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd4_token_2of5, "object/tangible/loot/cls/token/shared_rd4_token_2of5.iff")

object_tangible_loot_cls_token_shared_rd4_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd4_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd4_token_1of5, "object/tangible/loot/cls/token/shared_rd4_token_1of5.iff")

object_tangible_loot_cls_token_shared_rd5_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd5_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd5_token_5of5, "object/tangible/loot/cls/token/shared_rd5_token_5of5.iff")

object_tangible_loot_cls_token_shared_rd5_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd5_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd5_token_4of5, "object/tangible/loot/cls/token/shared_rd5_token_4of5.iff")

object_tangible_loot_cls_token_shared_rd5_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd5_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd5_token_3of5, "object/tangible/loot/cls/token/shared_rd5_token_3of5.iff")

object_tangible_loot_cls_token_shared_rd5_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd5_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd5_token_2of5, "object/tangible/loot/cls/token/shared_rd5_token_2of5.iff")

object_tangible_loot_cls_token_shared_rd5_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rd5_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rd5_token_1of5, "object/tangible/loot/cls/token/shared_rd5_token_1of5.iff")

object_tangible_loot_cls_token_shared_rl1_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl1_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl1_token_5of5, "object/tangible/loot/cls/token/shared_rl1_token_5of5.iff")

object_tangible_loot_cls_token_shared_rl1_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl1_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl1_token_4of5, "object/tangible/loot/cls/token/shared_rl1_token_4of5.iff")

object_tangible_loot_cls_token_shared_rl1_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl1_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl1_token_3of5, "object/tangible/loot/cls/token/shared_rl1_token_3of5.iff")

object_tangible_loot_cls_token_shared_rl1_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl1_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl1_token_2of5, "object/tangible/loot/cls/token/shared_rl1_token_2of5.iff")

object_tangible_loot_cls_token_shared_rl1_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl1_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl1_token_1of5, "object/tangible/loot/cls/token/shared_rl1_token_1of5.iff")

object_tangible_loot_cls_token_shared_rl2_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl2_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl2_token_5of5, "object/tangible/loot/cls/token/shared_rl2_token_5of5.iff")

object_tangible_loot_cls_token_shared_rl2_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl2_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl2_token_4of5, "object/tangible/loot/cls/token/shared_rl2_token_4of5.iff")

object_tangible_loot_cls_token_shared_rl2_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl2_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl2_token_3of5, "object/tangible/loot/cls/token/shared_rl2_token_3of5.iff")

object_tangible_loot_cls_token_shared_rl2_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl2_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl2_token_2of5, "object/tangible/loot/cls/token/shared_rl2_token_2of5.iff")

object_tangible_loot_cls_token_shared_rl2_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl2_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl2_token_1of5, "object/tangible/loot/cls/token/shared_rl2_token_1of5.iff")

object_tangible_loot_cls_token_shared_rl3_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl3_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl3_token_5of5, "object/tangible/loot/cls/token/shared_rl3_token_5of5.iff")

object_tangible_loot_cls_token_shared_rl3_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl3_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl3_token_4of5, "object/tangible/loot/cls/token/shared_rl3_token_4of5.iff")

object_tangible_loot_cls_token_shared_rl3_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl3_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl3_token_3of5, "object/tangible/loot/cls/token/shared_rl3_token_3of5.iff")

object_tangible_loot_cls_token_shared_rl3_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl3_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl3_token_2of5, "object/tangible/loot/cls/token/shared_rl3_token_2of5.iff")

object_tangible_loot_cls_token_shared_rl3_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl3_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl3_token_1of5, "object/tangible/loot/cls/token/shared_rl3_token_1of5.iff")

object_tangible_loot_cls_token_shared_rl4_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl4_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl4_token_5of5, "object/tangible/loot/cls/token/shared_rl4_token_5of5.iff")

object_tangible_loot_cls_token_shared_rl4_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl4_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl4_token_4of5, "object/tangible/loot/cls/token/shared_rl4_token_4of5.iff")

object_tangible_loot_cls_token_shared_rl4_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl4_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl4_token_3of5, "object/tangible/loot/cls/token/shared_rl4_token_3of5.iff")

object_tangible_loot_cls_token_shared_rl4_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl4_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl4_token_2of5, "object/tangible/loot/cls/token/shared_rl4_token_2of5.iff")

object_tangible_loot_cls_token_shared_rl4_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl4_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl4_token_1of5, "object/tangible/loot/cls/token/shared_rl4_token_1of5.iff")

object_tangible_loot_cls_token_shared_rl5_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl5_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl5_token_5of5, "object/tangible/loot/cls/token/shared_rl5_token_5of5.iff")

object_tangible_loot_cls_token_shared_rl5_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl5_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl5_token_4of5, "object/tangible/loot/cls/token/shared_rl5_token_4of5.iff")

object_tangible_loot_cls_token_shared_rl5_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl5_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl5_token_3of5, "object/tangible/loot/cls/token/shared_rl5_token_3of5.iff")

object_tangible_loot_cls_token_shared_rl5_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl5_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl5_token_2of5, "object/tangible/loot/cls/token/shared_rl5_token_2of5.iff")

object_tangible_loot_cls_token_shared_rl5_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_rl5_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_rl5_token_1of5, "object/tangible/loot/cls/token/shared_rl5_token_1of5.iff")

object_tangible_loot_cls_token_shared_cc_token_5of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_cc_token_5of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_cc_token_5of5, "object/tangible/loot/cls/token/shared_cc_token_5of5.iff")

object_tangible_loot_cls_token_shared_cc_token_4of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_cc_token_4of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_cc_token_4of5, "object/tangible/loot/cls/token/shared_cc_token_4of5.iff")

object_tangible_loot_cls_token_shared_cc_token_3of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_cc_token_3of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_cc_token_3of5, "object/tangible/loot/cls/token/shared_cc_token_3of5.iff")

object_tangible_loot_cls_token_shared_cc_token_2of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_cc_token_2of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_cc_token_2of5, "object/tangible/loot/cls/token/shared_cc_token_2of5.iff")

object_tangible_loot_cls_token_shared_cc_token_1of5 = SharedTangibleObjectTemplate:new {
clientTemplateFileName = "object/tangible/loot/cls/token/shared_cc_token_1of5.iff"

}

ObjectTemplates:addClientTemplate(object_tangible_loot_cls_token_shared_cc_token_1of5, "object/tangible/loot/cls/token/shared_cc_token_1of5.iff")
