
--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_armor_kit_composite_battleworn = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_armor_kit_composite_battleworn.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_armor_kit_composite_battleworn, "object/tangible/tcg/series7/shared_shared_armor_kit_composite_battleworn.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_armor_kit_composite_camo = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_armor_kit_composite_camo.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_armor_kit_composite_camo, "object/tangible/tcg/series7/shared_shared_armor_kit_composite_camo.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_object_broken_ball_turret = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_object_broken_ball_turret.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_object_broken_ball_turret, "object/tangible/tcg/series7/shared_shared_combine_object_broken_ball_turret.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_object_eweb_decor = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_object_eweb_decor.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_object_eweb_decor, "object/tangible/tcg/series7/shared_shared_combine_object_eweb_decor.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_object_gunship_blueprint = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_object_gunship_blueprint.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_object_gunship_blueprint, "object/tangible/tcg/series7/shared_shared_combine_object_gunship_blueprint.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_object_tie_canopy = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_object_tie_canopy.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_object_tie_canopy, "object/tangible/tcg/series7/shared_shared_combine_object_tie_canopy.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_object_xwing_wing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_object_xwing_wing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_object_xwing_wing, "object/tangible/tcg/series7/shared_shared_combine_object_xwing_wing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_combine_reward_deed_republic_gunship = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_combine_reward_deed_republic_gunship.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_combine_reward_deed_republic_gunship, "object/tangible/tcg/series7/shared_shared_combine_reward_deed_republic_gunship.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_consumable_rocket_launcher = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_consumable_rocket_launcher.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_consumable_rocket_launcher, "object/tangible/tcg/series7/shared_shared_consumable_rocket_launcher.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_01, "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_02, "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_03 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_03.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_imperial_graffiti_03, "object/tangible/tcg/series7/shared_shared_decal_imperial_graffiti_03.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_01, "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_02, "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_03 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_03.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decal_rebel_graffiti_03, "object/tangible/tcg/series7/shared_shared_decal_rebel_graffiti_03.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_decorative_atst_chair = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_decorative_atst_chair.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_decorative_atst_chair, "object/tangible/tcg/series7/shared_shared_decorative_atst_chair.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_diorama_firgrin_dan = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_diorama_firgrin_dan.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_diorama_firgrin_dan, "object/tangible/tcg/series7/shared_shared_diorama_firgrin_dan.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_diorama_max_rebo = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_diorama_max_rebo.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_diorama_max_rebo, "object/tangible/tcg/series7/shared_shared_diorama_max_rebo.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_familiar_deed_hutt_fighter = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_familiar_deed_hutt_fighter.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_familiar_deed_hutt_fighter, "object/tangible/tcg/series7/shared_shared_familiar_deed_hutt_fighter.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_familiar_deed_tie_fighter = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_familiar_deed_tie_fighter.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_familiar_deed_tie_fighter, "object/tangible/tcg/series7/shared_shared_familiar_deed_tie_fighter.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_familiar_deed_xwing_fighter = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_familiar_deed_xwing_fighter.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_familiar_deed_xwing_fighter, "object/tangible/tcg/series7/shared_shared_familiar_deed_xwing_fighter.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_multi_item_imperial_graffiti_set = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_multi_item_imperial_graffiti_set.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_multi_item_imperial_graffiti_set, "object/tangible/tcg/series7/shared_shared_multi_item_imperial_graffiti_set.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_multi_item_rebel_graffiti_set = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_multi_item_rebel_graffiti_set.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_multi_item_rebel_graffiti_set, "object/tangible/tcg/series7/shared_shared_multi_item_rebel_graffiti_set.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_painting_commando = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_painting_commando.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_painting_commando, "object/tangible/tcg/series7/shared_shared_painting_commando.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_painting_lando_poster = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_painting_lando_poster.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_painting_lando_poster, "object/tangible/tcg/series7/shared_shared_painting_lando_poster.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_structure_deed_commando_bunker = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_structure_deed_commando_bunker.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_structure_deed_commando_bunker, "object/tangible/tcg/series7/shared_shared_structure_deed_commando_bunker.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_structure_deed_vehicle_garage = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_structure_deed_vehicle_garage.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_structure_deed_vehicle_garage, "object/tangible/tcg/series7/shared_shared_structure_deed_vehicle_garage.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_structure_deed_vip_bunker = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_structure_deed_vip_bunker.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_structure_deed_vip_bunker, "object/tangible/tcg/series7/shared_shared_structure_deed_vip_bunker.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_backpack_armored = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_backpack_armored.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_backpack_armored, "object/tangible/tcg/series7/shared_shared_wearable_backpack_armored.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_backpack_recon = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_backpack_recon.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_backpack_recon, "object/tangible/tcg/series7/shared_shared_wearable_backpack_recon.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_gold_cape = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_gold_cape.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_gold_cape, "object/tangible/tcg/series7/shared_shared_wearable_gold_cape.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_purple_cape = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_purple_cape.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_purple_cape, "object/tangible/tcg/series7/shared_shared_wearable_purple_cape.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_sash_embroidered = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_sash_embroidered.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_sash_embroidered, "object/tangible/tcg/series7/shared_shared_wearable_sash_embroidered.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series7_shared_shared_wearable_sash_handmade = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series7/shared_shared_wearable_sash_handmade.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series7_shared_shared_wearable_sash_handmade, "object/tangible/tcg/series7/shared_shared_wearable_sash_handmade.iff")
