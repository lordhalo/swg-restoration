
--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_bas_relief = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_bas_relief.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_bas_relief, "object/tangible/tcg/series1/shared_decorative_bas_relief.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_chon_bust = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_chon_bust.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_chon_bust, "object/tangible/tcg/series1/shared_decorative_chon_bust.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_computer_console_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_computer_console_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_computer_console_01, "object/tangible/tcg/series1/shared_decorative_computer_console_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_computer_console_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_computer_console_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_computer_console_02, "object/tangible/tcg/series1/shared_decorative_computer_console_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_display_case_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_display_case_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_display_case_01, "object/tangible/tcg/series1/shared_decorative_display_case_01.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_display_case_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_display_case_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_display_case_02, "object/tangible/tcg/series1/shared_decorative_display_case_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_dooku_bust = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_dooku_bust.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_dooku_bust, "object/tangible/tcg/series1/shared_decorative_dooku_bust.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_drink_dispenser = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_drink_dispenser.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_drink_dispenser, "object/tangible/tcg/series1/shared_decorative_drink_dispenser.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_fish_tank = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_fish_tank.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_fish_tank, "object/tangible/tcg/series1/shared_decorative_fish_tank.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_indoor_fountain_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_indoor_fountain_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_indoor_fountain_01, "object/tangible/tcg/series1/shared_decorative_indoor_fountain_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_indoor_fountain_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_indoor_fountain_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_indoor_fountain_02, "object/tangible/tcg/series1/shared_decorative_indoor_fountain_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_indoor_garden_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_indoor_garden_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_indoor_garden_01, "object/tangible/tcg/series1/shared_decorative_indoor_garden_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_indoor_garden_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_indoor_garden_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_indoor_garden_02, "object/tangible/tcg/series1/shared_decorative_indoor_garden_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_jedi_library_bookshelf = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_jedi_library_bookshelf.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_jedi_library_bookshelf, "object/tangible/tcg/series1/shared_decorative_jedi_library_bookshelf.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_painting_alliance_propaganda = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_painting_alliance_propaganda.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_painting_alliance_propaganda, "object/tangible/tcg/series1/shared_decorative_painting_alliance_propaganda.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_painting_darth_vader = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_painting_darth_vader.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_painting_darth_vader, "object/tangible/tcg/series1/shared_decorative_painting_darth_vader.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_painting_imperial_propaganda = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_painting_imperial_propaganda.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_painting_imperial_propaganda, "object/tangible/tcg/series1/shared_decorative_painting_imperial_propaganda.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_painting_jedi_crest = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_painting_jedi_crest.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_painting_jedi_crest, "object/tangible/tcg/series1/shared_decorative_painting_jedi_crest.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_painting_trooper = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_painting_trooper.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_painting_trooper, "object/tangible/tcg/series1/shared_decorative_painting_trooper.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_statuette_darth_vader = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_statuette_darth_vader.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_statuette_darth_vader, "object/tangible/tcg/series1/shared_decorative_statuette_darth_vader.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_decorative_statuette_princess_leia = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_decorative_statuette_princess_leia.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_decorative_statuette_princess_leia, "object/tangible/tcg/series1/shared_decorative_statuette_princess_leia.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_hans_hydrospanner = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_hans_hydrospanner.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_hans_hydrospanner, "object/tangible/tcg/series1/shared_consumable_hans_hydrospanner.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_keelkana_tooth = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_keelkana_tooth.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_keelkana_tooth, "object/tangible/tcg/series1/shared_consumable_keelkana_tooth.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_lepese_dictionary = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_lepese_dictionary.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_lepese_dictionary, "object/tangible/tcg/series1/shared_consumable_lepese_dictionary.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_mandalorian_strongbox = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_mandalorian_strongbox.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_mandalorian_strongbox, "object/tangible/tcg/series1/shared_consumable_mandalorian_strongbox.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_nuna_ball_advertisement = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_nuna_ball_advertisement.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_nuna_ball_advertisement, "object/tangible/tcg/series1/shared_consumable_nuna_ball_advertisement.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_radtrooper_badge = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_radtrooper_badge.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_radtrooper_badge, "object/tangible/tcg/series1/shared_consumable_radtrooper_badge.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_consumable_versafunction88_datapad = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_consumable_versafunction88_datapad.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_consumable_versafunction88_datapad, "object/tangible/tcg/series1/shared_consumable_versafunction88_datapad.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_greeter_deed_ewok = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_greeter_deed_ewok.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_greeter_deed_ewok, "object/tangible/tcg/series1/shared_greeter_deed_ewok.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_greeter_deed_gungan = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_greeter_deed_gungan.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_greeter_deed_gungan, "object/tangible/tcg/series1/shared_greeter_deed_gungan.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_greeter_deed_jawa = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_greeter_deed_jawa.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_greeter_deed_jawa, "object/tangible/tcg/series1/shared_greeter_deed_jawa.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_greeter_deed_meatlump = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_greeter_deed_meatlump.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_greeter_deed_meatlump, "object/tangible/tcg/series1/shared_greeter_deed_meatlump.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_greeter_deed_serving_droid = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_greeter_deed_serving_droid.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_greeter_deed_serving_droid, "object/tangible/tcg/series1/shared_greeter_deed_serving_droid.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_house_capacity_beru_whitesuns_cookbook = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_house_capacity_beru_whitesuns_cookbook.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_house_capacity_beru_whitesuns_cookbook, "object/tangible/tcg/series1/shared_house_capacity_beru_whitesuns_cookbook.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_house_capacity_housecleaning_kit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_house_capacity_housecleaning_kit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_house_capacity_housecleaning_kit, "object/tangible/tcg/series1/shared_house_capacity_housecleaning_kit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_house_capacity_organizational_datapad = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_house_capacity_organizational_datapad.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_house_capacity_organizational_datapad, "object/tangible/tcg/series1/shared_house_capacity_organizational_datapad.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_reusable_buff_item_gorax_ear = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_reusable_buff_item_gorax_ear.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_reusable_buff_item_gorax_ear, "object/tangible/tcg/series1/shared_reusable_buff_item_gorax_ear.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_reusable_buff_item_morgukai_shadow_scroll = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_reusable_buff_item_morgukai_shadow_scroll.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_reusable_buff_item_morgukai_shadow_scroll, "object/tangible/tcg/series1/shared_reusable_buff_item_morgukai_shadow_scroll.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_reusable_buff_item_sanyassan_skull = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_reusable_buff_item_sanyassan_skull.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_reusable_buff_item_sanyassan_skull, "object/tangible/tcg/series1/shared_reusable_buff_item_sanyassan_skull.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_reusable_buff_item_tuskan_talisman = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_reusable_buff_item_tuskan_talisman.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_reusable_buff_item_tuskan_talisman, "object/tangible/tcg/series1/shared_reusable_buff_item_tuskan_talisman.iff")



--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_starship_deed_v_wing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_starship_deed_v_wing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_starship_deed_v_wing, "object/tangible/tcg/series1/shared_starship_deed_v_wing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_structure_barn_ranchhand = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_structure_barn_ranchhand.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_structure_barn_ranchhand, "object/tangible/tcg/series1/shared_structure_barn_ranchhand.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_structure_deed_barn = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_structure_deed_barn.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_structure_deed_barn, "object/tangible/tcg/series1/shared_structure_deed_barn.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_structure_deed_diner = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_structure_deed_diner.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_structure_deed_diner, "object/tangible/tcg/series1/shared_structure_deed_diner.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_target_creature_acklay_display = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_target_creature_acklay_display.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_target_creature_acklay_display, "object/tangible/tcg/series1/shared_target_creature_acklay_display.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_target_creature_deed = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_target_creature_deed.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_target_creature_deed, "object/tangible/tcg/series1/shared_target_creature_deed.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_target_dummy_deed = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_target_dummy_deed.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_target_dummy_deed, "object/tangible/tcg/series1/shared_target_dummy_deed.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vehicle_deed_mechno_chair = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vehicle_deed_mechno_chair.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vehicle_deed_mechno_chair, "object/tangible/tcg/series1/shared_vehicle_deed_mechno_chair.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vehicle_deed_organa_speeder = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vehicle_deed_organa_speeder.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vehicle_deed_organa_speeder, "object/tangible/tcg/series1/shared_vehicle_deed_organa_speeder.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vehicle_deed_podracer_gasgano = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vehicle_deed_podracer_gasgano.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vehicle_deed_podracer_gasgano, "object/tangible/tcg/series1/shared_vehicle_deed_podracer_gasgano.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vehicle_deed_podracer_mawhonic = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vehicle_deed_podracer_mawhonic.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vehicle_deed_podracer_mawhonic, "object/tangible/tcg/series1/shared_vehicle_deed_podracer_mawhonic.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vehicle_deed_sith_speeder = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vehicle_deed_sith_speeder.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vehicle_deed_sith_speeder, "object/tangible/tcg/series1/shared_vehicle_deed_sith_speeder.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_bomarr_monk = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_bomarr_monk.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_bomarr_monk, "object/tangible/tcg/series1/shared_vendor_deed_bomarr_monk.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_ewok = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_ewok.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_ewok, "object/tangible/tcg/series1/shared_vendor_deed_ewok.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_gungan = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_gungan.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_gungan, "object/tangible/tcg/series1/shared_vendor_deed_gungan.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_jawa = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_jawa.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_jawa, "object/tangible/tcg/series1/shared_vendor_deed_jawa.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_meatlump = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_meatlump.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_meatlump, "object/tangible/tcg/series1/shared_vendor_deed_meatlump.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_vendor_deed_serving_droid = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_vendor_deed_serving_droid.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_vendor_deed_serving_droid, "object/tangible/tcg/series1/shared_vendor_deed_serving_droid.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_video_game_table = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_video_game_table.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_video_game_table, "object/tangible/tcg/series1/shared_video_game_table.iff")


--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_arc170_flightsuit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_arc170_flightsuit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_arc170_flightsuit, "object/tangible/tcg/series1/shared_wearable_arc170_flightsuit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_black_corset_dress = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_black_corset_dress.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_black_corset_dress, "object/tangible/tcg/series1/shared_wearable_black_corset_dress.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_glowing_blue_eyes = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_glowing_blue_eyes.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_glowing_blue_eyes, "object/tangible/tcg/series1/shared_wearable_glowing_blue_eyes.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_glowing_red_eyes = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_glowing_red_eyes.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_glowing_red_eyes, "object/tangible/tcg/series1/shared_wearable_glowing_red_eyes.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_imperial_flightsuit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_imperial_flightsuit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_imperial_flightsuit, "object/tangible/tcg/series1/shared_wearable_imperial_flightsuit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_naboo_jacket = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_naboo_jacket.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_naboo_jacket, "object/tangible/tcg/series1/shared_wearable_naboo_jacket.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_orange_flightsuit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_orange_flightsuit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_orange_flightsuit, "object/tangible/tcg/series1/shared_wearable_orange_flightsuit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series1_shared_wearable_snow_jacket = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series1/shared_wearable_snow_jacket.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series1_shared_wearable_snow_jacket, "object/tangible/tcg/series1/shared_wearable_snow_jacket.iff")

