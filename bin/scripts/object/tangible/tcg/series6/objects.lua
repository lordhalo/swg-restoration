
--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_combine_object_battle_droid_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_combine_object_battle_droid_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_combine_object_battle_droid_statuette, "object/tangible/tcg/series6/shared_shared_combine_object_battle_droid_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_combine_object_hk_droids_poster = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_combine_object_hk_droids_poster.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_combine_object_hk_droids_poster, "object/tangible/tcg/series6/shared_shared_combine_object_hk_droids_poster.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_combine_object_hk47_mustafar_diorama = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_combine_object_hk47_mustafar_diorama.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_combine_object_hk47_mustafar_diorama, "object/tangible/tcg/series6/shared_shared_combine_object_hk47_mustafar_diorama.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_combine_object_hk47_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_combine_object_hk47_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_combine_object_hk47_statuette, "object/tangible/tcg/series6/shared_shared_combine_object_hk47_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_consumable_ponda_baba_arm = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_consumable_ponda_baba_arm.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_consumable_ponda_baba_arm, "object/tangible/tcg/series6/shared_shared_consumable_ponda_baba_arm.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_decorative_baby_colo_claw_fish = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_decorative_baby_colo_claw_fish.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_decorative_baby_colo_claw_fish, "object/tangible/tcg/series6/shared_shared_decorative_baby_colo_claw_fish.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_decorative_baby_colo_claw_fishtank = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_decorative_baby_colo_claw_fishtank.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_decorative_baby_colo_claw_fishtank, "object/tangible/tcg/series6/shared_shared_decorative_baby_colo_claw_fishtank.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_decorative_house_lamp = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_decorative_house_lamp.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_decorative_house_lamp, "object/tangible/tcg/series6/shared_shared_decorative_house_lamp.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_decorative_jabba_bed = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_decorative_jabba_bed.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_decorative_jabba_bed, "object/tangible/tcg/series6/shared_shared_decorative_jabba_bed.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_deed_tcg_hk47_jetpack = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_deed_tcg_hk47_jetpack.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_deed_tcg_hk47_jetpack, "object/tangible/tcg/series6/shared_shared_deed_tcg_hk47_jetpack.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_dewback_armor_statue = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_dewback_armor_statue.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_dewback_armor_statue, "object/tangible/tcg/series6/shared_shared_dewback_armor_statue.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_diorama_han_greedo = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_diorama_han_greedo.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_diorama_han_greedo, "object/tangible/tcg/series6/shared_shared_diorama_han_greedo.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_diorama_jedi_council = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_diorama_jedi_council.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_diorama_jedi_council, "object/tangible/tcg/series6/shared_shared_diorama_jedi_council.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_hologram_cloud_city = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_hologram_cloud_city.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_hologram_cloud_city, "object/tangible/tcg/series6/shared_shared_hologram_cloud_city.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_itv_cloud_car = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_itv_cloud_car.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_itv_cloud_car, "object/tangible/tcg/series6/shared_shared_itv_cloud_car.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_mount_deed_gualaar = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_mount_deed_gualaar.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_mount_deed_gualaar, "object/tangible/tcg/series6/shared_shared_mount_deed_gualaar.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_multi_item_baby_colo_claw_set = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_multi_item_baby_colo_claw_set.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_multi_item_baby_colo_claw_set, "object/tangible/tcg/series6/shared_shared_multi_item_baby_colo_claw_set.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_painting_nightsister = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_painting_nightsister.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_painting_nightsister, "object/tangible/tcg/series6/shared_shared_painting_nightsister.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_painting_spined_rancor = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_painting_spined_rancor.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_painting_spined_rancor, "object/tangible/tcg/series6/shared_shared_painting_spined_rancor.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_painting_travel_ad_coruscant = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_painting_travel_ad_coruscant.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_painting_travel_ad_coruscant, "object/tangible/tcg/series6/shared_shared_painting_travel_ad_coruscant.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_painting_travel_ad_ord_mantell = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_painting_travel_ad_ord_mantell.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_painting_travel_ad_ord_mantell, "object/tangible/tcg/series6/shared_shared_painting_travel_ad_ord_mantell.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_painting_winged_quenker = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_painting_winged_quenker.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_painting_winged_quenker, "object/tangible/tcg/series6/shared_shared_painting_winged_quenker.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_reuseable_buff_item_auto_feeder = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_auto_feeder.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_reuseable_buff_item_auto_feeder, "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_auto_feeder.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_reuseable_buff_item_beast_muzzle = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_beast_muzzle.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_reuseable_buff_item_beast_muzzle, "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_beast_muzzle.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_reuseable_buff_item_shock_collar = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_shock_collar.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_reuseable_buff_item_shock_collar, "object/tangible/tcg/series6/shared_shared_reuseable_buff_item_shock_collar.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_statuette_lando = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_statuette_lando.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_statuette_lando, "object/tangible/tcg/series6/shared_shared_statuette_lando.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_structure_deed_emperors_spire = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_structure_deed_emperors_spire.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_structure_deed_emperors_spire, "object/tangible/tcg/series6/shared_shared_structure_deed_emperors_spire.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_structure_deed_rebel_spire = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_structure_deed_rebel_spire.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_structure_deed_rebel_spire, "object/tangible/tcg/series6/shared_shared_structure_deed_rebel_spire.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_wearable_greedos_outfit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_wearable_greedos_outfit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_wearable_greedos_outfit, "object/tangible/tcg/series6/shared_shared_wearable_greedos_outfit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_wearable_guise_of_fire = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_wearable_guise_of_fire.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_wearable_guise_of_fire, "object/tangible/tcg/series6/shared_shared_wearable_guise_of_fire.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series6_shared_shared_wearable_guise_of_ice = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series6/shared_shared_wearable_guise_of_ice.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series6_shared_shared_wearable_guise_of_ice, "object/tangible/tcg/series6/shared_shared_wearable_guise_of_ice.iff")
