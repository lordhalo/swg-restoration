
--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_at_at_blueprint = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_at_at_blueprint.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_at_at_blueprint, "object/tangible/tcg/series5/shared_at_at_blueprint.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_atat_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_atat_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_atat_statuette, "object/tangible/tcg/series5/shared_atat_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_cerimonial_travel_headdresss = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_cerimonial_travel_headdresss.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_cerimonial_travel_headdresss, "object/tangible/tcg/series5/shared_cerimonial_travel_headdresss.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_deathstar_hologram = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_deathstar_hologram.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_deathstar_hologram, "object/tangible/tcg/series5/shared_deathstar_hologram.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_galactic_hunters_poster = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_galactic_hunters_poster.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_galactic_hunters_poster, "object/tangible/tcg/series5/shared_galactic_hunters_poster.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_in_home_rain_storm = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_in_home_rain_storm.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_in_home_rain_storm, "object/tangible/tcg/series5/shared_in_home_rain_storm.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_itv_atat_head = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_itv_atat_head.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_itv_atat_head, "object/tangible/tcg/series5/shared_itv_atat_head.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_jabba_roasting_pit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_jabba_roasting_pit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_jabba_roasting_pit, "object/tangible/tcg/series5/shared_jabba_roasting_pit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_jedi_forms_painting = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_jedi_forms_painting.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_jedi_forms_painting, "object/tangible/tcg/series5/shared_jedi_forms_painting.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_klorri_clan_shield = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_klorri_clan_shield.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_klorri_clan_shield, "object/tangible/tcg/series5/shared_klorri_clan_shield.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_lcd_screen = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_lcd_screen.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_lcd_screen, "object/tangible/tcg/series5/shared_lcd_screen.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_mustafar_diorama = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_mustafar_diorama.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_mustafar_diorama, "object/tangible/tcg/series5/shared_mustafar_diorama.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_nightsister_backpack = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_nightsister_backpack.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_nightsister_backpack, "object/tangible/tcg/series5/shared_nightsister_backpack.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_rots_forearm = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_rots_forearm.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_rots_forearm, "object/tangible/tcg/series5/shared_rots_forearm.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_scurrier_crafted_plant = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_scurrier_crafted_plant.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_scurrier_crafted_plant, "object/tangible/tcg/series5/shared_scurrier_crafted_plant.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_sign_tcg_hanging = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_sign_tcg_hanging.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_sign_tcg_hanging, "object/tangible/tcg/series5/shared_sign_tcg_hanging.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_sign_tcg_standing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_sign_tcg_standing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_sign_tcg_standing, "object/tangible/tcg/series5/shared_sign_tcg_standing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_signal_unit = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_signal_unit.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_signal_unit, "object/tangible/tcg/series5/shared_signal_unit.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_skywalker_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_skywalker_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_skywalker_statuette, "object/tangible/tcg/series5/shared_skywalker_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_theater_poster = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_theater_poster.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_theater_poster, "object/tangible/tcg/series5/shared_theater_poster.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_tie_fighter_chair = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_tie_fighter_chair.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_tie_fighter_chair, "object/tangible/tcg/series5/shared_tie_fighter_chair.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_title_grant = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_title_grant.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_title_grant, "object/tangible/tcg/series5/shared_title_grant.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_trench_run_diorama = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_trench_run_diorama.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_trench_run_diorama, "object/tangible/tcg/series5/shared_trench_run_diorama.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_vader_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_vader_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_vader_statuette, "object/tangible/tcg/series5/shared_vader_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_varactyl_armor_statuette = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_varactyl_armor_statuette.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_varactyl_armor_statuette, "object/tangible/tcg/series5/shared_varactyl_armor_statuette.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_vehicle_deed_xj6_air_speeder = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_vehicle_deed_xj6_air_speeder.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_vehicle_deed_xj6_air_speeder, "object/tangible/tcg/series5/shared_vehicle_deed_xj6_air_speeder.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_vehicle_deed_fg_8t8_podracer = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_vehicle_deed_fg_8t8_podracer.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_vehicle_deed_fg_8t8_podracer, "object/tangible/tcg/series5/shared_vehicle_deed_fg_8t8_podracer.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_shared_vehicle_deed_air2_swoop_speeder = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/shared_vehicle_deed_air2_swoop_speeder.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_shared_vehicle_deed_air2_swoop_speeder, "object/tangible/tcg/series5/shared_vehicle_deed_air2_swoop_speeder.iff")

