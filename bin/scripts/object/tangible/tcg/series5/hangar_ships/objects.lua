
--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_arc170 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_arc170.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_arc170, "object/tangible/tcg/series5/hangar_ships/shared_shared_arc170.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_awing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_awing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_awing, "object/tangible/tcg/series5/hangar_ships/shared_shared_awing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_heavy_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_fighter_heavy_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_heavy_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_fighter_heavy_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_heavy_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_fighter_heavy_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_heavy_02, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_fighter_heavy_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_light_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_fighter_light_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_light_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_fighter_light_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_medium_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_fighter_medium_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_medium_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_fighter_medium_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_medium_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_fighter_medium_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_fighter_medium_02, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_fighter_medium_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_transport = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_black_sun_transport.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_black_sun_transport, "object/tangible/tcg/series5/hangar_ships/shared_shared_black_sun_transport.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_bwing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_bwing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_bwing, "object/tangible/tcg/series5/hangar_ships/shared_shared_bwing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_grievous_starship = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_grievous_starship.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_grievous_starship, "object/tangible/tcg/series5/hangar_ships/shared_shared_grievous_starship.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_heavy_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_heavy_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_heavy_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_heavy_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_heavy_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_heavy_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_heavy_02, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_heavy_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_light_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_light_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_light_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_light_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_light_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_light_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_light_02, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_light_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_medium_01 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_medium_01.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_medium_01, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_medium_01.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_medium_02 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_hutt_fighter_medium_02.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_hutt_fighter_medium_02, "object/tangible/tcg/series5/hangar_ships/shared_shared_hutt_fighter_medium_02.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_imperial_gunboat = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_imperial_gunboat.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_imperial_gunboat, "object/tangible/tcg/series5/hangar_ships/shared_shared_imperial_gunboat.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_jedi_fighter = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_jedi_fighter.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_jedi_fighter, "object/tangible/tcg/series5/hangar_ships/shared_shared_jedi_fighter.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_kse_firespray = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_kse_firespray.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_kse_firespray, "object/tangible/tcg/series5/hangar_ships/shared_shared_kse_firespray.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_naboo_n1 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_naboo_n1.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_naboo_n1, "object/tangible/tcg/series5/hangar_ships/shared_shared_naboo_n1.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_rebel_gunboat = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_rebel_gunboat.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_rebel_gunboat, "object/tangible/tcg/series5/hangar_ships/shared_shared_rebel_gunboat.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_sorosuub = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_sorosuub.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_sorosuub, "object/tangible/tcg/series5/hangar_ships/shared_shared_sorosuub.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_tie_advanced = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_tie_advanced.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_tie_advanced, "object/tangible/tcg/series5/hangar_ships/shared_shared_tie_advanced.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_tie_bomber = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_tie_bomber.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_tie_bomber, "object/tangible/tcg/series5/hangar_ships/shared_shared_tie_bomber.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_tie_fighter = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_tie_fighter.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_tie_fighter, "object/tangible/tcg/series5/hangar_ships/shared_shared_tie_fighter.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_tie_interceptor = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_tie_interceptor.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_tie_interceptor, "object/tangible/tcg/series5/hangar_ships/shared_shared_tie_interceptor.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_tie_oppressor = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_tie_oppressor.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_tie_oppressor, "object/tangible/tcg/series5/hangar_ships/shared_shared_tie_oppressor.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_vt49_decimator = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_vt49_decimator.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_vt49_decimator, "object/tangible/tcg/series5/hangar_ships/shared_shared_vt49_decimator.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_vwing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_vwing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_vwing, "object/tangible/tcg/series5/hangar_ships/shared_shared_vwing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_xwing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_xwing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_xwing, "object/tangible/tcg/series5/hangar_ships/shared_shared_xwing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_y8 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_y8.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_y8, "object/tangible/tcg/series5/hangar_ships/shared_shared_y8.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_ykl37r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_ykl37r.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_ykl37r, "object/tangible/tcg/series5/hangar_ships/shared_shared_ykl37r.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_yt1300 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_yt1300.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_yt1300, "object/tangible/tcg/series5/hangar_ships/shared_shared_yt1300.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_yt2400 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_yt2400.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_yt2400, "object/tangible/tcg/series5/hangar_ships/shared_shared_yt2400.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_ywing = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_ywing.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_ywing, "object/tangible/tcg/series5/hangar_ships/shared_shared_ywing.iff")

--Made with Skyyyr's Wrench Tool 

object_tangible_tcg_series5_hangar_ships_shared_shared_z95 = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/tcg/series5/series5/shared_shared_z95.iff"
}
ObjectTemplates:addClientTemplate(object_tangible_tcg_series5_hangar_ships_shared_shared_z95, "object/tangible/tcg/series5/hangar_ships/shared_shared_z95.iff")
