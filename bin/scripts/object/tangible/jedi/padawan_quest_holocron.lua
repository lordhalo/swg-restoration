object_tangible_jedi_padawan_quest_holocron = object_tangible_jedi_shared_padawan_quest_holocron:new {
	--objectName = "Initiate Quest Holocron 1/1",
	objectName = "@jedi_trials:padawan_holocron",
	objectMenuComponent = "PadawanQuestHolo",
	noTrade = 1
}

ObjectTemplates:addTemplate(object_tangible_jedi_padawan_quest_holocron, "object/tangible/jedi/padawan_quest_holocron.iff")
