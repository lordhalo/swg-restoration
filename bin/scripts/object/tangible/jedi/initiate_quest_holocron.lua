object_tangible_jedi_initiate_quest_holocron = object_tangible_jedi_shared_initiate_quest_holocron:new {
	--objectName = "Initiate Quest Holocron 1/1",
	objectName = "@jedi_trials:force_shrine_n",
	objectMenuComponent = "InitiateQuestHolo",
	noTrade = 1
}

ObjectTemplates:addTemplate(object_tangible_jedi_initiate_quest_holocron, "object/tangible/jedi/initiate_quest_holocron.iff")
