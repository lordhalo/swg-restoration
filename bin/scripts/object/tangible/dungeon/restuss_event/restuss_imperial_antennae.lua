
object_tangible_dungeon_restuss_event_restuss_imperial_antennae = object_tangible_dungeon_restuss_event_shared_restuss_imperial_antennae:new {
	pvpStatusBitmask = ATTACKABLE,
	optionsBitmask = 0,
	maxCondition = 100000,
	faction = "imperial",
	pvpFaction = "imperial",
	-- Damagetypes in WeaponObject
	vulnerability = BLAST,

	-- LIGHT, MEDIUM, HEAVY
	rating = HEAVY,

	kinetic = 90,
	energy = 95,
	electricity = 90,
	stun = 100,
	blast = -1,
	heat = 90,
	cold = 90,
	acid = 90,
	lightSaber =100,

}

ObjectTemplates:addTemplate(object_tangible_dungeon_restuss_event_restuss_imperial_antennae, "object/tangible/dungeon/restuss_event/restuss_imperial_antennae.iff")
