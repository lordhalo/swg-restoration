
--Neutral Belt
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_belt = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_belt.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_belt, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_belt.iff")


--Neutral Bicep Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bicep_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bicep_l.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bicep_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bicep_l.iff")


--Neutral Bicep Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bicep_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bicep_r.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bicep_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bicep_r.iff")


--Neutral Boots
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_boots = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_boots.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_boots, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_boots.iff")


--Neutral Bracer Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bracer_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bracer_r.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bracer_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bracer_r.iff")


--Neutral Bracer Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bracer_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bracer_l.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_bracer_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_bracer_l.iff")


--Neutral Chest Plate
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_chest_plate = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_chest_plate.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_chest_plate, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_chest_plate.iff")


--Neutral Gloves
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_gloves = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_gloves.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_gloves, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_gloves.iff")


--Neutral Helmet
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_helmet = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_helmet.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_helmet, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_helmet.iff")


--Neutral Leggings
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_leggings = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_leggings.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_neutral_s01_leggings, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_neutral_s01_leggings.iff")



--####################################################################################################Imperial Clone Armor############################################################################################################
--####################################################################################################################################################################################################################################


--Imperial Belt
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_belt = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_belt.iff"

}
ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_belt, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_belt.iff")


--Imperial Bicep Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bicep_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bicep_l.iff"

}
ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bicep_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bicep_l.iff")


--Imperial Bicep Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bicep_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bicep_r.iff"

}
ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bicep_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bicep_r.iff")


--Imperial Boots
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_boots = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_boots.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_boots, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_boots.iff")


--Imperial Bracer Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bracer_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bracer_r.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bracer_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bracer_r.iff")


--Imperial Bracer Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bracer_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bracer_l.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_bracer_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_bracer_l.iff")


--Imperial Chest Plate
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_chest_plate = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_chest_plate.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_chest_plate, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_chest_plate.iff")


--Imperial Gloves
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_gloves = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_gloves.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_gloves, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_gloves.iff")


--Imperial Helmet
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_helmet = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_helmet.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_helmet, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_helmet.iff")


--Imperial Leggings
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_leggings = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_leggings.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_imperial_s01_leggings, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_imperial_s01_leggings.iff")


--#########################################################################################Rebel Clone Armor############################################################################################################################
--######################################################################################################################################################################################################################################


--Rebel Belt
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_belt = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_belt.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_belt, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_belt.iff")


--Rebel Bicep Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bicep_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bicep_l.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bicep_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bicep_l.iff")


--Rebel Bicep Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bicep_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bicep_r.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bicep_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bicep_r.iff")


--Rebel Boots
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_boots = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_boots.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_boots, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_boots.iff")


--Rebel Bracer Right
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bracer_r = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bracer_r.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bracer_r, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bracer_r.iff")


--Rebel Bracer Left
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bracer_l = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bracer_l.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_bracer_l, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_bracer_l.iff")


--Rebel Chest Plate
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_chest_plate = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_chest_plate.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_chest_plate, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_chest_plate.iff")


--Rebel Gloves
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_gloves = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_gloves.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_gloves, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_gloves.iff")


--Rebel Helmet
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_helmet = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_helmet.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_helmet, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_helmet.iff")


--Rebel Leggings
object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_leggings = SharedTangibleObjectTemplate:new {
	clientTemplateFileName = "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_leggings.iff"
}

ObjectTemplates:addClientTemplate(object_tangible_wearables_armor_clone_trooper_shared_armor_clone_trooper_rebel_s01_leggings, "object/tangible/wearables/armor/clone_trooper/shared_armor_clone_trooper_rebel_s01_leggings.iff")
