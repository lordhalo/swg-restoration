object_tangible_wearables_armor_marine_armor_marine_gloves = object_tangible_wearables_armor_marine_shared_armor_marine_gloves:new {

	templateType = ARMOROBJECT,

	faction = "Rebel",

	objectMenuComponent = "ArmorObjectMenuComponent", 

	playerRaces = { "object/creature/player/bothan_male.iff",
				"object/creature/player/bothan_female.iff",
				"object/creature/player/human_male.iff",
				"object/creature/player/human_female.iff",
				"object/creature/player/moncal_male.iff",
				"object/creature/player/moncal_female.iff",
				"object/creature/player/rodian_male.iff",
				"object/creature/player/rodian_female.iff",
				"object/creature/player/sullustan_male.iff",
				"object/creature/player/sullustan_female.iff",
				"object/creature/player/twilek_male.iff",
				"object/creature/player/twilek_female.iff",
				"object/creature/player/zabrak_male.iff",
				"object/creature/player/zabrak_female.iff",
				"object/mobile/vendor/aqualish_female.iff",
				"object/mobile/vendor/aqualish_male.iff",
				"object/mobile/vendor/bith_female.iff",
				"object/mobile/vendor/bith_male.iff",
				"object/mobile/vendor/bothan_female.iff",
				"object/mobile/vendor/bothan_male.iff",
				"object/mobile/vendor/devaronian_male.iff",
				"object/mobile/vendor/gran_male.iff",
				"object/mobile/vendor/human_female.iff",
				"object/mobile/vendor/human_male.iff",
				"object/mobile/vendor/ishi_tib_male.iff",
				"object/mobile/vendor/moncal_female.iff",
				"object/mobile/vendor/moncal_male.iff",
				"object/mobile/vendor/nikto_male.iff",
				"object/mobile/vendor/quarren_male.iff",
				"object/mobile/vendor/rodian_female.iff",
				"object/mobile/vendor/rodian_male.iff",
				"object/mobile/vendor/sullustan_female.iff",
				"object/mobile/vendor/sullustan_male.iff",
				"object/mobile/vendor/twilek_female.iff",
				"object/mobile/vendor/twilek_male.iff",
				"object/mobile/vendor/weequay_male.iff",
				"object/mobile/vendor/zabrak_female.iff",
				"object/mobile/vendor/zabrak_male.iff" },

	-- Damage types in WeaponObject
	vulnerability = LIGHTSABER,

	-- These are default Blue Frog stats
	healthEncumbrance = 25,
	actionEncumbrance = 44,
	mindEncumbrance = 25,

	-- LIGHT, MEDIUM, HEAVY
	rating = LIGHT,

	kinetic = 20,
	energy = 20,
	electricity = 20,
	stun = 0,
	blast = 20,
	heat = 20,
	cold = 20,
	acid = 20,
	lightSaber = 0,

	numberExperimentalProperties = {1, 1, 1, 1, 1},
	experimentalProperties = {"XX", "XX", "XX", "OQ", "SR"},
	experimentalWeights = {1, 1, 1, 1, 1},
	experimentalGroupTitles = {"null", "null", "null", "exp_durability", "exp_durability"},
	experimentalSubGroupTitles = {"null", "null", "sockets", "hit_points", "armor_integrity"},
	experimentalMin = {0, 0, 0, 1000, 27500},
	experimentalMax = {0, 0, 0, 1000, 32500},
	experimentalPrecision = {0, 0, 0, 0, 0},
	experimentalCombineType = {0, 0, 0, 0, 0},
}

ObjectTemplates:addTemplate(object_tangible_wearables_armor_marine_armor_marine_gloves, "object/tangible/wearables/armor/marine/armor_marine_gloves.iff")
