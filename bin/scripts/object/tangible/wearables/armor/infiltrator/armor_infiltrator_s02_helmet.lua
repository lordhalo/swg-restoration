object_tangible_wearables_armor_infiltrator_armor_infiltrator_s02_helmet = object_tangible_wearables_armor_infiltrator_shared_armor_infiltrator_s02_helmet:new {
	templateType = ARMOROBJECT,

	playerRaces = { "object/creature/player/human_male.iff",
				"object/creature/player/human_female.iff",
				"object/creature/player/zabrak_male.iff",
				"object/creature/player/zabrak_female.iff",
				"object/mobile/vendor/human_female.iff",
				"object/mobile/vendor/human_male.iff",
				"object/mobile/vendor/zabrak_female.iff",
				"object/mobile/vendor/zabrak_male.iff" },
	
	-- Damage types in WeaponObject
	vulnerability = LIGHTSABER,

	-- These are default Blue Frog stats
	healthEncumbrance = 15,
	actionEncumbrance = 16,
	mindEncumbrance = 225,

	-- LIGHT, MEDIUM, HEAVY
	rating = LIGHT,

	maxCondition = 45000,
	
	kinetic = 30,
	energy = 30,
	electricity = 30,
	stun = 0,
	blast = 30,
	heat = 30,
	cold = 30,
	acid = 0,
	lightSaber = 0,
	
	numberExperimentalProperties = {1, 1, 1, 1, 1},
	experimentalProperties = {"XX", "XX", "XX", "OQ", "SR"},
	experimentalWeights = {1, 1, 1, 1, 1},
	experimentalGroupTitles = {"null", "null", "null", "exp_durability", "exp_durability"},
	experimentalSubGroupTitles = {"null", "null", "sockets", "hit_points", "armor_integrity"},
	experimentalMin = {0, 0, 0, 1000, 27500},
	experimentalMax = {0, 0, 0, 1000, 32500},
	experimentalPrecision = {0, 0, 0, 0, 0},
	experimentalCombineType = {0, 0, 0, 0, 0},
}
ObjectTemplates:addTemplate(object_tangible_wearables_armor_infiltrator_armor_infiltrator_s02_helmet, "object/tangible/wearables/armor/infiltrator/armor_infiltrator_s02_helmet.iff")
