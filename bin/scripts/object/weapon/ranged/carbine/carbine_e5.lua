object_weapon_ranged_carbine_carbine_e5 = object_weapon_ranged_carbine_shared_carbine_e5:new {

	playerRaces = { "object/creature/player/bothan_male.iff",
				"object/creature/player/bothan_female.iff",
				"object/creature/player/human_male.iff",
				"object/creature/player/human_female.iff",
				"object/creature/player/ithorian_male.iff",
				"object/creature/player/ithorian_female.iff",
				"object/creature/player/moncal_male.iff",
				"object/creature/player/moncal_female.iff",
				"object/creature/player/rodian_male.iff",
				"object/creature/player/rodian_female.iff",
				"object/creature/player/sullustan_male.iff",
				"object/creature/player/sullustan_female.iff",
				"object/creature/player/trandoshan_male.iff",
				"object/creature/player/trandoshan_female.iff",
				"object/creature/player/twilek_male.iff",
				"object/creature/player/twilek_female.iff",
				"object/creature/player/wookiee_male.iff",
				"object/creature/player/wookiee_female.iff",
				"object/creature/player/zabrak_male.iff",
				"object/creature/player/zabrak_female.iff" },

	-- RANGEDATTACK, MELEEATTACK, FORCEATTACK, TRAPATTACK, GRENADEATTACK, HEAVYACIDBEAMATTACK, 
	-- HEAVYLIGHTNINGBEAMATTACK, HEAVYPARTICLEBEAMATTACK, HEAVYROCKETLAUNCHERATTACK, HEAVYLAUNCHERATTACK
	attackType = RANGEDATTACK,
	weaponType = CARBINEWEAPON,

	-- ENERGY, KINETIC, ELECTRICITY, STUN, BLAST, HEAT, COLD, ACID, FORCE, LIGHTSABER
	damageType = ENERGY,
	

	-- NONE, LIGHT, MEDIUM, HEAVY
	armorPiercing = MEDIUM,

	-- combat_rangedspecialize_bactarifle, combat_rangedspecialize_rifle, combat_rangedspecialize_pistol, combat_rangedspecialize_heavy, combat_rangedspecialize_carbine
	-- combat_meleespecialize_unarmed, combat_meleespecialize_twohand, combat_meleespecialize_polearm, combat_meleespecialize_onehand, combat_general,
	-- combat_meleespecialize_twohandlightsaber, combat_meleespecialize_polearmlightsaber, combat_meleespecialize_onehandlightsaber
	xpType = "combat_rangedspecialize_carbine",
	
	-- See http://www.ocdsoft.com/files/certifications.xls
	certificationsRequired = { "cert_carbine" },
	-- See http://www.ocdsoft.com/files/accuracy.xls
	creatureAccuracyModifiers = { "carbine_accuracy" },

	creatureAimModifiers = { "carbine_aim", "aim" },

	-- See http://www.ocdsoft.com/files/defense.xls
	defenderDefenseModifiers = { "ranged_defense" },

	-- Leave as "dodge" for now, may have additions later
	defenderSecondaryDefenseModifiers = { "carbine_defense" },

	-- See http://www.ocdsoft.com/files/speed.xls
	speedModifiers = { "carbine_speed" },

	defenderToughnessModifiers = { "carbine_toughness" },

	-- Leave blank for now
	damageModifiers = { },
	

	-- The values below are the default values.  To be used for blue frog objects primarily
	healthAttackCost = 5,
	actionAttackCost = 5,
	mindAttackCost = 5,
	forceCost = 0,

	pointBlankRange = 0,
	pointBlankAccuracy = 0,

	idealRange = 40,
	idealAccuracy = 40,

	maxRange = 50,
	maxRangeAccuracy = -20,

	minDamage = 570,
	maxDamage = 1030,

	attackSpeed = 3.25,

	woundsRatio = 7,

	numberExperimentalProperties = {1, 1, 1},
	experimentalProperties = {"XX", "XX", "SR"},
	experimentalWeights = {1, 1, 1},
	experimentalGroupTitles = {"null", "null", "exp_durability"},
	experimentalSubGroupTitles = {"null", "null", "hitpoints"},
	experimentalMin = {0, 0, 1000},
	experimentalMax = {0, 0, 2000},
	experimentalPrecision = {0, 0, 0},
	experimentalCombineType = {0, 0, 1},

}

ObjectTemplates:addTemplate(object_weapon_ranged_carbine_carbine_e5, "object/weapon/ranged/carbine/carbine_e5.iff")
