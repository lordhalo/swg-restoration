
object_weapon_melee_special_massassiknuckler = object_weapon_melee_special_shared_massassiknuckler:new {

	playerRaces = { "object/creature/player/bothan_male.iff",
				"object/creature/player/bothan_female.iff",
				"object/creature/player/human_male.iff",
				"object/creature/player/human_female.iff",
				"object/creature/player/ithorian_male.iff",
				"object/creature/player/ithorian_female.iff",
				"object/creature/player/moncal_male.iff",
				"object/creature/player/moncal_female.iff",
				"object/creature/player/rodian_male.iff",
				"object/creature/player/rodian_female.iff",
				"object/creature/player/sullustan_male.iff",
				"object/creature/player/sullustan_female.iff",
				"object/creature/player/trandoshan_male.iff",
				"object/creature/player/trandoshan_female.iff",
				"object/creature/player/twilek_male.iff",
				"object/creature/player/twilek_female.iff",
				"object/creature/player/wookiee_male.iff",
				"object/creature/player/wookiee_female.iff",
				"object/creature/player/zabrak_male.iff",
				"object/creature/player/zabrak_female.iff" },

	-- RANGEDATTACK, MELEEATTACK, FORCEATTACK, TRAPATTACK, GRENADEATTACK, HEAVYACIDBEAMATTACK,
	-- HEAVYLIGHTNINGBEAMATTACK, HEAVYPARTICLEBEAMATTACK, HEAVYROCKETLAUNCHERATTACK, HEAVYLAUNCHERATTACK
	attackType = MELEEATTACK,
	weaponType = UNARMEDWEAPON,

	-- ENERGY, KINETIC, ELECTRICITY, STUN, BLAST, HEAT, COLD, ACID, FORCE, LIGHTSABER
	damageType = KINETIC,
	

	-- NONE, LIGHT, MEDIUM, HEAVY
	armorPiercing = NONE,

	-- combat_rangedspecialize_bactarifle, combat_rangedspecialize_rifle, combat_rangedspecialize_pistol, combat_rangedspecialize_heavy, combat_rangedspecialize_carbine
	-- combat_meleespecialize_unarmed, combat_meleespecialize_twohand, combat_meleespecialize_polearm, combat_meleespecialize_onehand, combat_general,
	-- combat_meleespecialize_twohandlightsaber, combat_meleespecialize_polearmlightsaber, combat_meleespecialize_onehandlightsaber
	xpType = "combat_meleespecialize_unarmed",

	-- See http://www.ocdsoft.com/files/certifications.xls
	certificationsRequired = { "cert_vibroknuckler" },
	-- See http://www.ocdsoft.com/files/accuracy.xls
	creatureAccuracyModifiers = { "unarmed_accuracy" },

	-- See http://www.ocdsoft.com/files/defense.xls
	defenderDefenseModifiers = { "melee_defense" },

	-- should be defensive acuity only
	defenderSecondaryDefenseModifiers = { "unarmed_defense" },

	defenderToughnessModifiers = { "unarmed_defense" },

	-- See http://www.ocdsoft.com/files/speed.xls
	speedModifiers = { "unarmed_speed" },

	-- Leave blank for now
	damageModifiers = { },


	-- The values below are the default values.  To be used for blue frog objects primarily
	healthAttackCost = 5,
	actionAttackCost = 5,
	mindAttackCost = 5,
	forceCost = 0,

	pointBlankRange = 0,
	pointBlankAccuracy = 5,

	idealRange = 1,
	idealAccuracy = 5,

	maxRange = 5,
	maxRangeAccuracy = 5,

	minDamage = 600,
	maxDamage = 900,

	attackSpeed = 3.0,

	woundsRatio = 8,

	 numberExperimentalProperties = {1, 1, 1},
	 experimentalProperties = {"XX", "XX", "SR"},
	 experimentalWeights = {1, 1, 1},
	 experimentalGroupTitles = {"null", "null", "exp_durability"},
	 experimentalSubGroupTitles = {"null", "null", "hitpoints"},
	 experimentalMin = {0, 0, 1000},
	 experimentalMax = {0, 0, 2000},
	 experimentalPrecision = {0, 0, 0},
	 experimentalCombineType = {0, 0, 1},
}

ObjectTemplates:addTemplate(object_weapon_melee_special_massassiknuckler, "object/weapon/melee/special/massassiknuckler.iff")
