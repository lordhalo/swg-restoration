object_static_worldbuilding_structures_attackable_antenna = object_static_worldbuilding_structures_shared_attackable_antenna :new {
	pvpStatusBitmask = ATTACKABLE,
	optionsBitmask = 0,
	maxCondition = 300000,
	-- Damagetypes in WeaponObject
	vulnerability = BLAST,

	-- LIGHT, MEDIUM, HEAVY
	rating = HEAVY,

	kinetic = 90,
	energy = 95,
	electricity = 90,
	stun = 100,
	blast = -1,
	heat = 90,
	cold = 90,
	acid = 90,
	lightSaber =100,
}

ObjectTemplates:addTemplate(object_static_worldbuilding_structures_attackable_antenna, "object/static/worldbuilding/structures/attackable_antenna.iff")
