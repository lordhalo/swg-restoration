/*
 * DotPackTemplate.h
 *
 *  Created on: 10/08/2010
 *      Author: victor
 */

#ifndef DOTPACKTEMPLATE_H_
#define DOTPACKTEMPLATE_H_

#include "templates/SharedTangibleObjectTemplate.h"

class DotPackTemplate : public SharedTangibleObjectTemplate {
	float power;

public:
	DotPackTemplate() {
		power = 0;
	}

	~DotPackTemplate() {

	}

	void readObject(LuaObject* templateData) {
		SharedTangibleObjectTemplate::readObject(templateData);

		power = templateData->getIntField("power");
    }

	inline int getPower() {
		return power;
	}

	bool isDotPackTemplate() {
		return true;
	}
};

#endif /* DOTPACKTEMPLATE_H_ */
