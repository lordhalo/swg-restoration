/*
 * InstantStimpackTemplate.h
 *
 *  Created on: 11/08/2010
 *      Author: victor
 */

#ifndef INSTANTSTIMPACKTEMPLATE_H_
#define INSTANTSTIMPACKTEMPLATE_H_

#include "StimPackTemplate.h"

class InstantStimpackTemplate : public StimPackTemplate {
	float effectiveness;

public:

	InstantStimpackTemplate() {
		effectiveness = 0;
	}

	~InstantStimpackTemplate() {

	}

	void readObject(LuaObject* templateData) {
		StimPackTemplate::readObject(templateData);

		effectiveness = templateData->getFloatField("effectiveness");

    }

	inline float getEffectiveness() {
		return effectiveness;
	}

	bool isInstantStimpackTemplate() {
		return true;
	}
};

#endif /* InstantStimpackTEMPLATE_H_ */
