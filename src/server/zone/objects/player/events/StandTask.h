#ifndef STANDTASK_H_
#define STANDTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/params/creature/CreatureAttribute.h"

class StandTask : public Task {
	ManagedReference<CreatureObject*> creature;
	float counter;
	int count;
public:

	StandTask(CreatureObject* creo, float time) {
		creature = creo;
		counter = 0;
		count = (time * 1000);
	}

	void run() {
		Locker lockerC(creature);
		Locker lockerCT(creature);
		if(creature != NULL) {
			int amountOfTicks = count;
			if (counter < amountOfTicks ) { // <-

				counter+= 100;
				this->reschedule(100); // Reschedule in 6 seconds...
			}
			else {
				if(creature->isDead() || creature->isIncapacitated()){
					creature->removePendingTask("standTask");
				}else {
					creature->removePendingTask("standTask");
					creature->setPosture(CreaturePosture::UPRIGHT, false, false);
					creature->inflictDamage(creature, CreatureAttribute::ACTION, 300, false);
				}
			}
		}
	}
};


#endif /* STANDTASK_H_ */
