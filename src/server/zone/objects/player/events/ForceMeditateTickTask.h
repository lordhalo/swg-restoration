/*
 * ChannelForceRegenTask.h
 *
 *  Created on: Aug 18, 2011
 *      Author: swgemu
 */

#ifndef FORCEMEDITATETICKTASK_H_
#define FORCEMEDITATETICKTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"

class ForceMeditateTickTask : public Task {
	ManagedReference<CreatureObject*> creature;
public:

	ForceMeditateTickTask(CreatureObject* pl) {
		creature = pl;
	}

	void run() {

		ManagedReference<PlayerObject*> player = creature->getPlayerObject();

		Locker locker(creature);
			//int rankMod = (creature->getSkillMod("rank_offense") * 10);
			float bonusRegen = 15;

			if (creature->isMeditating() == true && player->getForcePower() != player->getForcePowerMax()) {
				if (player->getForcePowerMax() > 0  && (player->getForcePower() + bonusRegen < player->getForcePowerMax())){
					player->setForcePower(player->getForcePower() + bonusRegen);
				}

				creature->playEffect("clienteffect/pl_force_meditate_self.cef", "");

				this->reschedule(6000);
			}
			else {
				creature->removePendingTask("forceMeditate");
		}
	}
};


#endif /* CHANNELFORCEREGENTASK_H_ */
