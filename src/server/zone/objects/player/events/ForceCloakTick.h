/*
 * ChannelForceRegenTask.h
 *
 *  Created on: Aug 18, 2011
 *      Author: swgemu
 */

#ifndef FORCECLOAKTICK_H_
#define FORCECLOAKTICK_H_


#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/Zone.h"

class ForceCloakTick : public Task {
	ManagedReference<CreatureObject*> creature;
	String buffname;
	uint32 buffcrc;
public:

	ForceCloakTick(CreatureObject* pl) {
		creature = pl;
		buffname = "forcecloak";
		buffcrc = buffname.hashCode();
	}

	void run() {

		ManagedReference<PlayerObject*> player = creature->getPlayerObject();

		Locker locker(creature);
			//int rankMod = (creature->getSkillMod("rank_offense") * 10);

			float forceCost = 10;

			CloseObjectsVector* vec = (CloseObjectsVector*) creature->getCloseObjects();

			assert(vec != NULL);

			SortedVector<QuadTreeEntry*> closeObjects;
			vec->safeCopyTo(closeObjects);


			for (int i = 0; i < closeObjects.size(); ++i) {
				SceneObject* obj = static_cast<SceneObject*> (closeObjects.get(i));
					if (obj->isCreatureObject())
						forceCost += 5;
					//if (obj->isAiAgent())
					//	forceCost += 2;
			}

			if (creature->hasBuff(buffcrc) && player->getForcePower() > forceCost) {
				player->setForcePower(player->getForcePower() - (forceCost));
				this->reschedule(5000);
			}
			else {

				creature->setInvisible(false);

				SortedVector<QuadTreeEntry*> objects(512, 512);
				CloseObjectsVector* closeObjectsVector = (CloseObjectsVector*) creature->getCloseObjects();

				if (closeObjectsVector == NULL) {
					creature->getZone()->getInRangeObjects(creature->getPositionX(), creature->getPositionY(), 32, &objects, true);
				} else {
					closeObjectsVector->safeCopyTo(objects);
				}

				for (int i = 0; i < objects.size(); ++i) {
					SceneObject* object = static_cast<SceneObject*>(objects.get(i));

					if (object->isCreatureObject()) {
						CreatureObject* creo = cast<CreatureObject*>(object);
						//obj->notifyInsert(creature);
						if(creo != creature){
							if(creature->getParent() == NULL ){
								creo->notifyInsert(creature);
							}
						}
					}
				}


				creature->removePendingTask("forceCloak");
				creature->removeBuff(buffcrc);
				//creature->setInvisible(false);
		}
	}
};


#endif /* CHANNELFORCEREGENTASK_H_ */
