/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef PLAYERACTIONMINDRECOVERYEVENT_H_
#define PLAYERACTIONMINDRECOVERYEVENT_H_

#include "server/zone/objects/player/PlayerObject.h"

namespace server {
namespace zone {
namespace objects {
namespace player {
namespace events {

class PlayerActionMindRecoveryEvent : public Task {
	ManagedWeakReference<PlayerObject*> player;
	Time startTime;

public:
	PlayerActionMindRecoveryEvent(PlayerObject* pl) : Task(2000) {
		player = pl;
		startTime.updateToCurrentTime();
	}

	~PlayerActionMindRecoveryEvent() {
		/*if (enQueued) {
			System::out << "ERROR: PlayerActionMindRecoveryEvent scheduled event deleted\n";
			raise(SIGSEGV);
		}*/
	}

	void run() {
		ManagedReference<PlayerObject*> play = player.get();

		if (play == NULL)
			return;

		ManagedReference<SceneObject*> strongParent = play->getParent();

		if (strongParent == NULL)
			return;

		Locker _locker(strongParent);

		if (play->isOnline() || play->isLinkDead())
			play->doAMRecovery(startTime.miliDifference());


	}

	void schedule(uint64 delay = 0)
	{
		startTime.updateToCurrentTime();
		Task::schedule(delay);
	}
};

}
}
}
}
}

using namespace server::zone::objects::player::events;

#endif /*PlayerActionMindRecoveryEvent_H_*/
