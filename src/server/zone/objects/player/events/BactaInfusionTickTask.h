#ifndef BACTAINFUSIONTICKTASK_H_
#define BACTAINFUSIONTICKTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/params/creature/CreatureAttribute.h"

class BactaInfusionTickTask : public Task {
	ManagedReference<CreatureObject*> creature;
	int counter;
	int power;
	int tickNumber;
public:

	BactaInfusionTickTask(CreatureObject* creo, int healPower, int ticks) {
		creature = creo;  // This is the Attacker
		counter = 0;
		power = healPower;
		tickNumber = ticks;
	}

	void run() {
		Locker lockerC(creature);

		if(creature != NULL) {
			int amountOfTicks = tickNumber;
			if (counter < amountOfTicks && !creature->isDead() && creature->isHealableBy(creature)) {
				creature->healDamage(creature, CreatureAttribute::HEALTH, power);
				creature->playEffect("clienteffect/healing_healdamage.cef", "");
				counter++;
				this->reschedule(6000); // Reschedule in 6 seconds...
			}
			else {
				creature->removePendingTask("bactaInfusionTickTask");
			}
		}
	}
};


#endif /* BACTAINFUSIONTICKTASK_H_ */
