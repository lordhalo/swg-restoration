/*
 * ChannelForceRegenTask.h
 *
 *  Created on: Aug 18, 2011
 *      Author: swgemu
 */

#ifndef FORCECHOKETICKTASK_H_
#define FORCECHOKETICKTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/tangible/wearables/ArmorObject.h"

class ForceChokeTickTask : public Task {
	ManagedReference<CreatureObject*> creature;
	ManagedReference<CreatureObject*> creatureTarget;
	int counter;
public:

	ForceChokeTickTask(CreatureObject* creo, CreatureObject* creoTarget) {
		creature = creo;  // This is the Attacker
		creatureTarget = creoTarget; // This is the target (one that takes damage)
		counter = 0;
	}

	void run() {
		Locker lockerC(creature);
		Locker lockerCT(creatureTarget,creature);
		if(creature != NULL && creatureTarget != NULL) {
			int amountOfTicks = creature->getSkillMod("force_choke");
			float rankMod = (1.0 + ((creature->getSkillMod("rank_offense") / 2) / 100.f));
			if (counter < amountOfTicks && !creatureTarget->isIncapacitated() && !creatureTarget->isDead() && (creatureTarget->getPvpStatusBitmask() != CreatureFlag::NONE)) {
				float damage = 350;
				float healthDamage = damage * rankMod;


				Vector<ManagedReference<ArmorObject*> > healthArmor = creatureTarget->getWearablesDeltaVector()->getArmorAtHitLocation(ArmorObjectTemplate::CHEST);
				if (System::random(1) == 0)
					healthArmor = creatureTarget->getWearablesDeltaVector()->getArmorAtHitLocation(ArmorObjectTemplate::ARMS);
				ManagedReference<ArmorObject*> healthArmorToHit = NULL;
				if (!healthArmor.isEmpty())
					healthArmorToHit = healthArmor.get(System::random(healthArmor.size() - 1));
				if (healthArmorToHit !=NULL){
						float armorReduction = healthArmorToHit->getKinetic();
						if (armorReduction > 0) healthDamage *= (1.f - (armorReduction / 100.f));
						Locker locker(healthArmorToHit);
						healthArmorToHit->inflictDamage(healthArmorToHit, 0, healthDamage * 0.1, true, true);
					}

				if (healthArmorToHit == NULL && (creatureTarget->hasSkill("combat_unarmed_master") || creatureTarget->hasSkill("force_discipline_defender_master"))){
					healthDamage *= 0.4;
				}

				creatureTarget->inflictDamage(creature, CreatureAttribute::HEALTH, healthDamage, true);

				creatureTarget->playEffect("clienteffect/pl_force_choke.cef", "");

				counter++;
				this->reschedule(6000); // Reschedule in 6 seconds...
			}
			else {

				creatureTarget->removePendingTask("forceChokeTickTask");
			}
		}
	}
};


#endif /* CHANNELFORCEREGENTASK_H_ */
