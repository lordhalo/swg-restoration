#ifndef COOLDOWNTASK_H_
#define COOLDOWNTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"

class CoolDownTask : public Task {
	ManagedReference<CreatureObject*> creature;
	int counter;
	float count;
	String say;
	String fly;
public:

	CoolDownTask(CreatureObject* creo, float time, String name, String flyText) {
		creature = creo;
		counter = 0;
		count = (time * 1000);
		say = name;
		fly = flyText;
	}

	void run() {
		Locker lockerC(creature);
		Locker lockerCT(creature);
		if(creature != NULL) {
			int amountOfTicks = count;
			if (counter < amountOfTicks ) {

				//counter++;
				//this->reschedule(1000);
				counter+= 100;
				this->reschedule(100);
			}
			else {
				//StringBuffer msgPlayer, msgTail;
				//msgTail << " is now ready.";

					creature->showFlyText("combat_effects", fly, 0, 255, 0, true);
					//msgPlayer << say << msgTail.toString();
					//creature->sendSystemMessage(msgPlayer.toString());
					creature->removePendingTask(say);
			}
		}
	}
};


#endif /* COOLDOWNTASK_H_ */
