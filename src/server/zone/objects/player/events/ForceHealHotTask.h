#ifndef FORCEHEALHOTTASK_H_
#define FORCEHEALHOTTASK_H_


#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/params/creature/CreatureAttribute.h"

class ForceHealHotTask : public Task {
	ManagedReference<CreatureObject*> creature;
	int counter;
	int duration;
	int healAmount;
public:

	ForceHealHotTask(CreatureObject* creo, int value, int dur) {
		creature = creo;  // This is the Attacker
		counter = 0;
		duration = dur;
		healAmount = value;
	}

	void run() {
		Locker lockerC(creature);

		if(creature != NULL) {
			int amountOfTicks = duration / 6;
			if (counter < amountOfTicks && !creature->isIncapacitated() && !creature->isDead() && creature->isHealableBy(creature)) {
				creature->healDamage(creature, CreatureAttribute::HEALTH, healAmount);
				creature->playEffect("clienteffect/pl_force_heal_self.cef", "");
				counter++;
				this->reschedule(6000); // Reschedule in 6 seconds...
			}
			else {
				creature->removePendingTask("forceHealTickTask");
			}
		}
	}
};


#endif /* CHANNELFORCEREGENTASK_H_ */
