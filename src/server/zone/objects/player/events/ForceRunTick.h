/*
 * ChannelForceRegenTask.h
 *
 *  Created on: Aug 18, 2011
 *      Author: swgemu
 */

#ifndef FORCERUNTICK_H_
#define FORCERUNTICK_H_


#include "server/zone/objects/creature/CreatureObject.h"

class ForceRunTick : public Task {
	ManagedReference<CreatureObject*> creature;
	int runLevel;
public:

	ForceRunTick(CreatureObject* pl, int level) {
		creature = pl;
		runLevel = level;
	}

	void run() {

		ManagedReference<PlayerObject*> player = creature->getPlayerObject();

		Locker locker(creature);
			//int rankMod = (creature->getSkillMod("rank_offense") * 10);
			float forceCost = 5; // 5 * runLevel;
			uint32 buffcrc1 = BuffCRC::JEDI_FORCE_RUN_1;
			uint32 buffcrc2 = BuffCRC::JEDI_FORCE_RUN_2;
			uint32 buffcrc3 = BuffCRC::JEDI_FORCE_RUN_3;

			if (forceCost > (player->getForcePower() - forceCost + 5)) {

				switch(runLevel){
					case 1:
						creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_1);
						break;
					case 2:
						creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_2);
						break;
					case 3:
						creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_3);
						break;

				}
			}

			if ((creature->hasBuff(buffcrc1) || creature->hasBuff(buffcrc2) || creature->hasBuff(buffcrc3))) {
				if (player->getForcePowerMax() > 0  && (player->getForcePowerMax() - player->getForcePower() > 0)){
					player->setForcePower(player->getForcePower() - (forceCost));
					VisibilityManager::instance()->increaseVisibility(creature, 1);
				}

				this->reschedule(10000);
			}
			else {
				creature->removePendingTask("runCheck");
		}
	}
};


#endif /* CHANNELFORCEREGENTASK_H_ */
