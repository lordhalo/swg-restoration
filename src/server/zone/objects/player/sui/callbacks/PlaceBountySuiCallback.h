
#ifndef PLACEBOUNTYSUICALLBACK_H_
#define PLACEBOUNTYSUICALLBACK_H_

#include "server/zone/objects/player/sui/SuiCallback.h"
#include "server/zone/objects/player/sui/SuiWindowType.h"
#include "server/zone/objects/player/sui/listbox/SuiListBox.h"
#include "server/zone/objects/player/sui/placebountybox/SuiPlaceBountyBox.h"
#include "server/zone/objects/scene/SceneObjectType.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/objects/building/BuildingObject.h"
#include "server/zone/managers/player/PlayerManager.h"

class PlaceBountySuiCallback : public SuiCallback {
	PlayerObject* attacker;
public:
	PlaceBountySuiCallback(ZoneServer* serv, PlayerObject* killedBy) : SuiCallback(serv){
		attacker = killedBy;
	}

	void run(CreatureObject* player, SuiBox* sui, uint32 eventIndex, Vector<UnicodeString>* args) {
		bool cancelPressed = (eventIndex == 1);

		if (!sui->isPlaceBountyBox() || cancelPressed)
			return;

		if (player == NULL || attacker == NULL )
			return;

		if (args->size() < 2)
			return;

		int credits = Integer::valueOf(args->get(0).toString());
		int bounty = Integer::valueOf(args->get(1).toString());

		uint32 currentCredits = player->getBankCredits();

		if (currentCredits == ((uint32) credits + (uint32) bounty)) {
			if(bounty > 0)
				attacker->addBounty(bounty);
			player->setBankCredits(credits);
		}

		const String sysMsg = "Bounty of " + String::valueOf(bounty) + " credits successfully placed.";
		player->sendSystemMessage(sysMsg);
	}
};
#endif /* PLACEBOUNTYSUICALLBACK_H_ */
