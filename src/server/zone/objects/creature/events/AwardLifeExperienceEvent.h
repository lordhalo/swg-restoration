/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef AWARDLIFEEXPERIENCEEVENT_H_
#define AWARDLIFEEXPERIENCEEVENT_H_

#include "server/zone/objects/creature/CreatureObject.h"

class AwardLifeExperienceEvent : public Task {
	ManagedWeakReference<CreatureObject*> creo;

public:
	AwardLifeExperienceEvent(CreatureObject* cr) : Task() {
		creo = cr;
	}

	void run() {
		ManagedReference<CreatureObject*> creature = creo.get();

		if (creature == NULL)
			return;

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

		Locker locker(creature);

		playerObject->addExperience("death_exp", 1, true);
		creature->setShouldLifeReward(0); //1 = Should 0 = should not
	}

};

#endif /*BURSTRUNNOTIFYAVAILABLEEVENT_H_*/
