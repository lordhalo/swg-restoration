/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef KNEECAPSHOT2COMMAND_H_
#define KNEECAPSHOT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class KneeCapShot2Command : public CombatQueueCommand {
public:

	KneeCapShot2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //KneeCapShotCommand_H_
