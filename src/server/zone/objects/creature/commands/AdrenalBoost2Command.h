/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef ADRENALBOOST2COMMAND_H_
#define ADRENALBOOST2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/tangible/pharmaceutical/EnhancePack.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/creature/events/InjuryTreatmentTask.h"
#include "server/zone/objects/creature/buffs/Buff.h"
#include "server/zone/objects/creature/BuffAttribute.h"
#include "server/zone/objects/creature/buffs/DelayedBuff.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/managers/collision/CollisionManager.h"

class AdrenalBoost2Command : public QueueCommand {
	float mindCost;
	float range;
public:

	AdrenalBoost2Command(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		
		mindCost = 150;
		range = 7;
	}

	void deactivateWoundTreatment(CreatureObject* creature) const {
		float modSkill = (float)creature->getSkillMod("healing_wound_speed");

		int delay = (int)round((modSkill * -(2.0f / 25.0f)) + 20.0f);

		if (creature->hasBuff(BuffCRC::FOOD_HEAL_RECOVERY)) {
			DelayedBuff* buff = cast<DelayedBuff*>( creature->getBuff(BuffCRC::FOOD_HEAL_RECOVERY));

			if (buff != NULL) {
				float percent = buff->getSkillModifierValue("heal_recovery");

				delay = round(delay * (100.0f - percent) / 100.0f);
			}
		}

		//Force the delay to be at least 3 seconds.
		delay = (delay < 3) ? 3 : delay;

		StringIdChatParameter message("healing_response", "healing_response_59"); //You are now ready to heal more wounds or apply more enhancements.
		Reference<InjuryTreatmentTask*> task = new InjuryTreatmentTask(creature, message, "woundTreatment");
		creature->addPendingTask("woundTreatment", task, delay * 1000);
	}

	bool canPerformSkill(CreatureObject* enhancer, CreatureObject* patient) const {
		if (patient->isDead())
			return false;

		if (!enhancer->canTreatWounds()) {
			enhancer->sendSystemMessage("@healing_response:enhancement_must_wait"); //You must wait before you can heal wounds or apply enhancements again.
			return false;
		}

		if (enhancer->isInCombat()) {
			enhancer->sendSystemMessage("You cannot do that while in Combat.");
			return false;
		}

		if (!patient->isPlayerCreature() && !(patient->isCreature() && patient->isPet())) {
			enhancer->sendSystemMessage("@healing_response:healing_response_77"); //Target must be a player or a creature pet in order to apply enhancements.
			return false;
		}

		if (patient->isInCombat()) {
			enhancer->sendSystemMessage("You cannot do that while your target is in Combat.");
			return false;
		}

		if (enhancer != patient && !CollisionManager::checkLineOfSight(enhancer, patient)) {
			enhancer->sendSystemMessage("@container_error_message:container18");
			return false;
		}

		return true;
	}

	void awardXp(CreatureObject* creature, const String& type, int power) const {
		if (!creature->isPlayerCreature())
			return;

		CreatureObject* player = cast<CreatureObject*>(creature);

		int amount = (int)round((float)power * 2.5f);

		if (amount <= 0)
			return;

		PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();
		playerManager->awardExperience(player, type, amount * 4, true);
	}

	void doAnimations(CreatureObject* enhancer, CreatureObject* patient) const {
		patient->playEffect("clienteffect/healing_healenhance.cef", "");

		if (enhancer == patient)
			enhancer->doAnimation("heal_self");
		else
			enhancer->doAnimation("heal_other");
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		int result = doCommonMedicalCommandChecks(creature);

		if (result != SUCCESS)
			return result;

		CreatureObject* enhancer = creature;

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);

		if (object != NULL) {
			if (!object->isCreatureObject()) {
				TangibleObject* tangibleObject = dynamic_cast<TangibleObject*>(object.get());
 
				if (tangibleObject != NULL && tangibleObject->isAttackableBy(creature)) {
					object = creature;
				} else {
					creature->sendSystemMessage("@healing_response:healing_response_77"); //Target must be a player or a creature pet in order to apply enhancements.
					return GENERALERROR;
				}
			}
		} else {
			object = creature;
		}

		CreatureObject* targetCreature = cast<CreatureObject*>( object.get());
		uint64 objectId = 0;

		if (!targetCreature->isInRange(creature, range + targetCreature->getTemplateRadius() + creature->getTemplateRadius()))
			return TOOFAR;

		CreatureObject* patient = cast<CreatureObject*>( targetCreature);

		Locker clocker(patient, creature);

		if (patient->isDead() || patient->isRidingMount())
			patient = enhancer;

		if (!canPerformSkill(enhancer, patient))
			return GENERALERROR;

		uint32 currentBuff = 0;
		uint32 buffcrc = 0x2E77F586;

		//if (patient->hasBuff(buffcrc)) {
		//	Buff* existingbuff = patient->getBuff(buffcrc);
		//}

		int duration = 2400;

		PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();

		ManagedReference<Buff*> buff = new Buff(patient, buffcrc, duration, BuffType::MEDICAL);

		Locker locker(buff);

		//buff->setStartMessage(startStringId);
		//buff->setEndMessage(endStringId);
		buff->setSkillModifier("combat_haste", 25);

		patient->addBuff(buff);

		deactivateWoundTreatment(enhancer);

		doAnimations(enhancer, patient);

		creature->notifyObservers(ObserverEventType::MEDPACKUSED);

		return SUCCESS;
	}

};

#endif //AdrenalBoostCommand_H_
