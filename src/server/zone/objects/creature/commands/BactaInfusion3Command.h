/*
Copyright (C) 2007 <SWGEmu>

This File is part of Core3.

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General
Public License along with this program; if not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Linking Engine3 statically or dynamically with other modules
is making a combined work based on Engine3.
Thus, the terms and conditions of the GNU Lesser General Public License
cover the whole combination.

In addition, as a special exception, the copyright holders of Engine3
give you permission to combine Engine3 program with free software
programs or libraries that are released under the GNU LGPL and with
code included in the standard release of Core3 under the GNU LGPL
license (or modified versions of such code, with unchanged license).
You may copy and distribute such a system following the terms of the
GNU LGPL for Engine3 and the licenses of the other code concerned,
provided that you include the source code of that other code when
and as the GNU LGPL requires distribution of source code.

Note that people who make modified versions of Engine3 are not obligated
to grant this special exception for their modified versions;
it is their choice whether to do so. The GNU Lesser General Public License
gives permission to release a modified version without this exception;
this exception also makes it possible to release a modified version
which carries forward this exception.
*/

#ifndef BACTAINFUSION3COMMAND_H_
#define BACTAINFUSION3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/BactaInfusionTickTask.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"

class BactaInfusion3Command : public QueueCommand {

	int healthHealed;
	float speed;
	float range;
	String cdName;
public:

	BactaInfusion3Command(const String& name, ZoneProcessServer* server)
			: QueueCommand(name, server) {

		healthHealed = 0;
		speed = 3;
		range = 6;
		cdName = "bactaInfusion";
	}

	void doAnimations(CreatureObject* creature, CreatureObject* creatureTarget) const {
			creatureTarget->playEffect("clienteffect/healing_healdamage.cef", "");

			if (creature == creatureTarget)
				creature->doAnimation("heal_self");
			else
				creature->doAnimation("heal_other");
		}

		int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

			int result = doCommonMedicalCommandChecks(creature);

			if (result != SUCCESS)
				return result;

			ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);

			if (object != NULL) {
				if (!object->isCreatureObject()) {
					TangibleObject* tangibleObject = dynamic_cast<TangibleObject*>(object.get());

					if (tangibleObject != NULL && tangibleObject->isAttackableBy(creature)) {
						object = creature;
					} else {
						creature->sendSystemMessage("@healing_response:healing_response_99"); //Target must be a player or a creature pet in order to quick heal.
						return GENERALERROR;
					}
				}
			} else {
				object = creature;
			}

			CreatureObject* creatureTarget = cast<CreatureObject*>( object.get());

			Locker clocker(creatureTarget, creature);

			int mindCost = 100;
			int battleFat = creature->getShockWounds();

			if(battleFat >= 100)
				mindCost += (battleFat / 2);

			if ((creatureTarget->isAiAgent() && !creatureTarget->isPet()) || creatureTarget->isDroidObject() || creatureTarget->isDead() || creatureTarget->isRidingMount() || creatureTarget->isAttackableBy(creature))
				creatureTarget = creature;

			if (!creature->isInRange(creatureTarget, range + creatureTarget->getTemplateRadius() + creature->getTemplateRadius()))
				return TOOFAR;

			if (!creatureTarget->isHealableBy(creature)) {
				creature->sendSystemMessage("@healing:pvp_no_help");  //It would be unwise to help such a patient.
				return GENERALERROR;
			}

			if (creature->getHAM(CreatureAttribute::MIND) < abs(mindCost)) {
				creature->sendSystemMessage("@healing_response:not_enough_mind"); //You do not have enough mind to do that.
				return GENERALERROR;
			}

			if (!creatureTarget->hasDamage(CreatureAttribute::HEALTH)) {
				if (creatureTarget == creature)
					creature->sendSystemMessage("@healing_response:healing_response_61"); //You have no damage to heal.
				else if (creatureTarget->isPlayerCreature()) {
					StringIdChatParameter stringId("healing_response", "healing_response_63"); //%NT has no damage to heal.
					stringId.setTT(creatureTarget->getObjectID());
					creature->sendSystemMessage(stringId);
				} else {
					StringBuffer message;
					message << creatureTarget->getDisplayedName() << " has no damage to heal.";
					creature->sendSystemMessage(message.toString());
				}

				return GENERALERROR;
			}

			Reference<BactaInfusionTickTask*> infusionCheck = creatureTarget->getPendingTask("bactaInfusionTickTask").castTo<BactaInfusionTickTask*>();
			if (infusionCheck != NULL){
				creature->sendSystemMessage("Bacta Infusion is Already active");
				return GENERALERROR;
			}

			int basePower = 100;
			int creatureSkill = creature->getSkillMod("healing_efficiency");
			int tickNumber = 6; //6 seconds between ticks

			double healPower = basePower * (1.0 + (creatureSkill / 100.f));


			if (creature->isPlayerCreature()) {
				PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();
				playerManager->sendBattleFatigueMessage(creature, creatureTarget);
			}


			creature->inflictDamage(creature, CreatureAttribute::MIND, mindCost, false);

			Reference<BactaInfusionTickTask*> infusion = new BactaInfusionTickTask(creatureTarget, healPower, tickNumber);
			creatureTarget->addPendingTask("bactaInfusionTickTask", infusion, 6000);

			doAnimations(creature, creatureTarget);

			checkForTef(creature, creatureTarget);

			return SUCCESS;
		}

};

#endif //TENDDAMAGECOMMAND_H_
