/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef ATTACKCOMMAND_H_
#define ATTACKCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class AttackCommand : public CombatQueueCommand {
public:

	AttackCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->isPlayerCreature())
			creature->sendSystemMessage("Use MeleeHit for Melee Weapons, RangedShot for Ranged Weapons");
			return INVALIDTARGET;

		/*ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		if (targetObject == NULL || !targetObject->isCreatureObject())
			return INVALIDTARGET;

		CreatureObject *targetCreo = targetObject->asCreatureObject();
		unsigned long targetID = targetCreo->getTargetID();

		if(targetID == 0)
			return INVALIDTARGET;

		if (creature->isPlayerCreature()){
			if (creature->getWeapon()->isRangedWeapon()){
				creature->enqueueCommand(STRING_HASHCODE("rangedShot"), 1, targetID, ""); // Should we limit the amount of times this can be enqueued?
			}else{
				creature->enqueueCommand(STRING_HASHCODE("meleeHit"), 1, targetID, ""); // Should we limit the amount of times this can be enqueued?
			}
		}*/

		return doCombatAction(creature, target);
	}
};

#endif //ATTACKCOMMAND_H_
