/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef BURSTSHOTCOMMAND_H_
#define BURSTSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class BurstShotCommand : public CombatQueueCommand {
public:

	BurstShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //BURSTSHOTCOMMAND_H_
