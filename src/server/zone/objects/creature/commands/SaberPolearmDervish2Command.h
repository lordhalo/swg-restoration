/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERPOLEARMDERVISH2COMMAND_H_
#define SABERPOLEARMDERVISH2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class SaberPolearmDervish2Command : public  CombatQueueCommand {
public:

	SaberPolearmDervish2Command(const String& name, ZoneProcessServer* server)
		:  CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			float timer = 8.0;
			float reduction = (creature->getSkillMod("polearmlightsaber_speed") / 1000.f);
			timer -= reduction;

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, timer, name, "saber_polearm_dervish2_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "saber_polearm_dervish2_start", 0, 0, 255, true);
		}

		return SUCCESS;
	}

};

#endif //SABERPOLEARMDERVISH2COMMAND_H_
