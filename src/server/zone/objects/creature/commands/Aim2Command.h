/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef AIM2COMMAND_H_
#define AIM2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class Aim2Command : public CombatQueueCommand {
public:

	Aim2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<WeaponObject*> weapon = creature->getWeapon();

		if (!weapon->isRangedWeapon()) {
			return INVALIDWEAPON;
		}

		int actionCost = 200;
		if (creature->getHAM(CreatureAttribute::ACTION)  < actionCost) {

			if (creature->isPlayerCreature())
				(creature)->sendSystemMessage("@cbt_spam:cover_fail_single"); // You fail to take cover.

			creature->sendStateCombatSpam("cbt_spam", "cover_fail", 0);
			return GENERALERROR;
		}

		creature->setAimingState(15, 160);
		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);

		//Send combat spam.
		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);
		if (targetObject != NULL && targetObject->isTangibleObject() && creature != targetObject) {
			TangibleObject* defender = cast<TangibleObject*>( targetObject.get());
			CombatManager::instance()->broadcastCombatSpam(creature, defender, NULL, 0, "cbt_spam", combatSpam, 0);
		}

		return SUCCESS;
	}

};

#endif //AIMCOMMAND_H_
