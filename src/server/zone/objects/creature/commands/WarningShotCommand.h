/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef WARNINGSHOTCOMMAND_H_
#define WARNINGSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class WarningShotCommand : public CombatQueueCommand {
public:

	WarningShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
				if(object->isCreature() || object->isPlayerCreature()){
					ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

					Locker clocker(creatureTarget, creature);

				if (creature != NULL && !creature->hasBuff(getNameCRC())) {
					ManagedReference<Buff*> buff = new Buff(creatureTarget, getNameCRC(), 15, BuffType::SKILL);
					Locker locker(buff);
					buff->setStartFlyText("combat_effects", "warningshot_start", 255, 0, 0);
					buff->setEndFlyText("combat_effects", "warningshot_end", 0, 0, 255);
					buff->setSkillModifier("private_defense_penalty", 80);

					creatureTarget->addBuff(buff);
					creatureTarget->playEffect("clienteffect/pl_armorbreak.cef", "");
					creatureTarget->clearCombatState();
					creature->clearCombatState();
				}
				}
		}
		return SUCCESS;
	}

};

#endif //WARNINGSHOTCOMMAND_H_
