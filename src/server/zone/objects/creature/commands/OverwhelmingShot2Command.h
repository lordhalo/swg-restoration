/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef OVERWHELMINGSHOT2COMMAND_H_
#define OVERWHELMINGSHOT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class OverwhelmingShot2Command : public CombatQueueCommand {
public:

	OverwhelmingShot2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		return doCombatAction(creature, target);
	}

};

#endif //OverwelmingShot2Command_H_
