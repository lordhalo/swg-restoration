/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef DEUTERIUMTOSS2COMMAND_H_
#define DEUTERIUMTOSS2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/tangible/pharmaceutical/DotPack.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/objects/tangible/threat/ThreatMap.h"
#include "CombatQueueCommand.h"

class DeuteriumToss2Command : public CombatQueueCommand {
public:

	DeuteriumToss2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	void doAnimationsRange(CreatureObject* creature, CreatureObject* creatureTarget, float range) const {
		String crc;

		if (range < 10.0f) {
				crc = "throw_grenade_near_poison";
		}
		else if (10.0f <= range && range < 20.f) {
				crc = "throw_grenade_medium_poison";
		}
		else {
				crc = "throw_grenade_far_poison";
		}

		CombatAction* action = new CombatAction(creature, creatureTarget,  crc.hashCode(), 1, 0L);

		creature->broadcastMessage(action, true);
	}

	bool checkTarget(CreatureObject* creature, CreatureObject* targetCreature, uint32 dotType) const {
		if (!targetCreature->isAttackableBy(creature))
			return false;

		if (targetCreature->hasDotImmunity(dotType))
			return false;

		if (creature != targetCreature && !CollisionManager::checkLineOfSight(creature, targetCreature))
			return false;

		return true;
	}

	void awardXp(CreatureObject* creature, const String& type, int power) const {
		if (!creature->isPlayerCreature())
			return;

		CreatureObject* player = cast<CreatureObject*>(creature);

		int amount = (int)round((float)power);

		if (amount <= 0)
			return;

		PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();
		playerManager->awardExperience(player, type, amount * 4, true);
	}

	void applyCost(CreatureObject* creature, int actionDamage, int mindDamage) const {
		if (mindDamage == 0 || actionDamage == 0)
			return;

		creature->inflictDamage(creature, CreatureAttribute::MIND, mindDamage, false);
		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionDamage, false);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		int result = doCommonMedicalCommandChecks(creature);

		if (result != SUCCESS)
			return result;

		int actionCost = 50;
		int mindCost = 250;

		if ((creature->getHAM(CreatureAttribute::MIND) < mindCost) || (creature->getHAM(CreatureAttribute::ACTION) < actionCost))
			return INSUFFICIENTHAM;

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);
		if (object == NULL || !object->isCreatureObject() || creature == object)
			return INVALIDTARGET;

		uint64 objectId = 0;

		ManagedReference<DotPack*> dotPack = NULL;

		SceneObject* inventory = creature->getSlottedObject("inventory");

		if (inventory != NULL) {
			dotPack = inventory->getContainerObject(objectId).castTo<DotPack*>();
		}


		PlayerManager* playerManager = server->getPlayerManager();
		CombatManager* combatManager = CombatManager::instance();

		CreatureObject* creatureTarget = cast<CreatureObject*>(object.get());

		if (creature != creatureTarget && !CollisionManager::checkLineOfSight(creature, creatureTarget)) {
			creature->sendSystemMessage("@container_error_message:container18");
			return GENERALERROR;
		}

		if (creatureTarget->isFrozen()){
			return GENERALERROR;
		}

		int	range = 30;

		if (creature != creatureTarget && !creature->isInRange(creatureTarget, range + creatureTarget->getTemplateRadius() + creature->getTemplateRadius())){
			creature->sendSystemMessage("@error_message:target_out_of_range"); //Your target is out of range for this action.
			return TOOFAR;
		}

		Locker clocker(creatureTarget, creature);

		if (!combatManager->startCombat(creature, creatureTarget))
			return INVALIDTARGET;

		applyCost(creature, actionCost, mindCost);

		int dotDMG = 0;
		int duration = 60;

		float gcwOffense = (1.0 + ((creature->getSkillMod("gcw_offence") / 2) / 100.f));
		float bhOffense = (1.0 + ((creature->getSkillMod("bhrank_offence") / 2) / 100.f));

		int basePower = 200 * gcwOffense * bhOffense;

		int creatureSkill = creature->getSkillMod("dot_efficiency");
		int potency = 50 + creature->getSkillMod("debuffing_efficiency");

		double dotPower = basePower * (1.0 + (creatureSkill / 100.f));

		if(dotPack != NULL) {
			int packPower = dotPack->calculatePower(creature);
			dotPower += packPower;
		}

		//if (!creatureTarget->hasDotImmunity(dotPack->getDotType())) {
			StringIdChatParameter stringId("healing", "apply_fire_self");
			stringId.setTT(creatureTarget->getObjectID());

			creature->sendSystemMessage(stringId);

			StringIdChatParameter stringId2("healing", "apply_fire_other");
			stringId2.setTU(creature->getObjectID());

			creatureTarget->sendSystemMessage(stringId2);

			dotDMG = creatureTarget->addDotState(creature, CreatureState::ONFIRE, nameCRC, dotPower, CreatureAttribute::HEALTH, duration, potency, creatureTarget->getSkillMod("resistance_fire"));
		//}

		if (dotDMG) {
			awardXp(creature, "medical", dotDMG); //No experience for healing yourself.
			creatureTarget->getThreatMap()->addDamage(creature, dotDMG, "");
		} else {
			StringIdChatParameter stringId("dot_message", "dot_resisted");
			stringId.setTT(creatureTarget->getObjectID());

			creature->sendSystemMessage(stringId);

			StringIdChatParameter stringId2("healing", "dot_resist_other");

			creatureTarget->sendSystemMessage(stringId2);
		}

		checkForTef(creature, creatureTarget);

		if (dotPack != NULL) {
			if (creatureTarget != creature)
				clocker.release();

			Locker dlocker(dotPack, creature);
			dotPack->decreaseUseCount();
		}

		doAnimationsRange(creature, creatureTarget, creature->getDistanceTo(creatureTarget));

		creature->notifyObservers(ObserverEventType::MEDPACKUSED);

		return SUCCESS;
	}

};

#endif //NeurotoxinCommand_H_
