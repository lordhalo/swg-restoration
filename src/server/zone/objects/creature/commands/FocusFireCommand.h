/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FOCUSFIRECOMMAND_H_
#define FOCUSFIRECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class FocusFireCommand : public CombatQueueCommand {
public:

	FocusFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //FocusFireCommand_H_

