/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef OVERWHELMINGSHOTCOMMAND_H_
#define OVERWHELMINGSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class OverwhelmingShotCommand : public CombatQueueCommand {
public:

	OverwhelmingShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		return doCombatAction(creature, target);
	}

};

#endif //OverwelmingShotCommand_H_
