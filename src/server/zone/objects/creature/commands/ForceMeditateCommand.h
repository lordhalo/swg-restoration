/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEMEDITATECOMMAND_H_
#define FORCEMEDITATECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/ForceMeditateTickTask.h"

class ForceMeditateCommand : public QueueCommand {
public:

	ForceMeditateCommand(const String& name, ZoneProcessServer* server)
	: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}
		
		if (creature->isInCombat()) {
			creature->sendSystemMessage("@jedi_spam:not_while_in_combat");
			return GENERALERROR;
		}

		if (creature->isMeditating()) {
			creature->sendSystemMessage("@jedi_spam:already_in_meditative_state");
			return GENERALERROR;
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 60, name, "force_meditate_stop");
		creature->addPendingTask(name, cdGroup, 1);
		creature->showFlyText("combat_effects", "force_meditate_start", 0, 0, 255, true);

		// Play Client Effect once.

		creature->playEffect("clienteffect/pl_force_meditate_self.cef", "");

		// Force Meditate Task
		ManagedReference<PlayerObject*> ghost = creature->getPlayerObject();
		
		creature->sendSystemMessage("@teraskasi:med_begin");
		Reference<ForceMeditateTickTask*> fmeditateTask = new ForceMeditateTickTask(creature);
		//fmeditateTask->setMoodString(creature->getMoodString());
		creature->addPendingTask("forceMeditate", fmeditateTask, 3500);

		creature->setMeditateState();

		PlayerManager* playermgr = server->getZoneServer()->getPlayerManager();	
		creature->registerObserver(ObserverEventType::POSTURECHANGED, playermgr);

		return SUCCESS;

	}

};

#endif //FORCEMEDITATECOMMAND_H_
