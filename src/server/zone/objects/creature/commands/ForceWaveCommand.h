#ifndef FORCEWAVECOMMAND_H_
#define FORCEWAVECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "ForcePowersQueueCommand.h"

class ForceWaveCommand : public ForcePowersQueueCommand {
public:

	ForceWaveCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		return doCombatAction(creature, target);
	}

};

#endif //FORCEWAVECOMMAND_H_
