/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef LETHALSHOTCOMMAND_H_
#define LETHALSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class LethalShotCommand : public CombatQueueCommand {
public:

	LethalShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //OVERCHARGESHOT1COMMAND_H_
