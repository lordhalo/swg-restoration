/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef OVERKILLSHOT2COMMAND_H_
#define OVERKILLSHOT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "CombatQueueCommand.h"

class OverKillShot2Command : public CombatQueueCommand {
public:
	String cdName;
	OverKillShot2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
		cdName = "overKillShot";
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(cdName)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
			}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 60, cdName, "overkillshot_stop");
			creature->addPendingTask(cdName, cdGroup, 1);
			creature->showFlyText("combat_effects", "overkillshot_start", 0, 0, 255, true);

		}
		return SUCCESS;
	}

};

#endif //OverkillShotCommand_H_

