#ifndef FORCEINFUSIONCOMMAND_H_
#define FORCEINFUSIONCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/ForceHealHotTask.h"
#include "ForceHealQueueCommand.h"

class ForceInfusionCommand : public ForceHealQueueCommand {
public:

	ForceInfusionCommand(const String& name, ZoneProcessServer* server)
		: ForceHealQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		int result = doCommonMedicalCommandChecks(creature);

		if (result != SUCCESS)
			return result;

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		Reference<ForceHealHotTask*> healCheck = creature->getPendingTask("forceHealTickTask").castTo<ForceHealHotTask*>();

		if(creature->getPendingTask("forceHealTickTask"))
				return INVALIDSTATE;

		int action = 1000;
		int aCost = (action * 0.07);

		int mind = 1000;
		int mCost = (mind * 0.12);

		int currentForce = playerObject->getForcePower();
		int forceCost = 10;

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return false;
		}

		if (playerObject && playerObject->getForcePower() < forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.
			return GENERALERROR;
		}


		int rankMod = 0;
		if(creature->getSkillMod("rank_defense") > 1) {
			rankMod = (creature->getSkillMod("rank_defense") * 5);
		}

		creature->playEffect("clienteffect/pl_force_heal_self.cef", "");

		if (playerObject != NULL)
			playerObject->setForcePower(playerObject->getForcePower() - forceCost);

		creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);

		int healAmount = 150 + rankMod;

		String buffname = "jedi.force_infusion";
		uint32 buffcrc = buffname.hashCode();
		Buff* infusion = new Buff(creature, buffcrc, 24, BuffType::SKILL);

		Locker locker(infusion);

		creature->addBuff(infusion);

		Reference<ForceHealHotTask*> hotHeal = new ForceHealHotTask(creature, healAmount, 24);
		creature->addPendingTask("forceHealTickTask", hotHeal, 6000);

			return SUCCESS;
		}

};

#endif //FORCEINFUSIONCOMMAND_H_
