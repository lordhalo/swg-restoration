
#ifndef SABER1HBLAZECOMMAND_H_
#define SABER1HBLAZECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class Saber1hBlazeCommand : public CombatQueueCommand {
public:

	Saber1hBlazeCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<WeaponObject*> weapon = creature->getWeapon();

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		if (!weapon->isJediOneHandedWeapon()) {
			return INVALIDWEAPON;
		}

		return doCombatAction(creature, target);
	}

};

#endif //SABER1HBLAZECOMMAND_H_
