/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEEXTINGUISH2COMMAND_H_
#define FORCEEXTINGUISH2COMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceExtinguish2Command : public ForceHealQueueCommand {
public:

	ForceExtinguish2Command(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
