#ifndef STASISCOMMAND_H_
#define STASISCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "ForcePowersQueueCommand.h"

class StasisCommand : public ForcePowersQueueCommand {
	String buffname;
	uint32 buffcrc;
public:

	StasisCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {
		buffname = "mezeroot";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
				if(object->isCreature() || object->isPlayerCreature()){
						ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

						Locker clocker(creatureTarget, creature);

					if (creature != NULL && !creature->hasBuff(buffcrc) && creatureTarget->checkRootRecovery()) {
						ManagedReference<Buff*> buff = new Buff(creatureTarget, buffcrc, 40, BuffType::SKILL);
						Locker locker(buff);
						buff->setStartFlyText("combat_effects", "stasis_start", 255, 0, 0);
						buff->setEndFlyText("combat_effects", "stasis_end", 0, 0, 255);

						creatureTarget->addBuff(buff);
						creatureTarget->playEffect("clienteffect/state_rooted_heavy.cef", "");
						creatureTarget->setRootedState(40);
						creatureTarget->updateRootRecovery();
						creatureTarget->updateRootDelay(50000);
						creatureTarget->clearCombatState();
						creature->clearCombatState();
					}
				}
		}
		return SUCCESS;
	}

};

#endif //STASISCOMMAND_H_
