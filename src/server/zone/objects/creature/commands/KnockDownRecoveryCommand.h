/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef KNOCKDOWNRECOVERYCOMMAND_H_
#define KNOCKDOWNRECOVERYCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/packets/chat/ChatSystemMessage.h"
#include "templates/params/creature/CreatureAttribute.h"

class KnockDownRecoveryCommand : public QueueCommand {
public:
	float actionCost;
	KnockDownRecoveryCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 300;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isKnockedDown()) {
			return GENERALERROR;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		if(!creature->isDead() || !creature->isIncapacitated()){
			creature->setPosture(CreaturePosture::UPRIGHT, true, true);
			creature->inflictDamage(creature, CreatureAttribute::ACTION, 300, false);
		}

		return SUCCESS;
	}

};

#endif //KnockDownRecoveryCommand_H_

