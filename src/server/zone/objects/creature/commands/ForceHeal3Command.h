/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEHEAL3COMMAND_H_
#define FORCEHEAL3COMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceHeal3Command : public ForceHealQueueCommand {
public:

	ForceHeal3Command(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
