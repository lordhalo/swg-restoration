/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCERUNCOMMAND_H_
#define FORCERUNCOMMAND_H_

#include "server/zone/objects/player/events/ForceRunTick.h"
#include "JediQueueCommand.h"

class ForceRunCommand : public JediQueueCommand {
public:

	ForceRunCommand(const String& name, ZoneProcessServer* server)
	: JediQueueCommand(name, server) {
		// BuffCRC's, first one is used.
		buffCRC = BuffCRC::JEDI_FORCE_RUN_1;

        // If these are active they will block buff use
		blockingCRCs.add(BuffCRC::JEDI_FORCE_RUN_2);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_RUN_3);


		// Skill mods.
		skillMods.put("force_run", 1);
		skillMods.put("slope_move", 33);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		int res = doJediSelfBuffCommand(creature);

		// Return if something errored.
		if (res != SUCCESS) {
			return res;
		}

		// SPECIAL - For Force Run.
		if (creature->hasBuff(STRING_HASHCODE("burstrun")) || creature->hasBuff(STRING_HASHCODE("retreat"))) {
			creature->removeBuff(STRING_HASHCODE("burstrun"));
			creature->removeBuff(STRING_HASHCODE("retreat"));
		}

		if(creature->isSnared()){
			creature->clearState(CreatureState::IMMOBILIZED);
		}

		Reference<ForceRunTick*> runTask = new ForceRunTick(creature, 1);
		creature->addPendingTask("runCheck", runTask, 1000);

		// Return.
		return SUCCESS;
	}

};

#endif //FORCERUN1COMMAND_H_
