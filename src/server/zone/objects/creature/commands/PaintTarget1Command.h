/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef PAINTTARGET1COMMAND_H_
#define PAINTTARGET1COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "CombatQueueCommand.h"

class PaintTarget1Command : public CombatQueueCommand {
public:

	PaintTarget1Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
			}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL && !creatureTarget->hasBuff(getNameCRC())) {

				Locker clocker(creatureTarget, creature);

				/*ManagedReference<Buff*> buff = new Buff(creatureTarget, getNameCRC(), 30, BuffType::SKILL);
				Locker locker(buff);
				buff->setStartFlyText("cmd_n", name, 255, 0, 0);
				buff->setEndFlyText("cmd_n", name, 0, 0, 255);
				buff->setSkillModifier("armor_break", 20);

				creatureTarget->addBuff(buff);
				creatureTarget->playEffect("clienteffect/pl_armorbreak.cef", "");
				*/
				creatureTarget->setArmorBreakState(15);

				Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 15, name, "paint_target_1_stop");
				creature->addPendingTask(name, cdGroup, 1);
				creature->showFlyText("combat_effects", "paint_target_1_start", 0, 0, 255, true);

			} else {
				Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 15, name, "paint_target_1_stop");
				creature->addPendingTask(name, cdGroup, 1);
				creature->showFlyText("combat_effects", "paint_target_1_start", 0, 0, 255, true);
			}

		}
		return SUCCESS;
	}

};

#endif //PAINTTARGET1COMMAND_H_
