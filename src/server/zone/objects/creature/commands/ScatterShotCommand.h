/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SCATTERSHOTCOMMAND_H_
#define SCATTERSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class ScatterShotCommand : public CombatQueueCommand {
public:

	ScatterShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;
		
		return doCombatAction(creature, target);
	}

};

#endif //SCATTERSHOT1COMMAND_H_
