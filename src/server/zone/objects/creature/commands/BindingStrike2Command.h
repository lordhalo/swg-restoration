/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef BINDINGSTRIKE2COMMAND_H_
#define BINDINGSTRIKE2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"


class BindingStrike2Command : public CombatQueueCommand {
public:

	BindingStrike2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL && !creatureTarget->hasBuff(getNameCRC())) {
					Locker clocker(creatureTarget, creature);

					ManagedReference<Buff*> buff = new Buff(creatureTarget, getNameCRC(), 45, BuffType::SKILL);

					Locker locker(buff);

					buff->setSkillModifier("binding_strike", 250);
					buff->setStartFlyText("cmd_n", name, 255, 0, 0);
					buff->setEndFlyText("cmd_n", name, 0, 0, 255);

					creatureTarget->addBuff(buff);

				}
		}
		return SUCCESS;
	}

};

#endif //BleedAttackCommand_H_
