/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABER2HPHANTOMCOMMAND_H_
#define SABER2HPHANTOMCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class Saber2hPhantomCommand : public CombatQueueCommand {
public:

	Saber2hPhantomCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			float timer = 8.0;
			float reduction = (creature->getSkillMod("twohandlightsaber_speed") / 1000.f);
			timer -= reduction;

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, timer, name, "saber_2h_phantom_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "saber_2h_phantom_start", 0, 0, 255, true);
		}

		return SUCCESS;
	}

};

#endif //SABER2HPHANTOMCOMMAND_H_
