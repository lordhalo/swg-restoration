/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERLEGHITCOMMAND_H_
#define SABERLEGHITCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class SaberLegHitCommand : public CombatQueueCommand {
public:

	SaberLegHitCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		return doCombatAction(creature, target);
	}

};

#endif //SaberHitCommand_H_
