/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SWORDFLURRY2COMMAND_H_
#define SWORDFLURRY2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class SwordFlurry2Command : public CombatQueueCommand {
public:

	SwordFlurry2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;


		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			if (!creature->hasBuff(STRING_HASHCODE("swordflurry"))) {

			Buff* flurry = new Buff(creature, STRING_HASHCODE("swordflurry"), 10, BuffType::SKILL);

			Locker locker(flurry);

			flurry->setSkillModifier("melee_defense", 120);
			flurry->setSkillModifier("ranged_defense", 120);

			flurry->setStartFlyText("combat_effects", "swordflurry_start", 0, 255, 0);
			flurry->setEndFlyText("combat_effects", "swordflurry_stop", 255, 0, 0);

			creature->addBuff(flurry);

			}
		}

		return SUCCESS;
	}

};

#endif //SwordFlurry2Command_H_
