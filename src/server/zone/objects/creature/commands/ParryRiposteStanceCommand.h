/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef PARRYRIPOSTESTANCECOMMAND_H_
#define PARRYRIPOSTESTANCECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"


class ParryRiposteStanceCommand : public QueueCommand {
	float actionCost;
public:

	ParryRiposteStanceCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 200;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		PlayerObject* ghost = creature->getPlayerObject();

		if (creature->hasBuff(STRING_HASHCODE("parryripostestance"))) {
			creature->sendSystemMessage("@combat_effects:already_centered");
			return GENERALERROR;
		}

		WeaponObject* weapon = creature->getWeapon();

		int duration = 5;
		int efficacy = 80;

		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);

		Buff* parryStance = new Buff(creature, STRING_HASHCODE("parryripostestance"), duration, BuffType::SKILL);

		Locker locker(parryStance);

		parryStance->setSkillModifier("melee_defense", efficacy);
		parryStance->setSkillModifier("ranged_defense", efficacy);
		parryStance->setSkillModifier("private_melee_dmg_shield", 15);

		/*StringIdChatParameter startMsg("combat_effects", "parryriposte_start");
		StringIdChatParameter endMsg("combat_effects", "parryriposte_stop");
		parryStance->setStartMessage(startMsg);
		parryStance->setEndMessage(endMsg);
		*/
		parryStance->setStartFlyText("combat_effects", "parryriposte_start_fly", 0, 0, 255);
		parryStance->setEndFlyText("combat_effects", "parryriposte_stop_fly", 0, 255, 0);

		creature->addBuff(parryStance);

		return SUCCESS;
	}

};

#endif //ParryRiposteStanceCommand_H_
