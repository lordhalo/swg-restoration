/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SWORDFLURRYCOMMAND_H_
#define SWORDFLURRYCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class SwordFlurryCommand : public CombatQueueCommand {
public:

	SwordFlurryCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;



		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			if (!creature->hasBuff(STRING_HASHCODE("swordflurry"))) {

			Buff* flurry = new Buff(creature, STRING_HASHCODE("swordflurry"), 10, BuffType::SKILL);

			Locker locker(flurry);

			flurry->setSkillModifier("melee_defense", 80);
			flurry->setSkillModifier("ranged_defense", 80);

			flurry->setStartFlyText("combat_effects", "swordflurry_start", 0, 255, 0);
			flurry->setEndFlyText("combat_effects", "swordflurry_stop", 255, 0, 0);

			creature->addBuff(flurry);

			}
		}

		return SUCCESS;
	}

};

#endif //SwordFlurryCommand_H_
