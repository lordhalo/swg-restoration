/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEARMORCOMMAND_H_
#define FORCEARMORCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/SingleUseBuff.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceArmorCommand : public QueueCommand {
public:

	ForceArmorCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		uint32 buffcrc1 = BuffCRC::JEDI_FORCE_ARMOR_1;
		uint32 buffcrc2 = BuffCRC::JEDI_FORCE_ARMOR_2;

		if(creature->hasBuff(buffcrc1) || creature->hasBuff(buffcrc2)) {
			creature->sendSystemMessage("@jedi_spam:force_buff_present");
			creature->removeBuff(buffcrc1);
			return GENERALERROR;
		}


		// Force cost of skill.
		int forceCost = 75;
		int action = 1000;
		float actionCost = (2 / 100.f);

		int aCost = (action * actionCost);

		int mind = 1000;
		float mindCost = (2 / 100.f);

		int mCost = (mind * mindCost);


		//Check for and deduct Force cost.

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();


		if (playerObject->getForcePower() <= forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->sendSystemMessage("Too Tired"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		playerObject->setForcePower(playerObject->getForcePower() - forceCost);
		creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);

		StringIdChatParameter startStringId("jedi_spam", "apply_forcearmor1");
		StringIdChatParameter endStringId("jedi_spam", "remove_forcearmor1");

		int duration = 900;
		//int skillMod = 25;
		int skillMod = 20;
		//int forceDefense = (creature->getSkillMod("rank_defence") / 10.f);

		int forceDefense = creature->getSkillMod("rank_defense");

		if(forceDefense > 0){
			forceDefense /= 10.f;
		}

		skillMod += forceDefense;

		if(skillMod > 99){
			skillMod = 99;
		}

		Vector<unsigned int> eventTypes;
		eventTypes.add(ObserverEventType::FORCEBUFFHITARMOR);

		ManagedReference<SingleUseBuff*> buff = new SingleUseBuff(creature, buffcrc1, duration, BuffType::JEDI, getNameCRC());

		Locker locker(buff);

		buff->setStartMessage(startStringId);
		buff->setEndMessage(endStringId);
		buff->setSkillModifier("force_armor", skillMod);
		buff->init(&eventTypes);

		creature->addBuff(buff);
		creature->playEffect("clienteffect/pl_force_armor_self.cef", "");
		VisibilityManager::instance()->increaseVisibility(creature, 1);
		//creature->doAnimation("force_protection");

		return SUCCESS;
	}

	void handleBuff(SceneObject* creature, ManagedObject* object, int64 param) {

		ManagedReference<CreatureObject*> creo = cast<CreatureObject*>( creature);
		if (creo == NULL)
			return;

		// Client Effect upon hit (needed)
		creo->playEffect("clienteffect/pl_force_armor_hit.cef", "");

		ManagedReference<PlayerObject*> playerObject = creo->getPlayerObject();
		if (playerObject == NULL)
			return;

		int rankMod = creo->getSkillMod("rank_control");

		if(rankMod > 0){
			rankMod /= 10;
		}

		if(rankMod <= 0){
			rankMod = 1;
		}

		int amount = param / rankMod;
		if (amount < 1) amount = 1;
		//7/26 Backup int forceCost = amount * 0.3;
		int forceCost = amount * 0.12;
		if (forceCost < 1) forceCost = 1;
		if (playerObject->getForcePower() <= forceCost) { // Remove buff if not enough force.
			Buff* buff = creo->getBuff(BuffCRC::JEDI_FORCE_ARMOR_1);
			if (buff != NULL) {
				Locker locker(buff);

				creo->removeBuff(buff);
			}
		} else
			playerObject->setForcePower(playerObject->getForcePower() - forceCost);
	}

};

#endif //FORCEARMOR1COMMAND_H_
