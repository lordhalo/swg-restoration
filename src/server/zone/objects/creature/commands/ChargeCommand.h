/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef CHARGECOMMAND_H_
#define CHARGECOMMAND_H_

#include "SquadLeaderCommand.h"

class ChargeCommand : public SquadLeaderCommand {
	String buffname;
	uint32 buffcrc;
public:

	ChargeCommand(const String& name, ZoneProcessServer* server)
		: SquadLeaderCommand(name, server) {
		buffname = "slBuff";
		buffcrc = buffname.hashCode();
	}

	bool checkBuffs(CreatureObject* creature) const {
		if (creature->isRidingMount()) {
			creature->sendSystemMessage("@cbt_spam:no_burst"); // You cannot burst-run while mounted on a creature or vehicle.
			return false;
		}

		Zone* zone = creature->getZone();

		if (zone == NULL) {
			return false;
		}

		/*if (zone->getZoneName() == "dungeon1") {
			creature->sendSystemMessage("@combat_effects:burst_run_space_dungeon"); //The artificial gravity makes burst running impossible here.
			return false;
		}*/

		uint32 burstCRC = STRING_HASHCODE("burstrun");
		uint32 forceRun1CRC = BuffCRC::JEDI_FORCE_RUN_1;
		uint32 forceRun2CRC = BuffCRC::JEDI_FORCE_RUN_2;
		uint32 forceRun3CRC = BuffCRC::JEDI_FORCE_RUN_3;

		if (creature->hasBuff(burstCRC) || creature->hasBuff(buffcrc) || creature->hasBuff(forceRun1CRC) || creature->hasBuff(forceRun2CRC) || creature->hasBuff(forceRun3CRC)) {
			creature->sendSystemMessage("@combat_effects:burst_run_no"); //You cannot burst run right now.
			return false;
		}

		if (!creature->checkCooldownRecovery("charge")) {
			creature->sendSystemMessage("@combat_effects:burst_run_no"); //You cannot burst run right now.
			return false;
		}

		return true;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		ManagedReference<CreatureObject*> player = cast<CreatureObject*>(creature);

		if (player == NULL)
			return GENERALERROR;

		ManagedReference<PlayerObject*> ghost = player->getPlayerObject();

		if (ghost == NULL)
			return GENERALERROR;

		ManagedReference<GroupObject*> group = player->getGroup();

		if (!checkGroupLeader(player, group))
			return GENERALERROR;

		float groupBurstRunMod = (float) player->getSkillMod("group_burst_run");
		int hamCost = (int) (100.0f * (1.0f - (groupBurstRunMod / 100.0f))) * calculateGroupModifier(group);

		int actionCost = creature->calculateCostAdjustment(CreatureAttribute::QUICKNESS, hamCost);
		int mindCost = creature->calculateCostAdjustment(CreatureAttribute::FOCUS, hamCost);

		if (!inflictHAM(player, actionCost, mindCost))
			return GENERALERROR;

		for (int i = 1; i < group->getGroupSize(); ++i) {
			ManagedReference<CreatureObject*> member = group->getGroupMember(i);

			if (member == NULL || !member->isPlayerCreature() || member->getZone() != creature->getZone())
				continue;

			if (!isValidGroupAbilityTarget(creature, member, false))
				continue;

			Locker clocker(member, player);

			sendCombatSpam(member);
			doRetreat(member);

			checkForTef(player, member);
		}

		if (!ghost->getCommandMessageString(STRING_HASHCODE("charge")).isEmpty() && creature->checkCooldownRecovery("command_message")) {
			UnicodeString shout(ghost->getCommandMessageString(STRING_HASHCODE("charge")));
			//server->getChatManager()->broadcastChatMessage(player, shout, 0, 80, player->getMoodID(), 0, ghost->getLanguageID());
			creature->updateCooldownTimer("command_message", 30 * 1000);
		}

		return SUCCESS;
	}


	void doRetreat(CreatureObject* player) const {
		if (player == NULL)
			return;

		if (!checkBuffs(player))
			return;


		float groupRunMod = (float) player->getSkillMod("group_burst_run");

		if (groupRunMod > 100.0f)
			groupRunMod = 100.0f;

		StringIdChatParameter startStringId("cbt_spam", "burstrun_start_single");
		StringIdChatParameter endStringId("cbt_spam", "burstrun_stop_single");

		int duration = 30;

		ManagedReference<Buff*> buff = new Buff(player, buffcrc, duration, BuffType::SKILL);

		Locker locker(buff);

		buff->setSpeedMultiplierMod(1.3);
		buff->setAccelerationMultiplierMod(1.3f);
		buff->setStartMessage(startStringId);
		buff->setEndMessage(endStringId);

		player->addBuff(buff);

		player->updateCooldownTimer("charge", 30000);

	}

};

#endif //RETREATCOMMAND_H_
