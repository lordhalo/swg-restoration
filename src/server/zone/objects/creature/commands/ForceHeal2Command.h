/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEHEAL2COMMAND_H_
#define FORCEHEAL2COMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceHeal2Command : public ForceHealQueueCommand {
public:

	ForceHeal2Command(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
