/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef DIZZYATTACK3COMMAND_H_
#define DIZZYATTACK3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class DizzyAttack3Command : public CombatQueueCommand {
public:

	DizzyAttack3Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}
};

#endif //DIZZYATTACKCOMMAND_H_
