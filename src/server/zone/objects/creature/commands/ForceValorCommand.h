/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEVALORCOMMAND_H_
#define FORCEVALORCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceValorCommand : public QueueCommand {
	float actionCost;
	String cdName;
	String buffname;
	uint32 buffcrc;

public:

	ForceValorCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 225;
		cdName = "forceValor";
		buffname = "jedi.force_valor";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		PlayerObject* playerObject = creature->getPlayerObject();
		int forceCost = 150;
		int rankMod = creature->getSkillMod("rank_control");

		forceCost -= rankMod;

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		if (playerObject && playerObject->getForcePower() < forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.
			return GENERALERROR;
		}

		if (creature->hasBuff(buffcrc)) {
			return GENERALERROR;
		}

		int duration = 30;
		int efficacy = 15;
		int currentForce = playerObject->getForcePower();

		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);
		playerObject->setForcePower(currentForce - forceCost);

		Buff* forcevalor = new Buff(creature, buffcrc, duration, BuffType::SKILL);

		Locker locker(forcevalor);

		forcevalor->setSkillModifier("saber_block", efficacy);

		creature->addBuff(forcevalor);


		VisibilityManager::instance()->increaseVisibility(creature, 1);


		return SUCCESS;
	}

};

#endif //CENTEROFBEINGCOMMAND_H_
