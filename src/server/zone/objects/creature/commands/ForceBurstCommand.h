/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEBURSTCOMMAND_H_
#define FORCEBURSTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/Zone.h"

class ForceBurstCommand : public QueueCommand {
public:

	ForceBurstCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}	

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(creature->isSnared()){
			creature->clearState(CreatureState::IMMOBILIZED);
		}

		if (creature->isRidingMount()) {
			creature->sendSystemMessage("@cbt_spam:no_burst"); // You cannot burst-run while mounted on a creature or vehicle.
			return false;
		}

		if (creature->hasBuff(STRING_HASHCODE("gallop")) || creature->hasBuff(STRING_HASHCODE("burstrun")) || creature->hasBuff(STRING_HASHCODE("retreat"))) {
			creature->sendSystemMessage("@combat_effects:burst_run_no"); // You cannot burst run right now.
			return false;
		}

		uint32 forceRun1CRC = BuffCRC::JEDI_FORCE_RUN_1;
		uint32 forceRun2CRC = BuffCRC::JEDI_FORCE_RUN_2;
		uint32 forceRun3CRC = BuffCRC::JEDI_FORCE_RUN_3;

		if(creature->hasBuff(forceRun1CRC) || creature->hasBuff(forceRun2CRC) || creature->hasBuff(forceRun3CRC)) {
			creature->sendSystemMessage("@combat_effects:burst_run_no"); // You cannot burst run right now.
			return false;
		}

		Zone* zone = creature->getZone();

		if (zone == NULL) {
			return false;
		}

		if (zone->getZoneName() == "dungeon1") {
			creature->sendSystemMessage("@combat_effects:burst_run_space_dungeon"); // The artificial gravity makes burst running impossible here.
			return false;
		}

		if (!creature->checkCooldownRecovery("burstrun")) {
			creature->sendSystemMessage("@combat_effects:burst_run_wait"); //You are too tired to Burst Run.
			return false;
		}

		uint32 crc = STRING_HASHCODE("burstrun");
		float duration = 30;
		float cooldown = 300;

		//creature->playEffect("clienteffect/pl_force_run_self.cef", "");

		StringIdChatParameter startStringId("cbt_spam", "burstrun_start_single");
		StringIdChatParameter modifiedStartStringId("combat_effects", "instant_burst_run");
		StringIdChatParameter endStringId("cbt_spam", "burstrun_stop_single");

		ManagedReference<Buff*> buff = new Buff(creature, crc, duration, BuffType::SKILL);

		Locker locker(buff);

		buff->setSpeedMultiplierMod(1.85f);
		buff->setAccelerationMultiplierMod(2.15f);

		StringIdChatParameter startSpam("cbt_spam", "burstrun_start");
		StringIdChatParameter endSpam("cbt_spam", "burstrun_stop");
		buff->setStartSpam(startSpam);
		buff->setEndSpam(endSpam);
		buff->setBroadcastSpam(true);

		creature->addBuff(buff);

		return SUCCESS;
	}

};

#endif //BURSTRUNCOMMAND_H_
