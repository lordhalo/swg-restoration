/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef DECONTAMINATECOMMAND_H_
#define DECONTAMINATECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/tangible/pharmaceutical/StimPack.h"
#include "server/zone/objects/tangible/pharmaceutical/RangedStimPack.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/creature/events/InjuryTreatmentTask.h"
#include "server/zone/objects/creature/buffs/Buff.h"
#include "server/zone/objects/creature/buffs/DelayedBuff.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/managers/collision/CollisionManager.h"

class DecontaminateCommand : public QueueCommand {
public:

	DecontaminateCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
	}

	void doAnimations(CreatureObject* creature, CreatureObject* creatureTarget) const {
		creatureTarget->playEffect("clienteffect/healing_healdamageaoe.cef", "");

		if (creature == creatureTarget)
			creature->doAnimation("heal_self");
		else
			creature->doAnimation("heal_other");
	}

	bool checkTarget(CreatureObject* creature, CreatureObject* creatureTarget) const {
		if (!creatureTarget->isPoisoned()) {
			return false;
		}

		PlayerManager* playerManager = server->getPlayerManager();

		if (creature != creatureTarget && !CollisionManager::checkLineOfSight(creature, creatureTarget)) {
			return false;
		}

		if (!creatureTarget->isHealableBy(creature)) {
			return false;
		}

		if (creatureTarget->isDead())
			return false;

		checkForTef(creature, creatureTarget);

		return true;
	}

	int calculateActionCost(CreatureObject* creature) const {
		int action = 1000;
		float actionCost = (10 / 100.f);

		int aCost = (action * actionCost);
		//int battleFat = creature->getShockWounds();

		//if(battleFat >= 100)
		//	baseCost += (battleFat / 2);

		return aCost;

	}

	int calculateMindCost(CreatureObject* creature) const {
		int mind = 1000;
		float mindCost = (18 / 100.f);

		int mCost = (mind * mindCost);
		//int battleFat = creature->getShockWounds();

		//if(battleFat >= 100)
		//	baseCost += (battleFat / 2);

		return mCost;

	}

	void doAreaMedicActionTarget(CreatureObject* creature, CreatureObject* targetCreature) const {
			uint32 skillMod = 25 + creature->getSkillMod("cure_efficiency");

			if(targetCreature->isDiseased())
				targetCreature->healDot(CreatureState::DISEASED, skillMod);
	}

	void handleArea(CreatureObject* creature, CreatureObject* areaCenter, float range) const {

		Zone* zone = creature->getZone();

		if (zone == NULL)
			return;

		try {
			//zone->rlock();

			CloseObjectsVector* closeObjectsVector = (CloseObjectsVector*) areaCenter->getCloseObjects();

			SortedVector<ManagedReference<QuadTreeEntry*> > closeObjects;
			closeObjectsVector->safeCopyTo(closeObjects);

			for (int i = 0; i < closeObjects.size(); i++) {
				SceneObject* object = cast<SceneObject*>( closeObjects.get(i).get());

				if (!object->isPlayerCreature() && !object->isPet())
					continue;

				if (object == areaCenter || object->isDroidObject())
					continue;

				if (!areaCenter->isInRange(object, range))
					continue;

				CreatureObject* creatureTarget = cast<CreatureObject*>( object);
				//zone->runlock();

				try {

					Locker crossLocker(creatureTarget, creature);

					if (checkTarget(creature, creatureTarget)) {
						doAreaMedicActionTarget(creature, creatureTarget);
					}

				} catch (Exception& e) {

				}

				//zone->rlock();

			}

			//zone->runlock();
		} catch (Exception& e) {
			//zone->runlock();
		}
	}

	float getCommandDuration(CreatureObject* object, const UnicodeString& arguments) const {
		return 4.0;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		int result = doCommonMedicalCommandChecks(creature);

		if (result != SUCCESS)
			return result;

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);

		if (object != NULL) {
			if (!object->isCreatureObject()) {
				TangibleObject* tangibleObject = dynamic_cast<TangibleObject*>(object.get());

				if (tangibleObject != NULL && tangibleObject->isAttackableBy(creature)) {
					object = creature;
				} else {
					creature->sendSystemMessage("@healing_response:healing_response_79"); //Target must be a player or a creature pet in order to apply first aid.
					return GENERALERROR;
				}
			}
		} else {
			object = creature;
		}

		CreatureObject* targetCreature = creature;//cast<CreatureObject*>( object.get());

		Locker clocker(targetCreature, creature);

		if ((targetCreature->isAiAgent() && !targetCreature->isPet()) || targetCreature->isDroidObject() || targetCreature->isDead() || targetCreature->isRidingMount() || targetCreature->isAttackableBy(creature))
			targetCreature = creature;

		if (creature->getHAM(CreatureAttribute::ACTION) < (calculateActionCost(creature)) || creature->getHAM(CreatureAttribute::MIND) < (calculateMindCost(creature))) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return false;
		}

		uint32 skillMod = 25 + creature->getSkillMod("cure_efficiency");

		targetCreature->healDot(CreatureState::DISEASED, skillMod);

		creature->inflictDamage(creature, CreatureAttribute::ACTION, calculateActionCost(creature), false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, calculateMindCost(creature), false);

		if (targetCreature != creature)
			clocker.release();

		handleArea(creature, targetCreature, 12);


		doAnimations(creature, targetCreature);

		checkForTef(creature, targetCreature);

		return SUCCESS;
	}

};

#endif //CounterToxinCommand_H_
