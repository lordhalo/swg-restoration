/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef LASTDITCHCOMMAND_H_
#define LASTDITCHCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/StateBuff.h"
#include "CombatQueueCommand.h"

class LastDitchCommand : public CombatQueueCommand {
public:

	LastDitchCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
			}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {
			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 60, name, "lastditch_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "lastditch_start", 0, 0, 255, true);
		}

		return SUCCESS;


	}

};

#endif //LASTDITCHCOMMAND_H_
