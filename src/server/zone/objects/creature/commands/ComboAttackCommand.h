/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef COMBOATTACKCOMMAND_H_
#define COMBOATTACKCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"


class ComboAttackCommand : public CombatQueueCommand {
public:

	ComboAttackCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //BleedAttackCommand_H_
