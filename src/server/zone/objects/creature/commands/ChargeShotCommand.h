/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef CHARGESHOTCOMMAND_H_
#define CHARGESHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class ChargeShotCommand : public CombatQueueCommand {
public:

	ChargeShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //CHARGESHOT1COMMAND_H_
