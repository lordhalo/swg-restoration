/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEAURACOMMAND_H_
#define FORCEAURACOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceAuraCommand : public QueueCommand {
	float actionCost;
	String cdName;
	String buffname;
	uint32 buffcrc;
	String buffname2;
	uint32 buffcrc2;

public:

	ForceAuraCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 225;
		cdName = "forceAura";
		buffname = "jedi.force_aura";
		buffcrc = buffname.hashCode();
		buffname2 = "melee.center_of_being";
		buffcrc2 = buffname2.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		PlayerObject* playerObject = creature->getPlayerObject();
		int forceCost = 200;
		int rankMod = creature->getSkillMod("rank_control");

		forceCost -= rankMod;

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		if (playerObject && playerObject->getForcePower() < forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.
			return GENERALERROR;
		}

		if (creature->hasBuff(buffcrc) || creature->hasBuff(buffcrc2)) {
			//creature->sendSystemMessage("@combat_effects:already_centered");
			return GENERALERROR;
		}

		WeaponObject* weapon = creature->getWeapon();

		if (creature->getLocomotion() != 0)
			return GENERALERROR;

		if (weapon->isRangedWeapon())
			return GENERALERROR;

		int duration = 30;
		int forceDefense = creature->getSkillMod("rank_defense");
		int efficacy = 250 + forceDefense;
		int currentForce = playerObject->getForcePower();

		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);
		playerObject->setForcePower(currentForce - forceCost);

		Buff* centered = new Buff(creature, buffcrc, duration, BuffType::SKILL);

		Locker locker(centered);

		centered->setSkillModifier("private_defense_bonus", efficacy);

		creature->addBuff(centered);
		creature->playEffect("clienteffect/pl_force_shield_self.cef", "");

		VisibilityManager::instance()->increaseVisibility(creature, 1);


		return SUCCESS;
	}

};

#endif //CENTEROFBEINGCOMMAND_H_
