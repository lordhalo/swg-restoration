/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef HEALSTATESCOMMAND_H_
#define HEALSTATESCOMMAND_H_

#include "ForceHealQueueCommand.h"

class HealStatesCommand : public ForceHealQueueCommand {
public:

	HealStatesCommand(const String& name, ZoneProcessServer* server)
		: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALSTATESSELFCOMMAND_H_
