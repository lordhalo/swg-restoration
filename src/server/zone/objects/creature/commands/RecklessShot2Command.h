/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef RECKLESSSHOT2COMMAND_H_
#define RECKLESSSHOT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/StateBuff.h"
#include "CombatQueueCommand.h"

class RecklessShot2Command : public CombatQueueCommand {
	String cdName;
	String buffname;
	uint32 buffcrc;
public:

	RecklessShot2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
		cdName = "recklessShot";
		buffname = "recklessShot";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> targetCreature = cast<CreatureObject*>( object.get());

			if (targetCreature != NULL && !targetCreature->hasBuff(buffcrc)){
				Locker clocker(targetCreature);

				ManagedReference<Buff*> buff = new Buff(targetCreature, buffcrc, 10, BuffType::SKILL);
				Locker locker(buff);
				buff->setStartFlyText("cmd_n", name, 255, 0, 0);
				buff->setEndFlyText("cmd_n", name, 0, 0, 255);
				buff->setSkillModifier("private_defense_penalty", 80);

				targetCreature->addBuff(buff);
				//targetCreature->playEffect("clienteffect/pl_debuff_overwelming_heavy.cef", "");

			}

		}

		return SUCCESS;


	}

};

#endif //LASTDITCHCOMMAND_H_
