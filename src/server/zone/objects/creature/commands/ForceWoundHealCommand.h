/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEWOUNDHEALCOMMAND_H_
#define FORCEWOUNDHEALCOMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceWoundHealCommand : public ForceHealQueueCommand {
public:

	ForceWoundHealCommand(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
