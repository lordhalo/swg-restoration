/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEWEAKEN2COMMAND_H_
#define FORCEWEAKEN2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "ForcePowersQueueCommand.h"

class ForceWeaken2Command : public ForcePowersQueueCommand {
public:
	String buffname;
	uint32 buffcrc;
	ForceWeaken2Command(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {
		buffname = "forceweaken";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL && !creatureTarget->hasBuff(buffcrc)) {
				Locker clocker(creatureTarget, creature);

				ManagedReference<Buff*> buff = new Buff(creatureTarget, buffcrc, 20, BuffType::JEDI);

				Locker locker(buff);

				buff->setSkillModifier("private_damage_susceptibility", 20);
				buff->setStartFlyText("cmd_n", name, 255, 0, 0);
				buff->setEndFlyText("cmd_n", name, 0, 0, 255);

				creatureTarget->addBuff(buff);
			}

		}

		return SUCCESS;
	}

};

#endif //FORCEWEAKEN2COMMAND_H_
