/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABER1HFLURRY2COMMAND_H_
#define SABER1HFLURRY2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class Saber1hFlurry2Command : public CombatQueueCommand {
public:

	Saber1hFlurry2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			float timer = 8.0;
			float reduction = (creature->getSkillMod("onehandlightsaber_speed") / 1000.f);
			timer -= reduction;

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, timer, name, "saber_1h_flurry2_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "saber_1h_flurry2_start", 0, 0, 255, true);
		}

		return SUCCESS;
	}

};

#endif //SABER1HFLURRY2COMMAND_H_
