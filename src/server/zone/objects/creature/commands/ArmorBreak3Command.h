/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef ARMORBREAK3COMMAND_H_
#define ARMORBREAK3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class ArmorBreak3Command : public CombatQueueCommand {
public:

	ArmorBreak3Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		return doCombatAction(creature, target);
	}

};

#endif //MELEE2HHIT3COMMAND_H_
