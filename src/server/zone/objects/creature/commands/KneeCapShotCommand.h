/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef KNEECAPSHOTCOMMAND_H_
#define KNEECAPSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class KneeCapShotCommand : public CombatQueueCommand {
public:

	KneeCapShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //KneeCapShotCommand_H_
