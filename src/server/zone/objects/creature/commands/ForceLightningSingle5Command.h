

#ifndef FORCELIGHTNINGSINGLE5COMMAND_H_
#define FORCELIGHTNINGSINGLE5COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "ForcePowersQueueCommand.h"

class ForceLightningSingle5Command : public ForcePowersQueueCommand {
public:

	ForceLightningSingle5Command(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		float mods[3] = {0.f, 0.f, 0.f};

		for (int i = 0; i < 2; i++)
			mods[System::random(2)] += 0.5f;

		UnicodeString args = "healthDamageMultiplier=" + String::valueOf(mods[0]) + ";actionDamageMultiplier=" + String::valueOf(mods[1]) + ";mindDamageMultiplier=" + String::valueOf(mods[2]) + ";";

		return doCombatAction(creature, target, args);
	}

};

#endif //FORCELIGHTNINGSINGLE5COMMAND_H_
