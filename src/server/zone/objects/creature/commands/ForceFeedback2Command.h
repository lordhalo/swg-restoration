/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEFEEDBACK2COMMAND_H_
#define FORCEFEEDBACK2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/SingleUseBuff.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceFeedback2Command : public QueueCommand {
public:

	ForceFeedback2Command(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;


		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		uint32 buffcrc1 = BuffCRC::JEDI_FORCE_FEEDBACK_1;
		uint32 buffcrc2 = BuffCRC::JEDI_FORCE_FEEDBACK_2;

		if(creature->hasBuff(buffcrc1) || creature->hasBuff(buffcrc2)) {
			creature->sendSystemMessage("@jedi_spam:force_buff_present");
			return GENERALERROR;
		}

		// Force cost of skill.
		int forceCost = 100;
		int action = 1000;
		float actionCost = (2 / 100.f);

		int aCost = (action * actionCost);

		int mind = 1000;
		float mindCost = (2 / 100.f);

		int mCost = (mind * mindCost);

		//Check for and deduct Force cost.
		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

		if (playerObject->getForcePower() <= forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->sendSystemMessage("Too Tired"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		playerObject->setForcePower(playerObject->getForcePower() - forceCost);
		creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);

		StringIdChatParameter startStringId("jedi_spam", "apply_forcefeedback2");
		StringIdChatParameter endStringId("jedi_spam", "remove_forcefeedback2");

		int duration = 60;

		Vector<unsigned int> eventTypes;
		eventTypes.add(ObserverEventType::FORCEBUFFHIT);

		ManagedReference<SingleUseBuff*> buff = new SingleUseBuff(creature, buffcrc2, duration, BuffType::JEDI, getNameCRC());

		Locker locker(buff);

		buff->setStartMessage(startStringId);
		buff->setEndMessage(endStringId);
		buff->setSkillModifier("force_feedback", 50);
		buff->init(&eventTypes);

		creature->addBuff(buff);
		creature->playEffect("clienteffect/pl_force_feedback_self.cef", "");

		VisibilityManager::instance()->increaseVisibility(creature, 1);

		return SUCCESS;
	}

	void handleBuff(SceneObject* creature, ManagedObject* object, int64 param) {
		ManagedReference<CreatureObject*> creo = cast<CreatureObject*>( creature);
		if (creo == NULL)
			return;

		// Client Effect upon hit (needed)
		creo->playEffect("clienteffect/pl_force_feedback_block.cef", "");
	}

};

#endif //FORCEFEEDBACK2COMMAND_H_
