/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEHEALCOMMAND_H_
#define FORCEHEALCOMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceHealCommand : public ForceHealQueueCommand {
public:

	ForceHealCommand(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
