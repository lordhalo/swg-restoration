/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef DIZZYATTAC2KCOMMAND_H_
#define DIZZYATTACK2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class DizzyAttack2Command : public CombatQueueCommand {
public:

	DizzyAttack2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}
};

#endif //DIZZYATTACKCOMMAND_H_
