/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef CALLEDSHOTCOMMAND_H_
#define CALLEDSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class CalledShotCommand : public CombatQueueCommand {
public:

	CalledShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->getGroup() == NULL)
			return INVALIDSTATE;

		return doCombatAction(creature, target);
	}

};

#endif //MELEE2HHIT3COMMAND_H_
