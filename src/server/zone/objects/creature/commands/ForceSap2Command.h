#ifndef FORCESAP2COMMAND_H_
#define FORCESAP2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "ForcePowersQueueCommand.h"

class ForceSap2Command : public CombatQueueCommand {
	String buffname;
	uint32 buffcrc;
	float mindCost;
	float actionCost;
	int forceCost;
public:

	ForceSap2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
		buffname = "forcesap";
		buffcrc = buffname.hashCode();
		actionCost = (12 / 100.f);
		mindCost = (4 / 100.f);
		forceCost = 140;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

		int action = 1000;
		int aCost = (action * actionCost);

		int mind = 1000;
		int mCost = (mind * mindCost);

		int currentForce = playerObject->getForcePower();

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return false;
		}

		if (playerObject && playerObject->getForcePower() < forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL) {
				if (!creatureTarget->hasBuff(buffcrc)){
					Locker clocker(creatureTarget, creature);

					ManagedReference<Buff*> buff = new Buff(creatureTarget, buffcrc, 30, BuffType::JEDI);

					Locker locker(buff);

					float rankMod = (1.0 + ((creature->getSkillMod("rank_offense") / 2) / 100.f));


					int willPower = 30 * rankMod;
					int stamina = 35 * rankMod;
					buff->setSkillModifier("stamina_debuff", stamina);
					buff->setSkillModifier("willpower_debuff", willPower);

					creatureTarget->addBuff(buff);

					creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
					creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);
					playerObject->setForcePower(currentForce - forceCost);
				} else{
					creature->sendSystemMessage("Target is already under the effects of this Debuff");
				}
			}

		}

		return SUCCESS;
	}

};

#endif //FORCESAPCOMMAND_H_
