/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef MELEEHITCOMMAND_H_
#define MELEEHITCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class MeleeHitCommand : public CombatQueueCommand {
public:

	MeleeHitCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->getWeapon()->isMeleeWeapon())
			return INVALIDWEAPON;

		return doCombatAction(creature, target);
	}
};

#endif //ATTACKCOMMAND_H_
