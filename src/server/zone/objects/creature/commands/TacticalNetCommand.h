#ifndef TACTICALNETCOMMAND_H_
#define TACTICALNETCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "CombatQueueCommand.h"

class TacticalNetCommand : public CombatQueueCommand {
public:

	TacticalNetCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;


		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 180, name, "tactical_net_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "tactical_net_start", 0, 0, 255, true);
		}
		return SUCCESS;
	}

};

#endif
