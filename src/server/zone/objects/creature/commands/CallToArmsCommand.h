/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef CALLTOARMSCOMMAND_H_
#define CALLTOARMSCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/tangible/pharmaceutical/EnhancePack.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/creature/events/InjuryTreatmentTask.h"
#include "server/zone/objects/creature/buffs/Buff.h"
#include "server/zone/objects/creature/BuffAttribute.h"
#include "server/zone/objects/creature/buffs/DelayedBuff.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/managers/collision/CollisionManager.h"

class CallToArmsCommand : public QueueCommand {
	float mindCost;
	float range;
public:

	CallToArmsCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		
		mindCost = 150;
		range = 7;
	}

	bool canPerformSkill(CreatureObject* enhancer, CreatureObject* patient) const {
		if (patient->isDead())
			return false;

		if (!enhancer->canTreatWounds()) {
			enhancer->sendSystemMessage("@healing_response:enhancement_must_wait"); //You must wait before you can heal wounds or apply enhancements again.
			return false;
		}

		if (enhancer->isInCombat()) {
			enhancer->sendSystemMessage("You cannot do that while in Combat.");
			return false;
		}

		if (!patient->isPlayerCreature() && !(patient->isCreature() && patient->isPet())) {
			enhancer->sendSystemMessage("@healing_response:healing_response_77"); //Target must be a player or a creature pet in order to apply enhancements.
			return false;
		}

		if (patient->isInCombat()) {
			enhancer->sendSystemMessage("You cannot do that while your target is in Combat.");
			return false;
		}

		if (enhancer->getHAM(CreatureAttribute::MIND) < mindCost) {
			enhancer->sendSystemMessage("@healing_response:not_enough_mind"); //You do not have enough mind to do that.
			return false;
		}

		if (enhancer != patient && !CollisionManager::checkLineOfSight(enhancer, patient)) {
			enhancer->sendSystemMessage("@container_error_message:container18");
			return false;
		}

		return true;
	}

	void doAnimations(CreatureObject* enhancer, CreatureObject* patient) const {
		patient->playEffect("clienteffect/healing_healenhance.cef", "");

		if (enhancer == patient)
			enhancer->doAnimation("heal_self");
		else
			enhancer->doAnimation("heal_other");
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		int result = doCommonMedicalCommandChecks(creature);

		if (result != SUCCESS)
			return result;

		CreatureObject* enhancer = creature;

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);

		if (object != NULL) {
			if (!object->isCreatureObject()) {
				TangibleObject* tangibleObject = dynamic_cast<TangibleObject*>(object.get());
 
				if (tangibleObject != NULL && tangibleObject->isAttackableBy(creature)) {
					object = creature;
				} else {
					creature->sendSystemMessage("@healing_response:healing_response_77"); //Target must be a player or a creature pet in order to apply enhancements.
					return GENERALERROR;
				}
			}
		} else {
			object = creature;
		}

		CreatureObject* targetCreature = cast<CreatureObject*>( object.get());

		uint8 attribute = BuffAttribute::HEALTH;
		uint64 objectId = 0;

		if (!targetCreature->isInRange(creature, range + targetCreature->getTemplateRadius() + creature->getTemplateRadius()))
			return TOOFAR;

		//parseModifier(arguments.toString(), attribute, objectId);

		if (attribute == BuffAttribute::UNKNOWN) {
			enhancer->sendSystemMessage("@healing_response:healing_response_75"); //You must specify a valid attribute.
			return GENERALERROR;
		}

		ManagedReference<EnhancePack*> enhancePack = NULL;

		if (objectId != 0) {
			SceneObject* inventory = creature->getSlottedObject("inventory");

			if (inventory != NULL) {
				enhancePack = inventory->getContainerObject(objectId).castTo<EnhancePack*>();
			}
		} else {
			enhancePack = findEnhancePack(creature);
		}

		CreatureObject* patient = cast<CreatureObject*>( targetCreature);

		Locker clocker(patient, creature);

		if (patient->isDead() || patient->isRidingMount())
			patient = enhancer;

		if (!canPerformSkill(enhancer, patient))
			return GENERALERROR;

		uint32 currentBuff = 0;
		uint32 buffcrc = 0x98321369;

		if (patient->hasBuff(buffcrc)) {
			Buff* existingbuff = patient->getBuff(buffcrc);

			if (existingbuff != NULL){
				currentBuff = existingbuff->getAttributeModifierValue(attribute);
			}
		}

		//Applies battle fatigue
		int basePower = (patient->getBaseHAM(0) /10.f);
		int creatureSkill = creature->getSkillMod("leadership");

		double buffPower = basePower * (1.0 + (creatureSkill / 100.f));
		if(enhancePack != NULL){
			buffPower += enhancePack->calculatePower(enhancer, patient);
		}

		if (buffPower < currentBuff) {
			if (patient == enhancer)
				enhancer->sendSystemMessage("Your current enhancements are of greater power and cannot be re-applied.");
			else
				enhancer->sendSystemMessage("Your target's current enhancements are of greater power and cannot be re-applied.");

			return 0;
		}

		int duration = 1200;

		PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();

		uint32 amountEnhanced = playerManager->healEnhance(enhancer, patient, attribute, buffPower, duration);

		sendEnhanceMessage(enhancer, patient, attribute, amountEnhanced);

		enhancer->inflictDamage(enhancer, CreatureAttribute::MIND, mindCost, false);

		deactivateWoundTreatment(enhancer);

		if (enhancePack != NULL) {
			Locker locker(enhancePack);
			enhancePack->decreaseUseCount();
		}

		if (patient != enhancer)
			awardXp(enhancer, "medical", amountEnhanced); //No experience for healing yourself.

		doAnimations(enhancer, patient);

		creature->notifyObservers(ObserverEventType::MEDPACKUSED);

		return SUCCESS;
	}

};

#endif //CallToArmsCommand_H_
