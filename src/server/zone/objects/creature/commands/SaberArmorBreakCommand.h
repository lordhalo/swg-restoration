/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERARMORBREAKCOMMAND_H_
#define SABERARMORBREAKCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class SaberArmorBreakCommand : public CombatQueueCommand {
	String cdName;
	String buffname;
	uint32 buffcrc;
public:

	SaberArmorBreakCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
		cdName = "armorBreak";
		buffname = "armorBreak";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);

		/*if (creature->containsPendingTask(cdName)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
			}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL && !creatureTarget->hasBuff(buffcrc)) {
					Locker clocker(creatureTarget, creature);
					creatureTarget->setArmorBreakState(20, 80);
			}
		}
		return SUCCESS;*/
	}

};

#endif //MELEE2HHIT3COMMAND_H_
