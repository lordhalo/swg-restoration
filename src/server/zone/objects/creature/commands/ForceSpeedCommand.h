/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCESPEEDCOMMAND_H_
#define FORCESPEEDCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/Buff.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceSpeedCommand : public QueueCommand {
public:

	ForceSpeedCommand(const String& name, ZoneProcessServer* server)
	: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;


		uint32 buffcrc1 = BuffCRC::JEDI_FORCE_SPEED_1;
		uint32 buffcrc2 = BuffCRC::JEDI_FORCE_SPEED_2;

		if(creature->hasBuff(buffcrc1) || creature->hasBuff(buffcrc2)) {
			creature->sendSystemMessage("@jedi_spam:force_buff_present");
			return GENERALERROR;
		}


		// Force cost of skill.
		int forceCost = 150;
		int action = 1000;
		float actionCost = (2 / 100.f);

		int aCost = (action * actionCost);

		int mind = 1000;
		float mindCost = (2 / 100.f);

		int mCost = (mind * mindCost);

		//Check for and deduct Force cost.

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();


		if (playerObject->getForcePower() <= forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->sendSystemMessage("Too Tired"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		playerObject->setForcePower(playerObject->getForcePower() - forceCost);
		creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);

		StringIdChatParameter startStringId("jedi_spam", "apply_forcespeed1");
		StringIdChatParameter endStringId("jedi_spam", "remove_forcespeed1");

		int duration = 360;

		ManagedReference<Buff*> buff = new Buff(creature, buffcrc1, duration, BuffType::JEDI);

		Locker locker(buff);

		buff->setStartMessage(startStringId);
		buff->setEndMessage(endStringId);
		buff->setSkillModifier("combat_haste", 15);

		creature->addBuff(buff);
		creature->playEffect("clienteffect/pl_force_speed_self.cef", "");

		VisibilityManager::instance()->increaseVisibility(creature, 1);

		return SUCCESS;
	}

};

#endif //FORCESPEED1COMMAND_H_
