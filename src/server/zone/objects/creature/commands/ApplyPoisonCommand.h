/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef APPLYPOISONCOMMAND_H_
#define APPLYPOISONCOMMAND_H_

class ApplyPoisonCommand : public QueueCommand {
public:

	ApplyPoisonCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
	}

};

#endif //APPLYPOISONCOMMAND_H_
