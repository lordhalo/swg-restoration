/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef LUNGE3COMMAND_H_
#define LUNGE3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"


class Lunge3Command : public CombatQueueCommand {
public:

	Lunge3Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //LungeCommand_H_
