/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef STANDCOMMAND_H_
#define STANDCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/packets/chat/ChatSystemMessage.h"
#include "server/zone/objects/player/events/StandTask.h"

class StandCommand : public QueueCommand {
public:

	StandCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if ((creature->isKnockedDown() && !creature->isAiAgent()) && creature->getLevel() >= 15 && creature->isInCombat()){
			creature->sendSystemMessage("You cannot stand while Knocked Down");
			return GENERALERROR;
		}


		if(creature->isInCombat() && creature->isAiAgent()) {
			creature->setPosture(CreaturePosture::UPRIGHT, false, true);
			creature->doCombatAnimation(STRING_HASHCODE("change_posture"));
		} else {
			creature->setPosture(CreaturePosture::UPRIGHT);
		}

		return SUCCESS;
	}

};

#endif //STANDCOMMAND_H_

