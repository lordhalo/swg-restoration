/*
 				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef PARALYZECOMMAND_H_
#define PARALYZECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/packets/object/CombatAction.h"
#include "CombatQueueCommand.h"

class ParalyzeCommand: public CombatQueueCommand {
	String buffname;
	uint32 buffcrc;
public:

	ParalyzeCommand(const String& name, ZoneProcessServer* server) :
		CombatQueueCommand(name, server) {
		buffname = "paralyze";
		buffcrc = buffname.hashCode();

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target,
			const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		try {

			int action = 1000;
			float actionCost = (20 / 100.f);

			int aCost = (action * actionCost);

			int mind = 1000;
			float mindCost = (15 / 100.f);

			int mCost = (mind * mindCost);

			if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
				creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
				return false;
			}

			ManagedReference<CreatureObject*> targetCreature =
					server->getZoneServer()->getObject(target).castTo<CreatureObject*>();

			if (targetCreature == NULL)
				return GENERALERROR;

			if (!targetCreature->isAttackableBy(creature)) {
				return GENERALERROR;
			}

			/// Check Range
			if(!checkDistance(creature, targetCreature, 20))
			{
				creature->sendSystemMessage("Out of range");
				return GENERALERROR;
			}

			if(targetCreature->hasBuff(buffcrc))
				return INVALIDTARGET;

			int targetDefense = 0;
			if(targetCreature->getWeapon()->isMeleeWeapon() || targetCreature->getWeapon()->isJediWeapon()){
				targetDefense = targetCreature->getSkillMod("melee_defense");
			}else if (targetCreature->getWeapon()->isRangedWeapon()){
				targetDefense = targetCreature->getSkillMod("ranged_defense");
			}


			//int targetDefense = CombatManager::instance()->getDefenderDefenseModifier(targetCreature, targetCreature->getWeapon(), creature);
			float hitChance = 50 + System::random(creature->getSkillMod("debuffing_efficiency"));

			int roll = System::random(targetDefense);
			bool hit = roll < hitChance;

			int distance = creature->getDistanceTo(targetCreature);
			String crc;

			if (distance < 10.0f) {
				crc = "throw_grenade_near_poison";
			}
			else if (10.0f <= distance && distance < 20.0f) {
				crc = "throw_grenade_medium_poison";
			}
			else {
				crc = "throw_grenade_far_poison";
			}

			CombatAction* cAction = new CombatAction(creature, targetCreature, crc.hashCode(), hit, 0L);
			creature->broadcastMessage(cAction, true);


			ManagedReference<Buff*> buff = NULL;
			int damage = 0;

			if (hit) {
				Locker clocker(targetCreature, creature);

				if(!targetCreature->isFrozen() && targetCreature->checkRootRecovery()){
					targetCreature->setRootedState(7);
					targetCreature->updateRootRecovery();
					targetCreature->updateRootDelay(50000);
					targetCreature->playEffect("clienteffect/pl_state_cantattack_light.cef", "");
				}

			}

			creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
			creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);

			return SUCCESS;

		} catch (Exception& e) {

		}

		return GENERALERROR;
	}

};

#endif //ThyroidRuptureCommand_H_
