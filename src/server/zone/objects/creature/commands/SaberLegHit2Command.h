/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERLEGHIT2COMMAND_H_
#define SABERLEGHIT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class SaberLegHit2Command : public CombatQueueCommand {
public:

	SaberLegHit2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		return doCombatAction(creature, target);
	}

};

#endif //SaberHitCommand_H_
