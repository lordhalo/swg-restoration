/*
Copyright (C) 2007 <SWGEmu>

This File is part of Core3.

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General
Public License along with this program; if not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Linking Engine3 statically or dynamically with other modules
is making a combined work based on Engine3.
Thus, the terms and conditions of the GNU Lesser General Public License
cover the whole combination.

In addition, as a special exception, the copyright holders of Engine3
give you permission to combine Engine3 program with free software
programs or libraries that are released under the GNU LGPL and with
code included in the standard release of Core3 under the GNU LGPL
license (or modified versions of such code, with unchanged license).
You may copy and distribute such a system following the terms of the
GNU LGPL for Engine3 and the licenses of the other code concerned,
provided that you include the source code of that other code when
and as the GNU LGPL requires distribution of source code.

Note that people who make modified versions of Engine3 are not obligated
to grant this special exception for their modified versions;
it is their choice whether to do so. The GNU Lesser General Public License
gives permission to release a modified version without this exception;
this exception also makes it possible to release a modified version
which carries forward this exception.
*/

#ifndef FORCEINSIGHTCOMMAND_H_
#define FORCEINSIGHTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"
#include "server/zone/managers/visibility/VisibilityManager.h"

class ForceInsightCommand : public QueueCommand {
public:

	ForceInsightCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		PlayerObject* ghost = creature->getPlayerObject();

		if (creature->hasBuff(String("Insight").hashCode())) {
			creature->sendSystemMessage("You are already focusing");
			return GENERALERROR;
		}

		int duration = 300;


		if (duration == 0)
			return GENERALERROR;

		Buff* focus = new Buff(creature, String("Insight").hashCode(), duration, BuffType::SKILL);

		Locker locker(focus);

		focus->setSkillModifier("private_critical_chance", 10);
		focus->setSkillModifier("private_critical_value", 5);

		StringIdChatParameter startMsg("Force Insight Active");
		StringIdChatParameter endMsg("Force Insight Inactive");
		focus->setStartMessage(startMsg);
		focus->setEndMessage(endMsg);

		focus->setStartFlyText("combat_effects", "start_insight", 0, 255, 0);
		focus->setEndFlyText("combat_effects", "end_insight", 255, 0, 0);

		creature->addBuff(focus);
		creature->playEffect("clienteffect/forcefocus.cef", "");

		VisibilityManager::instance()->increaseVisibility(creature, 1);

		return SUCCESS;
	}

};

#endif //FORCEFOCUSCOMMAND_H_
