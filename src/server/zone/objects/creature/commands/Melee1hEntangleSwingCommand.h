/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef MELEE1HENTANGLESWINGCOMMAND_H_
#define MELEE1HENTANGLESWINGCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "CombatQueueCommand.h"


class Melee1hEntangleSwingCommand : public CombatQueueCommand {
public:

	Melee1hEntangleSwingCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 5, name, "melee_1h_entangle_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "melee_1h_entangle_start", 0, 0, 255, true);
		}
		return SUCCESS;
	}

};

#endif //MELEE1HHIT3COMMAND_H_
