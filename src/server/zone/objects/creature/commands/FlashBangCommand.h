/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FLASHBANGCOMMAND_H_
#define FLASHBANGCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/packets/object/CombatAction.h"
#include "CombatQueueCommand.h"

class FlashBangCommand : public CombatQueueCommand {
public:

	FlashBangCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			if(object->isCreature() || object->isPlayerCreature()){
				ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

				doAnimations(creature, creatureTarget, creature->getDistanceTo(creatureTarget));
			}

		}
		return SUCCESS;
	}


	void doAnimations(CreatureObject* creature, CreatureObject* creatureTarget, float range) const {
		String crc;

		if (range < 10.0f) {
			crc = "throw_grenade_near_healing";
		}
		else if (10.0f <= range && range < 20.0f) {
			crc = "throw_grenade_medium_healing";
		}
		else {
			crc = "throw_grenade_far_healing";
		}


		CombatAction* action = new CombatAction(creature, creatureTarget,  crc.hashCode(), 1, 0L);
		creature->broadcastMessage(action, true);
	}

};

#endif //FLASHBANGCOMMAND_H_
