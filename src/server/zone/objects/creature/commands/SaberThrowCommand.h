/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERTHROWCOMMAND_H_
#define SABERTHROWCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class SaberThrowCommand : public CombatQueueCommand {
public:

	SaberThrowCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		return doCombatAction(creature, target);
	}

};

#endif //SABERTHROW1COMMAND_H_
