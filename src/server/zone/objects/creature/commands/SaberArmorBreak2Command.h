/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SABERARMORBREAK2COMMAND_H_
#define SABERARMORBREAK2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class SaberArmorBreak2Command : public CombatQueueCommand {
public:

	SaberArmorBreak2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //MELEE2HHIT3COMMAND_H_
