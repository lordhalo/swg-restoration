/*
Copyright (C) 2007 <SWGEmu>

This File is part of Core3.

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General
Public License along with this program; if not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Linking Engine3 statically or dynamically with other modules
is making a combined work based on Engine3.
Thus, the terms and conditions of the GNU Lesser General Public License
cover the whole combination.

In addition, as a special exception, the copyright holders of Engine3
give you permission to combine Engine3 program with free software
programs or libraries that are released under the GNU LGPL and with
code included in the standard release of Core3 under the GNU LGPL
license (or modified versions of such code, with unchanged license).
You may copy and distribute such a system following the terms of the
GNU LGPL for Engine3 and the licenses of the other code concerned,
provided that you include the source code of that other code when
and as the GNU LGPL requires distribution of source code.

Note that people who make modified versions of Engine3 are not obligated
to grant this special exception for their modified versions;
it is their choice whether to do so. The GNU Lesser General Public License
gives permission to release a modified version without this exception;
this exception also makes it possible to release a modified version
which carries forward this exception.
*/

#ifndef DUELISTSTANCE2COMMAND_H_
#define DUELISTSTANCE2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"
#include "templates/params/creature/CreatureAttribute.h"

class DuelistStance2Command : public QueueCommand {
public:
	String cdName;
	float actionCost;
	String buffname;
	uint32 buffcrc;

	DuelistStance2Command(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		cdName = "duelistStance";
		actionCost = 300;
		buffname = "ranged.duelist_stance";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		PlayerObject* ghost = creature->getPlayerObject();

		if (creature->hasBuff(buffcrc)) {
			creature->sendSystemMessage("Already using DuelistStance");
			return GENERALERROR;
		}

		WeaponObject* weapon = creature->getWeapon();

		if (creature->getLocomotion() != 0)
			return GENERALERROR;

		if (weapon->isMeleeWeapon())
			return GENERALERROR;

		int duration = 30;
		int rankDefense = creature->getSkillMod("bhrank_defense");
		int rankDefenseFaction = creature->getSkillMod("gcw_defense");
		int efficacy = 250 + rankDefense + rankDefenseFaction;


		if (duration == 0 || efficacy == 0)
			return GENERALERROR;

		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);

		Buff* duelist = new Buff(creature, buffcrc, duration, BuffType::SKILL);

		Locker locker(duelist);

		duelist->setSkillModifier("private_defense_bonus", efficacy);
		duelist->setSkillModifier("private_damage_shield", 25);


		creature->addBuff(duelist);
		creature->playEffect("clienteffect/bh_duelist_stance.cef", "");


		return SUCCESS;

	}

	float getCommandDuration(CreatureObject* object, const UnicodeString& arguments) const {
		return 2.0;
	}

};

#endif //CENTEROFBEINGCOMMAND_H_
