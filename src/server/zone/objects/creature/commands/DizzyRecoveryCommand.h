/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef DIZZYRECOVERYCOMMAND_H_
#define DIZZYRECOVERYCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/packets/chat/ChatSystemMessage.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/objects/player/events/StandTask.h"

class DizzyRecoveryCommand : public QueueCommand {
public:
	float actionCost;
	DizzyRecoveryCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 300;
	}

	float getCommandDuration(CreatureObject* object, const UnicodeString& arguments) const {
		return 1.0;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isDizzied()) {
			return INVALIDSTATE;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		if(!creature->isDead() || !creature->isIncapacitated() || !creature->isKnockedDown()){
			creature->clearState(CreatureState::DIZZY, true);
			creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);
		}

		return SUCCESS;
	}

};

#endif //KnockDownRecoveryCommand_H_

