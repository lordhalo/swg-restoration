/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef VAAPADSTANCECOMMAND_H_
#define VAAPADSTANCECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"


class VaapadStanceCommand : public QueueCommand {
public:

	VaapadStanceCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		PlayerObject* ghost = creature->getPlayerObject();

		if (creature->hasBuff(STRING_HASHCODE("centerofbeing"))) {
			creature->sendSystemMessage("@combat_effects:already_centered");
			return GENERALERROR;
		}

		if (creature->hasBuff(STRING_HASHCODE("vaapadstance"))) {
			creature->sendSystemMessage("You are already in a aggresssive stance");
			return GENERALERROR;
		}

		WeaponObject* weapon = creature->getWeapon();

		int duration = 0;
		int efficacy = 0;

		if (weapon->isUnarmedWeapon()) {
			duration = creature->getSkillMod("center_of_being_duration_unarmed");
			efficacy = creature->getSkillMod("unarmed_center_of_being_efficacy");
		} else if (weapon->isOneHandMeleeWeapon()) {
			duration = creature->getSkillMod("center_of_being_duration_onehandmelee");
			efficacy = creature->getSkillMod("onehandmelee_center_of_being_efficacy");
		} else if (weapon->isTwoHandMeleeWeapon()) {
			duration = creature->getSkillMod("center_of_being_duration_twohandmelee");
			efficacy = creature->getSkillMod("twohandmelee_center_of_being_efficacy");
		} else if (weapon->isPolearmWeaponObject()) {
			duration = creature->getSkillMod("center_of_being_duration_polearm");
			efficacy = creature->getSkillMod("polearm_center_of_being_efficacy");
		} else
			return GENERALERROR;

		if (duration == 0 || efficacy == 0)
			return GENERALERROR;

		Buff* vaapad = new Buff(creature, STRING_HASHCODE("vaapadstance"), duration, BuffType::SKILL);

		Locker locker(vaapad);

		vaapad->setSkillModifier("private_melee_damage_bonus", efficacy);

		StringIdChatParameter startMsg("combat_effects", "vaapad_start");
		StringIdChatParameter endMsg("combat_effects", "vaapad_stop");
		vaapad->setStartMessage(startMsg);
		vaapad->setEndMessage(endMsg);

		vaapad->setStartFlyText("combat_effects", "vaapad_start_fly", 0, 255, 0);
		vaapad->setEndFlyText("combat_effects", "vaapad_stop_fly", 255, 0, 0);

		creature->addBuff(vaapad);

		return SUCCESS;
	}

};

#endif //VAAPADSTANCECOMMAND_H_
