/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef POWERATTACKCOMMAND_H_
#define POWERATTACKCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "CombatQueueCommand.h"

class PowerAttackCommand : public CombatQueueCommand {
public:

	PowerAttackCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		return doCombatAction(creature, target);
	}

};

#endif //PowerAttackCommand
