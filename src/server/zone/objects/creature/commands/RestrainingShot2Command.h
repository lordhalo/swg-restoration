/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef RESTRAININGSHOT2COMMAND_H_
#define RESTRAININGSHOT2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class RestrainingShot2Command : public CombatQueueCommand {
public:

	RestrainingShot2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //RapidFireCommand_H_
