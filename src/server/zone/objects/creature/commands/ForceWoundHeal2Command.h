/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEWOUNDHEAL2COMMAND_H_
#define FORCEWOUNDHEAL2COMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceWoundHeal2Command : public ForceHealQueueCommand {
public:

	ForceWoundHeal2Command(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
