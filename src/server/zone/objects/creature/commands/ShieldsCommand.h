/*
Copyright (C) 2007 <SWGEmu>

This File is part of Core3.

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General
Public License along with this program; if not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Linking Engine3 statically or dynamically with other modules
is making a combined work based on Engine3.
Thus, the terms and conditions of the GNU Lesser General Public License
cover the whole combination.

In addition, as a special exception, the copyright holders of Engine3
give you permission to combine Engine3 program with free software
programs or libraries that are released under the GNU LGPL and with
code included in the standard release of Core3 under the GNU LGPL
license (or modified versions of such code, with unchanged license).
You may copy and distribute such a system following the terms of the
GNU LGPL for Engine3 and the licenses of the other code concerned,
provided that you include the source code of that other code when
and as the GNU LGPL requires distribution of source code.

Note that people who make modified versions of Engine3 are not obligated
to grant this special exception for their modified versions;
it is their choice whether to do so. The GNU Lesser General Public License
gives permission to release a modified version without this exception;
this exception also makes it possible to release a modified version
which carries forward this exception.
*/

#ifndef SHIELDSCOMMAND_H_
#define SHIELDSCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/buffs/SingleUseBuff.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/objects/player/events/CoolDownTask.h"

class ShieldsCommand : public QueueCommand {
public:

	ShieldsCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		uint32 buffcrc1 = BuffCRC::JEDI_FORCE_ARMOR_1;

		// Force cost of skill.
		int mindCost = 200;


		//Check for and deduct Force cost.

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();


		if (creature->getHAM(CreatureAttribute::MIND) <= mindCost) {
			creature->sendSystemMessage("Need More Mind"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		//info("Hashcode", String("shields").hashCode());

		creature->inflictDamage(creature, CreatureAttribute::MIND, mindCost, true);

		StringIdChatParameter startStringId("jedi_spam", "apply_forcearmor1");
		StringIdChatParameter endStringId("jedi_spam", "remove_forcearmor1");

		int duration = 120;
		int skillMod = 30;
		int bonus = 0;

		int bhDef = creature->getSkillMod("bhrank_defense");
		int gcwDef = creature->getSkillMod("gcw_defense");

		if(bhDef > 0){
			bhDef /= 10.f;
			bonus = bhDef;
		}else if(gcwDef > 0){
			gcwDef /= 10.f;
			bonus = gcwDef;
		}

		skillMod += bonus;

		Vector<unsigned int> eventTypes;
		eventTypes.add(ObserverEventType::SHIELDSHIT);

		ManagedReference<SingleUseBuff*> buff = new SingleUseBuff(creature, buffcrc1, duration, BuffType::OTHER, getNameCRC());

		Locker locker(buff);
		buff->setStartMessage(startStringId);
		buff->setEndMessage(endStringId);
		buff->setSkillModifier("shield_armor", skillMod);
		buff->init(&eventTypes);

		creature->addBuff(buff);
		creature->playEffect("clienteffect/bh_shields_effect.cef", "");

		Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 300, name, "shields_stop");
		creature->addPendingTask(name, cdGroup, 1);
		creature->showFlyText("combat_effects", "shields_start", 0, 0, 255, true);

		return SUCCESS;
	}

	void handleBuff(SceneObject* creature, ManagedObject* object, int64 param) {

		ManagedReference<CreatureObject*> creo = cast<CreatureObject*>( creature);
		if (creo == NULL)
			return;

		// Client Effect upon hit (needed)
		creo->playEffect("clienteffect/bh_shields_hit.cef", "");
		/*
		ManagedReference<PlayerObject*> playerObject = creo->getPlayerObject();
		if (playerObject == NULL)
			return;

		// TODO: Force Rank modifiers.
		int rankMod = (creo->getSkillMod("bh_control") / 100.f);
		int amount = param * rankMod;
		if (amount < 1) amount = 1;
		int actionCost = (param - amount) * 0.3;
		if (creo->getHAM(CreatureAttribute::ACTION) <= 150) { // Remove buff if not enough force.
			Buff* buff = creo->getBuff(BuffCRC::JEDI_FORCE_ARMOR_1);
			if (buff != NULL)
				Locker locker(buff);

				creo->removeBuff(buff);
		} else
			creo->inflictDamage(creo, CreatureAttribute::ACTION, actionCost, true); */
	}

};

#endif //FORCEARMOR1COMMAND_H_
