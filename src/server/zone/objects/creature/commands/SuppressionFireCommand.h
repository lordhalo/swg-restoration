/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SUPPRESSIONFIRECOMMAND_H_
#define SUPPRESSIONFIRECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class SuppressionFireCommand : public CombatQueueCommand {
public:

	SuppressionFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //SUPPRESSIONFIRE1COMMAND_H_
