/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef COREBOMBCOMMAND_H_
#define COREBOMBCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class CoreBombCommand : public CombatQueueCommand {
public:

	CoreBombCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //COREBOMBCOMMAND_H_

