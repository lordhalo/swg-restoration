/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORTIFYBODYCOMMAND_H_
#define FORTIFYBODYCOMMAND_H_

#include "JediQueueCommand.h"

class FortifyBodyCommand : public JediQueueCommand {
public:

	FortifyBodyCommand(const String& name, ZoneProcessServer* server)
: JediQueueCommand(name, server) {
		buffCRC = BuffCRC::TEST_FIRST;
		skillMods.put("absorption_bleeding", 200);
		skillMods.put("absorption_fire", 200);
		skillMods.put("absorption_poison", 200);
		skillMods.put("absorption_disease", 200);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		return doJediSelfBuffCommand(creature);
	}

};

#endif //FORTIFYBODYCOMMAND_H_
