/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef BINDINGSTRIKECOMMAND_H_
#define BINDINGSTRIKECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"


class BindingStrikeCommand : public CombatQueueCommand {
public:

	BindingStrikeCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL && !creatureTarget->hasBuff(getNameCRC())) {
					Locker clocker(creatureTarget, creature);

					ManagedReference<Buff*> buff = new Buff(creatureTarget, getNameCRC(), 30, BuffType::SKILL);

					Locker locker(buff);

					buff->setSkillModifier("binding_strike", 100);
					buff->setStartFlyText("cmd_n", name, 255, 0, 0);
					buff->setEndFlyText("cmd_n", name, 0, 0, 255);

					creatureTarget->addBuff(buff);

				}
		}
		return SUCCESS;
	}

};

#endif //BleedAttackCommand_H_
