/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef STOPPINGSHOT3COMMAND_H_
#define STOPPINGSHOT3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "CombatQueueCommand.h"

class StoppingShot3Command : public CombatQueueCommand {
public:
	String cdName;
	StoppingShot3Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
		cdName = "stoppingShot";
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->containsPendingTask(cdName)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
				if(object->isCreature()){
					ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

					Locker clocker(creatureTarget, creature);

					if (creatureTarget != NULL && !creatureTarget->hasBuff(getNameCRC()) && creatureTarget->checkRootRecovery()) {
						ManagedReference<Buff*> buff = new Buff(creatureTarget, getNameCRC(), 20, BuffType::SKILL);
						Locker locker(buff);
						buff->setStartFlyText("combat_effects", "go_rooted", 255, 0, 0);
						buff->setEndFlyText("combat_effects", "no_rooted", 0, 0, 255);
						creatureTarget->setRootedState(10);
						creatureTarget->updateRootRecovery();
						creatureTarget->updateRootDelay(40000);
						//buff->setAccelerationMultiplierMod(creatureTarget->getAccelerationMultiplierMod() * .01);

						creatureTarget->addBuff(buff);
						creatureTarget->playEffect("clienteffect/state_rooted_heavy.cef", "");
					}
				}
		}
		return SUCCESS;
	}

};

#endif //STOPPINGSHOTCOMMAND_H_
