/*
 				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef LOWBLOWCOMMAND_H_
#define LOWBLOWCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/packets/object/CombatAction.h"
#include "CombatQueueCommand.h"

class LowBlowCommand: public CombatQueueCommand {
	String buffname;
	uint32 buffcrc;
public:

	LowBlowCommand(const String& name, ZoneProcessServer* server) :
		CombatQueueCommand(name, server) {
		buffname = "lowblow";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target,
			const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		Reference<SceneObject*> object = server->getZoneServer()->getObject(target);

		if (!object->isCreatureObject()) {
			return GENERALERROR;
		}

		ManagedReference<CreatureObject*> targetCreature =
				server->getZoneServer()->getObject(target).castTo<CreatureObject*>();

		if (targetCreature->hasBuff(buffcrc))
			return INVALIDSTATE;

		try {

			int action = 1000;
			float actionCost = (24 / 100.f);

			int aCost = (action * actionCost);

			int mind = 1000;
			float mindCost = (4 / 100.f);

			int mCost = (mind * mindCost);

			int costGroup = creature->getLastCostGroup();
			if (costGroup == 83){
				aCost *= 2;
				mCost *= 2;
			}

			if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
				creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
				return false;
			}


			if (!targetCreature->isAttackableBy(creature)) {
				return GENERALERROR;
			}

			/// Check Range
			if(!checkDistance(creature, targetCreature, creature->getWeapon()->getMaxRange()))
			{
				creature->sendSystemMessage("Out of range");
				return GENERALERROR;
			}

			int targetDefense = 0;
			if(targetCreature->getWeapon()->isMeleeWeapon() || targetCreature->getWeapon()->isJediWeapon()){
				targetDefense = targetCreature->getSkillMod("melee_defense");
			}else if (targetCreature->getWeapon()->isRangedWeapon()){
				targetDefense = targetCreature->getSkillMod("ranged_defense");
			}


			//int targetDefense = CombatManager::instance()->getDefenderDefenseModifier(targetCreature, targetCreature->getWeapon(), creature);
			int hitChance = CombatManager::instance()->calculateAccuracy(creature, creature->getWeapon(), false);

			int roll = System::random(targetDefense);
			bool hit = roll < hitChance;

			int distance = creature->getDistanceTo(targetCreature);
			String crc = "fire_5_single_light";

			CombatAction* cAction = new CombatAction(creature, targetCreature, crc.hashCode(), hit, 0L);
			creature->broadcastMessage(cAction, true);


			ManagedReference<Buff*> buff = NULL;
			int damage = 0;

			if (hit) {
				Locker clocker(targetCreature, creature);
				int effectiveness = 40;


				ManagedReference<Buff*> buff = new Buff(targetCreature, buffcrc, 20, BuffType::SKILL);
				Locker locker(buff);
				buff->setStartFlyText("cmd_n", name, 255, 0, 0);
				buff->setEndFlyText("cmd_n", name, 0, 0, 255);
				buff->setSkillModifier("combat_slow", effectiveness);

				targetCreature->addBuff(buff);
				targetCreature->playEffect("clienteffect/pl_thyroidrupture_light.cef", "");

			}

			creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
			creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);


			return SUCCESS;

		} catch (Exception& e) {

		}

		return GENERALERROR;
	}

};

#endif //ThyroidRuptureCommand_H_
