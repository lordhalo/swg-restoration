/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef REGAINCONSCIOUSNESSCOMMAND_H_
#define REGAINCONSCIOUSNESSCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
//#include "server/zone/objects/player/events/RegainConsciousnessRegenTask.h"
#include "server/zone/objects/creature/events/AwardLifeExperienceEvent.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"

class RegainConsciousnessCommand : public QueueCommand {
public:

	RegainConsciousnessCommand(const String& name, ZoneProcessServer* server)
	: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;


		//Check for and deduct Force cost.

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

		/**
		 * Add Buff that makes immunity to VIS for X amount of Time
		 *
		 */

		bool canRes = false;

		if(playerObject->getExperience("death_exp") > 0){
			canRes = true;
		}

		// They should be dead...
		if (creature->isDead() && canRes == true){
			// Revive user by setting posture to standing.
			creature->setPosture(CreaturePosture::UPRIGHT);

			 creature->playEffect("clienteffect/pl_force_regain_consciousness_self.cef", "");


			// Jedi experience loss.
			if(creature->getJediDeathType() == 1){
					creature->setJediDeathType(0);
					playerObject->addExperience("death_exp", -1, true);
					playerObject->setForcePower(playerObject->getForcePowerMax());

					Reference<AwardLifeExperienceEvent*> inProgressCheck = creature->getPendingTask("awardLifeExperienceEvent").castTo<AwardLifeExperienceEvent*>();
					if (inProgressCheck == NULL){
						Reference<AwardLifeExperienceEvent*> lifeResto = new AwardLifeExperienceEvent(creature);
						creature->addPendingTask("awardLifeExperienceEvent", lifeResto, 172800000);
						creature->setShouldLifeReward(1); //1 = Should 0 = should not
					}

			}else{
				creature->sendSystemMessage("No Life Cost");
			}

			creature->sendSystemMessage("Using the Force, You channel the strength required to awake from a near permanent demise.");
			creature->setBlueGlowyState(30);

			//Reference<RegainConsciousnessRegenTask*> rcTask = new RegainConsciousnessRegenTask(creature, playerObject);
			//creature->addPendingTask("regainConsciousnessRegenTask", rcTask, (1800 * 1000));

			VisibilityManager::instance()->clearVisibility(creature);

			return SUCCESS;
		}

		if(creature->isDead() && canRes == false){
			creature->sendSystemMessage("The Force can no longer heal your broken body.");
		}

		return GENERALERROR;

	}

};

#endif //REGAINCONSCIOUSNESSCOMMAND_H_
