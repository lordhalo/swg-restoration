#ifndef FORCESUPPRESSION2COMMAND_H_
#define FORCESUPPRESSION2COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/objects/player/events/CoolDownTask.h"
#include "ForcePowersQueueCommand.h"

class ForceSuppression2Command : public ForcePowersQueueCommand {
	String buffname;
	uint32 buffcrc;
	float mindCost;
	float actionCost;
public:

	ForceSuppression2Command(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {
		buffname = "root";
		buffcrc = buffname.hashCode();
		actionCost = (9 / 100.f);
		mindCost = (4 / 100.f);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
					return NOJEDIARMOR;
			}
		}

		int action = 1000;
		int aCost = (action * actionCost);

		int mind = 1000;
		int mCost = (mind * mindCost);

		if (creature->getHAM(CreatureAttribute::ACTION) < aCost || creature->getHAM(CreatureAttribute::MIND) < mCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return false;
		}

		int res = doCombatAction(creature, target);

		if (res == SUCCESS) {

			// Setup debuff.

			Reference<SceneObject*> object = server->getZoneServer()->getObject(target);
			ManagedReference<CreatureObject*> creatureTarget = cast<CreatureObject*>( object.get());

			if (creatureTarget != NULL) {
				if (!creatureTarget->hasBuff(buffcrc)){
					Locker clocker(creatureTarget, creature);

					ManagedReference<Buff*> buff = new Buff(creatureTarget, buffcrc, 20, BuffType::JEDI);

					Locker locker(buff);

					creatureTarget->setRootedState(20);
					creatureTarget->playEffect("clienteffect/state_rooted_heavy.cef");

					creature->inflictDamage(creature, CreatureAttribute::ACTION, aCost, false);
					creature->inflictDamage(creature, CreatureAttribute::MIND, mCost, false);
				} else{
					creature->sendSystemMessage("Target is already Rooted");
				}
			}

		}

		return SUCCESS;
	}

};

#endif //FORCESAPCOMMAND_H_
