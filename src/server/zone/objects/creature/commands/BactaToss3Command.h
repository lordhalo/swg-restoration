#ifndef BACTATOSS3COMMAND_H_
#define BACTATOSS3COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/tangible/pharmaceutical/StimPack.h"
#include "server/zone/objects/tangible/pharmaceutical/RangedStimPack.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/creature/events/InjuryTreatmentTask.h"
#include "server/zone/objects/creature/buffs/Buff.h"
#include "server/zone/objects/creature/buffs/DelayedBuff.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/managers/collision/CollisionManager.h"

class BactaToss3Command : public QueueCommand {
public:
	BactaToss3Command(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
	}

	void doAnimationsRange(CreatureObject* creature, CreatureObject* creatureTarget, float range) const {
		String crc;

		if (range < 10.0f) {
			crc = "throw_grenade_near_healing";
		}
		else if (10.0f <= range && range < 20.0f) {
			crc = "throw_grenade_medium_healing";
		}
		else {
			crc = "throw_grenade_far_healing";
		}

		CombatAction* action = new CombatAction(creature, creatureTarget,  crc.hashCode(), 1, 0L);
		creature->broadcastMessage(action, true);
	}

	int calculateActionCost(CreatureObject* creature) const {
		int action = 1000;
		float actionCost = (7 / 100.f);

		int aCost = (action * actionCost);
		//int battleFat = creature->getShockWounds();

		//if(battleFat >= 100)
		//	baseCost += (battleFat / 2);

		return aCost;

	}

	int calculateMindCost(CreatureObject* creature) const {
		int mind = 1000;
		float mindCost = (10 / 100.f);

		int mCost = (mind * mindCost);
		//int battleFat = creature->getShockWounds();

		//if(battleFat >= 100)
		//	baseCost += (battleFat / 2);

		return mCost;

	}

	StimPack* findStimPack(CreatureObject* creature) const {
		SceneObject* inventory = creature->getSlottedObject("inventory");

		if (inventory == NULL)
			return NULL;

		for (int i = 0; i < inventory->getContainerObjectsSize(); ++i) {
			SceneObject* item = inventory->getContainerObject(i);

			if (!item->isTangibleObject())
				continue;

			TangibleObject* tano = cast<TangibleObject*>( item);

			if (tano->isPharmaceuticalObject()) {
				PharmaceuticalObject* pharma = cast<PharmaceuticalObject*>( tano);
				if (pharma->isRangedStimPack()) {
					RangedStimPack* stimPack = cast<RangedStimPack*>(pharma);
						return stimPack;
				}
			}
		}

		return NULL;
	}

	bool checkTarget(CreatureObject* creature, CreatureObject* creatureTarget) const {
		if (!creatureTarget->hasDamage(CreatureAttribute::HEALTH)) {
			return false;
		}

		PlayerManager* playerManager = server->getPlayerManager();

		if (creature != creatureTarget && !CollisionManager::checkLineOfSight(creature, creatureTarget)) {
			return false;
		}

		if (creatureTarget->isDead())
			return false;

		return true;
	}

	bool canPerformSkill(CreatureObject* creature, CreatureObject* creatureTarget, StimPack* stimPack) const {

		if (creature->isProne() || creature->isMeditating()) {
			creature->sendSystemMessage("@error_message:wrong_state"); //You cannot complete that action while in your current state.
			return false;
		}

		if (creature->isRidingMount()) {
			creature->sendSystemMessage("@error_message:survey_on_mount"); //You cannot perform that action while mounted on a creature or driving a vehicle.
			return false;
		}

		if (creature->getHAM(CreatureAttribute::ACTION) < (calculateActionCost(creature)) || creature->getHAM(CreatureAttribute::MIND) < (calculateMindCost(creature))) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return false;
		}

		if (!creatureTarget->isHealableBy(creature)) {
			creature->sendSystemMessage("@healing:pvp_no_help"); //It would be unwise to help such a patient.
			return false;
		}

		if (!creatureTarget->hasDamage(CreatureAttribute::HEALTH)) {
			if (creatureTarget == creature) {
				creature->sendSystemMessage("@healing_response:healing_response_61"); //You have no damage to heal.
			} else if (creatureTarget->isPlayerCreature()) {
				StringIdChatParameter stringId("healing_response", "healing_response_63"); //%NT has no damage to heal.
				stringId.setTT(creatureTarget->getObjectID());
				creature->sendSystemMessage(stringId);
			} else {
				StringBuffer message;
				message << creatureTarget->getDisplayedName() << " has no damage to heal.";
				creature->sendSystemMessage(message.toString());
			}

			return false;
		}

		return true;
	}

	void sendHealMessage(CreatureObject* creature, CreatureObject* creatureTarget, uint32 healthDamage) const {
		if (!creature->isPlayerCreature())
			return;

		CreatureObject* player = cast<CreatureObject*>(creature);

		StringBuffer msgPlayer, msgTarget, msgBody, msgTail;

		if (healthDamage > 0) {
			msgBody << healthDamage << " health";
		} else {
			return; //No damage to heal.
		}

		msgTail << " damage.";

		if (creature == creatureTarget) {
			msgPlayer << "You heal yourself for " << msgBody.toString() << msgTail.toString();
			creature->sendSystemMessage(msgPlayer.toString());
		} else if (creatureTarget->isPlayerCreature()) {
			msgPlayer << "You heal " << creatureTarget->getFirstName() << " for " << msgBody.toString() << msgTail.toString();
			player->sendSystemMessage(msgPlayer.toString());

			msgTarget << player->getFirstName() << " heals you for " << msgBody.toString() << msgTail.toString();
			creatureTarget->sendSystemMessage(msgTarget.toString());
		} else {
			msgPlayer << "You heal " << creatureTarget->getDisplayedName() << " for " << msgBody.toString() << msgTail.toString();
			player->sendSystemMessage(msgPlayer.toString());
		}
	}


	void awardXp(CreatureObject* creature, const String& type, int power) const {
		if (!creature->isPlayerCreature())
			return;

		CreatureObject* player = cast<CreatureObject*>(creature);

		int amount = (int)round((float)power * 0.25f);

		if (amount <= 0)
			return;

		PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();
		playerManager->awardExperience(player, type, amount * 4, true);
	}


	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);

		if (object != NULL) {
			if (!object->isCreatureObject()) {
				TangibleObject* tangibleObject = dynamic_cast<TangibleObject*>(object.get());

				if (tangibleObject != NULL && tangibleObject->isAttackableBy(creature)) {
					object = creature;
				} else {
					creature->sendSystemMessage("@healing_response:healing_response_62"); //Target must be a player or a creature pet in order to heal damage.
					return GENERALERROR;
				}
			}
		} else
			object = creature;

		CreatureObject* targetCreature = cast<CreatureObject*>( object.get());

		Locker clocker(targetCreature, creature);

		if ((targetCreature->isAiAgent() && !targetCreature->isPet()) || targetCreature->isDroidObject() || targetCreature->isDead() || targetCreature->isRidingMount() || targetCreature->isAttackableBy(creature))
			targetCreature = creature;

		uint64 pharmaceuticalObjectID = 0;

		try {
			if (!arguments.isEmpty())
				pharmaceuticalObjectID = UnsignedLong::valueOf(arguments.toString());
		} catch (Exception& e) {

		}

		ManagedReference<StimPack*> stimPack;

		if (pharmaceuticalObjectID == 0) {
			stimPack = findStimPack(creature);
		} else {
			SceneObject* inventory = creature->getSlottedObject("inventory");

			if (inventory != NULL) {
				stimPack = inventory->getContainerObject(pharmaceuticalObjectID).castTo<StimPack*>();
			}
		}

		if (!canPerformSkill(creature, targetCreature, stimPack))
			return GENERALERROR;

		float rangeToCheck = 35;

		if (!creature->isInRange(targetCreature, rangeToCheck))
			return TOOFAR;

		if (creature != targetCreature && !CollisionManager::checkLineOfSight(creature, targetCreature)) {
			creature->sendSystemMessage("@container_error_message:container18");
			return GENERALERROR;
		}

		int gcwDefense = (creature->getSkillMod("gcw_defense") * 5);
		int bhDefense = (creature->getSkillMod("bhrank_defense") * 5);

		int basePower = 700;
		int creatureSkill = creature->getSkillMod("healing_efficiency");

		double healingPower = basePower * (1.0 + (creatureSkill / 100.f));

		if(stimPack != NULL) {
			int stimPower = stimPack->calculatePower(creature, targetCreature);
			healingPower += stimPower;
		}

		double sapDebuff = creature->getSkillMod("private_force_sap");

		// Lets see how much healing they are doing.
		if(gcwDefense > 0){
			healingPower += gcwDefense;
		}else if(bhDefense >0){
			healingPower += bhDefense;
		}

		if(sapDebuff > 0){
			sapDebuff = ((100 - sapDebuff) / 100.f);
			//info("HealDebuff" + String::valueOf(sapDebuff), true);
			healingPower *= sapDebuff;
		}

		uint32 healthHealed = targetCreature->healDamage(creature, CreatureAttribute::HEALTH, (int)healingPower, true, false);

		if (creature->isPlayerCreature()) {
			PlayerManager* playerManager = server->getPlayerManager();
			playerManager->sendBattleFatigueMessage(creature, targetCreature);
		}

		sendHealMessage(creature, targetCreature, healthHealed);

		creature->inflictDamage(creature, CreatureAttribute::ACTION, calculateActionCost(creature), false);
		creature->inflictDamage(creature, CreatureAttribute::MIND, calculateMindCost(creature), false);

		if (stimPack != NULL) {
			Locker locker(stimPack);
			stimPack->decreaseUseCount();
		}

		if (!targetCreature->isPet())
			awardXp(creature, "medical", healthHealed); //No experience for healing yourself.

		if (targetCreature != creature)
			clocker.release();


		doAnimationsRange(creature, targetCreature, creature->getDistanceTo(targetCreature));

		creature->notifyObservers(ObserverEventType::MEDPACKUSED);

		return SUCCESS;
	}

};

#endif //HEALDAMAGECOMMAND_H_
