/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef TOTALHEALCOMMAND_H_
#define TOTALHEALCOMMAND_H_

#include "ForceHealQueueCommand.h"

class TotalHealCommand : public ForceHealQueueCommand {
public:

	TotalHealCommand(const String& name, ZoneProcessServer* server)
		: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALSTATESSELFCOMMAND_H_
