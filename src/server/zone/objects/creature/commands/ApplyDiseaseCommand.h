/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef APPLYDISEASECOMMAND_H_
#define APPLYDISEASECOMMAND_H_

class ApplyDiseaseCommand : public QueueCommand {
public:

	ApplyDiseaseCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
	}

};

#endif //APPLYDISEASECOMMAND_H_
