/*
Copyright (C) 2007 <SWGEmu>

This File is part of Core3.

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General
Public License along with this program; if not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Linking Engine3 statically or dynamically with other modules
is making a combined work based on Engine3.
Thus, the terms and conditions of the GNU Lesser General Public License
cover the whole combination.

In addition, as a special exception, the copyright holders of Engine3
give you permission to combine Engine3 program with free software
programs or libraries that are released under the GNU LGPL and with
code included in the standard release of Core3 under the GNU LGPL
license (or modified versions of such code, with unchanged license).
You may copy and distribute such a system following the terms of the
GNU LGPL for Engine3 and the licenses of the other code concerned,
provided that you include the source code of that other code when
and as the GNU LGPL requires distribution of source code.

Note that people who make modified versions of Engine3 are not obligated
to grant this special exception for their modified versions;
it is their choice whether to do so. The GNU Lesser General Public License
gives permission to release a modified version without this exception;
this exception also makes it possible to release a modified version
which carries forward this exception.
*/

#ifndef SERENITYCOMMAND_H_
#define SERENITYCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"

class SerenityCommand : public QueueCommand {
public:

	SerenityCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if(!creature->hasSkill("jedi_natural_novice")){
			if (isWearingArmor(creature)) {
				return NOJEDIARMOR;
			}
		}

		if (creature->containsPendingTask(name)) {
			creature->sendSystemMessage("On cool down");
			return GENERALERROR;
		}


		uint32 buffcrc = BuffCRC::JEDI_FORCE_PROTECTION_1;


		// Force cost of skill.
		int forceCost = 5;

		//Check for and deduct Force cost.

		ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();


		if (playerObject->getForcePower() <= forceCost) {
			creature->sendSystemMessage("@jedi_spam:no_force_power"); //"You do not have enough Force Power to peform that action.

			return GENERALERROR;
		}


		playerObject->setForcePower(playerObject->getForcePower() - forceCost);


			StringIdChatParameter startStringId("jedi_spam", "apply_serenity_start");
			StringIdChatParameter endStringId("jedi_spam", "apply_serenity_end");

			int duration = 30;
			int amount = creature->getSkillMod("serenity");

			ManagedReference<Buff*> buff = new Buff(creature, buffcrc, duration, BuffType::JEDI);
			Locker locker(buff);

			buff->setStartMessage(startStringId);
			buff->setEndMessage(endStringId);
			buff->setSkillModifier("serenity_mod", amount);

			creature->addBuff(buff);
			creature->playEffect("clienteffect/pl_force_resist_bleeding_self.cef", "");

			Reference<CoolDownTask*> cdGroup = new CoolDownTask(creature, 600, name, "serenity_stop");
			creature->addPendingTask(name, cdGroup, 1);
			creature->showFlyText("combat_effects", "serenity_start", 0, 0, 255, true);

		return SUCCESS;
	}

};

#endif //SERENITYCOMMAND_H_
