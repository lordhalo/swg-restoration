/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEEXTINGUISHCOMMAND_H_
#define FORCEEXTINGUISHCOMMAND_H_

#include "ForceHealQueueCommand.h"

class ForceExtinguishCommand : public ForceHealQueueCommand {
public:

	ForceExtinguishCommand(const String& name, ZoneProcessServer* server)
			: ForceHealQueueCommand(name, server) {

	}
};

#endif //HEALALLSELF1COMMAND_H_
