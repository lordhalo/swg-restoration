/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef CENTEROFBEINGCOMMAND_H_
#define CENTEROFBEINGCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/packets/object/ShowFlyText.h"


class CenterOfBeingCommand : public QueueCommand {
	float actionCost;
	String cdName;
	String buffname;
	uint32 buffcrc;
	String buffname2;
	uint32 buffcrc2;

public:

	CenterOfBeingCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {
		actionCost = 225;
		cdName = "forceAura";
		buffname = "melee.center_of_being";
		buffcrc = buffname.hashCode();
		buffname2 = "jedi.force_aura";
		buffcrc2 = buffname2.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		if (creature->getHAM(CreatureAttribute::ACTION) < actionCost) {
			creature->showFlyText("combat_effects", "action_too_tired", 100, 100, 0, true); //You do not have enough mind to do that.
			return GENERALERROR;
		}

		PlayerObject* ghost = creature->getPlayerObject();

		if (creature->hasBuff(buffcrc) || creature->hasBuff(buffcrc2)) {
			creature->sendSystemMessage("@combat_effects:already_centered");
			return GENERALERROR;
		}

		WeaponObject* weapon = creature->getWeapon();

		if (creature->getLocomotion() != 0)
			return GENERALERROR;

		if (weapon->isRangedWeapon())
			return GENERALERROR;

		int duration = 30;
		int rankDefense = creature->getSkillMod("bhrank_defense");
		int rankDefenseFaction = creature->getSkillMod("gcw_defense");
		int efficacy = 250 + rankDefense + rankDefenseFaction;

		creature->inflictDamage(creature, CreatureAttribute::ACTION, actionCost, false);

		Buff* centered = new Buff(creature, buffcrc, duration, BuffType::SKILL);

		Locker locker(centered);

		centered->setSkillModifier("private_defense_bonus", efficacy);


		creature->addBuff(centered);

		return SUCCESS;
	}

};

#endif //CENTEROFBEINGCOMMAND_H_
