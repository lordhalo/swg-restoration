/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCECLOAKCOMMAND_H_
#define FORCECLOAKCOMMAND_H_

#include "server/zone/objects/player/events/ForceCloakTick.h"
#include "server/zone/packets/tangible/TangibleObjectDeltaMessage3.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/Zone.h"
#include "server/zone/objects/scene/SceneObject.h"
#include "JediQueueCommand.h"

class ForceCloakCommand : public JediQueueCommand {
	String buffname;
	uint32 buffcrc;
public:

	ForceCloakCommand(const String& name, ZoneProcessServer* server)
	: JediQueueCommand(name, server) {
		buffname = "forcecloak";
		buffcrc = buffname.hashCode();
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		//return INVALIDSTATE;

		int res = doJediSelfBuffCommand(creature);

		// Return if something errored.
		if (res != SUCCESS) {
			return res;
		}

		if (creature->getRootParent() != NULL) {
			creature->sendSystemMessage("You cannot Force Cloak in a building");
			return INVALIDSTATE;
		}

		if (creature->isInCombat()) {
			return INVALIDSTATE;
		}

		if(!creature->hasBuff(buffcrc)){

			if(creature->hasBuff(STRING_HASHCODE("burstrun")) || creature->hasBuff(BuffCRC::JEDI_FORCE_RUN_1) || creature->hasBuff(BuffCRC::JEDI_FORCE_RUN_2)
					|| creature->hasBuff(BuffCRC::JEDI_FORCE_RUN_3) || creature->hasBuff(STRING_HASHCODE("retreat"))){
				creature->removeBuff(STRING_HASHCODE("burstrun"));
				creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_1);
				creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_2);
				creature->removeBuff(BuffCRC::JEDI_FORCE_RUN_3);
				creature->removeBuff(STRING_HASHCODE("retreat"));
			}

			ManagedReference<Buff*> buff = new Buff(creature, buffcrc, 30, BuffType::JEDI);

			Locker locker(buff);

			buff->setSpeedMultiplierMod(creature->getSpeedMultiplierBase() * .75);
			creature->playEffect("clienteffect/pl_force_speed_self.cef");
			creature->playEffect("clienteffect/combat_special_attacker_cover.cef");
			creature->sendSystemMessage("You use the Force to hide yourself from all beings around you.");

			SortedVector<QuadTreeEntry*> objects(512, 512);
			CloseObjectsVector* closeObjectsVector = (CloseObjectsVector*) creature->getCloseObjects();

			if (closeObjectsVector == NULL) {
				creature->getZone()->getInRangeObjects(creature->getPositionX(), creature->getPositionY(), 32, &objects, true);
			} else {
				closeObjectsVector->safeCopyTo(objects);
			}

			for (int i = 0; i < objects.size(); ++i) {
				SceneObject* object = static_cast<SceneObject*>(objects.get(i));

				if (object->isCreatureObject()) {
					CreatureObject* creo = cast<CreatureObject*>(object);
					creo->notifyDissapear(creature);
				}
			}

			creature->setInvisible(true);

			//creature->setForceCloak();
			creature->setInvisible(true);
			creature->clearCombatState(true);

			creature->addBuff(buff);

			Reference<ForceCloakTick*> forceCloak = new ForceCloakTick(creature);
			creature->addPendingTask("forceCloak", forceCloak, 1000);
		} else {

			creature->setInvisible(false);
			creature->setForceCloak();
			SortedVector<QuadTreeEntry*> objects(512, 512);
			CloseObjectsVector* closeObjectsVector = (CloseObjectsVector*) creature->getCloseObjects();

			if (closeObjectsVector == NULL) {
				creature->getZone()->getInRangeObjects(creature->getPositionX(), creature->getPositionY(), 32, &objects, true);
			} else {
				closeObjectsVector->safeCopyTo(objects);
			}

			for (int i = 0; i < objects.size(); ++i) {
				SceneObject* object = static_cast<SceneObject*>(objects.get(i));

				if (object->isCreatureObject()) {
					CreatureObject* creo = cast<CreatureObject*>(object);
					//obj->notifyInsert(creature);
					if(creo != creature){
						if(creature->getParent() == NULL ){
							creo->notifyInsert(creature);
						}
					}
				}
			}

			creature->removeBuff(buffcrc);

		}
		// Return.
		return SUCCESS;
	}

};

#endif //FORCERUN1COMMAND_H_
