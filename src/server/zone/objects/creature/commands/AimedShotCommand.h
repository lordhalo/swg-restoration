/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef AIMEDSHOTCOMMAND_H_
#define AIMEDSHOTCOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "CombatQueueCommand.h"

class AimedShotCommand : public CombatQueueCommand {
public:

	AimedShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //AimedShotCommand_H_
