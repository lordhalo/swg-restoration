/*
 * HardCollectionComponent.cpp
 *
 *  Created on: Jan 10, 2015
 *      Author: skyyyr
 */

#include "HardCollectionComponent.h"

#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/packets/player/PlayMusicMessage.h"
#include "server/zone/managers/skill/SkillManager.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/packets/object/ObjectMenuResponse.h"

#include "server/zone/managers/loot/LootManager.h"
#include "server/zone/managers/resource/ResourceManager.h"
#include "server/zone/objects/area/ActiveArea.h"


/*
 * Add my own option to allow them to "collect" for their reward.
 */
void HardCollectionComponent::fillObjectMenuResponse(SceneObject* sceneObject, ObjectMenuResponse* menuResponse, CreatureObject* player) const {

	TangibleObjectMenuComponent::fillObjectMenuResponse(sceneObject, menuResponse, player);

	menuResponse->addRadialMenuItem(20, 3, "Collect");

}

/*
 *
 * If they elect to collect, they will be ran through checks that will vary
 *
 * TODO:Pick a sound to play
 * TODO:Put a inventory check in prior to giving anything
 * TODO:Put a skill check just in-case
 *
 * They will be able to earn a skill that gives them a title
 * They will be able to earn a reward item
 * They will be able to earn a badge
 *
 */
int HardCollectionComponent::handleObjectMenuSelect(SceneObject* sceneObject, CreatureObject* creature, byte selectedID) const {

	if (selectedID != 20)
		return 0;

	if (!sceneObject->isASubChildOf(creature))
		return 0;

	//These checks will help ensure they are properly rewarded or not.
	bool earnSkill = false;
	bool earnBadge = false;
	bool earnItem = true;

	if (selectedID == 20) {
		//Tell them that your working.


		if (earnSkill == true) {//&& creature->getScreenPlayState("testCollectionSkill") == 0) {
			//Default skill for testing
			SkillManager::instance()->awardSkill("collection_test", creature, true, true, true);
			creature->setScreenPlayState("testCollectionSkill", 1);
		}

		if (earnBadge == true) {//&& creature->getScreenPlayState("testCollectionBadge") == 0) {
			//Award badge if they deserve one
			ManagedReference<PlayerObject*> player = creature->getPlayerObject();
			player->awardBadge(2);
			creature->setScreenPlayState("testCollectionBadge", 1);
		}

		if (earnItem == true) {//&& creature->getScreenPlayState("testCollectionItem") == 0) {
			//Award item if  they deserve one
			Locker playerLocker(creature);
			ManagedReference<LootManager*> lootManager = creature->getZoneServer()->getLootManager();
			ManagedReference<SceneObject*> inventory = creature->getSlottedObject("inventory");


			//Check if inventory is full, then give reward item.
			if (inventory->isContainerFullRecursive()) {
				creature->sendSystemMessage("You cannot redeem your reward at this time due to your inventory being full."); //	You have too many items in your inventory. In order to get your Padawan Robe you must clear out at least one free slot.
				return 0;
			}
			if (lootManager == NULL || inventory == NULL) {
			creature->sendSystemMessage("You failed to loot an item.");
			return false;
			}

			int dice;
			int level = 1;
			String lootGroup = "";
			//dice = System::random(200);
			level = 1;
			dice = 1;
			
			if (dice == 1) {
				lootGroup = "neck_insp";
			}
			
			/*
			if (dice >= 0 && dice < 160) {
				lootGroup = "forage_food";
			} else if (dice > 159 && dice < 200) {
				lootGroup = "forage_bait";
			} else {
				lootGroup = "forage_rare";
			} */

			lootManager->createLoot(inventory, lootGroup, level);
		}
		//Removes the collection reward item giver
		sceneObject->destroyObjectFromWorld(true);

	}

	/*
	 * Disabled for now.
	PlayMusicMessage* pmm = new PlayMusicMessage("sound/intro.snd");
	creature->sendMessage(pmm);
	*/

	return 0;
}
