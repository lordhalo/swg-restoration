#ifndef HARDCOLLECTIONCOMPONENT_H_
#define HARDCOLLECTIONCOMPONENT_H_

#include "TangibleObjectMenuComponent.h"

class HardCollectionComponent : public TangibleObjectMenuComponent {
public:
	virtual void fillObjectMenuResponse(SceneObject* sceneObject, ObjectMenuResponse* menuResponse, CreatureObject* player) const;
	virtual int handleObjectMenuSelect(SceneObject* sceneObject, CreatureObject* player, byte selectedID) const;

};

#endif /* HardCollectionCOMPONENT_H_ */
