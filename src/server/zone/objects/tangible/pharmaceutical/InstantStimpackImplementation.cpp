
#include "server/zone/objects/tangible/pharmaceutical/InstantStimpack.h"
#include "server/zone/packets/scene/AttributeListMessage.h"

void InstantStimpackImplementation::fillAttributeList(AttributeListMessage* msg, CreatureObject* object) {
	StimPackImplementation::fillAttributeList(msg, object);


	msg->insertAttribute("examine_heal_damage_health", Math::getPrecision(effectiveness, 0));

}
