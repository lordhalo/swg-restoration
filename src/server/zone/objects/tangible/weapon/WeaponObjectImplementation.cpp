/*
 * WeaponObjectImplementation.cpp
 *
 *  Created on: 30/07/2009
 *      Author: victor
 */

#include "server/zone/objects/tangible/weapon/WeaponObject.h"
#include "server/zone/packets/tangible/WeaponObjectMessage3.h"
#include "server/zone/packets/tangible/WeaponObjectMessage6.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/packets/scene/AttributeListMessage.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "templates/tangible/SharedWeaponObjectTemplate.h"
#include "templates/manager/TemplateManager.h"
#include "server/zone/objects/manufactureschematic/craftingvalues/CraftingValues.h"
#include "server/zone/objects/tangible/powerup/PowerupObject.h"
#include "server/zone/objects/tangible/component/lightsaber/LightsaberCrystalComponent.h"
#include "server/zone/packets/object/ObjectMenuResponse.h"
#include "server/zone/packets/object/WeaponRanges.h"
#include "server/zone/packets/tangible/TangibleObjectDeltaMessage3.h"
#include "server/zone/objects/player/sessions/SlicingSession.h"
#include "server/zone/ZoneProcessServer.h"


void WeaponObjectImplementation::initializeTransientMembers() {
	TangibleObjectImplementation::initializeTransientMembers();

	weaponTemplate = dynamic_cast<SharedWeaponObjectTemplate*>(TemplateManager::instance()->getTemplate(serverObjectCRC));

	setLoggingName("WeaponObject");

	if(damageSlice > 1.5 || damageSlice < 1) {
		damageSlice = 1;
	}

	if(damageSlice > 1.5 || damageSlice < 1) {
		damageSlice = 1;
	}

	if(speedSliceLevel > 4 || speedSliceLevel < 0) {
		speedSlice = 0;
	}

	if(customizationLevel > 100 || customizationLevel < 0) {
		customizationLevel = 0;
	}

	if(speedSliceLevel > 4 || speedSliceLevel < 0) {
		speedSlice = 0;
	}

	if(damageSliceLevel > 4 || damageSliceLevel < 0) {
		damageSliceLevel = 0;
	}
}

void WeaponObjectImplementation::notifyLoadFromDatabase() {
	if (forceCost != 0) {
		saberForceCost = forceCost;
		forceCost = 0;
	}

	TangibleObjectImplementation::notifyLoadFromDatabase();
}

void WeaponObjectImplementation::loadTemplateData(SharedObjectTemplate* templateData) {
	TangibleObjectImplementation::loadTemplateData(templateData);

	weaponTemplate = dynamic_cast<SharedWeaponObjectTemplate*>(templateData);

	certified = false;

	attackType = weaponTemplate->getAttackType();
	weaponEffect =  weaponTemplate->getWeaponEffect();
	weaponEffectIndex = weaponTemplate->getWeaponEffectIndex();

	damageType = weaponTemplate->getDamageType();
	elementType = weaponTemplate->getElementType();

	armorPiercing = weaponTemplate->getArmorPiercing();

	healthAttackCost = weaponTemplate->getHealthAttackCost();
	actionAttackCost = weaponTemplate->getActionAttackCost();
	mindAttackCost = weaponTemplate->getMindAttackCost();
	saberForceCost = weaponTemplate->getForceCost();

	pointBlankAccuracy = weaponTemplate->getPointBlankAccuracy();
	pointBlankRange = weaponTemplate->getPointBlankRange();

	idealRange = weaponTemplate->getIdealRange();
	idealAccuracy = weaponTemplate->getIdealAccuracy();

	int templateMaxRange = weaponTemplate->getMaxRange();

	if (templateMaxRange > 5 )
		maxRange = templateMaxRange;

	maxRangeAccuracy = weaponTemplate->getMaxRangeAccuracy();

	minDamage = weaponTemplate->getMinDamage();
	maxDamage = weaponTemplate->getMaxDamage();
	elementalDamage = weaponTemplate->getElementalDamage();
	accuracyBonus = weaponTemplate->getAccuracyBonus();

	woundsRatio = weaponTemplate->getWoundsRatio();

	damageRadius = weaponTemplate->getArea();

	float templateAttackSpeed = weaponTemplate->getAttackSpeed();

	if (templateAttackSpeed > 1)
		attackSpeed = templateAttackSpeed;

	if (!isJediWeapon()) {
		setSliceable(true);
	} else if (isJediWeapon()) {
		setSliceable(false);
	}
}

void WeaponObjectImplementation::sendContainerTo(CreatureObject* player) {

	if (isJediWeapon()) {

		ManagedReference<SceneObject*> saberInv = getSlottedObject("saber_inv");

		if (saberInv != NULL) {
			saberInv->sendDestroyTo(player);
			//saberInv->closeContainerTo(player, true);

			saberInv->sendWithoutContainerObjectsTo(player);
			saberInv->openContainerTo(player);
		}

	}
}

void WeaponObjectImplementation::createChildObjects() {
	// Create any child objects in a weapon.

	ZoneServer* zoneServer = server->getZoneServer();

		for (int i = 0; i < templateObject->getChildObjectsSize(); ++i) {
			ChildObject* child = templateObject->getChildObject(i);

			if (child == NULL)
				continue;

			ManagedReference<SceneObject*> obj = zoneServer->createObject(
					child->getTemplateFile().hashCode(), getPersistenceLevel());

			if (obj == NULL)
				continue;

			ContainerPermissions* permissions = obj->getContainerPermissions();
			permissions->setOwner(getObjectID());
			permissions->setInheritPermissionsFromParent(true);
			permissions->setDefaultDenyPermission(ContainerPermissions::MOVECONTAINER);
			permissions->setDenyPermission("owner", ContainerPermissions::MOVECONTAINER);

			if (!transferObject(obj, child->getContainmentType())) {
				obj->destroyObjectFromDatabase(true);
				continue;
			}

			childObjects.put(obj);

			obj->initializeChildObject(_this.getReferenceUnsafeStaticCast());
		}

}

void WeaponObjectImplementation::sendBaselinesTo(SceneObject* player) {
	debug("sending weapon object baselines");

	BaseMessage* weao3 = new WeaponObjectMessage3(_this.getReferenceUnsafeStaticCast());
	player->sendMessage(weao3);

	BaseMessage* weao6 = new WeaponObjectMessage6(_this.getReferenceUnsafeStaticCast());
	player->sendMessage(weao6);

	if (player->isCreatureObject()) {
		BaseMessage* ranges = new WeaponRanges(cast<CreatureObject*>(player), _this.getReferenceUnsafeStaticCast());
		player->sendMessage(ranges);
	}
}

String WeaponObjectImplementation::getWeaponType() {
	int weaponObjectType = getGameObjectType();

	String weaponType;

	switch (weaponObjectType) {
	case SceneObjectType::ONEHANDMELEEWEAPON:
		weaponType = "onehandmelee";
		break;
	case SceneObjectType::TWOHANDMELEEWEAPON:
		weaponType = "twohandmelee";
		break;
	case SceneObjectType::WEAPON:
	case SceneObjectType::MELEEWEAPON:
		weaponType = "unarmed";
		break;
	case SceneObjectType::RIFLE:
		weaponType = "rifle";
		break;
	case SceneObjectType::PISTOL:
		weaponType = "pistol";
		break;
	case SceneObjectType::CARBINE:
		weaponType = "carbine";
		break;
	case SceneObjectType::POLEARM:
		weaponType = "polearm";
		break;
	case SceneObjectType::THROWNWEAPON:
		weaponType = "thrownweapon";
		break;
	case SceneObjectType::MINE:
	case SceneObjectType::SPECIALHEAVYWEAPON:
	case SceneObjectType::HEAVYWEAPON:
		weaponType = "heavyweapon";
		break;
	default:
		weaponType = "unarmed";
		break;
	}

	if (isJediOneHandedWeapon()) weaponType = "onehandlightsaber";
	if (isJediTwoHandedWeapon()) weaponType = "twohandlightsaber";
	if (isJediPolearmWeapon()) weaponType = "polearmlightsaber";

	return weaponType;
}

void WeaponObjectImplementation::fillAttributeList(AttributeListMessage* alm, CreatureObject* object) {
	TangibleObjectImplementation::fillAttributeList(alm, object);

	bool res = isCertifiedFor(object);

	if (res) {
		alm->insertAttribute("weapon_cert_status", "Yes");
	} else {
		alm->insertAttribute("weapon_cert_status", "No");
	}

	if(hasPowerup())
		powerupObject->fillWeaponAttributeList(alm, _this.getReferenceUnsafeStaticCast());

	if (sliced){
		alm->insertAttribute("wpn_cust", String::valueOf(getCustomizationLevel()) + "%");
		if(damageSlice > 1){
			alm->insertAttribute("wpn_damage", String::valueOf((int)((100 - (damageSlice * 100)) * 1.0)) + "%");
		}
		if(speedSliceLevel > 0){
			alm->insertAttribute("wpn_fire_rate", String::valueOf(100 - (speedSlice * 100)) + "%");
		}
		if(criticalSlice > 1){
			alm->insertAttribute("wpn_crit_chance", String::valueOf(criticalSlice) + "%");
		}
	}

	/*if (usesRemaining > 0)
		alm->insertAttribute("count", usesRemaining);*/

	for(int i = 0; i < wearableSkillMods.size(); ++i) {
		String key = wearableSkillMods.elementAt(i).getKey();
		String statname = "cat_skill_mod_bonus.@stat_n:" + key;
		int value = wearableSkillMods.get(key);

		if (value > 0)
			alm->insertAttribute(statname, value);
	}

	//alm->insertAttribute("wpn_armor_pierce_rating", "Light");

	//Damage Information
	StringBuffer dmgtxt;

	switch (damageType) {
	case SharedWeaponObjectTemplate::KINETIC:
		dmgtxt << "Kinetic";
		break;
	case SharedWeaponObjectTemplate::ENERGY:
		dmgtxt << "Energy";
		break;
	case SharedWeaponObjectTemplate::ELECTRICITY:
		dmgtxt << "Electricity";
		break;
	case SharedWeaponObjectTemplate::STUN:
		dmgtxt << "Stun";
		break;
	case SharedWeaponObjectTemplate::BLAST:
		dmgtxt << "Blast";
		break;
	case SharedWeaponObjectTemplate::HEAT:
		dmgtxt << "Heat";
		break;
	case SharedWeaponObjectTemplate::COLD:
		dmgtxt << "Cold";
		break;
	case SharedWeaponObjectTemplate::ACID:
		dmgtxt << "Acid";
		break;
	case SharedWeaponObjectTemplate::LIGHTSABER:
		dmgtxt << "Lightsaber";
		break;
	default:
		dmgtxt << "Unknown";
		break;
	}

	StringBuffer eletxt;

	switch (elementType) {
	case SharedWeaponObjectTemplate::ELECTRICITY:
		eletxt << "Electricity";
		break;
	case SharedWeaponObjectTemplate::HEAT:
		eletxt << "Heat";
		break;
	case SharedWeaponObjectTemplate::COLD:
		eletxt << "Cold";
		break;
	case SharedWeaponObjectTemplate::ACID:
		eletxt << "Acid";
		break;
	default:
		eletxt << "Unknown";
		break;
	}

	alm->insertAttribute("damage.wpn_damage_type", dmgtxt);

	alm->insertAttribute("damage.wpn_attack_speed", Math::getPrecision(getAttackSpeed(), 2));

	Vector<String>* weaponSpeedMods = getSpeedModifiers();
	float speedMods = 0;
	for (int i = 0; i < weaponSpeedMods->size(); ++i) {
		speedMods += object->getSkillMod(weaponSpeedMods->get(i));
	}

	if(isRangedWeapon()){
		speedMods += object->getSkillMod("private_ranged_speed_bonus");
		speedMods += object->getSkillMod("force_ranged_speed");
		speedMods += object->getSkillMod("ranged_speed");
	}

	if(isMeleeWeapon()){
		speedMods += object->getSkillMod("private_melee_speed_bonus");
		speedMods += object->getSkillMod("force_melee_speed");
		speedMods += object->getSkillMod("melee_speed");
	}

	//if(speedMods > 125){
	//	speedMods = 125.f + ((speedMods - 125)  * .05);
	//}
	//float speedMod = object->getSkillMod(getSpeedModifiers());
	//float modifiedSpeed = getAttackSpeed() - (speedMods / 100.f);
	float modifiedSpeed = Math::getPrecision((float) (getAttackSpeed(true) * (0.5231 + (0.4769 * pow(2, -speedMods / 46)))), 2);

	if(modifiedSpeed < 1.0){
		modifiedSpeed = 1.0;
	}
	alm->insertAttribute("damage.wpn_real_speed", Math::getPrecision(modifiedSpeed, 2));

	if (getDamageRadius() != 0.0f)
		alm->insertAttribute("area", Math::getPrecision(getDamageRadius(), 0));


	float minDmg = round(getMinDamage());
	float maxDmg = round(getMaxDamage());

	alm->insertAttribute("damage.damage", String::valueOf(round(getMinDamage(true))) + " - " + String::valueOf(round(getMaxDamage(true))));

	if (elementType != 0) {
		alm->insertAttribute("damage.wpn_elemental_type", eletxt);
		if(getElementalDamage(false)){
			alm->insertAttribute("damage.wpn_elemental_value", String::valueOf(round(getElementalDamage(false))));
			if((getElementalModification() == false) && !isJediWeapon()) {
				setMinDamage(getMinDamage(false) - getElementalDamage(false));
				setMaxDamage(getMaxDamage(false) - getElementalDamage(false));
				setElementalModification(true);
			}
		}
	}


	alm->insertAttribute("damage.wpn_accuracy", getAccuracyBonus());

	StringBuffer woundsratio;

	float wnd = round(10 * getWoundsRatio()) / 10.0f;

	woundsratio << wnd << "%";

	alm->insertAttribute("damage.wpn_wound_chance", woundsratio);

	float baseDamage = (minDmg + maxDmg) / 2.0f;
	float eleBonus = getElementalDamage();
	float dps = (baseDamage + eleBonus) / getAttackSpeed();
	float dpsmod = (baseDamage + eleBonus) / modifiedSpeed;

	alm->insertAttribute("damage.wpn_base_dps", String::valueOf(dps) + " / sec");
	alm->insertAttribute("damage.wpn_real_dps", String::valueOf(dpsmod) + " / sec");


	if(getMaxRange() > 65){
		setMaxRange(65);
	}

	if(isCarbineWeapon()){
		setMaxRange(50);
	}

	if(isPistolWeapon()){
		setMaxRange(35);
	}

	alm->insertAttribute("cat_wpn_other.wpn_range", " 0-" + String::valueOf(getMaxRange()));

	if(getActionAttackCost() < 50){
		setActionAttackCost(100);
	}
	alm->insertAttribute("cat_wpn_other.cat_wpn_attack_cost", getActionAttackCost());

	if(sliced)
		alm->insertAttribute("cat_wpn_other.wpn_attr", "@obj_attr_n:hacked1");

	// Force Cost
	if (getForceCost() > 0)
		alm->insertAttribute("cat_wpn_other.forcecost", getForceCost());

	//Anti Decay Kit
	if(hasAntiDecayKit()){
		alm->insertAttribute("@veteran_new:antidecay_examine_title", "@veteran_new:antidecay_examine_text");
	}

	for (int i = 0; i < getNumberOfDots(); i++) {

			String dt;

			switch (getDotType(i)) {
			case 1:
				dt = "Poison";
				break;
			case 2:
				dt = "Disease";
				break;
			case 3:
				dt = "Fire";
				break;
			case 4:
				dt = "Bleeding";
				break;
			default:
				dt = "Unknown";
				break;
			}

			StringBuffer type;
			type << "cat_wpn_dot_0" << i << ".wpn_dot_type";
			alm->insertAttribute(type.toString(), dt);

			String da;

			switch (getDotAttribute(i)) {
			case 0:
				da = "Health";
				break;
			case 1:
				da = "Strength";
				break;
			case 2:
				da = "Constitution";
				break;
			case 3:
				da = "Action";
				break;
			case 4:
				da = "Quickness";
				break;
			case 5:
				da = "Stamina";
				break;
			case 6:
				da = "Mind";
				break;
			case 7:
				da = "Focus";
				break;
			case 8:
				da = "Willpower";
				break;
			default:
				da = "Unknown";
				break;
			}

			StringBuffer attrib;
			attrib << "cat_wpn_dot_0" << i << ".wpn_dot_attrib";
			alm->insertAttribute(attrib.toString(), da);

			StringBuffer str;
			str << "cat_wpn_dot_0" << i << ".wpn_dot_strength";
			alm->insertAttribute(str.toString(), getDotStrength(i));

			StringBuffer dotDur;
			dotDur << getDotDuration(i) << "s";
			StringBuffer dur;
			dur << "cat_wpn_dot_0" << i << ".wpn_dot_duration";
			alm->insertAttribute(dur.toString(), dotDur);

			StringBuffer dotPot;
			dotPot << getDotPotency(i) << "%";
			StringBuffer pot;
			pot << "cat_wpn_dot_0" << i << ".wpn_dot_potency";
			alm->insertAttribute(pot.toString(), dotPot);

			StringBuffer use;
			use << "cat_wpn_dot_0" << i << ".wpn_dot_uses";
			alm->insertAttribute(use.toString(), getDotUses(i));
		}

}

int WeaponObjectImplementation::getPointBlankAccuracy(bool withPup) {

	if(powerupObject != NULL && withPup)
		return pointBlankAccuracy + (abs(pointBlankAccuracy) * powerupObject->getPowerupStat("pointBlankAccuracy"));

	return pointBlankAccuracy;
}

int WeaponObjectImplementation::getPointBlankRange(bool withPup) {

	if(powerupObject != NULL && withPup)
		return pointBlankRange + (abs(pointBlankRange) * powerupObject->getPowerupStat("pointBlankRange"));

	return pointBlankRange;
}

int WeaponObjectImplementation::getIdealRange(bool withPup) {

	if(powerupObject != NULL && withPup)
		return idealRange + (abs(idealRange) * powerupObject->getPowerupStat("idealRange"));

	return idealRange;
}

int WeaponObjectImplementation::getMaxRange(bool withPup) {

	if(powerupObject != NULL && withPup)
		return maxRange + (abs(maxRange) * powerupObject->getPowerupStat("maxRange"));

	return maxRange;
}

int WeaponObjectImplementation::getIdealAccuracy(bool withPup) {

	if(powerupObject != NULL && withPup)
		return idealAccuracy + (abs(idealAccuracy) * powerupObject->getPowerupStat("idealAccuracy"));

	return idealAccuracy;
}


int WeaponObjectImplementation::getMaxRangeAccuracy(bool withPup) {

	if(powerupObject != NULL && withPup)
		return maxRangeAccuracy + (abs(maxRangeAccuracy) * powerupObject->getPowerupStat("maxRangeAccuracy"));

	return maxRangeAccuracy;
}

float WeaponObjectImplementation::getAttackSpeed(bool withPup) {

	float speed = attackSpeed;

	if(sliced)
		speed *= speedSlice;

	if(powerupObject != NULL && withPup)
		speed -= (speed * powerupObject->getPowerupStat("attackSpeed"));

	float calcSpeed = speed + getConditionReduction(speed);

	if(calcSpeed < 0.1)
		calcSpeed = 0.1;

	return calcSpeed;
}


float WeaponObjectImplementation::getMaxDamage(bool withPup) {

	float damage = maxDamage;

	if(sliced)
		damage *= damageSlice;

	if(powerupObject != NULL && withPup) {
		damage += (damage * powerupObject->getPowerupStat("maxDamage"));
		return damage - getConditionReduction(damage);
	}

	return damage - getConditionReduction(damage);
}

float WeaponObjectImplementation::getMinDamage(bool withPup) {

	float damage = minDamage;

	if(sliced)
		damage *= damageSlice;

	if(powerupObject != NULL && withPup) {
		damage += (damage * powerupObject->getPowerupStat("minDamage"));
		return damage - getConditionReduction(damage);
	}

	return damage - getConditionReduction(damage);
}

float WeaponObjectImplementation::getElementalDamage(bool withPup) {

	float eleDamage = elementalDamage;

	return eleDamage - getConditionReduction(eleDamage);
}

float WeaponObjectImplementation::getWoundsRatio(bool withPup) {

	if(powerupObject != NULL && withPup)
		return woundsRatio + (woundsRatio * powerupObject->getPowerupStat("woundsRatio"));

	return woundsRatio;
}

float WeaponObjectImplementation::getAccuracyBonus(bool withPup) {

	if(powerupObject != NULL && withPup)
		return accuracyBonus + (accuracyBonus * powerupObject->getPowerupStat("accuracyBonus"));

	return accuracyBonus;
}

float WeaponObjectImplementation::getDamageRadius(bool withPup) {

	if(powerupObject != NULL && withPup)
		return damageRadius + (damageRadius * powerupObject->getPowerupStat("damageRadius"));

	return damageRadius;
}


int WeaponObjectImplementation::getHealthAttackCost(bool withPup) {

	if(powerupObject != NULL && withPup)
		return healthAttackCost - (abs(healthAttackCost) * powerupObject->getPowerupStat("healthAttackCost"));

	return healthAttackCost;
}

int WeaponObjectImplementation::getActionAttackCost(bool withPup) {

	if(powerupObject != NULL && withPup)
		return actionAttackCost - (abs(actionAttackCost) * powerupObject->getPowerupStat("actionAttackCost"));

	return actionAttackCost;
}

int WeaponObjectImplementation::getMindAttackCost(bool withPup) {

	if(powerupObject != NULL && withPup)
		return mindAttackCost - (abs(mindAttackCost) * powerupObject->getPowerupStat("mindAttackCost"));

	return mindAttackCost;
}

void WeaponObjectImplementation::updateCraftingValues(CraftingValues* values, bool firstUpdate) {
	/*
	 * Incoming Values:					Ranges:
	 * mindamage						Differs between weapons
	 * maxdamage
	 * attackspeed
	 * woundchance
	 * roundsused
	 * hitpoints
	 * zerorangemod
	 * maxrange
	 * maxrangemod
	 * midrange
	 * midrangemod
	 * charges
	 * attackhealthcost
	 * attackactioncost
	 * attackmindcost
	 */
	float value = 0.f;
	setMinDamage(Math::max(values->getCurrentValue("mindamage"), 0.f));
	setMaxDamage(Math::max(values->getCurrentValue("maxdamage"), 0.f));
	setElementalDamage(Math::max(values->getCurrentValue("elementalvalue"), 0.f));

	setAttackSpeed(values->getCurrentValue("attackspeed"));
	setHealthAttackCost((int)values->getCurrentValue("attackhealthcost"));
	setActionAttackCost((int)values->getCurrentValue("attackactioncost"));
	setMindAttackCost((int)values->getCurrentValue("attackmindcost"));

	if (isJediWeapon()) {
		setForceCost((int)(values->getCurrentValue("forcecost")));
		setBladeColor(31);
	}

	value = values->getCurrentValue("woundchance");
	if (value != ValuesMap::VALUENOTFOUND)
		setWoundsRatio(value);

	//value = craftingValues->getCurrentValue("roundsused");
	//if(value != DraftSchematicValuesImplementation::VALUENOTFOUND)
		//_this.getReferenceUnsafeStaticCast()->set_______(value);

	value = values->getCurrentValue("zerorangemod");
	if (value != ValuesMap::VALUENOTFOUND)
		setPointBlankAccuracy((int)value);

	value = values->getCurrentValue("maxrange");
	if (value != ValuesMap::VALUENOTFOUND)
		setMaxRange((int)value);

	value = values->getCurrentValue("maxrangemod");
	if (value != ValuesMap::VALUENOTFOUND)
		setMaxRangeAccuracy((int)value);

	value = values->getCurrentValue("midrange");
	if (value != ValuesMap::VALUENOTFOUND)
		setIdealRange((int)value);

	value = values->getCurrentValue("midrangemod");
	if (value != ValuesMap::VALUENOTFOUND)
		setIdealAccuracy((int)value);

	value = values->getCurrentValue("wpn_accuracy");
	if (value != ValuesMap::VALUENOTFOUND)
		setAccuracyBonus((int)value);

	//value = craftingValues->getCurrentValue("charges");
	//if(value != CraftingValues::VALUENOTFOUND)
	//	setUsesRemaining((int)value);

	value = values->getCurrentValue("hitpoints");
	if (value != ValuesMap::VALUENOTFOUND)
		setMaxCondition((int)value);

	setConditionDamage(0);
}

bool WeaponObjectImplementation::isCertifiedFor(CreatureObject* object) {
	ManagedReference<PlayerObject*> ghost = object->getPlayerObject();

	if (ghost == NULL)
		return false;

	Vector<String>* certificationsRequired = weaponTemplate->getCertificationsRequired();

	for (int i = 0; i < certificationsRequired->size(); ++i) {
		const String& cert = certificationsRequired->get(i);

		if (!ghost->hasAbility(cert) && !object->hasSkill(cert)) {
			return false;
		}
	}

	return true;
}

void WeaponObjectImplementation::decreasePowerupUses(CreatureObject* player) {
	if (hasPowerup()) {
		powerupObject->decreaseUses();
		if (powerupObject->getUses() < 1) {
			Locker locker(_this.getReferenceUnsafeStaticCast());
			StringIdChatParameter message("powerup", "prose_pup_expire"); //The powerup on your %TT has expired.
			message.setTT(getDisplayedName());

			player->sendSystemMessage(message);

			ManagedReference<PowerupObject*> pup = removePowerup();
			if(pup != NULL) {
				Locker plocker(pup);

				pup->destroyObjectFromWorld( true );
				pup->destroyObjectFromDatabase( true );
			}
		}
		sendAttributeListTo(player);
	}
}

String WeaponObjectImplementation::repairAttempt(int repairChance) {

	String message = "@error_message:";

	if(repairChance < 25) {
		message += "sys_repair_failed";
		setMaxCondition(1, true);
		setConditionDamage(0, true);
	} else if(repairChance < 50) {
		message += "sys_repair_imperfect";
		setMaxCondition(getMaxCondition() * .65f, true);
		setConditionDamage(0, true);
	} else if(repairChance < 75) {
		setMaxCondition(getMaxCondition() * .80f, true);
		setConditionDamage(0, true);
		message += "sys_repair_slight";
	} else {
		setMaxCondition(getMaxCondition() * .95f, true);
		setConditionDamage(0, true);
		message += "sys_repair_perfect";
	}

	return message;
}

void WeaponObjectImplementation::decay(CreatureObject* user) {
	if (_this.getReferenceUnsafeStaticCast() == user->getSlottedObject("default_weapon") || user->isAiAgent() || hasAntiDecayKit()) {
		return;
	}

	int roll = System::random(100);
	int chance = 5;

	if (hasPowerup())
		chance += 10;

	if (roll < chance) {
		Locker locker(_this.getReferenceUnsafeStaticCast());

		if (isJediWeapon()) {
			ManagedReference<SceneObject*> saberInv = getSlottedObject("saber_inv");

			if (saberInv == NULL)
				return;

			// TODO: is this supposed to be every crystal, or random crystal(s)?
			for (int i = 0; i < saberInv->getContainerObjectsSize(); i++) {
				ManagedReference<LightsaberCrystalComponent*> crystal = saberInv->getContainerObject(i).castTo<LightsaberCrystalComponent*>();

				if (crystal != NULL && crystal->getColor() == 31) {
					crystal->inflictDamage(crystal, 0, 1, true, true);
				}
			}
		} else {
			inflictDamage(_this.getReferenceUnsafeStaticCast(), 0, 1, true, true);

			if (((float)conditionDamage - 1 / (float)maxCondition < 0.75) && ((float)conditionDamage / (float)maxCondition > 0.75))
				user->sendSystemMessage("@combat_effects:weapon_quarter");
			if (((float)conditionDamage - 1 / (float)maxCondition < 0.50) && ((float)conditionDamage / (float)maxCondition > 0.50))
				user->sendSystemMessage("@combat_effects:weapon_half");
		}
	}
}

bool WeaponObjectImplementation::isEquipped() {
	ManagedReference<SceneObject*> parent = getParent().get();
	if (parent != NULL && parent->isPlayerCreature())
		return true;

	return false;
}

void WeaponObjectImplementation::applySkillModsTo(CreatureObject* creature) {
	if (creature == NULL) {
		return;
	}

	for (int i = 0; i < wearableSkillMods.size(); ++i) {
		String name = wearableSkillMods.elementAt(i).getKey();
		int value = wearableSkillMods.get(name);

		if (!SkillModManager::instance()->isWearableModDisabled(name))
			creature->addSkillMod(SkillModManager::WEARABLE, name, value, true);
	}

	SkillModManager::instance()->verifyWearableSkillMods(creature);
}

void WeaponObjectImplementation::removeSkillModsFrom(CreatureObject* creature) {
	if (creature == NULL) {
		return;
	}

	for (int i = 0; i < wearableSkillMods.size(); ++i) {
		String name = wearableSkillMods.elementAt(i).getKey();
		int value = wearableSkillMods.get(name);

		if (!SkillModManager::instance()->isWearableModDisabled(name))
			creature->removeSkillMod(SkillModManager::WEARABLE, name, value, true);
	}

	SkillModManager::instance()->verifyWearableSkillMods(creature);
}

bool WeaponObjectImplementation::applyPowerup(CreatureObject* player, PowerupObject* pup) {
	if(hasPowerup())
		return false;

	addMagicBit(true);

	powerupObject = pup;

	if(pup->getParent() != NULL) {
		Locker clocker(pup, player);
		pup->destroyObjectFromWorld(true);
	}

	sendAttributeListTo(player);

	return true;
}

PowerupObject* WeaponObjectImplementation::removePowerup() {
	if(!hasPowerup())
		return NULL;

	PowerupObject* pup = powerupObject;
	powerupObject = NULL;

	removeMagicBit(true);

	return pup;
}
