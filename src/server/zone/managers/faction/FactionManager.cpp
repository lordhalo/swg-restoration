/*
 * FactionManager.cpp
 *
 *  Created on: Mar 17, 2011
 *      Author: crush
 */

#include "FactionManager.h"
#include "FactionMap.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "templates/manager/TemplateManager.h"

FactionManager::FactionManager() {
	setLoggingName("FactionManager");
	setGlobalLogging(false);
	setLogging(false);

	frsLog.setLoggingName("FRSLogger");
	StringBuffer fileName;
	fileName << "log/frs/frs.log";
	frsLog.setFileLogger(fileName.toString(), true);
	frsLog.setLogging(true);
}

void FactionManager::loadData() {
	loadLuaConfig();
	loadFactionRanks();
}

void FactionManager::loadFactionRanks() {
	IffStream* iffStream = TemplateManager::instance()->openIffFile("datatables/faction/rank.iff");

	if (iffStream == NULL) {
		warning("Faction ranks could not be found.");
		return;
	}

	DataTableIff dtiff;
	dtiff.readObject(iffStream);

	factionRanks.readObject(&dtiff);

	delete iffStream;

	info("loaded " + String::valueOf(factionRanks.getCount()) + " ranks", true);
}

void FactionManager::loadLuaConfig() {
	info("Loading config file.", true);

	FactionMap* fMap = getFactionMap();

	Lua* lua = new Lua();
	lua->init();

	//Load the faction manager lua file.
	lua->runFile("scripts/managers/faction_manager.lua");

	LuaObject luaObject = lua->getGlobalObject("factionList");

	if (luaObject.isValidTable()) {
		for (int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject factionData = luaObject.getObjectAt(i);

			if (factionData.isValidTable()) {
				String factionName = factionData.getStringAt(1);
				bool playerAllowed = factionData.getBooleanAt(2);
				String enemies = factionData.getStringAt(3);
				String allies = factionData.getStringAt(4);
				float adjustFactor = factionData.getFloatAt(5);

				Faction faction(factionName);
				faction.setAdjustFactor(adjustFactor);
				faction.setPlayerAllowed(playerAllowed);
				faction.parseEnemiesFromList(enemies);
				faction.parseAlliesFromList(allies);

				fMap->addFaction(faction);
			}

			factionData.pop();
		}
	}

	luaObject.pop();

	delete lua;
	lua = NULL;
}

FactionMap* FactionManager::getFactionMap() {
	return &factionMap;
}

void FactionManager::awardFactionStanding(CreatureObject* player, const String& factionName, int level) {
	if (player == NULL)
		return;

	ManagedReference<PlayerObject*> ghost = player->getPlayerObject();

	if (!factionMap.contains(factionName))
		return;

	const Faction& faction = factionMap.get(factionName);
	const SortedVector<String>* enemies = faction.getEnemies();
	const SortedVector<String>* allies = faction.getAllies();

	if (!faction.isPlayerAllowed())
		return;

	float gain = level * faction.getAdjustFactor();
	float lose = gain * 2;

	ghost->decreaseFactionStanding(factionName, lose);

	//Lose faction standing to allies of the creature.
	for (int i = 0; i < allies->size(); ++i) {
		const String& ally = allies->get(i);

		if ((ally == "rebel" || ally == "imperial")) {
			continue;
		}

		if (!factionMap.contains(ally))
			continue;

		const Faction& allyFaction = factionMap.get(ally);

		if (!allyFaction.isPlayerAllowed())
			continue;

		if(!player->hasSkill("rank_grey_novice")){
			ghost->decreaseFactionStanding(ally, lose);
		}
	}

	bool gcw = false;
	if (factionName == "rebel" || factionName == "imperial") {
		gcw = true;
	}

	//Gain faction standing to enemies of the creature.
	for (int i = 0; i < enemies->size(); ++i) {
		const String& enemy = enemies->get(i);

		if ((enemy == "rebel" || enemy == "imperial") && !gcw) {
			continue;
		}

		if (!factionMap.contains(enemy))
			continue;

		const Faction& enemyFaction = factionMap.get(enemy);

		if (!enemyFaction.isPlayerAllowed())
			continue;

		if(player->hasSkill("rank_grey_novice")){
			gain /= 2;
		}
		ghost->increaseFactionStanding(enemy, gain);
	}
}


void FactionManager::awardPvpFactionPoints(TangibleObject* killer, CreatureObject* destructedObject) {
	if (killer->isPlayerCreature() && destructedObject->isPlayerCreature()) {
		CreatureObject* killerCreature = cast<CreatureObject*>(killer);
		ManagedReference<PlayerObject*> ghost = killerCreature->getPlayerObject();

		ManagedReference<PlayerObject*> killedGhost = destructedObject->getPlayerObject();

		if (killer->isRebel() && destructedObject->isImperial()) {
			ghost->increaseFactionStanding("rebel", 30);
			ghost->decreaseFactionStanding("imperial", 45);

			killedGhost->decreaseFactionStanding("imperial", 45);
		} else if (killer->isImperial() && destructedObject->isRebel()) {
			ghost->increaseFactionStanding("imperial", 30);
			ghost->decreaseFactionStanding("rebel", 45);

			killedGhost->decreaseFactionStanding("rebel", 45);
		}
	}
}

void FactionManager::awardFrsPoints(TangibleObject* killer, CreatureObject* destructedObject) {

	if (killer->isPlayerCreature() && destructedObject->isPlayerCreature()) {
		CreatureObject* killerCreature = cast<CreatureObject*>(killer);
		ManagedReference<PlayerObject*> ghost = killerCreature->getPlayerObject();

		ManagedReference<PlayerObject*> killedGhost = destructedObject->getPlayerObject();

		int multi = destructedObject->getSkillMod("rank_control");
		int devi = killerCreature->getSkillMod("rank_control");

		multi /= 10.f;
		devi /= 5.f;

		if(multi <= 0){
			multi = 1;
		}

		int award = ((1000 * multi) / devi);

		if(award < 10){
			award = 10;
		}

		StringIdChatParameter message("base_player","prose_grant_xp");
		message.setDI(award);
		message.setTO("exp_n", "force_rank_xp");
		killerCreature->sendSystemMessage(message);

		destructedObject->sendSystemMessage("You have lost Force Rank Experience: " + String::valueOf((award/2) * -1));
		frsLog.info(killerCreature->getFirstName() + " " +  "Killed" + " " + destructedObject->getFirstName() + " " + "and Gained:" + String::valueOf(award) , true);

		ghost->addExperience("force_rank_xp", award);
		killedGhost->addExperience("force_rank_xp", ((award/2) * -1));
	}
}

void FactionManager::awardRankPoints(TangibleObject* killer, CreatureObject* destructedObject) {

	if (killer->isPlayerCreature() && destructedObject->isPlayerCreature()) {
		CreatureObject* killerCreature = cast<CreatureObject*>(killer);
		ManagedReference<PlayerObject*> ghost = killerCreature->getPlayerObject();

		ManagedReference<PlayerObject*> killedGhost = destructedObject->getPlayerObject();

		bool deathWatch = false;
		bool blackSun = false;
		bool stormTrooper = false;
		bool rebelSpecialForce = false;
		bool isJedi = false;
		bool isSith = false;

		String expType;

		if(killerCreature->hasSkill("death_watch_novice")){
			deathWatch = true;
			expType = "rank_xp";
		}
		else if(killerCreature->hasSkill("black_sun_novice")){
			blackSun = true;
			expType = "rank_xp";
		}
		else if(killerCreature->hasSkill("imp_rank_novice")){
			stormTrooper = true;
			expType = "gcw_xp";
		}
		else if(killerCreature->hasSkill("reb_rank_novice")){
			rebelSpecialForce = true;
			expType = "gcw_xp";
		}
		else if(killerCreature->hasSkill("rank_light_novice")){
			isJedi = true;
			expType = "force_rank_xp";
		}
		else if(killerCreature->hasSkill("rank_sith_novice")){
			isSith = true;
			expType = "force_rank_xp";
		}
		else{

		}

		int award = (5 * System::random(10));

		if(isJedi == true or isSith == true){
			award /= 2;
		}

		info(expType, true);

		StringIdChatParameter message("base_player","prose_grant_xp");
		message.setDI(award);
		message.setTO("exp_n", expType);

		killerCreature->sendSystemMessage(message);
		//destructedObject->sendSystemMessage("You have lost Force Rank Experience: " + String::valueOf((award/2) * -1));

		frsLog.info(killerCreature->getFirstName() + " " +  "Killed" + " " + destructedObject->getFirstName() + " " + "and Gained:" + String::valueOf(award) , true);

		ghost->addExperience(expType, award);
		//killedGhost->addExperience("force_rank_xp", ((award/2) * -1));
	}
}

String FactionManager::getRankName(int idx) {
	if (idx >= factionRanks.getCount())
		return "";

	return factionRanks.getRank(idx).getName();
}

int FactionManager::getRankCost(int rank) {
	if (rank >= factionRanks.getCount())
		return -1;

	return factionRanks.getRank(rank).getCost();
}

int FactionManager::getRankDelegateRatioFrom(int rank) {
	if (rank >= factionRanks.getCount())
		return -1;

	return factionRanks.getRank(rank).getDelegateRatioFrom();
}

int FactionManager::getRankDelegateRatioTo(int rank) {
	if (rank >= factionRanks.getCount())
		return -1;

	return factionRanks.getRank(rank).getDelegateRatioTo();
}

int FactionManager::getFactionPointsCap(int rank) {
	if (rank >= factionRanks.getCount())
		return -1;

	return Math::max(1000, getRankCost(rank) * 20);
}

bool FactionManager::isFaction(const String& faction) {
	if (factionMap.contains(faction))
		return true;

	return false;
}

bool FactionManager::isEnemy(const String& faction1, const String& faction2) {

	if (!factionMap.contains(faction1) || !factionMap.contains(faction2))
		return false;

	Faction* faction = factionMap.getFaction(faction1);

	return faction->getEnemies()->contains(faction2);
}

bool FactionManager::isAlly(const String& faction1, const String& faction2) {

	if (!factionMap.contains(faction1) || !factionMap.contains(faction2))
		return false;

	Faction* faction = factionMap.getFaction(faction1);

	return faction->getAllies()->contains(faction2);
}
