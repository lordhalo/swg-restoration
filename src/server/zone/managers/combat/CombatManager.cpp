/*
 * CombatManager.cpp
 *
 *  Created on: 24/02/2010
 *      Author: victor
 */

#include "CombatManager.h"
#include "CreatureAttackData.h"
#include "server/zone/objects/scene/variables/DeltaVector.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "templates/params/creature/CreatureState.h"
#include "server/zone/objects/creature/commands/CombatQueueCommand.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/packets/object/CombatSpam.h"
#include "server/zone/packets/object/CombatAction.h"
#include "server/zone/packets/tangible/UpdatePVPStatusMessage.h"
#include "server/zone/Zone.h"
#include "server/zone/managers/collision/CollisionManager.h"
#include "server/zone/managers/visibility/VisibilityManager.h"
#include "server/zone/managers/creature/LairObserver.h"
#include "server/zone/managers/reaction/ReactionManager.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/installation/components/TurretDataComponent.h"
#include "server/zone/objects/creature/ai/AiAgent.h"
#include "server/zone/objects/installation/InstallationObject.h"
#include "server/zone/packets/object/ShowFlyText.h"

#define COMBAT_SPAM_RANGE 85

bool CombatManager::startCombat(CreatureObject* attacker, TangibleObject* defender, bool lockDefender) {
	if (attacker == defender)
		return false;

	if (attacker->getZone() == NULL || defender->getZone() == NULL)
		return false;

	if (attacker->isRidingMount()) {
		ManagedReference<CreatureObject*> parent = attacker->getParent().get().castTo<CreatureObject*>();

		if (parent == NULL || !parent->isMount())
			return false;

		if (parent->hasBuff(STRING_HASHCODE("gallop")))
			return false;
	}

	if (attacker->hasRidingCreature())
		return false;

	if (!defender->isAttackableBy(attacker))
		return false;


	CreatureObject *creo = defender->asCreatureObject();
	if (creo != NULL && creo->isIncapacitated() && creo->isFeigningDeath() == false)
		return false;

	if (creo != NULL && creo->isBlueGlowy())
		return false;


	attacker->clearState(CreatureState::PEACE);

	Locker clocker(defender, attacker);

	attacker->setDefender(defender);
	defender->addDefender(attacker);

	return true;
}

bool CombatManager::attemptPeace(CreatureObject* attacker) {
	DeltaVector<ManagedReference<SceneObject*> >* defenderList = attacker->getDefenderList();

	for (int i = defenderList->size() - 1; i >= 0; --i) {
		try {
			ManagedReference<SceneObject*> object = defenderList->get(i);

			TangibleObject* defender = cast<TangibleObject*>( object.get());

			if (defender == NULL)
				continue;

			try {
				Locker clocker(defender, attacker);

				if (defender->hasDefender(attacker)) {


					if (defender->isCreatureObject()) {
						CreatureObject* creature = defender->asCreatureObject();

						if (creature->getMainDefender() != attacker || creature->hasState(CreatureState::PEACE) || creature->isDead() || attacker->isDead() || !creature->isInRange(attacker, 128.f)) {
							attacker->removeDefender(defender);
							defender->removeDefender(attacker);
						}
					} else {
						attacker->removeDefender(defender);
						defender->removeDefender(attacker);
					}
				} else {
					attacker->removeDefender(defender);

				}

				clocker.release();

			} catch (Exception& e) {
				error(e.getMessage());
				e.printStackTrace();
			}
		} catch (ArrayIndexOutOfBoundsException& exc) {

		}
	}

	if (defenderList->size() != 0) {
		//info("defenderList not empty, trying to set Peace State");

		attacker->setState(CreatureState::PEACE);

		return false;
	} else {
		attacker->clearCombatState(false);

		// clearCombatState() (rightfully) does not automatically set peace, so set it
		attacker->setState(CreatureState::PEACE);

		return true;
	}
}

void CombatManager::forcePeace(CreatureObject* attacker) {
	assert(attacker->isLockedByCurrentThread());

	DeltaVector<ManagedReference<SceneObject*> >* defenderList = attacker->getDefenderList();

	for (int i = 0; i < defenderList->size(); ++i) {
		ManagedReference<SceneObject*> object = defenderList->getSafe(i);

		if (object == NULL || !object->isTangibleObject())
			continue;

		TangibleObject* defender = cast<TangibleObject*>( object.get());

		Locker clocker(defender, attacker);

		if (defender->hasDefender(attacker)) {
			attacker->removeDefender(defender);
			defender->removeDefender(attacker);
		} else {
			attacker->removeDefender(defender);
		}

		--i;

		clocker.release();
	}

	attacker->clearCombatState(false);
	attacker->setState(CreatureState::PEACE);
}

int CombatManager::doCombatAction(CreatureObject* attacker, WeaponObject* weapon, TangibleObject* defenderObject, const CreatureAttackData& data) {
	//info("entering doCombat action with data ", true);

	if(data.getCommand() == NULL)
		return -3;

	if (!startCombat(attacker, defenderObject))
		return -1;

	//info("past start combat", true);

	if (attacker->hasAttackDelay() || !attacker->checkPostureChangeDelay())
		return -3;

	//info("past delay", true);

	if (!applySpecialAttackCost(attacker, weapon, data))
		return -2;

	//info("past special attack cost", true);

	int damage = 0;
	bool shouldGcwTef = false, shouldBhTef = false;
	damage = doTargetCombatAction(attacker, weapon, defenderObject, data, &shouldGcwTef, &shouldBhTef);

	if (data.getCommand()->isAreaAction() || data.getCommand()->isConeAction()) {

		Reference<SortedVector<ManagedReference<TangibleObject*> >* > areaDefenders = getAreaTargets(attacker, weapon, defenderObject, data);

		for(int i=0; i<areaDefenders->size(); i++) {
			damage += doTargetCombatAction(attacker, weapon, areaDefenders->get(i), data, &shouldGcwTef, &shouldBhTef);
		}
	}

	if (damage > 0) {
		attacker->updateLastSuccessfulCombatAction();

		if (attacker->isPlayerCreature() && data.getCommandCRC() != STRING_HASHCODE("attack"))
			weapon->decay(attacker);

		// This method can be called multiple times for area attacks. Let the calling method decrease the powerup once
		if (!data.isForceAttack())
			weapon->decreasePowerupUses(attacker);
	}

	// Update PvP TEF Duration
	if (shouldGcwTef || shouldBhTef) {
		ManagedReference<CreatureObject*> attackingCreature = attacker->isPet() ? attacker->getLinkedCreature() : attacker;

		if (attackingCreature != NULL) {
			PlayerObject* ghost = attackingCreature->getPlayerObject();

			if (ghost != NULL) {
				Locker olocker(attackingCreature, attacker);
				ghost->updateLastPvpCombatActionTimestamp(shouldGcwTef, shouldBhTef);
			}
		}
}

	return damage;
}

int CombatManager::doCombatAction(TangibleObject* attacker, WeaponObject* weapon, TangibleObject* defender, CombatQueueCommand* command) {
	if(command == NULL)
		return -3;

	const CreatureAttackData data = CreatureAttackData("", command, defender->getObjectID());
	int damage = 0;

	if(weapon != NULL){
		damage = doTargetCombatAction(attacker, weapon, defender, data);
		if (data.getCommand()->isAreaAction() || data.getCommand()->isConeAction()) {
			Reference<SortedVector<ManagedReference<TangibleObject*> >* > areaDefenders = getAreaTargets(attacker, weapon, defender, data);
			for(int i=0; i<areaDefenders->size(); i++) {
				damage += doTargetCombatAction(attacker, weapon, areaDefenders->get(i), data);
			}
		}
	}

	return damage;
}

int CombatManager::doTargetCombatAction(CreatureObject* attacker, WeaponObject* weapon, TangibleObject* tano, const CreatureAttackData& data, bool* shouldGcwTef, bool* shouldBhTef) {
	int damage = 0;

	Locker clocker(tano, attacker);

	if (!tano->isAttackableBy(attacker))
		return 0;

	attacker->addDefender(tano);
	tano->addDefender(attacker);

	if (tano->isCreatureObject()) {
		CreatureObject* defender = tano->asCreatureObject();

		if (defender->getWeapon() == NULL)
			return 0;

		damage = doTargetCombatAction(attacker, weapon, defender, data, shouldGcwTef, shouldBhTef);
	} else {
		int poolsToDamage = calculatePoolsToDamage(data.getPoolsToDamage());

		damage = applyDamage(attacker, weapon, tano, poolsToDamage, data);

		broadcastCombatAction(attacker, tano, weapon, data, damage, 0x01, 0);

		data.getCommand()->sendAttackCombatSpam(attacker, tano, HIT, damage, data);

	}

	if (damage > 0 && tano->isAiAgent()) {
		AiAgent* aiAgent = cast<AiAgent*>(tano);
		bool help = false;

		for (int i = 0; i < 9; i += 3) {
			if (aiAgent->getHAM(i) < (aiAgent->getMaxHAM(i) / 2)) {
				help = true;
				break;
			}
		}

		if (help)
			aiAgent->sendReactionChat(ReactionManager::HELP);
		else
			aiAgent->sendReactionChat(ReactionManager::HIT);
	}

	if (damage > 0 && attacker->isAiAgent()) {
		AiAgent* aiAgent = cast<AiAgent*>(attacker);
		aiAgent->sendReactionChat(ReactionManager::HITTARGET);
	}

	return damage;
}

int CombatManager::doTargetCombatAction(CreatureObject* attacker, WeaponObject* weapon, CreatureObject* defender, const CreatureAttackData& data, bool* shouldGcwTef, bool* shouldBhTef) {
	if (defender->isEntertaining())
		defender->stopEntertaining();

	int hitVal = HIT;
	uint8 hitLocation = 0;

	float damageMultiplier = data.getDamageMultiplier();
	int accBonusCommand = data.getAccuracyBonus();

	// need to calculate damage here to get proper client spam
	int damage = 0;

	/*float rankDamage = (attacker->getSkillMod("rank_offence") / 2); //TODO Add Me
	float bhDamage = (attacker->getSkillMod("bhrank_offence") / 2);
	float gcwDamage = (attacker->getSkillMod("gcw_offence") / 2);

	if (attacker->getSkillMod("rank_offence") > 0)
		rankDamage /= 100.f;
		damageMultiplier += rankDamage;

	if (attacker->getSkillMod("bhrank_offence") > 0)
		bhDamage /= 100.f;
		damageMultiplier += bhDamage;

	if (attacker->getSkillMod("gcw_offence") > 0)
		gcwDamage /= 100.f;
		damageMultiplier += gcwDamage;
		*/

	if(attacker->hasSkill("jedi_natural_novice")){
		for (int i = 0; i < attacker->getSlottedObjectsSize(); ++i) {
			int armorskill = attacker->getSkillMod("jediArmor");
			int reduction = (armorskill / 10);
			SceneObject* item = attacker->getSlottedObject(i);
			if (item != NULL && item->isArmorObject() && attacker->getSkillMod("jediArmor") < 6)
					damageMultiplier = damageMultiplier * reduction;
				if(damageMultiplier < 1)
					damageMultiplier = 1 + reduction;

		}
	}

	//info("DamageMulti: " + String::valueOf(damageMultiplier), true);
	//info("Ability Accuracy Bonus: " + String::valueOf(accBonusCommand), true);

	if (damageMultiplier != 0 && hitVal == HIT) {
		damage = calculateDamage(attacker, weapon, defender, data) * damageMultiplier;
	}

	if (!data.isStateOnlyAttack()) {
		hitVal = getHitChance(attacker, defender, weapon, damage, data.getAccuracyBonus() + attacker->getSkillMod(data.getCommand()->getAccuracySkillMod()), data.isForceAttack());

		if(hitVal == HIT){
			damage = calculateCrit(attacker, defender, weapon, damage, data);
		} //TODO Old place moved for proper effects
		//Send Attack Combat Spam. For state-only attacks, this is sent in applyStates().
		data.getCommand()->sendAttackCombatSpam(attacker, defender, hitVal, damage, data);
	}

	switch (hitVal) {
	case MISS:
		doMiss(attacker, weapon, defender, damage);
		broadcastCombatAction(attacker, defender, weapon, data, 0, hitVal, 0);
		checkForTefs(attacker, defender, shouldGcwTef, shouldBhTef);
		return 0;
		break;
	case BLOCK:
		doBlock(attacker, weapon, defender, damage);
		damageMultiplier = 0.0f;
		break;
	case DODGE:
		doDodge(attacker, weapon, defender, damage);
		damageMultiplier = 0.0f;
		break;
	case COUNTER: {
		doCounterAttack(attacker, weapon, defender, damage);
		damageMultiplier = 0.0f;
		break;}
	case RICOCHET:
		doLightsaberBlock(attacker, weapon, defender, damage);
		damageMultiplier = 0.0f;
		break;
	default:
		break;
	}
	// Apply states first
	applyStates(attacker, defender, data);

	//if it's a state only attack (intimidate, warcry, wookiee roar) don't apply dots or break combat delays
	if (!data.isStateOnlyAttack()) {
		if (defender->hasAttackDelay())
			defender->removeAttackDelay();

		if (damageMultiplier != 0 && damage != 0) {
			int poolsToDamage = calculatePoolsToDamage(data.getPoolsToDamage()); // TODO: animations are probably determined by which pools are damaged (high, mid, low, combos, etc)
			int unmitDamage = damage;
			damage = applyDamage(attacker, weapon, defender, damage, damageMultiplier, poolsToDamage, hitLocation, data);

			applyDots(attacker, defender, data, damage, unmitDamage, poolsToDamage);
			applyWeaponDots(attacker, defender, weapon);

		}
	}

	broadcastCombatAction(attacker, defender, weapon, data, damage, hitVal, hitLocation);

	checkForTefs(attacker, defender, shouldGcwTef, shouldBhTef);

	return damage;
}

int CombatManager::calculateCrit(CreatureObject* attacker, CreatureObject* defender, WeaponObject* weapon, int damage, const CreatureAttackData& data) {

	int baseCritChance = 5 + System::random(5);
	int critChance = attacker->getSkillMod("private_critical_chance");
	int exposure = defender->getSkillMod("private_strike_exposure");
	int skillBonus = data.getCritChance();
	int weaponCritBonus =  weapon->getCriticalSlicedValue();

	int critRoll = baseCritChance + critChance + exposure + skillBonus + weaponCritBonus;

	int critDefense = defender->getSkillMod("private_critical_defense");

	critRoll -= critDefense;

	if(critRoll <= 0){
		critRoll = 0;
	}

	if(System::random(100) < critRoll ){
		float critValue = 1.20 + (attacker->getSkillMod("private_critical_value") / 100.f);
		damage *= critValue;
		CombatSpam* spam = new CombatSpam(attacker,"Critical!", 10);
		defender->showFlyText("combat_effects", "critical", 0xFF, 0xFF, 0xFF);
		attacker->sendMessage(spam);
	}

	return damage;

}

int CombatManager::doTargetCombatAction(TangibleObject* attacker, WeaponObject* weapon, TangibleObject* tano, const CreatureAttackData& data) {

	int damage = 0;

	Locker clocker(tano, attacker);

	if (tano->isCreatureObject()) {
		CreatureObject* defenderObject = tano->asCreatureObject();
		if (defenderObject->getWeapon() != NULL)
			damage = doTargetCombatAction(attacker, weapon, defenderObject, data);
	} else {
		damage = 1;// TODO: implement, tano->tano damage
	}
	return damage;
}

int CombatManager::doTargetCombatAction(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defenderObject, const CreatureAttackData& data) {
	if(defenderObject == NULL || !defenderObject->isAttackableBy(attacker))
		return 0;

	if (defenderObject->isEntertaining())
		defenderObject->stopEntertaining();

	attacker->addDefender(defenderObject);
	defenderObject->addDefender(attacker);

	float damageMultiplier = data.getDamageMultiplier();

	// need to calculate damage here to get proper client spam
	int damage = 0;

	if (damageMultiplier != 0)
		damage = calculateDamage(attacker, weapon, defenderObject, data) * damageMultiplier;

	damageMultiplier = 1.0f;
	int hitVal = getHitChance(attacker, defenderObject, weapon, damage, data.getAccuracyBonus(), data.isForceAttack());
	uint8 hitLocation = 0;

	//Send Attack Combat Spam
	data.getCommand()->sendAttackCombatSpam(attacker, defenderObject, hitVal, damage, data);
	CombatAction* combatAction = NULL;

	switch (hitVal) {
	case MISS:
		doMiss(attacker, weapon, defenderObject, damage);
		break;
	case HIT:
		break;
	case BLOCK:
		doBlock(attacker, weapon, defenderObject, damage);
		damageMultiplier = 0.0f;
		break;
	case DODGE:
		doDodge(attacker, weapon, defenderObject, damage);
		damageMultiplier = 0.0f;
		break;
	case COUNTER:
		doCounterAttack(attacker, weapon, defenderObject, damage);
		//if (!defenderObject->hasState(CreatureState::PEACE))
		//	defenderObject->executeObjectControllerAction(STRING_HASHCODE("attack"), attacker->getObjectID(), "");
		damageMultiplier = 0.0f;
		break;
	case RICOCHET:
		doLightsaberBlock(attacker, weapon, defenderObject, damage);
		damageMultiplier = 0.0f;
		break;
	default:
		break;
	}
	if(hitVal != MISS) {
		int poolsToDamage = calculatePoolsToDamage(data.getPoolsToDamage());

		if (poolsToDamage == 0)
			return 0;

		if (defenderObject->hasAttackDelay())
			defenderObject->removeAttackDelay();

		if (damageMultiplier != 0 && damage != 0)
			damage = applyDamage(attacker, weapon, defenderObject, damage, damageMultiplier, poolsToDamage, hitLocation, data);
	} else {
		damage = 0;
	}
	defenderObject->updatePostures(false);
	uint32 animationCRC = data.getCommand()->getAnimation(attacker, defenderObject, weapon, hitLocation, damage).hashCode();
	//TODO Changed from Pub 8+.. redo someday
	//uint32 animationCRC = data.getAnimationCRC();

	//if (animationCRC == 0)
	//	animationCRC = getDefaultAttackAnimation(attacker, weapon, hitLocation, damage);

	combatAction = new CombatAction(attacker, defenderObject, animationCRC, hitVal, CombatManager::DEFAULTTRAIL);
	attacker->broadcastMessage(combatAction,true);

	return damage;
}

void CombatManager::applyDots(CreatureObject* attacker, CreatureObject* defender, const CreatureAttackData& data, int appliedDamage, int unmitDamage, int poolsToDamage) {
	Vector<DotEffect>* dotEffects = data.getDotEffects();

	if (defender->isPlayerCreature() && defender->getPvpStatusBitmask() == CreatureFlag::NONE)
		return;

	for (int i = 0; i < dotEffects->size(); i++) {
		const DotEffect& effect = dotEffects->get(i);

		if (defender->hasDotImmunity(effect.getDotType()) || effect.getDotDuration() == 0 || System::random(100) > effect.getDotChance())
			continue;

		const Vector<String>& defenseMods = effect.getDefenderStateDefenseModifers();
		int resist = 0;

		for (int j = 0; j < defenseMods.size(); j++)
			resist += defender->getSkillMod(defenseMods.get(j));

		int damageToApply = appliedDamage;
		uint32 dotType = effect.getDotType();

		if (effect.isDotDamageofHit()) {
			// determine if we should use unmitigated damage
			if (dotType != CreatureState::BLEEDING)
				damageToApply = unmitDamage;
		}

		int potency = effect.getDotPotency();

		if (potency == 0) {
			potency = 150;
		}

		uint8 pool = effect.getDotPool();

		if (pool == CreatureAttribute::UNKNOWN) {
			pool = getPoolForDot(dotType, poolsToDamage);
		}

		//info("entering addDotState with CRC:" + String::valueOf(dotCRC), true);


		defender->addDotState(attacker, effect.getDotType(), data.getCommand()->getNameCRC(), effect.getDotStrength(), effect.getDotPool(), effect.getDotDuration(), 150, resist, effect.getSecondaryPercent());
	}
}

void CombatManager::applyWeaponDots(CreatureObject* attacker, CreatureObject* defender, WeaponObject* weapon) {
	if (defender->getPvpStatusBitmask() == CreatureFlag::NONE)
		return;


	int resist = defender->getSkillMod("combat_bleeding_defense");

	for (int i = 0; i < weapon->getNumberOfDots(); i++) {
		if (weapon->getDotUses(i) <= 0)
			continue;

		int type = 0;

		switch (weapon->getDotType(i)) {
		case 1: //POISON
			type = CreatureState::POISONED;
			break;
		case 2: //DISEASE
			type = CreatureState::DISEASED;
			break;
		case 3: //FIRE
			type = CreatureState::ONFIRE;
			break;
		case 4: //BLEED
			type = CreatureState::BLEEDING;
			break;
		default:
			break;
		}

		if (defender->hasDotImmunity(type))
			continue;



		if (weapon->getDotPotency(i)*(1.f-resist/100.f) > System::random(100) && defender->addDotState(attacker, type, weapon->getObjectID(), weapon->getDotStrength(i), weapon->getDotAttribute(i), weapon->getDotDuration(i), -1, 0, weapon->getDotStrength(i)) > 0) // Unresisted, reduce use count.
			if (weapon->getDotUses(i) > 0) weapon->setDotUses(weapon->getDotUses(i) - 1, i);
	}
}

uint8 CombatManager::getPoolForDot(uint64 dotType, int poolsToDamage) {
	uint8 pool = 0;

	/*switch (dotType) {
	case CreatureState::POISONED:
	case CreatureState::ONFIRE:
	case CreatureState::BLEEDING:
		if (poolsToDamage & HEALTH) {
			pool = CreatureAttribute::HEALTH;
		} else if (poolsToDamage & ACTION) {
			pool = CreatureAttribute::ACTION;
		} else if (poolsToDamage & MIND) {
			pool = CreatureAttribute::MIND;
		}
		break;
	case CreatureState::DISEASED:
		if (poolsToDamage & HEALTH) {
			pool = CreatureAttribute::HEALTH + System::random(2);
		} else if (poolsToDamage & ACTION) {
			pool = CreatureAttribute::ACTION + System::random(2);
		} else if (poolsToDamage & MIND) {
			pool = CreatureAttribute::MIND + System::random(2);
		}
		break;
	default:
		break;
	}*/

	return HEALTH;
}

int CombatManager::calculatePostureModifier(CreatureObject* creature, WeaponObject* weapon) {
	CreaturePosture* postureLookup = CreaturePosture::instance();

	uint8 locomotion = creature->getLocomotion();

	if (!weapon->isMeleeWeapon())
		return postureLookup->getRangedAttackMod(locomotion);
	else
		return postureLookup->getMeleeAttackMod(locomotion);
}

int CombatManager::calculateTargetPostureModifier(WeaponObject* weapon, CreatureObject* targetCreature) {
	CreaturePosture* postureLookup = CreaturePosture::instance();

	uint8 locomotion = targetCreature->getLocomotion();

	if (!weapon->isMeleeWeapon())
		return postureLookup->getRangedDefenseMod(locomotion);
	else
		return postureLookup->getMeleeDefenseMod(locomotion);
}

int CombatManager::getAttackerAccuracyBonus(CreatureObject* attacker, WeaponObject* weapon) {
	int bonus = 0;

	//bonus += attacker->getSkillMod("private_attack_accuracy");
	//bonus += attacker->getSkillMod("private_accuracy_bonus");

	if (weapon->getAttackType() == SharedWeaponObjectTemplate::MELEEATTACK)
		bonus += attacker->getSkillMod("private_melee_accuracy_bonus");
	if (weapon->getAttackType() == SharedWeaponObjectTemplate::RANGEDATTACK)
		bonus += attacker->getSkillMod("private_ranged_accuracy_bonus");

	return bonus;
}

int CombatManager::getDefenderDefenseModifier(CreatureObject* defender, WeaponObject* weapon, TangibleObject* attacker) {
	int targetDefense = defender->isPlayerCreature() ? 0 : defender->getLevel();

	if (defender->isAiAgent()) {
		targetDefense =  cast<AiAgent*>(defender)->getDefense();
	}
	ManagedReference<WeaponObject*> defenderWeapon = defender->getWeapon();
	int buffDefense = 0;

	Vector<String>* defenseAccMods = weapon->getDefenderDefenseModifiers();

	for (int i = 0; i < defenseAccMods->size(); ++i) {
		const String& mod = defenseAccMods->get(i);
		//info("Primary Defese Mod: " + mod, true);
		targetDefense += defender->getSkillMod(mod);
		targetDefense += defender->getSkillMod("private_" + mod);
	}

	Vector<String>* secondaryDefense = defenderWeapon->getDefenderSecondaryDefenseModifiers();

	if (!defenderWeapon->isJediWeapon()){
		for (int i = 0; i < secondaryDefense->size(); ++i) {
			const String& mod = secondaryDefense->get(i);
			//info("Secondary Mod: " + mod, true);
			targetDefense += defender->getSkillMod(mod);
			targetDefense += defender->getSkillMod("private_" + mod);
		}
	}

	if (attacker->isPlayerCreature())
		targetDefense += defender->getSkillMod("private_defense");

	// SL bonuses go on top of hardcap
	for (int i = 0; i < defenseAccMods->size(); ++i) {
		const String& mod = defenseAccMods->get(i);
		targetDefense += defender->getSkillMod("private_group_" + mod);
	}

	// food bonus goes on top as well

	targetDefense += defender->getSkillMod("dodge_attack");
	targetDefense += defender->getSkillMod("private_dodge_attack");
	targetDefense += defender->getSkillMod("private_defense_bonus");

	// defense hardcap
	if (targetDefense > 200)
		targetDefense = 200.f + ((targetDefense - 200)  * .25);

	targetDefense -= defender->getSkillMod("private_defense_penalty");


	//info("Target defense after state affects and cap is " +  String::valueOf(targetDefense), true);

	return targetDefense;
}

float CombatManager::getDefenderToughnessModifier(CreatureObject* defender, int attackType, int damType, float damage) {
	ManagedReference<WeaponObject*> weapon = defender->getWeapon();

	Vector<String>* defenseToughMods = weapon->getDefenderToughnessModifiers();
	//int forceToughness = defender->getSkillMod("force_toughness");

	int gcwRank = defender->getSkillMod("gcw_defense");
	int bhRank = defender->getSkillMod("bhrank_defense");

	int damageShield = defender->getSkillMod("private_melee_dmg_shield");
	int duelistStance = defender->getSkillMod("private_damage_shield");

	if(damageShield > 0 && weapon->isMeleeWeapon())
		damage *= 1.f - (damageShield / 100.f);

	if(duelistStance > 0 && weapon->isRangedWeapon())
		damage *= 1.f - (duelistStance / 100.f);
	/*int forceRank = defender->getSkillMod("rank_defense");

	if(forceRank > 0)
		forceRank /= 10.f;

	if (attackType == weapon->getAttackType()) {
		for (int i = 0; i < defenseToughMods->size(); ++i) {
			int toughMod = defender->getSkillMod(defenseToughMods->get(i));
			if (forceToughness > toughMod) toughMod = forceToughness;

			if (toughMod > 40)
				toughMod = 40.f + ((toughMod - 40)  * .5);

			//toughMod += gcwRank;
			//toughMod += bhRank;
			toughMod += forceRank;

			if (toughMod > 0) damage *= 1.f - (toughMod / 100.f);
		}
	}*/

	if (bhRank > 0)
		damage *= 1.f - (bhRank / 100.f);

	if (gcwRank > 0)
		damage *= 1.f - (gcwRank / 100.f);

	/*int jediToughness = defender->getSkillMod("jedi_toughness");
	if (damType != WeaponObject::LIGHTSABER && jediToughness > 0)
		damage *= 1.f - (jediToughness / 200.f);



	int focusBonus = defender->getSkillMod("focus_value");
	if (focusBonus > 0)
		damage *= 1.f -(focusBonus / 200.f);*/

	//int foodBonus = defender->getSkillMod("mitigate_damage");
	//if (foodBonus > 0)
	//	damage *= 1.f - (foodBonus / 200.f);

	return damage < 0 ? 0 : damage;
}

float CombatManager::applyDamageModifiers(CreatureObject* attacker, WeaponObject* weapon, float damage, const CreatureAttackData& data) {
	if (!data.isForceAttack()) {
		Vector<String>* weaponDamageMods = weapon->getDamageModifiers();

		for (int i = 0; i < weaponDamageMods->size(); ++i) {
			damage += attacker->getSkillMod(weaponDamageMods->get(i));
		}

		if (weapon->getAttackType() == SharedWeaponObjectTemplate::MELEEATTACK)
			damage += attacker->getSkillMod("private_melee_damage_bonus");
		if (weapon->getAttackType() == SharedWeaponObjectTemplate::RANGEDATTACK)
			damage += attacker->getSkillMod("private_ranged_damage_bonus");
	}

	damage += attacker->getSkillMod("private_damage_bonus");

	float damageMultiplier = 1.0 + (attacker->getSkillMod("private_damage_multiplier") / 100.f);

	if (damageMultiplier != 0)
		damage *= damageMultiplier;

	int damageDivisor = attacker->getSkillMod("private_damage_divisor");

	if (damageDivisor != 0)
		damage /= damageDivisor;

	return damage;
}

int CombatManager::getSpeedModifier(CreatureObject* attacker, WeaponObject* weapon) {
	int speedMods = 0;

	Vector<String>* weaponSpeedMods = weapon->getSpeedModifiers();

	for (int i = 0; i < weaponSpeedMods->size(); ++i) {
		speedMods += attacker->getSkillMod(weaponSpeedMods->get(i));
	}

	speedMods += attacker->getSkillMod("private_speed_bonus");
	speedMods -= attacker->getSkillMod("combat_slow");

	if (weapon->getAttackType() == SharedWeaponObjectTemplate::MELEEATTACK) {
		speedMods += attacker->getSkillMod("private_melee_speed_bonus");
		speedMods += attacker->getSkillMod("force_melee_speed");
		speedMods += attacker->getSkillMod("melee_speed");
	} else if (weapon->getAttackType() == SharedWeaponObjectTemplate::RANGEDATTACK) {
		speedMods += attacker->getSkillMod("private_ranged_speed_bonus");
		speedMods += attacker->getSkillMod("force_ranged_speed");
		speedMods += attacker->getSkillMod("ranged_speed");
	}

	return speedMods;
}

int CombatManager::getArmorObjectReduction(ArmorObject* armor, int damageType) {
	float resist = 0;

	switch (damageType) {
	case SharedWeaponObjectTemplate::KINETIC:
		resist = armor->getKinetic();
		break;
	case SharedWeaponObjectTemplate::ENERGY:
		resist = armor->getEnergy();
		break;
	case SharedWeaponObjectTemplate::ELECTRICITY:
		resist = armor->getElectricity();
		break;
	case SharedWeaponObjectTemplate::STUN:
		resist = armor->getStun();
		break;
	case SharedWeaponObjectTemplate::BLAST:
		resist = armor->getBlast();
		break;
	case SharedWeaponObjectTemplate::HEAT:
		resist = armor->getHeat();
		break;
	case SharedWeaponObjectTemplate::COLD:
		resist = armor->getCold();
		break;
	case SharedWeaponObjectTemplate::ACID:
		resist = armor->getAcid();
		break;
	case SharedWeaponObjectTemplate::LIGHTSABER:
		resist = armor->getLightSaber();
		break;
	}

	return Math::max(0, (int)resist);

}

int CombatManager::getArmorNpcReduction(AiAgent* defender, int damageType) {
	float resist = 0;

	switch (damageType) {
	case SharedWeaponObjectTemplate::KINETIC:
		resist = defender->getKinetic();
		break;
	case SharedWeaponObjectTemplate::ENERGY:
		resist = defender->getEnergy();
		break;
	case SharedWeaponObjectTemplate::ELECTRICITY:
		resist = defender->getElectricity();
		break;
	case SharedWeaponObjectTemplate::HEAT:
		resist = defender->getHeat();
		break;
	case SharedWeaponObjectTemplate::COLD:
		resist = defender->getCold();
		break;
	case SharedWeaponObjectTemplate::ACID:
		resist = defender->getAcid();
		break;
	}

	return Math::max(0, (int)resist);

}

ArmorObject* CombatManager::getArmorObject(CreatureObject* defender, uint8 hitLocation) {

	Vector<ManagedReference<ArmorObject*> > armor = defender->getWearablesDeltaVector()->getArmorAtHitLocation(hitLocation);

	if(armor.isEmpty())
		return NULL;

	return armor.get(System::random(armor.size()-1));

}

ArmorObject* CombatManager::getPSGArmor(CreatureObject* defender) {
	SceneObject* psg = defender->getSlottedObject("utility_belt");

	if (psg != NULL && psg->isPsgArmorObject())
		return cast<ArmorObject*>(psg);

	return NULL;
}

int CombatManager::getArmorReduction(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, float damage, int hitLocation, const CreatureAttackData& data) {

	int damageType = 0;

	if (!data.isForceAttack()) {
		damageType = weapon->getDamageType();
	} else {
		damageType = data.getDamageType();
	}

	float eleType = weapon->getElementType();
	float eleDamage = (weapon->getElementalDamage() / 1000.f);
	float eleDamageApplication = damage * eleDamage;
	//float eleDamageApplication = damage + eleSetup;

	//info("eleType: " + String::valueOf(eleType), true);
	//info("Elemenetal Damage: " + String::valueOf(eleDamageApplication), true);


	if (defender->isAiAgent()) {
		damage += eleDamageApplication;

		float armorReduction = getArmorNpcReduction(cast<AiAgent*>(defender), damageType);

		float armorBreak = defender->getSkillMod("armor_break");

		if (armorBreak > 0){
			armorReduction = armorReduction - (armorReduction * (armorBreak / 530));
		}

		float protection = (60 * (armorReduction / 10000.f) + ((10000 - armorReduction) / 10000) * (armorReduction / 200));

		if (protection > 0) {
			damage *= (1.f - (protection / 100.f));
		}

		//info("NPC Armor Reduction: " + String::valueOf(protection), true);

		return damage;
	}

	if (!data.isForceAttack()) {
		// Force Armor
		float rawDamage = damage;

		int bhShields = defender->getSkillMod("shield_armor");

		if (bhShields > 0){
			float dmgAbsorbedBH = rawDamage - (damage *= 1.f - (bhShields / 100.f));
			defender->notifyObservers(ObserverEventType::SHIELDSHIT, attacker, dmgAbsorbedBH);
			sendMitigationCombatSpam(defender, NULL, (int)dmgAbsorbedBH, PSG);
		}
	} else {

		float jediBuffDamage = 0;
		float rawDamage = damage + eleDamageApplication;

		// Force Shield
		int forceShield = defender->getSkillMod("force_shield");

		if (forceShield > 0) {
			jediBuffDamage = rawDamage - (damage *= 1.f - (forceShield / 100.f));
			defender->notifyObservers(ObserverEventType::FORCEBUFFHITSHIELD, attacker, jediBuffDamage);
			sendMitigationCombatSpam(defender, NULL, (int)jediBuffDamage, FORCESHIELD);
		}

		// Force Feedback
		int forceFeedback = defender->getSkillMod("force_feedback");
		if (forceFeedback > 0 && (defender->hasBuff(BuffCRC::JEDI_FORCE_FEEDBACK_1) || defender->hasBuff(BuffCRC::JEDI_FORCE_FEEDBACK_2))) {
			float feedbackDmg = rawDamage * (forceFeedback / 100.f);

			attacker->inflictDamage(defender, CreatureAttribute::HEALTH, feedbackDmg, true, true, true);
			broadcastCombatSpam(defender, attacker, NULL, feedbackDmg, "cbt_spam", "forcefeedback_hit", 1);
			defender->playEffect("clienteffect/pl_force_feedback_block.cef", "");
		}

		// Force Absorb
		if (defender->getSkillMod("force_absorb") > 0 && defender->isPlayerCreature()) {
			defender->notifyObservers(ObserverEventType::FORCEABSORB, attacker, data.getForceCost());
		}

		defender->notifyObservers(ObserverEventType::FORCEBUFFHIT, attacker, jediBuffDamage);

	}
	// PSG
	ManagedReference<ArmorObject*> psg = getPSGArmor(defender);

	if (psg != NULL && !psg->isVulnerable(damageType)) {
		float armorReduction =  getArmorObjectReduction(psg, damageType);
		float dmgAbsorbed = damage;

        if (armorReduction > 0) damage *= 1.f - (armorReduction / 100.f);

		dmgAbsorbed -= damage;
		if (dmgAbsorbed > 0)
			sendMitigationCombatSpam(defender, psg, (int)dmgAbsorbed, PSG);

		// inflict condition damage
		// TODO: this formula makes PSG's take more damage than regular armor, but that's how it was on live
		// it can be fixed by doing condition damage after all damage reductions

		Locker plocker(psg);

		psg->inflictDamage(psg, 0, damage * 0.1, true, true);

	}

	/*if(attacker->isCreatureObject()){
		int strikethroughChance = attacker->asCreatureObject()->getSkillMod("private_strikethrough_chance");
		int skillBonus = data.getStrikeThroughChance();
		strikethroughChance += 4;
		strikethroughChance += skillBonus;

		if(System::random(100) < strikethroughChance ){
			float strikeThroughValue = 1.15;
			damage *= strikeThroughValue;
			CombatSpam* spam = new CombatSpam(attacker->asCreatureObject(),"Strike Through!", 10);
			defender->showFlyText("combat_effects", "bypass", 0xFF, 0xFF, 0xFF);
			attacker->asCreatureObject()->sendMessage(spam);

		}

	} */

	// Standard Armor
	ManagedReference<ArmorObject*> armor = NULL;

	armor = getArmorObject(defender, hitLocation);

	if(armor == NULL && (defender->hasSkill("combat_unarmed_novice") || defender->hasSkill("force_title_jedi_rank_02"))) {
		float dmgAbsorbed = damage + eleDamageApplication;
		float armorReduction = 0;
		float tkArmor = (defender->getSkillMod("tka_armor") * 100);
		float jediArmor = (defender->getSkillMod("jedi_armor") * 100);
		float forceArmor = (defender->getSkillMod("force_armor") * 100);
		float rankDefense = (defender->getSkillMod("rank_defense") * 2);
		float bhDefense = (defender->getSkillMod("bhrank_defense") * 2);
		float armorBreak = defender->getSkillMod("armor_break");
		float eleAbsorbed = eleDamageApplication;

		if(tkArmor > jediArmor){
			armorReduction = tkArmor;
		}else{
			armorReduction = jediArmor + forceArmor;
		}

		if (rankDefense > 0 && forceArmor == 0)
			armorReduction += rankDefense;

		if (bhDefense > 0)
			armorReduction += bhDefense;

		if (armorBreak > 0){
			armorReduction = armorReduction - (armorReduction * (armorBreak / 530));
		}

		float protection = (60 * (armorReduction / 10000.f) + ((10000 - armorReduction) / 10000) * (armorReduction / 200));
		float eleProtection = (60 * (armorReduction / 10000.f) + ((10000 - armorReduction) / 10000) * (armorReduction / 200));
		//info("TKA Armor: " + String::valueOf(protection), true);
		//info("TKA Elemental: " + String::valueOf(eleProtection), true);

		if (protection > 0) {
			damage *= (1.f - (protection / 100.f));
			dmgAbsorbed -= damage;
			if (eleProtection > 0 && eleDamageApplication > 0){
				eleDamageApplication *= (1.f - (eleProtection / 100.f));
				dmgAbsorbed -= eleDamageApplication;
			}
			sendMitigationCombatSpam(defender, NULL, (int)dmgAbsorbed, FORCEARMOR);
			if(forceArmor > 0){
				defender->notifyObservers(ObserverEventType::FORCEBUFFHITARMOR, attacker, dmgAbsorbed);
				//sendMitigationCombatSpam(defender, NULL, (int)dmgAbsorbed, FORCEARMOR);
			}
		}

		} else if (armor != NULL && !armor->isVulnerable(damageType)) {
			float armorReduction = getArmorObjectReduction(armor, damageType);
			float eleReduction = 0;// = getArmorObjectReduction(armor, eleType);
			float dmgAbsorbed = damage;
			float eleAbsorbed = eleDamageApplication;
			float armorBreak = defender->getSkillMod("armor_break");

			if(eleType != 0){
				eleReduction = getArmorObjectReduction(armor, eleType);
			}


			if (armorBreak > 0){
				armorReduction = armorReduction - (armorReduction * (armorBreak / 530));
			}

			float protection = (60 * (armorReduction / 10000.f) + ((10000 - armorReduction) / 10000) * (armorReduction / 200));
			float eleProtection = (60 * (eleReduction / 10000.f) + ((10000 - eleReduction) / 10000) * (eleReduction / 200));

			//info("Normal Armor: " + String::valueOf(armorReduction), true);
			//info("eleProtection: " + String::valueOf(eleProtection), true);

			if (protection > 0) {
				damage *= (1.f - (protection / 100.f));
				if (eleProtection > 0 && eleDamageApplication > 0){
					eleDamageApplication *= (1.f - (eleProtection / 100.f));
					dmgAbsorbed -= eleDamageApplication;
				}
				dmgAbsorbed -= damage;
				sendMitigationCombatSpam(defender, armor, (int)dmgAbsorbed, ARMOR);
			}

			/*if (eleReduction > 0 && eleDamageApplication > 0) {
				eleDamageApplication *= (1.f - (eleReduction / 10000.f));
				eleAbsorbed -= eleDamageApplication;
				sendMitigationCombatSpam(defender, armor, (int)eleAbsorbed, ARMOR);
			}*/

			// inflict condition damage
			Locker alocker(armor);

			armor->inflictDamage(armor, 0, damage * 0.01, true, true);

		//}
	}

	//info("Existing Elemental Damage:" + String::valueOf(eleDamageApplication), true);
	return damage + eleDamageApplication;
}

float CombatManager::calculateDamage(CreatureObject* attacker, WeaponObject* weapon, TangibleObject* defender, const CreatureAttackData& data) {
	float damage = 0;

	int diff = 0;

	if (data.getMinDamage() > 0 || data.getMaxDamage() > 0) { // this is a special attack (force, etc)
		float pAccuracy = calculateAccuracy(attacker, weapon, data.isForceAttack());
		float forceDamage = (1.0 + (attacker->getSkillMod("force_damage") / 100.f));
		float rankDamage = (1.0 + ((attacker->getSkillMod("rank_offence") / 2) / 100.f));
		float minDamage = data.getMinDamage();
		float maxDamage = data.getMaxDamage();


		if (forceDamage > 2.0) {
			forceDamage = forceDamage - 1.0;
		}
		if (rankDamage > 2.0) {
			rankDamage = rankDamage - 1.0;
		}

		//float maxDamage = (data.getMaxDamage() * forceDamage);

		damage = minDamage + pAccuracy;
		damage *= forceDamage;
		damage *= rankDamage;

		if(damage >= maxDamage)
			damage = maxDamage * (pAccuracy / 200.f);

		damage *= rankDamage;

	} else {

		float minDamage = weapon->getMinDamage(), maxDamage = weapon->getMaxDamage();

		damage = minDamage;
		diff = maxDamage - minDamage;
	}

	if (diff > 0)
		damage += System::random(diff);

	damage = applyDamageModifiers(attacker, weapon, damage, data);

	//if (attacker->isPlayerCreature())
	//	damage *= 1.5;

	//info("damage to be dealt is " + String::valueOf(damage), true);

	ManagedReference<LairObserver*> lairObserver = NULL;
	SortedVector<ManagedReference<Observer*> > observers = defender->getObservers(ObserverEventType::OBJECTDESTRUCTION);

	for (int i = 0; i < observers.size(); i++) {
		lairObserver = cast<LairObserver*>(observers.get(i).get());
		if (lairObserver != NULL)
			break;
	}

	if (lairObserver && lairObserver->getSpawnNumber() > 2)
		damage *= 3.5;

	return damage;
}

float CombatManager::doDroidDetonation(CreatureObject* droid, CreatureObject* defender, float damage) {
	if (defender->isPlayerCreature() && defender->getPvpStatusBitmask() == CreatureFlag::NONE) {
		return 0;
	}
	if (defender->isCreatureObject()) {
		if (defender->isPlayerCreature())
			damage *= 0.25;
		// pikc a pool to target
		int pool = calculatePoolsToDamage(RANDOM);
		// we now have damage to use lets apply it
		float healthDamage = 0.f, actionDamage = 0.f, mindDamage = 0.f;
		// need to check armor reduction with just defender, blast and their AR + resists
		if(defender->isVehicleObject()) {
			int ar = cast<VehicleObject*>(defender)->getBlast();
			if ( ar > 0)
				damage *= (1.f - (ar / 100.f));
			healthDamage = damage;
			actionDamage = damage;
			mindDamage = damage;
		} else if (defender->isAiAgent()) {
			int ar = cast<AiAgent*>(defender)->getBlast();
			if ( ar > 0)
				damage *= (1.f - (ar / 100.f));
			healthDamage = damage;
			actionDamage = damage;
			mindDamage = damage;

		} else {
			// player
			static uint8 bodyHitLocations[] = {HIT_BODY, HIT_BODY, HIT_LARM, HIT_RARM};

			ArmorObject* healthArmor = getArmorObject(defender, bodyHitLocations[System::random(3)]);
			ArmorObject* mindArmor = getArmorObject(defender, HIT_HEAD);
			ArmorObject* actionArmor = getArmorObject(defender, HIT_LLEG); // This hits both the pants and feet regardless
			ArmorObject* psgArmor = getPSGArmor(defender);
			if (psgArmor != NULL && !psgArmor->isVulnerable(SharedWeaponObjectTemplate::BLAST)) {
				float armorReduction =  psgArmor->getBlast();
				if (armorReduction > 0) damage *= (1.f - (armorReduction / 100.f));

				Locker plocker(psgArmor);

				psgArmor->inflictDamage(psgArmor, 0, damage * 0.1, true, true);
			}
			// reduced by psg not check each spot for damage
			healthDamage = damage;
			actionDamage = damage;
			mindDamage = damage;
			if (healthArmor != NULL && !healthArmor->isVulnerable(SharedWeaponObjectTemplate::BLAST) && (pool & HEALTH)) {
				float armorReduction = healthArmor->getBlast();
				if (armorReduction > 0)
					healthDamage *= (1.f - (armorReduction / 100.f));

				Locker hlocker(healthArmor);

				healthArmor->inflictDamage(healthArmor, 0, healthDamage * 0.1, true, true);
				return (int)healthDamage * 0.1;
			}
			if (mindArmor != NULL && !mindArmor->isVulnerable(SharedWeaponObjectTemplate::BLAST) && (pool & MIND)) {
				float armorReduction = mindArmor->getBlast();
				if (armorReduction > 0)
					mindDamage *= (1.f - (armorReduction / 100.f));

				Locker mlocker(mindArmor);

				mindArmor->inflictDamage(mindArmor, 0, mindDamage * 0.1, true, true);
				return (int)mindDamage * 0.1;
			}
			if (actionArmor != NULL && !actionArmor->isVulnerable(SharedWeaponObjectTemplate::BLAST) && (pool & ACTION)) {
				float armorReduction = actionArmor->getBlast();
				if (armorReduction > 0)
					actionDamage *= (1.f - (armorReduction / 100.f));

				Locker alocker(actionArmor);

				actionArmor->inflictDamage(actionArmor, 0, actionDamage * 0.1, true, true);
				return (int)actionDamage * 0.1;
			}
		}
		if((pool & ACTION)){
			defender->inflictDamage(droid, CreatureAttribute::ACTION, (int)actionDamage, true, true, true);
			return (int)actionDamage;
		}
		if((pool & HEALTH)) {
			defender->inflictDamage(droid, CreatureAttribute::HEALTH, (int)healthDamage, true, true, true);
			return (int)healthDamage;
		}
		if((pool & MIND)) {
			defender->inflictDamage(droid, CreatureAttribute::MIND, (int)mindDamage, true, true, true);
			return (int)mindDamage;
		}
		return 0;
	} else {
		return 0;
	}
}

float CombatManager::calculateAccuracy(TangibleObject* attacker, WeaponObject* weapon, bool isForceAttack) {

	if (attacker->isAiAgent()) {
		return cast<AiAgent*>(attacker)->getChanceHit();
	} else if (attacker->isInstallationObject()) {
		return cast<InstallationObject*>(attacker)->getHitChance() * 100;
	}

	if (!attacker->isCreatureObject()) {
		return 0;
	}

	CreatureObject* creoAttacker = cast<CreatureObject*>(attacker);

	int attackerAccuracy = 0;

	Vector<String>* creatureAccMods = weapon->getCreatureAccuracyModifiers();

	for (int i = 0; i < creatureAccMods->size(); ++i) {
		const String& mod = creatureAccMods->get(i);
		attackerAccuracy += creoAttacker->getSkillMod(mod);
		attackerAccuracy += creoAttacker->getSkillMod("private_" + mod);
	}

	attackerAccuracy += creoAttacker->getSkillMod("attack_accuracy") + creoAttacker->getSkillMod("dead_eye");
	attackerAccuracy += creoAttacker->getSkillMod("private_accuracy_bonus");

	if (weapon->getAttackType() == SharedWeaponObjectTemplate::MELEEATTACK){
		attackerAccuracy += creoAttacker->getSkillMod("force_melee_accuracy");
		attackerAccuracy += creoAttacker->getSkillMod("melee_accuracy");
	}else if (weapon->getAttackType() == SharedWeaponObjectTemplate::RANGEDATTACK){
		attackerAccuracy += creoAttacker->getSkillMod("force_ranged_accuracy");
		attackerAccuracy += creoAttacker->getSkillMod("ranged_accuracy");
	}

	if (isForceAttack)
		attackerAccuracy = creoAttacker->getSkillMod("ranged_accuracy");
		attackerAccuracy += creoAttacker->getSkillMod("force_accuracy");

	float rankDamage = (1.0 + ((creoAttacker->getSkillMod("rank_offence") / 2) / 100.f));
	float bhDamage = (1.0 + ((creoAttacker->getSkillMod("bhrank_offence") / 2) / 100.f));
	float gcwDamage = (1.0 + ((creoAttacker->getSkillMod("gcw_offence") / 2) / 100.f));

	attackerAccuracy *= rankDamage;
	attackerAccuracy *= bhDamage;
	attackerAccuracy *= bhDamage;

	if (attackerAccuracy > 200)
		attackerAccuracy = 200.f + ((attackerAccuracy - 200)  * .25);

	return attackerAccuracy;

}

float CombatManager::calculateDamage(CreatureObject* attacker, WeaponObject* weapon, CreatureObject* defender, const CreatureAttackData& data) {
	float damage = 0;

	int diff = 0;

	float pAccuracy = calculateAccuracy(attacker, weapon, data.isForceAttack());
	float pDefense = getDefenderDefenseModifier(defender, weapon, attacker);
	//info("pAccuracy: " + String::valueOf(pAccuracy), true);
	//info("pDefense: " + String::valueOf(pDefense), true);
	double percentage;// = ((pDefense - pAccuracy) / 1000.0); // was 250

	if (pAccuracy > pDefense){
		percentage = ((pAccuracy - pDefense) / 1000.0);
	} else{
		percentage = (((pDefense - pAccuracy) / 1000.0) * -1);
	}

	//info("Percetnage: " + String::valueOf(percentage), true);
	if (percentage < -100.0) {
		percentage = -100.0;
	} else if (percentage > 100.0) {
		percentage = 100.0;
	}

	if (data.getMinDamage() > 0 || data.getMaxDamage() > 0) { // this is a special attack (force, etc)
		//float pAccuracy = calculateAccuracy(attacker, weapon, data.isForceAttack());
		float forceDamage = (1.0 + (attacker->getSkillMod("force_damage") / 100.f));
		float rankDamage = (1.0 + ((attacker->getSkillMod("rank_offence") / 2) / 100.f));
		float minDamage = data.getMinDamage();
		float maxDamage = data.getMaxDamage();


		if (forceDamage > 2.0) {
			forceDamage = forceDamage - 1.0;
		}
		if (rankDamage > 2.0) {
			rankDamage = rankDamage - 1.0;
		}


		float averageDmg = ((maxDamage - minDamage) / 2.0);
		//info("Average Dmg: " + String::valueOf(averageDmg), true);
		damage = minDamage + (1.0 + percentage) * averageDmg;
		damage *= forceDamage;
		damage *= rankDamage;
		//info("Dmg: " + String::valueOf(damage), true);
	}

	float minDamage = weapon->getMinDamage();
	float maxDamage = weapon->getMaxDamage();

	double averageDmg = ((maxDamage - minDamage) / 2.0);
	//info("Average Dmg: " + String::valueOf(averageDmg), true);
	if(!data.isForceAttack()){
		float mathStuff = (1.0 + percentage) * averageDmg;
		percentage = minDamage + mathStuff;
		//info("percentage * averageDmg: " + String::valueOf(mathStuff), true);
		damage = percentage;
		//info("Dmg: " + String::valueOf(damage), true);
	}

	damage = applyDamageModifiers(attacker, weapon, damage, data);

	float damageSusceptibility = 1.0 + (defender->getSkillMod("private_damage_susceptibility") / 100.f);

	if (damageSusceptibility != 1.0)
		damage *= damageSusceptibility;

	//if(damage > 1){
	//	damage = calculateCrit(attacker, defender, damage, data);
	//}

	int calledShot = defender->getSkillMod("called_shot");
		damage += calledShot;

	/*float fortify = ((100 - defender->getSkillMod("fortify")) / 100.f);
	int fortifySkillMod = defender->getSkillMod("fortify");

	if (defender->isKneeling()){
		if(fortifySkillMod > 0){
			damage *= fortify;
		}else {
			damage *= 1.12f;
		}
	}*/ //removed with CU

	int attackerLevel = attacker->getLevel();
	int defenderLevel = defender->getLevel();

	float levelDifference = (float)attackerLevel / (float)defenderLevel;

	if(levelDifference > 2.0){
		levelDifference = 2.0;
	}else if (levelDifference < 0.5){
		levelDifference = 0.5;
	}
		damage *= levelDifference;

		//info("attackerLevel: " + String::valueOf(attackerLevel), true);
		//info("defenderLevel: " + String::valueOf(defenderLevel), true);
		//info("levelDifference: " + String::valueOf(levelDifference), true);

	if (defender->isKnockedDown())
		damage *= 1.2; // was .33

	if (attacker->isIntimidated()){
		damage *= 0.85;
	}
	// Toughness reduction

	if (data.isForceAttack())
		damage = getDefenderToughnessModifier(defender, SharedWeaponObjectTemplate::FORCEATTACK, data.getDamageType(), damage);
	else
		damage = getDefenderToughnessModifier(defender, weapon->getAttackType(), weapon->getDamageType(), damage);


	if (damage < 1) damage = 1;

	//info("damage to be dealt is " + String::valueOf(damage), true);

	return damage;
}

float CombatManager::calculateDamage(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, const CreatureAttackData& data) {
	float damage = 0;

	int diff = weapon->getMinDamage();
	float minDamage = weapon->getMinDamage();

	if (diff > 0)
		damage = System::random(diff) + (int)minDamage;

	damage += defender->getSkillMod("private_damage_susceptibility");

	if (defender->isKnockedDown())
		damage *= 1.5f;

	// Toughness reduction
	damage = getDefenderToughnessModifier(defender, weapon->getAttackType(), weapon->getDamageType(), damage);

	return damage;
}

int CombatManager::getHitChance(TangibleObject* attacker, CreatureObject* targetCreature, WeaponObject* weapon, int damage, int accuracyBonus, bool isForce) {
	int hitChance = 0;
	int attackType = weapon->getAttackType();
	CreatureObject* creoAttacker = NULL;

	if (attacker->isCreatureObject()) {
		creoAttacker = attacker->asCreatureObject();
	}
	float weaponAccuracy = 0.0f;
	int attackerAccuracy = calculateAccuracy(attacker, weapon, isForce);
	int bonusAccuracy = 0;
	int baseHit = 125;

	if (creoAttacker != NULL)
		bonusAccuracy = getAttackerAccuracyBonus(creoAttacker, weapon);
	// this is the scout/ranger creature hit bonus that only works against creatures (not NPCS)
	if (targetCreature->isCreature() && creoAttacker != NULL) {
		int accuracyDebuffs = creoAttacker->getSkillMod("private_attack_accuracy");
		bonusAccuracy += creoAttacker->getSkillMod("creature_hit_bonus");
		if (accuracyDebuffs != 0)
			baseHit += accuracyDebuffs;
	}
	int targetDefense = getDefenderDefenseModifier(targetCreature, weapon, attacker);

	if(isForce == true){
		targetDefense += targetCreature->getSkillMod("force_defense");
		attackerAccuracy += 50; //Base Force Accuracy Hit rate
	}
	if (targetDefense <= 0)
		targetDefense = 0;

	//info("Defender defense is " + String::valueOf(targetDefense), true);

	int postureDefense = calculateTargetPostureModifier(weapon, targetCreature);
	int gcwRank = targetCreature->getSkillMod("gcw_defense");
	int bhRank = targetCreature->getSkillMod("bhrank_defense");
	//int forceFocus = creoAttacker->getSkillMod("force_focus"); Pre CU Jedi feature
	//int forceRank = (targetCreature->getSkillMod("rank_defense") /10);

	targetDefense += gcwRank;
	targetDefense += bhRank;
	//targetDefense += forceRank;

	int attackerRoll =  attackerAccuracy + bonusAccuracy;
	int defenderRoll = targetDefense;

	if(attackerRoll < 0)
		attackerRoll = 0;


	//info("Final hit chance is " + String::valueOf(attackerRoll), true);
	//info("Base Hit Chance: " + String::valueOf(baseHit), true);
	//info("Target Defense Chance: " + String::valueOf(defenderRoll), true);

	if (System::random(attackerRoll) + System::random(baseHit) < System::random(defenderRoll))
		return MISS;
	// now we have a successful hit, so calculate secondary defenses if there is a damage component
	if (damage > 0) {
		ManagedReference<WeaponObject*> targetWeapon = targetCreature->getWeapon();
		if (targetWeapon->isJediWeapon()){
			Vector<String>* defenseAccMods = targetWeapon->getDefenderSecondaryDefenseModifiers();
			const String& def = defenseAccMods->get(0); // FIXME: this is hacky, but a lot faster than using contains()

			// saber block is special because it's just a % chance to block based on the skillmod
			if (def == "saber_block") {
				if (!attacker->isTurret() && (weapon->getAttackType() == SharedWeaponObjectTemplate::RANGEDATTACK) && ((System::random(100)) < targetCreature->getSkillMod(def)))
					return RICOCHET;
				else return HIT;
			}
		}

	}

	return HIT;
}

float CombatManager::calculateWeaponAttackSpeed(CreatureObject* attacker, WeaponObject* weapon, float skillSpeedRatio, int weaponOverwrite) {
	float speedMod = getSpeedModifier(attacker, weapon);
	float jediSpeed = attacker->getSkillMod("combat_haste") / 100.0f;

	//if (speedMod > 125)
	//	speedMod = 125.f + ((speedMod - 125)  * .05);

	//float modifiedSpeed = weapon->getAttackSpeed() - (speedMod / 100.f);

	float modifiedSpeed = Math::getPrecision((float) (weapon->getAttackSpeed(true) * (0.5231 + (0.4769 * pow(2, -speedMod / 46)))), 2);

	if (weaponOverwrite == 1)
		modifiedSpeed = 0.0;

	//float attackSpeed = (1.0f - ((float) speedMod / 100.0f)) * skillSpeedRatio * weapon->getAttackSpeed();
	//float attackSpeed = MAX((skillSpeedRatio - ((float) speedMod / 100.0f)), 1.0f) + (weapon->getAttackSpeed() - ((float) speedMod / 100.0f));
	float attackSpeed = skillSpeedRatio + modifiedSpeed;

	if (jediSpeed > 0)
		attackSpeed = attackSpeed - jediSpeed;

	//attackSpeed = attackSpeed + 1.5f;

	//info("Speed total: " + String::valueOf(attackSpeed), true);

	return Math::max(attackSpeed, 1.0f);
}

void CombatManager::doMiss(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	defender->showFlyText("combat_effects", "miss", 0xFF, 0xFF, 0xFF);

}

void CombatManager::doCounterAttack(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	defender->showFlyText("combat_effects", "counterattack", 0, 0xFF, 0);
	//defender->doCombatAnimation(defender, STRING_HASHCODE("dodge"), 0);

}

void CombatManager::doBlock(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	defender->showFlyText("combat_effects", "block", 0, 0xFF, 0);

	//defender->doCombatAnimation(defender, STRING_HASHCODE("dodge"), 0);

}

void CombatManager::doCrit(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	defender->showFlyText("combat_effects", "crit", 0, 0xFF, 0);

	//defender->doCombatAnimation(defender, STRING_HASHCODE("dodge"), 0);

}

void CombatManager::doLightsaberBlock(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	// No Fly Text.

	//creature->doCombatAnimation(defender, STRING_HASHCODE("test_sword_ricochet"), 0);

}

void CombatManager::showHitLocationFlyText(CreatureObject *attacker, CreatureObject *defender, uint8 location) {

	if (defender->isVehicleObject())
		return;

	ShowFlyText* fly = NULL;
	switch(location) {
	case HIT_HEAD:
		fly = new ShowFlyText(defender, "combat_effects", "hit_head", 0, 0, 0xFF, 1.0f);
		break;
	case HIT_BODY:
		fly = new ShowFlyText(defender, "combat_effects", "hit_body", 0xFF, 0, 0, 1.0f);
		break;
	case HIT_LARM:
		fly = new ShowFlyText(defender, "combat_effects", "hit_larm", 0xFF, 0, 0, 1.0f);
		break;
	case HIT_RARM:
		fly = new ShowFlyText(defender, "combat_effects", "hit_rarm", 0xFF, 0, 0, 1.0f);
		break;
	case HIT_LLEG:
		fly = new ShowFlyText(defender, "combat_effects", "hit_lleg", 0, 0xFF, 0, 1.0f);
		break;
	case HIT_RLEG:
		fly = new ShowFlyText(defender, "combat_effects", "hit_rleg", 0, 0xFF, 0, 1.0f);
		break;
	}

	if(fly != NULL)
		attacker->sendMessage(fly);
}
void CombatManager::doDodge(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage) {
	defender->showFlyText("combat_effects", "dodge", 0, 0xFF, 0);

	//defender->doCombatAnimation(defender, STRING_HASHCODE("dodge"), 0);

}

bool CombatManager::applySpecialAttackCost(CreatureObject* attacker, WeaponObject* weapon, const CreatureAttackData& data) {
	if (attacker->isAiAgent() || data.isForceAttack())
		return true;

	int baseForceCost = data.getForceCost();

	float force = (baseForceCost + weapon->getForceCost()) * data.getForceCostMultiplier();

	if (force > 0) { // Need Force check first otherwise it can be spammed.
		ManagedReference<PlayerObject*> playerObject = attacker->getPlayerObject();
		if (playerObject != NULL) {
			if (playerObject->getForcePower() <= force) {
				//attacker->sendSystemMessage("@jedi_spam:no_force_power");
				attacker->showFlyText("combat_effects", "action_low_force", 100, 0, 100, true);
				return false;
			} else {
				playerObject->setForcePower(playerObject->getForcePower() - force);
				VisibilityManager::instance()->increaseVisibility(attacker, 1); // Give visibility
			}
		}
	}

	float weaponCost = (weapon->getActionAttackCost() / 100.f);
	float actionAbility = weaponCost * data.getActionCostMultiplier();
	float mindAbility = weaponCost * data.getMindCostMultiplier();
	//float action = attacker->getMaxHAM(CreatureAttribute::ACTION) * (actionAbility / 100.f);
	//float mind = attacker->getMaxHAM(CreatureAttribute::MIND) * (mindAbility / 100.f);
	float action = 1000 * (actionAbility / 100.f);
	float mind = 1000 * (mindAbility / 100.f);

	/*int costGroup = data.getCostGroup();
	int lastGroupUsed = attacker->getLastCostGroup();

	//info("CostGroup From Data: " + String::valueOf(costGroup), true);
	//info("Last Group Used: " + String::valueOf(lastGroupUsed), true);
	attacker->setLastCostGroup(costGroup);

	if(costGroup == lastGroupUsed && costGroup != 0){
		action *= 2;
		mind *= 2;
	}
	*/

	float hemorrhageDebuff = (float)attacker->getSkillMod("hemorrhage_debuff");
	float traumatizeDebuff = (float)attacker->getSkillMod("traumatize_debuff");

	if (hemorrhageDebuff >= 1){
		hemorrhageDebuff /= 10.f;
		//info("hemorrhageDebuff" + String::valueOf(hemorrhageDebuff), true);
		action *= 1.0 + hemorrhageDebuff;
	}

	if (traumatizeDebuff >= 1){
		traumatizeDebuff /= 10.f;
		//info("traumatizeDebuff" + String::valueOf(traumatizeDebuff), true);
		mind *= 1.0 + traumatizeDebuff;
	}

	float strength = (float)attacker->getHAM(CreatureAttribute::STRENGTH);
	float quickness = (float)attacker->getHAM(CreatureAttribute::QUICKNESS);
	float focus = (float)attacker->getHAM(CreatureAttribute::FOCUS);


	if (attacker->getHAM(CreatureAttribute::ACTION) <= action)
		return false;

	if (attacker->getHAM(CreatureAttribute::MIND) <= mind)
		return false;


	if (action > 0)
		attacker->inflictDamage(attacker, CreatureAttribute::ACTION, action, true, true, true);

	if (mind > 0)
		attacker->inflictDamage(attacker, CreatureAttribute::MIND, mind, true, true, true);

	return true;
}

void CombatManager::applyStates(CreatureObject* creature, CreatureObject* targetCreature, const CreatureAttackData& data) {
	VectorMap<uint8, StateEffect>* stateEffects = data.getStateEffects();
	int stateAccuracyBonus = data.getStateAccuracyBonus();

	if (targetCreature->isPlayerCreature() && targetCreature->getPvpStatusBitmask() == CreatureFlag::NONE)
		return;

	int playerLevel = 0;
	int shockDebuff = targetCreature->getSkillMod("private_state_resist_debuff");

	if (targetCreature->isPlayerCreature()) {
		ZoneServer* server = targetCreature->getZoneServer();
		if (server != NULL) {
			PlayerManager* pManager = server->getPlayerManager();
			if (pManager != NULL) {
				playerLevel = pManager->calculatePlayerLevel(targetCreature) - 5;
			}
		}
	}

	// loop through all the states in the command
	for (int i = 0; i < stateEffects->size(); i++) {
		const StateEffect& effect = stateEffects->get(i);
		bool failed = false;
		uint8 effectType = effect.getEffectType();

		float accuracyMod = effect.getStateChance() + stateAccuracyBonus;


		if (data.isStateOnlyAttack())
			accuracyMod += creature->getSkillMod(data.getCommand()->getAccuracySkillMod());

		//Check for state immunity.
		if (targetCreature->hasEffectImmunity(effectType))
			failed = true;

		if(!failed) {
			const Vector<String>& exclusionTimers = effect.getDefenderExclusionTimers();
			// loop through any exclusion timers
			for (int j = 0; j < exclusionTimers.size(); j++)
				if (!targetCreature->checkCooldownRecovery(exclusionTimers.get(j))) failed = true;
		}

		float targetDefense = 0.f;

		// if recovery timer conditions aren't satisfied, it won't matter
		if (!failed) {
			const Vector<String>& defenseMods = effect.getDefenderStateDefenseModifiers();
			// add up all defenses against the state the target has
			for (int j = 0; j < defenseMods.size(); j++)
				targetDefense += targetCreature->getSkillMod(defenseMods.get(j));

			targetDefense -= targetCreature->calculateBFRatio();

			targetDefense /= 1.5;
			targetDefense += playerLevel;
			targetDefense -= shockDebuff;
			accuracyMod *= 1.5;

			if(targetDefense <= 0){
				targetDefense = 1;
			}
			//info("targetDefense " + String::valueOf(targetDefense), true);
			if(System::random(accuracyMod) < System::random(targetDefense))

				failed = true;

			// no reason to apply jedi defenses if primary defense was successful
			// and only perform second roll if the character is a Jedi
			if (!failed && targetCreature->isPlayerCreature() && targetCreature->getPlayerObject()->isJedi()) {
				targetDefense = 0.f;
				const Vector<String>& jediMods = effect.getDefenderJediStateDefenseModifiers();
				// second chance for jedi, roll against their special defense "jedi_state_defense"
				for (int j = 0; j < jediMods.size(); j++)
					targetDefense += targetCreature->getSkillMod(jediMods.get(j));

				targetDefense /= 1.5;
				targetDefense += playerLevel;
				if(targetDefense <= 0){
					targetDefense = 1;
				}

				//if (System::random(100) > accuracyMod - targetDefense)
				if(System::random(accuracyMod) < System::random(targetDefense))

					failed = true;
			}
		}

		if (!failed) {
			if (effectType == CommandEffect::NEXTATTACKDELAY) {
				StringIdChatParameter stringId("combat_effects", "delay_applied_other");
				stringId.setTT(targetCreature->getObjectID());
				stringId.setDI(effect.getStateLength());
				creature->sendSystemMessage(stringId);
			}

			data.getCommand()->applyEffect(creature, targetCreature, effectType, effect.getStateStrength() + stateAccuracyBonus);
		}

		// can move this to scripts, but only these states have fail messages
		if (failed) {
			switch (effectType) {
			case CommandEffect::KNOCKDOWN:
				if (!targetCreature->checkKnockdownRecovery() && targetCreature->getPosture() != CreaturePosture::UPRIGHT)
					targetCreature->setPosture(CreaturePosture::UPRIGHT);
				creature->sendSystemMessage("@cbt_spam:knockdown_fail");
				break;
			case CommandEffect::POSTUREDOWN:
				if (!targetCreature->checkPostureDownRecovery() && targetCreature->getPosture() != CreaturePosture::UPRIGHT)
					targetCreature->setPosture(CreaturePosture::UPRIGHT);
				creature->sendSystemMessage("@cbt_spam:posture_change_fail");
				break;
			case CommandEffect::POSTUREUP:
				if (!targetCreature->checkPostureUpRecovery() && targetCreature->getPosture() != CreaturePosture::UPRIGHT)
					targetCreature->setPosture(CreaturePosture::UPRIGHT);
				creature->sendSystemMessage("@cbt_spam:posture_change_fail");
				break;
			case CommandEffect::FROZEN:
				creature->sendSystemMessage("Failed to Mez target!");
				break;
			case CommandEffect::SNARED:
				creature->sendSystemMessage("Failed to Snare target!");
				break;
			case CommandEffect::NEXTATTACKDELAY:
				targetCreature->showFlyText("combat_effects", "warcry_miss", 0xFF, 0, 0 );
				break;
			case CommandEffect::INTIMIDATE:
				targetCreature->showFlyText("combat_effects", "intimidated_miss", 0xFF, 0, 0 );
				break;
			default:
				break;
			}
		}

		// now check combat equilibrium
		//TODO: This should eventually be moved to happen AFTER the CombatAction is broadcast to "fix" it's animation (Mantis #4832)
		if (!failed && (effectType == CommandEffect::KNOCKDOWN || effectType == CommandEffect::POSTUREDOWN || effectType == CommandEffect::POSTUREUP)) {
			int combatEquil = targetCreature->getSkillMod("combat_equillibrium");

			if (combatEquil > 100)
				combatEquil = 100;

			if ((combatEquil >> 1) > (int) System::random(100) && !targetCreature->isDead() && !targetCreature->isIntimidated())
				targetCreature->setPosture(CreaturePosture::UPRIGHT, false);
		}

		//Send Combat Spam for state-only attacks.
		if (data.isStateOnlyAttack()) {
			if (failed)
				data.getCommand()->sendAttackCombatSpam(creature, targetCreature, MISS, 0, data);
			else
				data.getCommand()->sendAttackCombatSpam(creature, targetCreature, HIT, 0, data);
		}
	}

}

int CombatManager::calculatePoolsToDamage(int poolsToDamage) {

	poolsToDamage = HEALTH;
	return poolsToDamage;
}

int CombatManager::applyDamage(TangibleObject* attacker, WeaponObject* weapon, CreatureObject* defender, int damage, float damageMultiplier, int poolsToDamage, uint8& hitLocation, const CreatureAttackData& data) {
	if (poolsToDamage == 0 || damageMultiplier == 0)
		return 0;

	float ratio = weapon->getWoundsRatio();
	float healthDamage = 0.f, actionDamage = 0.f, mindDamage = 0.f;

	if (defender->isPlayerCreature() && defender->getPvpStatusBitmask() == CreatureFlag::NONE) {
		return 0;
	}

	String xpType;

	if (data.isForceAttack())
		xpType = "jedi_general";
	else if (attacker->isPet())
		xpType = "creaturehandler";
	else
		xpType = weapon->getXpType();

	bool healthDamaged = (!!(poolsToDamage & HEALTH) && data.getHealthDamageMultiplier() > 0.0f);
	bool actionDamaged = (!!(poolsToDamage & ACTION) && data.getActionDamageMultiplier() > 0.0f);
	bool mindDamaged   = (!!(poolsToDamage & MIND)   && data.getMindDamageMultiplier()   > 0.0f);

	// from screenshots, it appears that food mitigation and armor mitigation were independently calculated
	// and then added together.
	int foodBonus = defender->getSkillMod("mitigate_damage");
	int totalFoodMit = 0;

	if (healthDamaged) {
		static uint8 bodyLocations[] = {HIT_BODY, HIT_BODY, HIT_LARM, HIT_RARM, HIT_LLEG, HIT_RLEG, HIT_HEAD};
		hitLocation = bodyLocations[System::random(6)];

		healthDamage = getArmorReduction(attacker, weapon, defender, damage, hitLocation, data);

		int foodMitigation = 0;

		if (foodBonus > 0) {
			foodMitigation = (int)(healthDamage * foodBonus / 100.f);
			foodMitigation = Math::min(healthDamage, foodMitigation * data.getHealthDamageMultiplier());
		}

		healthDamage -= foodMitigation;
		totalFoodMit += foodMitigation;

		defender->inflictDamage(attacker, CreatureAttribute::HEALTH, (int)healthDamage, true, xpType, true, true);
	}

	int totalDamage =  (int) (healthDamage + actionDamage + mindDamage);
	defender->notifyObservers(ObserverEventType::DAMAGERECEIVED, attacker, totalDamage);

	if (System::random(100) < ratio) {
		defender->addWounds(CreatureAttribute::HEALTH, 1, true);
	}

	//if(attacker->isPlayerCreature())
	//	showHitLocationFlyText(attacker->asCreatureObject(), defender, hitLocation);

	//Send defensive buff combat spam last.
	if (totalFoodMit > 0)
		sendMitigationCombatSpam(defender, weapon, totalFoodMit, FOOD);

	return totalDamage;
}

int CombatManager::applyDamage(CreatureObject* attacker, WeaponObject* weapon, TangibleObject* defender, int poolsToDamage, const CreatureAttackData& data) {
	if (poolsToDamage == 0)
		return 0;

	if (defender->isPlayerCreature() && defender->getPvpStatusBitmask() == CreatureFlag::NONE) {
		return 0;
	}


	int damage = calculateDamage(attacker, weapon, defender, data);

	float damageMultiplier = data.getDamageMultiplier();

	if(data.getCostGroup() == 81){
		if(attacker->getMaxHAM(0) >= (attacker->getMaxHAM(0) * 0.1)){
			damageMultiplier += 0.6;
		}else if (attacker->getMaxHAM(0) >= (attacker->getMaxHAM(0) * 0.3)){
			damageMultiplier += 0.4;
		}else if(attacker->getMaxHAM(0) >= (attacker->getMaxHAM(0) * 0.5)){
			damageMultiplier += 0.2;
		}else{
			damageMultiplier = data.getDamageMultiplier();
		}
	}

	if (damageMultiplier != 0)
		damage *= damageMultiplier;

	String xpType;


	if (data.isForceAttack())
		xpType = "jedi_general"; //TODO Should be force_exp??
	else if (attacker->isPet())
		xpType = "creaturehandler";
	else
		xpType = weapon->getXpType();

	if (defender->isTurret()) {

		int damageType = 0;

		if (!data.isForceAttack()) {
			damageType = weapon->getDamageType();
		} else {
			damageType = data.getDamageType();
		}

		int armorReduction = getArmorTurretReduction(attacker, defender, damageType);

		damage *= (1.f - (armorReduction / 100.f));
	}


	defender->inflictDamage(attacker, 0, damage, true, xpType, true, true);

	defender->notifyObservers(ObserverEventType::DAMAGERECEIVED, attacker, damage);

	return damage;
}

void CombatManager::sendMitigationCombatSpam(CreatureObject* defender, TangibleObject* item, uint32 damage, int type) {
	if (defender == NULL || !defender->isPlayerCreature())
			return;

	int color = 0; //text color
	String file = "";
	String stringName = "";

	switch (type) {
	case PSG:
		color = 1; //green, confirmed
		file = "cbt_spam";
		stringName = "shield_damaged";
		break;
	case FORCESHIELD:
		color = 1; //green, unconfirmed
		file = "cbt_spam";
		stringName = "forceshield_hit";
		item = NULL;
		break;
	case FORCEFEEDBACK:
		color = 2; //red, confirmed
		file = "cbt_spam";
		stringName = "forcefeedback_hit";
		item = NULL;
		break;
	case FORCEABSORB:
		color = 0; //white, unconfirmed
		file = "cbt_spam";
		stringName = "forceabsorb_hit";
		item = NULL;
		break;
	case FORCEARMOR:
		color = 1; //green, confirmed
		file = "cbt_spam";
		stringName = "forcearmor_hit";
		item = NULL;
		break;
	case ARMOR:
		color = 1; //green, confirmed
		file = "cbt_spam";
		stringName = "armor_damaged";
		break;
	case FOOD:
		color = 0; //white, confirmed
		file = "combat_effects";
		stringName = "mitigate_damage";
		item = NULL;
		break;
	default:
		break;
	}

	CombatSpam* spam = new CombatSpam(defender, NULL, defender, item, damage, file, stringName, color);
	defender->sendMessage(spam);

}

void CombatManager::broadcastCombatSpam(TangibleObject* attacker, TangibleObject* defender, TangibleObject* item, int damage, const String& file, const String& stringName, byte color) {
	if (attacker == NULL)
		return;

	Zone* zone = attacker->getZone();
	if (zone == NULL)
		return;

	CloseObjectsVector* vec = (CloseObjectsVector*) attacker->getCloseObjects();
	SortedVector<QuadTreeEntry*> closeObjects;

	if (vec != NULL) {
		closeObjects.removeAll(vec->size(), 10);
		vec->safeCopyTo(closeObjects);
	} else {
#ifdef COV_DEBUG
		info("Null closeobjects vector in CombatManager::broadcastCombatSpam", true);
#endif
		zone->getInRangeObjects(attacker->getWorldPositionX(), attacker->getWorldPositionY(), COMBAT_SPAM_RANGE, &closeObjects, true);
	}

	for (int i = 0; i < closeObjects.size(); ++i) {
		SceneObject* object = static_cast<SceneObject*>( closeObjects.get(i));

		if (object->isPlayerCreature() && attacker->isInRange(object, COMBAT_SPAM_RANGE)) {
			CreatureObject* receiver = static_cast<CreatureObject*>( object);
			CombatSpam* spam = new CombatSpam(attacker, defender, receiver, item, damage, file, stringName, color);
			receiver->sendMessage(spam);
		}
	}
}


void CombatManager::broadcastCombatAction(CreatureObject * attacker, TangibleObject * defenderObject, WeaponObject* weapon, const CreatureAttackData & data, int damage, uint8 hit, uint8 hitLocation) {
	const String& animation = data.getCommand()->getAnimation(attacker, defenderObject, weapon, hitLocation, damage);

	uint32 animationCRC = 0;

	if (!animation.isEmpty())
		animationCRC = animation.hashCode();

	//assert(animationCRC != 0);
	if(animationCRC != 0){
		info("Animation Error", true);
	}

	uint64 weaponID = weapon->getObjectID();

	CreatureObject *dcreo = defenderObject->asCreatureObject();
	if (dcreo != NULL) { // All of this funkiness only applies to creo targets, tano's don't animate hits or posture changes

		dcreo->updatePostures(false); // Commit pending posture changes to the client and notify observers

		if (data.getPrimaryTarget() != defenderObject->getObjectID()){ // Check if we should play the default animation or one of several reaction animations

			if (hit == HIT) {

				if (data.changesDefenderPosture() && (!dcreo->isIncapacitated() && !dcreo->isDead())) {
					dcreo->doCombatAnimation(STRING_HASHCODE("change_posture")); // We're not the primary target, but we are the victim of a posture change attack
				} else {
					dcreo->doCombatAnimation(STRING_HASHCODE("get_hit_medium")); // We're not the primary target but were hit - play the got hit animation
				}

			} else { // Not a hit but also not the primary target - play a dodge animation
				dcreo->doCombatAnimation(STRING_HASHCODE("dodge"));
			}

		} else { // Primary target attack - play default animation
			attacker->doCombatAnimation(dcreo, animationCRC, hit, data.getTrails(), weaponID);
		}

	} else {
		if(data.getPrimaryTarget() == defenderObject->getObjectID()){ // Tano target attack - play default animation
			attacker->doCombatAnimation(defenderObject, animationCRC, hit, data.getTrails(), weaponID);
		}
	}

	if(data.changesAttackerPosture())
		attacker->updatePostures(false);

	const String& effect = data.getCommand()->getEffectString();
	const String& targetEffect = data.getCommand()->getTargetEffect();

	if (!effect.isEmpty())
		attacker->playEffect(effect);

	if (!targetEffect.isEmpty() && defenderObject->isCreatureObject())
		cast<CreatureObject*>(defenderObject)->playEffect(targetEffect);
}

void CombatManager::requestDuel(CreatureObject* player, CreatureObject* targetPlayer) {
	/* Pre: player != targetPlayer and not NULL; player is locked
	 * Post: player requests duel to targetPlayer
	 */

	Locker clocker(targetPlayer, player);

	PlayerObject* ghost = player->getPlayerObject();
	PlayerObject* targetGhost = targetPlayer->getPlayerObject();

	if (ghost->requestedDuelTo(targetPlayer)) {
		StringIdChatParameter stringId("duel", "already_challenged");
		stringId.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId);

		return;
	}

	player->info("requesting duel");

	ghost->addToDuelList(targetPlayer);

	if (targetGhost->requestedDuelTo(player)) {
		BaseMessage* pvpstat = new UpdatePVPStatusMessage(targetPlayer, player,
				targetPlayer->getPvpStatusBitmask()
				| CreatureFlag::ATTACKABLE
				| CreatureFlag::AGGRESSIVE);
		player->sendMessage(pvpstat);

		for (int i = 0; i < targetGhost->getActivePetsSize(); i++) {
			ManagedReference<AiAgent*> pet = targetGhost->getActivePet(i);

			if (pet != NULL) {
				BaseMessage* petpvpstat = new UpdatePVPStatusMessage(pet, player,
						pet->getPvpStatusBitmask()
						| CreatureFlag::ATTACKABLE
						| CreatureFlag::AGGRESSIVE);
				player->sendMessage(petpvpstat);
			}
		}

		StringIdChatParameter stringId("duel", "accept_self");
		stringId.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId);

		BaseMessage* pvpstat2 = new UpdatePVPStatusMessage(player, targetPlayer,
				player->getPvpStatusBitmask() | CreatureFlag::ATTACKABLE
				| CreatureFlag::AGGRESSIVE);
		targetPlayer->sendMessage(pvpstat2);

		for (int i = 0; i < ghost->getActivePetsSize(); i++) {
			ManagedReference<AiAgent*> pet = ghost->getActivePet(i);

			if (pet != NULL) {
				BaseMessage* petpvpstat = new UpdatePVPStatusMessage(pet, targetPlayer,
						pet->getPvpStatusBitmask()
						| CreatureFlag::ATTACKABLE
						| CreatureFlag::AGGRESSIVE);
				targetPlayer->sendMessage(petpvpstat);
			}
		}

		StringIdChatParameter stringId2("duel", "accept_target");
		stringId2.setTT(player->getObjectID());
		targetPlayer->sendSystemMessage(stringId2);
	} else {
		StringIdChatParameter stringId3("duel", "challenge_self");
		stringId3.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId3);

		StringIdChatParameter stringId4("duel", "challenge_target");
		stringId4.setTT(player->getObjectID());
		targetPlayer->sendSystemMessage(stringId4);
	}
}

void CombatManager::requestEndDuel(CreatureObject* player, CreatureObject* targetPlayer) {
	/* Pre: player != targetPlayer and not NULL; player is locked
	 * Post: player requested to end the duel with targetPlayer
	 */

	Locker clocker(targetPlayer, player);

	PlayerObject* ghost = player->getPlayerObject();
	PlayerObject* targetGhost = targetPlayer->getPlayerObject();

	if (!ghost->requestedDuelTo(targetPlayer)) {
		StringIdChatParameter stringId("duel", "not_dueling");
		stringId.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId);

		return;
	}

	player->info("ending duel");

	ghost->removeFromDuelList(targetPlayer);
	player->removeDefender(targetPlayer);

	if (targetGhost->requestedDuelTo(player)) {
		targetGhost->removeFromDuelList(player);
		targetPlayer->removeDefender(player);

		player->sendPvpStatusTo(targetPlayer);

		for (int i = 0; i < ghost->getActivePetsSize(); i++) {
			ManagedReference<AiAgent*> pet = ghost->getActivePet(i);

			if (pet != NULL) {


				targetPlayer->removeDefender(pet);
				pet->sendPvpStatusTo(targetPlayer);

				ManagedReference<CreatureObject*> target = targetPlayer;


				Core::getTaskManager()->executeTask([=] () {
					Locker locker(pet);

					pet->removeDefender(target);

				}, "PetRemoveDefenderLambda");
			}
		}

		StringIdChatParameter stringId("duel", "end_self");
		stringId.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId);

		targetPlayer->sendPvpStatusTo(player);

		for (int i = 0; i < targetGhost->getActivePetsSize(); i++) {
			ManagedReference<AiAgent*> pet = targetGhost->getActivePet(i);

			if (pet != NULL) {


				player->removeDefender(pet);
				pet->sendPvpStatusTo(player);

				ManagedReference<CreatureObject*> play = player;


				Core::getTaskManager()->executeTask([=] () {
					Locker locker(pet);

					pet->removeDefender(play);

				}, "PetRemoveDefenderLambda2");
			}
		}

		StringIdChatParameter stringId2("duel", "end_target");
		stringId2.setTT(player->getObjectID());
		targetPlayer->sendSystemMessage(stringId2);
	}
}

void CombatManager::freeDuelList(CreatureObject* player, bool spam) {
	/* Pre: player not NULL and is locked
	 * Post: player removed and warned all of the objects from its duel list
	 */
	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == NULL || ghost->isDuelListEmpty())
		return;

	player->info("freeing duel list");

	while (ghost->getDuelListSize() != 0) {
		ManagedReference<CreatureObject*> targetPlayer = ghost->getDuelListObject(0);
		PlayerObject* targetGhost = targetPlayer->getPlayerObject();

		if (targetPlayer != NULL && targetGhost != NULL && targetPlayer.get() != player) {
			try {
				Locker clocker(targetPlayer, player);

				ghost->removeFromDuelList(targetPlayer);
				player->removeDefender(targetPlayer);

				if (targetGhost->requestedDuelTo(player)) {
					targetGhost->removeFromDuelList(player);
					targetPlayer->removeDefender(player);

					player->sendPvpStatusTo(targetPlayer);

					for (int i = 0; i < ghost->getActivePetsSize(); i++) {
						ManagedReference<AiAgent*> pet = ghost->getActivePet(i);

						if (pet != NULL) {
							targetPlayer->removeDefender(pet);
							pet->sendPvpStatusTo(targetPlayer);

							Core::getTaskManager()->executeTask([=] () {
								Locker locker(pet);

								pet->removeDefender(targetPlayer);
							}, "PetRemoveDefenderLambda3");
						}
					}

					if (spam) {
						StringIdChatParameter stringId("duel", "end_self");
						stringId.setTT(targetPlayer->getObjectID());
						player->sendSystemMessage(stringId);
					}

					targetPlayer->sendPvpStatusTo(player);

					for (int i = 0; i < targetGhost->getActivePetsSize(); i++) {
						ManagedReference<AiAgent*> pet = targetGhost->getActivePet(i);

						if (pet != NULL) {
							player->removeDefender(pet);
							pet->sendPvpStatusTo(player);

							ManagedReference<CreatureObject*> play = player;

							Core::getTaskManager()->executeTask([=] () {
								Locker locker(pet);

								pet->removeDefender(play);
							}, "PetRemoveDefenderLambda4");
						}
					}

					if (spam) {
						StringIdChatParameter stringId2("duel", "end_target");
						stringId2.setTT(player->getObjectID());
						targetPlayer->sendSystemMessage(stringId2);
					}
				}


			} catch (ObjectNotDeployedException& e) {
				ghost->removeFromDuelList(targetPlayer);

				System::out << "Exception on CombatManager::freeDuelList()\n"
						<< e.getMessage() << "\n";
			}
		}
	}
}

void CombatManager::declineDuel(CreatureObject* player, CreatureObject* targetPlayer) {
	/* Pre: player != targetPlayer and not NULL; player is locked
	 * Post: player declined Duel to targetPlayer
	 */

	Locker clocker(targetPlayer, player);

	PlayerObject* ghost = player->getPlayerObject();
	PlayerObject* targetGhost = targetPlayer->getPlayerObject();

	if (targetGhost->requestedDuelTo(player)) {
		targetGhost->removeFromDuelList(player);

		StringIdChatParameter stringId("duel", "cancel_self");
		stringId.setTT(targetPlayer->getObjectID());
		player->sendSystemMessage(stringId);

		StringIdChatParameter stringId2("duel", "cancel_target");
		stringId2.setTT(player->getObjectID());
		targetPlayer->sendSystemMessage(stringId2);
	}
}

bool CombatManager::areInDuel(CreatureObject* player1, CreatureObject* player2) {
	PlayerObject* ghost1 = player1->getPlayerObject().get();
	PlayerObject* ghost2 = player2->getPlayerObject().get();

	if (ghost1 != NULL && ghost2 != NULL) {
		if (ghost1->requestedDuelTo(player2) && ghost2->requestedDuelTo(player1))
			return true;
	}

	return false;
}

bool CombatManager::checkConeAngle(SceneObject* target, float angle,
		float creatureVectorX, float creatureVectorY, float directionVectorX,
		float directionVectorY) {
	float Target1 = target->getPositionX() - creatureVectorX;
	float Target2 = target->getPositionY() - creatureVectorY;

	float resAngle = atan2(Target2, Target1) - atan2(directionVectorY, directionVectorX);
	float degrees = resAngle * 180 / M_PI;

	float coneAngle = angle / 2;

	if (degrees > coneAngle || degrees < -coneAngle) {
		return false;
	}

	return true;
}

Reference<SortedVector<ManagedReference<TangibleObject*> >* > CombatManager::getAreaTargets(TangibleObject* attacker, WeaponObject* weapon, TangibleObject* defenderObject, const CreatureAttackData& data) {
	float creatureVectorX = attacker->getPositionX();
	float creatureVectorY = attacker->getPositionY();

	float directionVectorX = defenderObject->getPositionX() - creatureVectorX;
	float directionVectorY = defenderObject->getPositionY() - creatureVectorY;

	Reference<SortedVector<ManagedReference<TangibleObject*> >* > defenders = new SortedVector<ManagedReference<TangibleObject*> >();

	Zone* zone = attacker->getZone();

	if (zone == NULL)
		return defenders;

	PlayerManager* playerManager = zone->getZoneServer()->getPlayerManager();

	int damage = 0;

	int range = data.getAreaRange();

	if (data.getCommand()->isConeAction()) {
		int coneRange = data.getConeRange();

		if(coneRange > -1) {
			range = coneRange;
		} else {
			range = data.getRange();
		}
	}

	if (range < 0) {
		range = weapon->getMaxRange();
	}

	if (data.isSplashDamage())
		range += data.getRange();

	if (weapon->isThrownWeapon() || weapon->isHeavyWeapon())
		range = weapon->getMaxRange() + data.getAreaRange();

	try {
		//zone->rlock();

		CloseObjectsVector* vec =  (CloseObjectsVector*)attacker->getCloseObjects();

		SortedVector<QuadTreeEntry*> closeObjects;

		if (vec != NULL) {
			closeObjects.removeAll(vec->size(), 10);
			vec->safeCopyTo(closeObjects);
		} else {
#ifdef COV_DEBUG
			attacker->info("Null closeobjects vector in CombatManager::getAreaTargets", true);
#endif
			zone->getInRangeObjects(attacker->getWorldPositionX(), attacker->getWorldPositionY(), 128, &closeObjects, true);
		}

		for (int i = 0; i < closeObjects.size(); ++i) {
			SceneObject* object = static_cast<SceneObject*>(closeObjects.get(i));


			TangibleObject* tano = object->asTangibleObject();
			CreatureObject* creo = object->asCreatureObject();

			if (tano == NULL) {
				continue;
			}


			if (object == attacker || object == defenderObject) {
				//error("object is attacker");
				continue;
			}

			if (!tano->isAttackableBy(attacker)) {
				//error("object is not attackable");
				continue;
			}


			if (attacker->getWorldPosition().distanceTo(object->getWorldPosition()) - attacker->getTemplateRadius() - object->getTemplateRadius() > range) {
				//error("not in range " + String::valueOf(range));
				continue;
			}

			if (data.isSplashDamage() || weapon->isThrownWeapon() || weapon->isHeavyWeapon()) {


				if (defenderObject->getWorldPosition().distanceTo(tano->getWorldPosition()) - tano->getTemplateRadius() > data.getAreaRange() )
					continue;
			}

			if (creo != NULL && creo->isFeigningDeath() == false && creo->isIncapacitated()) {
				//error("object is incapacitated");
				continue;
			}

			if (data.getCommand()->isConeAction() && !checkConeAngle(tano, data.getConeAngle(), creatureVectorX, creatureVectorY, directionVectorX, directionVectorY)) {
				//error("object is not in cone angle");
				continue;
			}

			//			zone->runlock();

			try {
				if (!(weapon->isThrownWeapon()) && !(data.isSplashDamage()) && !(weapon->isHeavyWeapon())) {
					if (CollisionManager::checkLineOfSight(object, attacker)) {


						defenders->put(tano);
					}
				} else {
					if (CollisionManager::checkLineOfSight(object, defenderObject)) {


						defenders->put(tano);
					}
				}
			} catch (Exception& e) {
				error(e.getMessage());
			} catch (...) {
				//zone->rlock();

				throw;
			}

			//			zone->rlock();
		}

		//		zone->runlock();
	} catch (...) {
		//		zone->runlock();

		throw;
	}

	return defenders;

}

int CombatManager::getArmorTurretReduction(CreatureObject* attacker, TangibleObject* defender, int damageType) {
	int resist = 0;

	if (defender != NULL && defender->isTurret()) {
		DataObjectComponentReference* data = defender->getDataObjectComponent();

		if (data != NULL) {

			TurretDataComponent* turretData = cast<TurretDataComponent*>(data->get());

			if (turretData != NULL) {

				switch (damageType) {
				case SharedWeaponObjectTemplate::KINETIC:
					resist = turretData->getKinetic();
					break;
				case SharedWeaponObjectTemplate::ENERGY:
					resist = turretData->getEnergy();
					break;
				case SharedWeaponObjectTemplate::ELECTRICITY:
					resist = turretData->getElectricity();
					break;
				case SharedWeaponObjectTemplate::STUN:
					resist = turretData->getStun();
					break;
				case SharedWeaponObjectTemplate::BLAST:
					resist = turretData->getBlast();
					break;
				case SharedWeaponObjectTemplate::HEAT:
					resist = turretData->getHeat();
					break;
				case SharedWeaponObjectTemplate::COLD:
					resist = turretData->getCold();
					break;
				case SharedWeaponObjectTemplate::ACID:
					resist = turretData->getAcid();
					break;
				case SharedWeaponObjectTemplate::LIGHTSABER:
					resist = turretData->getLightSaber();
					break;

				}
			}
		}
	}

	return resist;
}

void CombatManager::initializeDefaultAttacks() {

	defaultRangedAttacks.add(STRING_HASHCODE("fire_1_single_light"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_1_single_medium"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_1_single_light_face"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_1_single_medium_face"));

	defaultRangedAttacks.add(STRING_HASHCODE("fire_3_single_light"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_3_single_medium"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_3_single_light_face"));
	defaultRangedAttacks.add(STRING_HASHCODE("fire_3_single_medium_face"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_light_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_light_0"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_medium_0"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_medium_0"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_light_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_light_1"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_medium_1"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_medium_1"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_light_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_light_2"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_medium_2"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_medium_2"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_light_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_light_3"));

	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_left_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_center_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_high_right_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_left_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_center_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_mid_right_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_left_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_right_medium_3"));
	defaultMeleeAttacks.add(STRING_HASHCODE("attack_low_center_medium_3"));
}
void CombatManager::checkForTefs(CreatureObject* attacker, CreatureObject* defender, bool* shouldGcwTef, bool* shouldBhTef) {
	if (*shouldGcwTef && *shouldBhTef)
		return;

	ManagedReference<CreatureObject*> attackingCreature = attacker->isPet() ? attacker->getLinkedCreature() : attacker;
	ManagedReference<CreatureObject*> targetCreature = defender->isPet() || defender->isVehicleObject() ? defender->getLinkedCreature() : defender;

	if (attackingCreature != NULL && targetCreature != NULL && attackingCreature->isPlayerCreature() && targetCreature->isPlayerCreature() && !areInDuel(attackingCreature, targetCreature)) {

		//if(targetCreature->hasSkill("force_title_jedi_rank_02") && attackingCreature->hasSkill("force_title_jedi_rank_02"))
		//	*shouldGcwTef = true;

		if (targetCreature->hasSkill("force_title_jedi_rank_02"))
			*shouldGcwTef = true;

		if (!(*shouldGcwTef) && (attackingCreature->getFaction() != targetCreature->getFaction()) && (attackingCreature->getFactionStatus() == FactionStatus::OVERT) && (targetCreature->getFactionStatus() == FactionStatus::OVERT))
			*shouldGcwTef = true;

		if (!(*shouldBhTef) && (attackingCreature->hasBountyMissionFor(targetCreature) || targetCreature->hasBountyMissionFor(attackingCreature)))
			*shouldBhTef = true;
	}
}
