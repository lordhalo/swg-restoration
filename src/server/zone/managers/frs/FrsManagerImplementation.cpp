#include "server/zone/managers/frs/FrsManager.h"
#include "server/zone/ZoneServer.h"
#include "server/zone/managers/frs/RankMaintenanceTask.h"
#include "server/zone/managers/frs/MaintenanceTask.h"
#include "server/zone/managers/frs/FrsRankingData.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/objects/player/variables/FrsData.h"
#include "server/zone/objects/building/BuildingObject.h"
#include "server/chat/ChatManager.h"
#include "server/zone/managers/stringid/StringIdManager.h"
#include "templates/faction/Factions.h"
#include "server/zone/objects/player/FactionStatus.h"
#include "server/zone/managers/skill/SkillManager.h"
#include "server/zone/managers/object/ObjectManager.h"

void FrsManagerImplementation::initialize() {
	loadLuaConfig();

	if (!frsEnabled)
		return;

	setupEnclaves();
	loadFrsData();

	Locker locker(managerData);

	//wipeArenaChallenges();

	uint64 lastTick = managerData->getLastMaintenanceTick();
	uint64 miliDiff = Time().getMiliTime() - lastTick;

	rankMaintenanceTask = new RankMaintenanceTask(_this.getReferenceUnsafeStaticCast());


	if (miliDiff >= maintenanceInterval) {
		rankMaintenanceTask->execute();
	} else {
		rankMaintenanceTask->schedule(maintenanceInterval - miliDiff);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	uint64 lastTickTwo = managerData->getLastPromoteTick();
	uint64 miliDiffTwo = Time().getMiliTime() - lastTickTwo;

	promotePlayerTask = new MaintenanceTask(_this.getReferenceUnsafeStaticCast());


	if (miliDiffTwo >= promoteInterval) {
		promotePlayerTask->execute();
	} else {
		promotePlayerTask->schedule(promoteInterval - miliDiffTwo);
	}

}

void FrsManagerImplementation::loadFrsData() {
	info("Loading frs manager data from frsmanager.db");

	ObjectDatabaseManager* dbManager = ObjectDatabaseManager::instance();
	ObjectDatabase* rankDatabase = ObjectDatabaseManager::instance()->loadObjectDatabase("frsmanager", true);

	if (rankDatabase == nullptr) {
		error("Could not load the frsmanager database.");
		return;
	}

	try {
		ObjectDatabaseIterator iterator(rankDatabase);

		uint64 objectID;

		while (iterator.getNextKey(objectID)) {
			Reference<FrsManagerData*> manData = Core::getObjectBroker()->lookUp(objectID).castTo<FrsManagerData*>();

			if (manData != nullptr) {
				managerData = manData;
			}
		}
	} catch (DatabaseException& e) {
		error("Database exception in FrsManagerImplementation::loadFrsData(): "	+ e.getMessage());
	}

	if (managerData == nullptr) {
		FrsManagerData* newManagerData = new FrsManagerData();
		ObjectManager::instance()->persistObject(newManagerData, 1, "frsmanager");

		managerData = newManagerData;
	}

	Locker locker(managerData);

	Vector<ManagedReference<FrsRank*> >* rankData = managerData->getLightRanks();

	if (rankData->size() == 0) {

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_LIGHT, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getDarkRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newRankData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_DARK, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getSithRanks();
	//Vector<ManagedReference<FrsRank*> >* sithRanks = managerData->getSithRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newSithData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_SITH, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getGreyRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newGreyData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_GREY, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getDeathWatchRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newDeathWatchData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_DEATHWATCH, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getBlackSunRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newBlackSunData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_BLACKSUN, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getImperialRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newImperialRankingData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_IMPERIAL, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}

	rankData = managerData->getRebelRanks();

	if (rankData->size() == 0) {
		Vector<ManagedReference<FrsRank*> >* newRebelRankingData = nullptr;

		for (int i = 0; i <= 11; i++) {
			ManagedReference<FrsRank*> newRank = new FrsRank(COUNCIL_REBEL, i);
			ObjectManager::instance()->persistObject(newRank, 1, "frsdata");
			rankData->add(newRank);
		}
	}
}

void FrsManagerImplementation::loadLuaConfig() {
	Lua* lua = new Lua();
	lua->init();

	if (!lua->runFile("scripts/managers/jedi/frs_manager.lua")) {
		error("Unable to load FrsManager data.");
		delete lua;
		return;
	}

	frsEnabled = lua->getGlobalInt("frsEnabled");
	petitionInterval = lua->getGlobalInt("petitionInterval");
	votingInterval = lua->getGlobalInt("votingInterval");
	acceptanceInterval = lua->getGlobalInt("acceptanceInterval");
	maintenanceInterval = lua->getGlobalInt("maintenanceInterval");
	promoteInterval = lua->getGlobalInt("promoteInterval");
	requestDemotionDuration = lua->getGlobalInt("requestDemotionDuration");
	voteChallengeDuration = lua->getGlobalInt("voteChallengeDuration");
	baseMaintCost = lua->getGlobalInt("baseMaintCost");
	requestDemotionCost = lua->getGlobalInt("requestDemotionCost");
	voteChallengeCost = lua->getGlobalInt("voteChallengeCost");
	maxPetitioners = lua->getGlobalInt("maxPetitioners");
	missedVotePenalty = lua->getGlobalInt("missedVotePenalty");

	uint32 enclaveID = lua->getGlobalInt("lightEnclaveID");

	lightEnclave = zoneServer->getObject(enclaveID).castTo<BuildingObject*>();

	enclaveID = lua->getGlobalInt("darkEnclaveID");

	darkEnclave = zoneServer->getObject(enclaveID).castTo<BuildingObject*>();

	LuaObject luaObject = lua->getGlobalObject("enclaveRoomRequirements");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			uint32 cellID = entry.getIntAt(1);
			int rank = entry.getIntAt(2);

			roomRequirements.put(cellID, rank);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("lightRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			lightRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("darkRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			darkRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("sithRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			sithRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("greyRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			greyRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("deathWatchRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			deathWatchRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("blackSunRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			blackSunRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("mandalorianRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			mandalorianRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("imperialRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			imperialRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("rebelRankingData");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);

			int rank = entry.getIntAt(1);
			String skillName = entry.getStringAt(2);
			int reqExp = entry.getIntAt(3);
			int playerCap = entry.getIntAt(4);

			Reference<FrsRankingData*> data = new FrsRankingData(rank, skillName, reqExp, playerCap);

			rebelRankingData.put(rank, data);

			entry.pop();
		}
	}

	luaObject.pop();

	luaObject = lua->getGlobalObject("frsExperienceValues");

	if (luaObject.isValidTable()) {
		for(int i = 1; i <= luaObject.getTableSize(); ++i) {
			LuaObject entry = luaObject.getObjectAt(i);
			uint64 keyHash = entry.getStringAt(1).hashCode();

			Vector<int> expValues;

			for (int j = 0; j <= 11; j++) {
				int value = entry.getIntAt(j + 2);
				expValues.add(value);
			}

			experienceValues.put(keyHash, expValues);

			entry.pop();
		}
	}

	luaObject.pop();

	delete lua;
	lua = NULL;
}

void FrsManagerImplementation::setupEnclaves() {
	ManagedReference<BuildingObject*> enclaveBuilding = lightEnclave.get();

	if (enclaveBuilding != NULL)
		setupEnclaveRooms(enclaveBuilding, "LightEnclaveRank");

	enclaveBuilding = darkEnclave.get();

	if (enclaveBuilding != NULL)
		setupEnclaveRooms(enclaveBuilding, "SithEnclaveRank");
}

FrsRank* FrsManagerImplementation::getFrsRank(short councilType, int rank) {
	if (rank < 0)
		return nullptr;

	ManagedReference<FrsRank*> frsRank = nullptr;

	Locker locker(managerData);

	if (councilType == COUNCIL_LIGHT) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getLightRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_SITH) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getSithRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_GREY) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getGreyRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_DARK) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getDarkRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_DEATHWATCH) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getDeathWatchRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_BLACKSUN) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getBlackSunRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_MANDALORIAN) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getMandalorianRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_IMPERIAL) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getImperialRanks();

		frsRank = rankData->get(rank);
	} else if (councilType == COUNCIL_REBEL) {
		Vector<ManagedReference<FrsRank*> >* rankData = managerData->getRebelRanks();

		frsRank = rankData->get(rank);
	}

	return frsRank;
}

void FrsManagerImplementation::setupEnclaveRooms(BuildingObject* enclaveBuilding, const String& groupName) {
	for (int j = 1; j <= enclaveBuilding->getTotalCellNumber(); ++j) {
		ManagedReference<CellObject*> cell = enclaveBuilding->getCell(j);

		if (cell != NULL) {
			cell->setContainerComponent("EnclaveContainerComponent");

			int roomReq = getRoomRequirement(cell->getObjectID());

			if (roomReq == -1)
				continue;

			ContainerPermissions* permissions = cell->getContainerPermissions();

			permissions->setInheritPermissionsFromParent(false);
			permissions->clearDefaultAllowPermission(ContainerPermissions::WALKIN);
			permissions->setDefaultDenyPermission(ContainerPermissions::WALKIN);

			for (int i = 11; i >= roomReq; i--) {
				permissions->setAllowPermission(groupName + String::valueOf(i), ContainerPermissions::WALKIN);
			}
		}
	}
}

void FrsManagerImplementation::setPlayerRank(CreatureObject* player, int rank) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	uint64 playerID = player->getObjectID();

	FrsData* playerData = ghost->getFrsData();

	int councilType = playerData->getCouncilType();
	String groupName = "";

	info("setPlayerRank: CouncilType: " + String::valueOf(councilType), true);
	info("setPlayerRank: rank: " + String::valueOf(rank), true);

	if (councilType == COUNCIL_LIGHT)
		groupName = "LightEnclaveRank";
	else if (councilType == COUNCIL_SITH)
		groupName = "SithEnclaveRank";
	else if (councilType == COUNCIL_DARK)
		groupName = "DarkEnclaveRank";
	else if (councilType == COUNCIL_GREY)
		groupName = "GreyEnclaveRank";
	else if (councilType == COUNCIL_DEATHWATCH)
		groupName = "DeathWatchEnclaveRank";
	else if (councilType == COUNCIL_BLACKSUN)
		groupName = "BlackSunEnclaveRank";
	else if (councilType == COUNCIL_MANDALORIAN)
		groupName = "MandalorianEnclaveRank";
	else if (councilType == COUNCIL_IMPERIAL)
		groupName = "ImperialEnclaveRank";
	else if (councilType == COUNCIL_REBEL)
		groupName = "RebelEnclaveRank";
	else
		return;

	int curRank = playerData->getRank();

	info("setPlayerRank: curRank: " + String::valueOf(curRank), true);

	if (curRank > 0) {
		/*ghost->removePermissionGroup(groupName + String::valueOf(curRank), true);

		ManagedReference<FrsRank*> rankData = getFrsRank(councilType, curRank);

		if (rankData != nullptr) {
			Locker clocker(rankData, player);
			rankData->removeFromPlayerList(playerID);
		}*/

		if (curRank > rank) {
			String stfRank = "@force_rank:rank" + String::valueOf(rank);
			String rankString = StringIdManager::instance()->getStringId(stfRank.hashCode()).toString();

			StringIdChatParameter param("@force_rank:rank_lost"); // You have been demoted to %TO.
			param.setTO(rankString);
			player->sendSystemMessage(param);
		}

		// Player is getting demoted, remove any pending petitions
	/*	if (curRank < 11 && curRank > rank) {
			rankData = getFrsRank(councilType, curRank + 1);

			if (rankData != nullptr) {
				Locker clocker(rankData, player);

				if (rankData->isOnPetitionerList(playerID)) {
					if (councilType == COUNCIL_DARK)
						modifySuddenDeathFlags(player, rankData, true);

					rankData->removeFromPetitionerList(playerID);
				}
			}
		} */
	}

	playerData->setRank(rank);

	/*if (rank <= 0) {
		Locker clocker(managerData, player);
		managerData->removeChallengeTime(playerID);
	}*/

	if (rank >= 0)
		ghost->addPermissionGroup(groupName + String::valueOf(rank), true);

	/*if (rank >= 0) {
		ManagedReference<FrsRank*> rankData = getFrsRank(councilType, rank);

		if (rankData != nullptr) {
			Locker clocker(rankData, player);
			rankData->addToPlayerList(playerID);
		}
	}*/

	updatePlayerSkills(player);
}

int FrsManagerImplementation::getSkillRank(const String& skillName, int councilType) {
	VectorMap<uint, Reference<FrsRankingData*> > rankingData;

	if (councilType == COUNCIL_LIGHT)
		rankingData = lightRankingData;
	else if (councilType == COUNCIL_DARK)
		rankingData = darkRankingData;
	else if (councilType == COUNCIL_SITH)
		rankingData = sithRankingData;
	else if (councilType == COUNCIL_GREY)
		rankingData = greyRankingData;
	else if (councilType == COUNCIL_DEATHWATCH)
		rankingData = deathWatchRankingData;
	else if (councilType == COUNCIL_BLACKSUN)
		rankingData = blackSunRankingData;
	else if (councilType == COUNCIL_MANDALORIAN)
		rankingData = mandalorianRankingData;
	else if (councilType == COUNCIL_IMPERIAL)
		rankingData = imperialRankingData;
	else if (councilType == COUNCIL_REBEL)
		rankingData = rebelRankingData;
	else
		return -1;

	for (int i = 0; i <= 11; i++) {
		Reference<FrsRankingData*> rankData = rankingData.get(i);
		String rankSkill = rankData->getSkillName();

		if (rankSkill.hashCode() == skillName.hashCode())
			return i;
	}

	return -1;
}

void FrsManagerImplementation::updatePlayerSkills(CreatureObject* player) {
	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == NULL)
		return;

	info("Update Player Skill", true);
	FrsData* frsData = ghost->getFrsData();
	int playerRank = frsData->getRank();
	int councilType = frsData->getCouncilType();
	VectorMap<uint, Reference<FrsRankingData*> > rankingData;

	if (councilType == COUNCIL_LIGHT)
		rankingData = lightRankingData;
	else if (councilType == COUNCIL_DARK)
		rankingData = darkRankingData;
	else if (councilType == COUNCIL_SITH)
		rankingData = sithRankingData;
	else if (councilType == COUNCIL_GREY)
		rankingData = greyRankingData;
	else if (councilType == COUNCIL_DEATHWATCH)
		rankingData = deathWatchRankingData;
	else if (councilType == COUNCIL_BLACKSUN)
		rankingData = blackSunRankingData;
	else if (councilType == COUNCIL_MANDALORIAN)
		rankingData = mandalorianRankingData;
	else if (councilType == COUNCIL_IMPERIAL)
		rankingData = imperialRankingData;
	else if (councilType == COUNCIL_REBEL)
		rankingData = rebelRankingData;
	else
		return;

	SkillManager* skillManager = zoneServer->getSkillManager();

	if (skillManager == nullptr)
		return;

	for (int i = 11; i >= 0; i--) {
		Reference<FrsRankingData*> rankData = rankingData.get(i);
		String rankSkill = rankData->getSkillName();
		int rank = rankData->getRank();

		if (playerRank >= rank) {
			//info("Player Rank > Rank", true);
			//info("Player Rank: " + String::valueOf(playerRank), true);
			//info("RankData Rank: " + String::valueOf(rank), true);
			if (!player->hasSkill(rankSkill))
				//info("Award Skill", true);
				skillManager->awardSkill(rankSkill, player, true, true, true);

			if (rank == 4 && !player->hasSkill("force_title_jedi_rank_04"))
				player->addSkill("force_title_jedi_rank_04", true);
			if (rank == 8 && !player->hasSkill("force_title_jedi_master"))
				player->addSkill("force_title_jedi_master", true);
		} else {
			if (player->hasSkill(rankSkill))
				skillManager->surrenderSkill(rankSkill, player, true, false);
		}
}
}

void FrsManagerImplementation::promotePlayer(CreatureObject* player) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;


	FrsData* playerData = ghost->getFrsData();

	int councilType = playerData->getCouncilType();
	int rank = playerData->getRank();

	if (rank < 0)
		rank = 0;

	info("void::promotePlayer(playerRank) : " + String::valueOf(rank), true);
	info("void::promotePlayer(councilType) : " + String::valueOf(councilType), true);

	Reference<FrsRankingData*> rankingData = nullptr;

	if (councilType == COUNCIL_LIGHT)
		rankingData = lightRankingData.get(rank + 1);
	else if (councilType == COUNCIL_SITH)
		rankingData = sithRankingData.get(rank + 1);
	else if (councilType == COUNCIL_DARK)
			rankingData = darkRankingData.get(rank + 1);
	else if (councilType == COUNCIL_GREY)
			rankingData = greyRankingData.get(rank + 1);
	else if (councilType == COUNCIL_DEATHWATCH)
			rankingData = deathWatchRankingData.get(rank + 1);
	else if (councilType == COUNCIL_BLACKSUN)
			rankingData = blackSunRankingData.get(rank + 1);
	else if (councilType == COUNCIL_MANDALORIAN)
			rankingData = mandalorianRankingData.get(rank + 1);
	else if (councilType == COUNCIL_IMPERIAL)
			rankingData = imperialRankingData.get(rank + 1);
	else if (councilType == COUNCIL_REBEL)
			rankingData = rebelRankingData.get(rank + 1);

	Reference<FrsRankingData*> oldRankData = nullptr;

	if (councilType == COUNCIL_LIGHT)
		oldRankData = lightRankingData.get(rank);
	else if (councilType == COUNCIL_SITH)
		oldRankData = sithRankingData.get(rank);
	else if (councilType == COUNCIL_DARK)
		oldRankData = darkRankingData.get(rank);
	else if (councilType == COUNCIL_GREY)
		oldRankData = greyRankingData.get(rank);
	else if (councilType == COUNCIL_DEATHWATCH)
		oldRankData = deathWatchRankingData.get(rank);
	else if (councilType == COUNCIL_BLACKSUN)
		oldRankData = blackSunRankingData.get(rank);
	else if (councilType == COUNCIL_MANDALORIAN)
		oldRankData = mandalorianRankingData.get(rank);
	else if (councilType == COUNCIL_IMPERIAL)
		oldRankData = imperialRankingData.get(rank);
	else if (councilType == COUNCIL_REBEL)
		oldRankData = rebelRankingData.get(rank);

	int newRank = rank;
	int currentExp = ghost->getExperience("force_rank_xp");
	int nextBoxRequiredXP;
	int currentBoxRequiredXP;

	if (rank == -1) {
		removeFromFrs(player);
	}


	if (rankingData != nullptr){
		nextBoxRequiredXP = rankingData->getRequiredExperience();
	}

	if (oldRankData != nullptr){
		currentBoxRequiredXP = oldRankData->getRequiredExperience();
	}

	if (currentExp < currentBoxRequiredXP){
		demotePlayer(player);
		return;
	}

	if (rank >= 4)
		return;

	if (rank > 10)
		return;

	if (currentExp > nextBoxRequiredXP){
		newRank = (rank + 1);

		String stfRank = "@force_rank:rank" + String::valueOf(newRank);
		String rankString = StringIdManager::instance()->getStringId(stfRank.hashCode()).toString();

		StringIdChatParameter param("@force_rank:rank_gained"); // You have achieved the Enclave rank of %TO.
		param.setTO(rankString);
		player->sendSystemMessage(param);
	}


	ManagedReference<FrsManager*> strongMan = _this.getReferenceUnsafeStaticCast();
	ManagedReference<CreatureObject*> strongRef = player->asCreatureObject();

	Core::getTaskManager()->executeTask([strongMan, strongRef, newRank] () {
		Locker locker(strongRef);
		strongMan->setPlayerRank(strongRef, newRank);
		//strongMan->recoverJediItems(strongRef);
	}, "SetPlayerRankTask");


	validatePlayerData(player);
}

void FrsManagerImplementation::demotePlayer(CreatureObject* player) {
	if (player == NULL)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == NULL)
		return;

	FrsData* frsData = ghost->getFrsData();
	int rank = frsData->getRank();

	if (rank == 0)
		return;

	info("Demote Player Method", true);
	int newRank = rank - 1;
	setPlayerRank(player, newRank);

}

void FrsManagerImplementation::adjustFrsExperience(CreatureObject* player, int amount) {
	if (player == NULL || amount == 0)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == NULL)
		return;

	if (amount > 0) {
		ghost->addExperience("force_rank_xp", amount, true);

		StringIdChatParameter param("@force_rank:experience_granted"); // You have gained %DI Force Rank experience.
		param.setDI(amount);

		player->sendSystemMessage(param);
	} else {
		FrsData* frsData = ghost->getFrsData();
		int rank = frsData->getRank();
		int councilType = frsData->getCouncilType();

		int curExperience = ghost->getExperience("force_rank_xp");

		// Ensure we dont go into the negatives
		if ((amount * -1) > curExperience)
			amount = curExperience * -1;

		ghost->addExperience("force_rank_xp", amount, true);

		StringIdChatParameter param("@force_rank:experience_lost"); // You have lost %DI Force Rank experience.
		param.setDI(amount * -1);
		player->sendSystemMessage(param);

		curExperience += amount;

		Reference<FrsRankingData*> rankingData = NULL;

		if (councilType == COUNCIL_LIGHT)
			rankingData = lightRankingData.get(rank);
		else if (councilType == COUNCIL_SITH)
			rankingData = sithRankingData.get(rank);
		else if (councilType == COUNCIL_GREY)
			rankingData = greyRankingData.get(rank);
		else if (councilType == COUNCIL_DARK)
			rankingData = darkRankingData.get(rank);
		else if (councilType == COUNCIL_DEATHWATCH)
			rankingData = deathWatchRankingData.get(rank);
		else if (councilType == COUNCIL_BLACKSUN)
			rankingData = blackSunRankingData.get(rank);
		else if (councilType == COUNCIL_MANDALORIAN)
			rankingData = mandalorianRankingData.get(rank);
		else if (councilType == COUNCIL_IMPERIAL)
			rankingData = imperialRankingData.get(rank);
		else if (councilType == COUNCIL_REBEL)
			rankingData = rebelRankingData.get(rank);

		if (rankingData == NULL)
			return;

		int reqXp = rankingData->getRequiredExperience();

	}
}

Vector<uint64> FrsManagerImplementation::getFullPlayerList() {
	Vector<uint64> playerList;
	Vector<uint64> memberList = getPlayerListByCouncil(COUNCIL_LIGHT);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_DARK);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_SITH);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_GREY);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_DEATHWATCH);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_BLACKSUN);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_MANDALORIAN);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_IMPERIAL);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	memberList = getPlayerListByCouncil(COUNCIL_REBEL);

	for (int i = 0; i < memberList.size(); ++i) {
		uint64 playerID = memberList.get(i);
		if(!playerList.contains(playerID)){
			playerList.add(playerID);
		}
	}

	return playerList;
}

Vector<uint64> FrsManagerImplementation::getPlayerListByCouncil(int councilType) {
	Vector<uint64> playerList;

	Vector<ManagedReference<FrsRank*> >* rankData;

	Locker locker(managerData);

	if (councilType == COUNCIL_LIGHT)
		rankData = managerData->getLightRanks();
	else if (councilType == COUNCIL_DARK)
		rankData = managerData->getDarkRanks();
	else if (councilType == COUNCIL_SITH)
		rankData = managerData->getSithRanks();
	else if (councilType == COUNCIL_GREY)
		rankData = managerData->getGreyRanks();
	else if (councilType == COUNCIL_DEATHWATCH)
		rankData = managerData->getDeathWatchRanks();
	else if (councilType == COUNCIL_BLACKSUN)
		rankData = managerData->getBlackSunRanks();
	else if (councilType == COUNCIL_MANDALORIAN)
		rankData = managerData->getMandalorianRanks();
	else if (councilType == COUNCIL_IMPERIAL)
		rankData = managerData->getImperialRanks();
	else if (councilType == COUNCIL_REBEL)
		rankData = managerData->getRebelRanks();
	else
		return playerList;

	for (int i = 0; i < rankData->size(); i++) {
		ManagedReference<FrsRank*> frsRank = rankData->get(i);

		if (frsRank == nullptr)
			continue;

		Locker clocker(frsRank, managerData);

		SortedVector<uint64>* rankList = frsRank->getPlayerList();

		for (int j = 0; j < rankList->size(); j++) {
			uint64 playerID = rankList->get(j);
			if(!playerList.contains(playerID)){
				playerList.add(playerID);
			}
		}
	}

	return playerList;
}

void FrsManagerImplementation::deductMaintenanceXp(CreatureObject* player) {
	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == NULL)
		return;

	FrsData* frsData = ghost->getFrsData();
	int rank = frsData->getRank();

	if (rank == 0)
		return;

	info("Deduct Maintenace XP - Player Rank: " + String::valueOf(rank), true);

	int maintXp = rank * baseMaintCost;

	ChatManager* chatManager = zoneServer->getChatManager();

	StringIdChatParameter mailBody("@force_rank:xp_maintenance_body"); // You have lost %DI Force Rank experience. All members of Rank 1 or higher must pay experience each day to remain in their current positions. (Note: This loss may not take effect until your next login.)
	mailBody.setDI(maintXp);

	chatManager->sendMail("Enclave Records", "@force_rank:xp_maintenace_sub", mailBody, player->getFirstName(), NULL);

	info("MaintXP Debt" + String::valueOf(maintXp), true);
	addExperienceDebt(player, maintXp);
}

void FrsManagerImplementation::addExperienceDebt(CreatureObject* player, int amount) {
	uint64 playerID = player->getObjectID();

	Locker clocker(managerData, player);

	int curDebt = managerData->getExperienceDebt(playerID);
	int setValue = curDebt + amount;

	if (setValue < 0){
		setValue = 0;
	}

	info("curDebt Debt" + String::valueOf(curDebt), true);

	managerData->removeExperienceDebt(playerID);
	managerData->setExperienceDebt(playerID, setValue);
}

void FrsManagerImplementation::deductDebtExperience(CreatureObject* player) {
	uint64 playerID = player->getObjectID();

	Locker clocker(managerData, player);

	int curDebt = managerData->getExperienceDebt(playerID);

	info("Current EXP Debt" + String::valueOf(curDebt), true);

	if (curDebt <= 0)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	FrsData* frsData = ghost->getFrsData();
	int rank = frsData->getRank();

	if (rank > 0)
		adjustFrsExperience(player, curDebt * -1);

	managerData->removeExperienceDebt(playerID);
}

bool FrsManagerImplementation::isValidFrsBattle(CreatureObject* attacker, CreatureObject* victim) {
	PlayerObject* attackerGhost = attacker->getPlayerObject();
	PlayerObject* victimGhost = victim->getPlayerObject();

	// No credit if they were killed by the attacker recently
	if (attackerGhost == NULL || victimGhost == NULL)
		return false;

	FrsData* attackerData = attackerGhost->getFrsData();
	int attackerRank = attackerData->getRank();
	int attackerCouncil = attackerData->getCouncilType();

	FrsData* victimData = victimGhost->getFrsData();
	int victimRank = victimData->getRank();
	int victimCouncil = victimData->getCouncilType();

	// Neither player is in the FRS
	if (victimCouncil == 0 && attackerCouncil == 0)
		return false;

	// No credit if they are in the same council
	if ((attackerCouncil == COUNCIL_LIGHT && victimCouncil == COUNCIL_LIGHT) || (attackerCouncil == COUNCIL_SITH && victimCouncil == COUNCIL_SITH) ||
			(attackerCouncil == COUNCIL_DEATHWATCH && victimCouncil == COUNCIL_DEATHWATCH) || (attackerCouncil == COUNCIL_BLACKSUN && victimCouncil == COUNCIL_BLACKSUN) ||
			(attackerCouncil == COUNCIL_IMPERIAL && victimCouncil == COUNCIL_IMPERIAL) || (attackerCouncil == COUNCIL_REBEL && victimCouncil == COUNCIL_REBEL))
		return false;

	return true;
}

int FrsManagerImplementation::calculatePvpExperienceChange(CreatureObject* attacker, CreatureObject* victim, float contribution, bool isVictim) {
	PlayerObject* attackerGhost = attacker->getPlayerObject();
	PlayerObject* victimGhost = victim->getPlayerObject();

	if (attackerGhost == NULL || victimGhost == NULL)
		return 0;

	int targetRating = 0;
	int opponentRating = 0;

	PlayerObject* playerGhost = NULL;
	PlayerObject* opponentGhost = NULL;

	if (isVictim) {
		targetRating = victimGhost->getPvpRating();
		opponentRating = attackerGhost->getPvpRating();

		playerGhost = victimGhost;
		opponentGhost = attackerGhost;
	} else {
		targetRating = attackerGhost->getPvpRating();
		opponentRating = victimGhost->getPvpRating();

		playerGhost = attackerGhost;
		opponentGhost = victimGhost;
	}

	int ratingDiff = abs(targetRating - opponentRating);

	if (ratingDiff > 2000)
		ratingDiff = 2000;

	float xpAdjustment = ((float)ratingDiff / 2000.f) * 0.5f;
	int xpChange = getBaseExperienceGain(playerGhost, opponentGhost, !isVictim);

	if (xpChange != 0) {
		xpChange = (int)((float)xpChange * contribution);

		// Adjust xp value depending on pvp rating
		// A lower rated victim will lose less experience, a higher rated victim will lose more experience
		// A lower rated victor will gain more experience, a higher rated victor will gain less experience
		if ((targetRating < opponentRating && isVictim) || (targetRating > opponentRating && !isVictim)) {
			xpChange -= (int)((float)xpChange * xpAdjustment);
		} else {
			xpChange += (int)((float)xpChange * xpAdjustment);
		}
	}

	return xpChange;
}

int FrsManagerImplementation::getBaseExperienceGain(PlayerObject* playerGhost, PlayerObject* opponentGhost, bool playerWon) {
	ManagedReference<CreatureObject*> opponent = opponentGhost->getParentRecursively(SceneObjectType::PLAYERCREATURE).castTo<CreatureObject*>();

	if (opponent == NULL)
		return 0;

	FrsData* playerData = playerGhost->getFrsData();
	int playerRank = playerData->getRank();
	int playerCouncil = playerData->getCouncilType();

	FrsData* opponentData = opponentGhost->getFrsData();
	int opponentRank = opponentData->getRank();

	// Make sure player is part of a council before we grab any value to award
	if (playerCouncil == 0)
		return 0;

	String key = "";

	if (opponent->hasSkill("combat_bountyhunter_master")) { // Opponent is MBH
		key = "bh_";
	} else if (opponentRank >= 0 && opponent->hasSkill("force_title_jedi_rank_03")) { // Opponent is at least a knight
		key = "rank" + String::valueOf(opponentRank) + "_";
	} else if (opponent->hasSkill("force_title_jedi_rank_02")) { // Opponent is padawan
		key = "padawan_";
	} else { // Opponent is non jedi
		key = "nonjedi_";
	}

	if (playerWon) {
		key = key + "win";
	} else {
		key = key + "lose";
	}

	uint64 keyHash = key.hashCode();

	if (!experienceValues.contains(keyHash))
		return 0;

	Vector<int> expValues = experienceValues.get(keyHash);

	int returnValue = expValues.get(playerRank);

	if (!playerWon)
		returnValue *= -1;

	info("FRSManager:returnValue: " + String::valueOf(returnValue), true);

	return returnValue;
}

void FrsManagerImplementation::playerLoggedIn(CreatureObject* player) {
	if (!frsEnabled || player == nullptr)
		return;

	Locker lock(player);

	validatePlayerData(player);
	deductDebtExperience(player);
	//promotePlayer(player);


}

void FrsManagerImplementation::nonJediLoggedIn(CreatureObject* player) {
	if (!frsEnabled || player == nullptr)
		return;

	Locker lock(player);

	info("Non Jedi logged in", true);

	validateNonJediData(player);
	deductDebtExperience(player);
	//promotePlayer(player);


}

void FrsManagerImplementation::validatePlayerData(CreatureObject* player) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	FrsData* playerData = ghost->getFrsData();
	int councilType = playerData->getCouncilType();
	int curPlayerRank = playerData->getRank();

	if (curPlayerRank == -1)
		return;

	int realPlayerRank = 0;

	if (curPlayerRank == 0 && !player->hasSkill("rank_light_novice") && !player->hasSkill("rank_sith_novice") && !player->hasSkill("rank_grey_novice") )
		realPlayerRank = -1;

	uint64 playerID = player->getObjectID();

	for (int i = 0; i <= 11; ++i) {
		ManagedReference<FrsRank*> rankData = getFrsRank(councilType, i);

		if (rankData == nullptr)
			continue;

		Locker clocker(rankData, player);

		if (rankData->isOnPlayerList(playerID)) {
			realPlayerRank = curPlayerRank;
			break;
		}
	}

	if ((councilType == COUNCIL_LIGHT && !player->hasSkill("rank_light_novice")) || (councilType == COUNCIL_SITH && !player->hasSkill("rank_sith_novice"))
			|| (councilType == COUNCIL_GREY && !player->hasSkill("rank_grey_novice")))
		realPlayerRank = -1;


	if (realPlayerRank != curPlayerRank) {
		if (realPlayerRank == -1 && (councilType == COUNCIL_LIGHT || councilType == COUNCIL_SITH || councilType == COUNCIL_GREY)) {
			removeFromFrs(player);
		} else {
			setPlayerRank(player, realPlayerRank);
		}
	}

	if (realPlayerRank >= 0 && (councilType == COUNCIL_LIGHT || councilType == COUNCIL_DARK || councilType == COUNCIL_SITH || councilType == COUNCIL_GREY)) {
		if (councilType == COUNCIL_LIGHT && player->getFaction() != Factions::FACTIONREBEL)
			player->setFaction(Factions::FACTIONREBEL);
		else if ( councilType == COUNCIL_SITH && player->getFaction() != Factions::FACTIONIMPERIAL)
			player->setFaction(Factions::FACTIONIMPERIAL);
		else if ( councilType == COUNCIL_GREY && player->getFaction() != Factions::FACTIONNEUTRAL)
			player->setFaction(Factions::FACTIONNEUTRAL);

		if (player->getFactionStatus() != FactionStatus::COVERT)
			player->setFactionStatus(FactionStatus::COVERT);

		if (realPlayerRank >= 4 && !player->hasSkill("force_title_jedi_rank_04"))
			player->addSkill("force_title_jedi_rank_04", true);
		if (realPlayerRank >= 8 && !player->hasSkill("force_title_jedi_master"))
			player->addSkill("force_title_jedi_master", true);

		/*if (realPlayerRank == 0) {
			SkillManager* skillManager = zoneServer->getSkillManager();

			if (skillManager == nullptr)
				return;

			if (councilType == COUNCIL_LIGHT && player->getSkillMod("force_control_light") == 0) {
				player->removeSkill("rank_light_novice", true);
				skillManager->awardSkill("rank_light_novice", player, true, false, true);
			} else if (councilType == COUNCIL_DARK && player->getSkillMod("force_control_dark") == 0) {
				player->removeSkill("force_rank_dark_novice", true);
				skillManager->awardSkill("force_rank_dark_novice", player, true, false, true);
			}
		}*/

		if (realPlayerRank >= 0) {
			ManagedReference<FrsRank*> rankData = getFrsRank(councilType, realPlayerRank);

			if (rankData != nullptr) {
				Locker clocker(rankData, player);
				if (!rankData->isOnPlayerList(playerID)) {
					rankData->addToPlayerList(playerID);
				}
			}
		}
	}

	ghost->recalculateForcePower();
}

void FrsManagerImplementation::validateNonJediData(CreatureObject* player) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	FrsData* playerData = ghost->getFrsData();
	int councilType = playerData->getCouncilType();
	int curPlayerRank = playerData->getRank();

	if (curPlayerRank == -1)
		return;

	int realPlayerRank = 0;

	if (curPlayerRank == 0 && !player->hasSkill("death_watch_novice") && !player->hasSkill("black_sun_novice") && !player->hasSkill("mandalorian_novice") )
		realPlayerRank = -1;

	uint64 playerID = player->getObjectID();

	for (int i = 0; i <= 11; i++) {
		ManagedReference<FrsRank*> rankData = getFrsRank(councilType, i);

		if (rankData == nullptr)
			continue;

		Locker clocker(rankData, player);

		if (rankData->isOnPlayerList(playerID)) {
			realPlayerRank = curPlayerRank;
			break;
		}
	}

	if ((councilType == COUNCIL_DEATHWATCH && !player->hasSkill("death_watch_novice")) || (councilType == COUNCIL_BLACKSUN && !player->hasSkill("black_sun_novice"))
			|| (councilType == COUNCIL_MANDALORIAN && !player->hasSkill("mandalorian_novice")))
		realPlayerRank = -1;

	if (realPlayerRank != curPlayerRank) {
		if (realPlayerRank == -1 && (councilType == COUNCIL_DEATHWATCH || councilType == COUNCIL_BLACKSUN || councilType == COUNCIL_MANDALORIAN)) {
			removeFromFrs(player);
		} else {
			setPlayerRank(player, realPlayerRank);
		}
	}

	if (realPlayerRank >= 0 && (councilType == COUNCIL_DEATHWATCH || councilType == COUNCIL_BLACKSUN || councilType == COUNCIL_MANDALORIAN)) {
		if (councilType == COUNCIL_DEATHWATCH && player->getFaction() != Factions::FACTIONREBEL)
			player->setFaction(Factions::FACTIONREBEL);
		else if ( councilType == COUNCIL_BLACKSUN && player->getFaction() != Factions::FACTIONIMPERIAL)
			player->setFaction(Factions::FACTIONIMPERIAL);
		else if ( councilType == COUNCIL_MANDALORIAN && player->getFaction() != Factions::FACTIONNEUTRAL)
			player->setFaction(Factions::FACTIONNEUTRAL);

		if (player->getFactionStatus() != FactionStatus::OVERT)
			player->setFactionStatus(FactionStatus::OVERT);

		if (realPlayerRank >= 0) {
			ManagedReference<FrsRank*> rankData = getFrsRank(councilType, realPlayerRank);

			if (rankData != nullptr) {
				Locker clocker(rankData, player);
				if (!rankData->isOnPlayerList(playerID)) {
					rankData->addToPlayerList(playerID);
				}
			}
		}
	}

}

void FrsManagerImplementation::removeFromFrs(CreatureObject* player) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	uint64 playerID = player->getObjectID();

	FrsData* playerData = ghost->getFrsData();
	int curRank = playerData->getRank();

	int councilType = playerData->getCouncilType();
	String groupName = "";

	if (councilType == COUNCIL_LIGHT) {
		groupName = "LightEnclaveRank";
	} else if (councilType == COUNCIL_DARK) {
		groupName = "DarkEnclaveRank";
	} else if (councilType == COUNCIL_SITH) {
		groupName = "SithEnclaveRank";
	}else {
		playerData->setRank(-1);
		return;
	}

	ManagedReference<FrsRank*> rankData = getFrsRank(councilType, curRank);

	if (rankData != nullptr) {
		Locker clocker(rankData, player);
		rankData->removeFromPlayerList(playerID);
	}

	playerData->setRank(-1);
	playerData->setCouncilType(0);

	updatePlayerSkills(player);

	/*StringIdChatParameter param("@force_rank:council_left"); // You have left the %TO.

	if (councilType == COUNCIL_LIGHT) {
		param.setTO("Light Jedi Council");
	} else if (councilType == COUNCIL_DARK) {
		param.setTO("Dark Jedi Council");
	} else if (councilType == COUNCIL_SITH) {
		param.setTO("Sith Council");
	} else if (councilType == COUNCIL_GREY) {
		param.setTO("Grey Jedi Council");
	}

	player->sendSystemMessage(param); */
}

void FrsManagerImplementation::handleSkillRevoked(CreatureObject* player, const String& skillName) {
	PlayerObject* ghost = player->getPlayerObject();

	if (ghost == nullptr)
		return;

	FrsData* playerData = ghost->getFrsData();
	int playerRank = playerData->getRank();
	int councilType = playerData->getCouncilType();

	if (playerRank < 0 || councilType == 0)
		return;

	if (skillName.hashCode() == STRING_HASHCODE("force_title_jedi_rank_03")) {
		VectorMap<uint, Reference<FrsRankingData*> > rankingData;

		if (councilType == COUNCIL_LIGHT)
			rankingData = lightRankingData;
		else if (councilType == COUNCIL_DARK)
			rankingData = darkRankingData;
		else if (councilType == COUNCIL_SITH)
			rankingData = sithRankingData;
		else if (councilType == COUNCIL_GREY)
			rankingData = greyRankingData;

		SkillManager* skillManager = zoneServer->getSkillManager();

		for (int i = rankingData.size() -1; i >= 0; i--) {
			Reference<FrsRankingData*> rankData = rankingData.get(i);
			String rankSkill = rankData->getSkillName();

			if (player->hasSkill(rankSkill)) {
				skillManager->surrenderSkill(rankSkill, player, true, false);
			}
		}

		return;
	}

	int skillRank = getSkillRank(skillName, councilType);

	if (skillRank < 0) {
		return;
	} else if (skillRank > 0) {
		setPlayerRank(player, skillRank - 1);
	} else if (skillRank == 0) {
		removeFromFrs(player);
	}
}

int FrsManagerImplementation::getAvailableRankSlots(FrsRank* rankData) {
	short councilType = rankData->getCouncilType();
	int rank = rankData->getRank();

	VectorMap<uint, Reference<FrsRankingData*> > rankingData;

	if (councilType == COUNCIL_LIGHT)
		rankingData = lightRankingData;
	else if (councilType == COUNCIL_DARK)
		rankingData = darkRankingData;
	else if (councilType == COUNCIL_SITH)
		rankingData = sithRankingData;
	else if (councilType == COUNCIL_GREY)
		rankingData = greyRankingData;

	Reference<FrsRankingData*> rankInfo = rankingData.get(rank);

	if (rankData == nullptr)
		return 0;

	return rankInfo->getPlayerCap() - rankData->getTotalPlayersInRank();
}
