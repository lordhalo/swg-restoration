#include "server/zone/managers/frs/FrsManagerData.h"

void FrsManagerDataImplementation::updateDemoteTime(uint64 playerID) {
	Time current;
	demoteTimes.put(playerID, current);
}

bool FrsManagerDataImplementation::hasDemotedRecently(uint64 playerID, uint64 demoteCooldown) {
	if (!demoteTimes.contains(playerID))
		return false;

	if (getDemoteDuration(playerID) > demoteCooldown) {
		demoteTimes.drop(playerID);
		return false;
	}

	return true;
}

uint64 FrsManagerDataImplementation::getDemoteTime(uint64 playerID) {
	return demoteTimes.get(playerID).getMiliTime();
}

uint64 FrsManagerDataImplementation::getDemoteDuration(uint64 playerID) {
	return (Time().getMiliTime() - getDemoteTime(playerID));
}

